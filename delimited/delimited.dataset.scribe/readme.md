# About

isr.IO.Delimited.DataSet.Scribe is a .Net library supporting delimited dataset comma-separated-values I/O.

# How to Use

Read a data set using a column builder:
```
''' <summary> Reads a file by building the <see cref="Regex"/> column builder. </summary>
''' <remarks> David, 2022-02-23. </remarks>
''' <param name="fileName">        The file name. </param>
''' <param name="tableName">       Name of the table. </param>
''' <param name="dataGridView">    The data grid view. </param>
''' <param name="misReadsListBox"> The mis reads control. </param>
''' <returns> The file. </returns>
Private Shared Function ReadFile(ByVal fileName As String, ByVal tableName As String,
                                    ByVal dataGridView As DataGridView, ByVal misReadsListBox As ListBox) As isr.IO.DelimitedFileDataSet

    ' create a new instance of MyTextFileDataSet
    Dim textFileDataSet = New isr.IO.DelimitedFileDataSet()

    ' create a new RegexColumnBuilder
    Dim builder As New RegexColumnBuilder()
    builder.AddColumn("ID", ","c, RegexColumnType.Integer)
    builder.AddColumn("Name", ","c, RegexColumnType.String)
    builder.AddColumn("Date", ","c, RegexColumnType.Date)

    ' add the RegexColumnBuilder to the TextFileDataSet
    textFileDataSet.ApplyColumnBuilder(builder)

    ' set the optional table name - default is 'Table1' 
    textFileDataSet.TableName = tableName

    ' open the file 
    Dim path As String = System.IO.Path.Combine(System.AppDomain.CurrentDomain.BaseDirectory, fileName)
    Using fileStream As New FileStream(path, FileMode.Open, FileAccess.Read)
        ' fill the dataset
        textFileDataSet.Fill(fileStream)
    End Using

    ' display the misreads
    misReadsListBox.Items.Clear()
    For Each item As String In textFileDataSet.Misreads
        misReadsListBox.Items.Add(item)
    Next item

    ' display the dataset
    dataGridView.DataSource = textFileDataSet
    dataGridView.DataMember = textFileDataSet.TableName

    Return textFileDataSet
End Function
```

Read a data set using a regular expression:
```
''' <summary> Reads a file into a dataset using a <see cref="Regex"/> expression for building the columns. </summary>
''' <param name="fileName">        The file name. </param>
''' <param name="dataGridView">    The data grid view. </param>
''' <param name="misReadsListBox"> The mis reads control. </param>
''' <param name="expression">      The expression, e.g., ^(?<ID>[^,]+),(?<Name>[^,]+),(?<Date>[^,]+)$. </param>
''' <returns> The file. </returns>
Private Shared Function ReadFile(ByVal fileName As String, ByVal dataGridView As DataGridView,
                                    ByVal misReadsListBox As ListBox, ByVal expression As String) As isr.IO.DelimitedFileDataSet

    ' create a new instance of a delimited dataset specifying the regular expression for validating and recognizing columns
    Dim textFileDataSet = New isr.IO.DelimitedFileDataSet() With {
        .ContentExpression = New Regex(expression)
    }

    ' open the file 
    Dim path As String = System.IO.Path.Combine(System.AppDomain.CurrentDomain.BaseDirectory, fileName)
    Using fileStream As New FileStream(path, FileMode.Open, FileAccess.Read)
        ' fill the dataset
        textFileDataSet.Fill(fileStream)
    End Using

    ' display the misreads
    misReadsListBox.Items.Clear()
    For Each item As String In textFileDataSet.Misreads
        misReadsListBox.Items.Add(item)
    Next item

    ' display the dataset
    dataGridView.DataSource = textFileDataSet
    dataGridView.DataMember = textFileDataSet.TableName

    Return textFileDataSet
End Function
```

# Key Features

* Builds a data set from a delimited file.

# Main Types

The main types provided by this library are:

* _DelimitedFileDataSet_ Class for transforming a text-file into a dataset based on one regular expression.
* _RegexColumnBuilder_  Class for creating regular expressions for use in _DelimitedFileDataSet_.

# Feedback

isr.IO.Delimited.DataSet.Scribe is released as open source under the MIT license.
Bug reports and contributions are welcome at the [IO Repository].

[IO Repository]: https://bitbucket.org/davidhary/dn.io

