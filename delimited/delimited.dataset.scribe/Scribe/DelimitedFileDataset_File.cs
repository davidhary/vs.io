using System;
using System.IO;

namespace isr.IO
{
    public partial class DelimitedFileDataSet
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Constructs this class. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        public DelimitedFileDataSet() : base()
        {
            this._FullFileName = string.Empty;
            this.FileExtension = ".CSV";
            this.FileDialogFilter = "Text files (*.txt;*.csv;*.log)|*.txt;*.csv;*.log|All files (*.*)|*.*";
            this.FileDialogTitle = "Select File";
        }

        /// <summary> Creates a new DelimitedFileDataSet. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <returns> A DelimitedFileDataSet. </returns>
        public static DelimitedFileDataSet Create()
        {
            DelimitedFileDataSet ds = null;
            try
            {
                ds = new DelimitedFileDataSet();
                return ds;
            }
            catch
            {
                ds?.Dispose();
                throw;
            }
        }

        #endregion

        #region " FILE DIALOG "

        /// <summary> Gets or sets the file dialog filter. </summary>
        /// <value> The file dialog filter. </value>
        public string FileDialogFilter { get; set; }

        /// <summary> Gets or sets the file dialog title. </summary>
        /// <value> The file dialog title. </value>
        public string FileDialogTitle { get; set; }

#if false
        // to be implemented in the calling assembly
        
        /// <summary> Opens the File Open dialog box and gets a file name. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <returns> A Windows.Forms.DialogResult. </returns>
        public bool OpenFileDialog()
        {
            bool result;
            var Ofd = new  Core.OpenFileDialog()
            {
                FileDialogTitle = FileDialogTitle,
                FullFileName = FullFileName,
                FileDialogFilter = FileDialogFilter,
                FileExtension = FileExtension
            };
            result = Ofd.TrySelectFile();
            if (result)
                this.FullFileName = Ofd.FullFileName;
            return result;
        }

        /// <summary> Opens the File Save dialog box and gets a file name. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <returns> A Windows.Forms.DialogResult. </returns>
        public bool SaveFileDialog()
        {
            bool result;
            var Ofd = new Core.SaveFileDialog()
            {
                FileDialogTitle = FileDialogTitle,
                FullFileName = FullFileName,
                FileDialogFilter = FileDialogFilter,
                FileExtension = FileExtension
            };
            result = Ofd.TrySelectFile();
            if (result)
                this.FullFileName = Ofd.FullFileName;
            return result;
        }

        /// <summary> Try find file. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <param name="fileName"> Filename of the file. </param>
        /// <returns> A String if file found or empty if not. </returns>
        public static string TryFindFile(string fileName)
        {
            return Core.OpenFileDialog.TryFindFile(fileName);
        }
#endif

        #endregion

        #region " OPEN AND CLOSE "

        /// <summary> opens the file for input. </summary>
        /// <remarks> Use this method to open the instance. </remarks>
        public void Open()
        {
            try
            {
                this.TextFile = new FileStream( this.FullFileName, FileMode.Open, FileAccess.Read );
            }
            catch
            {
                // close to meet strong guarantees
                try
                {
                    this.Close();
                }
                finally
                {
                }

                throw;
            }
        }

        /// <summary> Open for output. </summary>
        /// <remarks> Use this method to open the instance.  The method Returns <c>True</c> if success or
        /// false if it failed opening the file. </remarks>
        /// <param name="append"> A Boolean expression that specifies if the new data will append to an
        /// existing file or if a new file will be Created possibly over-writing an existing file. </param>
        public void Open( bool append )
        {
            try
            {
                this.TextFile = append ? new FileStream( this.FullFileName, FileMode.Open, FileAccess.ReadWrite ) : new FileStream( this.FullFileName, FileMode.Open, FileAccess.Read );
            }
            catch
            {
                // close to meet strong guarantees
                try
                {
                    this.Close();
                }
                finally
                {
                }

                throw;
            }
        }

        /// <summary> Closes the instance. </summary>
        /// <remarks> Use this method to close the instance.  The method Returns <c>True</c> if success or
        /// false if it failed closing the instance. </remarks>
        public virtual void Close()
        {

            // close the file if not closed
            if ( this.IsOpen )
            {
                this.TextFile.Close();
            }
        }

        #endregion

        #region " IO "

        /// <summary> Determines whether the specified folder path is writable. </summary>
        /// <remarks>
        /// Uses a temporary random file name to test if the file can be created. The file is deleted
        /// thereafter.
        /// </remarks>
        /// <param name="path"> The path. </param>
        /// <returns> <c>True</c> if the specified path is writable; otherwise, <c>False</c>. </returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        public static bool IsFolderWritable( string path )
        {
            string filePath = string.Empty;
            bool affirmative = false;
            try
            {
                filePath = Path.Combine( path, Path.GetRandomFileName() );
                using ( var s = File.Open( filePath, FileMode.OpenOrCreate ) )
                {
                }

                affirmative = true;
            }
            catch
            {
            }
            finally
            {
                // SS reported an exception from this test possibly indicating that Windows allowed writing the file 
                // by failed report deletion. Or else, Windows raised another exception type.
                try
                {
                    if ( File.Exists( filePath ) )
                    {
                        File.Delete( filePath );
                    }
                }
                catch
                {
                }
            }

            return affirmative;
        }

        /// <summary> Selects the default file path. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <returns>
        /// The default file path: either the application folder or the user documents folder.
        /// </returns>
        public static string DefaultFolderPath()
        {
            string candidatePath = System.AppDomain.CurrentDomain.BaseDirectory;
            if ( !DelimitedFileDataSet.IsFolderWritable( candidatePath ) )
            {
                candidatePath = System.Environment.GetFolderPath( System.Environment.SpecialFolder.CommonDocuments );
            }
            return candidatePath;
        }

        #endregion

        #region " PROPERTIES "

        /// <summary> Gets or sets the file extension. </summary>
        /// <value> The file extension. </value>
        public string FileExtension { get; set; }

        /// <summary> Full pathname of the file. </summary>
        private string _FullFileName;

        /// <summary> Gets or sets the file name. </summary>
        /// <remarks> Use this property to get or set the file name. </remarks>
        /// <value> <c>FullFileName</c> is a String property. </value>
        public string FullFileName
        {
            get {
                if ( this._FullFileName.Length == 0 )
                {
                    // set default file name if empty.
                    this._FullFileName = Path.Combine( DelimitedFileDataSet.DefaultFolderPath(), System.Reflection.Assembly.GetExecutingAssembly().GetName().Name + this.FileExtension );
                }

                return this._FullFileName;
            }

            set => this._FullFileName = value;
        }

        /// <summary> Returns <c>True</c> if the file is open. </summary>
        /// <remarks> The file is open if the file number is not zero. </remarks>
        /// <value> <c>IsOpen</c>Is a Boolean property. </value>
        public bool IsOpen => this.TextFile is object; // Me.FileNumber <> 0

        /// <summary> Returns <c>True</c> if the file is at end of file. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <returns> <c>True</c> if end of file. </returns>
        public bool IsEndOfFile()
        {
            return this.TextFile.Position >= this.TextFile.Length;
        }

        /// <summary> Queries if a given file exists. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        public bool FileExists()
        {
            var fi = new FileInfo( this.FullFileName );
            return fi.Exists;
        }

        #endregion

    }
}
