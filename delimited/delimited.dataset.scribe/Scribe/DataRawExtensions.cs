
namespace isr.IO.Delimited.DataSet.Scribe.DataRowExtensions
{
    /// <summary>   A data row extensions. </summary>
    /// <remarks>   David, 2021-08-16. </remarks>
    public static class DataRowExtensionMethods
    {
        /// <summary>   A DataRow extension method that gets a field. </summary>
        /// <remarks>   David, 2021-08-17. </remarks>
        /// <typeparam name="T">    Generic type parameter. </typeparam>
        /// <param name="dataRow">      The dataRow to act on. </param>
        /// <param name="columnName">   Name of the column. </param>
        /// <returns>   The field. </returns>
        public static T GetField<T>( this System.Data.DataRow dataRow, string columnName )
        {
            return ( T ) dataRow[columnName];
        }
    }

}

