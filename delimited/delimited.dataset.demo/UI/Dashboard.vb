Imports System.IO
Imports System.Text.RegularExpressions

''' <summary> A dashboard. </summary>
''' <remarks> David, 2021-03-23. </remarks>
Partial Public Class Dashboard
    Inherits Form

    Public Sub New()
        Me.InitializeComponent()
    End Sub

    Private Const _FileName As String = "DemoText.txt"
    Private Const _TableName As String = "DemoText"


    Private _MyTextFileDataSet As isr.IO.DelimitedFileDataSet

    Private Sub OpenBuilder_Click(ByVal sender As Object, ByVal e As EventArgs) Handles OpenBuilder.Click
        Me._MyTextFileDataSet = Dashboard.ReadFile(Dashboard._FileName, Dashboard._TableName, Me.dataGridView1, Me.listBox1)
    End Sub

    ''' <summary> Reads a file by building the <see cref="Regex"/> column builder. </summary>
    ''' <remarks> David, 2022-02-23. </remarks>
    ''' <param name="fileName">        The file name. </param>
    ''' <param name="tableName">       Name of the table. </param>
    ''' <param name="dataGridView">    The data grid view. </param>
    ''' <param name="misReadsListBox"> The mis reads control. </param>
    ''' <returns> The file. </returns>
    Private Shared Function ReadFile(ByVal fileName As String, ByVal tableName As String,
                                     ByVal dataGridView As DataGridView, ByVal misReadsListBox As ListBox) As isr.IO.DelimitedFileDataSet

        ' create a new instance of MyTextFileDataSet
        Dim textFileDataSet = New isr.IO.DelimitedFileDataSet()

        ' create a new RegexColumnBuilder
        Dim builder As New RegexColumnBuilder()
        builder.AddColumn("ID", ","c, RegexColumnType.Integer)
        builder.AddColumn("Name", ","c, RegexColumnType.String)
        builder.AddColumn("Date", ","c, RegexColumnType.Date)

        ' add the RegexColumnBuilder to the TextFileDataSet
        textFileDataSet.ApplyColumnBuilder(builder)

        ' set the optional table name - default is 'Table1' 
        textFileDataSet.TableName = tableName

        ' open the file 
        Dim path As String = System.IO.Path.Combine(System.AppDomain.CurrentDomain.BaseDirectory, fileName)
        Using fileStream As New FileStream(path, FileMode.Open, FileAccess.Read)
            ' fill the dataset
            textFileDataSet.Fill(fileStream)
        End Using

        ' display the misreads
        misReadsListBox.Items.Clear()
        For Each item As String In textFileDataSet.Misreads
            misReadsListBox.Items.Add(item)
        Next item

        ' display the dataset
        dataGridView.DataSource = textFileDataSet
        dataGridView.DataMember = textFileDataSet.TableName

        Return textFileDataSet
    End Function

    Private Sub ShowDataSet()
        Me.dataGridView1.DataSource = Me._MyTextFileDataSet
        Me.dataGridView1.DataMember = Me._MyTextFileDataSet.TableName
    End Sub

    Private Sub ShowMisReads()
        Me.listBox1.Items.Clear()
        For Each item As String In Me._MyTextFileDataSet.Misreads
            Me.listBox1.Items.Add(item)
        Next item
    End Sub

    Private Sub OpenExpression_Click(ByVal sender As Object, ByVal e As EventArgs) Handles OpenExpression.Click
        Me._MyTextFileDataSet = Dashboard.ReadFile(Dashboard._FileName, Me.dataGridView1, Me.listBox1, Me.Expression.Text)
    End Sub

    ''' <summary> Reads a file into a dataset using a <see cref="Regex"/> expression for building the columns. </summary>
    ''' <param name="fileName">        The file name. </param>
    ''' <param name="dataGridView">    The data grid view. </param>
    ''' <param name="misReadsListBox"> The mis reads control. </param>
    ''' <param name="expression">      The expression, e.g., ^(?<ID>[^,]+),(?<Name>[^,]+),(?<Date>[^,]+)$. </param>
    ''' <returns> The file. </returns>
    Private Shared Function ReadFile(ByVal fileName As String, ByVal dataGridView As DataGridView,
                                     ByVal misReadsListBox As ListBox, ByVal expression As String) As isr.IO.DelimitedFileDataSet

        ' create a new instance of a delimited dataset specifying the regular expression for validating and recognizing columns
        Dim textFileDataSet = New isr.IO.DelimitedFileDataSet() With {
            .ContentExpression = New Regex(expression)
        }

        ' open the file 
        Dim path As String = System.IO.Path.Combine(System.AppDomain.CurrentDomain.BaseDirectory, fileName)
        Using fileStream As New FileStream(path, FileMode.Open, FileAccess.Read)
            ' fill the dataset
            textFileDataSet.Fill(fileStream)
        End Using

        ' display the misreads
        misReadsListBox.Items.Clear()
        For Each item As String In textFileDataSet.Misreads
            misReadsListBox.Items.Add(item)
        Next item

        ' display the dataset
        dataGridView.DataSource = textFileDataSet
        dataGridView.DataMember = textFileDataSet.TableName

        Return textFileDataSet
    End Function

End Class
