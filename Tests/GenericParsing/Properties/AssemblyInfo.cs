﻿using System.Reflection;
using System.Runtime.InteropServices;
[assembly: AssemblyTitle("GenericParsing.UnitTests")]
[assembly: AssemblyDescription("This assembly is used by the GenericParser for unit tests.")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyProduct("isr.IO.GenericParsingTests")]
