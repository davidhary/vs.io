Namespace NetListTests

    ''' <summary>
    ''' Contains all unit tests for the <see cref="isr.IO.NetList.Cable">Cable</see> class.
    ''' </summary>
    ''' <remarks>
    ''' (c) 2007 Integrated Scientific Resources, Inc. All rights reserved. <para>
    ''' Licensed under The MIT License. </para><para>  
    ''' David, 12/12/07, 6.0.2902. Created </para>
    ''' </remarks>
    <TestClass()>
    Public Class CableTest

#Region " CONSTRUCTION and CLEANUP "

        ''' <summary> My class initialize. </summary>
        ''' <remarks>
        ''' Use ClassInitialize to run code before running the first test in the class.
        ''' </remarks>
        ''' <param name="testContext"> Gets or sets the test context which provides information about
        '''                            and functionality for the current test run. </param>
        <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
        <CodeAnalysis.SuppressMessage("Style", "IDE0060:Remove unused parameter", Justification:="<Pending>")>
        <ClassInitialize(), CLSCompliant(False)>
        Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
            Try
                _TestSite = New TestSite
                _TestSite.AddTraceMessagesQueue(_TestSite.TraceMessagesQueueListener)
                _TestSite.AddTraceMessagesQueue(isr.Core.My.MyLibrary.UnpublishedTraceMessages)
                _TestSite.InitializeTraceListener()
            Catch
                ' cleanup to meet strong guarantees
                Try
                    MyClassCleanup()
                Finally
                End Try
                Throw
            End Try
        End Sub

        ''' <summary> My class cleanup. </summary>
        ''' <remarks> Use ClassCleanup to run code after all tests in a class have run. </remarks>
        <ClassCleanup()>
        Public Shared Sub MyClassCleanup()
            _TestSite?.Dispose()
        End Sub

        ''' <summary> Initializes before each test runs. </summary>
        ''' <remarks> David, 10/8/2020. </remarks>
        <TestInitialize()> Public Sub MyTestInitialize()
            ' assert reading of test settings from the configuration file.
            Assert.IsTrue(TestInfo.Exists, $"{NameOf(TestInfo)} settings should exist")
            Dim expectedUpperLimit As Double = 12
            Assert.IsTrue(Math.Abs(TestInfo.TimeZoneOffset) < expectedUpperLimit, $"{NameOf(TestSite.TimeZoneOffset)} should be lower than {expectedUpperLimit}")
            TestInfo.ClearMessageQueue()
        End Sub

        ''' <summary> Cleans up after each test has run. </summary>
        ''' <remarks> David, 10/8/2020. </remarks>
        <TestCleanup()> Public Sub MyTestCleanup()
            TestInfo.AssertMessageQueue()
        End Sub

        ''' <summary>
        ''' Gets the test context which provides information about and functionality for the current test
        ''' run.
        ''' </summary>
        ''' <value> The test context. </value>
        Public Property TestContext() As TestContext

        ''' <summary> The test site. </summary>
        Private Shared _TestSite As TestSite

        ''' <summary> Gets information describing the test. </summary>
        ''' <value> Information describing the test. </value>
        Private Shared ReadOnly Property TestInfo() As TestSite
            Get
                Return _TestSite
            End Get
        End Property

#End Region

        ''' <summary> Test reading DB9 null modem cable. </summary>
        ''' <remarks> David, 10/8/2020. </remarks>
        ''' <param name="cable">                   The cable. </param>
        ''' <param name="cableName">               Name of the cable. </param>
        ''' <param name="filePathName">            Full pathname of the file file. </param>
        ''' <param name="expectedConnectorsCount"> Number of expected connectors. </param>
        ''' <param name="expectedNodesCount">      Number of expected nodes. </param>
        ''' <param name="expectedPinsCount">       Number of expected pins. </param>
        ''' <param name="expectedWiresCount">      Number of expected wires. </param>
        Private Shared Sub GenericCableTest(ByVal cable As isr.IO.NetList.Cable,
                                    ByVal cableName As String, ByVal filePathName As String,
                                    ByVal expectedConnectorsCount As Integer,
                                    ByVal expectedNodesCount As Integer,
                                    ByVal expectedPinsCount As Integer,
                                    ByVal expectedWiresCount As Integer)
            Do
                Dim actual As Boolean = cable.ReadNetList(filePathName)
                Assert.IsTrue(actual, "Failed reading cable {0} from {1}", cableName, filePathName)
            Loop Until True

            Do
                Dim actual As Boolean = cable.PopulateWires(True)
                Assert.IsTrue(actual, "Failed populating cable {0} wires", cableName)
            Loop Until True

            Do
                Dim actualConnectorsCount As Integer = cable.Connectors.Count
                Assert.AreEqual(expectedConnectorsCount, actualConnectorsCount,
                        "Cable {0} Connectors count: Expected '{0}' ?= '{1}'", cableName, expectedConnectorsCount, actualConnectorsCount)
            Loop Until True

            Do
                Dim actualNodesCount As Integer = cable.Nodes.Count
                Assert.AreEqual(expectedNodesCount, actualNodesCount,
                        "Cable {0} Nodes count: Expected '{0}' ?= '{1}'", cableName, expectedNodesCount, actualNodesCount)
            Loop Until True

            Do
                Dim actualPinsCount As Integer = cable.Pins.Count
                Assert.AreEqual(expectedPinsCount, actualPinsCount,
                        "Cable {0} Pins count: Expected '{0}' ?= '{1}'", cableName, expectedPinsCount, actualPinsCount)
            Loop Until True

            Do
                Dim actualWiresCount As Integer = cable.Wires.Count
                Assert.AreEqual(expectedWiresCount, actualWiresCount,
                        "Cable {0} Wires count: Expected '{0}' ?= '{1}'", cableName, expectedWiresCount, actualWiresCount)
            Loop Until True


        End Sub

        ''' <summary> Test reading DB9 null modem cable. </summary>
        ''' <remarks> David, 10/8/2020. </remarks>
        ''' <param name="cableName">               Name of the cable. </param>
        ''' <param name="filePathName">            Full pathname of the file file. </param>
        ''' <param name="expectedConnectorsCount"> Number of expected connectors. </param>
        ''' <param name="expectedNodesCount">      Number of expected nodes. </param>
        ''' <param name="expectedPinsCount">       Number of expected pins. </param>
        ''' <param name="expectedWiresCount">      Number of expected wires. </param>
        Private Shared Sub GenericCableTest(ByVal cableName As String, ByVal filePathName As String,
                                    ByVal expectedConnectorsCount As Integer,
                                    ByVal expectedNodesCount As Integer,
                                    ByVal expectedPinsCount As Integer,
                                    ByVal expectedWiresCount As Integer)

            Using cable As New isr.IO.NetList.Cable
                GenericCableTest(cable, cableName, filePathName, expectedConnectorsCount,
                         expectedNodesCount, expectedPinsCount, expectedWiresCount)
            End Using
        End Sub

        ''' <summary> Test reading DB9 null modem cable. </summary>
        ''' <remarks> David, 10/8/2020. </remarks>
        <TestMethod(), Description("Reads 5 Connector Cable")>
        Public Sub FiveConnectorCableTest()
            Dim filePathName As String = ".\NetList\5 CONNECTOR CABLE.NET"
            GenericCableTest("5CONN", filePathName, 5, 26, 67, 123) ' 41)

        End Sub

        ''' <summary> Test reading DB9 null modem cable. </summary>
        ''' <remarks> David, 10/8/2020. </remarks>
        <TestMethod(), Description("Reads DB9 Cable")>
        Public Sub DB9CableTest()
            Dim filePathName As String = ".\NetList\DE-9 PIN TO PIN CABLE.NET"
            GenericCableTest("DB9", filePathName, 2, 9, 18, 9)
        End Sub

    End Class

End Namespace
