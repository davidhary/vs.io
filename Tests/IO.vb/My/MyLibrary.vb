Namespace Tests.My

    ''' <summary> Provides assembly information for the class library. </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    Partial Friend NotInheritable Class MyLibrary

        ''' <summary>
        ''' Constructor that prevents a default instance of this class from being created.
        ''' </summary>
        ''' <remarks> David, 10/8/2020. </remarks>
        Private Sub New()
            MyBase.New()
        End Sub

        ''' <summary> The assembly title. </summary>
        Public Const AssemblyTitle As String = "Structured IO Tests"

        ''' <summary> Information describing the assembly. </summary>
        Public Const AssemblyDescription As String = "Unit Tests for the Structured IO Library"

        ''' <summary> The assembly product. </summary>
        Public Const AssemblyProduct As String = "isr.IO.Structured.Tests"

    End Class

End Namespace


