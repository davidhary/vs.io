Imports FluentAssertions
Namespace OfficeTests

    ''' <summary>
    ''' Contains all unit tests for the <see cref="isr.IO.Office.ExcelReader">Excel Reader</see>
    ''' class.
    ''' </summary>
    ''' <remarks>
    ''' (c) 2019 Integrated Scientific Resources, Inc. All rights reserved.<para>
    ''' Licensed under The MIT License.</para><para>
    ''' David, 6/21/2019 </para>
    ''' </remarks>
    <TestClass()>
    Public Class ExcelReaderTest

#Region " CONSTRUCTION and CLEANUP "

        ''' <summary> My class initialize. </summary>
        ''' <remarks>
        ''' Use ClassInitialize to run code before running the first test in the class.
        ''' </remarks>
        ''' <param name="testContext"> Gets or sets the test context which provides information about
        '''                            and functionality for the current test run. </param>
        <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
        <CodeAnalysis.SuppressMessage("Style", "IDE0060:Remove unused parameter", Justification:="<Pending>")>
        <ClassInitialize(), CLSCompliant(False)>
        Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
            Try
                _TestSite = New TestSite
                _TestSite.AddTraceMessagesQueue(_TestSite.TraceMessagesQueueListener)
                _TestSite.AddTraceMessagesQueue(isr.Core.My.MyLibrary.UnpublishedTraceMessages)
                _TestSite.InitializeTraceListener()
            Catch
                ' cleanup to meet strong guarantees
                Try
                    MyClassCleanup()
                Finally
                End Try
                Throw
            End Try
        End Sub

        ''' <summary> My class cleanup. </summary>
        ''' <remarks> Use ClassCleanup to run code after all tests in a class have run. </remarks>
        <ClassCleanup()>
        Public Shared Sub MyClassCleanup()
            _TestSite?.Dispose()
        End Sub

        ''' <summary> Initializes before each test runs. </summary>
        ''' <remarks> David, 10/8/2020. </remarks>
        <TestInitialize()> Public Sub MyTestInitialize()
            ' assert reading of test settings from the configuration file.
            Assert.IsTrue(TestInfo.Exists, $"{NameOf(TestInfo)} settings should exist")
            Dim expectedUpperLimit As Double = 12
            Assert.IsTrue(Math.Abs(TestInfo.TimeZoneOffset) < expectedUpperLimit, $"{NameOf(TestSite.TimeZoneOffset)} should be lower than {expectedUpperLimit}")
            TestInfo.ClearMessageQueue()
        End Sub

        ''' <summary> Cleans up after each test has run. </summary>
        ''' <remarks> David, 10/8/2020. </remarks>
        <TestCleanup()> Public Sub MyTestCleanup()
            TestInfo.AssertMessageQueue()
        End Sub

        ''' <summary>
        ''' Gets the test context which provides information about and functionality for the current test
        ''' run.
        ''' </summary>
        ''' <value> The test context. </value>
        Public Property TestContext() As TestContext

        ''' <summary> The test site. </summary>
        Private Shared _TestSite As TestSite

        ''' <summary> Gets information describing the test. </summary>
        ''' <value> Information describing the test. </value>
        Private Shared ReadOnly Property TestInfo() As TestSite
            Get
                Return _TestSite
            End Get
        End Property

#End Region

#Region " EXCEL READER : Requires installing the Access Redistributable for Ace Provider "

        ''' <summary> Reads Workbook into tables. </summary>
        ''' <remarks> David, 10/8/2020. </remarks>
        <TestMethod(), Description("Reads Workbook into tables")>
        Public Sub ReadXlsxJetWorkbookReaderTest()
            Dim is64BitProcess As Boolean = IntPtr.Size = 8
            ' the Jet provider requires a 32 bit process.
            If is64BitProcess Then Return
            Using reader As New Office.ExcelReader
                isr.IO.Office.XlsxOdbcImport.PreferredProvider = isr.IO.Office.XlsxOdbcImport.JetProvider4
                Dim success As Boolean = reader.ReadWorkbook(ExcelReaderTestInfo.Get.FilePathName)
                success.Should().BeTrue($"workbook is read from {reader.FilePathName}")
            End Using
        End Sub

#End Region

    End Class

End Namespace
