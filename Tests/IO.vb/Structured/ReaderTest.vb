Namespace StructuredTests

    ''' <summary>
    ''' Contains all unit tests for the <see cref="isr.IO.StructuredReader">Reader</see> class.
    ''' </summary>
    ''' <remarks>
    ''' (c) 2007 Integrated Scientific Resources, Inc. All rights reserved. <para>
    ''' Licensed under The MIT License. </para><para>  
    ''' David, 12/08/07, 6.0.2898. created </para>
    ''' </remarks>
    <TestClass()>
    Public Class ReaderTest

#Region " CONSTRUCTION and CLEANUP "

        ''' <summary> My class initialize. </summary>
        ''' <remarks>
        ''' Use ClassInitialize to run code before running the first test in the class.
        ''' </remarks>
        ''' <param name="testContext"> Gets or sets the test context which provides information about
        '''                            and functionality for the current test run. </param>
        <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
        <ClassInitialize(), CLSCompliant(False)>
        Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
            Try
                _TestSite = New TestSite
                _TestSite.AddTraceMessagesQueue(_TestSite.TraceMessagesQueueListener)
                _TestSite.AddTraceMessagesQueue(isr.Core.My.MyLibrary.UnpublishedTraceMessages)
                _TestSite.InitializeTraceListener()
                ' write a record of data for testing to the test file.
                ReaderTest._Writer = New isr.IO.StructuredWriter With {
                .FilePathName = System.IO.Path.Combine(isr.Core.KnownFolders.GetPath(isr.Core.KnownFolder.Documents),
                                                       $"{NameOf(ReaderTest)}.csv")
            }
                ' ask for a new file.
                ReaderTest._Writer.NewFile()

                ' ask for a new record
                ReaderTest._Writer.NewRecord()

                ' add data to the record
                ReaderTest._Writer.AddField(_BooleanValue)
                ReaderTest._Writer.AddField(_ByteValue)
                ReaderTest._Writer.AddField(DateValue)
                ReaderTest._Writer.AddField(DateValue.ToOADate)
                ReaderTest._Writer.AddField(_DecimalValue)
                ReaderTest._Writer.AddField(_DoubleValue)
                ReaderTest._Writer.AddField(_ShortValue)
                ReaderTest._Writer.AddField(_IntegerValue)
                ReaderTest._Writer.AddField(_LongValue)
                ReaderTest._Writer.AddField(_SingleValue)
                ReaderTest._Writer.AddField(_StringValue)
                ReaderTest._Writer.WriteFields()

                ReaderTest._Reader = New isr.IO.StructuredReader(ReaderTest._Writer.FilePathName)
                Assert.IsTrue(ReaderTest._Reader.ReadFields, "Structured.Reader.ReadFields from: {0} ", ReaderTest._Reader.FilePathName)

            Catch ex As Exception
                ' close to meet strong guarantees
                Try
                    ReaderTest.MyClassCleanup()
                Finally
                End Try
            End Try

        End Sub

        ''' <summary> Runs code after all tests in a class have run. </summary>
        ''' <remarks> David, 10/8/2020. </remarks>
        <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
        <ClassCleanup()>
        Public Shared Sub MyClassCleanup()
            _TestSite?.Dispose()
            Try
                If Not ReaderTest._Reader Is Nothing Then
                    ReaderTest._Reader.Dispose()
                    ReaderTest._Reader = Nothing
                End If
                If Not ReaderTest._Writer Is Nothing Then
                    ReaderTest._Writer = Nothing
                End If
            Catch
            Finally
                ReaderTest._Reader = Nothing
                ReaderTest._Writer = Nothing
            End Try
        End Sub

        ''' <summary> Initializes before each test runs. </summary>
        ''' <remarks> David, 10/8/2020. </remarks>
        <TestInitialize()> Public Sub MyTestInitialize()
            ' assert reading of test settings from the configuration file.
            Assert.IsTrue(TestInfo.Exists, $"{NameOf(TestInfo)} settings should exist")
            Dim expectedUpperLimit As Double = 12
            Assert.IsTrue(Math.Abs(TestInfo.TimeZoneOffset) < expectedUpperLimit, $"{NameOf(TestSite.TimeZoneOffset)} should be lower than {expectedUpperLimit}")
            TestInfo.ClearMessageQueue()
        End Sub

        ''' <summary> Cleans up after each test has run. </summary>
        ''' <remarks> David, 10/8/2020. </remarks>
        <TestCleanup()> Public Sub MyTestCleanup()
            TestInfo.AssertMessageQueue()
        End Sub

        ''' <summary>
        ''' Gets the test context which provides information about and functionality for the current test
        ''' run.
        ''' </summary>
        ''' <value> The test context. </value>
        Public Property TestContext() As TestContext

        ''' <summary> The test site. </summary>
        Private Shared _TestSite As TestSite

        ''' <summary> Gets information describing the test. </summary>
        ''' <value> Information describing the test. </value>
        Private Shared ReadOnly Property TestInfo() As TestSite
            Get
                Return _TestSite
            End Get
        End Property

#End Region

#Region " ADDITIONAL TEST ATTRIBUTED METHODS "

        ''' <summary> Name of the section. </summary>
        Private Const _SectionName As String = "Unit Tests"

        ''' <summary> The reader. </summary>
        Private Shared _Reader As isr.IO.StructuredReader

        ''' <summary> The writer. </summary>
        Private Shared _Writer As isr.IO.StructuredWriter

        ''' <summary> The date value Date/Time. </summary>
        Private Shared ReadOnly DateValue As DateTime = DateTime.FromOADate(DateTime.Now.ToOADate)

        ''' <summary> The string value. </summary>
        Private Const _StringValue As String = "String value"

        ''' <summary> True to boolean value. </summary>
        Private Const _BooleanValue As Boolean = True

        ''' <summary> The byte value. </summary>
        Private Const _ByteValue As Byte = 129

        ''' <summary> The decimal value. </summary>
        Private Const _DecimalValue As Decimal = CDec(12.12)

        ''' <summary> The double value. </summary>
        Private Const _DoubleValue As Double = Math.PI

        ''' <summary> The short value. </summary>
        Private Const _ShortValue As Int16 = 16

        ''' <summary> The integer value. </summary>
        Private Const _IntegerValue As Int32 = 32

        ''' <summary> The long value. </summary>
        Private Const _LongValue As Int64 = 64

        ''' <summary> The single value. </summary>
        Private Const _SingleValue As Single = Single.MinValue

#End Region

        ''' <summary> A test for FieldBoolean. </summary>
        ''' <remarks> David, 10/8/2020. </remarks>
        <TestMethod()>
        Public Sub FieldBooleanTest()

            Dim actual As Boolean = ReaderTest._Reader.ParseField(0, actual)
            Dim expected As Boolean = _BooleanValue
            Assert.AreEqual(expected, actual, "Structured.Reader.FieldBoolean: Expected '{0}' ?= '{1}'", expected, actual)

        End Sub

        ''' <summary> A test for FieldByte. </summary>
        ''' <remarks> David, 10/8/2020. </remarks>
        <TestMethod()>
        Public Sub FieldByteTest()

            Dim actual As Byte = ReaderTest._Reader.ParseField(1, actual)
            Dim expected As Byte = _ByteValue
            Assert.AreEqual(expected, actual, "Structured.Reader.FieldByte: Expected '{0}' ?= '{1}'", expected, actual)

        End Sub

        ''' <summary> A test for FieldDate. </summary>
        ''' <remarks> David, 10/8/2020. </remarks>
        <TestMethod()>
        Public Sub FieldDateTest()

            Dim actual As DateTime = ReaderTest._Reader.ParseField(2, actual)
            Dim expected As DateTime = DateTime.FromOADate(DateValue.ToOADate)
            ' accurate withing 1 second.
            Assert.AreEqual(expected.ToLongDateString, actual.ToLongDateString, "Structured.Reader.FieldDate: Expected '{0}' ?= '{1}'", expected, actual)
            Assert.AreEqual(expected.ToLongTimeString, actual.ToLongTimeString, "Structured.Reader.FieldDate: Expected '{0}' ?= '{1}'", expected, actual)

        End Sub

        ''' <summary> A test for FieldDateTime. </summary>
        ''' <remarks> David, 10/8/2020. </remarks>
        <TestMethod()>
        Public Sub FieldDateTimeTest()

            Dim actual As Double = ReaderTest._Reader.ParseField(3, actual)
            Dim expected As DateTime = DateValue
            Assert.AreEqual(expected, DateTime.FromOADate(actual), "Structured.Reader.FieldDateTime: Expected '{0}' ?= '{1}'", expected, DateTime.FromOADate(actual))

        End Sub

        ''' <summary> A test for FieldDecimal. </summary>
        ''' <remarks> David, 10/8/2020. </remarks>
        <TestMethod()>
        Public Sub FieldDecimalTest()

            Dim actual As Decimal = ReaderTest._Reader.ParseField(4, actual)
            Dim expected As Decimal = _DecimalValue
            Assert.AreEqual(expected, actual, "Structured.Reader.FieldDecimal: Expected '{0}' ?= '{1}'", expected, actual)

        End Sub

        ''' <summary> A test for FieldDouble. </summary>
        ''' <remarks> David, 10/8/2020. </remarks>
        <TestMethod()>
        Public Sub FieldDoubleTest()

            Dim actual As Double = ReaderTest._Reader.ParseField(5, actual)
            Dim expected As Double = _DoubleValue
            Assert.AreEqual(expected, actual, 0.000000000001, "Structured.Reader.FieldDouble: Expected '{0}' ?= '{1}'", expected, actual)

        End Sub

        ''' <summary> A test for FieldInt16. </summary>
        ''' <remarks> David, 10/8/2020. </remarks>
        <TestMethod()>
        Public Sub FieldInt16Test()

            Dim actual As Int16 = ReaderTest._Reader.ParseField(6, actual)
            Dim expected As Int16 = _ShortValue
            Assert.AreEqual(expected, actual, "Structured.Reader.FieldInt16: Expected '{0}' ?= '{1}'", expected, actual)

        End Sub

        ''' <summary> A test for FieldInt32. </summary>
        ''' <remarks> David, 10/8/2020. </remarks>
        <TestMethod()>
        Public Sub FieldInt32Test()

            Dim actual As Int32 = ReaderTest._Reader.ParseField(7, actual)
            Dim expected As Int32 = _IntegerValue
            Assert.AreEqual(expected, actual, "Structured.Reader.FieldInt32: Expected '{0}' ?= '{1}'", expected, actual)

        End Sub

        ''' <summary> A test for FieldInt64. </summary>
        ''' <remarks> David, 10/8/2020. </remarks>
        <TestMethod()>
        Public Sub FieldInt64Test()

            Dim actual As Int64 = ReaderTest._Reader.ParseField(8, actual)
            Dim expected As Int64 = _LongValue
            Assert.AreEqual(expected, actual, "Structured.Reader.FieldInt64: Expected '{0}' ?= '{1}'", expected, actual)

        End Sub

        ''' <summary> A test for FieldSingle. </summary>
        ''' <remarks> David, 10/8/2020. </remarks>
        <TestMethod()>
        Public Sub FieldSingleTest()

            Dim actual As Single = ReaderTest._Reader.ParseField(9, actual)
            Dim expected As Single = _SingleValue
            Assert.AreEqual(Math.Abs(actual / expected), 1, 0.000001, "Structured.Reader.FieldSingle: Expected '{0}' ?= '{1}'", expected, actual)

        End Sub

        ''' <summary> A test for FieldString. </summary>
        ''' <remarks> David, 10/8/2020. </remarks>
        <TestMethod()>
        Public Sub FieldStringTest()
            Dim actual As String = ReaderTest._Reader.SelectField(10)
            Dim expected As String = _StringValue
            Assert.AreEqual(expected, actual, "Structured.Reader.FieldString: Expected '{0}' ?= '{1}'", expected, actual)

        End Sub

        ''' <summary> A test for ReadFields. </summary>
        ''' <remarks> David, 10/8/2020. </remarks>
        <TestMethod()>
        Public Sub ReadFieldsTest()
            If Not ReaderTest._Reader.Parser.EndOfData Then
                Assert.IsTrue(ReaderTest._Reader.ReadFields, "Structured.Reader.ReadFields from: {0} ", ReaderTest._Reader.FilePathName)
            End If
        End Sub

    End Class

End Namespace
