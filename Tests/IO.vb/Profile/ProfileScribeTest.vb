Imports FluentAssertions

Namespace ProfileTests

    ''' <summary>
    ''' Contains all unit tests for the <see cref="isr.IO.ProfileScribe">Profile Reader</see>
    ''' class.
    ''' </summary>
    ''' <remarks>
    ''' (c) 2019 Integrated Scientific Resources, Inc. All rights reserved.<para>
    ''' Licensed under The MIT License.</para><para>
    ''' David, 6/21/2019 </para>
    ''' </remarks>
    <TestClass()>
    Public Class ProfileScribeTest

#Region " CONSTRUCTION and CLEANUP "

        ''' <summary> My class initialize. </summary>
        ''' <remarks>
        ''' Use ClassInitialize to run code before running the first test in the class.
        ''' </remarks>
        ''' <param name="testContext"> Gets or sets the test context which provides information about
        '''                            and functionality for the current test run. </param>
        <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
        <CodeAnalysis.SuppressMessage("Style", "IDE0060:Remove unused parameter", Justification:="<Pending>")>
        <ClassInitialize(), CLSCompliant(False)>
        Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
            Try
                _TestSite = New TestSite
                _TestSite.AddTraceMessagesQueue(_TestSite.TraceMessagesQueueListener)
                _TestSite.AddTraceMessagesQueue(isr.Core.My.MyLibrary.UnpublishedTraceMessages)
                _TestSite.InitializeTraceListener()
                ProfileScribeTest._Scribe = New isr.IO.ProfileScribe
                ProfileScribeTest.DeleteProfileFile()
            Catch
                ' cleanup to meet strong guarantees
                Try
                    MyClassCleanup()
                Finally
                End Try
                Throw
            End Try
        End Sub

        ''' <summary> My class cleanup. </summary>
        ''' <remarks> Use ClassCleanup to run code after all tests in a class have run. </remarks>
        <ClassCleanup()>
        Public Shared Sub MyClassCleanup()
            _TestSite?.Dispose()
            If ProfileScribeTest._Scribe IsNot Nothing Then ProfileScribeTest._Scribe.Dispose()
            ProfileScribeTest._Scribe = Nothing
        End Sub

        ''' <summary> Initializes before each test runs. </summary>
        ''' <remarks> David, 10/8/2020. </remarks>
        <TestInitialize()> Public Sub MyTestInitialize()
            ' assert reading of test settings from the configuration file.
            Assert.IsTrue(TestInfo.Exists, $"{NameOf(TestInfo)} settings should exist")
            Dim expectedUpperLimit As Double = 12
            Assert.IsTrue(Math.Abs(TestInfo.TimeZoneOffset) < expectedUpperLimit, $"{NameOf(TestSite.TimeZoneOffset)} should be lower than {expectedUpperLimit}")
            TestInfo.ClearMessageQueue()
        End Sub

        ''' <summary> Cleans up after each test has run. </summary>
        ''' <remarks> David, 10/8/2020. </remarks>
        <TestCleanup()> Public Sub MyTestCleanup()
            TestInfo.AssertMessageQueue()
        End Sub

        ''' <summary>
        ''' Gets the test context which provides information about and functionality for the current test
        ''' run.
        ''' </summary>
        ''' <value> The test context. </value>
        Public Property TestContext() As TestContext

        ''' <summary> The test site. </summary>
        Private Shared _TestSite As TestSite

        ''' <summary> Gets information describing the test. </summary>
        ''' <value> Information describing the test. </value>
        Private Shared ReadOnly Property TestInfo() As TestSite
            Get
                Return _TestSite
            End Get
        End Property

#End Region

#Region " SHARED SCRIBE INSTANCE "

        ''' <summary> The scribe. </summary>
        Private Shared _Scribe As isr.IO.ProfileScribe

        ''' <summary> Delete profile. </summary>
        ''' <remarks> David, 10/8/2020. </remarks>
        Private Shared Sub DeleteProfileFile()
            If ProfileScribeTest._Scribe.FileExists Then
                ' delete the existing file.
                System.IO.File.Delete(ProfileScribeTest._Scribe.FilePath)
            End If
            ' the file is created on the first write.
        End Sub

#End Region

#Region " READER "

        ''' <summary> Reads Workbook into tables. </summary>
        ''' <remarks> David, 10/8/2020. </remarks>
        <TestMethod(), Description("Tests writing and reading a private profile string")>
        Public Sub WriteReadProfileStringTest()
            Dim sectionName As String = NameOf(Me.WriteReadProfileStringTest)
            Dim settingsName As String = "TestString"
            Dim expectedValue As String = DateTimeOffset.Now.Date.ToShortDateString
            ProfileScribeTest._Scribe.SectionName = sectionName

            ' test writing
            Dim actualOutcome As Boolean = ProfileScribeTest._Scribe.Write(settingsName, expectedValue)
            actualOutcome.Should().BeTrue($"scriber can write {settingsName}={expectedValue} into the {ProfileScribeTest._Scribe.SectionName} section of the {ProfileScribeTest._Scribe.FilePath} file")

            ' test reading using string buffer
            Dim actualvalue As String = IO.ProfileScribe.Read(ProfileScribeTest._Scribe.FilePath, ProfileScribeTest._Scribe.SectionName, settingsName, expectedValue)
            actualvalue.Should().Be(expectedValue, $"scriber can read {settingsName}={expectedValue} from the {ProfileScribeTest._Scribe.SectionName} section of the {ProfileScribeTest._Scribe.FilePath} file")

            ' test reading using string builder
            actualvalue = IO.ProfileScribe.ReadBuffer(ProfileScribeTest._Scribe.FilePath, ProfileScribeTest._Scribe.SectionName, settingsName, expectedValue)
            actualvalue.Should().Be(expectedValue, $"scriber read buffer can read {settingsName}={expectedValue} from the {ProfileScribeTest._Scribe.SectionName} section of the {ProfileScribeTest._Scribe.FilePath} file")


            ' this fails because it only reads the first character of the date string. 
            ' test reading using string A entry point
            ' actualvalue = IO.ProfileScriber.ReadA(ProfileScribeTest.Scribe.FilePath, ProfileScribeTest.Scribe.SectionName, settingsName, expectedValue)
            ' actualvalue.Should().Be(expectedValue, $"scriber read A can read {settingsName}={expectedValue} from the {ProfileScribeTest.Scribe.SectionName} section of the {ProfileScribeTest.Scribe.FilePath} file")

        End Sub

#End Region

    End Class

End Namespace
