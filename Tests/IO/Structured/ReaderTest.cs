using System;

using Microsoft.VisualBasic.CompilerServices;
using Microsoft.VisualStudio.TestTools.UnitTesting;

#pragma warning disable IDE1006 // Naming Styles
namespace isr.IO.StructuredTests
#pragma warning restore IDE1006 // Naming Styles
{

    /// <summary>
    /// Contains all unit tests for the <see cref="isr.IO.StructuredReader">Reader</see> class.
    /// </summary>
    /// <remarks>
    /// (c) 2007 Integrated Scientific Resources, Inc. All rights reserved. <para>
    /// Licensed under The MIT License. </para><para>
    /// David, 2007-12-08, 6.0.2898. Created </para>
    /// </remarks>
    [TestClass()]
    public class ReaderTest
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> My class initialize. </summary>
        /// <remarks>
        /// Use ClassInitialize to run code before running the first test in the class.
        /// </remarks>
        /// <param name="testContext"> Gets or sets the test context which provides information about
        /// and functionality for the current test run. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        [ClassInitialize()]
        [CLSCompliant( false )]
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Style", "IDE0060:Remove unused parameter", Justification = "<Pending>" )]
        public static void MyClassInitialize( TestContext testContext )
        {
            try
            {
                TestInfo = new TestSite();
                TestInfo.AddTraceMessagesQueue( TestInfo.TraceMessagesQueueListener );
                TestInfo.AddTraceMessagesQueue( Core.My.MyLibrary.UnpublishedTraceMessages );
                TestInfo.InitializeTraceListener();
                // write a record of data for testing to the test file.
                _Writer = new isr.IO.StructuredWriter()
                    {
                        FilePathName = System.IO.Path.Combine( isr.Core.KnownFolders.GetPath( isr.Core.KnownFolder.Documents ), $"{nameof( ReaderTest )}.csv" )
                    };
                // ask for a new file.
                _Writer.NewFile();

                // ask for a new record
                _Writer.NewRecord();

                // add data to the record
                _ = _Writer.AddField( _BooleanValue );
                _ = _Writer.AddField( _ByteValue );
                _ = _Writer.AddField( DateValue );
                _ = _Writer.AddField( DateValue.ToOADate() );
                _ = _Writer.AddField( _DecimalValue );
                _ = _Writer.AddField( _DoubleValue );
                _ = _Writer.AddField( _ShortValue );
                _ = _Writer.AddField( ( long ) _IntegerValue );
                _ = _Writer.AddField( _LongValue );
                _ = _Writer.AddField( _SingleValue );
                _ = _Writer.AddField( _StringValue );
                _Writer.WriteFields();
                _Reader = new StructuredReader( _Writer.FilePathName );
                Assert.IsTrue( _Reader.ReadFields(), "Structured.Reader.ReadFields from: {0} ", _Reader.FilePathName );
            }
            catch ( Exception )
            {
                // close to meet strong guarantees
                try
                {
                    MyClassCleanup();
                }
                finally
                {
                }
            }
        }

        /// <summary> Runs code after all tests in a class have run. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        [ClassCleanup()]
        public static void MyClassCleanup()
        {
            if ( TestInfo is object )
            {
                TestInfo.Dispose();
                TestInfo = null;
            }

            try
            {
                if ( _Reader is object )
                {
                    _Reader.Dispose();
                    _Reader = null;
                }

                if ( _Writer is object )
                {
                    _Writer = null;
                }
            }
            catch
            {
            }
            finally
            {
                _Reader = null;
                _Writer = null;
            }
        }

        /// <summary> Initializes before each test runs. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        [TestInitialize()]
        public void MyTestInitialize()
        {
            // assert reading of test settings from the configuration file.
            Assert.IsTrue( TestInfo.Exists, $"{nameof( TestInfo )} settings should exist" );
            double expectedUpperLimit = 12d;
            Assert.IsTrue( Math.Abs( TestInfo.TimeZoneOffset ) < expectedUpperLimit, $"{nameof( TestSite.TimeZoneOffset ).Split()} should be lower than {expectedUpperLimit}" );
            _ = TestInfo.ClearMessageQueue();
        }

        /// <summary> Cleans up after each test has run. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        [TestCleanup()]
        public void MyTestCleanup()
        {
            TestInfo.AssertMessageQueue();
        }

        /// <summary>
        /// Gets the test context which provides information about and functionality for the current test
        /// run.
        /// </summary>
        /// <value> The test context. </value>
        public TestContext TestContext { get; set; }

        /// <summary> Gets information describing the test. </summary>
        /// <value> Information describing the test. </value>
        private static TestSite TestInfo { get; set; }

        #endregion

        #region " ADDITIONAL TEST ATTRIBUTED METHODS "

        /// <summary> Name of the section. </summary>
        private const string _SectionName = "Unit Tests";

        /// <summary> The reader. </summary>
        private static StructuredReader _Reader;

        /// <summary> The writer. </summary>
        private static StructuredWriter _Writer;

        /// <summary> The date value Date/Time. </summary>
        private static readonly DateTime DateValue = DateTime.FromOADate( DateTime.Now.ToOADate() );

        /// <summary> The string value. </summary>
        private const string _StringValue = "String value";

        /// <summary> True to boolean value. </summary>
        private const bool _BooleanValue = true;

        /// <summary> The byte value. </summary>
        private const byte _ByteValue = 129;

        /// <summary> The decimal value. </summary>
        private const decimal _DecimalValue = 12.12m;

        /// <summary> The double value. </summary>
        private const double _DoubleValue = Math.PI;

        /// <summary> The short value. </summary>
        private const short _ShortValue = 16;

        /// <summary> The integer value. </summary>
        private const int _IntegerValue = 32;

        /// <summary> The long value. </summary>
        private const long _LongValue = 64L;

        /// <summary> The single value. </summary>
        private const float _SingleValue = float.MinValue;

        #endregion

        /// <summary> A test for FieldBoolean. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        [TestMethod()]
        public void FieldBooleanTest()
        {
            bool actual = _Reader.ParseField( 0, false );
            bool expected = _BooleanValue;
            Assert.AreEqual( expected, actual, "Structured.Reader.FieldBoolean: Expected '{0}' ?= '{1}'", expected, actual );
        }

        /// <summary> A test for FieldByte. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        [TestMethod()]
        public void FieldByteTest()
        {
            byte actual = _Reader.ParseField( 1, byte.MinValue );
            byte expected = _ByteValue;
            Assert.AreEqual( expected, actual, "Structured.Reader.FieldByte: Expected '{0}' ?= '{1}'", expected, actual );
        }

        /// <summary> A test for FieldDate. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        [TestMethod()]
        public void FieldDateTest()
        {
            DateTime actual = _Reader.ParseField( 2, DateTime.MinValue );
            DateTime expected = DateTime.FromOADate( DateValue.ToOADate() );
            // accurate withing 1 second.
            Assert.AreEqual( expected.ToLongDateString(), actual.ToLongDateString(), "Structured.Reader.FieldDate: Expected '{0}' ?= '{1}'", ( object ) expected, ( object ) actual );
            Assert.AreEqual( expected.ToLongTimeString(), actual.ToLongTimeString(), "Structured.Reader.FieldDate: Expected '{0}' ?= '{1}'", ( object ) expected, ( object ) actual );
        }

        /// <summary> A test for FieldDateTime. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        [TestMethod()]
        public void FieldDateTimeTest()
        {
            double actual = _Reader.ParseField( 3, double.NaN );
            var expected = DateValue;
            Assert.AreEqual( expected, DateTime.FromOADate( actual ), "Structured.Reader.FieldDateTime: Expected '{0}' ?= '{1}'", expected, DateTime.FromOADate( actual ) );
        }

        /// <summary> A test for FieldDecimal. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        [TestMethod()]
        public void FieldDecimalTest()
        {
            decimal actual = _Reader.ParseField( 4, decimal.Zero );
            decimal expected = _DecimalValue;
            Assert.AreEqual( expected, actual, "Structured.Reader.FieldDecimal: Expected '{0}' ?= '{1}'", expected, actual );
        }

        /// <summary> A test for FieldDouble. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        [TestMethod()]
        public void FieldDoubleTest()
        {
            double actual = _Reader.ParseField( 5, double.NaN );
            double expected = _DoubleValue;
            Assert.AreEqual( expected, actual, 0.000000000001d, "Structured.Reader.FieldDouble: Expected '{0}' ?= '{1}'", expected, actual );
        }

        /// <summary> A test for FieldInt16. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        [TestMethod()]
        public void FieldInt16Test()
        {
            short actual = _Reader.ParseField( 6, short.MinValue );
            short expected = _ShortValue;
            Assert.AreEqual( expected, actual, "Structured.Reader.FieldInt16: Expected '{0}' ?= '{1}'", expected, actual );
        }

        /// <summary> A test for FieldInt32. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        [TestMethod()]
        public void FieldInt32Test()
        {
            int actual = _Reader.ParseField( 7, int.MinValue );
            int expected = _IntegerValue;
            Assert.AreEqual( expected, actual, "Structured.Reader.FieldInt32: Expected '{0}' ?= '{1}'", expected, actual );
        }

        /// <summary> A test for FieldInt64. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        [TestMethod()]
        public void FieldInt64Test()
        {
            long actual = _Reader.ParseField( 8, long.MinValue );
            long expected = _LongValue;
            Assert.AreEqual( expected, actual, "Structured.Reader.FieldInt64: Expected '{0}' ?= '{1}'", expected, actual );
        }

        /// <summary> A test for FieldSingle. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        [TestMethod()]
        public void FieldSingleTest()
        {
            float actual = _Reader.ParseField( 9, float.NaN );
            float expected = _SingleValue;
            Assert.AreEqual( Math.Abs( actual / expected ), 1d, 0.000001d, "Structured.Reader.FieldSingle: Expected '{0}' ?= '{1}'", expected, actual );
        }

        /// <summary> A test for FieldString. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        [TestMethod()]
        public void FieldStringTest()
        {
            string actual = _Reader.SelectField( 10 );
            string expected = _StringValue;
            Assert.AreEqual( expected, actual, "Structured.Reader.FieldString: Expected '{0}' ?= '{1}'", expected, actual );
        }

        /// <summary> A test for ReadFields. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        [TestMethod()]
        public void ReadFieldsTest()
        {
            if ( !_Reader.Parser.EndOfData )
            {
                Assert.IsTrue( _Reader.ReadFields(), "Structured.Reader.ReadFields from: {0} ", _Reader.FilePathName );
            }
        }
    }
}
