﻿using System;

using FluentAssertions;

using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace isr.IO.ProfileTests
{

    /// <summary>
    /// Contains all unit tests for the <see cref="isr.IO.ProfileScribe">Profile Reader</see>
    /// class.
    /// </summary>
    /// <remarks>
    /// (c) 2019 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2019-06-21 </para>
    /// </remarks>
    [TestClass()]
    public class ProfileScribeTest
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> My class initialize. </summary>
        /// <remarks>
        /// Use ClassInitialize to run code before running the first test in the class.
        /// </remarks>
        /// <param name="testContext"> Gets or sets the test context which provides information about
        /// and functionality for the current test run. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Style", "IDE0060:Remove unused parameter", Justification = "<Pending>" )]
        [ClassInitialize()]
        [CLSCompliant( false )]
        public static void MyClassInitialize( TestContext testContext )
        {
            try
            {
                TestInfo = new TestSite();
                TestInfo.AddTraceMessagesQueue( TestInfo.TraceMessagesQueueListener );
                TestInfo.AddTraceMessagesQueue( Core.My.MyLibrary.UnpublishedTraceMessages );
                TestInfo.InitializeTraceListener();
                _Scribe = new ProfileScribe();
                DeleteProfileFile();
            }
            catch
            {
                // cleanup to meet strong guarantees
                try
                {
                    MyClassCleanup();
                }
                finally
                {
                }

                throw;
            }
        }

        /// <summary> My class cleanup. </summary>
        /// <remarks> Use ClassCleanup to run code after all tests in a class have run. </remarks>
        [ClassCleanup()]
        public static void MyClassCleanup()
        {
            if ( TestInfo is object )
            {
                TestInfo.Dispose();
                TestInfo = null;
            }

            if ( _Scribe is object )
                _Scribe.Dispose();
            _Scribe = null;
        }

        /// <summary> Initializes before each test runs. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        [TestInitialize()]
        public void MyTestInitialize()
        {
            // assert reading of test settings from the configuration file.
            Assert.IsTrue( TestInfo.Exists, $"{nameof( TestInfo )} settings should exist" );
            double expectedUpperLimit = 12d;
            Assert.IsTrue( Math.Abs( TestInfo.TimeZoneOffset ) < expectedUpperLimit, $"{nameof( TestSite.TimeZoneOffset ).Split()} should be lower than {expectedUpperLimit}" );
            _ = TestInfo.ClearMessageQueue();
        }

        /// <summary> Cleans up after each test has run. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        [TestCleanup()]
        public void MyTestCleanup()
        {
            TestInfo.AssertMessageQueue();
        }

        /// <summary>
        /// Gets the test context which provides information about and functionality for the current test
        /// run.
        /// </summary>
        /// <value> The test context. </value>
        public TestContext TestContext { get; set; }

        /// <summary> Gets information describing the test. </summary>
        /// <value> Information describing the test. </value>
        private static TestSite TestInfo { get; set; }

        #endregion

        #region " SHARED SCRIBE INSTANCE "

        /// <summary> The scribe. </summary>
        private static ProfileScribe _Scribe;

        /// <summary> Delete profile. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        private static void DeleteProfileFile()
        {
            if ( _Scribe.FileExists() )
            {
                // delete the existing file.
                System.IO.File.Delete( _Scribe.FilePath );
            }
            // the file is created on the first write.
        }

        #endregion

        #region " READER "

        /// <summary> Reads Workbook into tables. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        [TestMethod()]
        [Description( "Tests writing and reading a private profile string" )]
        public void WriteReadProfileStringTest()
        {
            string sectionName = nameof( this.WriteReadProfileStringTest );
            string settingsName = "TestString";
            string expectedValue = DateTimeOffset.Now.Date.ToShortDateString();
            _Scribe.SectionName = sectionName;

            // test writing
            bool actualOutcome = _Scribe.Write( settingsName, expectedValue );
            _ = actualOutcome.Should().BeTrue( $"scriber can write {settingsName}={expectedValue} into the {_Scribe.SectionName} section of the {_Scribe.FilePath} file" );

            // test reading using string buffer
            string actualvalue = ProfileScribe.Read( _Scribe.FilePath, _Scribe.SectionName, settingsName, expectedValue );
            _ = actualvalue.Should().Be( expectedValue, $"scriber can read {settingsName}={expectedValue} from the {_Scribe.SectionName} section of the {_Scribe.FilePath} file" );

            // test reading using string builder
            actualvalue = ProfileScribe.ReadBuffer( _Scribe.FilePath, _Scribe.SectionName, settingsName, expectedValue );
            _ = actualvalue.Should().Be( expectedValue, $"scriber read buffer can read {settingsName}={expectedValue} from the {_Scribe.SectionName} section of the {_Scribe.FilePath} file" );


            // this fails because it only reads the first character of the date string. 
            // test reading using string A entry point
            // actualvalue = IO.ProfileScriber.ReadA(ProfileScribeTest.Scribe.FilePath, ProfileScribeTest.Scribe.SectionName, settingsName, expectedValue)
            // actualvalue.Should().Be(expectedValue, $"scriber read A can read {settingsName}={expectedValue} from the {ProfileScribeTest.Scribe.SectionName} section of the {ProfileScribeTest.Scribe.FilePath} file")

        }

        #endregion

    }
}