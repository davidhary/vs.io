using System;

using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace isr.IO.NetListTests
{

    /// <summary>
    /// Contains all unit tests for the <see cref="isr.IO.NetList.Cable">Cable</see> class.
    /// </summary>
    /// <remarks>
    /// (c) 2007 Integrated Scientific Resources, Inc. All rights reserved. <para>
    /// Licensed under The MIT License. </para><para>
    /// David, 2007-12-12, 6.0.2902. Created </para>
    /// </remarks>
    [TestClass()]
    public class CableTest
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> My class initialize. </summary>
        /// <remarks>
        /// Use ClassInitialize to run code before running the first test in the class.
        /// </remarks>
        /// <param name="testContext"> Gets or sets the test context which provides information about
        /// and functionality for the current test run. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Style", "IDE0060:Remove unused parameter", Justification = "<Pending>" )]
        [ClassInitialize()]
        [CLSCompliant( false )]
        public static void MyClassInitialize( TestContext testContext )
        {
            try
            {
                TestInfo = new TestSite();
                TestInfo.AddTraceMessagesQueue( TestInfo.TraceMessagesQueueListener );
                TestInfo.AddTraceMessagesQueue( Core.My.MyLibrary.UnpublishedTraceMessages );
                TestInfo.InitializeTraceListener();
            }
            catch
            {
                // cleanup to meet strong guarantees
                try
                {
                    MyClassCleanup();
                }
                finally
                {
                }

                throw;
            }
        }

        /// <summary> My class cleanup. </summary>
        /// <remarks> Use ClassCleanup to run code after all tests in a class have run. </remarks>
        [ClassCleanup()]
        public static void MyClassCleanup()
        {
            if ( TestInfo is object )
            {
                TestInfo.Dispose();
                TestInfo = null;
            }
        }

        /// <summary> Initializes before each test runs. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        [TestInitialize()]
        public void MyTestInitialize()
        {
            // assert reading of test settings from the configuration file.
            Assert.IsTrue( TestInfo.Exists, $"{nameof( TestInfo )} settings should exist" );
            double expectedUpperLimit = 12d;
            Assert.IsTrue( Math.Abs( TestInfo.TimeZoneOffset ) < expectedUpperLimit, $"{nameof( TestSite.TimeZoneOffset ).Split()} should be lower than {expectedUpperLimit}" );
            _ = TestInfo.ClearMessageQueue();
        }

        /// <summary> Cleans up after each test has run. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        [TestCleanup()]
        public void MyTestCleanup()
        {
            TestInfo.AssertMessageQueue();
        }

        /// <summary>
        /// Gets the test context which provides information about and functionality for the current test
        /// run.
        /// </summary>
        /// <value> The test context. </value>
        public TestContext TestContext { get; set; }

        /// <summary> Gets information describing the test. </summary>
        /// <value> Information describing the test. </value>
        private static TestSite TestInfo { get; set; }

        #endregion

        /// <summary> Test reading DB9 null modem cable. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <param name="cable">                   The cable. </param>
        /// <param name="cableName">               Name of the cable. </param>
        /// <param name="filePathName">            Full pathname of the file file. </param>
        /// <param name="expectedConnectorsCount"> Number of expected connectors. </param>
        /// <param name="expectedNodesCount">      Number of expected nodes. </param>
        /// <param name="expectedPinsCount">       Number of expected pins. </param>
        /// <param name="expectedWiresCount">      Number of expected wires. </param>
        private static void GenericCableTest( NetList.Cable cable, string cableName, string filePathName, int expectedConnectorsCount, int expectedNodesCount, int expectedPinsCount, int expectedWiresCount )
        {
            do
            {
                bool actual = cable.ReadNetList( filePathName );
                Assert.IsTrue( actual, "Failed reading cable {0} from {1}", cableName, filePathName );
            }
            while ( false );
            do
            {
                bool actual = cable.PopulateWires( true );
                Assert.IsTrue( actual, "Failed populating cable {0} wires", cableName );
            }
            while ( false );
            do
            {
                int actualConnectorsCount = cable.Connectors.Count;
                Assert.AreEqual( expectedConnectorsCount, actualConnectorsCount, "Cable {0} Connectors count: Expected '{0}' ?= '{1}'", cableName, expectedConnectorsCount, actualConnectorsCount );
            }
            while ( false );
            do
            {
                int actualNodesCount = cable.Nodes.Count;
                Assert.AreEqual( expectedNodesCount, actualNodesCount, "Cable {0} Nodes count: Expected '{0}' ?= '{1}'", cableName, expectedNodesCount, actualNodesCount );
            }
            while ( false );
            do
            {
                int actualPinsCount = cable.Pins.Count;
                Assert.AreEqual( expectedPinsCount, actualPinsCount, "Cable {0} Pins count: Expected '{0}' ?= '{1}'", cableName, expectedPinsCount, actualPinsCount );
            }
            while ( false );
            do
            {
                int actualWiresCount = cable.Wires.Count;
                Assert.AreEqual( expectedWiresCount, actualWiresCount, "Cable {0} Wires count: Expected '{0}' ?= '{1}'", cableName, expectedWiresCount, actualWiresCount );
            }
            while ( false );
        }

        /// <summary> Test reading DB9 null modem cable. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <param name="cableName">               Name of the cable. </param>
        /// <param name="filePathName">            Full pathname of the file file. </param>
        /// <param name="expectedConnectorsCount"> Number of expected connectors. </param>
        /// <param name="expectedNodesCount">      Number of expected nodes. </param>
        /// <param name="expectedPinsCount">       Number of expected pins. </param>
        /// <param name="expectedWiresCount">      Number of expected wires. </param>
        private static void GenericCableTest( string cableName, string filePathName, int expectedConnectorsCount, int expectedNodesCount, int expectedPinsCount, int expectedWiresCount )
        {
            using var cable = new NetList.Cable();
            GenericCableTest( cable, cableName, filePathName, expectedConnectorsCount, expectedNodesCount, expectedPinsCount, expectedWiresCount );
        }

        /// <summary> Test reading DB9 null modem cable. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        [TestMethod()]
        [Description( "Reads 5 Connector Cable" )]
        public void FiveConnectorCableTest()
        {
            string filePathName = @".\NetList\5 CONNECTOR CABLE.NET";
            GenericCableTest( "5CONN", filePathName, 5, 26, 67, 123 ); // 41)
        }

        /// <summary> Test reading DB9 null modem cable. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        [TestMethod()]
        [Description( "Reads DB9 Cable" )]
        public void DB9CableTest()
        {
            string filePathName = @".\NetList\DE-9 PIN TO PIN CABLE.NET";
            GenericCableTest( "DB9", filePathName, 2, 9, 18, 9 );
        }
    }
}
