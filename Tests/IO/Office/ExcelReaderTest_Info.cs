﻿using System.Collections.Generic;
using System.Diagnostics;

namespace isr.IO.OfficeTests
{

    /// <summary> A Excel Reader Test Info. </summary>
        /// <remarks> (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
        /// Licensed under The MIT License.</para><para>
        /// David, 2018-02-12 </para></remarks>
    [System.Runtime.CompilerServices.CompilerGenerated()]
    [System.CodeDom.Compiler.GeneratedCode("Microsoft.VisualStudio.Editors.SettingsDesigner.SettingsSingleFileGenerator", "16.7.0.0")]
    [System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Advanced)]
    internal class ExcelReaderTestInfo : Core.ApplicationSettingsBase
    {

        #region " SINGLETON "

        /// <summary>
        /// Initializes an instance of the <see cref="T:System.Configuration.ApplicationSettingsBase" />
        /// class to its default state.
        /// </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        private ExcelReaderTestInfo() : base()
        {
        }

        /// <summary>
        /// Gets the locking object to enforce thread safety when creating the singleton instance.
        /// </summary>
        /// <value> The sync locker. </value>
        private static object _SyncLocker { get; set; } = new object();

        /// <summary> Gets the instance. </summary>
        /// <value> The instance. </value>
        private static ExcelReaderTestInfo _Instance { get; set; }

        /// <summary> Instantiates the class. </summary>
        /// <remarks> Use this property to instantiate a single instance of this class. This class uses
        /// lazy instantiation, meaning the instance isn't created until the first time it's retrieved. </remarks>
        /// <returns> A new or existing instance of the class. </returns>
        public static ExcelReaderTestInfo Get()
        {
            if (_Instance is null)
            {
                lock (_SyncLocker)
                    _Instance = (ExcelReaderTestInfo)Synchronized(new ExcelReaderTestInfo());
            }

            return _Instance;
        }

        /// <summary> Returns true if an instance of the class was created and not disposed. </summary>
        /// <value> <c>True</c> if instantiated; otherwise, <c>False</c>. </value>
        public static bool Instantiated
        {
            get
            {
                lock (_SyncLocker)
                    return _Instance is object;
            }
        }

        #endregion

        #region " SETTINGS EDITORS EXCLUDED "

        /// <summary> Opens the settings editor. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        public static void OpenSettingsEditor()
        {
            Core.WindowsForms.EditConfiguration($"{typeof(ExcelReaderTestInfo)} Editor", Get());
        }

        #endregion

        #region " CONFIGURATION INFORMATION "

        /// <summary> Returns true if test settings exist. </summary>
        /// <value> <c>True</c> if testing settings exit. </value>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Code Quality", "IDE0051:Remove unused private members", Justification = "<Pending>")]
        [System.Configuration.UserScopedSetting()]
        [System.Configuration.DefaultSettingValue("True")]
        public bool Exists
        {
            get
            {
                return AppSettingGetter(false);
            }

            set
            {
                AppSettingSetter(value);
            }
        }

        /// <summary> Returns true to output test messages at the verbose level. </summary>
        /// <value> The verbose messaging level. </value>
        [System.Configuration.UserScopedSetting()]
        [System.Configuration.DefaultSettingValue("False")]
        public bool Verbose
        {
            get
            {
                return AppSettingGetter(false);
            }

            set
            {
                AppSettingSetter(value);
            }
        }

        /// <summary>
        /// Gets or sets the sentinel indicating of all data are to be used for a test.
        /// </summary>
        /// <value> <c>true</c> if all data are to be used for a test; otherwise <c>false</c>. </value>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Code Quality", "IDE0051:Remove unused private members", Justification = "<Pending>")]
        [System.Configuration.UserScopedSetting()]
        [System.Configuration.DefaultSettingValue("True")]
        public bool All
        {
            get
            {
                return AppSettingGetter(false);
            }

            set
            {
                AppSettingSetter(value);
            }
        }

        /// <summary> True if the test set is enabled. </summary>
        /// <value> The enabled option. </value>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Code Quality", "IDE0051:Remove unused private members", Justification = "<Pending>")]
        [System.Configuration.UserScopedSetting()]
        [System.Configuration.DefaultSettingValue("True")]
        public bool Enabled
        {
            get
            {
                return AppSettingGetter(false);
            }

            set
            {
                AppSettingSetter(value);
            }
        }

        #endregion

        #region " WORKBOOK SETTINGS "

        /// <summary> Gets the full pathname of the file. </summary>
        /// <value> The full pathname of the file. </value>
        internal string FilePathName
        {
            get
            {
                return System.IO.Path.Combine(My.MyProject.Application.Info.DirectoryPath, FileName);
            }
        }

        /// <summary> Gets or sets the filename of the file. </summary>
        /// <value> The name of the file. </value>
        [System.Configuration.UserScopedSetting()]
        [DebuggerNonUserCode()]
        [System.Configuration.DefaultSettingValue(@"\Office\Products.xlsx")]
        public string FileName
        {
            get
            {
                return AppSettingGetter(string.Empty);
            }

            set
            {
                AppSettingSetter(value);
            }
        }

        /// <summary> Gets or sets a list of names of the sheets. </summary>
        /// <value> A list of names of the sheets. </value>
        [System.Configuration.UserScopedSetting()]
        [DebuggerNonUserCode()]
        [System.Configuration.DefaultSettingValue("'Categories','Products','Sales',SalesDetails'")]
        public string SheetNames
        {
            get
            {
                return AppSettingGetter(string.Empty);
            }

            set
            {
                AppSettingSetter(value);
            }
        }

        /// <summary> The sheet name values. </summary>
        private System.Collections.ObjectModel.Collection<string> _SheetNameValues;

        /// <summary> Gets the sheet name values. </summary>
        /// <value> The sheet name values. </value>
        public System.Collections.ObjectModel.Collection<string> SheetNameValues
        {
            get
            {
                if (_SheetNameValues is null)
                {
                    _SheetNameValues = new System.Collections.ObjectModel.Collection<string>(new List<string>(SheetNames.Split(',')));
                }

                return _SheetNameValues;
            }
        }

        /// <summary> Gets or sets the category query. </summary>
        /// <value> The category query. </value>
        [System.Configuration.UserScopedSetting()]
        [DebuggerNonUserCode()]
        [System.Configuration.DefaultSettingValue("Select [CategoryName] from [Categories] Where ID=1;")]
        public string CategoryQuery
        {
            get
            {
                return AppSettingGetter(string.Empty);
            }

            set
            {
                AppSettingSetter(value);
            }
        }

        /// <summary> Gets or sets the category result. </summary>
        /// <value> The category result. </value>
        [System.Configuration.UserScopedSetting()]
        [DebuggerNonUserCode()]
        [System.Configuration.DefaultSettingValue("Air Filters")]
        public string CategoryResult
        {
            get
            {
                return AppSettingGetter(string.Empty);
            }

            set
            {
                AppSettingSetter(value);
            }
        }

        /// <summary> Gets or sets the number of categories rows. </summary>
        /// <value> The number of categories rows. </value>
        [System.Configuration.UserScopedSetting()]
        [DebuggerNonUserCode()]
        [System.Configuration.DefaultSettingValue("6")]
        public int CategoriesRowCount
        {
            get
            {
                return AppSettingGetter(0);
            }

            set
            {
                AppSettingSetter(value);
            }
        }

        #endregion

    }
}