﻿using System;

using FluentAssertions;

using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace isr.IO.OfficeTests
{

    /// <summary>
    /// Contains all unit tests for the <see cref="isr.IO.Office.ExcelReader">Excel Reader</see>
    /// class.
    /// </summary>
    /// <remarks>
    /// (c) 2019 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2019-06-21 </para>
    /// </remarks>
    [TestClass()]
    public class ExcelReaderTest
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> My class initialize. </summary>
        /// <remarks>
        /// Use ClassInitialize to run code before running the first test in the class.
        /// </remarks>
        /// <param name="testContext"> Gets or sets the test context which provides information about
        /// and functionality for the current test run. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Style", "IDE0060:Remove unused parameter", Justification = "<Pending>" )]
        [ClassInitialize()]
        [CLSCompliant( false )]
        public static void MyClassInitialize( TestContext testContext )
        {
            try
            {
                TestInfo = new TestSite();
                TestInfo.AddTraceMessagesQueue( TestInfo.TraceMessagesQueueListener );
                TestInfo.AddTraceMessagesQueue( Core.My.MyLibrary.UnpublishedTraceMessages );
                TestInfo.InitializeTraceListener();
            }
            catch
            {
                // cleanup to meet strong guarantees
                try
                {
                    MyClassCleanup();
                }
                finally
                {
                }

                throw;
            }
        }

        /// <summary> My class cleanup. </summary>
        /// <remarks> Use ClassCleanup to run code after all tests in a class have run. </remarks>
        [ClassCleanup()]
        public static void MyClassCleanup()
        {
            if ( TestInfo is object )
            {
                TestInfo.Dispose();
                TestInfo = null;
            }
        }

        /// <summary> Initializes before each test runs. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        [TestInitialize()]
        public void MyTestInitialize()
        {
            // assert reading of test settings from the configuration file.
            Assert.IsTrue( TestInfo.Exists, $"{nameof( TestInfo )} settings should exist" );
            double expectedUpperLimit = 12d;
            Assert.IsTrue( Math.Abs( TestInfo.TimeZoneOffset ) < expectedUpperLimit, $"{nameof( TestSite.TimeZoneOffset ).Split()} should be lower than {expectedUpperLimit}" );
            _ = TestInfo.ClearMessageQueue();
        }

        /// <summary> Cleans up after each test has run. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        [TestCleanup()]
        public void MyTestCleanup()
        {
            TestInfo.AssertMessageQueue();
        }

        /// <summary>
        /// Gets the test context which provides information about and functionality for the current test
        /// run.
        /// </summary>
        /// <value> The test context. </value>
        public TestContext TestContext { get; set; }

        /// <summary> Gets information describing the test. </summary>
        /// <value> Information describing the test. </value>
        private static TestSite TestInfo { get; set; }

        #endregion

        #region " EXCEL READER : Requires installing the Access Redistributable for Ace Provider "

        /// <summary> Reads Workbook into tables. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        [TestMethod()]
        [Description( "Reads Workbook into tables" )]
        public void ReadXlsxJetWorkbookReaderTest()
        {
            bool is64BitProcess = IntPtr.Size == 8;
            // the Jet provider requires a 32 bit process.
            if ( is64BitProcess )
                return;
            using ( var reader = new Office.ExcelReader() )
            {
                Office.XlsxOdbcImport.PreferredProvider = Office.XlsxOdbcImport.JetProvider4;
                bool success = reader.ReadWorkbook( ExcelReaderTestInfo.Get().FilePathName );
                _ = success.Should().BeTrue( $"workbook is read from {reader.FilePathName}" );
            }
        }

        #endregion

    }
}