﻿using System;
using System.Reflection;

[assembly: AssemblyTitle( isr.IO.Tests.My.MyLibrary.AssemblyTitle )]
[assembly: AssemblyDescription( isr.IO.Tests.My.MyLibrary.AssemblyDescription )]
[assembly: AssemblyProduct( isr.IO.Tests.My.MyLibrary.AssemblyProduct )]
[assembly: CLSCompliant( true )]
[assembly: System.Runtime.InteropServices.ComVisible( false )]
