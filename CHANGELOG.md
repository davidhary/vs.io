# Changelog
All notable changes to these libraries will be documented in this file.
The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/)

## [4.0.7140] - 2019-07-20
* Adds unit tests for the profile scribe. Renames Profile Scriber to Profile Scribe and adds read functionality for using settings name. Replaces string collection for item lists with Enumerable of string.

## [4.0.7112] - 2019-06-22
* Breaks backwards compatibility of the Excel reader due to the break in the Jet provider, which no longer works.

## [3.2.6667] - 2018-04-03
* 2018 release.

## [3.1.6505] - 2017-10-23
* Breaking Delimited File Dataset: Replaces skip first row with skip first row.

## [3.0.6393] - 2017-07-03
* Breaking. Changes file dialogs to always return dialog result.

## [2.1.6384] - 2017-06-24
* Defaults to UTC time.

## [2.1.6295] - 2017-03-27
* Adds Delimited File dataset.

## [2.1.6174] - 2016-11-26
* Cryptography: Supports key management, using a known key, and trust validation based on the original signing key.

## [2.0.6173] - 2016-11-25
* Cryptography: Modifies XML Signature. Changes break backwards compatibility. Supports license signature file.

## [1.2.5718] - 2015-08-28
*  Uses null-conditionals. Requires VS 2015 and above.

## [1.2.5303] - 2014-07-09
* Adds file watcher classes.

## [1.2.5211] - 2014-04-08
* Structured Reader: Adds methods to read contiguous records, returning nothing if the parser skipped a row.

## [1.2.5204] - 2014-04-01
* Updates Structured reader to parse and try parse values.

## [1.2.4710] - 2012-11-23
* Removes .VB tags from assemblies.

## [1.2.4504] - 2012-05-01
* Splits off a solution for x86 because x64 does not support OLEDB.4.0 that is used for reading Excel files. Adds x86 project.

## [1.2.4498] - 2012-04-17
* Implements code analysis rules for .NET 4.0.

## [1.2.4232] - 2011-08-03
* Standardize code elements and documentation.

## [1.2.4213] - 2011-07-15
* Simplifies the assembly information.

## [1.2.3294] - 2009-01-07
* Add Excel Import and Excel reader and interface. Add object parser.

## [1.2.3141] - 2008-08-07
* Net List Reader: Add support for military style pins. Allow populating the connector before reading. Add mating side and relative pin number.

## [1.2.2961] - 2008-02-09
* Update to .NET 3.5.

## [1.1.2908] - 2007-12-18
* Upgrade to Visual Studio 2008. Add Net List classes. Add unit tests for Structured I/O and net list. Remove NUNIT tests.

## [1.1.2301] - 2006-04-20
* Upgrade to Visual Studio 2005.

## [1.0.2257] - 2006-03-07
* Add binary extended reader and writer.

## [1.0.2219] - 2006-01-28
* Remove Visual Basic import.

## [1.0.2206] - 2006-01-15
* Use path name for the full file and folder names. New support, core, and exception classes. Use Int32, Int64, and Int16 instead of Integer, Long, and Short.

## [1.0.2205] - 2006-01-14
* Use path name for folder and file name specifications.

## [1.0.1933] - 2005-04-17
* Created from Delimited File, XML Sign, and Token Sign.

\(C\) 2005 Integrated Scientific Resources, Inc. All rights reserved.


```
## Release template - [version] - [date]
## Unreleased
### Added
### Changed
### Deprecated
### Removed
### Fixed
*<project name>*
```
