﻿Namespace FileWatcher

    ''' <summary>
    ''' Contains settings that initializes the file system watchers created within the Watcher class.
    ''' </summary>
    ''' <remarks>
    ''' (c) 2010 John Simmons.<para>
    ''' Licensed under The MIT License.</para><para>
    ''' David, 7/8/2014, . based on
    ''' http://www.codeproject.com/Articles/58740/FileSystemWatcher-Pure-Chaos-Part-of. </para>
    ''' </remarks>
    Public Class WatcherInfo

        ''' <summary> Default constructor. </summary>
        ''' <remarks> David, 10/8/2020. </remarks>
        Public Sub New()
            Me.WatchPath = String.Empty
            Me.IncludeSubfolders = False
            Me.WatchForError = False
            Me.WatchForDisposed = False
            Me.ChangesFilters = System.IO.NotifyFilters.Attributes
            Me.WatchesFilters = System.IO.WatcherChangeTypes.All
            Me.FileFilter = String.Empty
            Me.BufferSize = 8 * 1024
            Me.MonitorPathInterval = TimeSpan.Zero
        End Sub

        ''' <summary> Gets or sets the full pathname of the watch file. </summary>
        ''' <value> The full pathname of the watch file. </value>
        Public Property WatchPath() As String

        ''' <summary> Gets or sets the include sub folders. </summary>
        ''' <value> The include sub folders. </value>
        Public Property IncludeSubfolders() As Boolean

        ''' <summary> Gets or sets the watch for error. </summary>
        ''' <value> The watch for error. </value>
        Public Property WatchForError() As Boolean

        ''' <summary> Gets or sets the watch for disposed. </summary>
        ''' <value> The watch for disposed. </value>
        Public Property WatchForDisposed() As Boolean

        ''' <summary> Gets or sets the changes filters. </summary>
        ''' <value> The changes filters. </value>
        Public Property ChangesFilters() As System.IO.NotifyFilters

        ''' <summary> Gets or sets the watches filters. </summary>
        ''' <value> The watches filters. </value>
        Public Property WatchesFilters() As System.IO.WatcherChangeTypes

        ''' <summary> Gets or sets the file filter. </summary>
        ''' <value> The file filter. </value>
        Public Property FileFilter() As String

        ''' <summary> Maximum Size of the buffer in KBytes. </summary>
        Private Const maximumBufferSize As Integer = 64

        ''' <summary> Minimum Size of the buffer in KBytes. </summary>
        Private Const minimumBufferSize As Integer = 4

        ''' <summary> The buffer k in bytes. </summary>
        Private _BufferSize As Integer

        ''' <summary> Gets or sets the buffer size. </summary>
        ''' <value> The buffer k bytes; defaults to 8 KBytes. </value>
        Public Property BufferSize() As Integer
            Get
                Return Me._BufferSize
            End Get
            Set(value As Integer)
                Dim kBytes As Integer = CInt(Math.Ceiling(value / 1024))
                Me._BufferSize = 1024 * Math.Max(Math.Min(kBytes, maximumBufferSize), minimumBufferSize)
            End Set
        End Property

        ''' <summary> Gets or sets the monitor path interval. </summary>
        ''' <value> The monitor path interval. </value>
        Public Property MonitorPathInterval() As TimeSpan

    End Class

End Namespace
