﻿Imports System.IO
Namespace FileWatcher

    ''' <summary>
    ''' This class allows us to pass any type of watcher arguments to the calling object's handler
    ''' via a single object instead of having to add a lot of event handlers for the various event
    ''' arguments types.
    ''' </summary>
    ''' <remarks>
    ''' (c) 2010 John Simmons.<para>
    ''' Licensed under The MIT License.</para><para>
    ''' David, 7/8/2014. Created based on
    ''' http://www.codeproject.com/Articles/58740/FileSystemWatcher-Pure-Chaos-Part-of. </para>
    ''' </remarks>
    Public Class WatcherEventArgs
        Inherits System.EventArgs

#Region " Constructors "

        ''' <summary> Constructor. </summary>
        ''' <remarks> David, 10/8/2020. </remarks>
        ''' <param name="watcher">      The watcher. </param>
        ''' <param name="arguments">    The arguments. </param>
        ''' <param name="argumentType"> Type of the argument. </param>
        ''' <param name="filter">       Specifies the filter. </param>
        Public Sub New(ByVal watcher As FileSystemWatcher, ByVal arguments As Object, ByVal argumentType As ArgumentType, ByVal filter As NotifyFilters)
            Me.Watcher = watcher
            Me.Arguments = arguments
            Me.ArgumentType = argumentType
            Me.Filter = filter
        End Sub

        ''' <summary> Constructor. </summary>
        ''' <remarks> David, 10/8/2020. </remarks>
        ''' <param name="watcher">      The watcher. </param>
        ''' <param name="arguments">    The arguments. </param>
        ''' <param name="argumentType"> Type of the argument. </param>
        Public Sub New(ByVal watcher As FileSystemWatcher, ByVal arguments As Object, ByVal argumentType As ArgumentType)
            Me.Watcher = watcher
            Me.Arguments = arguments
            Me.ArgumentType = argumentType
            Me.Filter = NotifyFilters.Attributes
        End Sub

#End Region ' Constructors

        ''' <summary> Gets or sets the watcher. </summary>
        ''' <value> The watcher. </value>
        Public Property Watcher() As FileSystemWatcher

        ''' <summary> Gets or sets the arguments. </summary>
        ''' <value> The arguments. </value>
        Public Property Arguments() As Object

        ''' <summary> Gets or sets the type of the argument. </summary>
        ''' <value> The type of the argument. </value>
        Public Property ArgumentType() As ArgumentType

        ''' <summary> Gets or sets the filter. </summary>
        ''' <value> The filter. </value>
        Public Property Filter() As NotifyFilters

    End Class

    ''' <summary>
    ''' Enumerates argument types; used to point which argument is valid in the
    ''' <see cref="WatcherEventArgs">watcher event argument object</see>.
    ''' </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    Public Enum ArgumentType

        ''' <summary> An Enum constant representing the file system option. </summary>
        FileSystem

        ''' <summary> An Enum constant representing the renamed option. </summary>
        Renamed

        ''' <summary> An Enum constant representing the error option. </summary>
        [Error]

        ''' <summary> An Enum constant representing the standard event option. </summary>
        StandardEvent

        ''' <summary> An Enum constant representing the folder availability option. </summary>
        FolderAvailability
    End Enum

End Namespace
