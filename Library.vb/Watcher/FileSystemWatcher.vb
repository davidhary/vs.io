Imports System.Threading

Imports isr.IO.ExceptionExtensions
Namespace FileWatcher

    ''' <summary> Extended File system watcher. </summary>
    ''' <remarks>
    ''' (c) 2010 John Simmons.<para>
    ''' Licensed under The MIT License.</para><para>
    ''' David, 7/8/2014, . based on
    ''' http://www.codeproject.com/Articles/58740/FileSystemWatcher-Pure-Chaos-Part-of. </para>
    ''' </remarks>
    Public Class FileSystemWatcher
        Inherits System.IO.FileSystemWatcher

#Region " Constructors "

        ''' <summary> Default constructor. </summary>
        ''' <remarks> David, 10/8/2020. </remarks>
        Public Sub New()
            Me.New(DefaultInterval, DefaultName)
        End Sub

        ''' <summary> Constructor. </summary>
        ''' <remarks> David, 10/8/2020. </remarks>
        ''' <param name="path"> Full pathname of the file. </param>
        Public Sub New(ByVal path As String)
            Me.New(path, DefaultInterval, DefaultName)
        End Sub

        ''' <summary> Constructor. </summary>
        ''' <remarks> David, 10/8/2020. </remarks>
        ''' <param name="interval"> The folder monitoring interval. </param>
        Public Sub New(ByVal interval As TimeSpan)
            Me.New(interval, DefaultName)
        End Sub

        ''' <summary> Constructor. </summary>
        ''' <remarks> David, 10/8/2020. </remarks>
        ''' <param name="path">     Full pathname of the file. </param>
        ''' <param name="interval"> The folder monitoring interval. </param>
        Public Sub New(ByVal path As String, ByVal interval As TimeSpan)
            Me.New(path, interval, DefaultName)
        End Sub

        ''' <summary> Constructor. </summary>
        ''' <remarks> David, 10/8/2020. </remarks>
        ''' <param name="interval"> The folder monitoring interval. </param>
        ''' <param name="name">     The name. </param>
        Public Sub New(ByVal interval As TimeSpan, ByVal name As String)
            MyBase.New()
            Me._Interval = interval
            Me.Name = name
            Me.CreateThread()
        End Sub

        ''' <summary> Constructor. </summary>
        ''' <remarks> David, 10/8/2020. </remarks>
        ''' <param name="path">     Full pathname of the file. </param>
        ''' <param name="interval"> The folder monitoring interval. </param>
        ''' <param name="name">     The name. </param>
        Public Sub New(ByVal path As String, ByVal interval As TimeSpan, ByVal name As String)
            MyBase.New(path)
            Me._Interval = interval
            Me.Name = name
            Me.CreateThread()
        End Sub

        ''' <summary>
        ''' Gets or sets the dispose status sentinel of the base class.  This applies to the derived
        ''' class provided proper implementation.
        ''' </summary>
        ''' <value> <c>True</c> if disposed; otherwise, <c>False</c>. </value>
        Protected Property IsDisposed() As Boolean

        ''' <summary>
        ''' Releases the unmanaged resources used by the
        ''' <see cref="T:System.IO.FileSystemWatcher" /> and optionally releases the managed resources.
        ''' </summary>
        ''' <remarks> David, 10/8/2020. </remarks>
        ''' <param name="disposing"> true to release both managed and unmanaged resources; false to
        '''                          release only unmanaged resources. </param>
        <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
        Protected Overrides Sub Dispose(disposing As Boolean)
            Try
                If Not Me.IsDisposed AndAlso disposing Then
                    Me.RemoveFolderAvailabilityChangedEvent(Me.FolderAvailabilityChangedEvent)
                End If
            Finally
                ' set the sentinel indicating that the class was disposed.
                Me.IsDisposed = True
                MyBase.Dispose(disposing)
            End Try
        End Sub

#End Region

#Region " MONITOR FOLDER AVAILABILITY "

        ''' <summary> Start the monitoring thread. </summary>
        ''' <remarks> David, 10/8/2020. </remarks>
        Public Sub StartFolderMonitor()
            If Not Me.IsMonitoring Then
                Interlocked.Increment(Me._MonitoringSentinel)
                If Me._Thread IsNot Nothing Then
                    Me._Thread.Start()
                End If
            End If
        End Sub

        ''' <summary> Attempts to stop the monitoring thread. </summary>
        ''' <remarks> David, 10/8/2020. </remarks>
        Public Sub StopFolderMonitor()
            If Me.IsMonitoring Then
                Interlocked.Decrement(Me._MonitoringSentinel)
                If Not Me._Thread.Join(3000) Then
                    Try
                        Me._Thread.Abort()
                    Catch
                        Throw
                    End Try
                End If
                Me._Thread = Nothing
            End If
        End Sub

        ''' <summary> The name. </summary>
        ''' <value> The name. </value>
        Public Property Name As String

        ''' <summary> The default name. </summary>
        ''' <value> The default name. </value>
        Public Shared ReadOnly Property DefaultName As String = "FileSystemWatcher"

        ''' <summary> The default folder check interval time span. </summary>
        ''' <value> The default interval. </value>
        Public Shared ReadOnly Property DefaultInterval As TimeSpan = TimeSpan.FromMilliseconds(100)

        ''' <summary> The minimum folder check interval time span. </summary>
        ''' <value> The minimum interval. </value>
        Public Shared ReadOnly Property MinInterval As TimeSpan = TimeSpan.FromMilliseconds(10)

        ''' <summary> The maximum folder check interval time span. </summary>
        ''' <value> The maximum interval. </value>
        Public Shared ReadOnly Property MaxInterval As TimeSpan = TimeSpan.FromDays(1) - TimeSpan.FromMilliseconds(1)

        ''' <summary> True if is folder exists, false if not. </summary>
        Private _IsFolderExists As Boolean = True

        ''' <summary> <c>true</c> if the <see cref="path">path</see> exists. </summary>
        ''' <value> The is folder exists. </value>
        Public Property IsFolderExists As Boolean
            Get
                Return Me._IsFolderExists
            End Get
            Set(value As Boolean)
                If Not value.Equals(Me.IsFolderExists) Then
                    Me._IsFolderExists = value
                    Dim evt As EventHandler(Of System.EventArgs) = Me.FolderAvailabilityChangedEvent
                    evt?.Invoke(Me, New System.EventArgs)
                End If
            End Set
        End Property

        ''' <summary> The interval. </summary>
        Private _Interval As TimeSpan

        ''' <summary> Gets the monitoring interval. </summary>
        ''' <value> The interval. </value>
        Public ReadOnly Property Interval As TimeSpan
            Get
                Return Me._Interval
            End Get
        End Property


        ''' <summary> The thread. </summary>
        Private _Thread As Thread

        ''' <summary> Creates the thread if the interval is greater than 0 milliseconds. </summary>
        ''' <remarks> David, 10/8/2020. </remarks>
        Private Sub CreateThread()

            ' Normalize  the interval
            Me._Interval = TimeSpan.FromMilliseconds(Math.Max(MinInterval.TotalMilliseconds,
                                                              Math.Min(Me._Interval.TotalMilliseconds, MaxInterval.TotalMilliseconds)))
            ' If the interval is 0, this indicates we don't want to monitor the path 
            ' for availability.
            If Me._Interval > MinInterval Then
                Me._Thread = New Thread(New ThreadStart(AddressOf Me.MonitorFolderAvailability)) With {
                    .Name = Me.Name,
                    .IsBackground = True
                }
            End If

        End Sub

        ''' <summary> The monitoring sentinel. </summary>
        Private _MonitoringSentinel As Long

        ''' <summary> Reads the monitoring sentinel to determine if monitoring is active. </summary>
        ''' <value> The is running. </value>
        Public ReadOnly Property IsMonitoring As Boolean
            Get
                Return Interlocked.Read(Me._MonitoringSentinel) > 0
            End Get
        End Property

        ''' <summary> The thread method. It sits and spins making sure the folder exists. </summary>
        ''' <remarks> David, 10/8/2020. </remarks>
        Public Sub MonitorFolderAvailability()
            Do While Me.IsMonitoring
                Me.IsFolderExists = System.IO.Directory.Exists(Me.Path)
                Thread.Sleep(Me.Interval)
            Loop
        End Sub

        ''' <summary> Event queue for all listeners interested in FolderAvailabilityChanged events. </summary>
        Public Event FolderAvailabilityChanged As EventHandler(Of System.EventArgs)

        ''' <summary> Removes folder availability changed event. </summary>
        ''' <remarks> David, 10/8/2020. </remarks>
        ''' <param name="value"> The value. </param>
        <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
        Private Sub RemoveFolderAvailabilityChangedEvent(ByVal value As EventHandler(Of System.EventArgs))
            For Each d As [Delegate] In If(value Is Nothing, Array.Empty(Of [Delegate])(), value.GetInvocationList)
                Try
                    RemoveHandler Me.FolderAvailabilityChanged, CType(d, EventHandler(Of System.EventArgs))
                Catch ex As Exception
                    Debug.Assert(Not Debugger.IsAttached, ex.ToFullBlownString)
                End Try
            Next
        End Sub

#End Region

    End Class

End Namespace


