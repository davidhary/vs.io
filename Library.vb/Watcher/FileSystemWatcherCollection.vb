﻿Namespace FileWatcher

    ''' <summary> The list of <see cref="FileSystemWatcher">file system watchers</see>. </summary>
    ''' <remarks>
    ''' (c) 2010 John Simmons.<para>
    ''' Licensed under The MIT License.</para><para>
    ''' David, 7/8/2014. Created based on
    ''' http://www.codeproject.com/Articles/58740/FileSystemWatcher-Pure-Chaos-Part-of. </para>
    ''' </remarks>
    Public Class FileSystemWatcherCollection
        Inherits List(Of FileWatcher.FileSystemWatcher)

        ''' <summary> Enables the raising events. </summary>
        ''' <remarks> David, 10/8/2020. </remarks>
        Public Sub EnableRaisingEvents()
            For Each Item As FileSystemWatcher In Me
                Item.EnableRaisingEvents = True
            Next
        End Sub

        ''' <summary> Disables the raising events. </summary>
        ''' <remarks> David, 10/8/2020. </remarks>
        Public Sub DisableRaisingEvents()
            For Each Item As FileSystemWatcher In Me
                Item.EnableRaisingEvents = False
            Next
        End Sub

        ''' <summary>
        ''' Starts all of the internal <see cref="FileSystemWatcher">file system watchers</see>
        ''' by setting their EnableRaisingEvents property to true.
        ''' </summary>
        ''' <remarks> David, 10/8/2020. </remarks>
        Public Sub Start()
            If Me.Any Then
                Me.Item(0).StartFolderMonitor()
            End If
            Me.EnableRaisingEvents()
        End Sub

        ''' <summary> Stops all of the internal <see cref="FileSystemWatcher">file system watchers</see> by
        ''' setting their EnableRaisingEvents property to true. </summary>
        Public Sub [Stop]()
            If Me.Any Then
                Me.Item(0).StopFolderMonitor()
            End If
            Me.DisableRaisingEvents()
        End Sub

    End Class

End Namespace

