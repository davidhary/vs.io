Imports System.IO

Imports isr.IO.ExceptionExtensions
Namespace FileWatcher

    ''' <summary>
    ''' This is the main class (and the one you'll use directly). Create an instance of the class
    ''' (passing in a WatcherInfo object for initialization), and then attach event handlers to this
    ''' object.  One or more watchers will be created to handle the various events and filters, and
    ''' will marshal these events into a single set from which you can gather info.
    ''' </summary>
    ''' <remarks>
    ''' (c) 2010 John Simmons.<para>
    ''' Licensed under The MIT License.</para><para>
    ''' David, 7/8/2014. Created based on
    ''' http://www.codeproject.com/Articles/58740/FileSystemWatcher-Pure-Chaos-Part-of. </para><para>
    ''' David, 8/28/2015. Uses null-conditionals. </para>
    ''' </remarks>
    Public Class Watcher
        Implements IDisposable

#Region " Constructors "

        ''' <summary> Constructor. </summary>
        ''' <remarks> David, 10/8/2020. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="info"> The information. </param>
        Public Sub New(ByVal info As WatcherInfo)
            If info Is Nothing Then Throw New ArgumentNullException(NameOf(info))
            Me._WatcherInfo = info
            Me.Initialize()
        End Sub

        ''' <summary> Calls <see cref="M:Dispose(Boolean Disposing)" /> to cleanup. </summary>
        ''' <remarks>
        ''' Do not make this method Overridable (virtual) because a derived class should not be able to
        ''' override this method.
        ''' </remarks>
        Public Sub Dispose() Implements IDisposable.Dispose

            ' this disposes all child classes.
            Me.Dispose(True)

            ' Take this object off the finalization(Queue) and prevent finalization code 
            ' from executing a second time.
            GC.SuppressFinalize(Me)

        End Sub

        ''' <summary>
        ''' Gets or sets the dispose status sentinel of the base class.  This applies to the derived
        ''' class provided proper implementation.
        ''' </summary>
        ''' <value> <c>True</c> if disposed; otherwise, <c>False</c>. </value>
        Protected Property IsDisposed() As Boolean

        ''' <summary>
        ''' Releases the unmanaged resources used by the <see cref="T:System.Windows.Forms.Control" />
        ''' and its child controls and optionally releases the managed resources.
        ''' </summary>
        ''' <remarks> David, 10/8/2020. </remarks>
        ''' <param name="disposing"> <c>True</c> to release both managed and unmanaged resources;
        '''                                                   <c>False</c> to release only unmanaged
        '''                                                   resources when called from the runtime
        '''                                                   finalize. </param>
        <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
        <System.Diagnostics.DebuggerNonUserCode()>
        Protected Overridable Sub Dispose(disposing As Boolean)
            Try
                If Not Me.IsDisposed AndAlso disposing Then
                    Me.DisposeWatchers()

                    Dim d As [Delegate]
                    Dim ex As Exception

                    For Each d In If(Me.ChangedAttributeEvent Is Nothing, Array.Empty(Of [Delegate])(), Me.ChangedAttributeEvent.GetInvocationList)
                        Try
                            RemoveHandler Me.ChangedAttribute, CType(d, EventHandler(Of WatcherEventArgs))
                        Catch ex
                            Debug.Assert(Not Debugger.IsAttached, ex.ToFullBlownString)
                        End Try
                    Next

                    For Each d In If(Me.ChangedCreationTimeEvent Is Nothing, Array.Empty(Of [Delegate])(), Me.ChangedCreationTimeEvent.GetInvocationList)
                        Try
                            RemoveHandler Me.ChangedCreationTime, CType(d, EventHandler(Of WatcherEventArgs))
                        Catch ex
                            Debug.Assert(Not Debugger.IsAttached, ex.ToFullBlownString)
                        End Try
                    Next

                    For Each d In If(Me.ChangedDirectoryNameEvent Is Nothing, Array.Empty(Of [Delegate])(), Me.ChangedDirectoryNameEvent.GetInvocationList)
                        Try
                            RemoveHandler Me.ChangedDirectoryName, CType(d, EventHandler(Of WatcherEventArgs))
                        Catch ex
                            Debug.Assert(Not Debugger.IsAttached, ex.ToFullBlownString)
                        End Try
                    Next

                    For Each d In If(Me.ChangedFileNameEvent Is Nothing, Array.Empty(Of [Delegate])(), Me.ChangedFileNameEvent.GetInvocationList)
                        Try
                            RemoveHandler Me.ChangedFileName, CType(d, EventHandler(Of WatcherEventArgs))
                        Catch ex
                            Debug.Assert(Not Debugger.IsAttached, ex.ToFullBlownString)
                        End Try
                    Next

                    For Each d In If(Me.ChangedLastAccessEvent Is Nothing, Array.Empty(Of [Delegate])(), Me.ChangedLastAccessEvent.GetInvocationList)
                        Try
                            RemoveHandler Me.ChangedLastAccess, CType(d, EventHandler(Of WatcherEventArgs))
                        Catch ex
                            Debug.Assert(Not Debugger.IsAttached, ex.ToFullBlownString)
                        End Try
                    Next

                    For Each d In If(Me.ChangedLastWriteEvent Is Nothing, Array.Empty(Of [Delegate])(), Me.ChangedLastWriteEvent.GetInvocationList)
                        Try
                            RemoveHandler Me.ChangedLastWrite, CType(d, EventHandler(Of WatcherEventArgs))
                        Catch ex
                            Debug.Assert(Not Debugger.IsAttached, ex.ToFullBlownString)
                        End Try
                    Next

                    For Each d In If(Me.ChangedSecurityEvent Is Nothing, Array.Empty(Of [Delegate])(), Me.ChangedSecurityEvent.GetInvocationList)
                        Try
                            RemoveHandler Me.ChangedSecurity, CType(d, EventHandler(Of WatcherEventArgs))
                        Catch ex
                            Debug.Assert(Not Debugger.IsAttached, ex.ToFullBlownString)
                        End Try
                    Next

                    For Each d In If(Me.ChangedSizeEvent Is Nothing, Array.Empty(Of [Delegate])(), Me.ChangedSizeEvent.GetInvocationList)
                        Try
                            RemoveHandler Me.ChangedSize, CType(d, EventHandler(Of WatcherEventArgs))
                        Catch ex
                            Debug.Assert(Not Debugger.IsAttached, ex.ToFullBlownString)
                        End Try
                    Next

                    For Each d In If(Me.CreatedEvent Is Nothing, Array.Empty(Of [Delegate])(), Me.CreatedEvent.GetInvocationList)
                        Try
                            RemoveHandler Me.Created, CType(d, EventHandler(Of WatcherEventArgs))
                        Catch ex
                            Debug.Assert(Not Debugger.IsAttached, ex.ToFullBlownString)
                        End Try
                    Next

                    For Each d In If(Me.DeletedEvent Is Nothing, Array.Empty(Of [Delegate])(), Me.DeletedEvent.GetInvocationList)
                        Try
                            RemoveHandler Me.Deleted, CType(d, EventHandler(Of WatcherEventArgs))
                        Catch ex
                            Debug.Assert(Not Debugger.IsAttached, ex.ToFullBlownString)
                        End Try
                    Next

                    For Each d In If(Me.RenamedEvent Is Nothing, Array.Empty(Of [Delegate])(), Me.RenamedEvent.GetInvocationList)
                        Try
                            RemoveHandler Me.Renamed, CType(d, EventHandler(Of WatcherEventArgs))
                        Catch ex
                            Debug.Assert(Not Debugger.IsAttached, ex.ToFullBlownString)
                        End Try
                    Next

                    For Each d In If(Me.ChangedAttributeEvent Is Nothing, Array.Empty(Of [Delegate])(), Me.ChangedAttributeEvent.GetInvocationList)
                        Try
                            RemoveHandler Me.ErrorOccurred, CType(d, EventHandler(Of WatcherEventArgs))
                        Catch ex
                            Debug.Assert(Not Debugger.IsAttached, ex.ToFullBlownString)
                        End Try
                    Next

                    For Each d In If(Me.DisposedEvent Is Nothing, Array.Empty(Of [Delegate])(), Me.DisposedEvent.GetInvocationList)
                        Try
                            RemoveHandler Me.Disposed, CType(d, EventHandler(Of WatcherEventArgs))
                        Catch ex
                            Debug.Assert(Not Debugger.IsAttached, ex.ToFullBlownString)
                        End Try
                    Next

                    For Each d In If(Me.ConnectionChangedEvent Is Nothing, Array.Empty(Of [Delegate])(), Me.ConnectionChangedEvent.GetInvocationList)
                        Try
                            RemoveHandler Me.ConnectionChanged, CType(d, EventHandler(Of WatcherEventArgs))
                        Catch ex
                            Debug.Assert(Not Debugger.IsAttached, ex.ToFullBlownString)
                        End Try
                    Next

                End If
            Finally
                Me.IsDisposed = True
            End Try
        End Sub

        ''' <summary>
        ''' Disposes of all of our watchers (called from Dispose, or as a result of loosing access to a
        ''' folder)
        ''' </summary>
        ''' <remarks> David, 10/8/2020. </remarks>
        Public Sub DisposeWatchers()
            ' turn watches off
            If Me._FileSystemWatchers IsNot Nothing Then
                Me._FileSystemWatchers.DisableRaisingEvents()
                Debug.WriteLine("Watcher.DisposeWatchers()")
                For i As Integer = 0 To Me._FileSystemWatchers.Count - 1
                    If i = 0 Then
                        Me.RemoveMainWatcher(Me._FileSystemWatchers(i))
                    Else
                        Me.RemoveWatcher(Me._FileSystemWatchers(i))
                    End If
                    Me._FileSystemWatchers(i).Dispose()
                Next i
                Me._FileSystemWatchers.Clear()
            End If
        End Sub

#End Region

#Region " Event Definitions "

        ''' <summary> Event queue for all listeners interested in EventChangedAttribute events. </summary>
        Public Event ChangedAttribute As EventHandler(Of WatcherEventArgs)

        ''' <summary> Event queue for all listeners interested in EventChangedCreationTime events. </summary>
        Public Event ChangedCreationTime As EventHandler(Of WatcherEventArgs)

        ''' <summary> Event queue for all listeners interested in EventChangedDirectoryName events. </summary>
        Public Event ChangedDirectoryName As EventHandler(Of WatcherEventArgs)

        ''' <summary> Event queue for all listeners interested in EventChangedFileName events. </summary>
        Public Event ChangedFileName As EventHandler(Of WatcherEventArgs)

        ''' <summary> Event queue for all listeners interested in EventChangedLastAccess events. </summary>
        Public Event ChangedLastAccess As EventHandler(Of WatcherEventArgs)

        ''' <summary> Event queue for all listeners interested in EventChangedLastWrite events. </summary>
        Public Event ChangedLastWrite As EventHandler(Of WatcherEventArgs)

        ''' <summary> Event queue for all listeners interested in EventChangedSecurity events. </summary>
        Public Event ChangedSecurity As EventHandler(Of WatcherEventArgs)

        ''' <summary> Event queue for all listeners interested in EventChangedSize events. </summary>
        Public Event ChangedSize As EventHandler(Of WatcherEventArgs)

        ''' <summary> Event queue for all listeners interested in EventCreated events. </summary>
        Public Event Created As EventHandler(Of WatcherEventArgs)

        ''' <summary> Event queue for all listeners interested in EventDeleted events. </summary>
        Public Event Deleted As EventHandler(Of WatcherEventArgs)

        ''' <summary> Event queue for all listeners interested in EventRenamed events. </summary>
        Public Event Renamed As EventHandler(Of WatcherEventArgs)

        ''' <summary> Event queue for all listeners interested in EventError events. </summary>
        Public Event ErrorOccurred As EventHandler(Of WatcherEventArgs)

        ''' <summary> Event queue for all listeners interested in EventDisposed events. </summary>
        Public Event Disposed As EventHandler(Of WatcherEventArgs)

        ''' <summary> Event queue for all listeners interested in connection changed events. </summary>
        Public Event ConnectionChanged As EventHandler(Of WatcherEventArgs)

#End Region

#Region " Helper Methods "

        ''' <summary> Information describing the watcher. </summary>
        Private ReadOnly _WatcherInfo As WatcherInfo

        ''' <summary> The list of <see cref="FileSystemWatcher">file system watchers</see>. </summary>
        Private _FileSystemWatchers As FileSystemWatcherCollection

        ''' <summary> <c>true</c> if the <see cref="path">path</see> exists. </summary>
        ''' <value> The is connected. </value>
        Public ReadOnly Property IsConnected As Boolean = True

        ''' <summary>
        ''' Determines if the specified NotifyFilter item has been specified to be handled by this object.
        ''' </summary>
        ''' <remarks> David, 10/8/2020. </remarks>
        ''' <param name="filter"> . </param>
        ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c>. </returns>
        Public Function HandleNotifyFilter(ByVal filter As NotifyFilters) As Boolean
            Return ((CType(Me._WatcherInfo.ChangesFilters And filter, NotifyFilters)) = filter)
        End Function

        ''' <summary>
        ''' Determines if the specified WatcherChangeType item has been specified to be handled by this
        ''' object.
        ''' </summary>
        ''' <remarks> David, 10/8/2020. </remarks>
        ''' <param name="filter"> . </param>
        ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c>. </returns>
        Public Function HandleWatchesFilter(ByVal filter As WatcherChangeTypes) As Boolean
            Return ((CType(Me._WatcherInfo.WatchesFilters And filter, WatcherChangeTypes)) = filter)
        End Function

        ''' <summary>
        ''' Initializes this object by creating all of the required internal FileSystemWatcher objects
        ''' necessary to monitor the folder/file for the desired changes.
        ''' </summary>
        ''' <remarks> David, 10/8/2020. </remarks>
        Private Sub Initialize()

            Debug.WriteLine("Watcher.Initialize()")

            ' the buffer can be from 4 to 64 k bytes.  Default is 8
            Me._WatcherInfo.BufferSize = Me._WatcherInfo.BufferSize

            Me._FileSystemWatchers = New FileSystemWatcherCollection

            ' create the main watcher (handles create/delete, rename, error, and dispose)
            Me.AddWatcher()

            ' create a change watcher for each NotifyFilter item
            Me.AddWatcher(NotifyFilters.Attributes)
            Me.AddWatcher(NotifyFilters.CreationTime)
            Me.AddWatcher(NotifyFilters.DirectoryName)
            Me.AddWatcher(NotifyFilters.FileName)
            Me.AddWatcher(NotifyFilters.LastAccess)
            Me.AddWatcher(NotifyFilters.LastWrite)
            Me.AddWatcher(NotifyFilters.Security)
            Me.AddWatcher(NotifyFilters.Size)

            Debug.WriteLine(String.Format("Watcher.Initialize() - {0} watchers created", Me._FileSystemWatchers.Count))
        End Sub

        ''' <summary>
        ''' Adds the necessary FileSystemWatcher objects, depending on which notify filters the user
        ''' specified.
        ''' </summary>
        ''' <remarks> David, 10/8/2020. </remarks>
        ''' <param name="filter"> . </param>
        Private Sub AddWatcher(ByVal filter As NotifyFilters)
            Debug.WriteLine(String.Format("Watcher.AddWatcher({0})", filter.ToString()))

            ' Each "Change" filter gets its own watcher so we can determine *what* 
            ' actually changed. This will allow us to react only to the change events 
            ' that we actually want.  The reason I do this is because some programs 
            ' fire TWO events for  certain changes. For example, Notepad sends two 
            ' events when a file is created. One for CreationTime, and one for 
            ' Attributes.
            ' if we're not handling the currently specified filter, get out
            If Me.HandleNotifyFilter(filter) Then
                Dim fileSystemWatcher As FileSystemWatcher = New FileSystemWatcher(Me._WatcherInfo.WatchPath) With {
                    .IncludeSubdirectories = Me._WatcherInfo.IncludeSubfolders,
                    .Filter = Me._WatcherInfo.FileFilter,
                    .NotifyFilter = filter,
                    .InternalBufferSize = Me._WatcherInfo.BufferSize
                }

                Select Case filter
                    Case NotifyFilters.Attributes
                        AddHandler fileSystemWatcher.Changed, AddressOf Me.FileSystemWatcher_ChangedAttribute
                    Case NotifyFilters.CreationTime
                        AddHandler fileSystemWatcher.Changed, AddressOf Me.FileSystemWatcher_ChangedCreationTime
                    Case NotifyFilters.DirectoryName
                        AddHandler fileSystemWatcher.Changed, AddressOf Me.FileSystemWatcher_ChangedDirectoryName
                    Case NotifyFilters.FileName
                        AddHandler fileSystemWatcher.Changed, AddressOf Me.FileSystemWatcher_ChangedFileName
                    Case NotifyFilters.LastAccess
                        AddHandler fileSystemWatcher.Changed, AddressOf Me.FileSystemWatcher_ChangedLastAccess
                    Case NotifyFilters.LastWrite
                        AddHandler fileSystemWatcher.Changed, AddressOf Me.FileSystemWatcher_ChangedLastWrite
                    Case NotifyFilters.Security
                        AddHandler fileSystemWatcher.Changed, AddressOf Me.FileSystemWatcher_ChangedSecurity
                    Case NotifyFilters.Size
                        AddHandler fileSystemWatcher.Changed, AddressOf Me.FileSystemWatcher_ChangedSize
                End Select
                Me._FileSystemWatchers.Add(fileSystemWatcher)
            End If
        End Sub

        ''' <summary>
        ''' Removes the FileSystemWatcher event handlers depending on which notify filters the user
        ''' specified.
        ''' </summary>
        ''' <remarks> David, 10/8/2020. </remarks>
        ''' <param name="fileSystemWatcher"> The file system watcher. </param>
        Private Sub RemoveWatcher(ByVal fileSystemWatcher As FileSystemWatcher)

            If fileSystemWatcher IsNot Nothing AndAlso Me.HandleNotifyFilter(fileSystemWatcher.NotifyFilter) Then
                Debug.WriteLine(String.Format("Watcher.RemoveWatcher({0})", fileSystemWatcher.Name))
                Select Case fileSystemWatcher.NotifyFilter
                    Case NotifyFilters.Attributes
                        RemoveHandler fileSystemWatcher.Changed, AddressOf Me.FileSystemWatcher_ChangedAttribute
                    Case NotifyFilters.CreationTime
                        RemoveHandler fileSystemWatcher.Changed, AddressOf Me.FileSystemWatcher_ChangedCreationTime
                    Case NotifyFilters.DirectoryName
                        RemoveHandler fileSystemWatcher.Changed, AddressOf Me.FileSystemWatcher_ChangedDirectoryName
                    Case NotifyFilters.FileName
                        RemoveHandler fileSystemWatcher.Changed, AddressOf Me.FileSystemWatcher_ChangedFileName
                    Case NotifyFilters.LastAccess
                        RemoveHandler fileSystemWatcher.Changed, AddressOf Me.FileSystemWatcher_ChangedLastAccess
                    Case NotifyFilters.LastWrite
                        RemoveHandler fileSystemWatcher.Changed, AddressOf Me.FileSystemWatcher_ChangedLastWrite
                    Case NotifyFilters.Security
                        RemoveHandler fileSystemWatcher.Changed, AddressOf Me.FileSystemWatcher_ChangedSecurity
                    Case NotifyFilters.Size
                        RemoveHandler fileSystemWatcher.Changed, AddressOf Me.FileSystemWatcher_ChangedSize
                End Select
            End If
        End Sub

        ''' <summary> Adds the main watcher for the change types the user specified. </summary>
        ''' <remarks> David, 10/8/2020. </remarks>
        Private Sub AddWatcher()
            Debug.WriteLine("Watcher.AddWatcher()")

            ' All other FileSystemWatcher events are handled through a single "main" watcher.
            If Me.HandleWatchesFilter(WatcherChangeTypes.Created) OrElse
                Me.HandleWatchesFilter(WatcherChangeTypes.Deleted) OrElse
                Me.HandleWatchesFilter(WatcherChangeTypes.Renamed) OrElse
                Me._WatcherInfo.WatchForError OrElse Me._WatcherInfo.WatchForDisposed Then

                Dim fileSystemWatcher As FileSystemWatcher = New FileSystemWatcher(Me._WatcherInfo.WatchPath, Me._WatcherInfo.MonitorPathInterval) With {
                    .IncludeSubdirectories = Me._WatcherInfo.IncludeSubfolders,
                    .Filter = Me._WatcherInfo.FileFilter,
                    .InternalBufferSize = Me._WatcherInfo.BufferSize
                }

                If Me.HandleWatchesFilter(WatcherChangeTypes.Created) Then
                    AddHandler fileSystemWatcher.Created, AddressOf Me.FileSystemWatcher_CreatedDeleted
                End If
                If Me.HandleWatchesFilter(WatcherChangeTypes.Deleted) Then
                    AddHandler fileSystemWatcher.Deleted, AddressOf Me.FileSystemWatcher_CreatedDeleted
                End If
                If Me.HandleWatchesFilter(WatcherChangeTypes.Renamed) Then
                    AddHandler fileSystemWatcher.Renamed, AddressOf Me.FileSystemWatcher_Renamed
                End If
                If Me._WatcherInfo.MonitorPathInterval > TimeSpan.Zero Then
                    AddHandler fileSystemWatcher.FolderAvailabilityChanged, AddressOf Me.FileSystemWatcher_FolderAvailabilityChanged
                End If

                If Me._WatcherInfo.WatchForError Then
                    AddHandler fileSystemWatcher.Error, AddressOf Me.FileSystemWatcher_Error
                End If
                If Me._WatcherInfo.WatchForDisposed Then
                    AddHandler fileSystemWatcher.Disposed, AddressOf Me.FileSystemWatcher_Disposed
                End If
                Me._FileSystemWatchers.Add(fileSystemWatcher)
            End If

        End Sub

        ''' <summary> Adds the watcher for the change types the user specified. </summary>
        ''' <remarks> David, 10/8/2020. </remarks>
        ''' <param name="fileSystemWatcher"> The file system watcher. </param>
        Private Sub RemoveMainWatcher(fileSystemWatcher As FileSystemWatcher)

            If fileSystemWatcher IsNot Nothing Then
                Debug.WriteLine(String.Format("Watcher.RemoveMainWatcher({0})", fileSystemWatcher.Name))

                If Me.HandleWatchesFilter(WatcherChangeTypes.Created) Then
                    RemoveHandler fileSystemWatcher.Created, AddressOf Me.FileSystemWatcher_CreatedDeleted
                End If
                If Me.HandleWatchesFilter(WatcherChangeTypes.Deleted) Then
                    RemoveHandler fileSystemWatcher.Deleted, AddressOf Me.FileSystemWatcher_CreatedDeleted
                End If
                If Me.HandleWatchesFilter(WatcherChangeTypes.Renamed) Then
                    RemoveHandler fileSystemWatcher.Renamed, AddressOf Me.FileSystemWatcher_Renamed
                End If
                If Me._WatcherInfo.MonitorPathInterval > TimeSpan.Zero Then
                    RemoveHandler fileSystemWatcher.FolderAvailabilityChanged, AddressOf Me.FileSystemWatcher_FolderAvailabilityChanged
                End If

                If Me._WatcherInfo.WatchForError Then
                    RemoveHandler fileSystemWatcher.Error, AddressOf Me.FileSystemWatcher_Error
                End If
                If Me._WatcherInfo.WatchForDisposed Then
                    RemoveHandler fileSystemWatcher.Disposed, AddressOf Me.FileSystemWatcher_Disposed
                End If
            End If

        End Sub

        ''' <summary>
        ''' Starts all of the internal FileSystemWatcher objects by setting their EnableRaisingEvents
        ''' property to true.
        ''' </summary>
        ''' <remarks> David, 10/8/2020. </remarks>
        Public Sub Start()
            Debug.WriteLine("Watcher.Start()")
            If Me._FileSystemWatchers IsNot Nothing Then
                Me._FileSystemWatchers.Start()
            End If
        End Sub

        ''' <summary> Stops all of the internal FileSystemWatcher objects by setting their
        ''' EnableRaisingEvents property to true. </summary>
        Public Sub [Stop]()
            Debug.WriteLine("Watcher.Stop()")
            If Me._FileSystemWatchers IsNot Nothing Then
                Me._FileSystemWatchers.Stop()
            End If
        End Sub

#End Region ' Helper Methods

#Region " Native Watcher Events "

        ''' <summary>
        ''' Fired when the watcher responsible for monitoring attribute changes is triggered.
        ''' </summary>
        ''' <remarks> David, 10/8/2020. </remarks>
        ''' <param name="sender"> Source of the event. </param>
        ''' <param name="e">      File system event information. </param>
        Private Sub FileSystemWatcher_ChangedAttribute(ByVal sender As Object, ByVal e As FileSystemEventArgs)
            Dim fileSystemWatcher As FileSystemWatcher = CType(sender, FileSystemWatcher)
            If fileSystemWatcher IsNot Nothing AndAlso e IsNot Nothing Then
                Debug.WriteLine("EVENT - Changed Attribute")
                Dim evt As EventHandler(Of WatcherEventArgs) = Me.ChangedAttributeEvent
                evt?.Invoke(Me, New WatcherEventArgs(fileSystemWatcher, e, ArgumentType.FileSystem, NotifyFilters.Attributes))
            End If
        End Sub

        ''' <summary>
        ''' Fired when the watcher responsible for monitoring creation time changes is triggered.
        ''' </summary>
        ''' <remarks> David, 10/8/2020. </remarks>
        ''' <param name="sender"> Source of the event. </param>
        ''' <param name="e">      File system event information. </param>
        Private Sub FileSystemWatcher_ChangedCreationTime(ByVal sender As Object, ByVal e As FileSystemEventArgs)
            Dim fileSystemWatcher As FileSystemWatcher = CType(sender, FileSystemWatcher)
            If fileSystemWatcher IsNot Nothing AndAlso e IsNot Nothing Then
                Debug.WriteLine("EVENT - Changed CreationTime")
                Dim evt As EventHandler(Of WatcherEventArgs) = Me.ChangedCreationTimeEvent
                evt?.Invoke(Me, New WatcherEventArgs(fileSystemWatcher, e, ArgumentType.FileSystem, NotifyFilters.CreationTime))
            End If
        End Sub

        ''' <summary>
        ''' Fired when the watcher responsible for monitoring directory name changes is triggered.
        ''' </summary>
        ''' <remarks> David, 10/8/2020. </remarks>
        ''' <param name="sender"> Source of the event. </param>
        ''' <param name="e">      File system event information. </param>
        Private Sub FileSystemWatcher_ChangedDirectoryName(ByVal sender As Object, ByVal e As FileSystemEventArgs)
            Dim fileSystemWatcher As FileSystemWatcher = CType(sender, FileSystemWatcher)
            If fileSystemWatcher IsNot Nothing AndAlso e IsNot Nothing Then
                Debug.WriteLine("EVENT - Changed DirectoryName")
                Dim evt As EventHandler(Of WatcherEventArgs) = Me.ChangedDirectoryNameEvent
                evt?.Invoke(Me, New WatcherEventArgs(fileSystemWatcher, e, ArgumentType.FileSystem, NotifyFilters.DirectoryName))
            End If
        End Sub

        ''' <summary>
        ''' Fired when the watcher responsible for monitoring file name changes is triggered.
        ''' </summary>
        ''' <remarks> David, 10/8/2020. </remarks>
        ''' <param name="sender"> Source of the event. </param>
        ''' <param name="e">      File system event information. </param>
        Private Sub FileSystemWatcher_ChangedFileName(ByVal sender As Object, ByVal e As FileSystemEventArgs)
            Dim fileSystemWatcher As FileSystemWatcher = CType(sender, FileSystemWatcher)
            If fileSystemWatcher IsNot Nothing AndAlso e IsNot Nothing Then
                Debug.WriteLine("EVENT - Changed FileName")
                Dim evt As EventHandler(Of WatcherEventArgs) = Me.ChangedFileNameEvent
                evt?.Invoke(Me, New WatcherEventArgs(fileSystemWatcher, e, ArgumentType.FileSystem, NotifyFilters.FileName))
            End If
        End Sub

        ''' <summary>
        ''' Fired when the watcher responsible for monitoring last access date/time changes is triggered.
        ''' </summary>
        ''' <remarks> David, 10/8/2020. </remarks>
        ''' <param name="sender"> Source of the event. </param>
        ''' <param name="e">      File system event information. </param>
        Private Sub FileSystemWatcher_ChangedLastAccess(ByVal sender As Object, ByVal e As FileSystemEventArgs)
            Dim fileSystemWatcher As FileSystemWatcher = CType(sender, FileSystemWatcher)
            If fileSystemWatcher IsNot Nothing AndAlso e IsNot Nothing Then
                Debug.WriteLine("EVENT - Changed LastAccess")
                Dim evt As EventHandler(Of WatcherEventArgs) = Me.ChangedLastAccessEvent
                evt?.Invoke(Me, New WatcherEventArgs(fileSystemWatcher, e, ArgumentType.FileSystem, NotifyFilters.LastAccess))
            End If
        End Sub

        ''' <summary>
        ''' Fired when the watcher responsible for monitoring last write date/time changes is triggered.
        ''' </summary>
        ''' <remarks> David, 10/8/2020. </remarks>
        ''' <param name="sender"> Source of the event. </param>
        ''' <param name="e">      File system event information. </param>
        Private Sub FileSystemWatcher_ChangedLastWrite(ByVal sender As Object, ByVal e As FileSystemEventArgs)
            Dim fileSystemWatcher As FileSystemWatcher = CType(sender, FileSystemWatcher)
            If fileSystemWatcher IsNot Nothing AndAlso e IsNot Nothing Then
                Debug.WriteLine("EVENT - Changed LastWrite")
                Dim evt As EventHandler(Of WatcherEventArgs) = Me.ChangedLastWriteEvent
                evt?.Invoke(Me, New WatcherEventArgs(fileSystemWatcher, e, ArgumentType.FileSystem, NotifyFilters.LastWrite))
            End If
        End Sub

        ''' <summary>
        ''' Fired when the watcher responsible for monitoring security changes is triggered.
        ''' </summary>
        ''' <remarks> David, 10/8/2020. </remarks>
        ''' <param name="sender"> Source of the event. </param>
        ''' <param name="e">      File system event information. </param>
        Private Sub FileSystemWatcher_ChangedSecurity(ByVal sender As Object, ByVal e As FileSystemEventArgs)
            Dim fileSystemWatcher As FileSystemWatcher = CType(sender, FileSystemWatcher)
            If fileSystemWatcher IsNot Nothing AndAlso e IsNot Nothing Then
                Debug.WriteLine("EVENT - Changed Security")
                Dim evt As EventHandler(Of WatcherEventArgs) = Me.ChangedSecurityEvent
                evt?.Invoke(Me, New WatcherEventArgs(fileSystemWatcher, e, ArgumentType.FileSystem, NotifyFilters.Security))
            End If
        End Sub

        ''' <summary>
        ''' Fired when the watcher responsible for monitoring size changes is triggered.
        ''' </summary>
        ''' <remarks> David, 10/8/2020. </remarks>
        ''' <param name="sender"> Source of the event. </param>
        ''' <param name="e">      File system event information. </param>
        Private Sub FileSystemWatcher_ChangedSize(ByVal sender As Object, ByVal e As FileSystemEventArgs)
            Dim fileSystemWatcher As FileSystemWatcher = CType(sender, FileSystemWatcher)
            If fileSystemWatcher IsNot Nothing AndAlso e IsNot Nothing Then
                Debug.WriteLine("EVENT - Changed Size")
                Dim evt As EventHandler(Of WatcherEventArgs) = Me.ChangedSizeEvent
                evt?.Invoke(Me, New WatcherEventArgs(fileSystemWatcher, e, ArgumentType.FileSystem, NotifyFilters.Size))
            End If
        End Sub

        ''' <summary> Fired when an internal watcher is disposed. </summary>
        ''' <remarks> David, 10/8/2020. </remarks>
        ''' <param name="sender"> Source of the event. </param>
        ''' <param name="e">      Event information. </param>
        Private Sub FileSystemWatcher_Disposed(ByVal sender As Object, ByVal e As EventArgs)
            Dim fileSystemWatcher As FileSystemWatcher = CType(sender, FileSystemWatcher)
            If fileSystemWatcher IsNot Nothing AndAlso e IsNot Nothing Then
                Debug.WriteLine("EVENT - Disposed")
                Dim evt As EventHandler(Of WatcherEventArgs) = Me.DisposedEvent
                evt?.Invoke(Me, New WatcherEventArgs(fileSystemWatcher, e, ArgumentType.StandardEvent))
            End If
        End Sub

        ''' <summary>
        ''' Fired when the main watcher detects an error (the watcher that detected the error is part of
        ''' the event's arguments object)
        ''' </summary>
        ''' <remarks> David, 10/8/2020. </remarks>
        ''' <param name="sender"> Source of the event. </param>
        ''' <param name="e">      Error event information. </param>
        Private Sub FileSystemWatcher_Error(ByVal sender As Object, ByVal e As ErrorEventArgs)
            Dim fileSystemWatcher As FileSystemWatcher = CType(sender, FileSystemWatcher)
            If fileSystemWatcher IsNot Nothing AndAlso e IsNot Nothing Then
                Debug.WriteLine("EVENT - Error")
                Dim evt As EventHandler(Of WatcherEventArgs) = Me.ErrorOccurredEvent
                evt?.Invoke(Me, New WatcherEventArgs(fileSystemWatcher, e, ArgumentType.Error))
            End If
        End Sub

        ''' <summary> Fired when the main watcher detects a file rename. </summary>
        ''' <remarks> David, 10/8/2020. </remarks>
        ''' <param name="sender"> Source of the event. </param>
        ''' <param name="e">      Renamed event information. </param>
        Private Sub FileSystemWatcher_Renamed(ByVal sender As Object, ByVal e As RenamedEventArgs)
            Dim fileSystemWatcher As FileSystemWatcher = CType(sender, FileSystemWatcher)
            If fileSystemWatcher IsNot Nothing AndAlso e IsNot Nothing Then
                Debug.WriteLine("EVENT - Renamed")
                Dim evt As EventHandler(Of WatcherEventArgs) = Me.RenamedEvent
                evt?.Invoke(Me, New WatcherEventArgs(fileSystemWatcher, e, ArgumentType.Renamed))
            End If
        End Sub

        ''' <summary> Event handler. Called by watcher for created deleted events. </summary>
        ''' <remarks> David, 10/8/2020. </remarks>
        ''' <param name="sender"> Source of the event. </param>
        ''' <param name="e">      File system event information. </param>
        Private Sub FileSystemWatcher_CreatedDeleted(ByVal sender As Object, ByVal e As FileSystemEventArgs)
            Dim fileSystemWatcher As FileSystemWatcher = CType(sender, FileSystemWatcher)
            If fileSystemWatcher IsNot Nothing AndAlso e IsNot Nothing Then
                Select Case e.ChangeType
                    Case WatcherChangeTypes.Created
                        Debug.WriteLine("EVENT - Created")
                        Dim evt As EventHandler(Of WatcherEventArgs) = Me.CreatedEvent
                        evt?.Invoke(Me, New WatcherEventArgs(fileSystemWatcher, e, ArgumentType.FileSystem))
                    Case WatcherChangeTypes.Deleted
                        Debug.WriteLine("EVENT - Changed Deleted")
                        Dim evt As EventHandler(Of WatcherEventArgs) = Me.DeletedEvent
                        evt?.Invoke(Me, New WatcherEventArgs(fileSystemWatcher, e, ArgumentType.FileSystem))
                End Select
            End If
        End Sub

        ''' <summary> Event handler. Called by watcher for event folder availability events. </summary>
        ''' <remarks> David, 10/8/2020. </remarks>
        ''' <param name="sender"> Source of the event. </param>
        ''' <param name="e">      Folder availability event information. </param>
        Private Sub FileSystemWatcher_FolderAvailabilityChanged(ByVal sender As Object, ByVal e As System.EventArgs)
            Dim fileSystemWatcher As FileSystemWatcher = CType(sender, FileSystemWatcher)
            If fileSystemWatcher IsNot Nothing AndAlso e IsNot Nothing Then
                Debug.WriteLine("EVENT - Folder Availability Changed")
                Me._IsConnected = fileSystemWatcher.IsFolderExists
                Dim evt As EventHandler(Of WatcherEventArgs) = Me.ConnectionChangedEvent
                evt?.Invoke(Me, New WatcherEventArgs(fileSystemWatcher, e, ArgumentType.FolderAvailability))
                Me.DisposeWatchers()
                If Me.IsConnected Then
                    Me.Initialize()
                End If
            End If
        End Sub

#End Region ' Native Watcher Events

    End Class

End Namespace

