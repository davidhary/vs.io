Namespace Office

    ''' <summary> Defines a base class for reading spreadsheets. </summary>
    ''' <remarks>
    ''' (c) 2009 Integrated Scientific Resources, Inc. All rights reserved. <para>
    ''' Licensed under The MIT License.</para><para>
    ''' David, 01/07/09, 1.2.3294.x. </para>
    ''' </remarks>
    Public MustInherit Class SpreadsheetReaderBase
        Implements IDisposable

#Region " CONSTRUCTION and CLEANUP "

        ''' <summary> Specialized default constructor for use only by derived class. </summary>
        ''' <remarks> David, 10/8/2020. </remarks>
        Protected Sub New()
            MyBase.New
        End Sub

#Region " IDISPOSABLE SUPPORT "

        ''' <summary> True if is disposed, false if not. </summary>
        Private _IsDisposed As Boolean

        ''' <summary>
        ''' Gets or sets the dispose status sentinel of the base class.  This applies to the derived
        ''' class provided proper implementation.
        ''' </summary>
        ''' <value> The is disposed. </value>
        <CodeAnalysis.SuppressMessage("Code Quality", "IDE0051:Remove unused private members", Justification:="<Pending>")>
        Protected Property IsDisposed() As Boolean
            Get
                Return Me._IsDisposed
            End Get
            Private Set(ByVal value As Boolean)
                Me._IsDisposed = value
            End Set
        End Property

        ''' <summary>
        ''' Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged
        ''' resources.
        ''' </summary>
        ''' <remarks> David, 10/8/2020. </remarks>
        ''' <param name="disposing"> True to release both managed and unmanaged resources; false to
        '''                          release only unmanaged resources. </param>
        Protected Overridable Sub Dispose(disposing As Boolean)
            Me._IsDisposed = True
        End Sub

        ''' <summary>
        ''' Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged
        ''' resources.
        ''' </summary>
        ''' <remarks> David, 10/8/2020. </remarks>
        Public Sub Dispose() Implements IDisposable.Dispose
            Me.Dispose(True)
            GC.SuppressFinalize(Me)
        End Sub

#End Region

#End Region

        ''' <summary> Gets or sets the name of the last file that was read. </summary>
        ''' <value> The full pathname of the file file. </value>
        Public Property FilePathName() As String

        ''' <summary> Reads all spreadsheets from the spreadsheet file. </summary>
        ''' <remarks> David, 10/8/2020. </remarks>
        ''' <param name="filePathName"> . </param>
        ''' <returns> True if it succeeds, false if it fails. </returns>
        Public MustOverride Function ReadWorkbook(ByVal filePathName As String) As Boolean

        ''' <summary> The current worksheet name. </summary>
        Private _CurrentWorksheetName As String

        ''' <summary> Returns the name of the currently selected worksheet. </summary>
        ''' <value> The name of the current worksheet. </value>
        Public Property CurrentWorksheetName() As String
            Get
                Return Me._CurrentWorksheetName
            End Get
            Protected Set(value As String)
                Me._CurrentWorksheetName = value
            End Set
        End Property

        ''' <summary> Selects a worksheet by name. </summary>
        ''' <remarks> David, 10/8/2020. </remarks>
        ''' <param name="name"> The name. </param>
        ''' <returns> True if it succeeds, false if it fails. </returns>
        Public MustOverride Function ActivateWorksheet(ByVal name As String) As Boolean

        ''' <summary> Returns a cell value from the work sheet or DB NUll. </summary>
        ''' <remarks> David, 10/8/2020. </remarks>
        ''' <param name="row"> Specifies a row number. </param>
        ''' <param name="col"> Specifies a column number. </param>
        ''' <returns> True if it succeeds, false if it fails. </returns>
        Public MustOverride Function CellValueGetter(ByVal row As Integer, ByVal col As Integer) As Object

        ''' <summary> Returns the value of the given cell as a Nullable Boolean type. </summary>
        ''' <remarks> David, 10/8/2020. </remarks>
        ''' <param name="row">          Specifies a row number. </param>
        ''' <param name="col">          Specifies a column number. </param>
        ''' <param name="defaultValue"> Specifies a default value. </param>
        ''' <returns> True if it succeeds, false if it fails. </returns>
        Public MustOverride Function CellValueGetter(ByVal row As Integer, ByVal col As Integer, ByVal defaultValue As Boolean?) As Boolean?

        ''' <summary> Returns the value of the specified cell as a Boolean type. </summary>
        ''' <remarks> David, 10/8/2020. </remarks>
        ''' <param name="row">          Specifies a row number. </param>
        ''' <param name="col">          Specifies a column number. </param>
        ''' <param name="defaultValue"> Specifies a default value. </param>
        ''' <returns> True if it succeeds, false if it fails. </returns>
        Public MustOverride Function CellValueGetter(ByVal row As Integer, ByVal col As Integer, ByVal defaultValue As Boolean) As Boolean

        ''' <summary> Returns the value of the specified cell as a Nullable Date type. </summary>
        ''' <remarks> David, 10/8/2020. </remarks>
        ''' <param name="row">          Specifies a row number. </param>
        ''' <param name="col">          Specifies a column number. </param>
        ''' <param name="defaultValue"> Specifies a default value. </param>
        ''' <returns> A DateTime? </returns>
        Public MustOverride Function CellValueGetter(ByVal row As Integer, ByVal col As Integer, ByVal defaultValue As DateTime?) As DateTime?

        ''' <summary> Returns the value of the specified cell as a Date type. </summary>
        ''' <remarks> David, 10/8/2020. </remarks>
        ''' <param name="row">          Specifies a row number. </param>
        ''' <param name="col">          Specifies a column number. </param>
        ''' <param name="defaultValue"> Specifies a default value. </param>
        ''' <returns> A DateTime. </returns>
        Public MustOverride Function CellValueGetter(ByVal row As Integer, ByVal col As Integer, ByVal defaultValue As DateTime) As DateTime

        ''' <summary> Returns the value of the specified cell as a Nullable Double type. </summary>
        ''' <remarks> David, 10/8/2020. </remarks>
        ''' <param name="row">          Specifies a row number. </param>
        ''' <param name="col">          Specifies a column number. </param>
        ''' <param name="defaultValue"> Specifies a default value. </param>
        ''' <returns> A Double? </returns>
        Public MustOverride Function CellValueGetter(ByVal row As Integer, ByVal col As Integer, ByVal defaultValue As Double?) As Double?

        ''' <summary> Returns the value of the specified cell as a Double type. </summary>
        ''' <remarks> David, 10/8/2020. </remarks>
        ''' <param name="row">          Specifies a row number. </param>
        ''' <param name="col">          Specifies a column number. </param>
        ''' <param name="defaultValue"> Specifies a default value. </param>
        ''' <returns> A Double. </returns>
        Public MustOverride Function CellValueGetter(ByVal row As Integer, ByVal col As Integer, ByVal defaultValue As Double) As Double

        ''' <summary> Returns the value of the specified cell as a Nullable Integer type. </summary>
        ''' <remarks> David, 10/8/2020. </remarks>
        ''' <param name="row">          Specifies a row number. </param>
        ''' <param name="col">          Specifies a column number. </param>
        ''' <param name="defaultValue"> Specifies a default value. </param>
        ''' <returns> An Integer? </returns>
        Public MustOverride Function CellValueGetter(ByVal row As Integer, ByVal col As Integer, ByVal defaultValue As Integer?) As Integer?

        ''' <summary> Returns the value of the specified cell as a Integer type. </summary>
        ''' <remarks> David, 10/8/2020. </remarks>
        ''' <param name="row">          Specifies a row number. </param>
        ''' <param name="col">          Specifies a column number. </param>
        ''' <param name="defaultValue"> Specifies a default value. </param>
        ''' <returns> An Integer. </returns>
        Public MustOverride Function CellValueGetter(ByVal row As Integer, ByVal col As Integer, ByVal defaultValue As Integer) As Integer

        ''' <summary> Returns a cell value or default value of String type. </summary>
        ''' <remarks> David, 10/8/2020. </remarks>
        ''' <param name="row">          Specifies a row number. </param>
        ''' <param name="col">          Specifies a column number. </param>
        ''' <param name="defaultValue"> Specifies a default value. </param>
        ''' <returns> A String. </returns>
        Public MustOverride Function CellValueGetter(ByVal row As Integer, ByVal col As Integer, ByVal defaultValue As String) As String

        ''' <summary> The current worksheet used range column number. </summary>
        Private _CurrentWorksheetUsedRangeColumnNumber As Integer

        ''' <summary> Gets or sets the left Column. </summary>
        ''' <value> The current worksheet used range column number. </value>
        Public Property CurrentWorksheetUsedRangeColumnNumber() As Integer
            Get
                Return Me._CurrentWorksheetUsedRangeColumnNumber
            End Get
            Protected Set(value As Integer)
                Me._CurrentWorksheetUsedRangeColumnNumber = value
            End Set
        End Property

        ''' <summary> The current worksheet used range row number. </summary>
        Private _CurrentWorksheetUsedRangeRowNumber As Integer

        ''' <summary> Gets or sets the top row. </summary>
        ''' <value> The current worksheet used range row number. </value>
        Public Property CurrentWorksheetUsedRangeRowNumber() As Integer
            Get
                Return Me._CurrentWorksheetUsedRangeRowNumber
            End Get
            Protected Set(value As Integer)
                Me._CurrentWorksheetUsedRangeRowNumber = value
            End Set
        End Property

        ''' <summary> Returns <c>True</c> if the specified cell includes a DBNull value. </summary>
        ''' <remarks> David, 10/8/2020. </remarks>
        ''' <param name="row"> Specifies a row number. </param>
        ''' <param name="col"> Specifies a column number. </param>
        ''' <returns> True if database null, false if not. </returns>
        Public MustOverride Function IsDBNull(ByVal row As Integer, ByVal col As Integer) As Boolean

        ''' <summary> Returns <c>True</c> if the specified cell is empty. </summary>
        ''' <remarks> David, 10/8/2020. </remarks>
        ''' <param name="row"> Specifies a row number. </param>
        ''' <param name="col"> Specifies a column number. </param>
        ''' <returns> True if empty, false if not. </returns>
        Public MustOverride Function IsEmpty(ByVal row As Integer, ByVal col As Integer) As Boolean

        ''' <summary>
        ''' Returns <c>True</c> if the specified cell is empty or includes a DBNull value.
        ''' </summary>
        ''' <remarks> David, 10/8/2020. </remarks>
        ''' <param name="row"> Specifies a row number. </param>
        ''' <param name="col"> Specifies a column number. </param>
        ''' <returns> True if the database null or is empty, false if not. </returns>
        Public MustOverride Function IsDBNullOrEmpty(ByVal row As Integer, ByVal col As Integer) As Boolean

    End Class

End Namespace
