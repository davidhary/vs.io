Imports System.Data
Imports System.Collections.Generic
Namespace Office

    ''' <summary> Imports Access table files using OLE DB Provider. </summary>
    ''' <remarks> (c) 2009 Integrated Scientific Resources, Inc. All rights reserved. <para>
    ''' Licensed under The MIT License.</para> </remarks>
    ''' <remarks> Access reader 
    '''           https://www.codeproject.com/Questions/529337/ConnectplusMSplusAccessplusDatabaseplus-mdb-pluswi
    ''' <para>
    ''' David, 01/07/09, 1.2.3294.x. </para></remarks>
    Public NotInheritable Class AccessImport

        ''' <summary>
        ''' Prevent construction.
        ''' </summary>
        Private Sub New()
            MyBase.New()
        End Sub

#Region " OLEDB CONNECTION STRING BUILDERS "

        ''' <summary> The jet provider version 4.0. </summary>
        Public Const JetProvider4 As String = "Microsoft.Jet.OLEDB.4.0"

        ''' <summary> The ace provider version 12.0. </summary>
        Public Const AceProviderVersion12 As String = "Microsoft.Ace.OLEDB.12.0"

        ''' <summary> The ace provider version 16. </summary>
        Public Const AceProviderVersion16 As String = "Microsoft.Ace.OLEDB.12.0"

        ''' <summary> Gets or sets the preferred provider. </summary>
        ''' <value> The preferred provider. </value>
        Public Shared Property PreferredProvider As String = AccessImport.AceProviderVersion16

        ''' <summary> Builds the connection string using the <see cref="PreferredProvider"/>. </summary>
        ''' <param name="filePathName">        Specifies the database file name. </param>
        ''' <returns> A String. </returns>
        Public Shared Function BuildConnectionString(ByVal filePathName As String) As String
            Return $"Provider={AccessImport.PreferredProvider};Data Source={filePathName};"
        End Function

        ''' <summary> Builds an OLEDB Connection string to an access database file. </summary>
        ''' <remarks>
        ''' A setting of IMEX=1 is used in the connection string�s extended property determines whether
        ''' the ImportMixedTypes value is honored. IMEX refers to Import/Export mode. There are three
        ''' possible values. IMEX=0 and IMEX=2 result in ImportMixedTypes being ignored and the default
        ''' value of 'Majority Types' is used. IMEX=1 is the only way to ensure ImportMixedTypes=Text is
        ''' honored.
        ''' </remarks>
        ''' <param name="provider">            The provider. </param>
        ''' <param name="filePathName">        Specifies the database file name. </param>
        ''' <returns> A String. </returns>
        Public Shared Function BuildConnectionString(ByVal provider As String, ByVal filePathName As String) As String
            Return $"Provider={provider};Data Source={filePathName};"
        End Function

#End Region

#Region " OLE DB READER "

        Private Shared Sub CheckJet(Optional ByVal fileName As String = "C:\My\Libraries\VS\IO\IO\Tests\Office\Access\Products.mdb")
            Dim is64 As Boolean = IntPtr.Size = 8
            Dim connectionString As String = isr.IO.Office.AccessImport.BuildConnectionString(isr.IO.Office.AccessImport.JetProvider4, fileName)
            Using connection As New OleDb.OleDbConnection(connectionString)
                connection.Open()
            End Using
        End Sub

        ''' <summary> Reads a database. </summary>
        ''' <param name="provider"> The provider. </param>
        ''' <param name="fileName"> Specifies the database file name. </param>
        ''' <returns> The database. </returns>
        Public Shared Function ReadDatabase(ByVal provider As String, ByVal fileName As String) As DataSet
            Return AccessImport.ReadDatabase(AccessImport.BuildConnectionString(provider, fileName))
        End Function

        <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Reliability", "CA2000:Dispose objects before losing scope", Justification:="Dataset is returned.")>
        Public Shared Function ReadDatabase(ByVal connectionString As String) As DataSet
            Dim ds As New DataSet With {.Locale = Globalization.CultureInfo.InvariantCulture}
            Using connection As New OleDb.OleDbConnection(connectionString)
                Return AccessImport.ReadDatabase(connection)
            End Using
        End Function

        ''' <summary> Reads a database. </summary>
        ''' <param name="connection"> The connection. </param>
        ''' <returns> The database. </returns>
        Public Shared Function ReadDatabase(ByVal connection As OleDb.OleDbConnection) As DataSet
            Dim ds As New DataSet With {.Locale = Globalization.CultureInfo.InvariantCulture}
            connection.Open()
            Dim tableNames As New List(Of String)
            ' get the list of tables from the database
            Using schemaTables As DataTable = connection.GetOleDbSchemaTable(OleDb.OleDbSchemaGuid.Tables, Nothing)
                For Each row As DataRow In schemaTables.Rows
                    tableNames.Add(CStr(row.Item("TABLE_NAME")))
                Next
            End Using

            ' read each sheet into a table and add to the data set
            For Each tableName As String In tableNames
                ' get an adapter to read all records from the sheet
                Using adapter As OleDb.OleDbDataAdapter = New OleDb.OleDbDataAdapter($"SELECT * FROM [{tableName}]", connection)
                    adapter.TableMappings.Add("Table", tableName)
                    adapter.Fill(ds)
                End Using
            Next
            ' close also disposes causing a double dispose; otherwise uncomment: connection.Close()
            Return ds
        End Function

        ''' <summary> Reads a table. </summary>
        ''' <param name="connection"> The connection. </param>
        ''' <param name="dataset">    The dataset. </param>
        ''' <param name="tableName">  Name of the table. </param>
        Public Shared Sub ReadTable(ByVal connection As OleDb.OleDbConnection, ByVal dataset As DataSet, ByVal tableName As String)
            ' get an adapter to read all records from the sheet
            Using adapter As OleDb.OleDbDataAdapter = New OleDb.OleDbDataAdapter($"SELECT * FROM [{tableName}]", connection)
                adapter.TableMappings.Add("Table", tableName)
                adapter.Fill(dataset)
            End Using
        End Sub

        ''' <summary> Reads a table. </summary>
        ''' <param name="connection"> The connection. </param>
        ''' <param name="tableName">  Name of the table. </param>
        ''' <returns> The table. </returns>
        Public Shared Function ReadTable(ByVal connection As OleDb.OleDbConnection, ByVal tableName As String) As DataSet
            Dim ds As New DataSet With {.Locale = Globalization.CultureInfo.InvariantCulture}
            ' get an adapter to read all records from the sheet
            Using adapter As OleDb.OleDbDataAdapter = New OleDb.OleDbDataAdapter($"SELECT * FROM [{tableName}]", connection)
                adapter.TableMappings.Add("Table", tableName)
                adapter.Fill(ds)
            End Using
            Return ds
        End Function

#End Region

    End Class

End Namespace
