Imports System.Data
Namespace Office

    ''' <summary> Imports Excel files using XML (XLS). </summary>
    ''' <remarks>
    ''' David, 01/07/09, 1.2.3294. Excel Data Manipulation Using VB.NET
    ''' http://www.CodeProject.com/KB/vb/ExcelDataManipulation.aspx <para>
    ''' (c) 2009 Integrated Scientific Resources, Inc. All rights reserved.</para><para>
    ''' Licensed under The MIT License.</para>
    ''' </remarks>
    Public NotInheritable Class XlsImport

        ''' <summary> Prevent construction. </summary>
        ''' <remarks> David, 10/8/2020. </remarks>
        Private Sub New()
            MyBase.New()
        End Sub

#Region " OPEN XML IMPORTER: XLS "

        ''' <summary> Defines a structure to handle various column types. </summary>
        ''' <remarks> David, 10/8/2020. </remarks>
        Private Structure ColumnType

            ''' <summary> Gets or sets the type of the date. </summary>
            ''' <value> The type of the date. </value>
            Public ReadOnly Property DateType As Type
            ''' <summary> The name. </summary>
            <CodeAnalysis.SuppressMessage("Code Quality", "IDE0052:Remove unread private members", Justification:="<Pending>")>
            Private ReadOnly _Name As String

            ''' <summary> Constructor. </summary>
            ''' <remarks> David, 10/8/2020. </remarks>
            ''' <param name="type"> The type. </param>
            Public Sub New(ByVal type As Type)
                Me.DateType = type
                Me._Name = type.ToString().ToLower(Globalization.CultureInfo.CurrentCulture)
            End Sub

            ''' <summary> Parses the input string to strongly typed values. </summary>
            ''' <remarks> David, 10/8/2020. </remarks>
            ''' <param name="input"> . </param>
            ''' <returns> An Object. </returns>
            <CodeAnalysis.SuppressMessage("Code Quality", "IDE0051:Remove unused private members", Justification:="<Pending>")>
            Public Function ParseString(ByVal input As String) As Object
                If String.IsNullOrWhiteSpace(input) Then
                    Return DBNull.Value
                End If
                Select Case Me.DateType.ToString()
                    Case GetType(System.DateTimeOffset).ToString
                        Return DateTimeOffset.Parse(input, Globalization.CultureInfo.CurrentCulture)
                    Case GetType(System.DateTime).ToString
                        Return DateTime.Parse(input, Globalization.CultureInfo.CurrentCulture)
                    Case GetType(System.Decimal).ToString
                        Return Decimal.Parse(input, Globalization.NumberStyles.Number Or Globalization.NumberStyles.AllowExponent, Globalization.CultureInfo.CurrentCulture)
                    Case GetType(System.Boolean).ToString
                        Return Boolean.Parse(input)
                    Case Else
                        Return input
                End Select
            End Function

        End Structure

        ''' <summary> Returns a default (String) column type. </summary>
        ''' <remarks> David, 10/8/2020. </remarks>
        ''' <returns> A ColumnType. </returns>
        Private Shared Function DefaultColumnType() As ColumnType
            Return New ColumnType(GetType(String))
        End Function

        ''' <summary> Imports a work book to a database from an Excel file. </summary>
        ''' <remarks> David, 10/8/2020. </remarks>
        ''' <param name="filePathName"> Specifies an Excel file path name. </param>
        ''' <returns> A DataSet. </returns>
        Public Shared Function ImportWorkbook(ByVal filePathName As String) As DataSet
            Return ImportWorkbook(filePathName, True, True)
        End Function

        ''' <summary> Imports workbook from open XML file. </summary>
        ''' <remarks> David, 10/8/2020. </remarks>
        ''' <param name="filePathName">         Specifies an XLS file path name. </param>
        ''' <param name="userFirstRowHeaders">  Specifies the definition of header for each column. If
        '''                                     the value is True, the first row will be treated as
        '''                                     heading. Otherwise, the heading will be generated by the
        '''                                     system like F1, F2 and so on. </param>
        ''' <param name="autoDetectColumnType"> . </param>
        ''' <returns> A DataSet. </returns>
        Public Shared Function ImportWorkbook(ByVal filePathName As String, ByVal userFirstRowHeaders As Boolean, ByVal autoDetectColumnType As Boolean) As DataSet
            Using fs As System.IO.FileStream = New System.IO.FileStream(filePathName, System.IO.FileMode.Open, System.IO.FileAccess.Read, System.IO.FileShare.ReadWrite)
                Return ImportWorkbook(fs, userFirstRowHeaders, autoDetectColumnType)
            End Using
        End Function

        ''' <summary> Imports workbook from open XML file stream. </summary>
        ''' <remarks> David, 10/8/2020. </remarks>
        ''' <param name="inputFileStream">      . </param>
        ''' <param name="userFirstRowHeaders">  Specifies the definition of header for each column. If
        '''                                     the value is True, the first row will be treated as
        '''                                     heading. Otherwise, the heading will be generated by the
        '''                                     system like F1, F2 and so on. </param>
        ''' <param name="autoDetectColumnType"> . </param>
        ''' <returns> A DataSet. </returns>
        Public Shared Function ImportWorkbook(ByVal inputFileStream As System.IO.Stream, ByVal userFirstRowHeaders As Boolean, ByVal autoDetectColumnType As Boolean) As DataSet

            Dim doc As Xml.XmlDocument = New Xml.XmlDocument()
            doc.Load(New Xml.XmlTextReader(inputFileStream))
            Dim nsmgr As Xml.XmlNamespaceManager = New Xml.XmlNamespaceManager(doc.NameTable)

            nsmgr.AddNamespace("o", "urn:schemas-microsoft-com:office:office")
            nsmgr.AddNamespace("x", "urn:schemas-microsoft-com:office:excel")
            nsmgr.AddNamespace("ss", "urn:schemas-microsoft-com:office:spreadsheet")

            Dim ds As DataSet = New DataSet() With {
                .Locale = Globalization.CultureInfo.InvariantCulture
            }

            For Each node As Xml.XmlNode In doc.DocumentElement.SelectNodes("//ss:Worksheet", nsmgr)
                Dim dt As DataTable = New DataTable(node.Attributes("ss:Name").Value) With {
                    .Locale = Globalization.CultureInfo.InvariantCulture
                }
                ds.Tables.Add(dt)
                Dim rows As Xml.XmlNodeList = node.SelectNodes("ss:Table/ss:Row", nsmgr)
                If rows.Count > 0 Then
                    Dim columns As IList(Of ColumnType) = New List(Of ColumnType)()
                    Dim startIndex As Integer = 0
                    If userFirstRowHeaders Then
                        For Each data As Xml.XmlNode In rows(0).SelectNodes("ss:Cell/ss:Data", nsmgr)
                            columns.Add(New ColumnType(GetType(String))) 'default to text
                            dt.Columns.Add(data.InnerText, GetType(String))
                        Next data
                        startIndex += 1
                    End If
                    If autoDetectColumnType AndAlso rows.Count > 0 Then
                        Dim cells As Xml.XmlNodeList = rows(startIndex).SelectNodes("ss:Cell", nsmgr)
                        Dim actualCellIndex As Integer = 0
                        For cellIndex As Integer = 0 To cells.Count - 1
                            Dim cell As Xml.XmlNode = cells(cellIndex)
                            If Not cell.Attributes("ss:Index") Is Nothing Then
                                actualCellIndex = Integer.Parse(cell.Attributes("ss:Index").Value,
                                                                Globalization.NumberStyles.Number Or Globalization.NumberStyles.AllowLeadingSign, Globalization.CultureInfo.CurrentCulture) - 1
                            End If

                            Dim autoDetectType As ColumnType = ParseColumnType(cell.SelectSingleNode("ss:Data", nsmgr))

                            If actualCellIndex >= dt.Columns.Count Then
                                dt.Columns.Add($"Column{actualCellIndex}", autoDetectType.DateType)
                                columns.Add(autoDetectType)
                            Else
                                dt.Columns(actualCellIndex).DataType = autoDetectType.DateType
                                columns(actualCellIndex) = autoDetectType
                            End If

                            actualCellIndex += 1
                        Next cellIndex
                    End If

                    For i As Integer = startIndex To rows.Count - 1

                        Dim row As DataRow = dt.NewRow()
                        Dim cells As Xml.XmlNodeList = rows(i).SelectNodes("ss:Cell", nsmgr)
                        Dim actualCellIndex As Integer = 0
                        For cellIndex As Integer = 0 To cells.Count - 1
                            Dim cell As Xml.XmlNode = cells(cellIndex)
                            If Not cell.Attributes("ss:Index") Is Nothing Then
                                actualCellIndex = Integer.Parse(cell.Attributes("ss:Index").Value,
                                                                Globalization.NumberStyles.Number Or Globalization.NumberStyles.AllowLeadingSign, Globalization.CultureInfo.CurrentCulture) - 1
                            End If

                            Dim data As Xml.XmlNode = cell.SelectSingleNode("ss:Data", nsmgr)

                            If actualCellIndex >= dt.Columns.Count Then

                                For j As Integer = dt.Columns.Count To actualCellIndex - 1
                                    dt.Columns.Add($"Column{actualCellIndex}", GetType(String))
                                    columns.Add(DefaultColumnType())
                                Next j

                                Dim autoDetectType As ColumnType = ParseColumnType(cell.SelectSingleNode("ss:Data", nsmgr))
                                dt.Columns.Add($"Column{actualCellIndex}", GetType(String))
                                columns.Add(autoDetectType)
                            End If
                            If Not data Is Nothing Then
                                row(actualCellIndex) = data.InnerText
                            End If

                            actualCellIndex += 1
                        Next cellIndex

                        dt.Rows.Add(row)
                    Next i
                End If
            Next node
            Return ds

            '<?xml version="1.0"?>
            '<?mso-application ProgId="Excel.Sheet"?>
            '<Workbook>
            ' <Worksheet ss:Name="Sheet1">
            '  <Table>
            '   <Row>
            '    <Cell><Data ss:Type="String">Item Number</Data></Cell>
            '    <Cell><Data ss:Type="String">Description</Data></Cell>
            '    <Cell ss:StyleID="s21"><Data ss:Type="String">Item Barcode</Data></Cell>
            '   </Row>
            ' </Worksheet>
            '</Workbook>

        End Function

        ''' <summary> Parse the column type. </summary>
        ''' <remarks> David, 10/8/2020. </remarks>
        ''' <param name="data"> . </param>
        ''' <returns> A ColumnType. </returns>
        Private Overloads Shared Function ParseColumnType(ByVal data As Xml.XmlNode) As ColumnType

            Dim type As String = If(data.Attributes("ss:Type") Is Nothing OrElse data.Attributes("ss:Type").Value Is Nothing,
                String.Empty,
                data.Attributes("ss:Type").Value)
            Select Case type
                Case "DateTimeOffset"
                    Return New ColumnType(GetType(DateTimeOffset))
                Case "DateTime"
                    Return New ColumnType(GetType(DateTime))
                Case "Boolean"
                    Return New ColumnType(GetType(Boolean))
                Case "Number"
                    Return New ColumnType(GetType(Decimal))
                Case ""
                    Dim candidate As Decimal
                    Return If(data Is Nothing OrElse String.IsNullOrWhiteSpace(data.InnerText) OrElse Decimal.TryParse(data.InnerText, candidate),
                        New ColumnType(GetType(Decimal)),
                        New ColumnType(GetType(String)))
                Case Else '"String"
                    Return New ColumnType(GetType(String))
            End Select
        End Function

#End Region

    End Class

End Namespace
