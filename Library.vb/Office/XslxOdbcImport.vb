Imports System.Data
Namespace Office

    ''' <summary> Imports XSLX files using OLE DB Provider. </summary>
    ''' <remarks>
    ''' David, 01/07/09, 1.2.3294. Excel Data Manipulation Using VB.NET  <para>
    ''' http://www.CodeProject.com/KB/vb/ExcelDataManipulation.aspx
    ''' https://stackoverflow.com/questions/14261655/best-fastest-way-to-read-an-excel-sheet-into-a-datatable
    ''' https://support.microsoft.com/en-us/help/278973/excelado-demonstrates-how-to-use-ado-to-read-and-write-data-in-excel-w
    ''' https://blogs.msdn.microsoft.com/dataaccesstechnologies/2017/10/18/unexpected-error-from-external-database-driver-1-microsoft-jet-database-engine-after-applying-october-security-updates/https://social.msdn.microsoft.com/Forums/en-US/2feac7ff-3fbd-4d46-afdc-65341762f753/odbc-excel-driver-stopped-working-with-quotunexpected-error-from-external-database-driver-1?forum=sqldataaccess
    ''' (c) 2009 Integrated Scientific Resources, Inc. All rights reserved. </para><para>
    ''' Licensed under The MIT License. </para>
    ''' </remarks>
    Public NotInheritable Class XlsxOdbcImport

        ''' <summary> Prevent construction. </summary>
        ''' <remarks> David, 10/8/2020. </remarks>
        Private Sub New()
            MyBase.New()
        End Sub

#Region " OLEDB CONNECTION STRING BUILDERS "

        ''' <summary> The jet provider version 4.0. </summary>
        Public Const JetProvider4 As String = "Microsoft.Jet.OLEDB.4.0"

        ''' <summary> The ace provider version 12.0. </summary>
        Public Const AceProviderVersion12 As String = "Microsoft.Ace.OLEDB.12.0"

        ''' <summary> The ace provider version 16. </summary>
        Public Const AceProviderVersion16 As String = "Microsoft.Ace.OLEDB.12.0"

        ''' <summary> Gets or sets the preferred provider. </summary>
        ''' <value> The preferred provider. </value>
        Public Shared Property PreferredProvider As String = AceProviderVersion16

        ''' <summary>
        ''' Builds an OLEDB Connection string to an excel file using the <see cref="PreferredProvider"/>.
        ''' </summary>
        ''' <remarks> Uses excel version 2000 and above. </remarks>
        ''' <param name="filePathName"> Specifies the excel file name. </param>
        ''' <returns> A String. </returns>
        Public Shared Function BuildConnectionString(ByVal filePathName As String) As String
            Return BuildConnectionString(PreferredProvider, filePathName, False, False)
            ' Dim format As String = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source={0};Extended Properties='Excel 8.0; IMEX=1;';"
            ' Return String.Format(Globalization.CultureInfo.CurrentCulture, format, filePathName)
        End Function

        ''' <summary>
        ''' Builds an OLEDB Connection string to an excel file using the <see cref="PreferredProvider"/>.
        ''' </summary>
        ''' <remarks> Uses excel version 2000 and above. </remarks>
        ''' <param name="provider">     The provider. </param>
        ''' <param name="filePathName"> Specifies the excel file name. </param>
        ''' <returns> A String. </returns>
        Public Shared Function BuildConnectionString(ByVal provider As String, ByVal filePathName As String) As String
            Return BuildConnectionString(provider, filePathName, False, True)
        End Function

        ''' <summary>
        ''' Builds an OLEDB Connection string to an excel file using the <see cref="PreferredProvider"/>.
        ''' </summary>
        ''' <remarks> Uses excel version 2000 and above. </remarks>
        ''' <param name="provider">            The provider. </param>
        ''' <param name="filePathName">        Specifies the excel file name. </param>
        ''' <param name="userFirstRowHeaders"> True to export headers. </param>
        ''' <returns> A String. </returns>
        Public Shared Function BuildConnectionString(ByVal provider As String, ByVal filePathName As String, ByVal userFirstRowHeaders As Boolean) As String
            Return BuildConnectionString(provider, filePathName, False, userFirstRowHeaders)
        End Function

        ''' <summary>
        ''' Builds an OLEDB Connection string to an excel file using the <see cref="PreferredProvider"/>.
        ''' </summary>
        ''' <remarks>
        ''' A setting of IMEX=1 is used in the connection string’s extended property determines whether
        ''' the ImportMixedTypes value is honored. IMEX refers to Import/Export mode. There are three
        ''' possible values. IMEX=0 and IMEX=2 result in ImportMixedTypes being ignored and the default
        ''' value of 'Majority Types' is used. IMEX=1 is the only way to ensure ImportMixedTypes=Text is
        ''' honored.
        ''' </remarks>
        ''' <param name="provider">              The provider. </param>
        ''' <param name="filePathName">          Specifies the excel file name. </param>
        ''' <param name="excelVersionBelow2000"> Specifies the excel version.  
        '''                                      For Excel 2000 and above, it is set it to Excel 8.0 and
        '''                                      for all others, it is Excel 5.0. </param>
        ''' <param name="userFirstRowHeaders">   Specifies the definition of header for each column. If
        '''                                      the value is True, the first row will be treated as
        '''                                      heading. Otherwise, the heading will be generated by the
        '''                                      system like F1, F2 and so on. </param>
        ''' <returns> A String. </returns>
        Public Shared Function BuildConnectionString(ByVal provider As String, ByVal filePathName As String, ByVal excelVersionBelow2000 As Boolean,
                                                      ByVal userFirstRowHeaders As Boolean) As String
            Dim excelVersion As String = If(excelVersionBelow2000, "5.0", "8.0")
            Return BuildConnectionString(provider, filePathName, excelVersion, userFirstRowHeaders)
            ' Dim format As String = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source={0};Extended Properties='Excel {1}; HDR={2}; IMEX=1;';"
            ' Return String.Format(Globalization.CultureInfo.CurrentCulture, format, filePathName, If(excelVersionBelow2000, "5.0", "8.0"), If(userFirstRowHeaders, "Yes", "No"))
            ' "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=.\excel\myExcelFile.xls;Extended Properties=Excel 8.0;HDR=Yes;"
        End Function

        ''' <summary> Builds an OLEDB Connection string to an excel file. </summary>
        ''' <remarks>
        ''' A setting of IMEX=1 is used in the connection string’s extended property determines whether
        ''' the ImportMixedTypes value is honored. IMEX refers to Import/Export mode. There are three
        ''' possible values. IMEX=0 and IMEX=2 result in ImportMixedTypes being ignored and the default
        ''' value of 'Majority Types' is used. IMEX=1 is the only way to ensure ImportMixedTypes=Text is
        ''' honored.
        ''' </remarks>
        ''' <param name="provider">            The provider. </param>
        ''' <param name="filePathName">        Specifies the excel file name. </param>
        ''' <param name="excelVersion">        The excel version. </param>
        ''' <param name="userFirstRowHeaders"> True to export headers. </param>
        ''' <returns> A String. </returns>
        Public Shared Function BuildConnectionString(ByVal provider As String, ByVal filePathName As String, ByVal excelVersion As String, ByVal userFirstRowHeaders As Boolean) As String
            Return $"Provider={provider};Data Source={filePathName};Extended Properties='Excel {excelVersion}; HDR={If(userFirstRowHeaders, "Yes", "No")}; IMEX=1;';"
        End Function

#End Region

#Region " OLE DB READER "

        ''' <summary> Returns a human readable sheet name from the internal sheet name. </summary>
        ''' <remarks> David, 10/8/2020. </remarks>
        ''' <param name="internalSheetName"> Specifies the internal sheet name. </param>
        ''' <returns> A String. </returns>
        Private Shared Function ParseSheetName(ByVal internalSheetName As String) As String
            Return internalSheetName.Trim("$"c).Replace("#"c, ".")
        End Function

        ''' <summary> Reads the work sheets from the workbook into datasets. </summary>
        ''' <remarks> David, 10/8/2020. </remarks>
        ''' <param name="filePathName"> Specifies an Excel file path name. </param>
        ''' <returns> The worksheets. </returns>
        Public Shared Function ReadWorksheets(ByVal filePathName As String) As DataSet
            Dim ds As New DataSet With {.Locale = Globalization.CultureInfo.InvariantCulture}
            Dim connectionString As String = BuildConnectionString(filePathName)

            Using connection As New OleDb.OleDbConnection(connectionString)

                connection.Open()

                Dim internalSheetNames As New List(Of String)
                ' get the list of sheets from the workbook
                Using schemaTables As DataTable = connection.GetOleDbSchemaTable(OleDb.OleDbSchemaGuid.Tables, Nothing)
                    For Each row As DataRow In schemaTables.Rows
                        internalSheetNames.Add(CStr(row.Item("TABLE_NAME")))
                    Next
                End Using

                ' read each sheet into a table and add to the data set
                For Each internalSheetName As String In internalSheetNames
                    ' get an adapter to read all records from the sheet
                    Using adapter As OleDb.OleDbDataAdapter = New OleDb.OleDbDataAdapter($"SELECT * FROM [{internalSheetName}]", connection)
                        Dim tableName As String = ParseSheetName(internalSheetName)
                        adapter.TableMappings.Add("Table", tableName)
                        adapter.Fill(ds)
                    End Using
                Next

                ' close also disposes causing a double dispose; otherwise uncomment: connection.Close()
            End Using

            Return ds

        End Function

#End Region

#Region " Excel File IMPORTER USING OLE DB "

        ''' <summary> Imports a work book to a database from an Excel file. </summary>
        ''' <remarks> David, 10/8/2020. </remarks>
        ''' <param name="connectionString"> Specifies an OLEDB Connection string. </param>
        ''' <returns> A DataSet. </returns>
        Public Shared Function ImportWorkbook(ByVal connectionString As String) As DataSet

            Dim output As DataSet = New DataSet() With {.Locale = Globalization.CultureInfo.InvariantCulture}

            Using connection As OleDb.OleDbConnection = New OleDb.OleDbConnection(connectionString)

                connection.Open()

                Dim dt As DataTable = connection.GetOleDbSchemaTable(OleDb.OleDbSchemaGuid.Tables, New Object() {Nothing, Nothing, Nothing, "TABLE"})

                For Each row As DataRow In dt.Rows

                    Dim internalSheetName As String = row("TABLE_NAME").ToString()

                    Dim cmd As OleDb.OleDbCommand = New OleDb.OleDbCommand("SELECT * FROM [" & internalSheetName & "]", connection) With {
                        .CommandType = CommandType.Text
                    }

                    Dim tableName As String = ParseSheetName(internalSheetName)
                    Dim outputTable As DataTable = New DataTable(tableName) With {
                        .Locale = Globalization.CultureInfo.InvariantCulture
                    }
                    output.Tables.Add(outputTable)

                    CType(New OleDb.OleDbDataAdapter(cmd), OleDb.OleDbDataAdapter).Fill(outputTable)

                Next row

                'close also disposes causing a double dispose; otherwise uncomment: connection.Close()

            End Using

            Return output

        End Function

        ''' <summary> Imports a work book to a database from an Excel file. </summary>
        ''' <remarks> Uses Excel 8.0 file format. </remarks>
        ''' <exception cref="System.IO.IOException"> Thrown when an IO failure occurred. </exception>
        ''' <param name="provider">            The provider. </param>
        ''' <param name="filePathName">        Specifies an Excel file path name. </param>
        ''' <param name="userFirstRowHeaders"> Specifies the definition of header for each column. If
        '''                                    the value is True, the first row will be treated as
        '''                                    heading. Otherwise, the heading will be generated by the
        '''                                    system like F1, F2 and so on. </param>
        ''' <returns> A DataSet. </returns>
        Public Shared Function ImportWorkbook(ByVal provider As String, ByVal filePathName As String, ByVal userFirstRowHeaders As Boolean) As DataSet

            Try

                Dim connectionString As String = BuildConnectionString(provider, filePathName, False, userFirstRowHeaders)
                Return ImportWorkbook(connectionString)

            Catch ex As OleDb.OleDbException

                Dim additionalInfo As String = String.Empty
                For Each oledbError As OleDb.OleDbError In ex.Errors
                    additionalInfo &= String.Format(Globalization.CultureInfo.CurrentCulture,
                                                    "Failed importing workbook. Message: {1}{0}Native Error: {2}{0}SQL State: {3}{0}Source: {4}",
                                                     Environment.NewLine, oledbError.Message, oledbError.NativeError, oledbError.SQLState, oledbError.Source)
                Next
                Throw New System.IO.IOException(additionalInfo, ex)

            Catch

                Throw

            End Try

        End Function

#End Region

    End Class

End Namespace
