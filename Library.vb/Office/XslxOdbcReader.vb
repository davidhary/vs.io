Imports System.Data
Namespace Office

    ''' <summary>
    ''' Handles access to Excel spreadsheets using an <see cref="IExcelReader">Excel Data Reader.
    ''' </see>.
    ''' </summary>
    ''' <remarks>
    ''' Requires using the ACE provided, which mean s installing the Access runtime for eaither 32 or
    ''' 64 bits.  This likely to break programs using 64 or 32 bit provider, respectively, and is
    ''' therefore unsafe. (c) 2009 Integrated Scientific Resources, Inc. All rights reserved. <para>
    ''' Licensed under The MIT License.</para><para>
    ''' David, 6/22/2019,  </para><para>
    ''' David, 01/07/09, 1.2.3294.x. </para>
    ''' </remarks>
    Public Class ExcelReader
        Inherits SpreadsheetReaderBase

#Region " CONSTRUCTION and CLEANUP "

        ''' <summary> Constructs this class. </summary>
        ''' <remarks> David, 10/8/2020. </remarks>
        Public Sub New()

            MyBase.New()

        End Sub

        ''' <summary>
        ''' Releases the unmanaged resources used by the <see cref="T:System.Windows.Forms.Control" />
        ''' and its child controls and optionally releases the managed resources.
        ''' </summary>
        ''' <remarks> David, 10/8/2020. </remarks>
        ''' <param name="disposing"> <c>True</c> to release both managed and unmanaged resources;
        '''                                                   <c>False</c> to release only unmanaged
        '''                                                   resources when called from the runtime
        '''                                                   finalize. </param>
        <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
        <System.Diagnostics.DebuggerNonUserCode()>
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            Try
                If Not Me.IsDisposed AndAlso disposing Then
                End If
                If Me._Workbook IsNot Nothing Then Me._Workbook.Dispose() : Me._Workbook = Nothing
                If Me._Worksheet IsNot Nothing Then Me._Worksheet.Dispose() : Me._Worksheet = Nothing
            Finally
                MyBase.Dispose(disposing)
            End Try
        End Sub

        ''' <summary>
        ''' This destructor will run only if the Dispose method does not get called. It gives the base
        ''' class the opportunity to finalize. Do not provide destructors in types derived from this
        ''' class.
        ''' </summary>
        ''' <remarks> David, 10/8/2020. </remarks>
        Protected Overrides Sub Finalize()
            ' Do not re-create Dispose clean-up code here.
            ' Calling Dispose(false) is optimal for readability and maintainability.
            Me.Dispose(False)
        End Sub

#End Region

#Region " EXCEL READER "

        ''' <summary> Reads all worksheets from the Excel file. </summary>
        ''' <remarks> David, 10/8/2020. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <exception cref="System.IO.FileNotFoundException"> Thrown when the requested file is not present. </exception>
        ''' <param name="filePathName"> . </param>
        ''' <returns> True if it succeeds, false if it fails. </returns>
        Public Overrides Function ReadWorkbook(ByVal filePathName As String) As Boolean

            ' dispose of previous data
            If Me._Workbook IsNot Nothing Then Me._Workbook.Dispose() : Me._Workbook = Nothing

            ' dispose of the worksheet
            If Me._Worksheet IsNot Nothing Then Me._Worksheet.Dispose() : Me._Worksheet = Nothing

            ' clear the file name
            Me.FilePathName = String.Empty

            If String.IsNullOrWhiteSpace(filePathName) Then Throw New ArgumentNullException(NameOf(filePathName))
            If Not System.IO.File.Exists(filePathName) Then
                Throw New System.IO.FileNotFoundException("Failed reading workbook because the workbook file -- file not found.", filePathName)
            End If

            Me.FilePathName = filePathName

            ' build the connection string using the default provider
            Dim connectionString As String = XlsxOdbcImport.BuildConnectionString(filePathName)

            ' read the worksheets using OLE DB.
            Me._Workbook = XlsxOdbcImport.ImportWorkbook(connectionString)

            Return True

        End Function

        ''' <summary> The workbook. </summary>
        Private _Workbook As DataSet

        ''' <summary> Gets reference to the workbook dataset. </summary>
        ''' <value> The workbook. </value>
        Public ReadOnly Property Workbook() As DataSet
            Get
                Return Me._Workbook
            End Get
        End Property

#End Region

#Region " WORKSHEET "

        ''' <summary> The worksheet. </summary>
        Private _Worksheet As DataTable

        ''' <summary>
        ''' Gets or sets reference to the worksheet table representing the last selected work sheet.
        ''' </summary>
        ''' <value> The worksheet. </value>
        Public Property Worksheet() As DataTable
            Get
                Return Me._Worksheet
            End Get
            Set(value As DataTable)
                Me._Worksheet = value
                Me.CurrentWorksheetName = Me._Worksheet?.TableName
                Me.CurrentWorksheetUsedRangeColumnNumber = If(Me.Worksheet IsNot Nothing AndAlso Me._Worksheet?.Columns.Count > 0, 1, 0)
                Me.CurrentWorksheetUsedRangeRowNumber = If(Me.Worksheet IsNot Nothing AndAlso Me._Worksheet?.Rows.Count > 0, 1, 0)
            End Set
        End Property

        ''' <summary> Selects a worksheet by name. </summary>
        ''' <remarks> David, 10/8/2020. </remarks>
        ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
        ''' <exception cref="System.IO.IOException">               Thrown when an IO failure occurred. </exception>
        ''' <param name="name"> The name. </param>
        ''' <returns> True if it succeeds, false if it fails. </returns>
        Public Overrides Function ActivateWorksheet(ByVal name As String) As Boolean

            If Me._Workbook Is Nothing Then
                Throw New InvalidOperationException("failed activating worksheet because workbook was not read from the Excel file.")
            End If

            ' Select the specified sheet
            If Not String.IsNullOrWhiteSpace(Me.CurrentWorksheetName) AndAlso Me.CurrentWorksheetName.Equals(name, StringComparison.OrdinalIgnoreCase) Then
                Return True
            Else
                Try
                    Me._Worksheet = Me._Workbook.Tables(name)
                Finally
                End Try
                If String.IsNullOrWhiteSpace(Me.CurrentWorksheetName) Then
                    Throw New System.IO.IOException($"failed activating worksheet {name} in {Me.FilePathName}")
                    Return False
                Else
                    Return True
                End If
            End If

        End Function

#End Region

#Region " GETTERS "

        ''' <summary> Returns a cell value from the work sheet. </summary>
        ''' <remarks> Excel rows and columns are one-based. </remarks>
        ''' <exception cref="System.IO.IOException">                 Thrown when an IO failure occurred. </exception>
        ''' <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
        '''                                                the required range. </exception>
        ''' <param name="row"> Specifies a one-based row number. </param>
        ''' <param name="col"> Specifies a one-based column number. </param>
        ''' <returns>
        ''' Cell value or DB Null if column or row indexes exceed the number of rows or columns in the
        ''' worksheet. This allows reading values that are generally thought to be defined in a worksheet
        ''' but where not year defined in the particular worksheet and thus might be expected to be given
        ''' a default value. The default values are derived in the relevant getters.  Otherwise, a DB
        ''' Null is returned.
        ''' </returns>
        Public Overrides Function CellValueGetter(ByVal row As Integer, ByVal col As Integer) As Object
            If Me._Worksheet Is Nothing Then
                Throw New System.IO.IOException("Worksheet not activated")
            End If
            If row <= 0 Then
                Throw New ArgumentOutOfRangeException(NameOf(row), "row number must be a positive")
            End If
            If col <= 0 Then
                Throw New ArgumentOutOfRangeException(NameOf(col), "column number must be positive")
            End If
            Return If(row <= Me._Worksheet.Rows.Count AndAlso col <= Me._Worksheet.Columns.Count,
                Parser.Parse(Me._Worksheet.Rows(row - 1).Item(col - 1)),
                DBNull.Value)
        End Function

        ''' <summary> Returns the value of the given cell as a Nullable Boolean type. </summary>
        ''' <remarks> Excel rows and columns are one-based. </remarks>
        ''' <exception cref="System.IO.IOException"> Thrown when an IO failure occurred. </exception>
        ''' <param name="row">          Specifies a one-based row number. </param>
        ''' <param name="col">          Specifies a one-based column number. </param>
        ''' <param name="defaultValue"> Specifies a default value. </param>
        ''' <returns> True if it succeeds, false if it fails. </returns>
        Public Overrides Function CellValueGetter(ByVal row As Integer, ByVal col As Integer, ByVal defaultValue As Boolean?) As Boolean?

            If Me._Worksheet Is Nothing Then
                Throw New System.IO.IOException("Worksheet not activated")
            End If
            Return Parser.Parse(Me.CellValueGetter(row, col), defaultValue)

        End Function

        ''' <summary> Returns the value of the specified cell as a Boolean type. </summary>
        ''' <remarks> Excel rows and columns are one-based. </remarks>
        ''' <param name="row">          Specifies a one-based row number. </param>
        ''' <param name="col">          Specifies a one-based column number. </param>
        ''' <param name="defaultValue"> Specifies a default value. </param>
        ''' <returns> True if it succeeds, false if it fails. </returns>
        Public Overrides Function CellValueGetter(ByVal row As Integer, ByVal col As Integer, ByVal defaultValue As Boolean) As Boolean
            Return Me.CellValueGetter(row, col, New Boolean?(defaultValue)).Value
        End Function

        ''' <summary> Returns the value of the specified cell as a Nullable Date type. </summary>
        ''' <remarks> Excel rows and columns are one-based. </remarks>
        ''' <param name="row">          Specifies a one-based row number. </param>
        ''' <param name="col">          Specifies a one-based column number. </param>
        ''' <param name="defaultValue"> Specifies a default value. </param>
        ''' <returns> A DateTime? </returns>
        Public Overrides Function CellValueGetter(ByVal row As Integer, ByVal col As Integer, ByVal defaultValue As DateTime?) As DateTime?

            Dim value As Double? = Me.CellValueGetter(row, col, New Double?)
            Return If(value.HasValue, DateTime.FromOADate(value.Value), defaultValue)

        End Function

        ''' <summary>
        ''' Returns the value of the specified cell as a <see cref="T:DateTime">DateTime</see> type.
        ''' </summary>
        ''' <remarks> Excel rows and columns are one-based. </remarks>
        ''' <param name="row">          Specifies a one-based row number. </param>
        ''' <param name="col">          Specifies a one-based column number. </param>
        ''' <param name="defaultValue"> Specifies a default value. </param>
        ''' <returns> A DateTime. </returns>
        Public Overrides Function CellValueGetter(ByVal row As Integer, ByVal col As Integer, ByVal defaultValue As DateTime) As DateTime
            Return Me.CellValueGetter(row, col, New DateTime?(defaultValue)).Value
        End Function

        ''' <summary> Returns the value of the specified cell as a Nullable Double type. </summary>
        ''' <remarks> Excel rows and columns are one-based. </remarks>
        ''' <exception cref="System.IO.IOException"> Thrown when an IO failure occurred. </exception>
        ''' <param name="row">          Specifies a one-based row number. </param>
        ''' <param name="col">          Specifies a one-based column number. </param>
        ''' <param name="defaultValue"> Specifies a default value. </param>
        ''' <returns> A Double? </returns>
        Public Overrides Function CellValueGetter(ByVal row As Integer, ByVal col As Integer, ByVal defaultValue As Double?) As Double?

            If Me._Worksheet Is Nothing Then
                Throw New System.IO.IOException("Worksheet not activated")
            End If
            Return Parser.Parse(Me.CellValueGetter(row, col), defaultValue)

        End Function

        ''' <summary> Returns the value of the specified cell as a double type. </summary>
        ''' <remarks> Excel rows and columns are one-based. </remarks>
        ''' <param name="row">          Specifies a one-based row number. </param>
        ''' <param name="col">          Specifies a one-based column number. </param>
        ''' <param name="defaultValue"> Specifies a default value. </param>
        ''' <returns> A Double. </returns>
        Public Overrides Function CellValueGetter(ByVal row As Integer, ByVal col As Integer, ByVal defaultValue As Double) As Double
            Return Me.CellValueGetter(row, col, New Double?(defaultValue)).Value
        End Function

        ''' <summary> Returns the value of the specified cell as a Nullable Integer type. </summary>
        ''' <remarks> Excel rows and columns are one-based. </remarks>
        ''' <param name="row">          Specifies a one-based row number. </param>
        ''' <param name="col">          Specifies a one-based column number. </param>
        ''' <param name="defaultValue"> Specifies a default value. </param>
        ''' <returns> An Integer? </returns>
        Public Overrides Function CellValueGetter(ByVal row As Integer, ByVal col As Integer, ByVal defaultValue As Integer?) As Integer?

            Dim value As Double? = Me.CellValueGetter(row, col, New Double?)
            Return If(value.HasValue, Convert.ToInt32(value.Value), defaultValue)

        End Function

        ''' <summary> Returns the value of the specified cell as a Integer type. </summary>
        ''' <remarks> Excel rows and columns are one-based. </remarks>
        ''' <param name="row">          Specifies a one-based row number. </param>
        ''' <param name="col">          Specifies a one-based column number. </param>
        ''' <param name="defaultValue"> Specifies a default value. </param>
        ''' <returns> An Integer. </returns>
        Public Overrides Function CellValueGetter(ByVal row As Integer, ByVal col As Integer, ByVal defaultValue As Integer) As Integer
            Return Me.CellValueGetter(row, col, New Integer?(defaultValue)).Value
        End Function

        ''' <summary> Returns a cell value or default value of String type. </summary>
        ''' <remarks> Excel rows and columns are one-based. </remarks>
        ''' <exception cref="System.IO.IOException"> Thrown when an IO failure occurred. </exception>
        ''' <param name="row">          Specifies a one-based row number. </param>
        ''' <param name="col">          Specifies a one-based column number. </param>
        ''' <param name="defaultValue"> Specifies a default value. </param>
        ''' <returns> A String. </returns>
        Public Overrides Function CellValueGetter(ByVal row As Integer, ByVal col As Integer, ByVal defaultValue As String) As String
            If Me._Worksheet Is Nothing Then
                Throw New System.IO.IOException("Worksheet not activated")
            End If
            Return Parser.Parse(Me.CellValueGetter(row, col), defaultValue)
        End Function

        ''' <summary> Returns <c>True</c> if the specified cell includes a DBNull value. </summary>
        ''' <remarks> Excel rows and columns are one-based. </remarks>
        ''' <exception cref="System.IO.IOException"> Thrown when an IO failure occurred. </exception>
        ''' <param name="row"> Specifies a one-based row number. </param>
        ''' <param name="col"> Specifies a one-based column number. </param>
        ''' <returns> True if database null, false if not. </returns>
        Public Overrides Function IsDBNull(ByVal row As Integer, ByVal col As Integer) As Boolean

            If Me._Worksheet Is Nothing Then
                Throw New System.IO.IOException("Worksheet not activated")
            End If
            Return Parser.IsDBNull(Me.CellValueGetter(row, col))

        End Function

        ''' <summary> Returns <c>True</c> if the specified cell is empty. </summary>
        ''' <remarks> Excel rows and columns are one-based. </remarks>
        ''' <exception cref="System.IO.IOException"> Thrown when an IO failure occurred. </exception>
        ''' <param name="row"> Specifies a one-based row number. </param>
        ''' <param name="col"> Specifies a one-based column number. </param>
        ''' <returns> True if empty, false if not. </returns>
        Public Overrides Function IsEmpty(ByVal row As Integer, ByVal col As Integer) As Boolean
            If Me._Worksheet Is Nothing Then
                Throw New System.IO.IOException("Worksheet not activated")
            End If
            Return Parser.IsEmpty(Me.CellValueGetter(row, col))
        End Function

        ''' <summary>
        ''' Returns <c>True</c> if the specified cell is empty or includes a DBNull value.
        ''' </summary>
        ''' <remarks> Excel rows and columns are one-based. </remarks>
        ''' <param name="row"> Specifies a one-based row number. </param>
        ''' <param name="col"> Specifies a one-based column number. </param>
        ''' <returns> True if the database null or is empty, false if not. </returns>
        Public Overrides Function IsDBNullOrEmpty(ByVal row As Integer, ByVal col As Integer) As Boolean
            Return Me.IsDBNull(row, col) OrElse Me.IsEmpty(row, col)
        End Function

#End Region

    End Class

End Namespace
