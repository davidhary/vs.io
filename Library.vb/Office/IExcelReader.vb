Namespace Office

    ''' <summary> Defines an interface for reading excel workbooks. </summary>
    ''' <remarks>
    ''' (c) 2009 Integrated Scientific Resources, Inc. All rights reserved. <para>
    ''' Licensed under The MIT License.</para><para>
    ''' David, 01/07/09, 1.2.3294.x. </para>
    ''' </remarks>
    Public Interface IExcelReader

        Inherits IDisposable

        ''' <summary> Gets or sets the name of the last file that was read. </summary>
        ''' <value> The full pathname of the file file. </value>
        ReadOnly Property FilePathName() As String

        ''' <summary> Reads all worksheets from the Excel file. </summary>
        ''' <param name="filePathName"> . </param>
        ''' <returns> True if it succeeds, false if it fails. </returns>
        Function ReadWorkbook(ByVal filePathName As String) As Boolean

        ''' <summary> Returns the name of the currently selected worksheet. </summary>
        ''' <value> The name of the current worksheet. </value>
        ReadOnly Property CurrentWorksheetName() As String

        ''' <summary> Selects a worksheet by name. </summary>
        ''' <param name="name"> The name. </param>
        ''' <returns> True if it succeeds, false if it fails. </returns>
        Function ActivateWorksheet(ByVal name As String) As Boolean

        ''' <summary> Returns a cell value from the work sheet or DB NUll. </summary>
        ''' <param name="row"> Specifies a row number. </param>
        ''' <param name="col"> Specifies a column number. </param>
        ''' <returns> True if it succeeds, false if it fails. </returns>
        Function CellValueGetter(ByVal row As Integer, ByVal col As Integer) As Object

        ''' <summary> Returns the value of the given cell as a Nullable Boolean type. </summary>
        ''' <param name="row">          Specifies a row number. </param>
        ''' <param name="col">          Specifies a column number. </param>
        ''' <param name="defaultValue"> Specifies a default value. </param>
        ''' <returns> True if it succeeds, false if it fails. </returns>
        Function CellValueGetter(ByVal row As Integer, ByVal col As Integer, ByVal defaultValue As Boolean?) As Boolean?

        ''' <summary> Returns the value of the specified cell as a Boolean type. </summary>
        ''' <param name="row">          Specifies a row number. </param>
        ''' <param name="col">          Specifies a column number. </param>
        ''' <param name="defaultValue"> Specifies a default value. </param>
        ''' <returns> True if it succeeds, false if it fails. </returns>
        Function CellValueGetter(ByVal row As Integer, ByVal col As Integer, ByVal defaultValue As Boolean) As Boolean

        ''' <summary> Returns the value of the specified cell as a Nullable Date type. </summary>
        ''' <param name="row">          Specifies a row number. </param>
        ''' <param name="col">          Specifies a column number. </param>
        ''' <param name="defaultValue"> Specifies a default value. </param>
        ''' <returns> A DateTime? </returns>
        Function CellValueGetter(ByVal row As Integer, ByVal col As Integer, ByVal defaultValue As DateTime?) As DateTime?

        ''' <summary> Returns the value of the specified cell as a Date type. </summary>
        ''' <param name="row">          Specifies a row number. </param>
        ''' <param name="col">          Specifies a column number. </param>
        ''' <param name="defaultValue"> Specifies a default value. </param>
        ''' <returns> A DateTime. </returns>
        Function CellValueGetter(ByVal row As Integer, ByVal col As Integer, ByVal defaultValue As DateTime) As DateTime

        ''' <summary> Returns the value of the specified cell as a Nullable Double type. </summary>
        ''' <param name="row">          Specifies a row number. </param>
        ''' <param name="col">          Specifies a column number. </param>
        ''' <param name="defaultValue"> Specifies a default value. </param>
        ''' <returns> A Double? </returns>
        Function CellValueGetter(ByVal row As Integer, ByVal col As Integer, ByVal defaultValue As Double?) As Double?

        ''' <summary> Returns the value of the specified cell as a Double type. </summary>
        ''' <param name="row">          Specifies a row number. </param>
        ''' <param name="col">          Specifies a column number. </param>
        ''' <param name="defaultValue"> Specifies a default value. </param>
        ''' <returns> A Double. </returns>
        Function CellValueGetter(ByVal row As Integer, ByVal col As Integer, ByVal defaultValue As Double) As Double

        ''' <summary> Returns the value of the specified cell as a Nullable Integer type. </summary>
        ''' <param name="row">          Specifies a row number. </param>
        ''' <param name="col">          Specifies a column number. </param>
        ''' <param name="defaultValue"> Specifies a default value. </param>
        ''' <returns> An Integer? </returns>
        Function CellValueGetter(ByVal row As Integer, ByVal col As Integer, ByVal defaultValue As Integer?) As Integer?

        ''' <summary> Returns the value of the specified cell as a Integer type. </summary>
        ''' <param name="row">          Specifies a row number. </param>
        ''' <param name="col">          Specifies a column number. </param>
        ''' <param name="defaultValue"> Specifies a default value. </param>
        ''' <returns> An Integer. </returns>
        Function CellValueGetter(ByVal row As Integer, ByVal col As Integer, ByVal defaultValue As Integer) As Integer

        ''' <summary> Returns a cell value or default value of String type. </summary>
        ''' <param name="row">          Specifies a row number. </param>
        ''' <param name="col">          Specifies a column number. </param>
        ''' <param name="defaultValue"> Specifies a default value. </param>
        ''' <returns> A String. </returns>
        Function CellValueGetter(ByVal row As Integer, ByVal col As Integer, ByVal defaultValue As String) As String

        ''' <summary> Gets or sets the left Column. </summary>
        ''' <value> The current worksheet used range column number. </value>
        ReadOnly Property CurrentWorksheetUsedRangeColumnNumber() As Integer

        ''' <summary> Gets or sets the top row. </summary>
        ''' <value> The current worksheet used range row number. </value>
        ReadOnly Property CurrentWorksheetUsedRangeRowNumber() As Integer

        ''' <summary> Returns <c>True</c> if the specified cell includes a DBNull value. </summary>
        ''' <param name="row"> Specifies a row number. </param>
        ''' <param name="col"> Specifies a column number. </param>
        ''' <returns> True if database null, false if not. </returns>
        Function IsDBNull(ByVal row As Integer, ByVal col As Integer) As Boolean

        ''' <summary> Returns <c>True</c> if the specified cell is empty. </summary>
        ''' <param name="row"> Specifies a row number. </param>
        ''' <param name="col"> Specifies a column number. </param>
        ''' <returns> True if empty, false if not. </returns>
        Function IsEmpty(ByVal row As Integer, ByVal col As Integer) As Boolean

        ''' <summary>
        ''' Returns <c>True</c> if the specified cell is empty or includes a DBNull value.
        ''' </summary>
        ''' <param name="row"> Specifies a row number. </param>
        ''' <param name="col"> Specifies a column number. </param>
        ''' <returns> True if the database null or is empty, false if not. </returns>
        Function IsDBNullOrEmpty(ByVal row As Integer, ByVal col As Integer) As Boolean

    End Interface

End Namespace
