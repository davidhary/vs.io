''' <summary> This is the base class for the Delimited file class library. </summary>
''' <remarks>
''' (c) 2004 Integrated Scientific Resources, Inc. All rights reserved. <para>
''' Licensed under The MIT License. </para><para>  
''' David, 03/04/04, 1.0.1524.x. </para>
''' </remarks>
Public MustInherit Class DelimitedFileBase

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Constructs this class. </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    Protected Sub New()
        MyBase.New()
        Me._FilePathName = String.Empty
        Me._FileExtension = ".CSV"
        Me._FileDialogFilter = "Text files (*.txt;*.csv;*.log)|*.txt;*.csv;*.log|All files (*.*)|*.*"
        Me._FileDialogTitle = "Select File"
    End Sub

#End Region

#Region " FILE DIALOG "

    ''' <summary> Gets or sets the file dialog filter. </summary>
    ''' <value> The file dialog filter. </value>
    Public Property FileDialogFilter As String

    ''' <summary> Gets or sets the file dialog title. </summary>
    ''' <value> The file dialog title. </value>
    Public Property FileDialogTitle As String

    ''' <summary> Opens the File dialog box and gets a file name. </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    ''' <returns> <c>True</c> of selected a file; <c>False</c> if user canceled. </returns>
    Public MustOverride Function OpenFileDialog() As Boolean

    ''' <summary> Try find file. </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    ''' <param name="fileName"> Filename of the file. </param>
    ''' <returns> A String if file found or empty if not. </returns>
    Public Shared Function TryFindFile(ByVal fileName As String) As String
        Return Core.OpenFileDialog.TryFindFile(fileName)
    End Function

#End Region

#Region " OPEN AND CLOSE "

    ''' <summary> opens the file for input. </summary>
    ''' <remarks> Use this method to open the instance. </remarks>
    <System.Security.Permissions.FileIOPermission(Security.Permissions.SecurityAction.Demand, Unrestricted:=True)>
    Public Sub Open()
        Try
            ' open the file for reading
            Me.FileNumber = NewFileNumber()
            ' TO_DO: Use task; See VI Session Base Await Service request
            Dim trialCount As Integer = 10
            Do While trialCount > 0
                trialCount -= 1
                Try
                    Microsoft.VisualBasic.FileOpen(Me.FileNumber, Me.FilePathName, Microsoft.VisualBasic.OpenMode.Input)
                    trialCount = 0
                    Exit Do
                Catch
                    If trialCount = 1 Then Throw
                End Try
                If trialCount > 0 Then
                    My.MyLibrary.DoEvents()
                    My.MyLibrary.Delay(100)
                End If
            Loop
        Catch
            ' close to meet strong guarantees
            Try : Me.Close() : Finally : End Try
            Throw
        Finally
            My.MyLibrary.DoEvents()
        End Try
    End Sub

    ''' <summary> Open for output. </summary>
    ''' <remarks> Use this method to open the instance.  The method Returns <c>True</c> if success or
    ''' false if it failed opening the file. </remarks>
    ''' <param name="append"> A Boolean expression that specifies if the new data will append to an
    ''' existing file or if a new file will be Created possibly over-writing an existing file. </param>
    <System.Security.Permissions.FileIOPermission(Security.Permissions.SecurityAction.Demand, unrestricted:=True)>
    Public Sub [Open](ByVal append As Boolean)

        Try
            Me.FileNumber = NewFileNumber
            ' TO_DO: Use task; See VI Session Base Await Service request
            Dim trialCount As Integer = 10
            Do While trialCount > 0
                trialCount -= 1
                Try
                    If append Then
                        Microsoft.VisualBasic.FileOpen(Me.FileNumber, Me.FilePathName, Microsoft.VisualBasic.OpenMode.Append)
                    Else
                        Microsoft.VisualBasic.FileOpen(Me.FileNumber, Me.FilePathName, Microsoft.VisualBasic.OpenMode.Output)
                    End If
                    trialCount = 0
                    Exit Do
                Catch
                    If trialCount = 1 Then Throw
                End Try
                If trialCount > 0 Then
                    My.MyLibrary.DoEvents()
                    My.MyLibrary.Delay(100)
                End If
            Loop
        Catch
            ' close to meet strong guarantees
            Try : Me.Close() : Finally : End Try
            Throw
        Finally
            My.MyLibrary.DoEvents()
        End Try
    End Sub

    ''' <summary> Open for output. </summary>
    ''' <param name="openMode"> Specifies how the file is to be opened. </param>
    ''' <remarks> Use this method to open the instance.  The method Returns <c>True</c> if success or
    ''' false if it failed opening the file. </remarks>
    <System.Security.Permissions.FileIOPermission(Security.Permissions.SecurityAction.Demand, Unrestricted:=True)>
    Public Sub [Open](ByVal openMode As Microsoft.VisualBasic.OpenMode)

        Try
            Me.FileNumber = NewFileNumber
            ' TO_DO: Use task; See VI Session Base Await Service request
            Dim trialCount As Integer = 10
            Do While trialCount > 0
                trialCount -= 1
                Try
                    Microsoft.VisualBasic.FileOpen(Me.FileNumber, Me.FilePathName, openMode)
                    trialCount = 0
                    Exit Do
                Catch
                    If trialCount = 1 Then Throw
                End Try
                If trialCount > 0 Then
                    My.MyLibrary.DoEvents()
                    My.MyLibrary.Delay(100)
                End If
            Loop
        Catch
            ' close to meet strong guarantees
            Try : Me.Close() : Finally : End Try
            Throw
        Finally
            My.MyLibrary.DoEvents()
        End Try
    End Sub

    ''' <summary> Closes the instance. </summary>
    ''' <remarks> Use this method to close the instance.  The method Returns <c>True</c> if success or
    ''' false if it failed closing the instance. </remarks>
    <System.Security.Permissions.FileIOPermission(Security.Permissions.SecurityAction.Demand, unrestricted:=True)>
    Public Overridable Sub [Close]()
        ' close the file if not closed
        If Me.IsOpen() Then
            Microsoft.VisualBasic.FileClose(Me._FileNumber)
            ' zero the file number to indicate that the file is closed.
            Me._FileNumber = 0
        End If
    End Sub

#End Region

#Region " IO "

    ''' <summary> Determines whether the specified folder path is writable. </summary>
    ''' <remarks>
    ''' Uses a temporary random file name to test if the file can be created. The file is deleted
    ''' thereafter.
    ''' </remarks>
    ''' <param name="path"> The path. </param>
    ''' <returns> <c>True</c> if the specified path is writable; otherwise, <c>False</c>. </returns>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Public Shared Function IsFolderWritable(ByVal path As String) As Boolean
        Dim filePath As String = String.Empty
        Dim affirmative As Boolean = False
        Try
            filePath = System.IO.Path.Combine(path, System.IO.Path.GetRandomFileName())
            Using s As System.IO.FileStream = System.IO.File.Open(filePath, System.IO.FileMode.OpenOrCreate)
            End Using
            affirmative = True
        Catch
        Finally
            ' SS reported an exception from this test possibly indicating that Windows allowed writing the file 
            ' by failed report deletion. Or else, Windows raised another exception type.
            Try
                If System.IO.File.Exists(filePath) Then
                    System.IO.File.Delete(filePath)
                End If
            Catch
            End Try
        End Try
        Return affirmative
    End Function

    ''' <summary> Selects the default file path. </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    ''' <returns>
    ''' The default file path: either the application folder or the user documents folder.
    ''' </returns>
    Public Shared Function DefaultFolderPath() As String
        Dim candidatePath As String = My.Application.Info.DirectoryPath
        If Not IsFolderWritable(candidatePath) Then
            candidatePath = My.Computer.FileSystem.SpecialDirectories.MyDocuments
        End If
        Return candidatePath
    End Function

#End Region

#Region " SAFE DELETE "

    ''' <summary> Deletes the file described by fileName. </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    ''' <param name="fileName"> Filename of the file. </param>
    Public Shared Sub DeleteFile(ByVal fileName As String)
        If My.Computer.FileSystem.FileExists(fileName) Then
            My.Computer.FileSystem.DeleteFile(fileName)
            My.MyLibrary.DoEvents()
            My.MyLibrary.Delay(100)
        End If
    End Sub

#End Region

#Region " PROPERTIES "

    ''' <summary> Gets or sets the file extension. </summary>
    ''' <value> The file extension. </value>
    Public Property FileExtension As String

    ''' <summary> Full pathname of the file. </summary>
    Private _FilePathName As String

    ''' <summary> Gets or sets the file name. </summary>
    ''' <remarks> Use this property to get or set the file name. </remarks>
    ''' <value> <c>FilePathName</c> is a String property. </value>
    Public Property FilePathName() As String
        Get
            If Me._FilePathName.Length = 0 Then
                ' set default file name if empty.
                Me._FilePathName = System.IO.Path.Combine(DefaultFolderPath,
                                                          My.Application.Info.AssemblyName & Me.FileExtension)
            End If
            Return Me._FilePathName
        End Get
        Set(ByVal value As String)
            Me._FilePathName = value
        End Set
    End Property

    ''' <summary> Gets the file number. </summary>
    ''' <value> <c>FileNumber</c>is an Int32 property. </value>
    Public Property FileNumber() As Integer

    ''' <summary> Returns <c>True</c> if the file is at end of file. </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    ''' <returns> <c>True</c> if end of file. </returns>
    <System.Security.Permissions.FileIOPermission(Security.Permissions.SecurityAction.Demand, Unrestricted:=True)>
    Public Function IsEndOfFile() As Boolean
        Return Microsoft.VisualBasic.EOF(Me._FileNumber)
    End Function

    ''' <summary> Returns <c>True</c> if the file is open. </summary>
    ''' <remarks> The file is open if the file number is not zero. </remarks>
    ''' <value> <c>IsOpen</c>Is a Boolean property. </value>
    Public ReadOnly Property IsOpen() As Boolean
        Get
            Return Me.FileNumber <> 0
        End Get
    End Property

    ''' <summary>
    ''' The property returns a new free file number that can be used to open a file.
    ''' </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    ''' <returns> A new free file number that can be used to open a file. </returns>
    <System.Security.Permissions.FileIOPermission(Security.Permissions.SecurityAction.Demand, Unrestricted:=True)>
    Public Shared Function NewFileNumber() As Integer
        Return Microsoft.VisualBasic.FreeFile()
    End Function

#End Region

End Class
