Imports System.Text
Imports System.Text.RegularExpressions

''' <summary>
''' Class for creating regular expressions for use in <see cref="DelimitedFileDataSet"/>
''' </summary>
''' <remarks>
''' (c) 2017 Christiaan van Bergen. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 3/27/2017,  </para><para>
''' https://www.codeproject.com/Articles/22400/Converting-text-files-CSV-to-datasets.
''' https://www.codeproject.com/script/Membership/View.aspx?mid=325286 </para>
''' </remarks>
Public Class RegexColumnBuilder

    ''' <summary>
    ''' Adds a column to the collection of columns from which the regular expression will be created.
    ''' </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    ''' <param name="columnName"> name of the column. </param>
    ''' <param name="delimiter">  column delimiter. </param>
    Public Sub AddColumn(ByVal columnName As String, ByVal delimiter As Char)
        Me.AddColumn(columnName, String.Format("[^{0}]+", delimiter), String.Empty)
        Me.Delimiter = delimiter.ToString()
    End Sub

    ''' <summary>
    ''' Adds a column to the collection of columns from which the regular expression will be created.
    ''' </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    ''' <param name="columnName"> name of the column. </param>
    ''' <param name="delimiter">  column delimiter. </param>
    ''' <param name="columnType"> RegexColumnType of this column. </param>
    Public Sub AddColumn(ByVal columnName As String, ByVal delimiter As Char, ByVal columnType As RegexColumnType)
        Me.AddColumn(columnName, String.Format("[^{0}]+", delimiter), String.Empty, columnType)
        Me.Delimiter = delimiter.ToString()
    End Sub

    ''' <summary>
    ''' Adds a column to the collection of columns from which the regular expression will be created.
    ''' </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    ''' <param name="columnName"> name of the column. </param>
    ''' <param name="length">     amount of characters this column has. </param>
    Public Sub AddColumn(ByVal columnName As String, ByVal length As Integer)
        Me.AddColumn(columnName, ".{" & length & "}", String.Empty)
    End Sub

    ''' <summary>
    ''' Adds a column to the collection of columns from which the regular expression will be created.
    ''' </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    ''' <param name="columnName"> name of the column. </param>
    ''' <param name="length">     amount of characters this column has. </param>
    ''' <param name="columnType"> RegexColumnType of this column. </param>
    Public Sub AddColumn(ByVal columnName As String, ByVal length As Integer, ByVal columnType As RegexColumnType)
        Me.AddColumn(columnName, ".{" & length & "}", String.Empty, columnType)
    End Sub

    ''' <summary>
    ''' Adds a column to the collection of columns from which the regular expression will be created.
    ''' </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    ''' <param name="columnName">  name of the column. </param>
    ''' <param name="columnRegex"> regular expression for capturing the value of this column. </param>
    Public Sub AddColumn(ByVal columnName As String, ByVal columnRegex As String)
        Me.AddColumn(columnName, columnRegex, String.Empty)
    End Sub

    ''' <summary>
    ''' Adds a column to the collection of columns from which the regular expression will be created.
    ''' </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    ''' <param name="columnName">  name of the column. </param>
    ''' <param name="columnRegex"> regular expression for capturing the value of this column. </param>
    ''' <param name="columnType">  RegexColumnType of this column. </param>
    Public Sub AddColumn(ByVal columnName As String, ByVal columnRegex As String, ByVal columnType As RegexColumnType)
        Me.AddColumn(columnName, columnRegex, String.Empty, columnType)
    End Sub

    ''' <summary>
    ''' Adds a column to the collection of columns from which the regular expression will be created.
    ''' </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    ''' <param name="columnName">    name of the column. </param>
    ''' <param name="columnRegex">   regular expression for capturing the value of this column. </param>
    ''' <param name="trailingRegex"> regular expression for any data not to be captured for this
    '''                              column. </param>
    Public Sub AddColumn(ByVal columnName As String, ByVal columnRegex As String, ByVal trailingRegex As String)
        Me._Columns.Add(New RegexColumn(columnName, columnRegex, trailingRegex))
    End Sub

    ''' <summary>
    ''' Adds a column to the collection of columns from which the regular expression will be created.
    ''' </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    ''' <param name="columnName">    name of the column. </param>
    ''' <param name="columnRegex">   regular expression for capturing the value of this column. </param>
    ''' <param name="trailingRegex"> regular expression for any data not to be captured for this
    '''                              column. </param>
    ''' <param name="columnType">    RegexColumnType of this column. </param>
    Public Sub AddColumn(ByVal columnName As String, ByVal columnRegex As String, ByVal trailingRegex As String, ByVal columnType As RegexColumnType)
        Me._Columns.Add(New RegexColumn(columnName, columnRegex, trailingRegex, columnType))
    End Sub

    ''' <summary> The columns. </summary>
    Private ReadOnly _Columns As New List(Of RegexColumn)()

    ''' <summary> Get the collection of created RegexColumns. </summary>
    ''' <value> The columns. </value>
    Public ReadOnly Property Columns() As IEnumerable(Of RegexColumn)
        Get
            Return Me._Columns
        End Get
    End Property

    ''' <summary>
    ''' A delimiter string that will be appended after each column expression (including any trailing
    ''' regular expression)
    ''' </summary>
    ''' <value> The delimiter. </value>
    Public Property Delimiter() As String = String.Empty

    ''' <summary>
    ''' Indicates whether the regular expression will start searching at each beginning of a
    ''' line/string default is set to true.
    ''' </summary>
    ''' <value> The start at begin of string. </value>
    Public Property StartAtBeginOfString() As Boolean = True

    ''' <summary>
    ''' Indicates whether the regular expression will end searching at each end of a line/string
    ''' default is set to true.
    ''' </summary>
    ''' <value> The end at end of string. </value>
    Public Property EndAtEndOfString() As Boolean = True

    ''' <summary>
    ''' creates a regular expression string based on the added columns and optional delimiter.
    ''' </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    ''' <returns> regular expression for use in TextFileDataSet. </returns>
    Public Function CreateRegularExpressionString() As String
        Dim builder As New StringBuilder()
        If Me.StartAtBeginOfString Then
            builder.Append("^")
        End If
        For i As Integer = 0 To Me.Columns.Count - 1
            builder.Append("(?<")
            builder.Append(Me.Columns(i).ColumnName)
            builder.Append(">")
            builder.Append(Me.Columns(i).Regex)
            builder.Append(")")
            builder.Append(Me.Columns(i).TrailingRegex)
            If i < Me.Columns.Count - 1 Then
                builder.Append(Me.Delimiter)
            End If
        Next i
        If Me.EndAtEndOfString Then
            builder.Append("$")
        End If
        Return builder.ToString()
    End Function

    ''' <summary>
    ''' creates a regular expression based on the added columns and optional delimiter.
    ''' </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    ''' <returns> The new regular expression. </returns>
    Public Function CreateRegularExpression() As Regex
        Return New Regex(Me.CreateRegularExpressionString())
    End Function

    ''' <summary>
    ''' creates a regular expression based on the added columns and optional delimiter.
    ''' </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    ''' <param name="options"> . </param>
    ''' <returns> The new regular expression. </returns>
    Public Function CreateRegularExpression(ByVal options As RegexOptions) As Regex
        Return New Regex(Me.CreateRegularExpressionString(), options)
    End Function
End Class

''' <summary> Class for defining a regular expression column. </summary>
''' <remarks>
''' (c) 2017 Christiaan van Bergen. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 3/27/2017,  </para><para>
''' https://www.codeproject.com/Articles/22400/Converting-text-files-CSV-to-datasets.
''' https://www.codeproject.com/script/Membership/View.aspx?mid=325286 </para>
''' </remarks>
Public Class RegexColumn

    ''' <summary> Constructor. </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    ''' <param name="columnName">    name of the column. </param>
    ''' <param name="regex">         regular expression for capturing the value of this column. </param>
    ''' <param name="trailingRegex"> regular expression for any data not to be captured for this
    '''                              column. </param>
    Public Sub New(ByVal columnName As String, ByVal regex As String, ByVal trailingRegex As String)
        MyBase.New()
        Me.Init(columnName, regex, trailingRegex, RegexColumnType.String)
    End Sub

    ''' <summary> Constructor. </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    ''' <param name="columnName">    name of the column. </param>
    ''' <param name="regex">         regular expression for capturing the value of this column. </param>
    ''' <param name="trailingRegex"> regular expression for any data not to be captured for this
    '''                              column. </param>
    ''' <param name="columnType">    Type of this column. </param>
    Public Sub New(ByVal columnName As String, ByVal regex As String, ByVal trailingRegex As String, ByVal columnType As RegexColumnType)
        MyBase.New()
        Me.Init(columnName, regex, trailingRegex, columnType)
    End Sub

    ''' <summary> Initializes this object. </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    ''' <param name="columnName">    name of the column. </param>
    ''' <param name="regEx">         The RegEx. </param>
    ''' <param name="trailingRegex"> regular expression for any data not to be captured for this
    '''                              column. </param>
    ''' <param name="columnType">    Type of this column. </param>
    Private Sub Init(ByVal columnName As String, ByVal regEx As String, ByVal trailingRegex As String, ByVal columnType As RegexColumnType)
        Me.ColumnName = columnName
        Me.Regex = regEx
        Me.TrailingRegex = trailingRegex
        Me.ColumnType = columnType
    End Sub

    ''' <summary> Get or set the name of the column. </summary>
    ''' <value> The name of the column. </value>
    Public Property ColumnName() As String

    ''' <summary> Get or set the regular expression for capturing the value of this column. </summary>
    ''' <value> The RegEx. </value>
    Public Property Regex() As String

    ''' <summary>
    ''' Get or set the regular expression for any data not to be captured for this column.
    ''' </summary>
    ''' <value> The trailing RegEx. </value>
    Public Property TrailingRegex() As String

    ''' <summary> Get or set the Type of this column. </summary>
    ''' <value> The type of the column. </value>
    Public Property ColumnType() As RegexColumnType

    ''' <summary> Get the System.Type of this RegexColumn. </summary>
    ''' <value> The type of the column type as. </value>
    Public ReadOnly Property ColumnTypeAsType() As Type
        Get
            Return If(Me.ColumnType = RegexColumnType.Integer,
                GetType(Integer),
                If(Me.ColumnType = RegexColumnType.Double,
                GetType(Double),
                If(Me.ColumnType = RegexColumnType.Date, GetType(Date), GetType(String))))
        End Get
    End Property
End Class

''' <summary> Enumeration for certain types used in TextFileDataSet. </summary>
''' <remarks> David, 10/8/2020. </remarks>
Public Enum RegexColumnType
    ''' <summary>
    ''' Integer
    ''' </summary>
    [Integer]
    ''' <summary>
    ''' Double
    ''' </summary>
    [Double]
    ''' <summary>
    ''' String
    ''' </summary>
    [String]
    ''' <summary>
    ''' DateTime
    ''' </summary>
    [Date]
End Enum
