Imports System.Data
Imports System.IO
Imports System.Text.RegularExpressions

''' <summary>
''' Class for transforming a text-file into a dataset based on one regular expression.
''' </summary>
''' <remarks>
''' (c) 2017 Christiaan van Bergen. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 3/27/2017,  </para><para>
''' https://www.codeproject.com/Articles/22400/Converting-text-files-CSV-to-datasets.
''' https://www.codeproject.com/script/Membership/View.aspx?mid=325286 </para>
''' </remarks>
<CodeAnalysis.SuppressMessage("Usage", "CA2237:Mark ISerializable types with serializable", Justification:="<Pending>")>
Public Class DelimitedFileDataSet
    Inherits System.Data.DataSet

    ''' <summary> Name of the new. </summary>
    Private Const _NewName As String = "NewName"

    ''' <summary> The default group. </summary>
    Private Const _DefaultGroup As String = "0"

    ''' <summary> The default table name. </summary>
    Private Const _DefaultTableName As String = "Table1"

    ''' <summary> The RegEx columns. </summary>
    Private _RegexColumns As List(Of RegexColumn)

    ''' <summary>
    ''' Set the RegexColumnBuilder on setting this the columns and their RegexColumnTypes are set as
    ''' is the complete ContentExpression.
    ''' </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="value"> The value. </param>
    Public Sub ApplyColumnBuilder(ByVal value As RegexColumnBuilder)
        If value Is Nothing Then Throw New ArgumentNullException(NameOf(value))
        Me._RegexColumns = New List(Of RegexColumn)(value.Columns)
        Me.ContentExpression = value.CreateRegularExpression()
    End Sub

    ''' <summary> The content RegEx. </summary>
    Private _ContentRegex As Regex

    ''' <summary>
    ''' Regular Expression that is used for validating text lines and defining the column names of
    ''' the dataset.
    ''' </summary>
    ''' <value> The content expression. </value>
    Public Property ContentExpression() As Regex
        Get
            Return Me._ContentRegex
        End Get
        Set(ByVal value As Regex)
            If value IsNot Nothing AndAlso (Me.ContentExpression Is Nothing OrElse Not Equals(value, Me.ContentExpression)) Then
                Me._ContentRegex = value
                Me.ContentExpressionHasChanged = True
            End If
        End Set
    End Property

    ''' <summary> Gets the content expression has changed. </summary>
    ''' <value> The content expression has changed. </value>
    Private Property ContentExpressionHasChanged() As Boolean

    ''' <summary>
    ''' The regular expression used for handling a first row that could contain column headers. If
    ''' you do not have a first row with headers use UseFirstRowNamesAsColumnNames=false, or if you
    ''' do have a row with headers but don't want to use them use: SkipFirstRow=true.
    ''' </summary>
    ''' <value> The first row expression. </value>
    Public Property FirstRowExpression() As Regex

    ''' <summary>
    ''' When set to true the values in the first match made are used as column names instead of the
    ''' ones supplied in te regular expression.
    ''' </summary>
    ''' <value> A list of names of the use first row names as columns. </value>
    Public Property UseFirstRowNamesAsColumnNames() As Boolean = False

    ''' <summary> Gets the numbers of first rows to skip. </summary>
    ''' <value> The number of skip first rows. </value>
    Public Property SkipFirstRowCount() As Integer

    ''' <summary>
    ''' The name the data table in the dataset should get or the name of the data table to use when a
    ''' dataset is provided.
    ''' </summary>
    ''' <value> The name of the table. </value>
    Public Property TableName() As String

    ''' <summary> The text file to be read. </summary>
    ''' <value> The text file. </value>
    Public Property TextFile() As Stream

    ''' <summary> Gets the data table. </summary>
    ''' <value> The data table. </value>
    Private ReadOnly Property DataTable() As DataTable
        Get
            If String.IsNullOrWhiteSpace(Me.TableName) Then Me.TableName = _DefaultTableName
            If Not Me.Tables.Contains(Me.TableName) Then
                Dim newTable As New DataTable(Me.TableName) With {
                    .Locale = Globalization.CultureInfo.CurrentCulture
                }
                Me.Tables.Add(newTable)
            End If
            Return Me.Tables(Me.TableName)
        End Get
    End Property

    ''' <summary> The misreads. </summary>
    Private _Misreads As IList(Of String)

    ''' <summary> Lines in the text file that did not match the regular expression. </summary>
    ''' <value> The misreads. </value>
    Public ReadOnly Property Misreads() As IList(Of String)
        Get
            Return Me._Misreads
        End Get
    End Property

    ''' <summary> Adds the mis read. </summary>
    ''' <remarks> David, 9/7/2020. </remarks>
    ''' <param name="missRead"> The miss read. </param>
    Private Sub AddMisRead(ByVal missRead As String)
        If Me._Misreads Is Nothing Then
            Me._Misreads = New List(Of String)()
        End If
        Me._Misreads.Add(missRead)
    End Sub

    ''' <summary> Removes the data table. </summary>
    ''' <remarks> David, 9/7/2020. </remarks>
    Private Sub RemoveDataTable()
        If Me.Tables.Contains(Me.TableName) Then
            Me.Tables.Remove(Me.TableName)
        End If
    End Sub

    ''' <summary> Builds RegEx schema into data set. </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    Private Sub BuildRegexSchemaIntoDataSet()
        If Me.ContentExpression IsNot Nothing OrElse Me.ContentExpressionHasChanged Then
            Me.RemoveDataTable()
            For Each sGroup As String In Me.ContentExpression.GetGroupNames()
                If sGroup <> _DefaultGroup Then
                    Dim newDc As New DataColumn() With {
                        .DataType = GetType(String)
                    }

                    If Me._RegexColumns IsNot Nothing Then
                        For Each r As RegexColumn In Me._RegexColumns
                            If r.ColumnName = sGroup Then
                                newDc.DataType = r.ColumnTypeAsType
                                Exit For
                            End If
                        Next r
                    End If
                    newDc.ColumnName = sGroup
                    Me.DataTable.Columns.Add(newDc)
                End If
            Next sGroup
        End If
    End Sub

    ''' <summary>
    ''' Reads every line in the text file and tries to match it with the given regular expression.
    ''' Every match will be placed as a new row in the data table.
    ''' </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    ''' <param name="textFile">          . </param>
    ''' <param name="regularExpression"> . </param>
    ''' <param name="tableName">         . </param>
    Public Sub Fill(ByVal textFile As Stream, ByVal regularExpression As Regex, ByVal tableName As String)
        Me.TableName = tableName
        Me.Fill(textFile, regularExpression)
    End Sub

    ''' <summary>
    ''' Reads every line in the text file and tries to match it with the given regular expression.
    ''' Every match will be placed as a new row in the data table.
    ''' </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    ''' <param name="textFile">          . </param>
    ''' <param name="regularExpression"> . </param>
    Public Sub Fill(ByVal textFile As Stream, ByVal regularExpression As Regex)
        Me.ContentExpression = regularExpression
        Me.Fill(textFile)
    End Sub

    ''' <summary>
    ''' Reads every line in the text file and tries to match it with the given regular expression.
    ''' Every match will be placed as a new row in the data table.
    ''' </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    ''' <param name="textFile"> . </param>
    Public Sub Fill(ByVal textFile As Stream)
        Me.TextFile = textFile
        Me.Fill()
    End Sub

    ''' <summary>
    ''' Reads every line in the text file and tries to match it with the given regular expression.
    ''' Every match will be placed as a new row in the Data Table.
    ''' </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    Public Sub Fill()

        Me.BuildRegexSchemaIntoDataSet()

        If Me.TextFile Is Nothing Then Throw New InvalidOperationException("No stream available to convert to a DataSet")

        Me.TextFile.Seek(0, SeekOrigin.Begin)
        Dim sr As New StreamReader(Me.TextFile)
        Dim isFirstLine As Boolean = True
        Dim rowNumber As Integer = 0
        Do Until sr.EndOfStream
            Dim line As String = sr.ReadLine()
            rowNumber += 1

            ' allow skipping empty lines.
            If String.IsNullOrWhiteSpace(line) Then Continue Do

            If rowNumber <= Me.SkipFirstRowCount Then Continue Do
            If isFirstLine AndAlso Me.UseFirstRowNamesAsColumnNames Then
                If Me.FirstRowExpression Is Nothing Then
                    Throw New InvalidOperationException("'First Row Expression' is not set, but 'Use First Row Names As Column Names' is set to true")
                End If
                If Not Me.FirstRowExpression.IsMatch(line) Then
                    Throw New InvalidOperationException("The first row in the file does not match the 'First Row Expression'")
                End If

                Dim m As Match = Me.FirstRowExpression.Match(line)
                For Each sGroup As String In Me.FirstRowExpression.GetGroupNames()
                    If sGroup <> _DefaultGroup Then
                        Me.DataTable.Columns(sGroup).ExtendedProperties.Add(_NewName, m.Groups(sGroup).Value)
                    End If
                Next sGroup

            ElseIf Me.ContentExpression.IsMatch(line) Then
                Dim m As Match = Me.ContentExpression.Match(line)
                Dim newRow As DataRow = Me.DataTable.NewRow()
                For Each sGroup As String In Me.ContentExpression.GetGroupNames()
                    If sGroup <> _DefaultGroup Then
                        If newRow.Table.Columns(sGroup).DataType Is GetType(Integer) Then
                            newRow(sGroup) = Convert.ToInt32(m.Groups(sGroup).Value)
                        ElseIf newRow.Table.Columns(sGroup).DataType Is GetType(Double) Then
                            newRow(sGroup) = Convert.ToDouble(m.Groups(sGroup).Value)
                        ElseIf newRow.Table.Columns(sGroup).DataType Is GetType(Date) Then
                            newRow(sGroup) = Convert.ToDateTime(m.Groups(sGroup).Value)
                        Else
                            newRow(sGroup) = m.Groups(sGroup).Value
                        End If
                    End If
                Next sGroup
                Me.DataTable.Rows.Add(newRow)
            ElseIf Not isFirstLine Then
                Me.AddMisRead(line)
            End If
            isFirstLine = False
            My.MyLibrary.DoEvents()
        Loop
        If Me.UseFirstRowNamesAsColumnNames Then
            For Each column As DataColumn In Me.DataTable.Columns
                If column.ExtendedProperties.ContainsKey(_NewName) Then
                    column.ColumnName = column.ExtendedProperties(_NewName).ToString()
                End If
            Next column
        End If

    End Sub

End Class

