Imports System.Data

''' <summary> This is the writer class of the delimited file class library. </summary>
''' <remarks>
''' (c) 2004 Integrated Scientific Resources, Inc. All rights reserved. <para>
''' Licensed under The MIT License. </para><para>  
''' David, 03/04/04, 1.0.1524.x. </para>
''' </remarks>
Public Class DelimitedWriter
    Inherits DelimitedFileBase

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Constructs this class. </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    Public Sub New()
        MyBase.New()
        Me.Delimiter = DefaultDelimiter
    End Sub

#End Region

#Region " FILE DIALOG "

    ''' <summary> Opens the File Save dialog box and gets a file name. </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    ''' <returns> A Windows.Forms.DialogResult. </returns>
    Public Overrides Function OpenFileDialog() As Boolean
        Dim result As Boolean
        Dim Ofd As New isr.Core.SaveFileDialog With {
            .FileDialogTitle = Me.FileDialogTitle,
            .FilePathName = Me.FilePathName,
            .FileDialogFilter = Me.FileDialogFilter,
            .FileExtension = Me.FileExtension
        }
        result = Ofd.TrySelectFile
        If result Then Me.FilePathName = Ofd.FilePathName
        Return result
    End Function

#End Region

#Region " WRITE VALUE "

    ''' <summary> Gets or sets the tab delimited. </summary>
    ''' <value> The tab delimited. </value>
    Public Property TabDelimited As Boolean
        Get
            Return String.Equals(Me.Delimiter, Microsoft.VisualBasic.vbTab)
        End Get
        Set(value As Boolean)
            Me.Delimiter = If(value, Convert.ToChar(Microsoft.VisualBasic.vbTab), DefaultDelimiter)
        End Set
    End Property

    ''' <summary> The default delimiter. </summary>
    Public Const DefaultDelimiter As Char = ","c

    ''' <summary> Gets the delimiter. </summary>
    ''' <value> The delimiter. </value>
    Public Property Delimiter As Char

    ''' <summary> Writes an end of record (CR LF) to the file. </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    <System.Security.Permissions.FileIOPermission(Security.Permissions.SecurityAction.Demand, Unrestricted:=True)>
    Public Sub EndRecord()
        Microsoft.VisualBasic.WriteLine(Me.FileNumber)
    End Sub

    ''' <summary>
    ''' Gets or set limiting output data with commas and notation marks such as quotes for text. With
    ''' header sections, such notation needs to be turned off.
    ''' </summary>
    ''' <value> The is automatic delimited. </value>
    Public Property IsAutoDelimited() As Boolean

    ''' <summary> The last value. </summary>
    Private _LastValue As Object = String.Empty

    ''' <summary> Returns the last value written. </summary>
    ''' <value> The last value. </value>
    Public ReadOnly Property LastValue() As Object
        Get
            Return Me._LastValue
        End Get
    End Property

    ''' <summary>
    ''' Writes a value to the delimited file,  limiting output data with commas and notation marks
    ''' such as quotes for text.
    ''' </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    ''' <param name="value"> The value to write. </param>
    <System.Security.Permissions.FileIOPermission(Security.Permissions.SecurityAction.Demand, Unrestricted:=True)>
    Public Sub Write(ByVal value As Object)
        Me._LastValue = value
        If Me.IsAutoDelimited Then
            If Me.TabDelimited Then
                Microsoft.VisualBasic.Print(Me.FileNumber, value)
                Microsoft.VisualBasic.Print(Me.FileNumber, Me.Delimiter)
            Else
                Microsoft.VisualBasic.Write(Me.FileNumber, value)
            End If
        Else
            Microsoft.VisualBasic.Print(Me.FileNumber, value)
            Microsoft.VisualBasic.Print(Me.FileNumber, Me.Delimiter)
        End If
    End Sub

    ''' <summary> Writes a value to the delimited file and holds this value. </summary>
    ''' <remarks>
    ''' The end-of-record indicator controls how the object handles writing.  When on, the writer
    ''' will insert end of record character (CR LF) instead of a delimiter.
    ''' </remarks>
    ''' <param name="value">       The value to write. </param>
    ''' <param name="endOfRecord"> true to end this record after writing the value. </param>
    <System.Security.Permissions.FileIOPermission(Security.Permissions.SecurityAction.Demand, Unrestricted:=True)>
    Public Sub Write(ByVal value As Object, ByVal endOfRecord As Boolean)
        Me._LastValue = value
        If endOfRecord Then
            If Me.IsAutoDelimited Then
                Microsoft.VisualBasic.WriteLine(Me.FileNumber, value)
            Else
                Microsoft.VisualBasic.PrintLine(Me.FileNumber, value)
            End If
        Else
            Me.Write(value)
        End If
    End Sub

#End Region

#Region " WRITE ROWS "

    ''' <summary> Writes a single dimension array to the file. </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    ''' <exception cref="ArgumentNullException" guarantee="strong"> Values is Nothing. </exception>
    ''' <param name="values"> Contains the array values. </param>
    <System.Security.Permissions.FileIOPermission(Security.Permissions.SecurityAction.Demand, Unrestricted:=True)>
    Public Sub WriteRow(ByVal values() As Double)

        If values Is Nothing Then Throw New System.ArgumentNullException(NameOf(values))

        If values.Any Then
            Dim stringRecord As New System.Text.StringBuilder
            For i As Integer = values.GetLowerBound(0) To values.GetUpperBound(0)
                If i > 0 Then
                    stringRecord.Append(Me.Delimiter)
                End If
                stringRecord.Append(values(i).ToString(Globalization.CultureInfo.CurrentCulture))
            Next
            Microsoft.VisualBasic.PrintLine(Me.FileNumber, stringRecord.ToString)
        End If

    End Sub

    ''' <summary> Writes a two-dimensional array to the file. </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    ''' <exception cref="ArgumentNullException" guarantee="strong"> Values is Nothing. </exception>
    ''' <param name="values"> Contains the array values to write.  Array rows are written to the file
    '''                       as comma-separated successive lines. </param>
    <System.Security.Permissions.FileIOPermission(Security.Permissions.SecurityAction.Demand, Unrestricted:=True)>
    Public Sub WriteColumns(ByVal values(,) As Double)

        If values Is Nothing Then Throw New System.ArgumentNullException(NameOf(values))

        If values.Length > 0 Then
            For i As Integer = values.GetLowerBound(0) To values.GetUpperBound(0)
                Dim rowRecord(values.GetUpperBound(1) - values.GetLowerBound(1) + 1) As Double
                For j As Integer = values.GetLowerBound(1) To values.GetUpperBound(1)
                    rowRecord(j) = values(i, j)
                Next
                Me.WriteRow(rowRecord)
            Next
        End If

    End Sub

    ''' <summary> Writes a two-dimensional array to the file. </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    ''' <exception cref="ArgumentNullException" guarantee="strong"> Values is Nothing. </exception>
    ''' <param name="values"> Contains the array values to write.  Array rows are written to the file
    '''                       as comma-separated successive lines. </param>
    <System.Security.Permissions.FileIOPermission(Security.Permissions.SecurityAction.Demand, Unrestricted:=True)>
    Public Sub WriteRows(ByVal values(,) As Double)

        If values Is Nothing Then Throw New System.ArgumentNullException(NameOf(values))

        If values.Length > 0 Then
            For i As Integer = values.GetLowerBound(1) To values.GetUpperBound(1)
                Dim rowRecord(values.GetUpperBound(0) - values.GetLowerBound(0) + 1) As Double
                For j As Integer = values.GetLowerBound(0) To values.GetUpperBound(0)
                    rowRecord(j) = values(j, i)
                Next
                Me.WriteRow(rowRecord)
            Next
        End If

    End Sub

    ''' <summary> Writes a jagged two-dimensional array to the file. </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    ''' <exception cref="ArgumentNullException" guarantee="strong"> Values is Nothing. </exception>
    ''' <param name="values"> Contains the array values to write.  Array rows are written to the file
    '''                       as comma-separated successive lines. </param>
    <System.Security.Permissions.FileIOPermission(Security.Permissions.SecurityAction.Demand, Unrestricted:=True)>
    Public Sub WriteColumns(ByVal values()() As Double)

        If values Is Nothing Then Throw New System.ArgumentNullException(NameOf(values))

        If values.Length > 0 Then
            For j As Integer = values.GetLowerBound(1) To values.GetUpperBound(1)
                Dim rowRecord(values.GetUpperBound(0) - values.GetLowerBound(0) + 1) As Double
                For i As Integer = values.GetLowerBound(0) To values.GetUpperBound(0)
                    rowRecord(i) = values(i)(j)
                Next
                Me.WriteRow(rowRecord)
            Next
        End If

    End Sub

    ''' <summary> Writes a jagged two-dimensional array to the file in columns. </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    ''' <exception cref="ArgumentNullException" guarantee="strong"> Values is Nothing. </exception>
    ''' <param name="values"> Contains the array values to write.  Array rows are written to the file
    '''                       as comma-separated successive lines. </param>
    <System.Security.Permissions.FileIOPermission(Security.Permissions.SecurityAction.Demand, Unrestricted:=True)>
    Public Sub WriteRows(ByVal values()() As Double)

        If values Is Nothing Then Throw New System.ArgumentNullException(NameOf(values))

        If values.Length > 0 Then
            For i As Integer = values.GetLowerBound(0) To values.GetUpperBound(0)
                Me.WriteRow(values(i))
            Next
        End If
    End Sub

#End Region

#Region " WRITE DATA TABLE "

    ''' <summary> Writes a data table. </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="sourceTable">    Source table. </param>
    ''' <param name="writer">         The writer. </param>
    ''' <param name="includeHeaders"> True to include, false to exclude the headers. </param>
    Public Shared Sub WriteDataTable(ByVal sourceTable As DataTable, ByVal writer As System.IO.TextWriter, ByVal includeHeaders As Boolean)
        If writer Is Nothing Then Throw New ArgumentNullException(NameOf(writer))
        If sourceTable Is Nothing Then Throw New ArgumentNullException(NameOf(sourceTable))
        If includeHeaders Then
            Dim headerValues As IEnumerable(Of String) = sourceTable.Columns.OfType(Of DataColumn)().Select(Function(column) QuoteValue(column.ColumnName))
            writer.WriteLine(String.Join(",", headerValues))
        End If

        Dim items As IEnumerable(Of String) = Nothing
        For Each row As DataRow In sourceTable.Rows
            items = row.ItemArray.Select(Function(o) QuoteValue(o))
            'items = row.ItemArray.Select(Function(o) QuoteValue(If(o?.ToString(), String.Empty)))
            writer.WriteLine(String.Join(",", items))
        Next row
        writer.Flush()
    End Sub

    ''' <summary> Quote value. </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    ''' <param name="value"> The value to write. </param>
    ''' <returns> A String. </returns>
    Private Shared Function QuoteValue(ByVal value As Object) As String
        Return If(value Is Nothing, String.Empty, If(TypeOf (value) Is String, QuoteValue(value.ToString), value.ToString))
    End Function

    ''' <summary> Quote value. </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="value"> The value to write. </param>
    ''' <returns> A String. </returns>
    Private Shared Function QuoteValue(ByVal value As String) As String
        If value Is Nothing Then Throw New ArgumentNullException(NameOf(value))
        Return String.Concat("""", value.Replace("""", """"""), """")
    End Function

    ''' <summary> Writes a data table. </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="sourceTable">    Source table. </param>
    ''' <param name="includeHeaders"> True to include, false to exclude the headers. </param>
    Public Sub WriteDataTable(ByVal sourceTable As DataTable, ByVal includeHeaders As Boolean)
        If sourceTable Is Nothing Then Throw New ArgumentNullException(NameOf(sourceTable))
        Using writer As System.IO.StreamWriter = My.Computer.FileSystem.OpenTextFileWriter(Me.FilePathName, False)
            WriteDataTable(sourceTable, writer, includeHeaders)
        End Using
    End Sub

#End Region

End Class
