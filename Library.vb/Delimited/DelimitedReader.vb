''' <summary> This is the reader class of the delimited file class library. </summary>
''' <remarks>
''' (c) 2004 Integrated Scientific Resources, Inc. All rights reserved. <para>
''' Licensed under The MIT License. </para><para>  
''' David, 03/04/04, 1.0.1524.x. </para>
''' </remarks>
Public Class DelimitedReader
    Inherits DelimitedFileBase

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Constructs this class. </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    Public Sub New()
        MyBase.New()
        Me.Delimiter = DefaultDelimiter
    End Sub

#End Region

#Region " FILE DIALOG "

    ''' <summary> Opens the File Open dialog box and gets a file name. </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    ''' <returns> A Windows.Forms.DialogResult. </returns>
    Public Overrides Function OpenFileDialog() As Boolean
        Dim result As Boolean
        Dim Ofd As New isr.Core.OpenFileDialog With {
            .FileDialogTitle = Me.FileDialogTitle,
            .FilePathName = Me.FilePathName,
            .FileDialogFilter = Me.FileDialogFilter,
            .FileExtension = Me.FileExtension
        }
        result = Ofd.TrySelectFile
        If result Then Me._FileInfo = New System.IO.FileInfo(Me.FilePathName)
        If result Then Me.FilePathName = Ofd.FilePathName
        Return result
    End Function

    ''' <summary> Gets information describing the file. </summary>
    ''' <value> Information describing the file. </value>
    Public ReadOnly Property FileInfo As System.IO.FileInfo

    ''' <summary> Queries if a given file exists. </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    Public Function FileExists() As Boolean
        Dim fi As New System.IO.FileInfo(Me.FilePathName)
        Return fi.Exists
    End Function

#End Region

#Region " READ VALUE "

    ''' <summary> The last value. </summary>
    Private _LastValue As Object = String.Empty

    ''' <summary> Returns the last value read. </summary>
    ''' <value> The last value. </value>
    Public ReadOnly Property LastValue() As Object
        Get
            Return Me._LastValue
        End Get
    End Property

    ''' <summary> Reads a value from the delimited file. </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    ''' <exception cref="System.IO.EndOfStreamException"> Thrown when the end of the stream was
    '''                                                   unexpectedly reached. </exception>
    ''' <returns> An Object. </returns>
    <System.Security.Permissions.FileIOPermission(Security.Permissions.SecurityAction.Demand, Unrestricted:=True)>
    Public Function ReadObject() As Object
        If Me.IsEndOfFile Then
            ' if end of file, raise an exception
            Throw New System.IO.EndOfStreamException()
        Else
            Microsoft.VisualBasic.Input(Me.FileNumber, Me._LastValue)
            Return Me._LastValue
        End If
    End Function

#End Region

#Region " DELIMITED "

    ''' <summary> Gets or sets the tab delimited. </summary>
    ''' <value> The tab delimited. </value>
    Public Property TabDelimited As Boolean
        Get
            Return String.Equals(Me.Delimiter, Microsoft.VisualBasic.vbTab)
        End Get
        Set(value As Boolean)
            Me.Delimiter = If(value, Convert.ToChar(Microsoft.VisualBasic.vbTab), DefaultDelimiter)
        End Set
    End Property

    ''' <summary> The default delimiter. </summary>
    Public Const DefaultDelimiter As Char = ","c

    ''' <summary> Gets or sets the delimiter. </summary>
    ''' <value> The delimiter. </value>
    Public Property Delimiter As Char

#End Region

#Region " READ ROWS "

    ''' <summary> Skips the given line count. </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    ''' <param name="lineCount"> Number of lines. </param>
    Public Sub Skip(ByVal lineCount As Integer)
        For i As Integer = 1 To lineCount
            Microsoft.VisualBasic.LineInput(Me.FileNumber)
        Next
    End Sub

    ''' <summary> Reads the line. </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    ''' <returns> The line. </returns>
    Public Function ReadLine() As String
        Return Microsoft.VisualBasic.LineInput(Me.FileNumber)
    End Function

    ''' <summary> Returns an array of row data. </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    ''' <exception cref="ArgumentOutOfRangeException" guarantee="strong"> element count is negative. </exception>
    ''' <param name="elementCount"> Specifies the number of elements to tread. </param>
    ''' <returns> An array of <see cref="System.Double">Double values</see>. </returns>
    <System.Security.Permissions.FileIOPermission(Security.Permissions.SecurityAction.Demand, Unrestricted:=True)>
    Public Function ReadRowDouble(ByVal elementCount As Integer) As Double()

        If elementCount < 0 Then
            Dim message As String = "Array size specified as {0} must be non-negative."
            message = String.Format(Globalization.CultureInfo.CurrentCulture, message, elementCount)
            Throw New ArgumentOutOfRangeException(NameOf(elementCount), elementCount, message)
        End If

        If elementCount = 0 Then

            ' return the empty array 
            Dim data() As Double = Array.Empty(Of Double)()
            Return data

        Else

            Dim record As String = Microsoft.VisualBasic.LineInput(Me.FileNumber)
            If record.Length > 0 Then

                ' if we have data, get an array
                Dim stringValues As String() = record.Split(Me.Delimiter)

                ' allow as many elements as requested or present.
                elementCount = Math.Min(elementCount, stringValues.Length)

                ' allocate data array
                Dim data(elementCount - 1) As Double

                For i As Int32 = 0 To elementCount - 1
                    data(i) = Convert.ToDouble(stringValues(i), Globalization.CultureInfo.CurrentCulture)
                Next
                Return data

            Else

                ' return the empty array 
                Dim data() As Double = Array.Empty(Of Double)()
                Return data

            End If

        End If

    End Function

    ''' <summary> Returns an array of row data. </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    ''' <returns> The row double. </returns>
    <System.Security.Permissions.FileIOPermission(Security.Permissions.SecurityAction.Demand, Unrestricted:=True)>
    Public Function ReadRowDouble() As Double()

        Dim record As String = Microsoft.VisualBasic.LineInput(Me.FileNumber)
        If record.Length > 0 Then

            ' if we have data, get an array
            Dim stringValues As String() = record.Split(Me.Delimiter)

            ' allocate data array
            Dim data(stringValues.Length - 1) As Double

            For i As Int32 = 0 To stringValues.Length - 1
                data(i) = Convert.ToDouble(stringValues(i), Globalization.CultureInfo.CurrentCulture)
            Next
            Return data

        Else

            ' return the empty array 
            Dim data() As Double = Array.Empty(Of Double)()
            Return data

        End If

    End Function

    ''' <summary> Returns file rows as a two-dimensional array. </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    ''' <returns> The rows double array. </returns>
    <System.Security.Permissions.FileIOPermission(Security.Permissions.SecurityAction.Demand, Unrestricted:=True)>
    Public Function ReadRowsDoubleArray() As Double(,)

        Dim data(,) As Double = {}
        Dim columnIndex As Int32
        Do While Not Me.IsEndOfFile
            Dim oneDimensionValues As Double() = Me.ReadRowDouble()
            If (oneDimensionValues IsNot Nothing) AndAlso oneDimensionValues.Length > 0 Then
                If data.Length = 0 Then
                    ReDim data(oneDimensionValues.Length - 1, columnIndex)
                ElseIf data.GetUpperBound(1) < columnIndex Then
                    ReDim Preserve data(data.GetUpperBound(0), columnIndex)
                End If
                If data IsNot Nothing Then
                    For i As Int32 = 0 To Math.Min(data.GetUpperBound(0), oneDimensionValues.Length - 1)
                        data(i, columnIndex) = Convert.ToDouble(oneDimensionValues(i))
                    Next
                End If
            End If
            columnIndex += 1
        Loop
        Return data
    End Function

    ''' <summary>
    ''' Return file rows as a jagged two-dimensional array allowing each row to be of different
    ''' length.
    ''' </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    ''' <returns> The rows double. </returns>
    <System.Security.Permissions.FileIOPermission(Security.Permissions.SecurityAction.Demand, Unrestricted:=True)>
    Public Function ReadRowsDouble() As Double()()

        Dim data()() As Double = Array.Empty(Of Double())()
        Dim rowIndex As Int32
        Do While Not Me.IsEndOfFile
            Dim oneDimensionValues As Double() = Me.ReadRowDouble()
            If (oneDimensionValues IsNot Nothing) AndAlso oneDimensionValues.Length > 0 Then
                ReDim data(rowIndex)(oneDimensionValues.Length - 1)
                For i As Int32 = 0 To Math.Min(data.GetUpperBound(0), oneDimensionValues.Length - 1)
                    data(rowIndex)(i) = oneDimensionValues(i)
                Next
            End If
            rowIndex += 1
        Loop
        Return data

    End Function

    ''' <summary>
    ''' Return file rows as a jagged array allowing each row to be of different length.
    ''' </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    ''' <returns> all rows. </returns>
    Public Function ReadAllRows() As IList(Of String())
        Return Me.ReadAllRows(New String() {Me.Delimiter})
    End Function

    ''' <summary>
    ''' Return file rows as a jagged array allowing each row to be of different length.
    ''' </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    ''' <param name="skipCount"> Number of skips. </param>
    ''' <returns> all rows. </returns>
    Public Function ReadAllRows(ByVal skipCount As Integer) As IList(Of String())
        Return Me.ReadAllRows(skipCount, New String() {Me.Delimiter})
    End Function

    ''' <summary>
    ''' Return file rows as a jagged array allowing each row to be of different length.
    ''' </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    ''' <param name="delimiters"> The delimiters. </param>
    ''' <returns> all rows. </returns>
    Public Function ReadAllRows(ByVal delimiters As String()) As IList(Of String())
        If delimiters Is Nothing Then delimiters = New String() {Me.Delimiter}
        Using MyReader As New Microsoft.VisualBasic.FileIO.TextFieldParser(Me.FilePathName)
            MyReader.TextFieldType = Microsoft.VisualBasic.FileIO.FieldType.Delimited
            MyReader.Delimiters = delimiters
            Return ReadAllRows(MyReader)
        End Using

    End Function

    ''' <summary>
    ''' Return file rows as a jagged array allowing each row to be of different length.
    ''' </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    ''' <param name="skipCount">  Number of skips. </param>
    ''' <param name="delimiters"> The delimiters. </param>
    ''' <returns> all rows. </returns>
    Public Function ReadAllRows(ByVal skipCount As Integer, ByVal delimiters As String()) As IList(Of String())
        If delimiters Is Nothing Then delimiters = New String() {Me.Delimiter}
        Using MyReader As New Microsoft.VisualBasic.FileIO.TextFieldParser(Me.FilePathName)
            MyReader.TextFieldType = Microsoft.VisualBasic.FileIO.FieldType.Delimited
            MyReader.Delimiters = delimiters
            Do While skipCount > 0
                skipCount -= 1
                MyReader.ReadLine()
            Loop
            Return ReadAllRows(MyReader)
        End Using

    End Function

    ''' <summary>
    ''' Return file rows as a jagged array allowing each row to be of different length.
    ''' </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    ''' <param name="skipCount"> Number of skips. </param>
    ''' <param name="delimiter"> The delimiter. </param>
    ''' <returns> all rows. </returns>
    Public Function ReadAllRows(ByVal skipCount As Integer, ByVal delimiter As String) As IList(Of String())
        If String.IsNullOrEmpty(delimiter) Then delimiter = Me.Delimiter
        Dim delimiters As String() = New String() {delimiter}
        Using MyReader As New Microsoft.VisualBasic.FileIO.TextFieldParser(Me.FilePathName)
            MyReader.TextFieldType = Microsoft.VisualBasic.FileIO.FieldType.Delimited
            MyReader.Delimiters = delimiters
            Do While skipCount > 0
                skipCount -= 1
                MyReader.ReadLine()
            Loop
            Return ReadAllRows(MyReader)
        End Using

    End Function

    ''' <summary>
    ''' Return file rows as a jagged array allowing each row to be of different length.
    ''' </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="reader"> The reader. </param>
    ''' <returns> all rows. </returns>
    Public Shared Function ReadAllRows(ByVal reader As Microsoft.VisualBasic.FileIO.TextFieldParser) As IList(Of String())
        If reader Is Nothing Then Throw New ArgumentNullException(NameOf(reader))
        Dim l As New List(Of String())
        Dim currentRow As String()
        'Loop through all of the fields in the file. 
        'If any lines are corrupt, report an error and continue parsing. 
        While Not reader.EndOfData
            currentRow = reader.ReadFields()
            If (currentRow IsNot Nothing) AndAlso currentRow.Length > 0 Then
                l.Add(currentRow)
            End If
        End While
        Return l.ToArray
    End Function

#End Region

End Class
