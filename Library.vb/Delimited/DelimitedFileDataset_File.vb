Imports System.IO

Partial Public Class DelimitedFileDataSet

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Constructs this class. </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    Public Sub New()
        MyBase.New()
        Me._FilePathName = String.Empty
        Me._FileExtension = ".CSV"
        Me._FileDialogFilter = "Text files (*.txt;*.csv;*.log)|*.txt;*.csv;*.log|All files (*.*)|*.*"
        Me._FileDialogTitle = "Select File"
    End Sub

    ''' <summary> Creates a new DelimitedFileDataSet. </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    ''' <returns> A DelimitedFileDataSet. </returns>
    Public Shared Function Create() As DelimitedFileDataSet
        Dim ds As DelimitedFileDataSet = Nothing
        Try
            ds = New DelimitedFileDataSet
            Return ds
        Catch
            ds?.Dispose()
            Throw
        End Try
    End Function

#End Region

#Region " FILE DIALOG "

    ''' <summary> Gets or sets the file dialog filter. </summary>
    ''' <value> The file dialog filter. </value>
    Public Property FileDialogFilter As String

    ''' <summary> Gets or sets the file dialog title. </summary>
    ''' <value> The file dialog title. </value>
    Public Property FileDialogTitle As String

    ''' <summary> Opens the File Open dialog box and gets a file name. </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    ''' <returns> A Windows.Forms.DialogResult. </returns>
    Public Function OpenFileDialog() As Boolean
        Dim result As Boolean
        Dim Ofd As New isr.Core.OpenFileDialog With {
            .FileDialogTitle = Me.FileDialogTitle,
            .FilePathName = Me.FilePathName,
            .FileDialogFilter = Me.FileDialogFilter,
            .FileExtension = Me.FileExtension
        }
        result = Ofd.TrySelectFile
        If result Then Me.FilePathName = Ofd.FilePathName
        Return result
    End Function

    ''' <summary> Opens the File Save dialog box and gets a file name. </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    ''' <returns> A Windows.Forms.DialogResult. </returns>
    Public Function SaveFileDialog() As Boolean
        Dim result As Boolean
        Dim Ofd As New isr.Core.SaveFileDialog With {
            .FileDialogTitle = Me.FileDialogTitle,
            .FilePathName = Me.FilePathName,
            .FileDialogFilter = Me.FileDialogFilter,
            .FileExtension = Me.FileExtension
        }
        result = Ofd.TrySelectFile
        If result Then Me.FilePathName = Ofd.FilePathName
        Return result
    End Function

    ''' <summary> Try find file. </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    ''' <param name="fileName"> Filename of the file. </param>
    ''' <returns> A String if file found or empty if not. </returns>
    Public Shared Function TryFindFile(ByVal fileName As String) As String
        Return Core.OpenFileDialog.TryFindFile(fileName)
    End Function

#End Region

#Region " OPEN AND CLOSE "

    ''' <summary> opens the file for input. </summary>
    ''' <remarks> Use this method to open the instance. </remarks>
    <System.Security.Permissions.FileIOPermission(Security.Permissions.SecurityAction.Demand, Unrestricted:=True)>
    Public Sub Open()

        Try
            Me.TextFile = New FileStream(Me.FilePathName, FileMode.Open, FileAccess.Read)
        Catch
            ' close to meet strong guarantees
            Try : Me.Close() : Finally : End Try
            Throw
        End Try

    End Sub

    ''' <summary> Open for output. </summary>
    ''' <remarks> Use this method to open the instance.  The method Returns <c>True</c> if success or
    ''' false if it failed opening the file. </remarks>
    ''' <param name="append"> A Boolean expression that specifies if the new data will append to an
    ''' existing file or if a new file will be Created possibly over-writing an existing file. </param>
    <System.Security.Permissions.FileIOPermission(Security.Permissions.SecurityAction.Demand, Unrestricted:=True)>
    Public Sub [Open](ByVal append As Boolean)

        Try
            Me.TextFile = If(append,
                New FileStream(Me.FilePathName, FileMode.Open, FileAccess.ReadWrite),
                New FileStream(Me.FilePathName, FileMode.Open, FileAccess.Read))
        Catch
            ' close to meet strong guarantees
            Try : Me.Close() : Finally : End Try
            Throw
        End Try
    End Sub

    ''' <summary> Closes the instance. </summary>
    ''' <remarks> Use this method to close the instance.  The method Returns <c>True</c> if success or
    ''' false if it failed closing the instance. </remarks>
    <System.Security.Permissions.FileIOPermission(Security.Permissions.SecurityAction.Demand, Unrestricted:=True)>
    Public Overridable Sub [Close]()

        ' close the file if not closed
        If Me.IsOpen() Then
            Me.TextFile.Close()
        End If

    End Sub

#End Region

#Region " IO "

    ''' <summary> Determines whether the specified folder path is writable. </summary>
    ''' <remarks>
    ''' Uses a temporary random file name to test if the file can be created. The file is deleted
    ''' thereafter.
    ''' </remarks>
    ''' <param name="path"> The path. </param>
    ''' <returns> <c>True</c> if the specified path is writable; otherwise, <c>False</c>. </returns>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Public Shared Function IsFolderWritable(ByVal path As String) As Boolean
        Dim filePath As String = String.Empty
        Dim affirmative As Boolean = False
        Try
            filePath = System.IO.Path.Combine(path, System.IO.Path.GetRandomFileName())
            Using s As System.IO.FileStream = File.Open(filePath, FileMode.OpenOrCreate)
            End Using
            affirmative = True
        Catch
        Finally
            ' SS reported an exception from this test possibly indicating that Windows allowed writing the file 
            ' by failed report deletion. Or else, Windows raised another exception type.
            Try
                If File.Exists(filePath) Then
                    File.Delete(filePath)
                End If
            Catch
            End Try
        End Try
        Return affirmative
    End Function

    ''' <summary> Selects the default file path. </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    ''' <returns>
    ''' The default file path: either the application folder or the user documents folder.
    ''' </returns>
    Public Shared Function DefaultFolderPath() As String
        Dim candidatePath As String = My.Application.Info.DirectoryPath
        If Not DelimitedFileBase.IsFolderWritable(candidatePath) Then
            candidatePath = My.Computer.FileSystem.SpecialDirectories.MyDocuments
        End If
        Return candidatePath
    End Function

#End Region

#Region " PROPERTIES "

    ''' <summary> Gets or sets the file extension. </summary>
    ''' <value> The file extension. </value>
    Public Property FileExtension As String

    ''' <summary> Full pathname of the file. </summary>
    Private _FilePathName As String

    ''' <summary> Gets or sets the file name. </summary>
    ''' <remarks> Use this property to get or set the file name. </remarks>
    ''' <value> <c>FilePathName</c> is a String property. </value>
    Public Property FilePathName() As String
        Get
            If Me._FilePathName.Length = 0 Then
                ' set default file name if empty.
                Me._FilePathName = Path.Combine(DelimitedFileBase.DefaultFolderPath,
                                                          My.Application.Info.AssemblyName & Me.FileExtension)
            End If
            Return Me._FilePathName
        End Get
        Set(ByVal value As String)
            Me._FilePathName = value
        End Set
    End Property

    ''' <summary> Returns <c>True</c> if the file is open. </summary>
    ''' <remarks> The file is open if the file number is not zero. </remarks>
    ''' <value> <c>IsOpen</c>Is a Boolean property. </value>
    Public ReadOnly Property IsOpen() As Boolean
        Get
            Return Me.TextFile IsNot Nothing ' Me.FileNumber <> 0
        End Get
    End Property

    ''' <summary> Returns <c>True</c> if the file is at end of file. </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    ''' <returns> <c>True</c> if end of file. </returns>
    <System.Security.Permissions.FileIOPermission(Security.Permissions.SecurityAction.Demand, Unrestricted:=True)>
    Public Function IsEndOfFile() As Boolean
        Return Me.TextFile.Position >= Me.TextFile.Length
    End Function

    ''' <summary> Queries if a given file exists. </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    Public Function FileExists() As Boolean
        Dim fi As New System.IO.FileInfo(Me.FilePathName)
        Return fi.Exists
    End Function

#End Region

End Class

