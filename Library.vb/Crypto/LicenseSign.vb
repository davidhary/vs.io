Imports System.ComponentModel

Imports isr.Core.HashExtensions

Namespace Cryptography

    ''' <summary> Signs license tokens. </summary>
    ''' <remarks>
    ''' (c) 2010 Integrated Scientific Resources, Inc. All rights reserved. <para>
    ''' Licensed under The MIT License.</para><para>
    ''' David, 06/21/03, 1.0.1267.x. </para>
    ''' </remarks>
    Public Class LicenseSign
        Inherits isr.IO.Cryptography.TokenSignBase

#Region " CONSTRUCTION and CLEANUP "

        ''' <summary> Default constructor. </summary>
        ''' <remarks> David, 10/8/2020. </remarks>
        Public Sub New()
            MyBase.New()
            Me._SupportedTokenNames = LicenseTokenNames.AllOptions
            Me._VerifiableTokenNames = LicenseTokenNames.SerialNumber Or LicenseTokenNames.SerialNumberHash
            Me.ResourceName = "LicenseInfo"
            Me.ResourceNamespace = "Values"
            Me._ExpirationDate = Date.MaxValue
            Me._ActivationsCount = 0
            Me.Salt = "0"
        End Sub

        ''' <summary> Constructor. </summary>
        ''' <remarks> David, 10/8/2020. </remarks>
        ''' <param name="serialNumber">   The serial number. </param>
        ''' <param name="programVersion"> The program version. </param>
        ''' <param name="programOptions"> The program version. </param>
        Public Sub New(ByVal serialNumber As String, ByVal programVersion As Version, ByVal programOptions As Long)
            Me.New
            Me._SerialNumber = serialNumber
            Me._ProgramVersion = programVersion
            Me._ProgramOptions = programOptions
        End Sub

#End Region

#Region " BUILD TOKENS "

        ''' <summary> Adds tokens to the token dictionary. </summary>
        ''' <remarks> David, 10/8/2020. </remarks>
        ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
        Public Overrides Sub AddTokens()
            ' add object per the selected system token
            If (LicenseTokenNames.SerialNumber And Me.SupportedTokenNames) <> 0 Then
                Me.AddSerialNumberToken()
            Else
                Throw New InvalidOperationException($"Token of type '{LicenseTokenNames.SerialNumber}' is not supported at this time")

            End If
            If (LicenseTokenNames.SerialNumberHash And Me.SupportedTokenNames) <> 0 Then Me.AddSerialNumberHashToken()
            If (LicenseTokenNames.ProgramVersion And Me.SupportedTokenNames) <> 0 Then Me.AddProgramVersionToken()
            If (LicenseTokenNames.TopVersion And Me.SupportedTokenNames) <> 0 Then Me.AddTopVersionToken()
            If (LicenseTokenNames.ExpirationDate And Me.SupportedTokenNames) <> 0 Then Me.AddExpirationDate()
            If (LicenseTokenNames.ActivationCount And Me.SupportedTokenNames) <> 0 Then Me.AddActivationsCount()
            If (LicenseTokenNames.ProgramOptions And Me.SupportedTokenNames) <> 0 Then Me.AddProgramOptionsToken()
        End Sub

#End Region

#Region " READ AND PARSE "

        ''' <summary> Reads and parses the signed resource. </summary>
        ''' <remarks> Use this method to read the signed resource. </remarks>
        Public Overrides Sub ReadParse()
            MyBase.ReadParse()
            If (LicenseTokenNames.ProgramVersion And Me.SupportedTokenNames) <> 0 Then
                Me.ProgramVersion = New Version(Me.TokenDictionary.Item(LicenseTokenName.ProgramVersion.ToString))
            End If
            If (LicenseTokenNames.ProgramOptions And Me.SupportedTokenNames) <> 0 Then
                Me.ProgramOptions = Integer.Parse(Me.TokenDictionary.Item(LicenseTokenName.ProgramOptions.ToString))
            End If
            If (LicenseTokenNames.TopVersion And Me.SupportedTokenNames) <> 0 Then
                Me.TopVersion = New Version(Me.TokenDictionary.Item(LicenseTokenName.TopVersion.ToString))
            End If
            If (LicenseTokenNames.ExpirationDate And Me.SupportedTokenNames) <> 0 Then
                Me.ExpirationDate = Date.Parse(Me.TokenDictionary.Item(LicenseTokenName.ExpirationDate.ToString))
            End If
            If (LicenseTokenNames.ActivationCount And Me.SupportedTokenNames) <> 0 Then
                Me.ActivationsCount = Integer.Parse(Me.TokenDictionary.Item(LicenseTokenName.ActivationCount.ToString))
            End If

        End Sub


#End Region

#Region " TOKEN MANAGEMENT "

        ''' <summary> Gets or sets the supported system token names. </summary>
        ''' <remarks>
        ''' Use this property to get or set the type of token(s) the System Token will Create and sign.
        ''' </remarks>
        ''' <value>
        ''' <c>SystemTokenType</c> is a SystemTokenTypes enumerated property that can be read from or
        ''' written too (read or write).
        ''' </value>
        Public Property SupportedTokenNames() As LicenseTokenNames

        ''' <summary> Gets or sets a list of names of the verifiable tokens. </summary>
        ''' <value> A list of names of the verifiable tokens. </value>
        Public Property VerifiableTokenNames() As LicenseTokenNames

        ''' <summary> Verify tokens. </summary>
        ''' <remarks> David, 10/8/2020. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="e"> Cancel details event information. </param>
        ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        Public Overrides Function VerifyTokens(ByVal e As isr.Core.ActionEventArgs) As Boolean
            If e Is Nothing Then Throw New ArgumentNullException(NameOf(e))
            If (LicenseTokenNames.SerialNumber And Me.VerifiableTokenNames) <> 0 Then
                Me.VerifySerialNumberToken(e)
            End If
            If Not e.Failed AndAlso ((LicenseTokenNames.SerialNumberHash And Me.VerifiableTokenNames) <> 0) Then
                Me.VerifySerialNumberHashToken(e)
            End If
            Return Not e.Failed
        End Function

#Region " SERIAL NUMBER TOKEN "

        ''' <summary> Gets or sets the serial number. </summary>
        ''' <value> The serial number. </value>
        Public Property SerialNumber As String

        ''' <summary> Adds a system token data object based on the hard disk serial number. </summary>
        ''' <remarks> Use this method to add a hard disk serial number system token. </remarks>
        Private Sub AddSerialNumberToken()
            ' get serial number of system drive
            Me.AddToken(LicenseTokenName.SerialNumber.ToString, Me.SerialNumber)
        End Sub

        ''' <summary> verifies a system token data object based on the hard disk serial number. </summary>
        ''' <remarks>
        ''' Use this method to verify a token based on the system hard disk serial number.
        ''' </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="e"> Action event information. </param>
        ''' <returns> <c>True</c> if verified. </returns>
        Private Function VerifySerialNumberToken(ByVal e As isr.Core.ActionEventArgs) As Boolean
            If e Is Nothing Then Throw New ArgumentNullException(NameOf(e))
            Dim tokenValue As String = Me.SerialNumber
            Dim tokenName As String = LicenseTokenName.SerialNumber.ToString
            Return Me.VerifyResourceToken(tokenName, tokenValue, e)
        End Function

#End Region

#Region " SERIAL NUMBER HASH TOKEN "

        ''' <summary> Gets or sets the salt. </summary>
        ''' <value> The salt. </value>
        Public Property Salt As String

        ''' <summary> Adds a system token data object based on the hard disk serial number. </summary>
        ''' <remarks> Use this method to add a hard disk serial number system token. </remarks>
        Private Sub AddSerialNumberHashToken()
            ' get serial number of system drive
            Dim tokenValue As String = $"{Me.SerialNumber}.{Me.Salt}".ToBase64Hash
            Me.AddToken(LicenseTokenName.SerialNumberHash.ToString, tokenValue)
        End Sub

        ''' <summary> verifies a system token data object based on the hard disk serial number. </summary>
        ''' <remarks>
        ''' Use this method to verify a token based on the system hard disk serial number.
        ''' </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="e"> Action event information. </param>
        ''' <returns> <c>True</c> if verified. </returns>
        Private Function VerifySerialNumberHashToken(ByVal e As isr.Core.ActionEventArgs) As Boolean
            If e Is Nothing Then Throw New ArgumentNullException(NameOf(e))
            Dim tokenValue As String = $"{Me.SerialNumber}.{Me.Salt}".ToBase64Hash
            Dim tokenName As String = LicenseTokenName.SerialNumberHash.ToString
            Return Me.VerifyResourceToken(tokenName, tokenValue, e)
        End Function

#End Region

#Region " PROGRAM OPTIONS TOKEN "

        ''' <summary> Gets or sets the program options. </summary>
        ''' <value> The program version. </value>
        Public Property ProgramOptions As Long

        ''' <summary> Adds program options token. </summary>
        ''' <remarks> David, 10/8/2020. </remarks>
        Private Sub AddProgramOptionsToken()
            Me.AddToken(LicenseTokenName.ProgramOptions.ToString, Me.ProgramOptions.ToString)
        End Sub

#End Region

#Region " PROGRAM VERSION TOKEN "

        ''' <summary> Gets or sets the program version. </summary>
        ''' <value> The program version. </value>
        Public Property ProgramVersion As Version

        ''' <summary> Adds program version token. </summary>
        ''' <remarks> David, 10/8/2020. </remarks>
        Private Sub AddProgramVersionToken()
            Me.AddToken(LicenseTokenName.ProgramVersion.ToString, Me.ProgramVersion.ToString)
        End Sub

#End Region

#Region " TOP VERSION TOKEN "

        ''' <summary> Gets or sets the top version. </summary>
        ''' <value> The top version. </value>
        Public Property TopVersion As Version

        ''' <summary> Adds top version token. </summary>
        ''' <remarks> David, 10/8/2020. </remarks>
        Private Sub AddTopVersionToken()
            Me.TopVersion = New Version(Me.ProgramVersion.Major, 9)
            Me.AddToken(LicenseTokenName.TopVersion.ToString, Me.TopVersion.ToString)
        End Sub

        ''' <summary> Query if 'version' greater than top version. </summary>
        ''' <remarks> David, 10/8/2020. </remarks>
        ''' <param name="version"> The version. </param>
        ''' <returns>
        ''' <c>true</c> if specified version exceeds the top version; otherwise <c>false</c>
        ''' </returns>
        Public Function IsVersionedOut(ByVal version As Version) As Boolean
            Return version Is Nothing OrElse version.CompareTo(Me.TopVersion) > 0
        End Function

#End Region

#Region " EXPIRATION DATE "

        ''' <summary> Gets or sets the expiration date in universal time. </summary>
        ''' <value> The expiration date in universal time. </value>
        Public Property ExpirationDate As DateTimeOffset

        ''' <summary> Adds expiration date. </summary>
        ''' <remarks> David, 10/8/2020. </remarks>
        Private Sub AddExpirationDate()
            Me.AddToken(LicenseTokenName.ExpirationDate.ToString, Me.ExpirationDate.Date.ToShortDateString)
        End Sub

        ''' <summary> Query if this object is expired. </summary>
        ''' <remarks> David, 10/8/2020. </remarks>
        ''' <returns> <c>true</c> if expired; otherwise <c>false</c> </returns>
        Public Function IsExpired() As Boolean
            Return DateTimeOffset.UtcNow > Me.ExpirationDate
        End Function

#End Region

#Region " ACTIVATIONS "

        ''' <summary> Gets or sets the number of activations. </summary>
        ''' <value> The number of activations. </value>
        Public Property ActivationsCount As Integer

        ''' <summary> Adds activations count. </summary>
        ''' <remarks> David, 10/8/2020. </remarks>
        Private Sub AddActivationsCount()
            Me.AddToken(LicenseTokenName.ActivationCount.ToString, Me.ActivationsCount.ToString)
        End Sub

        ''' <summary> Query if 'count' is activated out. </summary>
        ''' <remarks> David, 10/8/2020. </remarks>
        ''' <param name="count"> Number of. </param>
        ''' <returns> <c>true</c> if activated out; otherwise <c>false</c> </returns>
        Public Function IsActivatedOut(ByVal count As Integer) As Boolean
            Return Me.ActivationsCount > 0 AndAlso Me.ActivationsCount < count
        End Function
#End Region

#End Region

    End Class

    ''' <summary> Enumerates which system tokens are supported. </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    <Flags()> Public Enum LicenseTokenNames

        ''' <summary> . </summary>
        <Description("None")> None

        ''' <summary> Serial number. </summary>
        <Description("Serial Number")> SerialNumber = 1

        ''' <summary> Serial number hash. </summary>
        <Description("Serial Number Hash")> SerialNumberHash = SerialNumber << 1

        ''' <summary> Expiration date. </summary>
        <Description("Expiration Date")> ExpirationDate = SerialNumberHash << 1

        ''' <summary> An enum constant representing the Activation Count option. </summary>
        <Description("Activation Count")> ActivationCount = ExpirationDate << 1

        ''' <summary> An enum constant representing the program version option. </summary>
        <Description("Program Version")> ProgramVersion = ActivationCount << 1

        ''' <summary> An enum constant representing the top version option. </summary>
        <Description("Top Version")> TopVersion = ProgramVersion << 1

        ''' <summary> An enum constant representing the program Options option. </summary>
        <Description("Program Options")> ProgramOptions = TopVersion << 1

        ''' <summary> An enum constant representing all options. </summary>
        <Description("All Options")> AllOptions = (ProgramOptions << 1) - 1
    End Enum

    ''' <summary> Values that represent system token names. </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    Public Enum LicenseTokenName

        ''' <summary> . </summary>
        <Description("None")> None

        ''' <summary> An enum constant representing the file name. </summary>
        <Description("File Version")> FileName = BaseTokenName.FileName

        ''' <summary> An enum constant representing the file version. </summary>
        <Description("File Version")> FileVersion = BaseTokenName.FileVersion

        ''' <summary> An enum constant representing the serial number option. </summary>
        <Description("Serial Number")> SerialNumber

        ''' <summary> An enum constant representing the serial number hash option. </summary>
        <Description("Serial Number Hash")> SerialNumberHash

        ''' <summary> An enum constant representing the expiration date option. </summary>
        <Description("Expiration Date")> ExpirationDate

        ''' <summary> An enum constant representing the Activation Count. </summary>
        <Description("Activation Count")> ActivationCount

        ''' <summary> An enum constant representing the program version. </summary>
        <Description("Program Version")> ProgramVersion

        ''' <summary> An enum constant representing the top version. </summary>
        <Description("Top Version")> TopVersion

        ''' <summary> An enum constant representing the program options. </summary>
        <Description("Program Options")> ProgramOptions
    End Enum

End Namespace


