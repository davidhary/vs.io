Imports System.ComponentModel
Namespace Cryptography

    ''' <summary> This is the abstract base class of the library of token signing. </summary>
    ''' <remarks>
    ''' Inherit this class to implement component token signing. (c) 2003 Integrated Scientific
    ''' Resources, Inc. All rights reserved. <para>
    ''' Licensed under The MIT License.</para><para>
    ''' David, 06/21/03, 1.0.1267.x. </para>
    ''' </remarks>
    Public MustInherit Class TokenSignBase

        Implements IDisposable

#Region " CONSTRUCTION and CLEANUP "

        ''' <summary> Constructs this class. </summary>
        ''' <remarks> David, 10/8/2020. </remarks>
        Protected Sub New()
            MyBase.New()
            Me._CurrentFileVersion = New Version(1, 0)
            Me._ResourceName = "Info"
            Me._ResourceName = "Info"
        End Sub

        ''' <summary> Calls <see cref="M:Dispose(Boolean Disposing)"/> to cleanup. </summary>
        ''' <remarks>
        ''' Do not make this method Overridable (virtual) because a derived class should not be able to
        ''' override this method.
        ''' </remarks>
        Public Sub Dispose() Implements IDisposable.Dispose

            ' Do not change this code.  Put cleanup code in Dispose(Boolean) below.

            ' this disposes all child classes.
            Me.Dispose(True)

            ' Take this object off the finalization(Queue) and prevent finalization code 
            ' from executing a second time.
            GC.SuppressFinalize(Me)

        End Sub

        ''' <summary>
        ''' Gets the dispose status sentinel of the base class.  This applies to the derived class
        ''' provided proper implementation.
        ''' </summary>
        ''' <value> The is disposed. </value>
        Protected Property IsDisposed() As Boolean

        ''' <summary>
        ''' Releases the unmanaged resources used by the <see cref="T:System.Windows.Forms.Control" />
        ''' and its child controls and optionally releases the managed resources.
        ''' </summary>
        ''' <remarks> David, 10/8/2020. </remarks>
        ''' <param name="disposing"> <c>True</c> to release both managed and unmanaged resources;
        '''                                                   <c>False</c> to release only unmanaged
        '''                                                   resources when called from the runtime
        '''                                                   finalize. </param>
        <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
        <System.Diagnostics.DebuggerNonUserCode()>
        Protected Overridable Sub Dispose(ByVal disposing As Boolean)
            Try
                If Not Me.IsDisposed AndAlso disposing Then
                    ' Free managed resources when explicitly called
                    If Me._XmlSign IsNot Nothing Then
                        Me._XmlSign.Dispose()
                        Me._XmlSign = Nothing
                    End If
                End If
            Finally
                Me.IsDisposed = True
            End Try
        End Sub

        ''' <summary>
        ''' This destructor will run only if the Dispose method does not get called. It gives the base
        ''' class the opportunity to finalize. Do not provide destructors in types derived from this
        ''' class.
        ''' </summary>
        ''' <remarks> David, 10/8/2020. </remarks>
        Protected Overrides Sub Finalize()
            ' Do not re-create Dispose clean-up code here.
            ' Calling Dispose(false) is optimal for readability and maintainability.
            Me.Dispose(False)
        End Sub

#End Region

#Region " OPEN AND CLOSE "

        ''' <summary> Gets the opened status of the instance. </summary>
        ''' <value>
        ''' <c>IsOpen</c> is a <see cref="Boolean"/> property that is True if the instance is open.
        ''' </value>
        Public ReadOnly Property IsOpen() As Boolean
            Get
                Return Me._XmlSign IsNot Nothing AndAlso Me.XmlSign.IsOpen
            End Get
        End Property

        ''' <summary> opens this instance. </summary>
        ''' <remarks> Use this method to open the instance.  The method Returns <c>True</c> if success or false if
        ''' it failed opening the instance. </remarks>
        Public Overridable Sub [Open]()
            Try
                ' instantiate and open the XML signature object
                Me._XmlSign = New isr.IO.Cryptography.XmlSign()
                Me.XmlSign.Open()
            Catch
                ' close to meet strong guarantees
                Try : Me.Close() : Finally : End Try
                Throw
            End Try
        End Sub

        ''' <summary> Closes the instance. </summary>
        Public Overridable Sub [Close]()
            ' close the signature object
            If Me.IsOpen Then
                Me.XmlSign.Close()
                Me._XmlSign = Nothing
            End If
        End Sub

#End Region

#Region " XML SIGNATURE: Does the hard work "

        ''' <summary> Gets or sets the XML signing object. </summary>
        ''' <remarks> Use this property to get reference to the XML signature for the token. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <value>
        ''' an <see cref="isr.IO.Cryptography.XmlSign"/> property that can be read from (read only).
        ''' </value>
        Public ReadOnly Property XmlSign() As XmlSign

#End Region

#Region " CREATE AND SAVE "

        ''' <summary> Gets or sets the filename of the full file. </summary>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <value> The filename of the full file. </value>
        Public ReadOnly Property FullFileName As String

        ''' <summary> Gets or sets the identifier of the resource. </summary>
        ''' <remarks> Must be set -- no default provided. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <value> The identifier of the resource. </value>
        Public Property ResourceId As String

        ''' <summary> Gets or sets the name of the resource. </summary>
        ''' <remarks> Defaults to 'Info'. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <value> The name of the resource. </value>
        Public Property ResourceName As String

        ''' <summary> Gets or sets the resource namespace. </summary>
        ''' <remarks> Defaults to 'Value'. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <value> The resource namespace. </value>
        Public Property ResourceNamespace As String

        ''' <summary> Creates and signs the resource document. </summary>
        ''' <remarks> Use this method to create a signed resource document. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="fullFileName">      The file name and path. </param>
        ''' <param name="resourceId">        The identifier of the resource. </param>
        ''' <param name="resourceName">      Name of the resource. </param>
        ''' <param name="resourceNamespace"> The resource namespace. </param>
        Public Sub CreateAndSignResource(ByVal fullFileName As String, ByVal resourceId As String, ByVal resourceName As String, ByVal resourceNamespace As String)

            If String.IsNullOrWhiteSpace(fullFileName) Then Throw New System.ArgumentNullException(NameOf(fullFileName))
            If String.IsNullOrWhiteSpace(resourceId) Then Throw New System.ArgumentNullException(NameOf(resourceId))
            If String.IsNullOrWhiteSpace(resourceName) Then Throw New System.ArgumentNullException(NameOf(resourceName))
            If String.IsNullOrWhiteSpace(resourceNamespace) Then Throw New System.ArgumentNullException(NameOf(resourceNamespace))
            Me._FullFileName = fullFileName
            Me.ResourceId = resourceId

            ' create the resource (XML document) that will contain the signed data nodes.
            Dim xmlDoc As Xml.XmlDocument = XmlSign.CreateResource(resourceName, resourceNamespace)

            ' clear the token dictionary
            Me.ClearKnownState()

            ' Add tokens to the token dictionary
            Me.AddTokens()

            ' adds the tokens to the resource document
            Me.PopulateResource(xmlDoc)

            ' adds the resource to the signed document
            Me.XmlSign.AddResource(xmlDoc, resourceId)

            ' generate the signature
            Me.XmlSign.GenerateSignature()

        End Sub

        ''' <summary> Creates and signs the resource document. </summary>
        ''' <remarks> Use this method to create a signed resource document. </remarks>
        ''' <param name="fullFileName">      The file name and path. </param>
        ''' <param name="resourceName">      Name of the resource. </param>
        ''' <param name="resourceNamespace"> The resource namespace. </param>
        Public Sub CreateAndSignResource(ByVal fullFileName As String, ByVal resourceName As String, ByVal resourceNamespace As String)
            Me.CreateAndSignResource(fullFileName, Me.ResourceId, resourceName, resourceNamespace)
        End Sub

        ''' <summary> Creates and sign resource. </summary>
        ''' <remarks> David, 10/8/2020. </remarks>
        ''' <param name="fullFileName"> Filename of the full file. </param>
        Public Sub CreateAndSignResource(ByVal fullFileName As String)
            Me.CreateAndSignResource(fullFileName, Me.ResourceId, Me.ResourceName, Me.ResourceNamespace)
        End Sub

        ''' <summary> Adds tokens to the token dictionary. </summary>
        ''' <remarks> David, 10/8/2020. </remarks>
        Public MustOverride Sub AddTokens()

        ''' <summary> Saves the signed resource to the <see cref="FullFileName"/> . </summary>
        ''' <remarks> David, 10/8/2020. </remarks>
        Public Sub Save()
            Me.XmlSign.WriteSignature(Me.FullFileName)
        End Sub

#End Region

#Region " READ and PARSE "

        ''' <summary> Verifies the trusted signed file. </summary>
        ''' <remarks> Use this method to verify the signed resource file. </remarks>
        ''' <param name="fullFileName"> The file name and path. </param>
        ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        Public Function VerifyTrustedFile(ByVal fullFileName As String) As Boolean
            Me._FullFileName = fullFileName
            Return XmlSign.IsVerifySignatureFile(fullFileName)
        End Function

        ''' <summary> Verify trust file. </summary>
        ''' <remarks> David, 10/8/2020. </remarks>
        ''' <param name="fullFileName"> The file name and path. </param>
        ''' <param name="publicKeyXml"> The public key XML. </param>
        ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        Public Function VerifyTrustFile(ByVal fullFileName As String, ByVal publicKeyXml As String) As Boolean
            Me._FullFileName = fullFileName
            Return XmlSign.IsVerifySignatureFile(fullFileName, publicKeyXml)
        End Function

        ''' <summary> Reads and parses the signed resource. </summary>
        ''' <remarks> Use this method to read the signed resource. </remarks>
        Public Overridable Sub ReadParse()
            Me.ClearKnownState()
            Me._FullFileName = Me.FullFileName
            Me.XmlSign.ReadSignature(Me.FullFileName)
            ' get the resource ID from the signed file.
            Me.ResourceId = Me.XmlSign.FetchResourceId
            Me.ResourceName = Me.XmlSign.FetchResourceName
            Dim resourceNode As Xml.XmlNode = Me.XmlSign.FetchResourceDataNode(Me.ResourceName)
            ' add all the tokens to the token dictionary.
            For Each node As Xml.XmlNode In resourceNode.ChildNodes
                Me.AddToken(node.Name, node.ChildNodes(0).Value)
            Next
        End Sub

        ''' <summary> Verify, trust, read and parse. </summary>
        ''' <remarks> David, 10/8/2020. </remarks>
        ''' <param name="fullFileName"> The file name and path. </param>
        ''' <param name="publicKeyXml"> The public key XML. </param>
        ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        Public Function VerifyTrustReadParse(ByVal fullFileName As String, ByVal publicKeyXml As String) As Boolean
            If Me.VerifyTrustFile(fullFileName, publicKeyXml) Then
                Me.ReadParse()
                Return True
            Else
                Return False
            End If
        End Function

#End Region

#Region " RESOURCE BUILDER "

        ''' <summary> The current file version. </summary>
        Private _CurrentFileVersion As Version

        ''' <summary> Gets or sets the current file version. </summary>
        ''' <value> The current file version. </value>
        Public Property CurrentFileVersion As Version
            Get
                Return Me._CurrentFileVersion
            End Get
            Protected Set(value As Version)
                Me._CurrentFileVersion = value
            End Set
        End Property

        ''' <summary> Clears the known state. </summary>
        ''' <remarks> David, 10/8/2020. </remarks>
        Protected Sub ClearKnownState()
            Me._TokenDictionary = New Dictionary(Of String, String)
        End Sub

        ''' <summary> Gets or sets a dictionary of tokens. </summary>
        ''' <value> A Dictionary of tokens. </value>
        Protected ReadOnly Property TokenDictionary As IDictionary(Of String, String)

        ''' <summary> Gets the token lists in this collection. </summary>
        ''' <remarks> David, 9/7/2020. </remarks>
        ''' <returns> The token list. </returns>
        Public Function GetTokenList() As ICollection(Of KeyValuePair(Of String, String))
            Return Me.TokenDictionary
        End Function

        ''' <summary> Gets the tokens. </summary>
        ''' <remarks> David, 10/8/2020. </remarks>
        ''' <returns> The tokens. </returns>
        Public Function GetTokens() As String
            Dim builder As New System.Text.StringBuilder
            For Each token As KeyValuePair(Of String, String) In Me.TokenDictionary
                builder.AppendLine($"{token.Key},{token.Value}")
            Next
            Return builder.ToString
        End Function

        ''' <summary> Adds a token to 'tokenValue'. </summary>
        ''' <remarks> David, 10/8/2020. </remarks>
        ''' <param name="tokenName">  Name of the token. </param>
        ''' <param name="tokenValue"> The token value. </param>
        Public Sub AddToken(ByVal tokenName As String, ByVal tokenValue As String)
            If Me.TokenDictionary.ContainsKey(tokenName) Then
                Me.TokenDictionary.Item(tokenName) = tokenValue
            Else
                Me.TokenDictionary.Add(tokenName, tokenValue)
            End If
        End Sub

        ''' <summary> Populate resource. </summary>
        ''' <remarks> David, 10/8/2020. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="xmlDoc"> The XML document. </param>
        Public Sub PopulateResource(ByVal xmlDoc As System.Xml.XmlDocument)
            If xmlDoc Is Nothing Then Throw New ArgumentNullException(NameOf(xmlDoc))
            Me.AddToken(BaseTokenName.FileName.ToString, My.Computer.FileSystem.GetFileInfo(Me.FullFileName).Name)
            Me.AddToken(BaseTokenName.FileVersion.ToString, Me.CurrentFileVersion.ToString)
            For Each token As KeyValuePair(Of String, String) In Me.TokenDictionary
                XmlSign.AddResourceToken(xmlDoc, token.Key, token.Value)
            Next
        End Sub

#End Region

#Region " RESOURCE VALUE VERIFICATIONS "

        ''' <summary> Verify resource token. </summary>
        ''' <remarks> David, 10/8/2020. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="fullFileName"> The file name and path. </param>
        ''' <param name="publicKeyXml"> The public key XML. </param>
        ''' <param name="tokenName">    Name of the token. </param>
        ''' <param name="tokenValue">   The token value. </param>
        ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        Public Function VerifyResourceToken(ByVal fullFileName As String, ByVal publicKeyXml As String, ByVal tokenName As String, ByVal tokenValue As String) As Boolean
            If String.IsNullOrWhiteSpace(fullFileName) Then Throw New System.ArgumentNullException(NameOf(fullFileName))
            If String.IsNullOrWhiteSpace(tokenName) Then Throw New System.ArgumentNullException(NameOf(tokenName))
            If String.IsNullOrWhiteSpace(tokenValue) Then Throw New System.ArgumentNullException(NameOf(tokenValue))
            Return Me.VerifyTrustReadParse(fullFileName, publicKeyXml) AndAlso
                   Me.TokenDictionary.ContainsKey(tokenName) AndAlso
                   String.Equals(tokenValue, Me.TokenDictionary.Item(tokenName), StringComparison.Ordinal)
        End Function

        ''' <summary> Verify resource token. </summary>
        ''' <remarks> David, 10/8/2020. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="tokenName">  Name of the token. </param>
        ''' <param name="tokenValue"> The token value. </param>
        ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        Protected Function VerifyResourceToken(ByVal tokenName As String, ByVal tokenValue As String) As Boolean
            If String.IsNullOrWhiteSpace(tokenName) Then Throw New System.ArgumentNullException(NameOf(tokenName))
            If String.IsNullOrWhiteSpace(tokenValue) Then Throw New System.ArgumentNullException(NameOf(tokenValue))
            Return Me.TokenDictionary.ContainsKey(tokenName) AndAlso String.Equals(tokenValue, Me.TokenDictionary.Item(tokenName), StringComparison.Ordinal)
        End Function

        ''' <summary> Verify resource token. </summary>
        ''' <remarks> David, 10/8/2020. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="tokenName">  Name of the token. </param>
        ''' <param name="tokenValue"> The token value. </param>
        ''' <param name="e">          Cancel details event information. </param>
        ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        Protected Function VerifyResourceToken(ByVal tokenName As String, ByVal tokenValue As String, ByVal e As isr.Core.ActionEventArgs) As Boolean
            If e Is Nothing Then Throw New ArgumentNullException(NameOf(e))
            If Not Me.VerifyResourceToken(tokenName, tokenValue) Then
                If Me.TokenDictionary.ContainsKey(tokenName) Then
                    e.RegisterFailure($"{tokenName} {Me.TokenDictionary.Item(tokenName)} different from {tokenValue}")
                Else
                    e.RegisterFailure($"{Me.FullFileName} does not contain the {tokenName} value")
                End If
            End If
            Return Not e.Failed
        End Function

        ''' <summary> Verifies all tokens. </summary>
        ''' <remarks> David, 10/8/2020. </remarks>
        ''' <param name="e"> Cancel details event information. </param>
        ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        Public MustOverride Function VerifyTokens(ByVal e As isr.Core.ActionEventArgs) As Boolean

#End Region

    End Class

    ''' <summary> Values that represent base token names. </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    Public Enum BaseTokenName

        ''' <summary> Not specified. </summary>
        <Description("Not specified")> None

        ''' <summary> Name of the file name token. </summary>
        <Description("File Version")> FileName

        ''' <summary> Name of the file name token. </summary>
        <Description("File Version")> FileVersion
    End Enum

End Namespace

