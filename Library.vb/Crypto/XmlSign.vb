Imports System.Xml

Imports isr.Core.HashExtensions
Imports isr.IO.ExceptionExtensions
Namespace Cryptography

    ''' <summary> Creates and verifies XML signatures. </summary>
    ''' <remarks>
    ''' Use this class to sign data using XML signature algorithms. (c) 2003 Integrated Scientific
    ''' Resources, Inc. All rights reserved. <para>
    ''' Licensed under The MIT License.</para><para>
    ''' David, 06/17/03, 1.0.1263.x. </para>
    ''' </remarks>
    Public Class XmlSign
        Implements IDisposable

#Region " CONSTRUCTION and CLEANUP "

        ''' <summary> Constructs this class. </summary>
        ''' <remarks> David, 10/8/2020. </remarks>
        Public Sub New()
            MyBase.New()
        End Sub

        ''' <summary> Calls <see cref="M:Dispose(Boolean Disposing)"/> to cleanup. </summary>
        ''' <remarks>
        ''' Do not make this method Overridable (virtual) because a derived class should not be able to
        ''' override this method.
        ''' </remarks>
        Public Sub Dispose() Implements IDisposable.Dispose

            ' Do not change this code.  Put cleanup code in Dispose(Boolean) below.

            ' this disposes all child classes.
            Me.Dispose(True)

            ' Take this object off the finalization(Queue) and prevent finalization code 
            ' from executing a second time.
            GC.SuppressFinalize(Me)

        End Sub

        ''' <summary>
        ''' Gets the dispose status sentinel of the base class.  This applies to the derived class
        ''' provided proper implementation.
        ''' </summary>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <exception cref="System.IO.FileNotFoundException"> Thrown when the requested file is not present. </exception>
        ''' <value> The is disposed. </value>
        Protected Property IsDisposed() As Boolean

        ''' <summary>
        ''' Releases the unmanaged resources used by the <see cref="T:System.Windows.Forms.Control" />
        ''' and its child controls and optionally releases the managed resources.
        ''' </summary>
        ''' <remarks> David, 10/8/2020. </remarks>
        ''' <param name="disposing"> <c>True</c> to release both managed and unmanaged resources;
        '''                                                   <c>False</c> to release only unmanaged
        '''                                                   resources when called from the runtime
        '''                                                   finalize. </param>
        <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
        <System.Diagnostics.DebuggerNonUserCode()>
        Protected Overridable Sub Dispose(ByVal disposing As Boolean)
            Try
                If Not Me.IsDisposed AndAlso disposing Then
                    ' Free managed resources when explicitly called
                    If Me._RsaSigningKey IsNot Nothing Then
                        Me._RsaSigningKey.Dispose()
                        Me._RsaSigningKey = Nothing
                    End If
                    Me._SignedXml = Nothing
                End If
            Finally
                Me.IsDisposed = True
            End Try
        End Sub

        ''' <summary>
        ''' This destructor will run only if the Dispose method does not get called. It gives the base
        ''' class the opportunity to finalize. Do not provide destructors in types derived from this
        ''' class.
        ''' </summary>
        ''' <remarks> David, 10/8/2020. </remarks>
        Protected Overrides Sub Finalize()
            ' Do not re-create Dispose clean-up code here.
            ' Calling Dispose(false) is optimal for readability and maintainability.
            Me.Dispose(False)
        End Sub

#End Region

#Region " SHARED: BUILD SIGNATURE FILE "

        ''' <summary> Adds a resource to the signature file. </summary>
        ''' <remarks>
        ''' Use this method to add resource specified by its data and id to the signed data set.
        ''' </remarks>
        ''' <exception cref="ArgumentNullException" guarantee="strong"> . </exception>
        ''' <param name="signedXml">    is the <see cref="System.Security.Cryptography.Xml.SignedXml">
        '''                             signed XML</see> containing the resource and signature. </param>
        ''' <param name="resourceData"> is an Object expression that specifies the data to add. </param>
        ''' <param name="resourceId">   Specifies the ID of the resource to add. </param>
        Public Overloads Shared Sub AddResource(ByVal signedXml As System.Security.Cryptography.Xml.SignedXml,
                                                ByVal resourceData As Xml.XmlNodeList, ByVal resourceId As String)

            If resourceData Is Nothing OrElse resourceData.Count = 0 Then Throw New System.ArgumentNullException(NameOf(resourceData))
            If String.IsNullOrWhiteSpace(resourceId) Then Throw New System.ArgumentNullException(NameOf(resourceId))
            If signedXml Is Nothing Then Throw New System.ArgumentNullException(NameOf(signedXml))

            ' Create a data object to hold the data to sign.
            Dim xmlDataObject As Security.Cryptography.Xml.DataObject = New System.Security.Cryptography.Xml.DataObject With {
                .Data = resourceData,
                .Id = resourceId
            }

            ' Add the data object to the signature.
            signedXml.AddObject(xmlDataObject)

            ' Create a reference to be able to package everything into the message.
            Dim xmlReference As New System.Security.Cryptography.Xml.Reference With {
                .Uri = String.Format(Globalization.CultureInfo.CurrentCulture, "#{0}", xmlDataObject.Id)
            }

            ' Add the reference to the message
            signedXml.AddReference(xmlReference)

        End Sub

        ''' <summary> Creates an XML resource document from the text data to sign. </summary>
        ''' <remarks> Use this method to create an XML document with data to sign. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="prefixName">        Name of the prefix. </param>
        ''' <param name="resourceName">      Specifies the name of the resource which to create. </param>
        ''' <param name="resourceNamespace"> The resource namespace. </param>
        ''' <returns> The XML resource document. </returns>
        Public Shared Function CreateResource(ByVal prefixName As String, ByVal resourceName As String, ByVal resourceNamespace As String) As System.Xml.XmlDocument

            If prefixName Is Nothing Then Throw New System.ArgumentNullException(NameOf(prefixName))
            If String.IsNullOrWhiteSpace(resourceName) Then Throw New System.ArgumentNullException(NameOf(resourceName))
            If String.IsNullOrWhiteSpace(resourceNamespace) Then Throw New System.ArgumentNullException(NameOf(resourceNamespace))

            ' Create example data to sign.
            Dim xmlDoc As New System.Xml.XmlDocument
            Dim xmlNode As System.Xml.XmlNode = xmlDoc.CreateNode(XmlNodeType.Element, prefixName, resourceName, resourceNamespace)
            xmlDoc.AppendChild(xmlNode)
            Return xmlDoc

        End Function

        ''' <summary> Creates an XML resource document from the text data to sign. </summary>
        ''' <remarks> Use this method to create an XML document with data to sign. </remarks>
        ''' <param name="resourceName">      Specifies the name of the resource which to create. </param>
        ''' <param name="resourceNamespace"> The element namespace. </param>
        ''' <returns> The XML resource document. </returns>
        Public Shared Function CreateResource(ByVal resourceName As String, ByVal resourceNamespace As String) As System.Xml.XmlDocument
            Return CreateResource("", resourceName, resourceNamespace)
        End Function

        ''' <summary> Adds a resource token (name and value). </summary>
        ''' <remarks> Adds a node to the resource XML element. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="xmlDocument"> The document. </param>
        ''' <param name="tokenName">   The name. </param>
        ''' <param name="tokenValue">  The value. </param>
        Public Shared Sub AddResourceToken(ByVal xmlDocument As System.Xml.XmlDocument, ByVal tokenName As String, ByVal tokenValue As String)

            If xmlDocument Is Nothing Then Throw New System.ArgumentNullException(NameOf(xmlDocument))
            If String.IsNullOrWhiteSpace(tokenValue) Then Throw New System.ArgumentNullException(NameOf(tokenValue))
            If String.IsNullOrWhiteSpace(tokenName) Then Throw New System.ArgumentNullException(NameOf(tokenName))

            Dim root As XmlNode = xmlDocument.DocumentElement
            Dim elem As XmlElement = xmlDocument.CreateElement(tokenName)
            elem.InnerText = tokenValue
            root.AppendChild(elem)

        End Sub

#End Region

#Region " SHARED: KEY MANAGEMENT "

        ''' <summary> Creates an RSA signing key. </summary>
        ''' <remarks> David, 10/8/2020. </remarks>
        ''' <returns> A new <see cref="System.Security.Cryptography.RSA">signing key</see>. </returns>
        Public Shared Function CreateRsaSigningKey() As System.Security.Cryptography.RSA
            Return Security.Cryptography.RSA.Create()
        End Function

        ''' <summary> Saves a rsa signing key. </summary>
        ''' <remarks> David, 10/8/2020. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="key">                      is an System.Security.Cryptography.KeyedHashAlgorithm
        '''                                         object specifying the key which to use for the
        '''                                         signature. </param>
        ''' <param name="fullFileName">             Filename of the full file. </param>
        ''' <param name="includePrivateParameters"> True to include, false to exclude the private
        '''                                         parameters. </param>
        Public Shared Sub SaveRsaSigningKey(ByVal key As System.Security.Cryptography.RSA, ByVal fullFileName As String, ByVal includePrivateParameters As Boolean)
            If key Is Nothing Then Throw New ArgumentNullException(NameOf(key))
            If String.IsNullOrWhiteSpace(fullFileName) Then Throw New ArgumentNullException(NameOf(fullFileName))
            Dim xmlString As String = key.ToXmlString(includePrivateParameters)
            My.Computer.FileSystem.WriteAllText(fullFileName, xmlString, False)
        End Sub

        ''' <summary> Creates rsa signing key from XML. </summary>
        ''' <remarks> David, 10/8/2020. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <exception cref="System.IO.FileNotFoundException"> Thrown when the requested file is not present. </exception>
        ''' <param name="fullFileName"> Filename of the full file. </param>
        ''' <returns> The new rsa signing key from XML. </returns>
        Public Shared Function CreateRsaSigningKeyFromXml(ByVal fullFileName As String) As System.Security.Cryptography.RSA
            If String.IsNullOrWhiteSpace(fullFileName) Then Throw New ArgumentNullException(NameOf(fullFileName))
            Dim fi As New System.IO.FileInfo(fullFileName)
            If fi.Exists AndAlso (fi.Length > 128) Then
                Dim xmlString As String = My.Computer.FileSystem.ReadAllText(fullFileName)
                Dim key As System.Security.Cryptography.RSA = Security.Cryptography.RSA.Create()
                key.FromXmlString(xmlString)
                Return key
            ElseIf Not fi.Exists Then
                Throw New System.IO.FileNotFoundException("RSA Key file not found", fullFileName)
            Else
                Throw New System.IO.FileNotFoundException("RSA Key file too small", fullFileName)
            End If
        End Function

#End Region

#Region " SHARED: SIGN SIGNATURE FILE "

        ''' <summary> Signs an XML file using an RSA key. </summary>
        ''' <remarks>
        ''' Use this method to sign a detached resource data and save the signature to a signature file.
        ''' </remarks>
        ''' <exception cref="ArgumentNullException" guarantee="strong"> . </exception>
        ''' <param name="resourceData"> is an Object expression that specifies the resource data which to
        '''                             sign. </param>
        ''' <param name="signingKey">   The signing key. </param>
        ''' <param name="resourceId">   Specifies the ID to associate with the signed resource. </param>
        ''' <param name="fullFileName"> Specifies the name of the output signature file. </param>
        ''' <returns>
        ''' A <see cref="System.Security.Cryptography.Xml.SignedXml">document</see> that includes the
        ''' signature.
        ''' </returns>
        Public Shared Function SignResource(ByVal resourceData As Xml.XmlNodeList,
                                            ByVal signingKey As System.Security.Cryptography.RSA,
                                            ByVal resourceId As String,
                                            ByVal fullFileName As String) As System.Security.Cryptography.Xml.SignedXml

            If resourceData Is Nothing OrElse resourceData.Count = 0 Then Throw New System.ArgumentNullException(NameOf(resourceData))
            If String.IsNullOrWhiteSpace(resourceId) Then Throw New System.ArgumentNullException(NameOf(resourceId))
            If String.IsNullOrWhiteSpace(fullFileName) Then Throw New System.ArgumentNullException(NameOf(fullFileName))

            ' Create a SignedXml object.
            Dim signedXml As New System.Security.Cryptography.Xml.SignedXml

            ' Add the xml data to the signed XML
            AddResource(signedXml, resourceData, resourceId)

            ' assign the key as a signing key.
            signedXml.SigningKey = signingKey

            ' Add a KeyInfo to the signature
            Dim thisKeyInfo As New System.Security.Cryptography.Xml.KeyInfo
            thisKeyInfo.AddClause(New System.Security.Cryptography.Xml.RSAKeyValue(signingKey))
            signedXml.KeyInfo = thisKeyInfo

            ' Compute the signature.
            signedXml.ComputeSignature()

            ' save the signature
            WriteSignatureFile(signedXml, fullFileName)

            ' return the signed xml
            Return signedXml

        End Function

        ''' <summary> Signs a <see cref="System.uri">URI</see> resource. </summary>
        ''' <remarks>
        ''' Use this method to Sign a <see cref="System.uri">URI</see> resource (e.g.,
        ''' "http://www.microsoft.com")
        ''' and save the signature in a new file.  the resource can be an XML file.
        ''' </remarks>
        ''' <exception cref="ArgumentNullException" guarantee="strong"> . </exception>
        ''' <param name="resourceUri">  Specifies the location of the resource which to sign. </param>
        ''' <param name="fullFileName"> Specifies the name of the output signature file. </param>
        ''' <param name="key">          is an System.Security.Cryptography.KeyedHashAlgorithm object
        '''                             specifying the key which to use for the signature. </param>
        ''' <returns>
        ''' A <see cref="System.Security.Cryptography.Xml.SignedXml">document</see> that includes the
        ''' signature.
        ''' </returns>
        Public Shared Function SignResource(ByVal resourceUri As System.Uri, ByVal fullFileName As String,
                                            ByVal key As System.Security.Cryptography.KeyedHashAlgorithm) As System.Security.Cryptography.Xml.SignedXml

            If resourceUri Is Nothing Then Throw New System.ArgumentNullException(NameOf(resourceUri))
            If String.IsNullOrWhiteSpace(fullFileName) Then Throw New System.ArgumentNullException(NameOf(fullFileName))
            If key Is Nothing Then Throw New System.ArgumentNullException(NameOf(key))

            ' Create a SignedXml object.
            Dim signedXml As New System.Security.Cryptography.Xml.SignedXml

            ' Create a reference to be signed.
            ' Add the passed URI to the reference object.
            Dim reference As New System.Security.Cryptography.Xml.Reference With {
                .Uri = resourceUri.ToString
            }

            ' Add a transformation if the URI is an XML file.
            If resourceUri.ToString.EndsWith("xml", StringComparison.OrdinalIgnoreCase) Then
                reference.AddTransform(New System.Security.Cryptography.Xml.XmlDsigC14NTransform)
            End If

            ' Add the reference to the SignedXml object.
            signedXml.AddReference(reference)

            ' Compute the signature.
            signedXml.ComputeSignature(key)

            ' save the signature
            WriteSignatureFile(signedXml, fullFileName)

            ' return the signed xml
            Return signedXml

        End Function

        ''' <summary> Signs an XML file using an RSA key. </summary>
        ''' <remarks>
        ''' Use this method to sign an XML file and save the signature to a signature file.
        ''' </remarks>
        ''' <exception cref="ArgumentNullException" guarantee="strong"> . </exception>
        ''' <param name="xmlFileName">  Specifies the XML file name which to sign. </param>
        ''' <param name="resourceId">   Specifies the ID to associate with the signed resource. </param>
        ''' <param name="signingKey">   The signing key. </param>
        ''' <param name="fullFileName"> Specifies the name of the output signature file. </param>
        ''' <returns>
        ''' A <see cref="System.Security.Cryptography.Xml.SignedXml">document</see> that includes the
        ''' signature.
        ''' </returns>
        Public Shared Function SignResource(ByVal xmlFileName As String, ByVal resourceId As String,
                                            ByVal signingKey As System.Security.Cryptography.RSA,
                                            ByVal fullFileName As String) As System.Security.Cryptography.Xml.SignedXml
            If String.IsNullOrWhiteSpace(xmlFileName) Then Throw New System.ArgumentNullException(NameOf(xmlFileName))
            If String.IsNullOrWhiteSpace(resourceId) Then Throw New System.ArgumentNullException(NameOf(resourceId))
            If String.IsNullOrWhiteSpace(fullFileName) Then Throw New System.ArgumentNullException(NameOf(fullFileName))

            ' instantiate a document to read the XML data
            Dim xmlDoc As New System.Xml.XmlDocument

            ' read the XML data
            xmlDoc.Load(xmlFileName)

            ' sign the XML data
            Return SignResource(xmlDoc.ChildNodes, signingKey, resourceId, fullFileName)

        End Function

        ''' <summary> Writes the signature file. </summary>
        ''' <remarks> Use this method to save the signature to file. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="signedXml">    is the <see cref="System.Security.Cryptography.Xml.SignedXml">
        '''                             signed XML</see> containing the resource and signature. </param>
        ''' <param name="fullFileName"> Specifies the name of the output signature file. </param>
        Public Shared Sub WriteSignatureFile(ByVal signedXml As System.Security.Cryptography.Xml.SignedXml,
                                             ByVal fullFileName As String)

            If signedXml Is Nothing Then Throw New System.ArgumentNullException(NameOf(signedXml))
            If String.IsNullOrWhiteSpace(fullFileName) Then Throw New System.ArgumentNullException(NameOf(fullFileName))

            ' Save the signed XML document to a file specified using the passed string.
            Using xmltw As New System.Xml.XmlTextWriter(fullFileName, New System.Text.UTF8Encoding(False))
                xmltw.Formatting = Formatting.Indented ' always sign indented and ignore white space.
                signedXml.GetXml.WriteTo(xmltw)
                ' this also disposes; Otherwise, uncomment: .Close()
            End Using

        End Sub

#End Region

#Region " SHARED: READ SIGNATURE FILE RESOURCE ID "

        ''' <summary> Returns the data string containing the signed resource. </summary>
        ''' <remarks> This method, which uses the signed XML API call, does not seem to work. </remarks>
        ''' <exception cref="ArgumentNullException" guarantee="strong"> . </exception>
        ''' <param name="signedXml">  is the <see cref="System.Security.Cryptography.Xml.SignedXml">
        '''                           signed XML</see> containing the resource and signature. </param>
        ''' <param name="resourceId"> Specifies the ID of the resource in the signature. </param>
        ''' <returns> The data string containing the signed resource. </returns>
        <Obsolete("This method does not seem to work")>
        Public Overloads Shared Function FetchResourceIdOuterXml(ByVal signedXml As System.Security.Cryptography.Xml.SignedXml,
                                                                 ByVal resourceId As String) As String

            If String.IsNullOrWhiteSpace(resourceId) Then Throw New System.ArgumentNullException(NameOf(resourceId))
            If signedXml Is Nothing Then Throw New System.ArgumentNullException(NameOf(signedXml))

            ' get the resource element and return the text
            Return FetchResourceIdElement(signedXml, resourceId).OuterXml

        End Function

        ''' <summary> Returns the signed XML element containing the signed resource. </summary>
        ''' <remarks> This method, which uses the signed XML API call, does not seem to work. </remarks>
        ''' <exception cref="ArgumentNullException" guarantee="strong"> . </exception>
        ''' <param name="signedXml">  is the <see cref="System.Security.Cryptography.Xml.SignedXml">
        '''                           signed XML</see> containing the resource and signature. </param>
        ''' <param name="resourceId"> Specifies the ID of the resource in the signature. </param>
        ''' <returns> The signed XML element containing the signed resource. </returns>
        <Obsolete("This method does not seem to work")>
        Public Overloads Shared Function FetchResourceIdElement(ByVal signedXml As System.Security.Cryptography.Xml.SignedXml,
                                                                ByVal resourceId As String) As System.Xml.XmlElement

            If String.IsNullOrWhiteSpace(resourceId) Then Throw New System.ArgumentNullException(NameOf(resourceId))
            If signedXml Is Nothing Then Throw New System.ArgumentNullException(NameOf(signedXml))

            ' Load the XML.
            Dim xmlDoc As New System.Xml.XmlDocument With {
                .PreserveWhitespace = False ' required if formatting with indentations.
                }
            xmlDoc.LoadXml(signedXml.GetXml.OuterXml)
            Return signedXml.GetIdElement(xmlDoc, resourceId)

        End Function

#End Region

#Region " SHARED: READ SIGNATURE FILE "

        ''' <summary> Name of the root node. </summary>
        Private Const _RootNodeName As String = "Signature"

        ''' <summary> Name of the key information node. </summary>
        Private Const _KeyInfoNodeName As String = "KeyInfo"

        ''' <summary> Name of the object node. </summary>
        Private Const _ObjectNodeName As String = "Object"

        ''' <summary> Name of the object node identifier. </summary>
        Private Const _ObjectNodeIdName As String = "Id"

        ''' <summary> The path element format. </summary>
        Private Const _PathElementFormat As String = "/*[local-name()='{0}']"

        ''' <summary> Builds XML path. </summary>
        ''' <remarks> David, 9/7/2020. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="path"> The path string, such as <c>/*[local-name()='Signature']/*[local-
        '''                     name()='KeyInfo']</c>. </param>
        ''' <returns> A String. </returns>
        Public Shared Function BuildXmlPath(ByVal path As IList(Of String)) As String
            If path Is Nothing OrElse Not path.Any Then Throw New System.ArgumentNullException(NameOf(path))
            Dim builder As New System.Text.StringBuilder
            For Each pathElement As String In path
                builder.AppendFormat(_PathElementFormat, pathElement)
            Next
            Return builder.ToString
        End Function

        ''' <summary> Fetches a node. </summary>
        ''' <remarks> David, 10/8/2020. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="signedXml"> is the <see cref="System.Security.Cryptography.Xml.SignedXml">
        '''                          signed XML</see> containing the resource and signature. </param>
        ''' <param name="xmlPath">   The XML document path string, such as
        '''                          <c>/*[local-name()='Signature']/*[local-name()='KeyInfo']</c>. 
        ''' </param>
        ''' <returns> The node. </returns>
        Public Overloads Shared Function FetchNode(ByVal signedXml As System.Security.Cryptography.Xml.SignedXml, ByVal xmlPath As String) As System.Xml.XmlNode
            If signedXml Is Nothing Then Throw New System.ArgumentNullException(NameOf(signedXml))
            If String.IsNullOrWhiteSpace(xmlPath) Then Throw New System.ArgumentNullException(NameOf(xmlPath))
            Dim xml As XmlDocument = New XmlDocument()
            xml.LoadXml(signedXml.GetXml.OuterXml)
            Dim node As XmlNode = xml.SelectSingleNode(xmlPath)
            Return node
        End Function

        ''' <summary> Fetches a node. </summary>
        ''' <remarks> David, 10/8/2020. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="signedXml"> is the <see cref="System.Security.Cryptography.Xml.SignedXml">
        '''                          signed XML</see> containing the resource and signature. </param>
        ''' <param name="xmlPath">   The XML document path. </param>
        ''' <returns> The node. </returns>
        Public Overloads Shared Function FetchNode(ByVal signedXml As System.Security.Cryptography.Xml.SignedXml, ByVal xmlPath As IList(Of String)) As System.Xml.XmlNode
            If signedXml Is Nothing Then Throw New System.ArgumentNullException(NameOf(signedXml))
            Return FetchNode(signedXml, BuildXmlPath(xmlPath))
        End Function

        ''' <summary> Returns the data string containing the signed resource. </summary>
        ''' <remarks> Use this method to get the data including the signed resource is stored. </remarks>
        ''' <exception cref="ArgumentNullException" guarantee="strong"> . </exception>
        ''' <param name="signedXml">    is the <see cref="System.Security.Cryptography.Xml.SignedXml">
        '''                             signed XML</see> containing the resource and signature. </param>
        ''' <param name="resourceName"> Specifies the name of the resource which to find. </param>
        ''' <returns> The data string containing the signed resource. </returns>
        Public Overloads Shared Function FetchResourceDataOuterXml(ByVal signedXml As System.Security.Cryptography.Xml.SignedXml,
                                                                   ByVal resourceName As String) As String

            If String.IsNullOrWhiteSpace(resourceName) Then Throw New System.ArgumentNullException(NameOf(resourceName))
            If signedXml Is Nothing Then Throw New System.ArgumentNullException(NameOf(signedXml))
            ' get the resource element and return the text
            Return FetchResourceDataNode(signedXml, resourceName).OuterXml

        End Function

        ''' <summary> Gets resource data node. </summary>
        ''' <remarks> David, 10/8/2020. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="signedXml">    is the <see cref="System.Security.Cryptography.Xml.SignedXml">
        '''                             signed XML</see> containing the resource and signature. </param>
        ''' <param name="resourceName"> Specifies the name of the resource which to find. </param>
        ''' <returns> The resource data node. </returns>
        Public Overloads Shared Function FetchResourceDataNode(ByVal signedXml As System.Security.Cryptography.Xml.SignedXml,
                                                             ByVal resourceName As String) As System.Xml.XmlNode
            If signedXml Is Nothing Then Throw New System.ArgumentNullException(NameOf(signedXml))
            If String.IsNullOrWhiteSpace(resourceName) Then Throw New System.ArgumentNullException(NameOf(resourceName))
            Return FetchNode(signedXml, New String() {_RootNodeName, _ObjectNodeName, resourceName})
        End Function

        ''' <summary> Returns the resource XML data element. </summary>
        ''' <remarks>
        ''' Use this method to get the resource encapsulated in the signature in its XML data element.
        ''' </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="signedXml"> is the <see cref="System.Security.Cryptography.Xml.SignedXml">
        '''                          signed XML</see> containing the resource and signature. </param>
        ''' <returns> The outer XML of the resource. </returns>
        Public Overloads Shared Function FetchResourceDataNode(ByVal signedXml As System.Security.Cryptography.Xml.SignedXml) As System.Xml.XmlNode
            If signedXml Is Nothing Then Throw New System.ArgumentNullException(NameOf(signedXml))
            Return FetchObjectNode(signedXml).ChildNodes(0)
        End Function

        ''' <summary>
        ''' Gets the resource name from the
        ''' <see cref="FetchObjectNode(Security.Cryptography.Xml.SignedXml)">object node</see>.
        ''' </summary>
        ''' <remarks> David, 10/8/2020. </remarks>
        ''' <param name="signedXml"> is the <see cref="System.Security.Cryptography.Xml.SignedXml">
        '''                          signed XML</see> containing the resource and signature. </param>
        ''' <returns> The resource identifier. </returns>
        Public Shared Function FetchResourceName(ByVal signedXml As System.Security.Cryptography.Xml.SignedXml) As String
            Return FetchResourceDataNode(signedXml).Name
        End Function

        ''' <summary>
        ''' Returns the resource XML object node, which contains the signed resource node.
        ''' </summary>
        ''' <remarks> David, 10/8/2020. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="signedXml"> is the <see cref="System.Security.Cryptography.Xml.SignedXml">
        '''                          signed XML</see> containing the resource and signature. </param>
        ''' <returns> The outer XML of the resource. </returns>
        Public Shared Function FetchObjectNode(ByVal signedXml As System.Security.Cryptography.Xml.SignedXml) As System.Xml.XmlNode
            If signedXml Is Nothing Then Throw New System.ArgumentNullException(NameOf(signedXml))
            Return FetchNode(signedXml, New String() {_RootNodeName, _ObjectNodeName})
        End Function

        ''' <summary> Gets the resource identifier. </summary>
        ''' <remarks> David, 10/8/2020. </remarks>
        ''' <param name="signedXml"> is the <see cref="System.Security.Cryptography.Xml.SignedXml">
        '''                          signed XML</see> containing the resource and signature. </param>
        ''' <returns> The resource identifier. </returns>
        Public Shared Function FetchResourceId(ByVal signedXml As System.Security.Cryptography.Xml.SignedXml) As String
            Return FetchObjectNode(signedXml).Attributes(_ObjectNodeIdName).Value
        End Function

        ''' <summary> Fetches key information node. </summary>
        ''' <remarks> David, 10/8/2020. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="signedXml"> is the <see cref="System.Security.Cryptography.Xml.SignedXml">
        '''                          signed XML</see> containing the resource and signature. </param>
        ''' <returns> The key information node. </returns>
        Public Overloads Shared Function FetchKeyInfoNode(ByVal signedXml As System.Security.Cryptography.Xml.SignedXml) As System.Xml.XmlNode
            If signedXml Is Nothing Then Throw New System.ArgumentNullException(NameOf(signedXml))
            Return FetchNode(signedXml, New String() {_RootNodeName, _KeyInfoNodeName})
        End Function

#End Region

#Region " SHARED: VERIFY SIGNATURE FILE "

        ''' <summary>
        ''' Query if 'fullFileName' is trusted by comparing the key info to the key info hash. A better
        ''' method would be to check the signature using the public key.
        ''' </summary>
        ''' <remarks> David, 10/8/2020. </remarks>
        ''' <param name="fullFileName"> Specifies the name of the signature file. </param>
        ''' <param name="keyInfoHash">  The key information hash. </param>
        ''' <returns> <c>true</c> if trusted; otherwise <c>false</c> </returns>
        Public Shared Function IsTrusted(ByVal fullFileName As String, ByVal keyInfoHash As String) As Boolean
            ' Create a SignedXml
            Dim signedXml As System.Security.Cryptography.Xml.SignedXml = ReadSignatureFile(fullFileName)
            Dim result As Boolean = String.Equals(keyInfoHash, FetchKeyInfoNode(signedXml).OuterXml.ToBase64Hash, StringComparison.Ordinal)
            Return result
        End Function

        ''' <summary> Verifies a signature file. </summary>
        ''' <remarks>
        ''' Use this method to verify a signature file using a key stored in the signature file.
        ''' </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="fullFileName"> Specifies the name of the signature file. </param>
        ''' <returns> <c>True</c> if the signature verified. </returns>
        Public Shared Function IsVerifySignatureFile(ByVal fullFileName As String) As Boolean
            If String.IsNullOrWhiteSpace(fullFileName) Then Throw New System.ArgumentNullException(NameOf(fullFileName))
            ' read the signature and return true if checked.
            Return ReadSignatureFile(fullFileName).CheckSignature()
        End Function

        ''' <summary> Verifies the signature XML elements. </summary>
        ''' <remarks> Use this method to verify a signature XML element. </remarks>
        ''' <exception cref="ArgumentNullException" guarantee="strong"> . </exception>
        ''' <param name="xmlElement"> is a System.Xml.XmlElement expression that specifies the XML
        '''                           signature element. </param>
        ''' <returns> <c>True</c> if the signature verified. </returns>
        Public Overloads Shared Function IsVerifySignature(ByVal xmlElement As System.Xml.XmlElement) As Boolean

            If xmlElement Is Nothing Then Throw New System.ArgumentNullException(NameOf(xmlElement))

            ' Load the XML element into an xml document
            Dim xmlDoc As New System.Xml.XmlDocument With {
                .PreserveWhitespace = False  ' required if formatting indentations.
                }
            xmlDoc.LoadXml(xmlElement.OuterXml)

            ' Create a SignedXml
            Dim signedXml As System.Security.Cryptography.Xml.SignedXml = LoadSignature(xmlDoc)
            ' return true if checked
            Return signedXml.CheckSignature()

        End Function

        ''' <summary> Verifies a signature file. </summary>
        ''' <remarks> Use this method to verify a signature file using a key. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="fullFileName"> Specifies the name of the signature file. </param>
        ''' <param name="key">          is an System.Security.Cryptography.KeyedHashAlgorithm object
        '''                             specifying the key which used to sign the file. </param>
        ''' <returns> <c>True</c> if the signature verified. </returns>
        Public Shared Function IsVerifySignatureFile(ByVal fullFileName As String,
                                                     ByVal key As System.Security.Cryptography.KeyedHashAlgorithm) As Boolean
            If String.IsNullOrWhiteSpace(fullFileName) Then Throw New System.ArgumentNullException(NameOf(fullFileName))
            ' read the signature and return true if checked.
            Return ReadSignatureFile(fullFileName).CheckSignature(key)

        End Function

        ''' <summary> Verifies a signature file. </summary>
        ''' <remarks> Use this method to verify a signature file using a key. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="fullFileName"> Specifies the name of the signature file. </param>
        ''' <param name="publicKeyXml"> The public key XML. </param>
        ''' <returns> <c>True</c> if the signature verified. </returns>
        Public Shared Function IsVerifySignatureFile(ByVal fullFileName As String,
                                                     ByVal publicKeyXml As String) As Boolean
            If String.IsNullOrWhiteSpace(publicKeyXml) Then Throw New System.ArgumentNullException(NameOf(publicKeyXml))
            Using key As System.Security.Cryptography.RSA = Security.Cryptography.RSA.Create()
                key.FromXmlString(publicKeyXml)
                Return ReadSignatureFile(fullFileName).CheckSignature(key)
            End Using
        End Function

        ''' <summary> Load a signature from an XML document. </summary>
        ''' <remarks> Use this method to read a signature file. </remarks>
        ''' <exception cref="ArgumentNullException" guarantee="strong"> . </exception>
        ''' <param name="xmlDoc"> Specifies the name of the signature file. </param>
        ''' <returns>
        ''' A <see cref="System.Security.Cryptography.Xml.SignedXml">document</see> that includes the
        ''' signature.
        ''' </returns>
        Public Shared Function LoadSignature(ByVal xmlDoc As System.Xml.XmlDocument) As System.Security.Cryptography.Xml.SignedXml

            If xmlDoc Is Nothing Then Throw New System.ArgumentNullException(NameOf(xmlDoc))

            ' Find the "Signature" node and create a new XmlNodeList object.
            Dim xmlNodeList As System.Xml.XmlNodeList = xmlDoc.GetElementsByTagName("Signature")

            ' Create a new SignedXml object and pass it the XML document class.
            Dim signedXml As New System.Security.Cryptography.Xml.SignedXml

            ' Load the signature node.
            signedXml.LoadXml(CType(xmlNodeList(0), System.Xml.XmlElement))

            ' return the signature 
            Return signedXml

        End Function

        ''' <summary> Reads a signature file. </summary>
        ''' <remarks> Use this method to read a signature file. </remarks>
        ''' <exception cref="ArgumentNullException" guarantee="strong"> . </exception>
        ''' <param name="fullFileName"> Specifies the name of the signature file. </param>
        ''' <returns>
        ''' A <see cref="System.Security.Cryptography.Xml.SignedXml">document</see> that includes the
        ''' signature.
        ''' </returns>
        Public Shared Function ReadSignatureFile(ByVal fullFileName As String) As System.Security.Cryptography.Xml.SignedXml

            If String.IsNullOrWhiteSpace(fullFileName) Then Throw New System.ArgumentNullException(NameOf(fullFileName))

            ' Create a new XML document.
            ' Load the passed XML file into the document.
            Dim xmlDoc As New System.Xml.XmlDocument With {
                .PreserveWhitespace = False  ' required if formatting indentations.
                }
            xmlDoc.Load(fullFileName)

            ' load and return the XML signature
            Return LoadSignature(xmlDoc)

        End Function

#End Region

#Region " OPEN / CLOSE "

        ''' <summary> Gets the signed XML. </summary>
        ''' <value> The signed XML. </value>
        Public ReadOnly Property SignedXml() As System.Security.Cryptography.Xml.SignedXml

        ''' <summary> Gets the opened status of the instance. </summary>
        ''' <value>
        ''' <c>IsOpen</c> is a <see cref="Boolean"/> property that is True if the instance is open.
        ''' </value>
        Public ReadOnly Property IsOpen() As Boolean
            Get
                Return Me._SignedXml IsNot Nothing
            End Get
        End Property

        ''' <summary> opens this instance. </summary>
        ''' <remarks> Use this method to open the instance. </remarks>
        Public Sub [Open]()
            Try
                ' instantiate the signed XML message.
                Me._SignedXml = New System.Security.Cryptography.Xml.SignedXml
            Catch
                ' close to meet strong guarantees
                Try : Me.Close() : Finally : End Try
                Throw
            End Try
        End Sub

        ''' <summary> Closes the instance. </summary>
        ''' <remarks> Use this method to close the instance. </remarks>
        Public Sub [Close]()
            Try
                ' terminate the signing key
                If Me._RsaSigningKey IsNot Nothing Then
                    Me._RsaSigningKey.Dispose()
                    Me._RsaSigningKey = Nothing
                End If
            Catch
                Throw
            Finally
                ' terminate the signed XML data object
                Me._SignedXml = Nothing
            End Try
        End Sub

#End Region

#Region " ADD RESOURCE "

        ''' <summary> Adds data to the signature file. </summary>
        ''' <remarks> Use this method to add data to the signed data set. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="resourceData"> is an Object expression that specifies the data to add. </param>
        ''' <param name="resourceId">   Specifies the ID of the data to add. </param>
        Public Overloads Sub AddResource(ByVal resourceData As Xml.XmlNodeList, ByVal resourceId As String)
            If resourceData Is Nothing OrElse resourceData.Count = 0 Then Throw New ArgumentNullException(NameOf(resourceData))
            If String.IsNullOrWhiteSpace(resourceId) Then Throw New ArgumentNullException(NameOf(resourceId))
            AddResource(Me.SignedXml, resourceData, resourceId)
        End Sub

        ''' <summary> Adds data to the signature file. </summary>
        ''' <remarks> Use this method to add data to the signed data set. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="xmlDoc">     Specifies the name of the signature file. </param>
        ''' <param name="resourceId"> Specifies the ID of the resource to add. </param>
        Public Overloads Sub AddResource(ByVal xmlDoc As Xml.XmlDocument, ByVal resourceId As String)
            If xmlDoc Is Nothing Then Throw New ArgumentNullException(NameOf(xmlDoc))
            If String.IsNullOrWhiteSpace(resourceId) Then Throw New ArgumentNullException(NameOf(resourceId))
            Me.AddResource(xmlDoc.ChildNodes, resourceId)
        End Sub

        ''' <summary> Adds data from file to the signature file. </summary>
        ''' <remarks> Use this method to add XML data to the signed data set. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="xmlFileName"> Specifies the XML file name where the data resides. </param>
        ''' <param name="resourceId">  Specifies the ID of the data to add. </param>
        ''' <returns>
        ''' A Boolean data type that is set true if the data was added and properly referenced.
        ''' </returns>
        Public Overloads Function AddResource(ByVal xmlFileName As String, ByVal resourceId As String) As Boolean

            If String.IsNullOrWhiteSpace(xmlFileName) Then Throw New ArgumentNullException(NameOf(xmlFileName))
            If String.IsNullOrWhiteSpace(resourceId) Then Throw New ArgumentNullException(NameOf(resourceId))

            ' instantiate a document to read the XML data
            Dim xmlDoc As New System.Xml.XmlDocument

            ' read the XML data
            xmlDoc.Load(xmlFileName)

            ' Add the xml data
            Me.AddResource(xmlDoc.ChildNodes, resourceId)

        End Function

#End Region

#Region " SIGN RESOURCE "

        ''' <summary> Gets the signing RSA key. </summary>
        ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
        ''' <value> The RSA signing key. </value>
        Public ReadOnly Property RsaSigningKey() As System.Security.Cryptography.RSA

        ''' <summary> Generates the signature. </summary>
        ''' <remarks> Use this method to generate the signature. </remarks>
        ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
        Public Sub GenerateSignature()

            If Me.RsaSigningKey Is Nothing Then Throw New InvalidOperationException("RSA signing key must be imported from XML file")

            ' assign the key as a signing key.
            Me.SignedXml.SigningKey = Me.RsaSigningKey

            ' Add a KeyInfo to the signature
            Dim thisKeyInfo As New System.Security.Cryptography.Xml.KeyInfo
            thisKeyInfo.AddClause(New System.Security.Cryptography.Xml.RSAKeyValue(Me.RsaSigningKey))
            Me.SignedXml.KeyInfo = thisKeyInfo

            ' Compute the signature.
            Me.SignedXml.ComputeSignature()

        End Sub

        ''' <summary> Saves a rsa signing key. </summary>
        ''' <remarks> David, 10/8/2020. </remarks>
        Public Sub SaveRsaSigningKey()
            Me.SaveRsaSigningKey(My.Settings.SigningKeyPairFullFileName, True)
            Me.SaveRsaSigningKey(My.Settings.VerifyingKeyFullFileName, False)
        End Sub

        ''' <summary> Saves a rsa signing key. </summary>
        ''' <remarks> David, 10/8/2020. </remarks>
        ''' <param name="fullFileName">             Filename of the full file. </param>
        ''' <param name="includePrivateParameters"> True to include, false to exclude the private
        '''                                         parameters. </param>
        Public Sub SaveRsaSigningKey(ByVal fullFileName As String, ByVal includePrivateParameters As Boolean)
            Dim xmlString As String = Me.RsaSigningKey.ToXmlString(includePrivateParameters)
            My.Computer.FileSystem.WriteAllText(fullFileName, xmlString, False)
        End Sub

        ''' <summary> Loads rsa signing key. </summary>
        ''' <remarks> David, 10/8/2020. </remarks>
        Public Sub LoadRsaSigningKey()
            Me.LoadRsaSigningKey(My.Settings.SigningKeyPairFullFileName)
        End Sub

        ''' <summary> Loads rsa verifying key. </summary>
        ''' <remarks> David, 10/8/2020. </remarks>
        Public Sub LoadRsaVerifyingKey()
            Me.LoadRsaSigningKey(My.Settings.VerifyingKeyFullFileName)
        End Sub

        ''' <summary> Loads rsa signing key. </summary>
        ''' <remarks> David, 10/8/2020. </remarks>
        ''' <param name="fullFileName"> Filename of the full file. </param>
        Public Sub LoadRsaSigningKey(ByVal fullFileName As String)
            Me._RsaSigningKey = CreateRsaSigningKeyFromXml(fullFileName)
        End Sub

#End Region

#Region " GET RESOURCE INFO "

        ''' <summary> Gets the outer XML of the signature file. </summary>
        ''' <remarks> Use this property to get the contents of the signature file. </remarks>
        ''' <value> <c>Signature</c> is a String property that can be read from (read only). </value>
        Public ReadOnly Property OuterXml() As String
            Get
                Return Me.SignedXml.GetXml.OuterXml
            End Get
        End Property

        ''' <summary> Fetches a node. </summary>
        ''' <remarks> David, 10/8/2020. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="xmlPath"> The XML document path. </param>
        ''' <returns> The node. </returns>
        Public Overloads Function FetchNode(ByVal xmlPath As String) As System.Xml.XmlNode
            If String.IsNullOrWhiteSpace(xmlPath) Then Throw New System.ArgumentNullException(NameOf(xmlPath))
            Return FetchNode(Me.SignedXml, xmlPath)
        End Function

        ''' <summary> Fetches a node. </summary>
        ''' <remarks> David, 10/8/2020. </remarks>
        ''' <param name="xmlPath"> The XML document path. </param>
        ''' <returns> The node. </returns>
        Public Overloads Function FetchNode(ByVal xmlPath As IList(Of String)) As System.Xml.XmlNode
            Return FetchNode(Me.SignedXml, xmlPath)
        End Function

        ''' <summary> Fetches key information node. </summary>
        ''' <remarks> David, 10/8/2020. </remarks>
        ''' <returns> The key information node. </returns>
        Public Overloads Function FetchKeyInfoNode() As System.Xml.XmlNode
            Return FetchKeyInfoNode(Me.SignedXml)
        End Function

        ''' <summary> Returns the data string containing the signed resource. </summary>
        ''' <remarks> Use this method to get the data including the signed resource is stored. </remarks>
        ''' <returns> The data string containing the signed resource. </returns>
        Public Overloads Function FetchResourceDataOuterXml() As String
            ' return the outer XML of the resource
            Return Me.FetchResourceDataNode.OuterXml
        End Function

        ''' <summary> Returns the resource XML data element. </summary>
        ''' <remarks>
        ''' Use this method to get the resource encapsulated in the signature in its XML data element.
        ''' </remarks>
        ''' <returns> The outer XML of the resource. </returns>
        Public Overloads Function FetchResourceDataNode() As System.Xml.XmlNode
            ' return the outer XML of the resource
            Return FetchResourceDataNode(Me.SignedXml)
        End Function

        ''' <summary> Returns the data string containing the signed resource. </summary>
        ''' <remarks> Use this method to get the data including the signed resource is stored. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="resourceName"> Specifies the name of the resource which to find. </param>
        ''' <returns> The data string containing the signed resource. </returns>
        Public Overloads Function FetchResourceDataOuterXml(ByVal resourceName As String) As String

            If String.IsNullOrWhiteSpace(resourceName) Then Throw New ArgumentNullException(NameOf(resourceName))
            ' return the outer XML of the resource
            Return Me.FetchResourceDataNode(resourceName).OuterXml

        End Function

        ''' <summary> Returns the resource XML data element. </summary>
        ''' <remarks>
        ''' Use this method to get the resource encapsulated in the signature in its XML data element.
        ''' </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="resourceName"> Specifies the name of the resource which to find. </param>
        ''' <returns> The outer XML of the resource. </returns>
        Public Overloads Function FetchResourceDataNode(ByVal resourceName As String) As System.Xml.XmlNode

            If String.IsNullOrWhiteSpace(resourceName) Then Throw New ArgumentNullException(NameOf(resourceName))

            ' return the outer XML of the resource
            Return FetchResourceDataNode(Me.SignedXml, resourceName)

        End Function

        ''' <summary> Gets the resource identifier. </summary>
        ''' <remarks> David, 10/8/2020. </remarks>
        ''' <returns> The resource identifier. </returns>
        Public Function FetchResourceId() As String
            Dim node As Xml.XmlNode = FetchObjectNode(Me.SignedXml)
            Return node.Attributes("Id").Value
        End Function

        ''' <summary> Gets resource name. </summary>
        ''' <remarks> David, 10/8/2020. </remarks>
        ''' <returns> The resource identifier. </returns>
        Public Function FetchResourceName() As String
            Return FetchResourceDataNode(Me.SignedXml).Name
        End Function

        ''' <summary> verifies the signature. </summary>
        ''' <remarks> Use this method to verify the signature. </remarks>
        ''' <returns> <c>True</c> if the signature verified. </returns>
        Public Overloads Function IsVerifySignature() As Boolean

            Return IsVerifySignature(Me.SignedXml.GetXml)

        End Function

        ''' <summary> verifies the signature. </summary>
        ''' <remarks> Use this method to read and verify the signature. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="signatureFileName"> Specifies the XML file name where the data resides. </param>
        ''' <returns> <c>True</c> if the signature verified. </returns>
        Public Overloads Function IsVerifySignature(ByVal signatureFileName As String) As Boolean

            If String.IsNullOrWhiteSpace(signatureFileName) Then Throw New ArgumentNullException(NameOf(signatureFileName))

            ' read the signature
            Me._SignedXml = ReadSignatureFile(signatureFileName)

            ' return true if verified
            Return Me.IsVerifySignature()

        End Function

        ''' <summary> Reads the signature file. </summary>
        ''' <remarks> Use this method to read the signature Information. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="fullFileName"> Specifies the XML file name where the data resides. </param>
        Public Sub ReadSignature(ByVal fullFileName As String)

            If String.IsNullOrWhiteSpace(fullFileName) Then Throw New ArgumentNullException(NameOf(fullFileName))

            ' read and set internal signature object.
            Me._SignedXml = ReadSignatureFile(fullFileName)

        End Sub

        ''' <summary> Writes the signature to the XML file. </summary>
        ''' <remarks> Use this method to write the signature. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="fullFileName"> Specifies the name of the output signature file. </param>
        Public Overloads Sub WriteSignature(ByVal fullFileName As String)

            If String.IsNullOrWhiteSpace(fullFileName) Then Throw New ArgumentNullException(NameOf(fullFileName))
            WriteSignatureFile(Me._SignedXml, fullFileName)
        End Sub

#End Region

#Region " EXAMPLES "

        ''' <summary> Try sign. </summary>
        ''' <remarks>
        ''' <code>
        ''' Dim details As String = String.Empty
        ''' Dim outcome as Boolean = TrySign("xmlsig.xml",details)
        ''' </code>
        ''' </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="xmlFileName"> Specifies the XML file name where the data resides. </param>
        ''' <param name="e">           Cancel details event information. </param>
        ''' <returns> <c>True</c> is okay. </returns>
        Public Shared Function TrySign(ByVal xmlFileName As String, ByVal e As isr.Core.ActionEventArgs) As Boolean
            If e Is Nothing Then Throw New ArgumentNullException(NameOf(e))


            If String.IsNullOrWhiteSpace(xmlFileName) Then Throw New System.ArgumentNullException(NameOf(xmlFileName))

            Dim result As Boolean = False
            Try

                ' The URI to sign.
                Dim resourceToSign As System.Uri = New System.Uri("http://www.microsoft.com")

                ' Generate a signing key.
                Using Key As New System.Security.Cryptography.HMACSHA1
                    Console.WriteLine("Signing: {0}", resourceToSign)

                    ' Sign the detached resource and save the signature in an XML file.
                    SignResource(resourceToSign, xmlFileName, Key)

                    Console.WriteLine("XML signature was successfully computed and saved to {0}.", xmlFileName)

                    ' Verify the signature of the signed XML.
                    Console.WriteLine("Verifying signature...")

                    ' Verify the XML signature in the XML file.
                    result = IsVerifySignatureFile(xmlFileName, Key)
                End Using

                ' Display the results of the signature verification to the console.
                If result Then
                    Console.WriteLine("The XML signature is valid.")
                Else
                    Console.WriteLine("The XML signature is not valid.")
                    e.RegisterFailure("The XML signature is not valid.")
                End If

            Catch ex As System.Security.Cryptography.CryptographicException
                Console.WriteLine(ex.Message)
                e.RegisterError($"Exception {ex.ToFullBlownString}")
            End Try
            Return result

        End Function

#End Region

    End Class

End Namespace

