Imports System.ComponentModel

Imports isr.Core.HashExtensions

Namespace Cryptography

    ''' <summary> Signs system tokens. </summary>
    ''' <remarks>
    ''' (c) 2010 Integrated Scientific Resources, Inc. All rights reserved. <para>
    ''' Licensed under The MIT License.</para><para>
    ''' David, 06/21/03, 1.0.1267.x. </para>
    ''' </remarks>
    Public Class SystemSign
        Inherits isr.IO.Cryptography.TokenSignBase

#Region " CONSTRUCTION and CLEANUP "

        ''' <summary> Default constructor. </summary>
        ''' <remarks> David, 10/8/2020. </remarks>
        Public Sub New()
            MyBase.New()
            Me._SupportedTokenNames = SystemTokenNames.SystemDriveSerialNumber Or SystemTokenNames.SystemDriveSerialNumberHash
            Me._VerifiableTokenNames = SystemTokenNames.SystemDriveSerialNumber Or SystemTokenNames.SystemDriveSerialNumberHash
            Me.ResourceName = "SystemInfo"
            Me.ResourceNamespace = "Values"
        End Sub

#End Region

#Region " SHARED "

        ''' <summary> This shared method gets a system property. </summary>
        ''' <remarks>
        ''' Use Read a system property The following information can be derived (path/property): Path:
        ''' "win32_logicalDisk.DeviceId='c:'" Properties: "VolumeSerialNumber", "deviceId", (see
        ''' win32_logicalDisk)
        ''' Path: "Win32_Processor.DeviceId='CPU0'" Properties: "ProcessorId" Note that some devices
        ''' require two keys in the path.  For these it would be possible to get the required information
        ''' from the enumerated list.
        ''' </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="systemPath">     is a String expression that identifies the system including a
        '''                               system name and key (see remarks). </param>
        ''' <param name="systemProperty"> is a String expression that identifies the system property. </param>
        ''' <returns> A system property. </returns>
        Public Shared Function GetSystemProperty(ByVal systemPath As String, ByVal systemProperty As String) As String

            If String.IsNullOrWhiteSpace(systemPath) Then Throw New ArgumentNullException(NameOf(systemPath))
            If String.IsNullOrWhiteSpace(systemProperty) Then Throw New ArgumentNullException(NameOf(systemProperty))

            Using systemEntity As New System.Management.ManagementObject(systemPath)
                systemEntity.Get()
                Return systemEntity(systemProperty).ToString
            End Using

        End Function

        ''' <summary> This shared method gets a system property. </summary>
        ''' <remarks>
        ''' Use Read a system property The following information can be derived (name/property): Name:
        ''' "Win32_Processor" Properties: "ProcessorId" Name: "Win32_BIOS" Properties: "Version" "BIOS
        ''' Version" Note that some devices require two keys in the path.  For theses it would be
        ''' possible to get the required information from the enumerated list.
        ''' </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="systemName">     is a String expression that identifies the system. </param>
        ''' <param name="systemProperty"> is a String expression that identifies the system property. </param>
        ''' <returns> A system property. </returns>
        Public Shared Function GetSystemPropertyFirstEnum(ByVal systemName As String, ByVal systemProperty As String) As String
            If String.IsNullOrWhiteSpace(systemName) Then Throw New ArgumentNullException(NameOf(systemName))
            If String.IsNullOrWhiteSpace(systemProperty) Then Throw New ArgumentNullException(NameOf(systemProperty))
            Dim result As String = String.Empty
            Using systemEntity As System.Management.ManagementClass = New System.Management.ManagementClass(systemName)
                Dim systemsAvailable As System.Management.ManagementObjectCollection
                Dim oneSystem As System.Management.ManagementObject
                systemsAvailable = systemEntity.GetInstances()
                For Each oneSystem In systemsAvailable
                    Dim mgmObj As Object = oneSystem(systemProperty)
                    If mgmObj IsNot Nothing Then
                        result = mgmObj.ToString
                        Exit For
                    End If
                Next
            End Using
            Return result
        End Function

        ''' <summary> This shared method enumerates the systems. </summary>
        ''' <remarks> David, 10/8/2020. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="systemName">     is a String expression that identifies the system. </param>
        ''' <param name="systemProperty"> is a String expression that identifies the system property. </param>
        ''' <returns> List of systems. </returns>
        ''' <example>
        ''' <code>
        '''   Dim systemName As Object
        '''   Dim systemList As ArrayList
        '''   systemList = SystemSign.EnumerateSystems("Win32_NetworkAdapter")
        '''   me.systemsTextBox.Clear()
        '''   For Each systemName In systemList
        '''     me.systemsTextBox.AppendText(systemName.ToString)
        '''     me.systemsTextBox.AppendText(Environment.NewLine)
        '''   Next
        ''' </code>
        ''' </example>
        Public Shared Function EnumerateSystems(ByVal systemName As String, ByVal systemProperty As String) As ArrayList

            If String.IsNullOrWhiteSpace(systemName) Then Throw New ArgumentNullException(NameOf(systemName))
            If String.IsNullOrWhiteSpace(systemProperty) Then Throw New ArgumentNullException(NameOf(systemProperty))

            Using systemEntity As System.Management.ManagementClass = New System.Management.ManagementClass(systemName)
                Dim systemsAvailable As System.Management.ManagementObjectCollection
                Dim oneSystem As System.Management.ManagementObject
                Dim names As New ArrayList

                systemsAvailable = systemEntity.GetInstances()
                names.Clear()
                For Each oneSystem In systemsAvailable
                    names.Add(oneSystem(systemProperty).ToString)
                Next
                Return names
            End Using
        End Function

#End Region

#Region " BUILD "

        ''' <summary> Adds tokens to the token dictionary. </summary>
        ''' <remarks> David, 10/8/2020. </remarks>
        ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
        Public Overrides Sub AddTokens()
            ' add object per the selected system token
            If (SystemTokenNames.SystemDriveSerialNumber And Me.SupportedTokenNames) <> 0 Then
                Me.AddSystemDriveSerialNumberToken()
            Else
                Throw New InvalidOperationException($"Token of type '{SystemTokenNames.SystemDriveSerialNumber}' is not supported at this time")

            End If
            If (SystemTokenNames.SystemDriveSerialNumberHash And Me.SupportedTokenNames) <> 0 Then
                Me.AddSystemDriveSerialNumberHashToken()
            End If
        End Sub

#End Region

#Region " TOKEN MANAGEMENT "

        ''' <summary> Gets or sets the supported system token names. </summary>
        ''' <remarks>
        ''' Use this property to get or set the type of token(s) the System Token will Create and sign.
        ''' </remarks>
        ''' <value>
        ''' <c>SystemTokenType</c> is a SystemTokenTypes enumerated property that can be read from or
        ''' written too (read or write).
        ''' </value>
        Public Property SupportedTokenNames() As SystemTokenNames

        ''' <summary> Gets or sets a list of names of the verifiable system tokens. </summary>
        ''' <value> A list of names of the verifiable system tokens. </value>
        Public Property VerifiableTokenNames() As SystemTokenNames

        ''' <summary> Verify tokens. </summary>
        ''' <remarks> David, 10/8/2020. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="e"> Cancel details event information. </param>
        ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        Public Overrides Function VerifyTokens(ByVal e As isr.Core.ActionEventArgs) As Boolean
            If e Is Nothing Then Throw New ArgumentNullException(NameOf(e))
            If (SystemTokenNames.SystemDriveSerialNumber And Me.VerifiableTokenNames) <> 0 Then
                Me.VerifySystemDriveSerialNumberToken(e)
            End If
            If Not e.Failed AndAlso ((SystemTokenNames.SystemDriveSerialNumberHash And Me.VerifiableTokenNames) <> 0) Then
                Me.VerifySystemDriveSerialNumberHashToken(e)
            End If
            Return Not e.Failed
        End Function

#Region " SERIAL NUMBER TOKEN "

        ''' <summary> Adds a system token data object based on the hard disk serial number. </summary>
        ''' <remarks> Use this method to add a hard disk serial number system token. </remarks>
        Private Sub AddSystemDriveSerialNumberToken()
            ' get serial number of system drive
            Dim tokenValue As String = GetSystemDriveSerialNumberToken()
            Me.AddToken(SystemTokenName.SystemDriveSerialNumber.ToString, tokenValue)
        End Sub

        ''' <summary> verifies a system token data object based on the hard disk serial number. </summary>
        ''' <remarks>
        ''' Use this method to verify a token based on the system hard disk serial number.
        ''' </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="e"> Action event information. </param>
        ''' <returns> <c>True</c> if verified. </returns>
        Private Function VerifySystemDriveSerialNumberToken(ByVal e As isr.Core.ActionEventArgs) As Boolean
            If e Is Nothing Then Throw New ArgumentNullException(NameOf(e))
            Dim tokenValue As String = GetSystemDriveSerialNumberToken()
            Dim tokenName As String = SystemTokenName.SystemDriveSerialNumber.ToString
            Return Me.VerifyResourceToken(tokenName, tokenValue, e)
        End Function

        ''' <summary> Gets the serial number of the system hard drive. </summary>
        ''' <remarks> Use this method to get the serial number of the system drive. </remarks>
        ''' <returns> The driver serial number. </returns>
        Private Shared Function GetSystemDriveSerialNumberToken() As String

            Dim drive As String = Environment.SystemDirectory.Substring(0, 2)
            Using disk As New System.Management.ManagementObject(String.Format(Globalization.CultureInfo.CurrentCulture,
                                                                             "win32_logicalDisk.DeviceId='{0}'", drive))
                disk.Get()
                Return disk("VolumeSerialNumber").ToString
            End Using

        End Function

#End Region

#Region " SERIAL NUMBER HASH TOKEN "

        ''' <summary> Adds a system token data object based on the hard disk serial number. </summary>
        ''' <remarks> Use this method to add a hard disk serial number system token. </remarks>
        Private Sub AddSystemDriveSerialNumberHashToken()
            ' get serial number of system drive
            Dim tokenValue As String = GetSystemDriveSerialNumberToken.ToBase64Hash
            Me.AddToken(SystemTokenName.SystemDriveSerialNumberHash.ToString, tokenValue)
        End Sub

        ''' <summary> verifies a system token data object based on the hard disk serial number. </summary>
        ''' <remarks>
        ''' Use this method to verify a token based on the system hard disk serial number.
        ''' </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="e"> Action event information. </param>
        ''' <returns> <c>True</c> if verified. </returns>
        Private Function VerifySystemDriveSerialNumberHashToken(ByVal e As isr.Core.ActionEventArgs) As Boolean
            If e Is Nothing Then Throw New ArgumentNullException(NameOf(e))
            Dim tokenValue As String = GetSystemDriveSerialNumberToken.ToBase64Hash
            Dim tokenName As String = SystemTokenName.SystemDriveSerialNumberHash.ToString
            Return Me.VerifyResourceToken(tokenName, tokenValue, e)
        End Function

#End Region


#End Region

    End Class

    ''' <summary> Enumerates which system tokens are supported. </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    <Flags()> Public Enum SystemTokenNames

        ''' <summary> . </summary>
        <Description("None")> None

        ''' <summary> System Drive Serial Number. </summary>
        <Description("System Drive Serial Number")> SystemDriveSerialNumber = 1

        ''' <summary> System Drive Serial Number Hash. </summary>
        <Description("System Drive Serial Number Hash")> SystemDriveSerialNumberHash = SystemDriveSerialNumber << 1

        ''' <summary>  Distance between two hidden system files. </summary>
        <Description("File Distance")> FileDistance = SystemDriveSerialNumberHash << 1

        ''' <summary> c:\ creation date and time. </summary>
        <Description("System Drive Creation Date")> SystemDriveCreationDate = FileDistance << 1

        ''' <summary> CPU ID. </summary>
        <Description("Processor Id")> ProcessorId = SystemDriveCreationDate << 1

        ''' <summary>  Bios Version. </summary>
        <Description("Bios Version")> BiosVersion = ProcessorId << 1
        <Description("All Options")> AllOptions = (BiosVersion << 1) - 1
    End Enum

    ''' <summary> Values that represent system token names. </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    Public Enum SystemTokenName

        ''' <summary> . </summary>
        <Description("None")> None

        ''' <summary> Name of the file name token. </summary>
        <Description("File Version")> FileName = BaseTokenName.FileName

        ''' <summary> Name of the file name token. </summary>
        <Description("File Version")> FileVersion = BaseTokenName.FileVersion

        ''' <summary> An enum constant representing the system drive serial number option. </summary>
        <Description("System Drive Serial Number")> SystemDriveSerialNumber

        ''' <summary> An enum constant representing the system drive serial number hash option. </summary>
        <Description("System Drive Serial Number Hash")> SystemDriveSerialNumberHash

        ''' <summary>  Distance between two hidden system files. </summary>
        <Description("File Distance")> FileDistance

        ''' <summary> c:\ creation date and time. </summary>
        <Description("System Drive Creation Date")> SystemDriveCreationDate

        ''' <summary> CPU ID. </summary>
        <Description("Processor Id")> ProcessorId

        ''' <summary>  Bios Version. </summary> 
        <Description("Bios Version")> BiosVersion
    End Enum

End Namespace


