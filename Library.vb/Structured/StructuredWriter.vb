''' <summary> Writes delimited files. </summary>
''' <remarks>
''' (c) 2006 Integrated Scientific Resources, Inc. All rights reserved. <para>
''' Licensed under The MIT License. </para><para>  
''' David, 04/20/06, 1.1.2301.x. </para>
''' </remarks>
Public Class StructuredWriter

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Constructs this class. </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    Public Sub New()
        Me.New(StructuredReader.DefaultFilePathName)
    End Sub

    ''' <summary> Constructs this class. </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    ''' <param name="filePathName"> Specifies the file path name. </param>
    Public Sub New(ByVal filePathName As String)
        MyBase.New()
        Me._FilePathName = filePathName
        Me._Delimiter = ","
    End Sub

#End Region

#Region " FILE "

    ''' <summary> Holds true if fields are to be enclosed in quotes. </summary>
    ''' <value> The has fields enclosed in quotes. </value>
    Public Property HasFieldsEnclosedInQuotes() As Boolean

    ''' <summary> Full pathname of the file. </summary>
    Private _FilePathName As String = String.Empty

    ''' <summary> Gets or sets the file name. </summary>
    ''' <remarks> Use this property to get or set the file name. </remarks>
    ''' <value> <c>FilePathName</c> is a String property. </value>
    Public Property FilePathName() As String
        Get
            If Me._FilePathName.Length = 0 Then
                ' set default file name if empty.
                Me._FilePathName = StructuredReader.DefaultFilePathName
            End If
            Return Me._FilePathName
        End Get
        Set(ByVal value As String)
            Me._FilePathName = value
        End Set
    End Property

#End Region

#Region " FIELDS "

    ''' <summary> Adds a "T:String" value to the current <see cref="Fields">record</see>. </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    ''' <param name="value"> Specifies the value to add to the record. </param>
    ''' <returns> The added value. </returns>
    Public Function AddField(ByVal value As String) As String
        Me.Fields.Add(value)
        Return value
    End Function

    ''' <summary>
    ''' Adds a <see cref="T:Boolean"/> value to the current <see cref="Fields">record</see>.
    ''' </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    ''' <param name="value"> Specifies the value to add to the record. </param>
    ''' <returns> The added value converted to string. </returns>
    Public Function AddField(ByVal value As Boolean) As String
        Return Me.AddField(value.ToString(Globalization.CultureInfo.CurrentCulture))
    End Function

    ''' <summary>
    ''' Adds a <see cref="T:Byte"/> value to the current <see cref="Fields">record</see>.
    ''' </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    ''' <param name="value"> Specifies the value to add to the record. </param>
    ''' <returns> The added value converted to string. </returns>
    Public Function AddField(ByVal value As Byte) As String
        Return Me.AddField(value.ToString(Globalization.CultureInfo.CurrentCulture))
    End Function

    ''' <summary>
    ''' Adds a <see cref="T:Byte"/> value to the current <see cref="Fields">record</see>.
    ''' </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    ''' <param name="value">  Specifies the value to add to the record. </param>
    ''' <param name="format"> Specifies the format to use when expressing the value. </param>
    ''' <returns> The added value converted to string. </returns>
    Public Function AddField(ByVal value As Byte, ByVal format As String) As String
        Return Me.AddField(value.ToString(format, Globalization.CultureInfo.CurrentCulture))
    End Function

    ''' <summary>
    ''' Adds a <see cref="T:DateTime"/> value to the current <see cref="Fields">record</see>.
    ''' </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    ''' <param name="value"> Specifies the value to add to the record. </param>
    ''' <returns> The added value converted to string. </returns>
    Public Function AddField(ByVal value As DateTime) As String
        Return Me.AddField(value.ToString(Globalization.CultureInfo.CurrentCulture))
    End Function

    ''' <summary>
    ''' Adds a <see cref="T:DateTime"/> value to the current <see cref="Fields">record</see>.
    ''' </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    ''' <param name="value">  Specifies the value to add to the record. </param>
    ''' <param name="format"> Specifies the format to use when expressing the value. </param>
    ''' <returns> The added value converted to string. </returns>
    Public Function AddField(ByVal value As DateTime, ByVal format As String) As String
        Return Me.AddField(value.ToString(format, Globalization.CultureInfo.CurrentCulture))
    End Function

    ''' <summary>
    ''' Adds a "T:DateTimeOffset" value to the current <see cref="Fields">record</see>.
    ''' </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    ''' <param name="value"> Specifies the value to add to the record. </param>
    ''' <returns> The added value. </returns>
    Public Function AddField(ByVal value As DateTimeOffset) As String
        Return Me.AddField(value.ToString(Globalization.CultureInfo.CurrentCulture))
    End Function

    ''' <summary>
    ''' Adds a <see cref="T:DateTimeOffset"/> value to the current <see cref="Fields">record</see>.
    ''' </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    ''' <param name="value">  Specifies the value to add to the record. </param>
    ''' <param name="format"> Specifies the format to use when expressing the value. </param>
    ''' <returns> The added value converted to string. </returns>
    Public Function AddField(ByVal value As DateTimeOffset, ByVal format As String) As String
        Return Me.AddField(value.ToString(format, Globalization.CultureInfo.CurrentCulture))
    End Function

    ''' <summary>
    ''' Adds a <see cref="T:Decimal"/> value to the current <see cref="Fields">record</see>.
    ''' </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    ''' <param name="value"> Specifies the value to add to the record. </param>
    ''' <returns> The added value converted to string. </returns>
    Public Function AddField(ByVal value As Decimal) As String
        Return Me.AddField(value.ToString(Globalization.CultureInfo.CurrentCulture))
    End Function

    ''' <summary>
    ''' Adds a <see cref="T:Decimal"/> value to the current <see cref="Fields">record</see>.
    ''' </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    ''' <param name="value">  Specifies the value to add to the record. </param>
    ''' <param name="format"> Specifies the format to use when expressing the value. </param>
    ''' <returns> The added value converted to string. </returns>
    Public Function AddField(ByVal value As Decimal, ByVal format As String) As String
        Return Me.AddField(value.ToString(format, Globalization.CultureInfo.CurrentCulture))
    End Function

    ''' <summary> Adds a "T:Double" value to the current <see cref="Fields">record</see>. </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    ''' <param name="value"> Specifies the value to add to the record. </param>
    ''' <returns> The added value converted to string. </returns>
    Public Function AddField(ByVal value As Double) As String
        Return Me.AddField(value.ToString(Globalization.CultureInfo.CurrentCulture))
    End Function

    ''' <summary> Adds a "T:Double" value to the current <see cref="Fields">record</see>. </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    ''' <param name="value">  Specifies the value to add to the record. </param>
    ''' <param name="format"> Specifies the format to use when expressing the value. </param>
    ''' <returns> The added value converted to string. </returns>
    Public Function AddField(ByVal value As Double, ByVal format As String) As String
        Return Me.AddField(value.ToString(format, Globalization.CultureInfo.CurrentCulture))
    End Function

    ''' <summary>
    ''' Adds a <see cref="T:Int64"/> value to the current <see cref="Fields">record</see>.
    ''' </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    ''' <param name="value"> Specifies the value to add to the record. </param>
    ''' <returns> The added value converted to string. </returns>
    Public Function AddField(ByVal value As Int64) As String
        Return Me.AddField(value.ToString(Globalization.CultureInfo.CurrentCulture))
    End Function

    ''' <summary>
    ''' Adds a <see cref="T:Int64"/> value to the current <see cref="Fields">record</see>.
    ''' </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    ''' <param name="value">  Specifies the value to add to the record. </param>
    ''' <param name="format"> Specifies the format to use when expressing the value. </param>
    ''' <returns> The added value converted to string. </returns>
    Public Function AddField(ByVal value As Int64, ByVal format As String) As String
        Return Me.AddField(value.ToString(format, Globalization.CultureInfo.CurrentCulture))
    End Function

    ''' <summary>
    ''' Adds a <see cref="T:Single"/> value to the current <see cref="Fields">record</see>.
    ''' </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    ''' <param name="value"> Specifies the value to add to the record. </param>
    ''' <returns> The added value converted to string. </returns>
    Public Function AddField(ByVal value As Single) As String
        Return Me.AddField(value.ToString(Globalization.CultureInfo.CurrentCulture))
    End Function

    ''' <summary>
    ''' Adds a <see cref="T:Single"/> value to the current <see cref="Fields">record</see>.
    ''' </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    ''' <param name="value">  Specifies the value to add to the record. </param>
    ''' <param name="format"> Specifies the format to use when expressing the value. </param>
    ''' <returns> The added value converted to string. </returns>
    Public Function AddField(ByVal value As Single, ByVal format As String) As String
        Return Me.AddField(value.ToString(format, Globalization.CultureInfo.CurrentCulture))
    End Function

#End Region

#Region " FIELDS METHODS AND PROPERTIES "

    ''' <summary> Holds the file delimiter. </summary>
    ''' <value> The delimiter. </value>
    Public Property Delimiter() As String

    ''' <summary> The fields. </summary>
    Private _Fields As ArrayList

    ''' <summary> Holds "T:ArrayList" of field values. </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    ''' <returns> The list of field values. </returns>
    Public Function Fields() As ArrayList
        If Me._Fields Is Nothing Then
            Me._Fields = New ArrayList
        End If
        Return Me._Fields
    End Function

    ''' <summary> Builds the current record for saving to the file. </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    ''' <param name="newLine"> Specifies if the record is being appended as a new line. </param>
    ''' <returns> The delimited record. </returns>
    Public Function BuildRecord(ByVal newLine As Boolean) As String
        Dim record As New Text.StringBuilder
        For Each field As String In Me.Fields
            If record.Length > 0 Then
                record.Append(Me._Delimiter)
            End If
            If Me._HasFieldsEnclosedInQuotes Then
                record.AppendFormat(Globalization.CultureInfo.CurrentCulture, """{0}""", field)
            Else
                record.Append(field)
            End If
        Next
        If newLine Then
            record.Insert(0, Environment.NewLine)
        End If
        Return record.ToString
    End Function

    ''' <summary> <c>true</c> if this object is new. </summary>
    Private _IsNew As Boolean

    ''' <summary> Tag the next write as creating a new file. </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    Public Sub NewFile()
        Me._IsNew = True
    End Sub

    ''' <summary> Creates a new record. </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    Public Sub NewRecord()
        Me._Fields = New ArrayList
    End Sub

    ''' <summary> Writes the current fields record to the file. </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    Public Sub WriteFields()
        ' append to he file if it exists and not requesting a new file.
        Dim append As Boolean = (Not Me._IsNew) AndAlso My.Computer.FileSystem.FileExists(Me.FilePathName)
        My.Computer.FileSystem.WriteAllText(Me.FilePathName, Me.BuildRecord(append), append)
        ' toggle the new flag so as the append from now on.
        Me._IsNew = False
    End Sub

#End Region

End Class

