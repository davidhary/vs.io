''' <summary> Reads delimited files. </summary>
''' <remarks>
''' (c) 2006 Integrated Scientific Resources, Inc. All rights reserved. <para>
''' Licensed under The MIT License. </para><para>  
''' David, 04/20/06, 1.1.2301.x. </para>
''' </remarks>
Public Class StructuredReader

    Implements IDisposable

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Constructs this class. </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    Public Sub New()

        ' instantiate the base class
        Me.New(DefaultFilePathName)

    End Sub

    ''' <summary> Constructs this class. </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    ''' <param name="filePathName"> Specifies the file path name. </param>
    Public Sub New(ByVal filePathName As String)

        ' instantiate the base class
        MyBase.New()

        Me._FilePathName = filePathName

        ' open the parser
        Me._Parser = New Microsoft.VisualBasic.FileIO.TextFieldParser(Me._FilePathName) With {
            .TextFieldType = Microsoft.VisualBasic.FileIO.FieldType.Delimited,
            .Delimiters = New String() {",", Microsoft.VisualBasic.vbTab}
        }

    End Sub

    ''' <summary> Calls <see cref="M:Dispose(Boolean Disposing)"/> to cleanup. </summary>
    ''' <remarks>
    ''' Do not make this method Overridable (virtual) because a derived class should not be able to
    ''' override this method.
    ''' </remarks>
    Public Sub Dispose() Implements IDisposable.Dispose

        ' Do not change this code.  Put cleanup code in Dispose(Boolean) below.

        ' this disposes all child classes.
        Me.Dispose(True)

        ' Take this object off the finalization(Queue) and prevent finalization code 
        ' from executing a second time.
        GC.SuppressFinalize(Me)

    End Sub

    ''' <summary>
    ''' Gets the dispose status sentinel of the base class.  This applies to the derived class
    ''' provided proper implementation.
    ''' </summary>
    ''' <value> The is disposed. </value>
    Protected Property IsDisposed() As Boolean

    ''' <summary>
    ''' Releases the unmanaged resources used by the <see cref="T:System.Windows.Forms.Control" />
    ''' and its child controls and optionally releases the managed resources.
    ''' </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    ''' <param name="disposing"> <c>True</c> to release both managed and unmanaged resources;
    '''                                                   <c>False</c> to release only unmanaged
    '''                                                   resources when called from the runtime
    '''                                                   finalize. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overridable Sub Dispose(ByVal disposing As Boolean)
        Try
            If Not Me.IsDisposed AndAlso disposing Then
                If Me._Parser IsNot Nothing Then Me._Parser.Close() : Me._Parser.Dispose() : Me._Parser = Nothing
            End If
        Finally
            Me.IsDisposed = True
        End Try
    End Sub

#End Region

#Region " SHARED "

    ''' <summary> Gets the default extension. </summary>
    Public Const DefaultExtension As String = ".csv"

    ''' <summary>
    ''' Returns the default file name for the structured I/O.  This would be the executable file name
    ''' appended with the
    ''' <see cref="DefaultExtension">default extension</see>.
    ''' </summary>
    ''' <value> The default file path name. </value>
    Public Shared ReadOnly Property DefaultFilePathName() As String
        Get
            Return Core.ApplicationInfo.BuildApplicationConfigFileName(DefaultExtension)
        End Get
    End Property

    ''' <summary>
    ''' Return file rows as a jagged array allowing each row to be of different length.
    ''' </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    ''' <param name="filePathName"> Specifies the file path name. </param>
    ''' <returns> The array of arrays of rows. </returns>
    Public Shared Function ReadRows(ByVal filePathName As String) As Collections.ObjectModel.ReadOnlyCollection(Of String())
        Dim list As New Generic.List(Of String())
        Using MyReader As New Microsoft.VisualBasic.FileIO.TextFieldParser(filePathName)
            MyReader.TextFieldType = Microsoft.VisualBasic.FileIO.FieldType.Delimited
            MyReader.Delimiters = New String() {","}
            'MyReader.HasFieldsEnclosedInQuotes'
            Dim currentRow As String()
            'Loop through all of the fields in the file. 
            'If any lines are corrupt, report an error and continue parsing. 
            While Not MyReader.EndOfData
                currentRow = MyReader.ReadFields()
                If (currentRow IsNot Nothing) AndAlso currentRow.Length > 0 Then
                    list.Add(currentRow)
                End If
            End While
        End Using
        Return New Collections.ObjectModel.ReadOnlyCollection(Of String())(list)
    End Function

    ''' <summary>
    ''' Return file rows as a jagged array allowing each row to be of different length.
    ''' </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    ''' <param name="filePathName"> Specifies the file path name. </param>
    ''' <returns> The array of arrays of rows. </returns>
    Public Shared Function ReadAllRows(ByVal filePathName As String) As String()()

        Dim data()() As String = Array.Empty(Of String())()
        Using MyReader As New Microsoft.VisualBasic.FileIO.TextFieldParser(filePathName)
            MyReader.TextFieldType = Microsoft.VisualBasic.FileIO.FieldType.Delimited
            MyReader.Delimiters = New String() {","}
            'MyReader.HasFieldsEnclosedInQuotes'
            Dim currentRow As String()
            'Loop through all of the fields in the file. 
            'If any lines are corrupt, report an error and continue parsing. 
            Dim n As Integer = 0
            While Not MyReader.EndOfData
                currentRow = MyReader.ReadFields()
                If (currentRow IsNot Nothing) AndAlso currentRow.Length > 0 Then
                    ReDim Preserve data(data.GetLength(0))
                    data(n) = currentRow
                End If
                n += 1
            End While
        End Using
        Return data

    End Function

#End Region

#Region " FILE NAME "

    ''' <summary> Full pathname of the file. </summary>
    Private _FilePathName As String = String.Empty

    ''' <summary> Gets or sets the file name. </summary>
    ''' <remarks> Use this property to get or set the file name. </remarks>
    ''' <value> <c>FilePathName</c> is a String property. </value>
    Public Property FilePathName() As String
        Get
            If Me._FilePathName.Length = 0 Then
                ' set default file name if empty.
                Me._FilePathName = DefaultFilePathName
            End If
            Return Me._FilePathName
        End Get
        Set(ByVal value As String)
            Me._FilePathName = value
        End Set
    End Property

#End Region

#Region " FIELDS: PARSE "

    ''' <summary>
    ''' Specifies the field index from which to read a field value. Resets to zero upon reading a new
    ''' record.
    ''' </summary>
    ''' <exception cref="InvalidOperationException">   Thrown when the requested operation is
    '''                                                invalid. </exception>
    ''' <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
    '''                                                the required range. </exception>
    ''' <value> The field index. </value>
    Public Property FieldIndex() As Integer

    ''' <summary>
    ''' Returns the field for the specified <paramref name="index">index</paramref>.
    ''' </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    ''' <exception cref="InvalidOperationException">   Thrown when the requested operation is
    '''                                                invalid. </exception>
    ''' <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
    '''                                                the required range. </exception>
    ''' <param name="index"> Specifies the index in the fields record from which to get the value. </param>
    ''' <returns> The field value. </returns>
    Public Function SelectField(ByVal index As Integer) As String
        If Me.Fields Is Nothing Then
            Throw New InvalidOperationException("Record not available")
        ElseIf index < 0 OrElse index >= Me._Fields.Length Then
            Throw New ArgumentOutOfRangeException(NameOf(index), index,
                String.Format(Globalization.CultureInfo.CurrentCulture, "Must be positive value less than {0}", Me._Fields.Length))
        End If
        Return Me._Fields(index)
    End Function

    ''' <summary> Tries to select field, returning empty if nothing. </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    ''' <param name="index"> Specifies the index in the fields record from which to get the value. </param>
    ''' <returns> A String. </returns>
    Public Function TrySelectField(ByVal index As Integer) As String
        If Me.Fields Is Nothing Then
            Return String.Empty
        ElseIf index < 0 OrElse index >= Me._Fields.Length Then
            Return String.Empty
        End If
        Return Me._Fields(index)
    End Function

    ''' <summary>
    ''' Returns the field for the current <see cref="FieldIndex">index</see>. Increments the current
    ''' field index.
    ''' </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    ''' <returns> The field value. </returns>
    Public Function SelectField() As String
        Me.FieldIndex += 1
        Return Me.SelectField(Me.FieldIndex - 1)
    End Function

    ''' <summary>
    ''' Returns a <see cref="T:Boolean"/> field value for the specified field
    ''' <paramref name="index">index</paramref>.
    ''' </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    ''' <param name="index"> Specifies the index in the fields record from which to get the value. </param>
    ''' <param name="value"> A dummy value. </param>
    ''' <returns> The <see cref="T:Boolean"/> field value. </returns>
    <CodeAnalysis.SuppressMessage("Style", "IDE0060:Remove unused parameter", Justification:="<Pending>")>
    Public Function ParseField(ByVal index As Integer, ByVal value As Boolean) As Boolean
        Return Boolean.Parse(Me.SelectField(index))
    End Function

    ''' <summary>
    ''' Returns a <see cref="T:Boolean"/> field value for the current
    ''' <see cref="FieldIndex">index</see>. Increments the current field index.
    ''' </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    ''' <param name="value"> A dummy value. </param>
    ''' <returns> The <see cref="T:Boolean"/> field value. </returns>
    Public Function ParseField(ByVal value As Boolean) As Boolean
        Me.FieldIndex += 1
        Return Me.ParseField(Me.FieldIndex - 1, value)
    End Function

    ''' <summary>
    ''' Returns a <see cref="T:Byte"/> field value for the specified field
    ''' <paramref name="index">index</paramref>.
    ''' </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    ''' <param name="index"> Specifies the index in the fields record from which to get the value. </param>
    ''' <param name="value"> A dummy value. </param>
    ''' <returns> The <see cref="T:Byte"/> field value. </returns>
    <CodeAnalysis.SuppressMessage("Style", "IDE0060:Remove unused parameter", Justification:="<Pending>")>
    Public Function ParseField(ByVal index As Integer, ByVal value As Byte) As Byte
        Return Byte.Parse(Me.SelectField(index), Globalization.NumberStyles.Integer, Globalization.CultureInfo.CurrentCulture)
    End Function

    ''' <summary>
    ''' Returns a <see cref="T:Byte"/> field value for the current
    ''' <see cref="FieldIndex">index</see>. Increments the current field index.
    ''' </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    ''' <param name="value"> A dummy value. </param>
    ''' <returns> The  <see cref="T:Byte"/> field value. </returns>
    Public Function ParseField(ByVal value As Byte) As Byte
        Me.FieldIndex += 1
        Return Me.ParseField(Me.FieldIndex - 1, value)
    End Function

    ''' <summary>
    ''' Returns a <see cref="T:DateTime"/> field value for the specified field
    ''' <paramref name="index">index</paramref>.
    ''' </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    ''' <param name="index"> Specifies the index in the fields record from which to get the value. </param>
    ''' <param name="value"> A dummy value. </param>
    ''' <returns> The field value. </returns>
    <CodeAnalysis.SuppressMessage("Style", "IDE0060:Remove unused parameter", Justification:="<Pending>")>
    Public Function ParseField(ByVal index As Integer, ByVal value As DateTime) As DateTime
        Return DateTime.Parse(Me.SelectField(index), Globalization.CultureInfo.CurrentCulture)
    End Function

    ''' <summary>
    ''' Returns a <see cref="T:DateTime"/> field value for the current
    ''' <see cref="FieldIndex">index</see>. Increments the current field index.
    ''' </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    ''' <param name="value"> A dummy value. </param>
    ''' <returns> The field value. </returns>
    Public Function ParseField(ByVal value As DateTime) As DateTime
        Me.FieldIndex += 1
        Return Me.ParseField(Me.FieldIndex - 1, value)
    End Function

    ''' <summary>
    ''' Returns a <see cref="T:DateTimeOffset"/> field value for the specified field.
    ''' </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    ''' <param name="index"> The field index. </param>
    ''' <param name="value"> A dummy value. </param>
    ''' <returns> The <see cref="T:Boolean"/> field value. </returns>
    <CodeAnalysis.SuppressMessage("Style", "IDE0060:Remove unused parameter", Justification:="<Pending>")>
    Public Function ParseField(ByVal index As Integer, ByVal value As DateTimeOffset) As DateTimeOffset
        Return DateTime.Parse(Me.SelectField(index), Globalization.CultureInfo.CurrentCulture)
    End Function

    ''' <summary>
    ''' Returns a <see cref="T:DateTimeOffset"/> field value for the specified field.
    ''' </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    ''' <param name="value"> A dummy value. </param>
    ''' <returns> The <see cref="T:Boolean"/> field value. </returns>
    Public Function ParseField(ByVal value As DateTimeOffset) As DateTimeOffset
        Me.FieldIndex += 1
        Return Me.ParseField(Me.FieldIndex - 1, value)
    End Function

    ''' <summary>
    ''' Returns a <see cref="T:Decimal"/> field value for the specified field
    ''' <paramref name="index">index</paramref>.
    ''' </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    ''' <param name="index"> Specifies the index in the fields record from which to get the value. </param>
    ''' <param name="value"> A dummy value. </param>
    ''' <returns> The field value. </returns>
    <CodeAnalysis.SuppressMessage("Style", "IDE0060:Remove unused parameter", Justification:="<Pending>")>
    Public Function ParseField(ByVal index As Integer, ByVal value As Decimal) As Decimal
        Return Decimal.Parse(Me.SelectField(index), Globalization.NumberStyles.Float, Globalization.CultureInfo.CurrentCulture)
    End Function

    ''' <summary>
    ''' Returns a <see cref="T:Decimal"/> field value for the current
    ''' <see cref="FieldIndex">index</see>. Increments the current field index.
    ''' </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    ''' <param name="value"> A dummy value. </param>
    ''' <returns> The field value. </returns>
    Public Function ParseField(ByVal value As Decimal) As Decimal
        Me.FieldIndex += 1
        Return Me.ParseField(Me.FieldIndex - 1, value)
    End Function

    ''' <summary>
    ''' Returns a <see cref="T:Double"/> field value for the specified field
    ''' <paramref name="index">index</paramref>.
    ''' </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    ''' <param name="index"> Specifies the index in the fields record from which to get the value. </param>
    ''' <param name="value"> A dummy value. </param>
    ''' <returns> The field value. </returns>
    <CodeAnalysis.SuppressMessage("Style", "IDE0060:Remove unused parameter", Justification:="<Pending>")>
    Public Function ParseField(ByVal index As Integer, ByVal value As Double) As Double
        Return Double.Parse(Me.SelectField(index), Globalization.NumberStyles.Float, Globalization.CultureInfo.CurrentCulture)
    End Function

    ''' <summary>
    ''' Returns a <see cref="T:Double"/> field value for the current
    ''' <see cref="FieldIndex">index</see>. Increments the current field index.
    ''' </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    ''' <param name="value"> A dummy value. </param>
    ''' <returns> The field value. </returns>
    Public Function ParseField(ByVal value As Double) As Double
        Me.FieldIndex += 1
        Return Me.ParseField(Me.FieldIndex - 1, value)
    End Function

    ''' <summary>
    ''' Returns a <see cref="T:Int16"/> field value for the specified field
    ''' <paramref name="index">index</paramref>.
    ''' </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    ''' <param name="index"> Specifies the index in the fields record from which to get the value. </param>
    ''' <param name="value"> A dummy value. </param>
    ''' <returns> The field value. </returns>
    <CodeAnalysis.SuppressMessage("Style", "IDE0060:Remove unused parameter", Justification:="<Pending>")>
    Public Function ParseField(ByVal index As Integer, ByVal value As Int16) As Int16
        Return Int16.Parse(Me.SelectField(index), Globalization.NumberStyles.Number, Globalization.CultureInfo.CurrentCulture)
    End Function

    ''' <summary>
    ''' Returns a <see cref="T:Int16"/> field value for the current
    ''' <see cref="FieldIndex">index</see>. Increments the current field index.
    ''' </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    ''' <param name="value"> A dummy value. </param>
    ''' <returns> The field value. </returns>
    Public Function ParseField(ByVal value As Int16) As Int16
        Me.FieldIndex += 1
        Return Me.ParseField(Me.FieldIndex - 1, value)
    End Function

    ''' <summary>
    ''' Returns a <see cref="T:Int32"/> field value for the specified field
    ''' <paramref name="index">index</paramref>.
    ''' </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    ''' <param name="index"> Specifies the index in the fields record from which to get the value. </param>
    ''' <param name="value"> A dummy value. </param>
    ''' <returns> The field value. </returns>
    <CodeAnalysis.SuppressMessage("Style", "IDE0060:Remove unused parameter", Justification:="<Pending>")>
    Public Function ParseField(ByVal index As Integer, ByVal value As Int32) As Int32
        Return Int32.Parse(Me.SelectField(index), Globalization.NumberStyles.Number, Globalization.CultureInfo.CurrentCulture)
    End Function

    ''' <summary>
    ''' Returns a <see cref="T:Int32"/> field value for the current
    ''' <see cref="FieldIndex">index</see>. Increments the current field index.
    ''' </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    ''' <param name="value"> A dummy value. </param>
    ''' <returns> The field value. </returns>
    Public Function ParseField(ByVal value As Int32) As Int32
        Me.FieldIndex += 1
        Return Me.ParseField(Me.FieldIndex - 1, value)
    End Function

    ''' <summary>
    ''' Returns a <see cref="T:Int64"/> field value for the specified field
    ''' <paramref name="index">index</paramref>.
    ''' </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    ''' <param name="index"> Specifies the index in the fields record from which to get the value. </param>
    ''' <param name="value"> A dummy value. </param>
    ''' <returns> The field value. </returns>
    <CodeAnalysis.SuppressMessage("Style", "IDE0060:Remove unused parameter", Justification:="<Pending>")>
    Public Function ParseField(ByVal index As Integer, ByVal value As Int64) As Int64
        Return Int64.Parse(Me.SelectField(index), Globalization.NumberStyles.Number, Globalization.CultureInfo.CurrentCulture)
    End Function

    ''' <summary>
    ''' Returns a <see cref="T:Int64"/> field value for the current
    ''' <see cref="FieldIndex">index</see>. Increments the current field index.
    ''' </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    ''' <param name="value"> A dummy value. </param>
    ''' <returns> The field value. </returns>
    Public Function ParseField(ByVal value As Int64) As Int64
        Me.FieldIndex += 1
        Return Me.ParseField(Me.FieldIndex - 1, value)
    End Function

    ''' <summary>
    ''' Returns a <see cref="T:Single"/> field value for the specified field
    ''' <paramref name="index">index</paramref>.
    ''' </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    ''' <param name="index"> Specifies the index in the fields record from which to get the value. </param>
    ''' <param name="value"> A dummy value. </param>
    ''' <returns> The field value. </returns>
    <CodeAnalysis.SuppressMessage("Style", "IDE0060:Remove unused parameter", Justification:="<Pending>")>
    Public Function ParseField(ByVal index As Integer, ByVal value As Single) As Single
        Return Single.Parse(Me.SelectField(index), Globalization.NumberStyles.Float, Globalization.CultureInfo.CurrentCulture)
    End Function

    ''' <summary>
    ''' Returns a <see cref="T:Single"/> field value for the current
    ''' <see cref="FieldIndex">index</see>. Increments the current field index.
    ''' </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    ''' <param name="value"> A dummy value. </param>
    ''' <returns> The field value. </returns>
    Public Function ParseField(ByVal value As Single) As Single
        Me.FieldIndex += 1
        Return Me.ParseField(Me.FieldIndex - 1, value)
    End Function

    ''' <summary>
    ''' Returns a <see cref="T:TimeSpan"/> field value for the specified field
    ''' <paramref name="index">index</paramref>.
    ''' </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    ''' <param name="index"> Specifies the index in the fields record from which to get the value. </param>
    ''' <param name="value"> A dummy value. </param>
    ''' <returns> The field value. </returns>
    <CodeAnalysis.SuppressMessage("Style", "IDE0060:Remove unused parameter", Justification:="<Pending>")>
    Public Function ParseField(ByVal index As Integer, ByVal value As TimeSpan) As TimeSpan
        Return TimeSpan.Parse(Me.SelectField(index))
    End Function

    ''' <summary>
    ''' Returns a <see cref="T:TimeSpan"/> field value for the current
    ''' <see cref="FieldIndex">index</see>. Increments the current field index.
    ''' </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    ''' <param name="value"> A dummy value. </param>
    ''' <returns> The field value. </returns>
    Public Function ParseField(ByVal value As TimeSpan) As TimeSpan
        Me.FieldIndex += 1
        Return Me.ParseField(Me.FieldIndex - 1, value)
    End Function

#End Region

#Region " FIELDS: TRY PARSE "

    ''' <summary>
    ''' Returns a <see cref="T:Boolean"/> field value for the specified field
    ''' <paramref name="index">index</paramref>.
    ''' </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    ''' <param name="index"> Specifies the index in the fields record from which to get the value. </param>
    ''' <param name="value"> [in,out] parsed value. </param>
    ''' <returns> The <see cref="T:Boolean"/> field value. </returns>
    Public Function TryParseField(ByVal index As Integer, ByRef value As Boolean) As Boolean
        Return Boolean.TryParse(Me.SelectField(index), value)
    End Function

    ''' <summary>
    ''' Returns a <see cref="T:Boolean"/> field value for the current
    ''' <see cref="FieldIndex">index</see>. Increments the current field index.
    ''' </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    ''' <param name="value"> [in,out] parsed value. </param>
    ''' <returns> The <see cref="T:Boolean"/> field value. </returns>
    Public Function TryParseField(ByRef value As Boolean) As Boolean
        Me.FieldIndex += 1
        Return Me.TryParseField(Me.FieldIndex - 1, value)
    End Function

    ''' <summary>
    ''' Returns a <see cref="T:Byte"/> field value for the specified field
    ''' <paramref name="index">index</paramref>.
    ''' </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    ''' <param name="index"> Specifies the index in the fields record from which to get the value. </param>
    ''' <param name="value"> [in,out] parsed value. </param>
    ''' <returns> The <see cref="T:Byte"/> field value. </returns>
    Public Function TryParseField(ByVal index As Integer, ByRef value As Byte) As Boolean
        Return Byte.TryParse(Me.SelectField(index), Globalization.NumberStyles.Number, Globalization.CultureInfo.CurrentCulture, value)
    End Function

    ''' <summary>
    ''' Returns a <see cref="T:Byte"/> field value for the current
    ''' <see cref="FieldIndex">index</see>. Increments the current field index.
    ''' </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    ''' <param name="value"> [in,out] parsed value. </param>
    ''' <returns> The  <see cref="T:Byte"/> field value. </returns>
    Public Function TryParseField(ByRef value As Byte) As Boolean
        Me.FieldIndex += 1
        Return Me.TryParseField(Me.FieldIndex - 1, value)
    End Function

    ''' <summary>
    ''' Returns a <see cref="T:DateTime"/> field value for the specified field
    ''' <paramref name="index">index</paramref>.
    ''' </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    ''' <param name="index"> Specifies the index in the fields record from which to get the value. </param>
    ''' <param name="value"> [in,out] parsed value. </param>
    ''' <returns> The field value. </returns>
    Public Function TryParseField(ByVal index As Integer, ByRef value As DateTime) As Boolean
        Return DateTime.TryParse(Me.SelectField(index), Globalization.CultureInfo.CurrentCulture, Globalization.DateTimeStyles.None, value)
    End Function

    ''' <summary>
    ''' Returns a <see cref="T:DateTime"/> field value for the current
    ''' <see cref="FieldIndex">index</see>. Increments the current field index.
    ''' </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    ''' <param name="value"> [in,out] parsed value. </param>
    ''' <returns> The field value. </returns>
    Public Function TryParseField(ByRef value As DateTime) As Boolean
        Me.FieldIndex += 1
        Return Me.TryParseField(Me.FieldIndex - 1, value)
    End Function

    ''' <summary>
    ''' Returns a <see cref="T:DateTimeOffset"/> field value for the specified field.
    ''' </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    ''' <param name="index"> index. </param>
    ''' <param name="value"> [in,out] parsed value. </param>
    ''' <returns> The <see cref="T:Boolean"/> field value. </returns>
    Public Function TryParseField(ByVal index As Integer, ByRef value As DateTimeOffset) As Boolean
        Return DateTimeOffset.TryParse(Me.SelectField(index), Globalization.CultureInfo.CurrentCulture, Globalization.DateTimeStyles.None, value)
    End Function

    ''' <summary>
    ''' Returns a <see cref="T:DateTimeOffset"/> field value for the current
    ''' <see cref="FieldIndex">index</see>. Increments the current field index.
    ''' </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    ''' <param name="value"> [in,out] parsed value. </param>
    ''' <returns> The <see cref="T:Boolean"/> field value. </returns>
    Public Function TryParseField(ByRef value As DateTimeOffset) As Boolean
        Me.FieldIndex += 1
        Return Me.TryParseField(Me.FieldIndex - 1, value)
    End Function

    ''' <summary>
    ''' Returns a <see cref="T:Decimal"/> field value for the specified field
    ''' <paramref name="index">index</paramref>.
    ''' </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    ''' <param name="index"> Specifies the index in the fields record from which to get the value. </param>
    ''' <param name="value"> [in,out] parsed value. </param>
    ''' <returns> The field value. </returns>
    Public Function TryParseField(ByVal index As Integer, ByRef value As Decimal) As Boolean
        Return Decimal.TryParse(Me.SelectField(index), Globalization.NumberStyles.Float, Globalization.CultureInfo.CurrentCulture, value)
    End Function

    ''' <summary>
    ''' Returns a <see cref="T:Decimal"/> field value for the current
    ''' <see cref="FieldIndex">index</see>. Increments the current field index.
    ''' </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    ''' <param name="value"> [in,out] A dummy value. </param>
    ''' <returns> The field value. </returns>
    Public Function TryParseField(ByRef value As Decimal) As Boolean
        Me.FieldIndex += 1
        Return Me.TryParseField(Me._FieldIndex - 1, value)
    End Function

    ''' <summary>
    ''' Returns a <see cref="T:Double"/> field value for the specified field
    ''' <paramref name="index">index</paramref>.
    ''' </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    ''' <param name="index"> Specifies the index in the fields record from which to get the value. </param>
    ''' <param name="value"> [in,out] parsed value. </param>
    ''' <returns> The field value. </returns>
    Public Function TryParseField(ByVal index As Integer, ByRef value As Double) As Boolean
        Return Double.TryParse(Me.SelectField(index), Globalization.NumberStyles.Float, Globalization.CultureInfo.CurrentCulture, value)
    End Function

    ''' <summary>
    ''' Returns a <see cref="T:Double"/> field value for the current
    ''' <see cref="FieldIndex">index</see>. Increments the current field index.
    ''' </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    ''' <param name="value"> [in,out] parsed value. </param>
    ''' <returns> The field value. </returns>
    Public Function TryParseField(ByRef value As Double) As Boolean
        Me.FieldIndex += 1
        Return Me.TryParseField(Me.FieldIndex - 1, value)
    End Function

    ''' <summary>
    ''' Returns a <see cref="T:Int16"/> field value for the specified field
    ''' <paramref name="index">index</paramref>.
    ''' </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    ''' <param name="index"> Specifies the index in the fields record from which to get the value. </param>
    ''' <param name="value"> [in,out] parsed value. </param>
    ''' <returns> The field value. </returns>
    Public Function TryParseField(ByVal index As Integer, ByRef value As Int16) As Boolean
        Return Int16.TryParse(Me.SelectField(index), Globalization.NumberStyles.Number, Globalization.CultureInfo.CurrentCulture, value)
    End Function

    ''' <summary>
    ''' Returns a <see cref="T:Int16"/> field value for the current
    ''' <see cref="FieldIndex">index</see>. Increments the current field index.
    ''' </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    ''' <param name="value"> [in,out] parsed value. </param>
    ''' <returns> The field value. </returns>
    Public Function TryParseField(ByRef value As Int16) As Boolean
        Me.FieldIndex += 1
        Return Me.TryParseField(Me.FieldIndex - 1, value)
    End Function

    ''' <summary>
    ''' Returns a <see cref="T:Int32"/> field value for the specified field
    ''' <paramref name="index">index</paramref>.
    ''' </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    ''' <param name="index"> Specifies the index in the fields record from which to get the value. </param>
    ''' <param name="value"> [in,out] parsed value. </param>
    ''' <returns> The field value. </returns>
    Public Function TryParseField(ByVal index As Integer, ByRef value As Int32) As Boolean
        Return Int32.TryParse(Me.SelectField(index), Globalization.NumberStyles.Number, Globalization.CultureInfo.CurrentCulture, value)
    End Function

    ''' <summary>
    ''' Returns a <see cref="T:Int32"/> field value for the current
    ''' <see cref="FieldIndex">index</see>. Increments the current field index.
    ''' </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    ''' <param name="value"> [in,out] parsed value. </param>
    ''' <returns> The field value. </returns>
    Public Function TryParseField(ByRef value As Int32) As Boolean
        Me.FieldIndex += 1
        Return Me.TryParseField(Me.FieldIndex - 1, value)
    End Function

    ''' <summary>
    ''' Returns a <see cref="T:Int64"/> field value for the specified field
    ''' <paramref name="index">index</paramref>.
    ''' </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    ''' <param name="index"> Specifies the index in the fields record from which to get the value. </param>
    ''' <param name="value"> [in,out] A dummy value. </param>
    ''' <returns> The field value. </returns>
    Public Function TryParseField(ByVal index As Integer, ByRef value As Int64) As Boolean
        Return Int64.TryParse(Me.SelectField(index), Globalization.NumberStyles.Number, Globalization.CultureInfo.CurrentCulture, value)
    End Function

    ''' <summary>
    ''' Returns a <see cref="T:Int64"/> field value for the current
    ''' <see cref="FieldIndex">index</see>. Increments the current field index.
    ''' </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    ''' <param name="value"> [in,out] parsed value. </param>
    ''' <returns> The field value. </returns>
    Public Function TryParseField(ByRef value As Int64) As Boolean
        Me.FieldIndex += 1
        Return Me.TryParseField(Me.FieldIndex - 1, value)
    End Function

    ''' <summary>
    ''' Returns a <see cref="T:Single"/> field value for the specified field
    ''' <paramref name="index">index</paramref>.
    ''' </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    ''' <param name="index"> Specifies the index in the fields record from which to get the value. </param>
    ''' <param name="value"> [in,out] parsed value. </param>
    ''' <returns> The field value. </returns>
    Public Function TryParseField(ByVal index As Integer, ByRef value As Single) As Boolean
        Return Single.TryParse(Me.SelectField(index), Globalization.NumberStyles.Float, Globalization.CultureInfo.CurrentCulture, value)
    End Function

    ''' <summary>
    ''' Returns a <see cref="T:Single"/> field value for the current
    ''' <see cref="FieldIndex">index</see>. Increments the current field index.
    ''' </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    ''' <param name="value"> [in,out] parsed value. </param>
    ''' <returns> The field value. </returns>
    Public Function TryParseField(ByRef value As Single) As Boolean
        Me.FieldIndex += 1
        Return Me.TryParseField(Me.FieldIndex - 1, value)
    End Function

    ''' <summary>
    ''' Returns a <see cref="T:TimeSpan"/> field value for the specified field
    ''' <paramref name="index">index</paramref>.
    ''' </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    ''' <param name="index"> Specifies the index in the fields record from which to get the value. </param>
    ''' <param name="value"> [in,out] parsed value. </param>
    ''' <returns> The field value. </returns>
    Public Function TryParseField(ByVal index As Integer, ByRef value As TimeSpan) As Boolean
        Return TimeSpan.TryParse(Me.SelectField(index), value)
    End Function

    ''' <summary>
    ''' Returns a <see cref="T:TimeSpan"/> field value for the current
    ''' <see cref="FieldIndex">index</see>. Increments the current field index.
    ''' </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    ''' <param name="value"> [in,out] parsed value. </param>
    ''' <returns> The field value. </returns>
    Public Function TryParseField(ByRef value As TimeSpan) As Boolean
        Me.FieldIndex += 1
        Return Me.TryParseField(Me.FieldIndex - 1, value)
    End Function

#End Region

#Region " FIELDS READER "

    ''' <summary> The fields. </summary>
    Private _Fields As String() = Array.Empty(Of String)()

    ''' <summary> Returns the last record that was read from the file. </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    ''' <returns> The last record that was read from the file. </returns>
    Public Function Fields() As String()
        Return Me._Fields
    End Function

    ''' <summary> Returns <c>True</c> if the parser read fields from the file. </summary>
    ''' <value> The has fields. </value>
    Public ReadOnly Property HasFields() As Boolean
        Get
            Return Me._Fields IsNot Nothing AndAlso Me._Fields.Length > 0
        End Get
    End Property

    ''' <summary>
    ''' Reads a record of fields from the file. Skips to cursor to the next line containing data.
    ''' </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    ''' <returns> <c>true</c> if has new data or false it end of file or no data. </returns>
    Public Function ReadFields() As Boolean
        Me.FieldIndex = 0
        Me._Fields = Array.Empty(Of String)()
        If Not Me.Parser.EndOfData Then
            Me._Fields = Me.Parser.ReadFields()
        End If
        Return Me.HasFields
    End Function

    ''' <summary> Reads the next record and returns an array of doubles. </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    ''' <param name="value"> The value. </param>
    ''' <returns> The fields converted to <see cref="T:Double"/>. </returns>
    Public Function ReadFields(ByVal value As Double) As Double()

        If Me.ReadFields() Then

            ' allocate data array
            Dim data(Me._Fields.Length - 1) As Double

            For i As Int32 = 0 To Me._Fields.Length - 1
                data(i) = Me.ParseField(i, value)
            Next
            Return data

        Else

            ' return the empty array 
            Dim data() As Double = Array.Empty(Of Double)()
            Return data

        End If

    End Function

    ''' <summary>
    ''' Reads the next record and returns an array of doubles skipping an field that failed to parse.
    ''' </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    ''' <param name="value"> The value. </param>
    ''' <returns> The fields converted to <see cref="T:Double"/>. </returns>
    <CodeAnalysis.SuppressMessage("Style", "IDE0060:Remove unused parameter", Justification:="<Pending>")>
    Public Function TryReadFields(ByVal value As Double) As Double()

        If Me.ReadFields() Then

            ' allocate data array
            Dim data(Me._Fields.Length - 1) As Double

            For i As Int32 = 0 To Me._Fields.Length - 1
                Me.TryParseField(i, data(i))
            Next
            Return data

        Else

            ' return the empty array 
            Dim data() As Double = Array.Empty(Of Double)()
            Return data

        End If

    End Function

    ''' <summary>
    ''' Reads a record of fields from the file. This read is contiguous--it requires that the line
    ''' number of the record is contiguous with the previous line. Normally, the
    ''' <see cref="Parser">parser</see> skips empty line reading from the next line with data. Skips
    ''' to cursor to the next line containing data.
    ''' </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    ''' <returns> <c>true</c> if has new data or false it end of file or no data. </returns>
    Public Function ReadFieldsContiguous() As Boolean
        Me.FieldIndex = 0
        Me._Fields = Array.Empty(Of String)()
        Dim currentLine As Long = Me.Parser.LineNumber
        If Not Me.Parser.EndOfData Then
            Me._Fields = Me.Parser.ReadFields()
            If Me.Parser.LineNumber > currentLine + 1 Then
                Me._Fields = Array.Empty(Of String)()
            End If
        End If
        Return Me.HasFields
    End Function

    ''' <summary>
    ''' Reads the next record and returns an array of doubles. This read is contiguous--it requires
    ''' that the line number of the record is contiguous with the previous line. Normally, the
    ''' <see cref="Parser">parser</see> skips empty line reading from the next line with data. Skips
    ''' to cursor to the next line containing data.
    ''' </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    ''' <param name="value"> The value. </param>
    ''' <returns> The fields converted to <see cref="T:Double"/>. </returns>
    Public Function ReadFieldsContiguous(ByVal value As Double) As Double()

        If Me.ReadFieldsContiguous() Then

            ' allocate data array
            Dim data(Me._Fields.Length - 1) As Double

            For i As Int32 = 0 To Me._Fields.Length - 1
                data(i) = Me.ParseField(i, value)
            Next
            Return data

        Else

            ' return the empty array 
            Dim data() As Double = Array.Empty(Of Double)()
            Return data

        End If

    End Function

    ''' <summary>
    ''' Reads the next record and returns an array of doubles skipping an field that failed to parse.
    ''' </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    ''' <param name="value"> The value. </param>
    ''' <returns> The fields converted to <see cref="T:Double"/>. </returns>
    <CodeAnalysis.SuppressMessage("Style", "IDE0060:Remove unused parameter", Justification:="<Pending>")>
    Public Function TryReadFieldsContiguous(ByVal value As Double) As Double()

        If Me.ReadFieldsContiguous() Then

            ' allocate data array
            Dim data(Me._Fields.Length - 1) As Double

            For i As Int32 = 0 To Me._Fields.Length - 1
                Me.TryParseField(i, data(i))
            Next
            Return data

        Else

            ' return the empty array 
            Dim data() As Double = Array.Empty(Of Double)()
            Return data

        End If

    End Function

    ''' <summary> Skips the given line count. </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    ''' <exception cref="System.IO.EndOfStreamException"> Thrown when the end of the stream was
    '''                                                    unexpectedly reached. </exception>
    ''' <param name="lineCount"> Number of lines. </param>
    '''
    Public Sub Skip(ByVal lineCount As Integer)
        Do While lineCount > 0 AndAlso Not Me.Parser.EndOfData
            lineCount -= 1
            Me.Parser.ReadLine()
        Loop
        If Me.Parser.EndOfData AndAlso lineCount > 0 Then
            Throw New System.IO.EndOfStreamException("Tried to read past end of file when skipping lines")
        End If
    End Sub

    ''' <summary> Skip until fields start with. </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    ''' <exception cref="System.IO.EndOfStreamException"> Thrown when the end of the stream was
    '''                                                    unexpectedly reached. </exception>
    ''' <param name="value"> A dummy value. </param>
    Public Sub SkipUntilFieldsStartWith(ByVal value As String)
        Dim found As Boolean

        Do
            Me.ReadFields()
            found = Me.SelectField(0).StartsWith(value, StringComparison.OrdinalIgnoreCase)
        Loop Until found OrElse Me.Parser.EndOfData
        If Not found Then
            Throw New System.IO.EndOfStreamException(String.Format("Reached end of file before finding '{0}'", value))
        End If
    End Sub

    ''' <summary> Skip until start with. </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    ''' <exception cref="System.IO.EndOfStreamException"> Thrown when the end of the stream was
    '''                                                    unexpectedly reached. </exception>
    ''' <param name="value"> A dummy value. </param>
    Public Sub SkipUntilStartWith(ByVal value As String)
        Dim found As Boolean
        Do
            found = Me.Parser.ReadLine().StartsWith(value, StringComparison.OrdinalIgnoreCase)
        Loop Until found OrElse Me.Parser.EndOfData
        If Not found Then
            Throw New System.IO.EndOfStreamException(String.Format("Reached end of file before finding '{0}'", value))
        End If
    End Sub

#End Region

#Region " PARSER: READ ROWS "

    ''' <summary>
    ''' Return file contiguous rows as a jagged two-dimensional array allowing each row to be of
    ''' different length.
    ''' </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    ''' <param name="value"> A dummy value. </param>
    ''' <returns> Array of arrays. </returns>
    Public Function ReadContiguousRows(ByVal value As Double) As Double()()

        Dim data()() As Double = New Double(0)() {}
        ReDim Preserve data(0 To 999)
        Dim rowIndex As Int32
        Dim isRowEmpty As Boolean = False
        Do While Not (isRowEmpty OrElse Me._Parser.EndOfData)
            Dim oneDimensionValues As Double() = Me.ReadFieldsContiguous(value)
            isRowEmpty = oneDimensionValues Is Nothing OrElse oneDimensionValues.Length = 0
            If Not isRowEmpty Then
                If rowIndex >= (data.GetLength(0)) Then
                    ReDim Preserve data(0 To rowIndex + 1000)
                End If
                data(rowIndex) = oneDimensionValues
                rowIndex += 1
            End If
        Loop
        ReDim Preserve data(0 To rowIndex - 1)
        Return data
    End Function

    ''' <summary>
    ''' Returns file contiguous rows as a jagged two-dimensional array allowing each row to be of
    ''' different length. Any field that failed to parse is skipped.
    ''' </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    ''' <param name="value"> A dummy value. </param>
    ''' <returns> Array of arrays. </returns>
    Public Function TryReadContiguousRows(ByVal value As Double) As Double()()

        Dim data()() As Double = New Double(0)() {}
        ReDim Preserve data(0 To 999)
        Dim rowIndex As Int32
        Dim isRowEmpty As Boolean = False
        Do While Not (isRowEmpty OrElse Me._Parser.EndOfData)
            Dim oneDimensionValues As Double() = Me.TryReadFieldsContiguous(value)
            isRowEmpty = oneDimensionValues Is Nothing OrElse oneDimensionValues.Length = 0
            If Not isRowEmpty Then
                If rowIndex >= (data.GetLength(0)) Then
                    ReDim Preserve data(0 To rowIndex + 1000)
                End If
                data(rowIndex) = oneDimensionValues
                rowIndex += 1
            End If
        Loop
        ReDim Preserve data(0 To rowIndex - 1)
        Return data
    End Function

#End Region

#Region " PARSER "

    ''' <summary> Returns <c>True</c> if the parser is open. </summary>
    ''' <value> <c>IsOpen</c>Is a Boolean property. </value>
    Public ReadOnly Property IsOpen() As Boolean
        Get
            Return Me._Parser IsNot Nothing
        End Get
    End Property

    ''' <summary> The parser. </summary>
    Private _Parser As Microsoft.VisualBasic.FileIO.TextFieldParser

    ''' <summary>
    ''' Returns reference to the
    ''' <see cref="Microsoft.VisualBasic.FileIO.TextFieldParser">text field parser</see>
    ''' for reading the file.
    ''' </summary>
    ''' <value> The parser. </value>
    Public ReadOnly Property Parser() As Microsoft.VisualBasic.FileIO.TextFieldParser
        Get
            Return Me._Parser
        End Get
    End Property

#End Region

End Class
