## ISR IO Libraries<sub>&trade;</sub>: I/O Class Library
* [History](#Revision-History)
* [License](#The-MIT-License)
* [Open Source](#Open-Source)
* [Closed software](#Closed-software)

### Revision History [](#){name=Revision-History}

*4.0.7140 07/20/19*  
Adds unit tests for the profile scribe. Renames
Profile Scriber to Profile Scribe and adds read functionality for using
settings name. Replaces string collection for item lists with Enumerable
of string.

*4.0.7112 06/22/19*  
Breaks backwards compatibility of the Excel reader due
to the break in the Jet provider, which no longer works.

*3.2.6667 04/03/18*  
2018 release.

*3.1.6505 10/23/17*  
Breaking Delimited File Dataset: Replaces skip first
row with skip first row.

*3.0.6393 07/03/17*  
Breaking. Changes file dialogs to always return dialog
result.

*2.1.6384 06/24/17*  
Defaults to UTC time.

*2.1.6295 03/27/17*  
Adds Delimited File dataset.

*2.1.6174 11/26/16*  
Cryptography: Supports key management, using a known
key, and trust validation based on the original signing key.

*2.0.6173 11/25/16*  
Cryptography: Modifies XML Signature. Changes break
backwards compatibility. Supports license signature file.

*1.2.5718 08/28/15 Uses null-conditionals. Requires VS 2015 and above.

*1.2.5303 07/09/14*  
Adds file watcher classes.

*1.2.5211 04/08/14*  
Structured Reader: Adds methods to read contiguous
records, returning nothing if the parser skipped a row.

*1.2.5204 04/01/14*  
Updates Structured reader to parse and try parse
values.

*1.2.4710 11/23/12*  
Removes .VB tags from assemblies.

*1.2.4504 05/01/12*  
Splits off a solution for x86 because x64 does not
support OLEDB.4.0 that is used for reading Excel files. Adds x86
project.

*1.2.4498 04/17/12*  
Implements code analysis rules for .NET 4.0.

*1.2.4232 08/03/11*  
Standardize code elements and documentation.

*1.2.4213 07/15/11*  
Simplifies the assembly information.

*1.2.3294 01/07/09*  
Add Excel Import and Excel reader and interface. Add
object parser.

*1.2.3141 08/07/08*  
Net List Reader: Add support for military style pins.
Allow populating the connector before reading. Add mating side and
relative pin number.

*1.2.2961 02/09/08*  
Update to .NET 3.5.

*1.1.2908 12/18/07*  
Upgrade to Visual Studio 2008. Add Net List classes.
Add unit tests for Structured I/O and net list. Remove NUNIT tests.

*1.1.2301 04/20/06*  
Upgrade to Visual Studio 2005.

*1.0.2257 03/07/06*  
Add binary extended reader and writer.

*1.0.2219 01/28/06*  
Remove Visual Basic import.

*1.0.2206 01/15/06*  
Use path name for the full file and folder names. New
support, core, and exception classes. Use Int32, Int64, and Int16
instead of Integer, Long, and Short.

*1.0.2205 01/14/06*  
Use path name for folder and file name specifications.

*1.0.1933 04/17/05 Create from Delimited File, XML Sign, and Token Sign.

\(C\) 2005 Integrated Scientific Resources, Inc. All rights reserved.

### The MIT License [](#){name=The-MIT-License}
THE SOFTWARE IS PROVIDED \"AS IS\", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

This software was developed and tested using Microsoft<sup>&reg;</sup> [Visual Studio](https://www.visualstudIO.com/) 2019.  

Source code for this project is hosted on [Bit Bucket](https://bitbucket.org/davidhary).

### Open source  [](#){name=Open-Source}
Open source used by this software is described and licensed at the
following sites:  
[Core Libraries](https://bitbucket.org/davidhary/vs.core)  
[IO Libraries](https://bitbucket.org/davidhary/vs.IO)  
[Excel Data Manipulation Using VB.NET](http://www.CodeProject.com/KB/vb/ExcelDataManipulation.aspx)  
[File System Watcher Pure Chaos](http://www.codeproject.com/Articles/58740/FileSystemWatcher-Pure-Chaos-Part-of)  
[Text file data set](https://www.codeproject.com/Articles/22400/Converting-text-files-CSV-to-datasets)
