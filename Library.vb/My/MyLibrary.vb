﻿Namespace My

    ''' <summary> Provides assembly information for the class library. </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    Partial Public NotInheritable Class MyLibrary

        ''' <summary>
        ''' Constructor that prevents a default instance of this class from being created.
        ''' </summary>
        ''' <remarks> David, 10/8/2020. </remarks>
        Private Sub New()
            MyBase.New()
        End Sub

        ''' <summary> Returns True if this is 64 bit process. </summary>
        ''' <value> The 64 bit process sentinel. </value>
        Public Shared ReadOnly Property Is64BitProcess As Boolean
            Get
                Return IntPtr.Size = 8
            End Get
        End Property

        ''' <summary> Gets the identifier of the trace source. </summary>
        Public Const TraceEventId As Integer = ProjectTraceEventId.CoreLibrary

        ''' <summary> The assembly title. </summary>
        Public Const AssemblyTitle As String = "IO Core Library"

        ''' <summary> Information describing the assembly. </summary>
        Public Const AssemblyDescription As String = "IO Core Library"

        ''' <summary> The assembly product. </summary>
        Public Const AssemblyProduct As String = "IO.Core"

    End Class

    ''' <summary> Values that represent project trace event identifiers. </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    Public Enum ProjectTraceEventId

        ''' <summary> An enum constant representing the none option. </summary>
        <System.ComponentModel.Description("Not specified")> None

        ''' <summary> . </summary>
        <System.ComponentModel.Description("IO Library")> CoreLibrary = Core.ProjectTraceEventId.IO

        ''' <summary> . </summary>
        <System.ComponentModel.Description("IO Tester")> IOTester = CoreLibrary + &HA

        ''' <summary> . </summary>
        <System.ComponentModel.Description("File Watcher Tester")> FileWatcherTester = CoreLibrary + &HB

        ''' <summary> . </summary>
        <System.ComponentModel.Description("IO Units")> CoreUnitTest = CoreLibrary + &HC
    End Enum

End Namespace

