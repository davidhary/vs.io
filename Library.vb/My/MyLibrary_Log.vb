Imports isr.Core
Imports isr.IO.ExceptionExtensions
Namespace My

    Partial Public NotInheritable Class MyLibrary

        ''' <summary> Logs unpublished exception. </summary>
        ''' <remarks> David, 10/8/2020. </remarks>
        ''' <param name="activity">  The activity. </param>
        ''' <param name="exception"> The exception. </param>
        Public Shared Sub LogUnpublishedException(ByVal activity As String, ByVal exception As Exception)
            LogUnpublishedMessage(New TraceMessage(TraceEventType.Error, TraceEventId, $"Exception {activity};. {exception.ToFullBlownString}"))
        End Sub

        ''' <summary> Applies the given value. </summary>
        ''' <remarks> David, 10/8/2020. </remarks>
        ''' <param name="value"> The value. </param>
        Public Shared Sub Apply(ByVal value As isr.Core.Logger)
            _Logger = value
        End Sub

        ''' <summary> Applies the trace level described by value. </summary>
        ''' <remarks> David, 10/8/2020. </remarks>
        ''' <param name="value"> The value. </param>
        Public Shared Sub ApplyTraceLogLevel(ByVal value As TraceEventType)
            TraceLevel = value
            Logger.ApplyTraceLevel(value)
        End Sub

        ''' <summary> Applies the trace level described by value. </summary>
        ''' <remarks> David, 10/8/2020. </remarks>
        Public Shared Sub ApplyTraceLogLevel()
            ApplyTraceLogLevel(TraceLevel)
        End Sub

    End Class

End Namespace
