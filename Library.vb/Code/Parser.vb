''' <summary> Parses object values. </summary>
''' <remarks>
''' (c) 2010 Integrated Scientific Resources, Inc. All rights reserved. <para>
''' Licensed under The MIT License. </para><para>  
''' David, 10/01/09, 3.0.3296.x. </para>
''' </remarks>
Public NotInheritable Class Parser

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Prevent constructing this class. </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    Private Sub New()
        MyBase.New()
    End Sub

#End Region

#Region " NULL OR EMPTY "

    ''' <summary> Gets or sets the DB Null string as stored in Excel. </summary>
    ''' <value> The database null string. </value>
    Public Shared ReadOnly Property DBNullString() As String = "#NULL#"

    ''' <summary> Returns <c>True</c> if the value is a DB Null. </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    ''' <param name="value"> Specifies the parsed value. </param>
    ''' <returns> <c>true</c> if database null; otherwise <c>false</c>. </returns>
    Public Shared Function IsDBNull(ByVal value As Object) As Boolean
        Return value IsNot Nothing AndAlso (TypeOf value Is DBNull)
    End Function

    ''' <summary> Returns <c>True</c> if the value is empty. </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    ''' <param name="value"> Specifies the parsed value. </param>
    ''' <returns> <c>true</c> if empty; otherwise <c>false</c>. </returns>
    Public Shared Function IsEmpty(ByVal value As Object) As Boolean
        Return value Is Nothing OrElse ((TypeOf value Is String) AndAlso String.IsNullOrWhiteSpace(value.ToString))
    End Function

    ''' <summary>
    ''' Returns <c>True</c> if the specified cell is empty or includes a DB Null value.
    ''' </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    ''' <param name="value"> Specifies the parsed value. </param>
    ''' <returns> <c>true</c> if a database null or is empty; otherwise <c>false</c>. </returns>
    Public Shared Function IsDBNullOrEmpty(ByVal value As Object) As Boolean
        Return IsDBNull(value) OrElse IsEmpty(value)
    End Function

#End Region

#Region " OBJECT PARSERS "

    ''' <summary> Returns a value or DB Null. </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    ''' <param name="value"> Specifies the parsed value. </param>
    ''' <returns> An Object. </returns>
    Public Shared Function Parse(ByVal value As Object) As Object
        If value IsNot Nothing AndAlso (TypeOf value Is String) AndAlso
            Convert.ToString(value, Globalization.CultureInfo.CurrentCulture).Equals(DBNullString) Then
            value = DBNull.Value
        End If
        Return value
    End Function

    ''' <summary> Returns the value of the given cell as a Nullable Boolean type. </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    ''' <param name="value">        Specifies the parsed value. </param>
    ''' <param name="defaultValue"> Specifies a default value. </param>
    ''' <returns> A Boolean? </returns>
    Public Shared Function Parse(ByVal value As Object, ByVal defaultValue As Boolean?) As Boolean?

        If (TypeOf value Is Boolean) Then
            Return CType(value, Boolean)
        ElseIf TypeOf value Is String Then
            Dim output As Boolean
            Return If(Boolean.TryParse(Convert.ToString(value, Globalization.CultureInfo.CurrentCulture), output), output, defaultValue)
        Else
            Return defaultValue
        End If

    End Function

    ''' <summary> Returns the value of the specified cell as a Boolean type. </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    ''' <param name="value">        Specifies the parsed value. </param>
    ''' <param name="defaultValue"> Specifies a default value. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c>. </returns>
    Public Shared Function Parse(ByVal value As Object, ByVal defaultValue As Boolean) As Boolean
        Return Parse(value, New Boolean?(defaultValue)).Value
    End Function

    ''' <summary> Returns the value of the specified cell as a Nullable Date type. </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    ''' <param name="value">        Specifies the parsed value. </param>
    ''' <param name="defaultValue"> Specifies a default value. </param>
    ''' <returns> A DateTime? </returns>
    Public Shared Function Parse(ByVal value As Object, ByVal defaultValue As DateTime?) As DateTime?

        Dim output As Double? = Parse(value, New Double?)
        Return If(output.HasValue, DateTime.FromOADate(output.Value), defaultValue)

    End Function

    ''' <summary>
    ''' Returns the value of the specified cell as a <see cref="T:DataType">DateTime</see>
    ''' type.
    ''' </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    ''' <param name="value">        Specifies the parsed value. </param>
    ''' <param name="defaultValue"> Specifies a default value. </param>
    ''' <returns> A DateTime. </returns>
    Public Shared Function Parse(ByVal value As Object, ByVal defaultValue As DateTime) As DateTime
        Return Parse(value, New DateTime?(defaultValue)).Value
    End Function

    ''' <summary> Returns the value of the specified cell as a Nullable Double type. </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    ''' <param name="value">        Specifies the parsed value. </param>
    ''' <param name="defaultValue"> Specifies a default value. </param>
    ''' <returns> A Double? </returns>
    Public Shared Function Parse(ByVal value As Object, ByVal defaultValue As Double?) As Double?

        If value Is Nothing Then
            Return defaultValue
        Else
            If TypeOf value Is Double Then
                Dim numericValue As Double
                numericValue = CType(value, Double)
                Return If(Double.IsNaN(numericValue), defaultValue, numericValue)
            ElseIf TypeOf value Is String Then
                Dim textValue As String = Convert.ToString(value, Globalization.CultureInfo.CurrentCulture).Trim
                If textValue.StartsWith("&", StringComparison.OrdinalIgnoreCase) Then
                    Dim numericValue As Long
                    textValue = textValue.TrimStart("&hH".ToCharArray)
                    Return If(Long.TryParse(textValue, Globalization.NumberStyles.AllowHexSpecifier,
                                     Globalization.CultureInfo.CurrentCulture, numericValue),
                        numericValue,
                        defaultValue)
                ElseIf textValue.StartsWith("0x", StringComparison.OrdinalIgnoreCase) Then
                    Dim numericValue As Long
                    textValue = textValue.Substring(2)
                    Return If(Long.TryParse(textValue, Globalization.NumberStyles.AllowHexSpecifier,
                                     Globalization.CultureInfo.CurrentCulture, numericValue),
                        numericValue,
                        defaultValue)
                Else
                    Dim numericValue As Double
                    Return If(Double.TryParse(textValue, Globalization.NumberStyles.Number Or
                                       Globalization.NumberStyles.AllowExponent,
                                       Globalization.CultureInfo.CurrentCulture, numericValue),
                        numericValue,
                        defaultValue)
                End If
            Else
                Return defaultValue
            End If
        End If

    End Function

    ''' <summary> Returns the value of the specified cell as a double type. </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    ''' <param name="value">        Specifies the parsed value. </param>
    ''' <param name="defaultValue"> Specifies a default value. </param>
    ''' <returns> A Double. </returns>
    Public Shared Function Parse(ByVal value As Object, ByVal defaultValue As Double) As Double
        Return Parse(value, New Double?(defaultValue)).Value
    End Function

    ''' <summary> Returns the value of the specified cell as a Nullable Integer type. </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    ''' <param name="value">        Specifies the parsed value. </param>
    ''' <param name="defaultValue"> Specifies a default value. </param>
    ''' <returns> An Integer? </returns>
    Public Shared Function Parse(ByVal value As Object, ByVal defaultValue As Integer?) As Integer?

        Dim output As Double? = Parse(value, New Double?)
        Return If(output.HasValue, Convert.ToInt32(output.Value), defaultValue)

    End Function

    ''' <summary> Returns the value of the specified cell as a Integer type. </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    ''' <param name="value">        Specifies the parsed value. </param>
    ''' <param name="defaultValue"> Specifies a default value. </param>
    ''' <returns> An Integer. </returns>
    Public Shared Function Parse(ByVal value As Object, ByVal defaultValue As Integer) As Integer
        Return Parse(value, New Integer?(defaultValue)).Value
    End Function

    ''' <summary> Returns a cell value or default value of String type. </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    ''' <param name="value">        Specifies the parsed value. </param>
    ''' <param name="defaultValue"> Specifies a default value. </param>
    ''' <returns> A String. </returns>
    Public Shared Function Parse(ByVal value As Object, ByVal defaultValue As String) As String
        Return If(IsDBNullOrEmpty(value), defaultValue, Convert.ToString(value, Globalization.CultureInfo.CurrentCulture))
    End Function

#End Region

End Class
