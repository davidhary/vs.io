''' <summary>
''' Extends the <see cref="System.IO.BinaryWriter">system binary reader</see> class.
''' </summary>
''' <remarks>
''' (c) 2010 Integrated Scientific Resources, Inc. All rights reserved. <para>
''' Licensed under The MIT License. </para><para>  
''' David, 03/07/2006, 1.0.2257.x. </para>
''' </remarks>
Public Class BinaryWriter

    Inherits System.IO.BinaryWriter

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Constructs this class. </summary>
    ''' <remarks> Use this constructor to open a binary reader for the specified file. </remarks>
    ''' <param name="filePathName"> Specifies the file name. </param>
    ''' <param name="fileMode">     Specifies the <see cref="System.IO.FileMode">file mode</see>.
    '''                             This would <see cref="System.IO.FileMode.CreateNew">Create
    '''                             New</see> to create a new file,
    '''                             <see cref="System.IO.FileMode.OpenOrCreate">create or open an
    '''                             existing</see>
    '''                             file or <see cref="System.IO.FileMode.Append">append</see>. to
    '''                             open and position for appending to the file. </param>
    Public Sub New(ByVal filePathName As String, ByVal fileMode As System.IO.FileMode)

        ' instantiate the base class
        MyBase.New(OpenStream(filePathName, fileMode))
        Me.FilePathName = filePathName

    End Sub

    ''' <summary> Opens a stream. </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    ''' <param name="filePathName"> Specifies the name of the binary file which to read. </param>
    ''' <param name="fileMode">     Specifies the <see cref="System.IO.FileMode">file mode</see>.
    '''                             This would <see cref="System.IO.FileMode.CreateNew">Create
    '''                             New</see> to create a new file,
    '''                             <see cref="System.IO.FileMode.OpenOrCreate">create or open an
    '''                             existing</see>
    '''                             file or <see cref="System.IO.FileMode.Append">append</see>. to
    '''                             open and position for appending to the file. </param>
    ''' <returns> A System.IO.FileStream. </returns>
    Private Shared Function OpenStream(ByVal filePathName As String, ByVal fileMode As System.IO.FileMode) As System.IO.FileStream
        Dim fileStream As System.IO.FileStream = Nothing
        Dim tempFileStream As System.IO.FileStream = Nothing
        If Not String.IsNullOrWhiteSpace(filePathName) Then
            Try
                tempFileStream = New System.IO.FileStream(filePathName, fileMode, System.IO.FileAccess.Write)
                fileStream = tempFileStream
            Catch
                If tempFileStream IsNot Nothing Then tempFileStream.Dispose()
                Throw
            End Try
        End If
        Return fileStream
    End Function

#End Region

#Region " METHODS  AND  PROPERTIES "

    ''' <summary> Closes the binary writer. </summary>
    ''' <remarks>
    ''' Use this method to close the instance.  The method Returns <c>True</c> if success or false if
    ''' it failed closing the instance.
    ''' </remarks>
    Public Overrides Sub [Close]()

        ' Close the file.
        Dim fs As System.IO.FileStream = CType(MyBase.BaseStream, System.IO.FileStream)
        MyBase.Close()
        If fs IsNot Nothing Then fs.Close()

    End Sub

    ''' <summary> Gets or sets the file name. </summary>
    ''' <remarks> Use this property to get or set the file name. </remarks>
    ''' <value> <c>FilePathName</c> is a String property. </value>
    Public ReadOnly Property FilePathName() As String = String.Empty

    ''' <summary>
    ''' Writes a single-dimension <see cref="Double">double-precision</see>
    ''' array to the data file.
    ''' </summary>
    ''' <remarks> Returns values as an array. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="values"> Holds the values to write. </param>
    Public Overloads Sub Write(ByVal values() As Double)

        If values Is Nothing Then Throw New ArgumentNullException(NameOf(values))

        ' write the data size for verifying size and, indirectly also location, upon read
        MyBase.Write(Convert.ToInt32(values.Length))

        ' write values to the file
        For i As Integer = values.GetLowerBound(0) To values.GetUpperBound(0)
            MyBase.Write(values(i))
        Next i

    End Sub

    ''' <summary>
    ''' Writes a single-dimension <see cref="Int32">Integer</see>
    ''' array to the data file.
    ''' </summary>
    ''' <remarks> Returns values as an array. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="values"> Holds the values to write. </param>
    Public Overloads Sub Write(ByVal values() As Int32)

        If values Is Nothing Then Throw New ArgumentNullException(NameOf(values))

        ' write the data size for verifying size and, indirectly also location, upon read
        MyBase.Write(Convert.ToInt32(values.Length))

        ' write values to the file
        For i As Integer = values.GetLowerBound(0) To values.GetUpperBound(0)
            MyBase.Write(values(i))
        Next i

    End Sub

    ''' <summary>
    ''' Writes a single-dimension <see cref="Int64">Long Integer</see>
    ''' array to the data file.
    ''' </summary>
    ''' <remarks> Returns values as an array. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="values"> Holds the values to write. </param>
    Public Overloads Sub Write(ByVal values() As Int64)

        If values Is Nothing Then Throw New ArgumentNullException(NameOf(values))

        ' write the data size for verifying size and, indirectly also location, upon read
        MyBase.Write(Convert.ToInt32(values.Length))

        ' write values to the file
        For i As Int32 = 0 To values.GetUpperBound(0)
            MyBase.Write(values(i))
        Next i

    End Sub

    ''' <summary>
    ''' Writes a single-dimension <see cref="Single">single-precision</see>
    ''' array to the data file.
    ''' </summary>
    ''' <remarks> Returns values as an array. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="values"> Holds the values to write. </param>
    Public Overloads Sub Write(ByVal values() As Single)

        If values Is Nothing Then Throw New ArgumentNullException(NameOf(values))

        ' write the data size for verifying size and, indirectly also location, upon read
        MyBase.Write(Convert.ToInt32(values.Length))

        ' write values to the file
        For i As Int32 = 0 To values.GetUpperBound(0)
            MyBase.Write(values(i))
        Next i

    End Sub

    ''' <summary>
    ''' Writes a string to the binary file padding it with spaces as necessary to fill the length.
    ''' </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="value">  Holds the value to write. </param>
    ''' <param name="length"> . </param>
    Public Overloads Sub Write(ByVal value As String, ByVal length As Int32)

        If value Is Nothing Then Throw New ArgumentNullException(NameOf(value))

        ' pad but make sure not to exceed length.
        MyBase.Write(value.PadRight(length).Substring(0, length))

    End Sub

    ''' <summary> Writes a double-precision value to the data file. </summary>
    ''' <remarks> Returns values as an array. </remarks>
    ''' <param name="value">    Specifies the value to write. </param>
    ''' <param name="location"> Specifies the file location. </param>
    Public Overloads Sub Write(ByVal value As Double, ByVal location As Int64)

        MyBase.BaseStream.Seek(location, System.IO.SeekOrigin.Begin)
        MyBase.Write(value)

    End Sub

    ''' <summary> Writes an Int32 value to the data file. </summary>
    ''' <remarks> Returns values as an array. </remarks>
    ''' <param name="value">    Specifies the value to write. </param>
    ''' <param name="location"> Specifies the file location. </param>
    Public Overloads Sub Write(ByVal value As Int32, ByVal location As Int64)

        MyBase.BaseStream.Seek(location, System.IO.SeekOrigin.Begin)
        MyBase.Write(value)

    End Sub

    ''' <summary> Writes a Int64 value to the data file. </summary>
    ''' <remarks> Returns values as an array. </remarks>
    ''' <param name="value">    Specifies the value to write. </param>
    ''' <param name="location"> Specifies the file location. </param>
    Public Overloads Sub Write(ByVal value As Int64, ByVal location As Int64)

        MyBase.BaseStream.Seek(location, System.IO.SeekOrigin.Begin)
        MyBase.Write(value)

    End Sub

    ''' <summary> Writes a single-precision value to the data file. </summary>
    ''' <remarks> Returns values as an array. </remarks>
    ''' <param name="value">    Specifies the value to write. </param>
    ''' <param name="location"> Specifies the file location. </param>
    Public Overloads Sub Write(ByVal value As Single, ByVal location As Int64)

        MyBase.BaseStream.Seek(location, System.IO.SeekOrigin.Begin)
        MyBase.Write(value)

    End Sub

    ''' <summary> Writes a string value to the data file. </summary>
    ''' <remarks> Returns values as an array. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="value">    Specifies the value to write. </param>
    ''' <param name="location"> Specifies the file location. </param>
    Public Overloads Sub Write(ByVal value As String, ByVal location As Int64)

        If value Is Nothing Then Throw New ArgumentNullException(NameOf(value))

        MyBase.BaseStream.Seek(location, System.IO.SeekOrigin.Begin)
        MyBase.Write(value)

    End Sub

#End Region

End Class

