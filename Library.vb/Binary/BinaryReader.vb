''' <summary>
''' Extends the <see cref="System.IO.BinaryReader">system binary reader</see> class.
''' </summary>
''' <remarks>
''' (c) 2006 Integrated Scientific Resources, Inc. All rights reserved. <para>
''' Licensed under The MIT License. </para><para>  
''' David, 03/07/2006, 1.0.2257.x. </para>
''' </remarks>
Public Class BinaryReader

    ' based on the system binary reader
    Inherits System.IO.BinaryReader

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Constructs this class. </summary>
    ''' <remarks> Use this constructor to open a binary reader for the specified file. </remarks>
    ''' <param name="filePathName"> Specifies the name of the binary file which to read. </param>
    Public Sub New(ByVal filePathName As String)

        ' instantiate the base class
        MyBase.New(OpenStream(filePathName))
        Me.FilePathName = filePathName

    End Sub

    ''' <summary> Opens a stream. </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    ''' <param name="filePathName"> Specifies the name of the binary file which to read. </param>
    ''' <returns> A System.IO.FileStream. </returns>
    Private Shared Function OpenStream(ByVal filePathName As String) As System.IO.FileStream
        Dim fileStream As System.IO.FileStream = Nothing
        Dim tempFileStream As System.IO.FileStream = Nothing
        If Not String.IsNullOrWhiteSpace(filePathName) Then
            Try
                tempFileStream = New System.IO.FileStream(filePathName, System.IO.FileMode.Open, System.IO.FileAccess.Read)
                fileStream = tempFileStream
            Catch
                If tempFileStream IsNot Nothing Then tempFileStream.Dispose()
                Throw
            End Try
        End If
        Return fileStream
    End Function

#End Region

#Region " METHODS  AND  PROPERTIES "

    ''' <summary> Closes the binary reader and base stream. </summary>
    ''' <remarks> Use this method to close the instance. </remarks>
    Public Overrides Sub [Close]()
        ' Close the file.
        Dim fs As System.IO.FileStream = CType(MyBase.BaseStream, System.IO.FileStream)
        MyBase.Close()
        fs?.Close()
    End Sub

    ''' <summary> Gets or sets the file name. </summary>
    ''' <remarks> Use this property to get or set the file name. </remarks>
    ''' <value> <c>FilePathName</c> is a String property. </value>
    Public ReadOnly Property FilePathName() As String = String.Empty

    ''' <summary>
    ''' Opens a binary file for reading and returns a reference to the reader. The file is
    ''' <see cref="System.IO.FileMode.Open">opened</see> in
    ''' <see cref="System.IO.FileAccess.Read">read access</see>.
    ''' </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <exception cref="System.IO.FileNotFoundException"> Thrown when the requested file is not present. </exception>
    ''' <param name="filePathName"> Specifies the file name. </param>
    ''' <returns> A reference to an open <see cref="IO.BinaryReader">binary reader</see>. </returns>
    Public Shared Function OpenBinaryReader(ByVal filePathName As String) As isr.IO.BinaryReader

        If String.IsNullOrWhiteSpace(filePathName) Then Throw New ArgumentNullException(NameOf(filePathName))

        If Not System.IO.File.Exists(filePathName) Then
            Dim message As String = "Failed opening a binary reader -- file not found."
            Throw New System.IO.FileNotFoundException(message, filePathName)
        End If

        Return New isr.IO.BinaryReader(filePathName)

    End Function

    ''' <summary>
    ''' Reads a single-dimension <see cref="Double">double-precision</see>
    ''' array from the data file.
    ''' </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    ''' <exception cref="System.IO.IOException"> Thrown when an IO failure occurred. </exception>
    ''' <returns> A <see cref="System.Double">double-precision</see> array. </returns>
    Public Overloads Function ReadDoubleArray() As Double()

        Dim startingPosition As Long = MyBase.BaseStream.Position

        ' read the stored length
        Dim storedLength As Int32 = MyBase.ReadInt32

        If storedLength < 0 Then
            Throw New System.IO.IOException(String.Format(Globalization.CultureInfo.CurrentCulture,
                                                          "Program encountered a negative array length of {0}. Possibly the file is corrupt the reader position at {1} is incorrect.",
                                                          storedLength, startingPosition))
        End If

        Return Me.ReadDoubleArray(storedLength)

    End Function

    ''' <summary>
    ''' Reads a single-dimension <see cref="Double">double-precision</see>
    ''' array from the data file.
    ''' </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    ''' <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
    '''                                                the required range. </exception>
    ''' <param name="elementCount"> Specifies the number of data points. </param>
    ''' <returns> A <see cref="Double">double-precision</see> array. </returns>
    Public Overloads Function ReadDoubleArray(ByVal elementCount As Int32) As Double()

        If elementCount < 0 Then
            Dim message As String = "Array size specified as {0} must be non-negative."
            message = String.Format(Globalization.CultureInfo.CurrentCulture, message, elementCount)
            Throw New ArgumentOutOfRangeException(NameOf(elementCount), elementCount, message)
        End If

        If elementCount = 0 Then

            ' return the empty array 
            Dim data() As Double = Array.Empty(Of Double)()
            Return data

        Else
            ' allocate data array
            Dim data(elementCount - 1) As Double

            ' Read the voltage from the file
            For sampleNumber As Int32 = 0 To elementCount - 1
                data(sampleNumber) = MyBase.ReadDouble
            Next sampleNumber
            Return data
        End If

    End Function

    ''' <summary>
    ''' Reads a single-dimension <see cref="Double">double-precision</see>
    ''' array from the data file.
    ''' </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    ''' <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
    '''                                                the required range. </exception>
    ''' <param name="elementCount"> Specifies the number of data points. </param>
    ''' <param name="verifyLength"> If true, verifies the length against the given length. </param>
    ''' <returns> A single-dimension <see cref="Double">double-precision</see> array. </returns>
    Public Overloads Function ReadDoubleArray(ByVal elementCount As Int32, ByVal verifyLength As Boolean) As Double()

        If verifyLength Then
            Dim startingPosition As Long = MyBase.BaseStream.Position
            Dim data() As Double = Me.ReadDoubleArray()
            If data.Length <> elementCount Then
                Dim message As String = "Data length stored in file of {0} elements does not match the expected data length of {1} elements at {2}."
                message = String.Format(Globalization.CultureInfo.CurrentCulture, message, data.Length, elementCount, startingPosition)
                Throw New ArgumentOutOfRangeException(NameOf(elementCount), elementCount, message)
            Else
                Return data
            End If
        Else
            Return Me.ReadDoubleArray()
        End If

    End Function

    ''' <summary>
    ''' Reads a single-dimension <see cref="Double">double-precision</see>
    ''' array from the data file.
    ''' </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    ''' <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
    '''                                                the required range. </exception>
    ''' <param name="count">      Specifies the number of data points. </param>
    ''' <param name="startIndex"> Specifies the index of the first data point. </param>
    ''' <param name="stepSize">   Specifies the step size between adjacent data points. </param>
    ''' <returns> A single-dimension <see cref="Double">double-precision</see> array. </returns>
    Public Overloads Function ReadDoubleArray(ByVal count As Int32, ByVal startIndex As Int32, ByVal stepSize As Int32) As Double()

        Dim startingPosition As Long = MyBase.BaseStream.Position

        ' read the stored length
        Dim storedLength As Int32 = MyBase.ReadInt32

        If storedLength <> count Then
            Dim message As String = "Data length stored in file of {0} elements does not match the expected data length of {1} elements at {2}."
            message = String.Format(Globalization.CultureInfo.CurrentCulture, message, storedLength, count, startingPosition)
            Throw New ArgumentOutOfRangeException(NameOf(count), count, message)
        End If

        ' allocate data array
        Dim data(count - 1) As Double

        ' skip samples to get to the first channel of this sample set.
        If startIndex > 0 Then
            For i As Integer = 1 To startIndex
                MyBase.ReadDouble()
            Next
        End If
        ' Read the voltage from the file
        For sampleNumber As Int32 = 0 To count - 1
            data(sampleNumber) = MyBase.ReadDouble
            If stepSize > 1 Then
                For i As Integer = 2 To stepSize
                    MyBase.ReadDouble()
                Next
            End If

        Next sampleNumber

        Return data

    End Function

    ''' <summary> Reads a double precision value from the data file. </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    ''' <param name="location"> Specifies the file location. </param>
    ''' <returns> A <see cref="System.Double">value</see>. </returns>
    Public Overloads Function ReadDoubleValue(ByVal location As Int64) As Double

        MyBase.BaseStream.Seek(location, System.IO.SeekOrigin.Begin)
        Return MyBase.ReadDouble()

    End Function

    ''' <summary>
    ''' Reads a single-dimension <see cref="Int32">Integer</see>
    ''' array from the data file.
    ''' </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    ''' <exception cref="System.IO.IOException"> Thrown when an IO failure occurred. </exception>
    ''' <returns> An <see cref="System.Int32">integer</see> array. </returns>
    Public Overloads Function ReadInt32Array() As Int32()

        Dim startingPosition As Long = MyBase.BaseStream.Position

        ' read the stored length
        Dim storedLength As Int32 = MyBase.ReadInt32

        If storedLength < 0 Then
            Throw New System.IO.IOException(String.Format(Globalization.CultureInfo.CurrentCulture,
                                                          "Program encountered a negative array length of {0}. Possibly the file is corrupt the reader position at {1} is incorrect.",
                                                          storedLength, startingPosition))
        End If

        Return Me.ReadInt32Array(storedLength)

    End Function

    ''' <summary>
    ''' Reads a single-dimension <see cref="Int32">Integer</see>
    ''' array from the data file.
    ''' </summary>
    ''' <remarks> Returns values as an array. </remarks>
    ''' <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
    '''                                                the required range. </exception>
    ''' <param name="elementCount"> Specifies the number of data points. </param>
    ''' <returns> An <see cref="System.Int32">integer</see> array. </returns>
    Public Overloads Function ReadInt32Array(ByVal elementCount As Int32) As Int32()

        If elementCount < 0 Then
            Dim message As String = "Array size must be non-negative {0}."
            message = String.Format(Globalization.CultureInfo.CurrentCulture, message, elementCount)
            Throw New ArgumentOutOfRangeException(NameOf(elementCount), elementCount, message)
        End If

        If elementCount = 0 Then

            ' return the empty array 
            Dim data() As Int32 = Array.Empty(Of Integer)()
            Return data

        Else
            ' allocate data array
            Dim data(elementCount - 1) As Int32

            ' Read the voltage from the file
            For sampleNumber As Int32 = 0 To elementCount - 1
                data(sampleNumber) = MyBase.ReadInt32
            Next sampleNumber
            Return data
        End If

    End Function

    ''' <summary>
    ''' Reads a single-dimension <see cref="Int32">Integer</see>
    ''' array from the data file.
    ''' </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    ''' <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
    '''                                                the required range. </exception>
    ''' <param name="elementCount"> Specifies the number of data points. </param>
    ''' <param name="verifyLength"> If true, verifies the length against the given length. </param>
    ''' <returns> An <see cref="System.Int32">integer</see> array. </returns>
    Public Overloads Function ReadInt32Array(ByVal elementCount As Int32, ByVal verifyLength As Boolean) As Int32()

        If verifyLength Then
            Dim startingPosition As Long = MyBase.BaseStream.Position
            Dim data() As Int32 = Me.ReadInt32Array()
            If data.Length <> elementCount Then
                Dim message As String = "Data length stored in file of {0} elements does not match the expected data length of {1} elements at {2}."
                message = String.Format(Globalization.CultureInfo.CurrentCulture, message, data.Length, elementCount, startingPosition)
                Throw New ArgumentOutOfRangeException(NameOf(elementCount), elementCount, message)
            Else
                Return data
            End If
        Else
            Return Me.ReadInt32Array()
        End If

    End Function

    ''' <summary>
    ''' Reads a single-dimension <see cref="Int32">Integer</see>
    ''' array from the data file.
    ''' </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    ''' <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
    '''                                                the required range. </exception>
    ''' <param name="count">      Specifies the number of data points. </param>
    ''' <param name="startIndex"> Specifies the index of the first data point. </param>
    ''' <param name="stepSize">   Specifies the step size between adjacent data points. </param>
    ''' <returns> An <see cref="System.Int32">integer</see> array. </returns>
    Public Overloads Function ReadInt32(ByVal count As Int32, ByVal startIndex As Int32, ByVal stepSize As Int32) As Int32()

        Dim startingPosition As Long = MyBase.BaseStream.Position

        ' read the stored length
        Dim storedLength As Int32 = MyBase.ReadInt32

        If storedLength <> count Then
            Dim message As String = "Data length stored in file of {0} elements does not match the expected data length of {1} elements at {2}."
            message = String.Format(Globalization.CultureInfo.CurrentCulture, message, storedLength, count, startingPosition)
            Throw New ArgumentOutOfRangeException(NameOf(count), count, message)
        End If

        ' allocate data array
        Dim data(count - 1) As Int32

        ' skip samples to get to the first channel of this sample set.
        If startIndex > 0 Then
            For i As Integer = 1 To startIndex
                MyBase.ReadInt32()
            Next
        End If
        ' Read the voltage from the file
        For sampleNumber As Int32 = 0 To count - 1
            data(sampleNumber) = MyBase.ReadInt32
            If stepSize > 1 Then
                For i As Integer = 2 To stepSize
                    MyBase.ReadInt32()
                Next
            End If

        Next sampleNumber

        Return data

    End Function

    ''' <summary> Reads an Int32 value from the data file. </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    ''' <param name="location"> Specifies the file location. </param>
    ''' <returns> An <see cref="System.Int32">integer</see> value. </returns>
    Public Overloads Function ReadInt32(ByVal location As Int64) As Int32

        MyBase.BaseStream.Seek(location, System.IO.SeekOrigin.Begin)
        Return MyBase.ReadInt32()

    End Function

    ''' <summary>
    ''' Reads a <see cref="Int64">long integer</see>
    ''' long-integer array from the data file.
    ''' </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    ''' <exception cref="System.IO.IOException"> Thrown when an IO failure occurred. </exception>
    ''' <returns> An <see cref="System.Int64">long</see> array. </returns>
    Public Overloads Function ReadInt64Array() As Int64()

        Dim startingPosition As Long = MyBase.BaseStream.Position

        ' read the stored length
        Dim storedLength As Int32 = MyBase.ReadInt32

        If storedLength < 0 Then
            Throw New System.IO.IOException(String.Format(Globalization.CultureInfo.CurrentCulture,
                                                          "Program encountered a negative array length of {0}. Possibly the file is corrupt the reader position at {1} is incorrect.",
                                                          storedLength, startingPosition))
        End If

        Return Me.ReadInt64Array(storedLength)

    End Function

    ''' <summary>
    ''' Reads a <see cref="Int64">long integer</see>
    ''' long integer array from the data file.
    ''' </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    ''' <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
    '''                                                the required range. </exception>
    ''' <param name="elementCount"> Specifies the number of data points. </param>
    ''' <returns> A <see cref="System.Int64">long</see> array. </returns>
    Public Overloads Function ReadInt64Array(ByVal elementCount As Int32) As Int64()

        If elementCount < 0 Then
            Dim message As String = "Array size must be non-negative {0}."
            message = String.Format(Globalization.CultureInfo.CurrentCulture, message, elementCount)
            Throw New ArgumentOutOfRangeException(NameOf(elementCount), elementCount, message)
        End If

        If elementCount = 0 Then

            ' return the empty array 
            Dim data() As Int64 = Array.Empty(Of Long)()
            Return data

        Else
            ' allocate data array
            Dim data(elementCount - 1) As Int64

            ' Read the voltage from the file
            For sampleNumber As Int32 = 0 To elementCount - 1
                data(sampleNumber) = MyBase.ReadInt64
            Next sampleNumber
            Return data
        End If

    End Function

    ''' <summary>
    ''' Reads a <see cref="Int64">long integer</see>
    ''' long integer array from the data file.
    ''' </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    ''' <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
    '''                                                the required range. </exception>
    ''' <param name="elementCount"> Specifies the number of data points. </param>
    ''' <param name="verifyLength"> If true, verifies the length against the given length. </param>
    ''' <returns> A <see cref="System.Int64">Long</see> array. </returns>
    Public Overloads Function ReadInt64Array(ByVal elementCount As Int32, ByVal verifyLength As Boolean) As Int64()

        If verifyLength Then
            Dim startingPosition As Long = MyBase.BaseStream.Position
            Dim data() As Int64 = Me.ReadInt64Array()
            If data.Length <> elementCount Then
                Dim message As String = "Data length stored in file of {0} elements does not match the expected data length of {1} elements at {2}."
                message = String.Format(Globalization.CultureInfo.CurrentCulture, message, data.Length, elementCount, startingPosition)
                Throw New ArgumentOutOfRangeException(NameOf(elementCount), elementCount, message)
            Else
                Return data
            End If
        Else
            Return Me.ReadInt64Array()
        End If

    End Function

    ''' <summary>
    ''' Reads a single-dimension <see cref="Int64">Long Integer</see>
    ''' array from the data file.
    ''' </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    ''' <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
    '''                                                the required range. </exception>
    ''' <param name="count">      Specifies the number of data points. </param>
    ''' <param name="startIndex"> Specifies the index of the first data point. </param>
    ''' <param name="stepSize">   Specifies the step size between adjacent data points. </param>
    ''' <returns> A <see cref="System.Int64">Long</see> array. </returns>
    Public Overloads Function ReadInt64(ByVal count As Int32, ByVal startIndex As Int32, ByVal stepSize As Int32) As Int64()

        ' read the stored length
        Dim storedLength As Int32 = MyBase.ReadInt32

        If storedLength <> count Then
            Dim startingPosition As Long = MyBase.BaseStream.Position
            Dim message As String = "Data length stored in file of {0} elements does not match the expected data length of {1} elements at {2}."
            message = String.Format(Globalization.CultureInfo.CurrentCulture, message, storedLength, count, startingPosition)
            Throw New ArgumentOutOfRangeException(NameOf(count), count, message)
        End If

        ' allocate data array
        Dim data(count - 1) As Int64

        ' skip samples to get to the first channel of this sample set.
        If startIndex > 0 Then
            For i As Integer = 1 To startIndex
                MyBase.ReadInt64()
            Next
        End If
        ' Read the voltage from the file
        For sampleNumber As Int32 = 0 To count - 1
            data(sampleNumber) = MyBase.ReadInt64
            If stepSize > 1 Then
                For i As Integer = 2 To stepSize
                    MyBase.ReadInt64()
                Next
            End If

        Next sampleNumber

        Return data

    End Function

    ''' <summary> Reads a Int64 value from the data file. </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    ''' <param name="location"> Specifies the file location. </param>
    ''' <returns> A <see cref="System.Int64">Long</see> value. </returns>
    Public Overloads Function ReadInt64(ByVal location As Int64) As Int64

        MyBase.BaseStream.Seek(location, System.IO.SeekOrigin.Begin)
        Return MyBase.ReadInt64()

    End Function

    ''' <summary>
    ''' Reads a single-dimension <see cref="Single">single-precision</see>
    ''' array from the data file.
    ''' </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    ''' <exception cref="System.IO.IOException"> Thrown when an IO failure occurred. </exception>
    ''' <returns> A <see cref="System.Single">single-dimension</see> array. </returns>
    Public Overloads Function ReadSingleArray() As Single()

        Dim startingPosition As Long = MyBase.BaseStream.Position

        ' read the stored length
        Dim storedLength As Int32 = MyBase.ReadInt32

        If storedLength < 0 Then
            Throw New System.IO.IOException(String.Format(Globalization.CultureInfo.CurrentCulture,
                                                          "Program encountered a negative array length of {0}. Possibly the file is corrupt the reader position at {1} is incorrect.",
                                                          storedLength, startingPosition))
        End If

        Return Me.ReadSingleArray(storedLength)

    End Function

    ''' <summary>
    ''' Reads a single-dimension <see cref="Single">single-precision</see>
    ''' array from the data file.
    ''' </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    ''' <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
    '''                                                the required range. </exception>
    ''' <param name="elementCount"> Specifies the number of data points. </param>
    ''' <returns> A <see cref="System.Single">Single-Dimension</see> array. </returns>
    Public Overloads Function ReadSingleArray(ByVal elementCount As Int32) As Single()

        If elementCount < 0 Then
            Dim message As String = "Array size must be non-negative {0}."
            message = String.Format(Globalization.CultureInfo.CurrentCulture, message, elementCount)
            Throw New ArgumentOutOfRangeException(NameOf(elementCount), elementCount, message)
        End If

        If elementCount = 0 Then

            ' return the empty array 
            Dim data() As Single = Array.Empty(Of Single)()
            Return data

        Else
            ' allocate data array
            Dim data(elementCount - 1) As Single

            ' Read the voltage from the file
            For sampleNumber As Int32 = 0 To elementCount - 1
                data(sampleNumber) = MyBase.ReadSingle
            Next sampleNumber
            Return data
        End If

    End Function

    ''' <summary>
    ''' Reads a single-dimension <see cref="Single">single-precision</see>
    ''' array from the data file.
    ''' </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    ''' <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
    '''                                                the required range. </exception>
    ''' <param name="elementCount"> Specifies the number of data points. </param>
    ''' <param name="verifyLength"> If true, verifies the length against the given length. </param>
    ''' <returns> A <see cref="System.Single">Single-Dimension</see> array. </returns>
    Public Overloads Function ReadSingleArray(ByVal elementCount As Int32, ByVal verifyLength As Boolean) As Single()

        If verifyLength Then
            Dim data() As Single = Me.ReadSingleArray()
            If data.Length <> elementCount Then
                Dim message As String = "Data length stored in file of {0} elements does not match the expected data length of {1} elements."
                message = String.Format(Globalization.CultureInfo.CurrentCulture, message, data.Length, elementCount)
                Throw New ArgumentOutOfRangeException(NameOf(elementCount), elementCount, message)
            Else
                Return data
            End If
        Else
            Return Me.ReadSingleArray()
        End If

    End Function

    ''' <summary>
    ''' Reads a single-dimension <see cref="Single">single-precision</see>
    ''' array from the data file.
    ''' </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    ''' <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
    '''                                                the required range. </exception>
    ''' <param name="count">      Specifies the number of data points. </param>
    ''' <param name="startIndex"> Specifies the index of the first data point. </param>
    ''' <param name="stepSize">   Specifies the step size between adjacent data points. </param>
    ''' <returns> A <see cref="System.Single">Single-Dimension</see> array. </returns>
    Public Overloads Function ReadSingle(ByVal count As Int32, ByVal startIndex As Int32, ByVal stepSize As Int32) As Single()

        Dim startingPosition As Long = MyBase.BaseStream.Position

        ' read the stored length
        Dim storedLength As Int32 = MyBase.ReadInt32

        If storedLength <> count Then
            Dim message As String = "Data length stored in file of {0} elements does not match the expected data length of {1} elements at {2}."
            message = String.Format(Globalization.CultureInfo.CurrentCulture, message, storedLength, count, startingPosition)
            Throw New ArgumentOutOfRangeException(NameOf(count), count, message)
        End If

        ' allocate data array
        Dim data(count - 1) As Single

        ' skip samples to get to the first channel of this sample set.
        If startIndex > 0 Then
            For i As Integer = 1 To startIndex
                MyBase.ReadSingle()
            Next
        End If
        ' Read the voltage from the file
        For sampleNumber As Int32 = 0 To count - 1
            data(sampleNumber) = MyBase.ReadSingle
            If stepSize > 1 Then
                For i As Integer = 2 To stepSize
                    MyBase.ReadSingle()
                Next
            End If

        Next sampleNumber

        Return data

    End Function

    ''' <summary> Reads a single precision value from the data file. </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    ''' <param name="location"> Specifies the file location. </param>
    ''' <returns> A <see cref="System.Single">Single-Dimension</see> value. </returns>
    Public Overloads Function ReadSingle(ByVal location As Int64) As Single

        MyBase.BaseStream.Seek(location, System.IO.SeekOrigin.Begin)
        Return MyBase.ReadSingle()

    End Function

    ''' <summary> Reads a string value from the data file. </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    ''' <param name="location"> Specifies the file location. </param>
    ''' <returns> A <see cref="System.String">String</see> value. </returns>
    Public Overloads Function ReadString(ByVal location As Int64) As String

        MyBase.BaseStream.Seek(location, System.IO.SeekOrigin.Begin)
        Return MyBase.ReadString()

    End Function

#End Region

End Class

