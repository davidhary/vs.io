Imports Microsoft.VisualBasic

''' <summary> Reads and writes private and public profile (INI Settings) files. </summary>
''' <remarks>
''' Use this class to read and write INI Settings (INI) file.<para>
''' With the ISR INI settings Scribe Class you can write and read information from INI files.
''' Both private (yours) and public (WIN.INI) files are accessible.</para><para>
''' INI Settings (INI) files typically include information about your application in strings that
''' are called Profile Strings. Such information is used most often to determine how programs are
''' run by setting specific properties in these programs with information from the INI Settings
''' (INI) file.  Thus determine the profile of the programs. </para><para>
''' With the ISR INI settings Scribe Class you can define the INI Settings (INI) file name and
''' write and read from the INI Settings file information of any variable type.  
''' In addition, you can write or read lists of data.</para>  <para>
''' (c) 2006 Integrated Scientific Resources, Inc. All rights reserved. </para><para>
''' Licensed under The MIT License. </para><para>  
''' David, 04/20/06, 1.0.2301.x. </para>
''' </remarks>
Public Class ProfileScribe
    Implements IDisposable

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Initializes a new instance of the <see cref="ProfileScribe" /> class. </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    Public Sub New()
        Me.New(BuildDefaultFileName())
    End Sub

    ''' <summary> Initializes a new instance of the <see cref="ProfileScribe" /> class. </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    ''' <param name="filePath"> The file path. This also serves as the instance name. </param>
    Public Sub New(ByVal filePath As String)
        MyBase.New()
        Me._FilePath = filePath
        Me._DefaultFileName = BuildDefaultFileName()
    End Sub

    ''' <summary> Calls <see cref="M:Dispose(Boolean Disposing)"/> to cleanup. </summary>
    ''' <remarks>
    ''' Do not make this method Overridable (virtual) because a derived class should not be able to
    ''' override this method.
    ''' </remarks>
    Public Sub Dispose() Implements IDisposable.Dispose

        ' Do not change this code.  Put cleanup code in Dispose(Boolean) below.

        ' this disposes all child classes.
        Me.Dispose(True)

        ' Take this object off the finalization(Queue) and prevent finalization code 
        ' from executing a second time.
        GC.SuppressFinalize(Me)

    End Sub

    ''' <summary>
    ''' Gets the dispose status sentinel of the base class.  This applies to the derived class
    ''' provided proper implementation.
    ''' </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <value> The is disposed. </value>
    Protected Property IsDisposed() As Boolean

    ''' <summary>
    ''' Releases the unmanaged resources used by the <see cref="T:ProfileScribe" />
    ''' and its child controls and optionally releases the managed resources.
    ''' </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    ''' <param name="disposing"> <c>True</c> to release both managed and unmanaged resources;
    '''                                                   <c>False</c> to release only unmanaged
    '''                                                   resources when called from the runtime
    '''                                                   finalize. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overridable Sub Dispose(ByVal disposing As Boolean)
        Try
            If Not Me.IsDisposed AndAlso disposing Then
                Me._FilePath = String.Empty
                Me._SectionName = String.Empty
                Me._SettingName = String.Empty
            End If
        Finally
            Me.IsDisposed = True
        End Try
    End Sub

    ''' <summary>
    ''' This destructor will run only if the Dispose method does not get called. It gives the base
    ''' class the opportunity to finalize. Do not provide destructors in types derived from this
    ''' class.
    ''' </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    Protected Overrides Sub Finalize()
        ' Do not re-create Dispose clean-up code here.
        ' Calling Dispose(false) is optimal for readability and maintainability.
        Me.Dispose(False)
    End Sub

#End Region

#Region " FILE "

    ''' <summary> Gets the full name of the file. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <value> The name of the file. </value>
    Public Property FilePath() As String

    ''' <summary> Queries if a given file exists. </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    Public Function FileExists() As Boolean
        Return FileExists(Me.FilePath)
    End Function

    ''' <summary> Queries if a given filename is valid and the file exists . </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    ''' <param name="filePath"> The file path. </param>
    ''' <returns> <c>True</c> if file name is valid, <c>False</c> otherwise. </returns>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Public Shared Function FileExists(ByVal filePath As String) As Boolean
        Try
            ' Check that the file exists
            Return System.IO.File.Exists(filePath)
        Catch
            Return False
        End Try
    End Function

    ''' <summary> Validates the given file name. </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    ''' <param name="filePath"> The file path. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Public Shared Function ValidateFileName(ByVal filePath As String) As Boolean
        Try
            ' Check that the file exists
            Return System.IO.File.Exists(filePath)
            Return True
        Catch
            Return False
        End Try
    End Function

    ''' <summary> Builds default file name. </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    ''' <returns> A String. </returns>
    Public Shared Function BuildDefaultFileName() As String
        ' System.Windows.Fo rms.Application.ExecutablePath & PrivateProfileScribe.DefaultExtension
        ' System.Windows.Fo rms.Application.ExecutablePath & ProfileScribe.DefaultExtension)
        Return $"{My.Application.Info.DirectoryPath}\{My.Application.Info.AssemblyName}{DefaultExtension}"
    End Function

    ''' <summary> Gets the default name of the file. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <value> The default name of the file. </value>
    Public Property DefaultFileName As String

    ''' <summary> Holds the default extension of an INI file. </summary>
    Public Const DefaultExtension As String = ".ini"

    ''' <summary> Returns a default INI Settings file name. </summary>
    ''' <remarks>
    ''' Use this method to get the default name for the executing assembly. The file name defaults
    ''' to: 'executable file name'.ini, e.g., ConfigurationTester.exe.ini.
    ''' </remarks>
    ''' <returns>
    ''' A <see cref="System.String">String</see> value consisting of the file name with the extension
    ''' .INI.
    ''' </returns>
    ''' <seealso cref="ProfileScribe.FilePath"/>
    Public Shared Function DefaultFilePath() As String
        ' Return the default file name
        ' Return System.Windows.Fo rms.Application.ExecutablePath & ProfileScribe.DefaultExtension
        Return BuildDefaultFileName()
    End Function

#End Region

#Region " READ SECTION "

    ''' <summary> Reads a whole section from the INI file. </summary>
    ''' <remarks>
    ''' Use This method to read a section from an INI file.  The section is returned in an enumerable
    ''' of string values where the section values are delimited by the null character (ASCII 0)
    ''' </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="filePath">    A <see cref="System.String">String</see> expression that
    '''                            specifies the INI file name. Specify the empty file name to read
    '''                            from WIN.INI. </param>
    ''' <param name="sectionName"> A <see cref="System.String">String</see> expression that
    '''                            specifies the section  
    '''                            which to read. </param>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process read section in this collection.
    ''' </returns>
    Public Shared Function ReadSection(ByVal filePath As String, ByVal sectionName As String) As IEnumerable(Of String)
        If filePath Is Nothing Then Throw New System.ArgumentNullException(NameOf(filePath))
        If String.IsNullOrWhiteSpace(sectionName) Then Throw New System.ArgumentNullException(NameOf(sectionName))
        Dim items As New List(Of String)
        Dim buffer(32768) As Byte
        Dim sb As System.Text.StringBuilder
        Dim i As Int32
        Dim bufferLength As Integer = If(filePath.Length = 0,
            SafeNativeMethods.GetProfileSection(sectionName, buffer, buffer.GetUpperBound(0)),
            SafeNativeMethods.GetPrivateProfileSection(sectionName, buffer, buffer.GetUpperBound(0), filePath))
        ' read the section
        If bufferLength > 0 Then
            sb = New System.Text.StringBuilder
            For i = 0 To bufferLength - 1
                ' separate strings delimited by the null character (ASCII 0)
                If buffer(i) <> 0 Then
                    sb.Append(Convert.ToChar(buffer(i)))
                Else
                    If sb.Length > 0 Then
                        items.Add(sb.ToString())
                        sb = New System.Text.StringBuilder
                    End If
                End If
            Next
        End If
        Return items
    End Function

    ''' <summary> Reads all the section names from the INI file. </summary>
    ''' <remarks>
    ''' Use This method to read section names from an INI file.  The names are returned in an
    ''' enumerable of string values.
    ''' </remarks>
    ''' <param name="filePath"> A <see cref="System.String">String</see> expression that specifies
    '''                         the INI file name. </param>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process read section names in this .
    ''' </returns>
    Public Shared Function ReadSectionNames(ByVal filePath As String) As IEnumerable(Of String)

        Dim sectionNames As New List(Of String)
        Dim buffer(32768) As Byte
        Dim sb As System.Text.StringBuilder
        Dim i As Int32

        Dim bufferLength As Integer = If(String.IsNullOrWhiteSpace(filePath),
            SafeNativeMethods.GetPrivateProfileSectionNames(buffer, buffer.GetUpperBound(0), PublicProfileFilePath),
            SafeNativeMethods.GetPrivateProfileSectionNames(buffer, buffer.GetUpperBound(0), filePath))
        ' read the section names
        If bufferLength > 0 Then
            sb = New System.Text.StringBuilder
            For i = 0 To bufferLength - 1
                ' separate sections along the null character.
                If buffer(i) <> 0 Then
                    sb.Append(Convert.ToChar(buffer(i)))
                Else
                    If sb.Length > 0 Then
                        sectionNames.Add(sb.ToString())
                        sb = New System.Text.StringBuilder
                    End If
                End If
            Next
        End If
        Return sectionNames
    End Function

#End Region

#Region " READ SECTION SETTING "

    ''' <summary> Reads a string value from the INI file. </summary>
    ''' <remarks> Use This method to read a String value from an INI file. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="filePath">    A <see cref="System.String">String</see> expression that
    '''                            specifies the INI file name. Use an empty file name to read from
    '''                            Win.ini. </param>
    ''' <param name="sectionName"> A <see cref="System.String">String</see> expression that
    '''                            specifies the section in the INI file from which to read the
    '''                            settings. </param>
    ''' <param name="keyName">     A <see cref="System.String">String</see> expression that
    '''                            specifies a Key in the section from which to read the settings. 
    ''' </param>
    ''' <returns> A <see cref="System.String">String</see> data type. </returns>
    Public Shared Function Read(ByVal filePath As String, ByVal sectionName As String, ByVal keyName As String) As String
        If filePath Is Nothing Then Throw New System.ArgumentNullException(NameOf(filePath))
        If String.IsNullOrWhiteSpace(sectionName) Then Throw New System.ArgumentNullException(NameOf(sectionName))
        If keyName Is Nothing Then Throw New System.ArgumentNullException(NameOf(keyName))
        Return Read(filePath, sectionName, keyName, String.Empty)
    End Function

    ''' <summary> Reads a string value from the INI file. </summary>
    ''' <remarks>
    ''' Use This method to read a String value from an INI file. <para>
    ''' David, 09/20/05, 2.0.2099. Return string.empty with zero characters. </para>
    ''' </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="filePath">     A <see cref="System.String">String</see> expression that
    '''                             specifies the INI file name. Use an empty file name to read from
    '''                             Win.ini. </param>
    ''' <param name="sectionName">  A <see cref="System.String">String</see> expression that
    '''                             specifies the section in the INI file from which to read the
    '''                             settings. </param>
    ''' <param name="keyName">      A <see cref="System.String">String</see> expression that
    '''                             specifies a Key in the section from which to read the settings. 
    ''' </param>
    ''' <param name="defaultValue"> A <see cref="System.String">String</see> expression that
    '''                             specifies a default value to return if the key is not found. 
    ''' </param>
    ''' <returns> A <see cref="System.String">String</see> data type. </returns>
    Public Shared Function Read(ByVal filePath As String, ByVal sectionName As String, ByVal keyName As String, ByVal defaultValue As String) As String
        If filePath Is Nothing Then Throw New System.ArgumentNullException(NameOf(filePath))
        If String.IsNullOrWhiteSpace(sectionName) Then Throw New System.ArgumentNullException(NameOf(sectionName))
        If keyName Is Nothing Then Throw New System.ArgumentNullException(NameOf(keyName))
        If defaultValue Is Nothing Then Throw New System.ArgumentNullException(NameOf(defaultValue))
        Dim unused As String = New String(" "c, SettingsBufferSize)
        Dim buffer As String = New String(Chr(0), SettingsBufferSize)
        If filePath.Length > 0 Then
            Dim length As Integer = SafeNativeMethods.GetPrivateProfileString(sectionName, keyName, defaultValue, buffer, buffer.Length, filePath)
            Return If(length > 0, buffer.Substring(0, length), defaultValue)
        Else
            Return If(SafeNativeMethods.GetProfileString(sectionName, keyName, defaultValue, buffer, SettingsBufferSize) > 0,
                buffer.ToString(),
                defaultValue)
        End If
    End Function

    ''' <summary> Reads using the GetPrivateProfileStringA entry point. </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="filePath">     A <see cref="System.String">String</see> expression that
    '''                             specifies the INI file name. Use an empty file name to read from
    '''                             Win.ini. </param>
    ''' <param name="sectionName">  A <see cref="System.String">String</see> expression that
    '''                             specifies the section in the INI file from which to read the
    '''                             settings. </param>
    ''' <param name="keyName">      A <see cref="System.String">String</see> expression that
    '''                             specifies a Key in the section from which to read the settings. 
    ''' </param>
    ''' <param name="defaultValue"> A <see cref="System.String">String</see> expression that
    '''                             specifies a default value to return if the key is not found. 
    ''' </param>
    ''' <returns> a. </returns>
    <Obsolete("Using GetPrivateProfileStringA fails reading a string such as 7/20/2019'; it reads only 7")>
    Public Shared Function ReadA(ByVal filePath As String, ByVal sectionName As String, ByVal keyName As String, ByVal defaultValue As String) As String
        If filePath Is Nothing Then Throw New System.ArgumentNullException(NameOf(filePath))
        If String.IsNullOrWhiteSpace(sectionName) Then Throw New System.ArgumentNullException(NameOf(sectionName))
        If keyName Is Nothing Then Throw New System.ArgumentNullException(NameOf(keyName))
        If defaultValue Is Nothing Then Throw New System.ArgumentNullException(NameOf(defaultValue))
        Dim unused As String = New String(" "c, SettingsBufferSize)
        Dim buffer As String = New String(Chr(0), SettingsBufferSize)
        If filePath.Length > 0 Then
            Dim length As Integer = SafeNativeMethods.ReadPrivateProfileString(sectionName, keyName, defaultValue, buffer, buffer.Length, filePath)
            Return If(length > 0, buffer.Substring(0, length), defaultValue)
        Else
            Return If(SafeNativeMethods.GetProfileString(sectionName, keyName, defaultValue, buffer, SettingsBufferSize) > 0,
                buffer.ToString(),
                defaultValue)
        End If
    End Function

    ''' <summary> Reads a "T:String" value from the INI file. </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="filePath">     A <see cref="System.String">String</see> expression that
    '''                             specifies the INI file name. Use an empty file name to read from
    '''                             Win.ini. </param>
    ''' <param name="sectionName">  A <see cref="System.String">String</see> expression that
    '''                             specifies the section in the INI file from which to read the
    '''                             settings. </param>
    ''' <param name="keyName">      A <see cref="System.String">String</see> expression that
    '''                             specifies a Key in the section from which to read the settings. 
    ''' </param>
    ''' <param name="defaultValue"> A <see cref="System.String">String</see> expression that
    '''                             specifies a default value to return if the key is not found. 
    ''' </param>
    ''' <returns> A <see cref="System.String">String</see> data type. </returns>
    Public Shared Function ReadBuffer(ByVal filePath As String, ByVal sectionName As String, ByVal keyName As String, ByVal defaultValue As String) As String
        If filePath Is Nothing Then Throw New System.ArgumentNullException(NameOf(filePath))
        If String.IsNullOrWhiteSpace(sectionName) Then Throw New System.ArgumentNullException(NameOf(sectionName))
        If keyName Is Nothing Then Throw New System.ArgumentNullException(NameOf(keyName))
        Dim buffer As System.Text.StringBuilder = New System.Text.StringBuilder(SettingsBufferSize)
        Dim length As Integer = If(filePath.Length > 0,
            SafeNativeMethods.GetPrivateProfileString(sectionName, keyName, defaultValue, buffer, buffer.Capacity, filePath),
            SafeNativeMethods.GetProfileString(sectionName, keyName, defaultValue, buffer, buffer.Capacity))
        Return If(length > 0, buffer.ToString(), defaultValue)
    End Function

    ''' <summary> Reads a value from the INI file. </summary>
    ''' <remarks>
    ''' The data type of the default value determines the type of the returned object.
    ''' </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="filePath">     A <see cref="System.String">String</see> expression that
    '''                             specifies the INI file name. Use an empty file name to read from
    '''                             Win.ini. </param>
    ''' <param name="sectionName">  A <see cref="System.String">String</see> expression that
    '''                             specifies the section in the INI file from which to read the
    '''                             settings. </param>
    ''' <param name="keyName">      A <see cref="System.String">String</see> expression that
    '''                             specifies a Key in the section from which to read the settings. 
    ''' </param>
    ''' <param name="defaultValue"> Is an Object expression that specifies a default value to return
    '''                             if the key is not found. </param>
    ''' <returns> A <see cref="System.String">String</see> data type. </returns>
    Public Shared Function Read(ByVal filePath As String, ByVal sectionName As String, ByVal keyName As String, ByVal defaultValue As Object) As Object
        If filePath Is Nothing Then Throw New System.ArgumentNullException(NameOf(filePath))
        If String.IsNullOrWhiteSpace(sectionName) Then Throw New System.ArgumentNullException(NameOf(sectionName))
        If keyName Is Nothing Then Throw New System.ArgumentNullException(NameOf(keyName))
        If defaultValue Is Nothing Then Throw New System.ArgumentNullException(NameOf(defaultValue))
        ' read setting from the file.
        Dim value As String = Read(filePath, sectionName, keyName, Core.Constructs.TypeConverter.Serialize(defaultValue))
        ' convert string to object
        Return Core.Constructs.TypeConverter.Deserialize(value, defaultValue)
    End Function

    ''' <summary> Reads a value from the INI file. </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="filePath">    A <see cref="System.String">String</see> expression that
    '''                            specifies the INI file name. Use an empty file name to read from
    '''                            Win.ini. </param>
    ''' <param name="sectionName"> A <see cref="System.String">String</see> expression that
    '''                            specifies the section in the INI file from which to read the
    '''                            settings. </param>
    ''' <param name="keyName">     A <see cref="System.String">String</see> expression that
    '''                            specifies a Key in the section from which to read the settings. 
    ''' </param>
    ''' <param name="dataType">    Is an System.Type expression that specifies the object type to
    '''                            return. </param>
    ''' <returns> A <see cref="System.String">String</see> data type. </returns>
    Public Shared Function Read(ByVal filePath As String, ByVal sectionName As String, ByVal keyName As String, ByVal dataType As System.Type) As Object
        If filePath Is Nothing Then Throw New System.ArgumentNullException(NameOf(filePath))
        If String.IsNullOrWhiteSpace(sectionName) Then Throw New System.ArgumentNullException(NameOf(sectionName))
        If keyName Is Nothing Then Throw New System.ArgumentNullException(NameOf(keyName))
        If dataType Is Nothing Then Throw New System.ArgumentNullException(NameOf(dataType))
        ' read setting from the file.
        Dim value As String = Read(filePath, sectionName, keyName)
        ' convert string to object
        Return Core.Constructs.TypeConverter.Deserialize(value, dataType)
    End Function

    ''' <summary> Reads a whole number value from the INI file. </summary>
    ''' <remarks> Use This method to read a whole number from an INI file. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="filePath">    A <see cref="System.String">String</see> expression that
    '''                            specifies the INI file name. </param>
    ''' <param name="sectionName"> A <see cref="System.String">String</see> expression that
    '''                            specifies the section in the INI file from which to read the
    '''                            settings. </param>
    ''' <param name="keyName">     A <see cref="System.String">String</see> expression that
    '''                            specifies a Key in the section from which to read the sSettings. 
    ''' </param>
    ''' <returns> Returns an Int32 data type. </returns>
    Public Shared Function ReadWholeNumber(ByVal filePath As String, ByVal sectionName As String, ByVal keyName As String) As Int32
        Dim defaultValue As Int32 = -1
        If filePath Is Nothing Then Throw New System.ArgumentNullException(NameOf(filePath))
        If String.IsNullOrWhiteSpace(sectionName) Then Throw New System.ArgumentNullException(NameOf(sectionName))
        If keyName Is Nothing Then Throw New System.ArgumentNullException(NameOf(keyName))
        Return SafeNativeMethods.GetPrivateProfile(sectionName, keyName, defaultValue, filePath)
    End Function

    ''' <summary> Reads a whole number value from the INI file. </summary>
    ''' <remarks> Use This method to read a whole number from an INI file. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="filePath">     A <see cref="System.String">String</see> expression that
    '''                             specifies the INI file name. </param>
    ''' <param name="sectionName">  A <see cref="System.String">String</see> expression that
    '''                             specifies the section in the INI file from which to read the
    '''                             settings. </param>
    ''' <param name="keyName">      A <see cref="System.String">String</see> expression that
    '''                             specifies a Key in the section from which to read the settings. 
    ''' </param>
    ''' <param name="defaultValue"> Is an Int32 expression that specifies a default value to return
    '''                             if the key is not found. </param>
    ''' <returns> Returns an Int32 data type. </returns>
    Public Shared Function ReadWholeNumber(ByVal filePath As String, ByVal sectionName As String, ByVal keyName As String, ByVal defaultValue As Int32) As Int32
        If filePath Is Nothing Then Throw New System.ArgumentNullException(NameOf(filePath))
        If String.IsNullOrWhiteSpace(sectionName) Then Throw New System.ArgumentNullException(NameOf(sectionName))
        If keyName Is Nothing Then Throw New System.ArgumentNullException(NameOf(keyName))
        Return SafeNativeMethods.GetPrivateProfile(sectionName, keyName, defaultValue, filePath)
    End Function

    ''' <summary> Writes a string value to the INI file. </summary>
    ''' <remarks> Use This method to write a String value to an INI file. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="filePath">    A <see cref="System.String">String</see> expression that
    '''                            specifies the INI file name. </param>
    ''' <param name="sectionName"> A <see cref="System.String">String</see> expression that
    '''                            specifies the section in the INI file to which to write the
    '''                            settings. </param>
    ''' <param name="keyName">     A <see cref="System.String">String</see> expression that
    '''                            specifies a Key in the section to which to write the settings. 
    ''' </param>
    ''' <param name="value">       A <see cref="System.String">String</see> expression that
    '''                            specifies the value to write. </param>
    ''' <returns> <c>True</c> if success; Otherwise, <c>False</c>. </returns>
    Public Shared Function Write(ByVal filePath As String, ByVal sectionName As String, ByVal keyName As String, ByVal value As String) As Boolean
        If filePath Is Nothing Then Throw New System.ArgumentNullException(NameOf(filePath))
        If String.IsNullOrWhiteSpace(sectionName) Then Throw New System.ArgumentNullException(NameOf(sectionName))
        If keyName Is Nothing Then Throw New System.ArgumentNullException(NameOf(keyName))
        Return If(filePath.Length > 0,
            SafeNativeMethods.WritePrivateProfileString(sectionName, keyName, value, filePath),
            SafeNativeMethods.WriteProfileString(sectionName, keyName, value))
    End Function

    ''' <summary> Writes a value to the INI file. </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="filePath">    A <see cref="System.String">String</see> expression that
    '''                            specifies the INI file name. </param>
    ''' <param name="sectionName"> A <see cref="System.String">String</see> expression that
    '''                            specifies the section in the INI file to which to write the
    '''                            settings. </param>
    ''' <param name="keyName">     A <see cref="System.String">String</see> expression that
    '''                            specifies a Key in the section to which to write the settings. 
    ''' </param>
    ''' <param name="value">       Is an Object expression that specifies the value to write. </param>
    ''' <returns> <c>True</c> if success; Otherwise, <c>False</c>. </returns>
    Public Shared Function Write(ByVal filePath As String, ByVal sectionName As String, ByVal keyName As String, ByVal value As Object) As Boolean
        If filePath Is Nothing Then Throw New System.ArgumentNullException(NameOf(filePath))
        If String.IsNullOrWhiteSpace(sectionName) Then Throw New System.ArgumentNullException(NameOf(sectionName))
        If keyName Is Nothing Then Throw New System.ArgumentNullException(NameOf(keyName))
        Return Write(filePath, sectionName, keyName, Core.Constructs.TypeConverter.Serialize(value))
    End Function

    ''' <summary> Writes a section to the INI file. </summary>
    ''' <remarks> Use This method to write a section to an INI file. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="filePath">    A <see cref="System.String">String</see> expression that
    '''                            specifies the INI file name. Specify an empty file name to read
    '''                            the WIN.INI file. </param>
    ''' <param name="sectionName"> A <see cref="System.String">String</see> expression that
    '''                            specifies the section in the INI file which to write. </param>
    ''' <param name="items">       The items. </param>
    ''' <returns> <c>True</c> if success; Otherwise, <c>False</c>. </returns>
    Public Shared Function WriteSection(ByVal filePath As String, ByVal sectionName As String, ByVal items As IEnumerable(Of String)) As Boolean
        If filePath Is Nothing Then Throw New System.ArgumentNullException(NameOf(filePath))
        If String.IsNullOrWhiteSpace(sectionName) Then Throw New System.ArgumentNullException(NameOf(sectionName))
        If items Is Nothing Then Throw New System.ArgumentNullException(NameOf(items))
        Dim buffer(32768) As Byte
        Dim bufferIndex As Int32 = 0
        Dim sectionItem As String
        For Each sectionItem In items
            Text.Encoding.ASCII.GetBytes(sectionItem, 0, sectionItem.Length, buffer, bufferIndex)
            bufferIndex += sectionItem.Length
            ' restore section item termination
            buffer(bufferIndex) = 0
            bufferIndex += 1
        Next
        ' add final section item termination
        buffer(bufferIndex) = 0
        ' write the section
        Return If(filePath.Length = 0,
            SafeNativeMethods.WriteProfileSection(sectionName, buffer),
            SafeNativeMethods.WritePrivateProfileSection(sectionName, buffer, filePath))
    End Function

    ''' <summary> Writes a section to the INI file. </summary>
    ''' <remarks> Use This method to write a section to an INI file. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="filePath">     A <see cref="System.String">String</see> expression that
    '''                             specifies the INI file name. Specify an empty file name to read
    '''                             the WIN.INI file. </param>
    ''' <param name="sectionName">  A <see cref="System.String">String</see> expression that
    '''                             specifies the section in the INI file which to write. </param>
    ''' <param name="sectionValue"> A <see cref="System.String">String</see> expression that
    '''                             specifies the section data. </param>
    ''' <returns> <c>True</c> if success; Otherwise, <c>False</c>. </returns>
    Public Shared Function WriteSection(ByVal filePath As String, ByVal sectionName As String, ByVal sectionValue As String) As Boolean
        If filePath Is Nothing Then Throw New System.ArgumentNullException(NameOf(filePath))
        If String.IsNullOrWhiteSpace(sectionName) Then Throw New System.ArgumentNullException(NameOf(sectionName))
        Return If(filePath.Length > 0,
            SafeNativeMethods.WritePrivateProfileSection(sectionName, sectionValue, filePath),
            SafeNativeMethods.WriteProfileSection(sectionName, sectionValue))
    End Function

    ''' <summary> Returns the win.ini full file name. </summary>
    ''' <value> <c>profileFilePath</c> is a read only string property. </value>
    Private Shared ReadOnly Property PublicProfileFilePath() As String
        Get
            Return System.IO.Path.Combine(Environment.SystemDirectory, "\win.ini")
        End Get
    End Property

    ''' <summary> Gets the size of the settings buffer. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <value> The size of the settings buffer. </value>
    Public Shared Property SettingsBufferSize() As Int32 = 256

    ''' <summary> Gets the size of the section buffer. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <value> The size of the section buffer. </value>
    Public Shared Property SectionBufferSize() As Int32 = 65535

#End Region

#Region " READ FILE SECTION SETTING "

    ''' <summary> Reads a "T:Boolean" value from the INI file. </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="filePath">     A <see cref="System.String">String</see> expression that
    '''                             specifies the INI file name. Use an empty file name to read from
    '''                             Win.ini. </param>
    ''' <param name="sectionName">  A <see cref="System.String">String</see> expression that
    '''                             specifies the section in the INI file from which to read the
    '''                             settings. </param>
    ''' <param name="keyName">      A <see cref="System.String">String</see> expression that
    '''                             specifies a Key in the section from which to read the settings. 
    ''' </param>
    ''' <param name="defaultValue"> A <see cref="System.Boolean">String</see> expression that
    '''                             specifies a default value to return if the key is not found. 
    ''' </param>
    ''' <returns> A <see cref="System.Boolean">Boolean</see> data type. </returns>
    Public Shared Function Read(ByVal filePath As String, ByVal sectionName As String, ByVal keyName As String, ByVal defaultValue As Boolean) As Boolean
        If filePath Is Nothing Then Throw New System.ArgumentNullException(NameOf(filePath))
        If String.IsNullOrWhiteSpace(sectionName) Then Throw New System.ArgumentNullException(NameOf(sectionName))
        If keyName Is Nothing Then Throw New System.ArgumentNullException(NameOf(keyName))
        Dim buffer As System.Text.StringBuilder = New System.Text.StringBuilder(SettingsBufferSize)
        Dim length As Integer = If(filePath.Length > 0,
            SafeNativeMethods.GetPrivateProfileString(sectionName, keyName, CStr(defaultValue), buffer, buffer.Capacity, filePath),
            SafeNativeMethods.GetProfileString(sectionName, keyName, CStr(defaultValue), buffer, buffer.Capacity))
        Return If(length > 0, Boolean.Parse(buffer.ToString()), defaultValue)
    End Function

    ''' <summary> Reads a "T:Byte" value from the INI file. </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="filePath">     A <see cref="System.String">String</see> expression that
    '''                             specifies the INI file name. Use an empty file name to read from
    '''                             Win.ini. </param>
    ''' <param name="sectionName">  A <see cref="System.String">String</see> expression that
    '''                             specifies the section in the INI file from which to read the
    '''                             settings. </param>
    ''' <param name="keyName">      A <see cref="System.String">String</see> expression that
    '''                             specifies a Key in the section from which to read the settings. 
    ''' </param>
    ''' <param name="defaultValue"> A <see cref="System.Byte">String</see> expression that specifies
    '''                             a default value to return if the key is not found. </param>
    ''' <returns> A <see cref="System.Byte">Byte</see> data type. </returns>
    Public Shared Function Read(ByVal filePath As String, ByVal sectionName As String, ByVal keyName As String, ByVal defaultValue As Byte) As Byte
        If filePath Is Nothing Then Throw New System.ArgumentNullException(NameOf(filePath))
        If String.IsNullOrWhiteSpace(sectionName) Then Throw New System.ArgumentNullException(NameOf(sectionName))
        If keyName Is Nothing Then Throw New System.ArgumentNullException(NameOf(keyName))
        Dim buffer As System.Text.StringBuilder = New System.Text.StringBuilder(SettingsBufferSize)
        Dim length As Integer = If(filePath.Length > 0,
            SafeNativeMethods.GetPrivateProfileString(sectionName, keyName, CStr(defaultValue), buffer, buffer.Capacity, filePath),
            SafeNativeMethods.GetProfileString(sectionName, keyName, CStr(defaultValue), buffer, buffer.Capacity))
        Return If(length > 0,
            Byte.Parse(buffer.ToString(), Globalization.NumberStyles.Number, Globalization.CultureInfo.CurrentCulture),
            defaultValue)
    End Function

    ''' <summary> Reads a "T:DateTime" value from the INI file. </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="filePath">     A <see cref="System.String">String</see> expression that
    '''                             specifies the INI file name. Use an empty file name to read from
    '''                             Win.ini. </param>
    ''' <param name="sectionName">  A <see cref="System.String">String</see> expression that
    '''                             specifies the section in the INI file from which to read the
    '''                             settings. </param>
    ''' <param name="keyName">      A <see cref="System.String">String</see> expression that
    '''                             specifies a Key in the section from which to read the settings. 
    ''' </param>
    ''' <param name="defaultValue"> A <see cref="System.DateTime">String</see> expression that
    '''                             specifies a default value to return if the key is not found. 
    ''' </param>
    ''' <returns> A <see cref="System.DateTime">Date Time</see> data type. </returns>
    Public Shared Function Read(ByVal filePath As String, ByVal sectionName As String, ByVal keyName As String, ByVal defaultValue As DateTime) As DateTime
        If filePath Is Nothing Then Throw New System.ArgumentNullException(NameOf(filePath))
        If String.IsNullOrWhiteSpace(sectionName) Then Throw New System.ArgumentNullException(NameOf(sectionName))
        If keyName Is Nothing Then Throw New System.ArgumentNullException(NameOf(keyName))
        Dim buffer As System.Text.StringBuilder = New System.Text.StringBuilder(SettingsBufferSize)
        Dim length As Integer = If(filePath.Length > 0,
            SafeNativeMethods.GetPrivateProfileString(sectionName, keyName, CStr(defaultValue), buffer, buffer.Capacity, filePath),
            SafeNativeMethods.GetProfileString(sectionName, keyName, CStr(defaultValue), buffer, buffer.Capacity))
        Return If(length > 0, DateTime.Parse(buffer.ToString(), Globalization.CultureInfo.CurrentCulture), defaultValue)
    End Function

    ''' <summary> Reads a string value from the INI file. </summary>
    ''' <remarks> Use This method to read a String value from an INI file. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="filePath">     A <see cref="System.String">String</see> expression that
    '''                             specifies the INI file name. Use an empty file name to read from
    '''                             Win.ini. </param>
    ''' <param name="sectionName">  A <see cref="System.String">String</see> expression that
    '''                             specifies the section in the INI file from which to read the
    '''                             settings. </param>
    ''' <param name="keyName">      A <see cref="System.String">String</see> expression that
    '''                             specifies a Key in the section from which to read the settings. 
    ''' </param>
    ''' <param name="defaultValue"> A <see cref="System.String">String</see> expression that
    '''                             specifies a default value to return if the key is not found. 
    ''' </param>
    ''' <returns> A <see cref="System.String">String</see> data type. </returns>
    Public Shared Function Read(ByVal filePath As String, ByVal sectionName As String, ByVal keyName As String, ByVal defaultValue As DateTimeOffset) As DateTimeOffset
        If filePath Is Nothing Then Throw New System.ArgumentNullException(NameOf(filePath))
        If String.IsNullOrWhiteSpace(sectionName) Then Throw New System.ArgumentNullException(NameOf(sectionName))
        If keyName Is Nothing Then Throw New System.ArgumentNullException(NameOf(keyName))
        Dim buffer As System.Text.StringBuilder = New System.Text.StringBuilder(SettingsBufferSize)
        Dim length As Integer = If(filePath.Length > 0,
            SafeNativeMethods.GetPrivateProfileString(sectionName, keyName, defaultValue.ToString("o"), buffer, buffer.Capacity, filePath),
            SafeNativeMethods.GetProfileString(sectionName, keyName, defaultValue.ToString("o"), buffer, buffer.Capacity))
        Return If(length > 0, DateTimeOffset.Parse(buffer.ToString(), Globalization.CultureInfo.CurrentCulture), defaultValue)
    End Function

    ''' <summary> Reads a "T:Decimal" value from the INI file. </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="filePath">     A <see cref="System.String">String</see> expression that
    '''                             specifies the INI file name. Use an empty file name to read from
    '''                             Win.ini. </param>
    ''' <param name="sectionName">  A <see cref="System.String">String</see> expression that
    '''                             specifies the section in the INI file from which to read the
    '''                             settings. </param>
    ''' <param name="keyName">      A <see cref="System.String">String</see> expression that
    '''                             specifies a Key in the section from which to read the settings. 
    ''' </param>
    ''' <param name="defaultValue"> A <see cref="System.Decimal">String</see> expression that
    '''                             specifies a default value to return if the key is not found. 
    ''' </param>
    ''' <returns> A <see cref="System.Decimal">double</see> data type. </returns>
    Public Shared Function Read(ByVal filePath As String, ByVal sectionName As String, ByVal keyName As String, ByVal defaultValue As Decimal) As Decimal
        If filePath Is Nothing Then Throw New System.ArgumentNullException(NameOf(filePath))
        If String.IsNullOrWhiteSpace(sectionName) Then Throw New System.ArgumentNullException(NameOf(sectionName))
        If keyName Is Nothing Then Throw New System.ArgumentNullException(NameOf(keyName))
        Dim buffer As System.Text.StringBuilder = New System.Text.StringBuilder(SettingsBufferSize)
        Dim length As Integer = If(filePath.Length > 0,
            SafeNativeMethods.GetPrivateProfileString(sectionName, keyName, CStr(defaultValue), buffer, buffer.Capacity, filePath),
            SafeNativeMethods.GetProfileString(sectionName, keyName, CStr(defaultValue), buffer, buffer.Capacity))
        Return If(length > 0,
            Decimal.Parse(buffer.ToString(), Globalization.NumberStyles.Number, Globalization.CultureInfo.CurrentCulture),
            defaultValue)
    End Function

    ''' <summary> Reads a "T:Double" value from the INI file. </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="filePath">     A <see cref="System.String">String</see> expression that
    '''                             specifies the INI file name. Use an empty file name to read from
    '''                             Win.ini. </param>
    ''' <param name="sectionName">  A <see cref="System.String">String</see> expression that
    '''                             specifies the section in the INI file from which to read the
    '''                             settings. </param>
    ''' <param name="keyName">      A <see cref="System.String">String</see> expression that
    '''                             specifies a Key in the section from which to read the settings. 
    ''' </param>
    ''' <param name="defaultValue"> A <see cref="System.Double">String</see> expression that
    '''                             specifies a default value to return if the key is not found. 
    ''' </param>
    ''' <returns> A <see cref="System.Double">Double</see> data type. </returns>
    Public Shared Function Read(ByVal filePath As String, ByVal sectionName As String, ByVal keyName As String, ByVal defaultValue As Double) As Double

        If filePath Is Nothing Then Throw New System.ArgumentNullException(NameOf(filePath))
        If String.IsNullOrWhiteSpace(sectionName) Then Throw New System.ArgumentNullException(NameOf(sectionName))
        If keyName Is Nothing Then Throw New System.ArgumentNullException(NameOf(keyName))
        Dim buffer As System.Text.StringBuilder = New System.Text.StringBuilder(SettingsBufferSize)
        Dim length As Integer = If(filePath.Length > 0,
            SafeNativeMethods.GetPrivateProfileString(sectionName, keyName, CStr(defaultValue), buffer, buffer.Capacity, filePath),
            SafeNativeMethods.GetProfileString(sectionName, keyName, CStr(defaultValue), buffer, buffer.Capacity))
        Return If(length > 0,
            Double.Parse(buffer.ToString(), Globalization.NumberStyles.Number, Globalization.CultureInfo.CurrentCulture),
            defaultValue)
    End Function

    ''' <summary> Reads a "T:Int16" value from the INI file. </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="filePath">     A <see cref="System.String">String</see> expression that
    '''                             specifies the INI file name. Use an empty file name to read from
    '''                             Win.ini. </param>
    ''' <param name="sectionName">  A <see cref="System.String">String</see> expression that
    '''                             specifies the section in the INI file from which to read the
    '''                             settings. </param>
    ''' <param name="keyName">      A <see cref="System.String">String</see> expression that
    '''                             specifies a Key in the section from which to read the settings. 
    ''' </param>
    ''' <param name="defaultValue"> A <see cref="System.Int16">String</see> expression that specifies
    '''                             a default value to return if the key is not found. </param>
    ''' <returns> An <see cref="System.Int16">Int16</see> data type. </returns>
    Public Shared Function Read(ByVal filePath As String, ByVal sectionName As String, ByVal keyName As String, ByVal defaultValue As Int16) As Int16
        If filePath Is Nothing Then Throw New System.ArgumentNullException(NameOf(filePath))
        If String.IsNullOrWhiteSpace(sectionName) Then Throw New System.ArgumentNullException(NameOf(sectionName))
        If keyName Is Nothing Then Throw New System.ArgumentNullException(NameOf(keyName))
        Dim buffer As System.Text.StringBuilder = New System.Text.StringBuilder(SettingsBufferSize)
        Dim length As Integer = If(filePath.Length > 0,
            SafeNativeMethods.GetPrivateProfileString(sectionName, keyName, CStr(defaultValue), buffer, buffer.Capacity, filePath),
            SafeNativeMethods.GetProfileString(sectionName, keyName, CStr(defaultValue), buffer, buffer.Capacity))
        Return If(length > 0,
            Int16.Parse(buffer.ToString(), Globalization.NumberStyles.Number, Globalization.CultureInfo.CurrentCulture),
            defaultValue)
    End Function

    ''' <summary> Reads a "T:Int32" value from the INI file. </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="filePath">     A <see cref="System.String">String</see> expression that
    '''                             specifies the INI file name. Use an empty file name to read from
    '''                             Win.ini. </param>
    ''' <param name="sectionName">  A <see cref="System.String">String</see> expression that
    '''                             specifies the section in the INI file from which to read the
    '''                             settings. </param>
    ''' <param name="keyName">      A <see cref="System.String">String</see> expression that
    '''                             specifies a Key in the section from which to read the settings. 
    ''' </param>
    ''' <param name="defaultValue"> A <see cref="System.Int32">String</see> expression that specifies
    '''                             a default value to return if the key is not found. </param>
    ''' <returns> An <see cref="System.Int32">Int32</see> data type. </returns>
    Public Shared Function Read(ByVal filePath As String, ByVal sectionName As String, ByVal keyName As String, ByVal defaultValue As Int32) As Int32
        If filePath Is Nothing Then Throw New System.ArgumentNullException(NameOf(filePath))
        If String.IsNullOrWhiteSpace(sectionName) Then Throw New System.ArgumentNullException(NameOf(sectionName))
        If keyName Is Nothing Then Throw New System.ArgumentNullException(NameOf(keyName))
        Dim buffer As System.Text.StringBuilder = New System.Text.StringBuilder(SettingsBufferSize)
        Dim length As Integer = If(filePath.Length > 0,
            SafeNativeMethods.GetPrivateProfileString(sectionName, keyName, CStr(defaultValue), buffer, buffer.Capacity, filePath),
            SafeNativeMethods.GetProfileString(sectionName, keyName, CStr(defaultValue), buffer, buffer.Capacity))
        Return If(length > 0,
            Int32.Parse(buffer.ToString(), Globalization.NumberStyles.Number, Globalization.CultureInfo.CurrentCulture),
            defaultValue)
    End Function

    ''' <summary> Reads a "T:Int64" value from the INI file. </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="filePath">     A <see cref="System.String">String</see> expression that
    '''                             specifies the INI file name. Use an empty file name to read from
    '''                             Win.ini. </param>
    ''' <param name="sectionName">  A <see cref="System.String">String</see> expression that
    '''                             specifies the section in the INI file from which to read the
    '''                             settings. </param>
    ''' <param name="keyName">      A <see cref="System.String">String</see> expression that
    '''                             specifies a Key in the section from which to read the settings. 
    ''' </param>
    ''' <param name="defaultValue"> A <see cref="System.Int64">String</see> expression that specifies
    '''                             a default value to return if the key is not found. </param>
    ''' <returns> An <see cref="System.Int64">Int64</see> data type. </returns>
    Public Shared Function Read(ByVal filePath As String, ByVal sectionName As String, ByVal keyName As String, ByVal defaultValue As Int64) As Int64
        If filePath Is Nothing Then Throw New System.ArgumentNullException(NameOf(filePath))
        If String.IsNullOrWhiteSpace(sectionName) Then Throw New System.ArgumentNullException(NameOf(sectionName))
        If keyName Is Nothing Then Throw New System.ArgumentNullException(NameOf(keyName))
        Dim buffer As System.Text.StringBuilder = New System.Text.StringBuilder(SettingsBufferSize)
        Dim length As Int64 = If(filePath.Length > 0,
            SafeNativeMethods.GetPrivateProfileString(sectionName, keyName, CStr(defaultValue), buffer, buffer.Capacity, filePath),
            SafeNativeMethods.GetProfileString(sectionName, keyName, CStr(defaultValue), buffer, buffer.Capacity))
        Return If(length > 0,
            Int64.Parse(buffer.ToString(), Globalization.NumberStyles.Number, Globalization.CultureInfo.CurrentCulture),
            defaultValue)
    End Function

    ''' <summary> Reads a "T:Single" value from the INI file. </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="filePath">     A <see cref="System.String">String</see> expression that
    '''                             specifies the INI file name. Use an empty file name to read from
    '''                             Win.ini. </param>
    ''' <param name="sectionName">  A <see cref="System.String">String</see> expression that
    '''                             specifies the section in the INI file from which to read the
    '''                             settings. </param>
    ''' <param name="keyName">      A <see cref="System.String">String</see> expression that
    '''                             specifies a Key in the section from which to read the settings. 
    ''' </param>
    ''' <param name="defaultValue"> A <see cref="System.Single">String</see> expression that
    '''                             specifies a default value to return if the key is not found. 
    ''' </param>
    ''' <returns> A <see cref="System.Single">Single</see> data type. </returns>
    Public Shared Function Read(ByVal filePath As String, ByVal sectionName As String, ByVal keyName As String, ByVal defaultValue As Single) As Single
        If filePath Is Nothing Then Throw New System.ArgumentNullException(NameOf(filePath))
        If String.IsNullOrWhiteSpace(sectionName) Then Throw New System.ArgumentNullException(NameOf(sectionName))
        If keyName Is Nothing Then Throw New System.ArgumentNullException(NameOf(keyName))
        Dim buffer As System.Text.StringBuilder = New System.Text.StringBuilder(SettingsBufferSize)
        Dim length As Single = If(filePath.Length > 0,
            SafeNativeMethods.GetPrivateProfileString(sectionName, keyName, CStr(defaultValue), buffer, buffer.Capacity, filePath),
            SafeNativeMethods.GetProfileString(sectionName, keyName, CStr(defaultValue), buffer, buffer.Capacity))
        Return If(length > 0,
            Single.Parse(buffer.ToString(), Globalization.NumberStyles.Number, Globalization.CultureInfo.CurrentCulture),
            defaultValue)
    End Function

#End Region

#Region " WRITE FILE SECTION SETTING "

    ''' <summary> Writes a "T:Boolean" value to the INI file. </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="filePath">    A <see cref="System.String">String</see> expression that
    '''                            specifies the INI file name. </param>
    ''' <param name="sectionName"> A <see cref="System.String">String</see> expression that
    '''                            specifies the section in the INI file to which to write the
    '''                            settings. </param>
    ''' <param name="keyName">     A <see cref="System.String">String</see> expression that
    '''                            specifies a Key in the section to which to write the settings. 
    ''' </param>
    ''' <param name="value">       A <see cref="System.Boolean">Boolean</see> expression that
    '''                            specifies the value to write. </param>
    ''' <returns> <c>True</c> if success; Otherwise, <c>False</c>. </returns>
    Public Shared Function Write(ByVal filePath As String, ByVal sectionName As String, ByVal keyName As String, ByVal value As Boolean) As Boolean
        If filePath Is Nothing Then Throw New System.ArgumentNullException(NameOf(filePath))
        If String.IsNullOrWhiteSpace(sectionName) Then Throw New System.ArgumentNullException(NameOf(sectionName))
        If keyName Is Nothing Then Throw New System.ArgumentNullException(NameOf(keyName))
        Return Write(filePath, sectionName, keyName, value.ToString(Globalization.CultureInfo.CurrentCulture))
    End Function

    ''' <summary> Writes a "T:DateTime" value to the INI file. </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="filePath">    A <see cref="System.String">String</see> expression that
    '''                            specifies the INI file name. </param>
    ''' <param name="sectionName"> A <see cref="System.String">String</see> expression that
    '''                            specifies the section in the INI file to which to write the
    '''                            settings. </param>
    ''' <param name="keyName">     A <see cref="System.String">String</see> expression that
    '''                            specifies a Key in the section to which to write the settings. 
    ''' </param>
    ''' <param name="value">       A <see cref="System.DateTime">DateTime</see> expression that
    '''                            specifies the value to write. </param>
    ''' <param name="format">      Specifies how to format the value. </param>
    ''' <returns> <c>True</c> if success; Otherwise, <c>False</c>. </returns>
    Public Shared Function Write(ByVal filePath As String, ByVal sectionName As String, ByVal keyName As String,
                                 ByVal value As DateTime, ByVal format As String) As Boolean
        If filePath Is Nothing Then Throw New System.ArgumentNullException(NameOf(filePath))
        If String.IsNullOrWhiteSpace(sectionName) Then Throw New System.ArgumentNullException(NameOf(sectionName))
        If keyName Is Nothing Then Throw New System.ArgumentNullException(NameOf(keyName))
        Return Write(filePath, sectionName, keyName, value.ToString(format, Globalization.CultureInfo.CurrentCulture))
    End Function

    ''' <summary> Writes a "T:DateTime" value to the INI file. </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="filePath">    A <see cref="System.String">String</see> expression that
    '''                            specifies the INI file name. </param>
    ''' <param name="sectionName"> A <see cref="System.String">String</see> expression that
    '''                            specifies the section in the INI file to which to write the
    '''                            settings. </param>
    ''' <param name="keyName">     A <see cref="System.String">String</see> expression that
    '''                            specifies a Key in the section to which to write the settings. 
    ''' </param>
    ''' <param name="value">       A <see cref="System.DateTime">DateTime</see> expression that
    '''                            specifies the value to write. </param>
    ''' <returns> <c>True</c> if success; Otherwise, <c>False</c>. </returns>
    Public Shared Function Write(ByVal filePath As String, ByVal sectionName As String, ByVal keyName As String,
                                 ByVal value As DateTime) As Boolean
        If filePath Is Nothing Then Throw New System.ArgumentNullException(NameOf(filePath))
        If String.IsNullOrWhiteSpace(sectionName) Then Throw New System.ArgumentNullException(NameOf(sectionName))
        If keyName Is Nothing Then Throw New System.ArgumentNullException(NameOf(keyName))
        Return Write(filePath, sectionName, keyName, value.ToString(Globalization.CultureInfo.CurrentCulture))
    End Function

    ''' <summary> Writes a "T:DateTimeOffset" value to the INI file. </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="filePath">    A <see cref="System.String">String</see> expression that
    '''                            specifies the INI file name. </param>
    ''' <param name="sectionName"> A <see cref="System.String">String</see> expression that
    '''                            specifies the section in the INI file to which to write the
    '''                            settings. </param>
    ''' <param name="keyName">     A <see cref="System.String">String</see> expression that
    '''                            specifies a Key in the section to which to write the settings. 
    ''' </param>
    ''' <param name="value">       A <see cref="System.String">String</see> expression that
    '''                            specifies the value to write. </param>
    ''' <param name="format">      Specifies how to format the value. </param>
    ''' <returns> <c>True</c> if success; Otherwise, <c>False</c>. </returns>
    Public Shared Function Write(ByVal filePath As String, ByVal sectionName As String, ByVal keyName As String,
                                 ByVal value As DateTimeOffset, ByVal format As String) As Boolean
        If filePath Is Nothing Then Throw New System.ArgumentNullException(NameOf(filePath))
        If String.IsNullOrWhiteSpace(sectionName) Then Throw New System.ArgumentNullException(NameOf(sectionName))
        If keyName Is Nothing Then Throw New System.ArgumentNullException(NameOf(keyName))
        Return Write(filePath, sectionName, keyName, value.ToString(format, Globalization.CultureInfo.CurrentCulture))
    End Function

    ''' <summary> Writes a string value to the INI file. </summary>
    ''' <remarks> Use This method to write a String value to an INI file. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="filePath">    A <see cref="System.String">String</see> expression that
    '''                            specifies the INI file name. </param>
    ''' <param name="sectionName"> A <see cref="System.String">String</see> expression that
    '''                            specifies the section in the INI file to which to write the
    '''                            settings. </param>
    ''' <param name="keyName">     A <see cref="System.String">String</see> expression that
    '''                            specifies a Key in the section to which to write the settings. 
    ''' </param>
    ''' <param name="value">       A <see cref="System.String">String</see> expression that
    '''                            specifies the value to write. </param>
    ''' <returns> <c>True</c> if success; Otherwise, <c>False</c>. </returns>
    Public Shared Function Write(ByVal filePath As String, ByVal sectionName As String, ByVal keyName As String,
                                 ByVal value As DateTimeOffset) As Boolean
        If filePath Is Nothing Then Throw New System.ArgumentNullException(NameOf(filePath))
        If String.IsNullOrWhiteSpace(sectionName) Then Throw New System.ArgumentNullException(NameOf(sectionName))
        If keyName Is Nothing Then Throw New System.ArgumentNullException(NameOf(keyName))
        Return Write(filePath, sectionName, keyName, value.ToString(Globalization.CultureInfo.CurrentCulture))
    End Function

    ''' <summary> Writes a "T:Decimal" value to the INI file. </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="filePath">    A <see cref="System.String">String</see> expression that
    '''                            specifies the INI file name. </param>
    ''' <param name="sectionName"> A <see cref="System.String">String</see> expression that
    '''                            specifies the section in the INI file to which to write the
    '''                            settings. </param>
    ''' <param name="keyName">     A <see cref="System.String">String</see> expression that
    '''                            specifies a Key in the section to which to write the settings. 
    ''' </param>
    ''' <param name="value">       A <see cref="System.Decimal">Decimal</see> expression that
    '''                            specifies the value to write. </param>
    ''' <param name="format">      Specifies how to format the value. </param>
    ''' <returns> <c>True</c> if success; Otherwise, <c>False</c>. </returns>
    Public Shared Function Write(ByVal filePath As String, ByVal sectionName As String, ByVal keyName As String,
                                 ByVal value As Decimal, ByVal format As String) As Boolean
        If filePath Is Nothing Then Throw New System.ArgumentNullException(NameOf(filePath))
        If String.IsNullOrWhiteSpace(sectionName) Then Throw New System.ArgumentNullException(NameOf(sectionName))
        If keyName Is Nothing Then Throw New System.ArgumentNullException(NameOf(keyName))
        Return Write(filePath, sectionName, keyName, value.ToString(format, Globalization.CultureInfo.CurrentCulture))
    End Function

    ''' <summary> Writes a "T:Decimal" value to the INI file. </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="filePath">    A <see cref="System.String">String</see> expression that
    '''                            specifies the INI file name. </param>
    ''' <param name="sectionName"> A <see cref="System.String">String</see> expression that
    '''                            specifies the section in the INI file to which to write the
    '''                            settings. </param>
    ''' <param name="keyName">     A <see cref="System.String">String</see> expression that
    '''                            specifies a Key in the section to which to write the settings. 
    ''' </param>
    ''' <param name="value">       A <see cref="System.Decimal">Decimal</see> expression that
    '''                            specifies the value to write. </param>
    ''' <returns> <c>True</c> if success; Otherwise, <c>False</c>. </returns>
    Public Shared Function Write(ByVal filePath As String, ByVal sectionName As String, ByVal keyName As String,
                                 ByVal value As Decimal) As Boolean
        If filePath Is Nothing Then Throw New System.ArgumentNullException(NameOf(filePath))
        If String.IsNullOrWhiteSpace(sectionName) Then Throw New System.ArgumentNullException(NameOf(sectionName))
        If keyName Is Nothing Then Throw New System.ArgumentNullException(NameOf(keyName))
        Return Write(filePath, sectionName, keyName, value.ToString(Globalization.CultureInfo.CurrentCulture))
    End Function

    ''' <summary> Writes a "T:Double" value to the INI file. </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="filePath">    A <see cref="System.String">String</see> expression that
    '''                            specifies the INI file name. </param>
    ''' <param name="sectionName"> A <see cref="System.String">String</see> expression that
    '''                            specifies the section in the INI file to which to write the
    '''                            settings. </param>
    ''' <param name="keyName">     A <see cref="System.String">String</see> expression that
    '''                            specifies a Key in the section to which to write the settings. 
    ''' </param>
    ''' <param name="value">       A <see cref="System.Double">Double</see> expression that
    '''                            specifies the value to write. </param>
    ''' <param name="format">      Specifies how to format the value. </param>
    ''' <returns> <c>True</c> if success; Otherwise, <c>False</c>. </returns>
    Public Shared Function Write(ByVal filePath As String, ByVal sectionName As String, ByVal keyName As String,
                                 ByVal value As Double, ByVal format As String) As Boolean
        If filePath Is Nothing Then Throw New System.ArgumentNullException(NameOf(filePath))
        If String.IsNullOrWhiteSpace(sectionName) Then Throw New System.ArgumentNullException(NameOf(sectionName))
        If keyName Is Nothing Then Throw New System.ArgumentNullException(NameOf(keyName))
        Return Write(filePath, sectionName, keyName, value.ToString(format, Globalization.CultureInfo.CurrentCulture))

    End Function

    ''' <summary> Writes a "T:Double" value to the INI file. </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="filePath">    A <see cref="System.String">String</see> expression that
    '''                            specifies the INI file name. </param>
    ''' <param name="sectionName"> A <see cref="System.String">String</see> expression that
    '''                            specifies the section in the INI file to which to write the
    '''                            settings. </param>
    ''' <param name="keyName">     A <see cref="System.String">String</see> expression that
    '''                            specifies a Key in the section to which to write the settings. 
    ''' </param>
    ''' <param name="value">       A <see cref="System.Double">Double</see> expression that
    '''                            specifies the value to write. </param>
    ''' <returns> <c>True</c> if success; Otherwise, <c>False</c>. </returns>
    Public Shared Function Write(ByVal filePath As String, ByVal sectionName As String, ByVal keyName As String,
                                 ByVal value As Double) As Boolean
        If filePath Is Nothing Then Throw New System.ArgumentNullException(NameOf(filePath))
        If String.IsNullOrWhiteSpace(sectionName) Then Throw New System.ArgumentNullException(NameOf(sectionName))
        If keyName Is Nothing Then Throw New System.ArgumentNullException(NameOf(keyName))
        Return Write(filePath, sectionName, keyName, value.ToString(Globalization.CultureInfo.CurrentCulture))
    End Function

    ''' <summary> Writes a "T:Int64" value to the INI file. </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="filePath">    A <see cref="System.String">String</see> expression that
    '''                            specifies the INI file name. </param>
    ''' <param name="sectionName"> A <see cref="System.String">String</see> expression that
    '''                            specifies the section in the INI file to which to write the
    '''                            settings. </param>
    ''' <param name="keyName">     A <see cref="System.String">String</see> expression that
    '''                            specifies a Key in the section to which to write the settings. 
    ''' </param>
    ''' <param name="value">       A <see cref="System.Int64">Int64</see> expression that specifies
    '''                            the value to write. </param>
    ''' <param name="format">      Specifies how to format the value. </param>
    ''' <returns> <c>True</c> if success; Otherwise, <c>False</c>. </returns>
    Public Shared Function Write(ByVal filePath As String, ByVal sectionName As String, ByVal keyName As String,
                                 ByVal value As Int64, ByVal format As String) As Boolean
        If filePath Is Nothing Then Throw New System.ArgumentNullException(NameOf(filePath))
        If String.IsNullOrWhiteSpace(sectionName) Then Throw New System.ArgumentNullException(NameOf(sectionName))
        If keyName Is Nothing Then Throw New System.ArgumentNullException(NameOf(keyName))
        Return Write(filePath, sectionName, keyName, value.ToString(format, Globalization.CultureInfo.CurrentCulture))
    End Function

    ''' <summary> Writes a "T:Int64" value to the INI file. </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="filePath">    A <see cref="System.String">String</see> expression that
    '''                            specifies the INI file name. </param>
    ''' <param name="sectionName"> A <see cref="System.String">String</see> expression that
    '''                            specifies the section in the INI file to which to write the
    '''                            settings. </param>
    ''' <param name="keyName">     A <see cref="System.String">String</see> expression that
    '''                            specifies a Key in the section to which to write the settings. 
    ''' </param>
    ''' <param name="value">       A <see cref="System.Int64">Int64</see> expression that specifies
    '''                            the value to write. </param>
    ''' <returns> <c>True</c> if success; Otherwise, <c>False</c>. </returns>
    Public Shared Function Write(ByVal filePath As String, ByVal sectionName As String, ByVal keyName As String,
                                 ByVal value As Int64) As Boolean
        If filePath Is Nothing Then Throw New System.ArgumentNullException(NameOf(filePath))
        If String.IsNullOrWhiteSpace(sectionName) Then Throw New System.ArgumentNullException(NameOf(sectionName))
        If keyName Is Nothing Then Throw New System.ArgumentNullException(NameOf(keyName))
        Return Write(filePath, sectionName, keyName, value.ToString(Globalization.CultureInfo.CurrentCulture))
    End Function

#End Region

#Region " NAMES "

    ''' <summary> Gets the name of the setting. </summary>
    ''' <remarks> Use This property to set or get the name of the INI Settings string. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <value>
    ''' <c>SettingsName</c> is a String property that can be read from or written to (read or write)
    ''' that specifies the name of the INI Settings string in the INI Settings file.
    ''' </value>
    ''' <seealso cref="ProfileScribe.FilePath"/>
    '''
    ''' <example>
    ''' This example sets a INI Settings file name, writes INI Settings strings to the file, and then
    ''' reads the information from back.
    ''' <code>
    ''' Sub Form_Click
    '''   ' Declare some variables.
    '''   Dim settingValue As Variant
    '''   ' instantiate the ISR INI Settings Scribe Class
    '''   Dim ini as isr.Configuration.IniSettings.Scribe
    '''   ini = New isr.Configuration.IniSettings.Scribe
    '''   ' Set to trap any errors
    '''   Try
    '''     ' Set the INI Settings file name
    '''     ini.FilePath = ini.DefaultFilePath()
    '''     ' Set the INI Settings Section
    '''     ini.SectionName = "Section1"
    '''     ' Write a string to the file String setting
    '''     ini.SettingName = "String"
    '''     ini.SettingValue = "A String"
    '''     ' Write an Int32 value
    '''     ini.SettingName = "Int32"
    '''     ini.SettingValue = Convert.ToInt32(0)
    '''     ' Write an Double value
    '''     ini.SettingName = "Double"
    '''     ini.settingFormat = "##0.000"
    '''     ini.SettingValue = convert.ToDouble(1.11)
    '''     ' Write two list items
    '''     ini.SettingName = "List"
    '''     ini.ListSetting(1) = "One"
    '''     ini.ListSetting(2) = "Two"
    '''     ' Read the string from the file
    '''     ini.SettingName = "String"
    '''     Me.Print "String: "; ini.SettingValue
    '''     ' Read an Int32 value
    '''     ini.SettingName = "Int32"
    '''     ini.DefaultValue= Convert.ToInt32(0)
    '''     Me.Print "Int32: "; ini.SettingValue
    '''     ' Read a double
    '''     ini.SettingName = "Double"
    '''     Me.Print "Double: "; ini.SettingValue
    '''     ' Read two list items
    '''     ini.SettingName = "List"
    '''     Me.Print ini.ListSetting(1)
    '''     Me.Print ini.ListSetting(2)
    '''     ' Read the entire section
    '''     Me.Print "Section: "; ini.sectionValue
    '''   Catch e As Exception
    '''     ' respond to any file name errors.
    '''     MsgBox e.ToString
    '''   End Try
    ''' End Sub  </code>
    ''' To run This example, paste the code fragment into a Windows Form class.
    ''' Run the program by pressing F5, and then click on the form.
    ''' </example>
    Public Property SettingName() As String

    ''' <summary> Gets the name of the section. </summary>
    ''' <remarks> Use This property to set or get the name of the INI Settings section. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <value>
    ''' <c>SectionName</c> is a String property that can be read from or written to (read or write)
    ''' that specifies the name of the section in the INI Settings file.
    ''' </value>
    ''' <seealso cref="ProfileScribe.SettingName"/>
    Public Property SectionName() As String

#End Region

#Region " SECTION "

    ''' <summary> Returns the section names. </summary>
    ''' <remarks>
    ''' Use This property to read or write an entire section to the INI Settings file.  When writing,
    ''' This property replaces the entire section erasing any information that was present in the
    ''' section and replacing it with new information.  Treat This property tenderly! <para>
    ''' 
    ''' When reading a section, returns a string with all the INI Settings in the section.</para>
    ''' <para>
    ''' 
    ''' If you do not specify a file name, the section is read from or written to the WIN.INI file.
    ''' </para>
    ''' </remarks>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process section names in this collection.
    ''' </returns>
    ''' <seealso cref="ProfileScribe.FilePath"/>
    Public Function SectionNames() As IEnumerable(Of String)
        ' If we have a file name, get the names
        Return ReadSection(Me.FilePath, Me.SectionName)
    End Function

    ''' <summary> Reads a whole section from the INI file. </summary>
    ''' <remarks>
    ''' Use This method to read a section from an INI file.  The section is returned in an enumerable
    ''' of string values where the section values are delimited by the null character (ASCII 0)
    ''' </remarks>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process read section in this collection.
    ''' </returns>
    Public Function ReadSection() As IEnumerable(Of String)
        Return ReadSection(Me.FilePath, Me.SectionName)
    End Function

    ''' <summary> Writes a section to the INI file. </summary>
    ''' <remarks> Use This method to write a section to an INI file. </remarks>
    ''' <param name="items"> The items. </param>
    ''' <returns> <c>True</c> if success; Otherwise, <c>False</c>. </returns>
    Public Function WriteSection(ByVal items As IEnumerable(Of String)) As Boolean
        Return WriteSection(Me.FilePath, Me.SectionName, items)
    End Function

#End Region

#Region " LIST "

    ''' <summary> Reads a list of INI Settings from a INI Settings file. </summary>
    ''' <remarks>
    ''' The format of the list of INI Settings is SettingName# (e.g., Setting1, Setting2,
    ''' ....).<para>
    ''' Reads all the INI Settings in the list in sequence until no more INI Settings are
    ''' found.</para> <para>
    ''' The list is dimensioned starting with First Item, which defaults to 1.  For example, the
    ''' first item value must be set to 0 in order to read INI Settings Setting0, Setting1,..'  or to
    ''' -1 to read the list ordered as -1,0,1,...</para>
    ''' </remarks>
    ''' <param name="firstItemIndex"> is an Int32 expression that specifies the index of the first list
    '''                               item (default is 1) </param>
    ''' <returns> The list. </returns>
    ''' <example>
    ''' This example writes a list of INI Settings to an INI file and then reads the list from the
    ''' file. <code>
    ''' Sub Form_Click <para>
    '''   ' Declare some data storage </para><para>
    '''   Dim i as Int32 </para><para>
    '''   ' Create an instance of the ISR INI Settings Scribe Class Dim ini as isr.IO.ProfileScribe
    '''   </para><para>
    '''   ' Create data to write to the INI Settings file Dim items As new List(of
    '''   String)</para><para>
    '''   For i = 0 to UBound(sngList)</para><para>
    '''     items.Add(Log(convert.ToSingle(i+1))).ToString)</para><para>
    '''   Next i </para><para>
    '''   ' Set the INI Settings file name, section name, and setting name </para><para>
    '''   ini.FilePath = ini.DefaultFilePath() </para><para>
    '''   ini.SectionName = "Section1" </para><para>
    '''   ini.SettingName = "Setting" </para><para>
    '''   ' Write the list to the file. </para><para>
    '''   ini.WriteList(items) </para><para>
    '''   ' Read the list from the file  </para><para>
    '''   items = ini.ReadList() </para><para>
    ''' End Sub </para></code>
    ''' To run This example, paste the code fragment into a Windows Form class. Run the program by
    ''' pressing F5, and then click on the form
    ''' </example>
    ''' <seealso cref="ProfileScribe.SettingName"/>
    Public Function ReadList(ByVal firstItemIndex As Int32) As IEnumerable(Of String)

        Dim items As New List(Of String)
        Dim currentItem As Integer = firstItemIndex
        Dim itemValue As String

        ' Loop through the list data.
        Do

            ' Read data from the INI Settings file
            itemValue = Read(Me.FilePath, Me.SectionName, Me.ListSettingName(currentItem), String.Empty)

            If (itemValue.Length > 0) Then

                ' add item to the list
                items.Add(itemValue)

                ' Increment item count
                currentItem += 1

            End If

        Loop Until itemValue.Length = 0

        ' return the list
        Return items

    End Function

    ''' <summary> Reads a list of INI Settings from a INI Settings file. </summary>
    ''' <remarks>
    ''' The format of the list of INI Settings is SettingName# (e.g., Setting1, Setting2,
    ''' ....).<para>
    ''' Reads all the INI Settings in the list in sequence until no more INI Settings are
    ''' found.</para> <para>
    ''' The list is dimensioned starting with First Item, which defaults to 1.  For example, the
    ''' first item value must be set to 0 in order to read INI Settings Setting0, Setting1,..'  or to
    ''' -1 to read the list ordered as -1,0,1,...</para>
    ''' </remarks>
    ''' <returns> The list. </returns>
    ''' <seealso cref="ProfileScribe.ReadList"/>
    Public Function ReadList() As IEnumerable(Of String)
        Return Me.ReadList(1)
    End Function

    ''' <summary> Writes a list of INI Settings to a INI Settings file. </summary>
    ''' <remarks>
    ''' The format of the list of INI Settings is SettingName# (e.g., Setting1, Setting2,
    ''' ....).<para>
    ''' Reads all the INI Settings in the list in sequence until no more INI Settings are
    ''' found.</para> <para>
    ''' The list is dimensioned starting with First Item, which defaults to 1.  For example, the
    ''' first item value must be set to 0 in order to read INI Settings Setting0, Setting1,..'  or to
    ''' -1 to read the list ordered as -1,0,1,...</para>
    ''' </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="values">         The list values. </param>
    ''' <param name="firstItemIndex"> is an Int32 expression that specifies the index of the first list
    '''                               item (default is 1) </param>
    ''' <seealso cref="ProfileScribe.SettingName"/>
    Public Sub WriteList(ByVal values As IEnumerable(Of String), ByVal firstItemIndex As Int32)
        If values Is Nothing Then Throw New ArgumentNullException(NameOf(values))
        Dim itemIndex As Int32 = firstItemIndex
        Dim itemValue As String
        ' Loop through the list data.
        For Each itemValue In values
            ' Write the list setting
            Write(Me.FilePath, Me.SectionName, Me.ListSettingName(itemIndex), itemValue)
            itemIndex += 1
        Next
    End Sub

    ''' <summary> Writes a list of INI Settings to a INI Settings file. </summary>
    ''' <remarks>
    ''' The format of the list of INI Settings is SettingName# (e.g., Setting1, Setting2,
    ''' ....).<para>
    ''' Reads all the INI Settings in the list in sequence until no more INI Settings are
    ''' found.</para> <para>
    ''' The list is dimensioned starting with First Item, which defaults to 1.  For example, the
    ''' first item value must be set to 0 in order to read INI Settings Setting0, Setting1,..'  or to
    ''' -1 to read the list ordered as -1,0,1,...</para>
    ''' </remarks>
    ''' <param name="values"> The list values. </param>
    ''' <seealso cref="ProfileScribe.ReadList"/>
    Public Sub WriteList(ByVal values As IEnumerable(Of String))
        Me.WriteList(values, 1)
    End Sub

    ''' <summary> This private property returns the name of the ini settings list. </summary>
    ''' <remarks>
    ''' Use This property to get the name of the list setting for reading or write a list item.
    ''' </remarks>
    ''' <param name="itemNumber"> Is an Int32 expression that specifies the item number in the list. </param>
    ''' <value> <c>_listSettingName</c> is a read only string property. </value>
    '''
    Private ReadOnly Property ListSettingName(ByVal itemNumber As Int32) As String
        Get
            Return String.Format(Globalization.CultureInfo.CurrentCulture, "{0}{1}", Me.SettingName, itemNumber)
        End Get
    End Property

#End Region

#Region " READ LIST ITEM "

    ''' <summary> Reads a "T:Boolean" list value from the INI file. </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    ''' <param name="itemNumber">   Specifies the list item number to read. </param>
    ''' <param name="defaultValue"> A <see cref="System.Boolean">String</see> expression that
    '''                             specifies a default value to return if the key is not found. 
    ''' </param>
    ''' <returns> A <see cref="System.Boolean">Boolean</see> data type. </returns>
    Public Function ReadListItem(ByVal itemNumber As Integer, ByVal defaultValue As Boolean) As Boolean
        Return Read(Me.FilePath, Me.SectionName, Me.ListSettingName(itemNumber), defaultValue)
    End Function

    ''' <summary> Reads a "T:Byte" list value from the INI file. </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    ''' <param name="itemNumber">   Specifies the list item number to read. </param>
    ''' <param name="defaultValue"> A <see cref="System.Byte">String</see> expression that specifies
    '''                             a default value to return if the key is not found. </param>
    ''' <returns> A <see cref="System.Byte">Byte</see> data type. </returns>
    Public Function ReadListItem(ByVal itemNumber As Integer, ByVal defaultValue As Byte) As Byte
        Return Read(Me.FilePath, Me.SectionName, Me.ListSettingName(itemNumber), defaultValue)
    End Function

    ''' <summary> Reads a "T:DateTime" list value from the INI file. </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    ''' <param name="itemNumber">   Specifies the list item number to read. </param>
    ''' <param name="defaultValue"> A <see cref="System.DateTime">String</see> expression that
    '''                             specifies a default value to return if the key is not found. 
    ''' </param>
    ''' <returns> A <see cref="System.DateTime">DateTime</see> data type. </returns>
    Public Function ReadListItem(ByVal itemNumber As Integer, ByVal defaultValue As DateTime) As DateTime
        Return Read(Me.FilePath, Me.SectionName, Me.ListSettingName(itemNumber), defaultValue)
    End Function

    ''' <summary> Reads a "T:DateTimeOffset" list value from the INI file. </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    ''' <param name="itemNumber">   Specifies the list item number to read. </param>
    ''' <param name="defaultValue"> A <see cref="System.DateTime">String</see> expression that
    '''                             specifies a default value to return if the key is not found. 
    ''' </param>
    ''' <returns> A <see cref="System.DateTimeOffset">DateTimeOffset</see> data type. </returns>
    Public Function ReadListItem(ByVal itemNumber As Integer, ByVal defaultValue As DateTimeOffset) As DateTimeOffset
        Return Read(Me.FilePath, Me.SectionName, Me.ListSettingName(itemNumber), defaultValue)
    End Function

    ''' <summary> Reads a "T:Decimal" list value from the INI file. </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    ''' <param name="itemNumber">   Specifies the list item number to read. </param>
    ''' <param name="defaultValue"> A <see cref="System.Decimal">String</see> expression that
    '''                             specifies a default value to return if the key is not found. 
    ''' </param>
    ''' <returns> A <see cref="System.Decimal">Decimal</see> data type. </returns>
    Public Function ReadListItem(ByVal itemNumber As Integer, ByVal defaultValue As Decimal) As Decimal
        Return Read(Me.FilePath, Me.SectionName, Me.ListSettingName(itemNumber), defaultValue)
    End Function

    ''' <summary> Reads a "T:Double" list value from the INI file. </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    ''' <param name="itemNumber">   Specifies the list item number to read. </param>
    ''' <param name="defaultValue"> A <see cref="System.Double">String</see> expression that
    '''                             specifies a default value to return if the key is not found. 
    ''' </param>
    ''' <returns> A <see cref="System.Double">Double</see> data type. </returns>
    Public Function ReadListItem(ByVal itemNumber As Integer, ByVal defaultValue As Double) As Double
        Return Read(Me.FilePath, Me.SectionName, Me.ListSettingName(itemNumber), defaultValue)
    End Function

    ''' <summary> Reads a "T:Int16" list value from the INI file. </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    ''' <param name="itemNumber">   Specifies the list item number to read. </param>
    ''' <param name="defaultValue"> A <see cref="System.Int16">String</see> expression that specifies
    '''                             a default value to return if the key is not found. </param>
    ''' <returns> A <see cref="System.Int16">Int16</see> data type. </returns>
    Public Function ReadListItem(ByVal itemNumber As Integer, ByVal defaultValue As Int16) As Int16
        Return Read(Me.FilePath, Me.SectionName, Me.ListSettingName(itemNumber), defaultValue)
    End Function

    ''' <summary> Reads a "T:Int32" list value from the INI file. </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    ''' <param name="itemNumber">   Specifies the list item number to read. </param>
    ''' <param name="defaultValue"> A <see cref="System.Int32">String</see> expression that specifies
    '''                             a default value to return if the key is not found. </param>
    ''' <returns> A <see cref="System.Int32">Int32</see> data type. </returns>
    Public Function ReadListItem(ByVal itemNumber As Integer, ByVal defaultValue As Int32) As Int32
        Return Read(Me.FilePath, Me.SectionName, Me.ListSettingName(itemNumber), defaultValue)
    End Function

    ''' <summary> Reads a "T:Int64" list value from the INI file. </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    ''' <param name="itemNumber">   Specifies the list item number to read. </param>
    ''' <param name="defaultValue"> A <see cref="System.Int64">String</see> expression that specifies
    '''                             a default value to return if the key is not found. </param>
    ''' <returns> A <see cref="System.Int64">Int64</see> data type. </returns>
    Public Function ReadListItem(ByVal itemNumber As Integer, ByVal defaultValue As Int64) As Int64
        Return Read(Me.FilePath, Me.SectionName, Me.ListSettingName(itemNumber), defaultValue)
    End Function

    ''' <summary> Reads a "T:Single" list value from the INI file. </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    ''' <param name="itemNumber">   Specifies the list item number to read. </param>
    ''' <param name="defaultValue"> A <see cref="System.Single">String</see> expression that
    '''                             specifies a default value to return if the key is not found. 
    ''' </param>
    ''' <returns> A <see cref="System.Single">Single</see> data type. </returns>
    Public Function ReadListItem(ByVal itemNumber As Integer, ByVal defaultValue As Single) As Single
        Return Read(Me.FilePath, Me.SectionName, Me.ListSettingName(itemNumber), defaultValue)
    End Function

    ''' <summary> Reads a "T:String" list value from the INI file. </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    ''' <param name="itemNumber">   Specifies the list item number to read. </param>
    ''' <param name="defaultValue"> A <see cref="System.String">String</see> expression that
    '''                             specifies a default value to return if the key is not found. 
    ''' </param>
    ''' <returns> A <see cref="System.String">String</see> data type. </returns>
    Public Function ReadListItem(ByVal itemNumber As Integer, ByVal defaultValue As String) As String
        Return Read(Me.FilePath, Me.SectionName, Me.ListSettingName(itemNumber), defaultValue)
    End Function

#End Region

#Region " READ SECTION VALUE "

    ''' <summary> Reads a "T:Boolean" Section value from the INI file. </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    ''' <param name="settingsName"> Name of the settings. </param>
    ''' <param name="defaultValue"> A <see cref="System.Boolean">String</see> expression that
    '''                             specifies a default value to return if the key is not found. 
    ''' </param>
    ''' <returns> A <see cref="System.Boolean">Boolean</see> data type. </returns>
    Public Function Read(ByVal settingsName As String, ByVal defaultValue As Boolean) As Boolean
        Return Read(Me.FilePath, Me.SectionName, settingsName, defaultValue)
    End Function

    ''' <summary> Reads a "T:Byte" Section value from the INI file. </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    ''' <param name="settingsName"> Name of the settings. </param>
    ''' <param name="defaultValue"> A <see cref="System.Byte">String</see> expression that specifies
    '''                             a default value to return if the key is not found. </param>
    ''' <returns> A <see cref="System.Byte">Byte</see> data type. </returns>
    Public Function Read(ByVal settingsName As String, ByVal defaultValue As Byte) As Byte
        Return Read(Me.FilePath, Me.SectionName, settingsName, defaultValue)
    End Function

    ''' <summary> Reads a "T:DateTime" Section value from the INI file. </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    ''' <param name="settingsName"> Name of the settings. </param>
    ''' <param name="defaultValue"> A <see cref="System.DateTime">String</see> expression that
    '''                             specifies a default value to return if the key is not found. 
    ''' </param>
    ''' <returns> A <see cref="System.DateTime">DateTime</see> data type. </returns>
    Public Function Read(ByVal settingsName As String, ByVal defaultValue As DateTime) As DateTime
        Return Read(Me.FilePath, Me.SectionName, settingsName, defaultValue)
    End Function

    ''' <summary> Reads a string value from the INI file. </summary>
    ''' <remarks> Use This method to read a String value from an INI file. </remarks>
    ''' <param name="settingsName"> Name of the settings. </param>
    ''' <param name="defaultValue"> A <see cref="System.DateTimeoffset">String</see> expression that
    '''                             specifies a default value to return if the key is not found. 
    ''' </param>
    ''' <returns> A <see cref="System.DateTimeOffset">DateTimeOffset</see> data type. </returns>
    Public Function Read(ByVal settingsName As String, ByVal defaultValue As DateTimeOffset) As DateTimeOffset
        Return Read(Me.FilePath, Me.SectionName, settingsName, defaultValue)
    End Function

    ''' <summary> Reads a "T:Decimal" Section value from the INI file. </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    ''' <param name="settingsName"> Name of the settings. </param>
    ''' <param name="defaultValue"> A <see cref="System.Decimal">String</see> expression that
    '''                             specifies a default value to return if the key is not found. 
    ''' </param>
    ''' <returns> A <see cref="System.Decimal">Decimal</see> data type. </returns>
    Public Function Read(ByVal settingsName As String, ByVal defaultValue As Decimal) As Decimal
        Return Read(Me.FilePath, Me.SectionName, settingsName, defaultValue)
    End Function

    ''' <summary> Reads a "T:Double" Section value from the INI file. </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    ''' <param name="settingsName"> Name of the settings. </param>
    ''' <param name="defaultValue"> A <see cref="System.Double">String</see> expression that
    '''                             specifies a default value to return if the key is not found. 
    ''' </param>
    ''' <returns> A <see cref="System.Double">Double</see> data type. </returns>
    Public Function Read(ByVal settingsName As String, ByVal defaultValue As Double) As Double
        Return Read(Me.FilePath, Me.SectionName, settingsName, defaultValue)
    End Function

    ''' <summary> Reads a "T:Int16" Section value from the INI file. </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    ''' <param name="settingsName"> Name of the settings. </param>
    ''' <param name="defaultValue"> A <see cref="System.Int16">String</see> expression that specifies
    '''                             a default value to return if the key is not found. </param>
    ''' <returns> A <see cref="System.Int16">Int16</see> data type. </returns>
    Public Function Read(ByVal settingsName As String, ByVal defaultValue As Int16) As Int16
        Return Read(Me.FilePath, Me.SectionName, settingsName, defaultValue)
    End Function

    ''' <summary> Reads a "T:Int32" Section value from the INI file. </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    ''' <param name="settingsName"> Name of the settings. </param>
    ''' <param name="defaultValue"> A <see cref="System.Int32">String</see> expression that specifies
    '''                             a default value to return if the key is not found. </param>
    ''' <returns> A <see cref="System.Int32">Int32</see> data type. </returns>
    Public Function Read(ByVal settingsName As String, ByVal defaultValue As Int32) As Int32
        Return Read(Me.FilePath, Me.SectionName, settingsName, defaultValue)
    End Function

    ''' <summary> Reads a "T:Int64" Section value from the INI file. </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    ''' <param name="settingsName"> Name of the settings. </param>
    ''' <param name="defaultValue"> A <see cref="System.Int64">String</see> expression that specifies
    '''                             a default value to return if the key is not found. </param>
    ''' <returns> A <see cref="System.Int64">Int64</see> data type. </returns>
    Public Function Read(ByVal settingsName As String, ByVal defaultValue As Int64) As Int64
        Return Read(Me.FilePath, Me.SectionName, settingsName, defaultValue)
    End Function

    ''' <summary> Reads a "T:Single" Section value from the INI file. </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    ''' <param name="settingsName"> Name of the settings. </param>
    ''' <param name="defaultValue"> A <see cref="System.Single">String</see> expression that
    '''                             specifies a default value to return if the key is not found. 
    ''' </param>
    ''' <returns> A <see cref="System.Single">Single</see> data type. </returns>
    Public Function Read(ByVal settingsName As String, ByVal defaultValue As Single) As Single
        Return Read(Me.FilePath, Me.SectionName, settingsName, defaultValue)
    End Function

    ''' <summary> Reads a "T:String" Section value from the INI file. </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    ''' <param name="settingsName"> Name of the settings. </param>
    ''' <param name="defaultValue"> A <see cref="System.String">String</see> expression that
    '''                             specifies a default value to return if the key is not found. 
    ''' </param>
    ''' <returns> A <see cref="System.String">String</see> data type. </returns>
    Public Function Read(ByVal settingsName As String, ByVal defaultValue As String) As String
        Return Read(Me.FilePath, Me.SectionName, settingsName, defaultValue)
    End Function

#End Region

#Region " READ VALUE "

    ''' <summary> Reads a "T:Boolean" value from the INI file. </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    ''' <param name="defaultValue"> A <see cref="System.Boolean">String</see> expression that
    '''                             specifies a default value to return if the key is not found. 
    ''' </param>
    ''' <returns> A <see cref="System.Boolean">Boolean</see> data type. </returns>
    Public Function Read(ByVal defaultValue As Boolean) As Boolean
        Return Read(Me.FilePath, Me.SectionName, Me.SettingName, defaultValue)
    End Function

    ''' <summary> Reads a "T:Byte" value from the INI file. </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    ''' <param name="defaultValue"> A <see cref="System.Byte">String</see> expression that specifies
    '''                             a default value to return if the key is not found. </param>
    ''' <returns> A <see cref="System.Byte">Byte</see> data type. </returns>
    Public Function Read(ByVal defaultValue As Byte) As Byte
        Return Read(Me.FilePath, Me.SectionName, Me.SettingName, defaultValue)
    End Function

    ''' <summary> Reads a "T:DateTime" value from the INI file. </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    ''' <param name="defaultValue"> A <see cref="System.DateTime">String</see> expression that
    '''                             specifies a default value to return if the key is not found. 
    ''' </param>
    ''' <returns> A <see cref="System.DateTime">DateTime</see> data type. </returns>
    Public Function Read(ByVal defaultValue As DateTime) As DateTime
        Return Read(Me.FilePath, Me.SectionName, Me.SettingName, defaultValue)
    End Function

    ''' <summary> Reads a string value from the INI file. </summary>
    ''' <remarks> Use This method to read a String value from an INI file. </remarks>
    ''' <param name="defaultValue"> A <see cref="System.DateTimeOffset">DateTimeOffset</see>
    '''                             expression that specifies a default value to return if the key is
    '''                             not found. </param>
    ''' <returns> A <see cref="System.String">String</see> data type. </returns>
    Public Function Read(ByVal defaultValue As DateTimeOffset) As DateTimeOffset
        Return Read(Me.FilePath, Me.SectionName, Me.SettingName, defaultValue)
    End Function

    ''' <summary> Reads a "T:Decimal" value from the INI file. </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    ''' <param name="defaultValue"> A <see cref="System.Decimal">String</see> expression that
    '''                             specifies a default value to return if the key is not found. 
    ''' </param>
    ''' <returns> A <see cref="System.Decimal">Decimal</see> data type. </returns>
    Public Function Read(ByVal defaultValue As Decimal) As Decimal
        Return Read(Me.FilePath, Me.SectionName, Me.SettingName, defaultValue)
    End Function

    ''' <summary> Reads a "T:Double" value from the INI file. </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    ''' <param name="defaultValue"> A <see cref="System.Double">String</see> expression that
    '''                             specifies a default value to return if the key is not found. 
    ''' </param>
    ''' <returns> A <see cref="System.Double">Double</see> data type. </returns>
    Public Function ReadDouble(ByVal defaultValue As Double) As Double
        Return Read(Me.FilePath, Me.SectionName, Me.SettingName, defaultValue)
    End Function

    ''' <summary> Reads a "T:Int16" value from the INI file. </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    ''' <param name="defaultValue"> A <see cref="System.Int16">String</see> expression that specifies
    '''                             a default value to return if the key is not found. </param>
    ''' <returns> A <see cref="System.Int16">Int16</see> data type. </returns>
    Public Function Read(ByVal defaultValue As Int16) As Int16
        Return Read(Me.FilePath, Me.SectionName, Me.SettingName, defaultValue)
    End Function

    ''' <summary> Reads a "T:Int32" value from the INI file. </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    ''' <param name="defaultValue"> A <see cref="System.Int32">String</see> expression that specifies
    '''                             a default value to return if the key is not found. </param>
    ''' <returns> A <see cref="System.Int32">Int32</see> data type. </returns>
    Public Function Read(ByVal defaultValue As Int32) As Int32
        Return Read(Me.FilePath, Me.SectionName, Me.SettingName, defaultValue)
    End Function

    ''' <summary> Reads a "T:Int64" value from the INI file. </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    ''' <param name="defaultValue"> A <see cref="System.Int64">String</see> expression that specifies
    '''                             a default value to return if the key is not found. </param>
    ''' <returns> A <see cref="System.Int64">Int64</see> data type. </returns>
    Public Function Read(ByVal defaultValue As Int64) As Int64
        Return Read(Me.FilePath, Me.SectionName, Me.SettingName, defaultValue)
    End Function

    ''' <summary> Reads a "T:Single" value from the INI file. </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    ''' <param name="defaultValue"> A <see cref="System.Single">String</see> expression that
    '''                             specifies a default value to return if the key is not found. 
    ''' </param>
    ''' <returns> A <see cref="System.Single">Single</see> data type. </returns>
    Public Function Read(ByVal defaultValue As Single) As Single
        Return Read(Me.FilePath, Me.SectionName, Me.SettingName, defaultValue)
    End Function

    ''' <summary> Reads a "T:String" value from the INI file. </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    ''' <param name="defaultValue"> A <see cref="System.String">String</see> expression that
    '''                             specifies a default value to return if the key is not found. 
    ''' </param>
    ''' <returns> A <see cref="System.String">String</see> data type. </returns>
    Public Function Read(ByVal defaultValue As String) As String
        Return Read(Me.FilePath, Me.SectionName, Me.SettingName, defaultValue)
    End Function

#End Region

#Region " WRITE LIST ITEEM "

    ''' <summary> Writes a "T:Boolean" value to the INI file. </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    ''' <param name="itemNumber"> Specifies the list item number to read. </param>
    ''' <param name="value">      A <see cref="System.Boolean">Boolean</see> expression that specifies
    '''                           the value to write. </param>
    ''' <returns> <c>True</c> if success; Otherwise, <c>False</c>. </returns>
    Public Function Write(ByVal itemNumber As Integer, ByVal value As Boolean) As Boolean
        Return Write(Me.FilePath, Me.SectionName, Me.ListSettingName(itemNumber), value)
    End Function

    ''' <summary> Writes a "T:DateTime" value to the INI file. </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    ''' <param name="itemNumber"> Specifies the list item number to read. </param>
    ''' <param name="value">      A <see cref="System.DateTime">DateTime</see> expression that
    '''                           specifies the value to write. </param>
    ''' <param name="format">     Specifies how to format the value. </param>
    ''' <returns> <c>True</c> if success; Otherwise, <c>False</c>. </returns>
    Public Function Write(ByVal itemNumber As Integer, ByVal value As DateTime, ByVal format As String) As Boolean
        Return Write(Me.FilePath, Me.SectionName, Me.ListSettingName(itemNumber), value, format)
    End Function

    ''' <summary> Writes a "T:DateTime" value to the INI file. </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    ''' <param name="itemNumber"> Specifies the list item number to read. </param>
    ''' <param name="value">      A <see cref="System.DateTime">DateTime</see> expression that
    '''                           specifies the value to write. </param>
    ''' <returns> <c>True</c> if success; Otherwise, <c>False</c>. </returns>
    Public Function Write(ByVal itemNumber As Integer, ByVal value As DateTime) As Boolean
        Return Write(Me.FilePath, Me.SectionName, Me.ListSettingName(itemNumber), value)
    End Function

    ''' <summary> Writes a "T:DateTime" value to the INI file. </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    ''' <param name="itemNumber"> Specifies the list item number to read. </param>
    ''' <param name="value">      A <see cref="System.DateTimeOffset">DateTimeOffset</see> expression
    '''                           that specifies the value to write. </param>
    ''' <param name="format">     Specifies how to format the value. </param>
    ''' <returns> <c>True</c> if success; Otherwise, <c>False</c>. </returns>
    Public Function Write(ByVal itemNumber As Integer, ByVal value As DateTimeOffset, ByVal format As String) As Boolean
        Return Write(Me.FilePath, Me.SectionName, Me.ListSettingName(itemNumber), value, format)
    End Function

    ''' <summary> Writes a "T:Boolean" value to the INI file. </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    ''' <param name="itemNumber"> Specifies the list item number to read. </param>
    ''' <param name="value">      A <see cref="System.DateTimeOffset">DateTimeOffset</see> expression
    '''                           that specifies the value to write. </param>
    ''' <returns> <c>True</c> if success; Otherwise, <c>False</c>. </returns>
    Public Function Write(ByVal itemNumber As Integer, ByVal value As DateTimeOffset) As Boolean
        Return Write(Me.FilePath, Me.SectionName, Me.ListSettingName(itemNumber), value)
    End Function

    ''' <summary> Writes a "T:Decimal" value to the INI file. </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    ''' <param name="itemNumber"> Specifies the list item number to read. </param>
    ''' <param name="value">      A <see cref="System.Decimal">Decimal</see> expression that specifies
    '''                           the value to write. </param>
    ''' <param name="format">     Specifies how to format the value. </param>
    ''' <returns> <c>True</c> if success; Otherwise, <c>False</c>. </returns>
    Public Function Write(ByVal itemNumber As Integer, ByVal value As Decimal, ByVal format As String) As Boolean
        Return Write(Me.FilePath, Me.SectionName, Me.ListSettingName(itemNumber), value, format)
    End Function

    ''' <summary> Writes a "T:Decimal" value to the INI file. </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    ''' <param name="itemNumber"> Specifies the list item number to read. </param>
    ''' <param name="value">      A <see cref="System.Decimal">Decimal</see> expression that specifies
    '''                           the value to write. </param>
    ''' <returns> <c>True</c> if success; Otherwise, <c>False</c>. </returns>
    Public Function Write(ByVal itemNumber As Integer, ByVal value As Decimal) As Boolean
        Return Write(Me.FilePath, Me.SectionName, Me.ListSettingName(itemNumber), value)
    End Function

    ''' <summary> Writes a "T:Double" value to the INI file. </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    ''' <param name="itemNumber"> Specifies the list item number to read. </param>
    ''' <param name="value">      A <see cref="System.Double">Double</see> expression that specifies
    '''                           the value to write. </param>
    ''' <param name="format">     Specifies how to format the value. </param>
    ''' <returns> <c>True</c> if success; Otherwise, <c>False</c>. </returns>
    Public Function Write(ByVal itemNumber As Integer, ByVal value As Double, ByVal format As String) As Boolean
        Return Write(Me.FilePath, Me.SectionName, Me.ListSettingName(itemNumber), value, format)
    End Function

    ''' <summary> Writes a "T:Double" value to the INI file. </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    ''' <param name="itemNumber"> Specifies the list item number to read. </param>
    ''' <param name="value">      A <see cref="System.Double">Double</see> expression that specifies
    '''                           the value to write. </param>
    ''' <returns> <c>True</c> if success; Otherwise, <c>False</c>. </returns>
    Public Function Write(ByVal itemNumber As Integer, ByVal value As Double) As Boolean
        Return Write(Me.FilePath, Me.SectionName, Me.ListSettingName(itemNumber), value)
    End Function

    ''' <summary> Writes a "T:Int64" value to the INI file. </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    ''' <param name="itemNumber"> Specifies the list item number to read. </param>
    ''' <param name="value">      A <see cref="System.Int64">Int64</see> expression that specifies the
    '''                           value to write. </param>
    ''' <param name="format">     Specifies how to format the value. </param>
    ''' <returns> <c>True</c> if success; Otherwise, <c>False</c>. </returns>
    Public Function Write(ByVal itemNumber As Integer, ByVal value As Int64, ByVal format As String) As Boolean
        Return Write(Me.FilePath, Me.SectionName, Me.ListSettingName(itemNumber), value, format)
    End Function

    ''' <summary> Writes a "T:Int64" value to the INI file. </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    ''' <param name="itemNumber"> Specifies the list item number to read. </param>
    ''' <param name="value">      A <see cref="System.Int64">Int64</see> expression that specifies the
    '''                           value to write. </param>
    ''' <returns> <c>True</c> if success; Otherwise, <c>False</c>. </returns>
    Public Function Write(ByVal itemNumber As Integer, ByVal value As Int64) As Boolean
        Return Write(Me.FilePath, Me.SectionName, Me.ListSettingName(itemNumber), value)
    End Function

    ''' <summary> Writes a "T:String" value to the INI file. </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    ''' <param name="itemNumber"> Specifies the list item number to read. </param>
    ''' <param name="value">      A <see cref="System.String">String</see> expression that specifies
    '''                           the value to write. </param>
    ''' <returns> <c>True</c> if success; Otherwise, <c>False</c>. </returns>
    Public Function Write(ByVal itemNumber As Integer, ByVal value As String) As Boolean
        Return Write(Me.FilePath, Me.SectionName, Me.ListSettingName(itemNumber), value)
    End Function

#End Region

#Region " WRITE SECTION VALUE "

    ''' <summary> Writes a "T:Boolean" value to the INI file. </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    ''' <param name="settingsName"> Name of the settings. </param>
    ''' <param name="value">        A <see cref="System.Boolean">Boolean</see> expression that
    '''                             specifies the value to write. </param>
    ''' <returns> <c>True</c> if success; Otherwise, <c>False</c>. </returns>
    Public Function Write(ByVal settingsName As String, ByVal value As Boolean) As Boolean
        Return Write(Me.FilePath, Me.SectionName, settingsName, value)
    End Function

    ''' <summary> Writes a "T:DateTime" value to the INI file. </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    ''' <param name="settingsName"> Name of the settings. </param>
    ''' <param name="value">        A <see cref="System.DateTime">DateTime</see> expression that
    '''                             specifies the value to write. </param>
    ''' <param name="format">       Specifies how to format the value. </param>
    ''' <returns> <c>True</c> if success; Otherwise, <c>False</c>. </returns>
    Public Function Write(ByVal settingsName As String, ByVal value As DateTime, ByVal format As String) As Boolean
        Return Write(Me.FilePath, Me.SectionName, settingsName, value, format)
    End Function

    ''' <summary> Writes a "T:DateTime" value to the INI file. </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    ''' <param name="settingsName"> Name of the settings. </param>
    ''' <param name="value">        A <see cref="System.DateTime">DateTime</see> expression that
    '''                             specifies the value to write. </param>
    ''' <returns> <c>True</c> if success; Otherwise, <c>False</c>. </returns>
    Public Function Write(ByVal settingsName As String, ByVal value As DateTime) As Boolean
        Return Write(Me.FilePath, Me.SectionName, settingsName, value)
    End Function

    ''' <summary> Writes a "T:DateTimeOffset" value to the INI file. </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    ''' <param name="settingsName"> Name of the settings. </param>
    ''' <param name="value">        A <see cref="System.DateTimeOffset">DateTimeOffset</see>
    '''                             expression that specifies the value to write. </param>
    ''' <param name="format">       Specifies how to format the value. </param>
    ''' <returns> <c>True</c> if success; Otherwise, <c>False</c>. </returns>
    Public Function Write(ByVal settingsName As String, ByVal value As DateTimeOffset, ByVal format As String) As Boolean
        Return Write(Me.FilePath, Me.SectionName, settingsName, value, format)
    End Function

    ''' <summary> Writes a "T:DateTimeOffset" value to the INI file. </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    ''' <param name="settingsName"> Name of the settings. </param>
    ''' <param name="value">        A <see cref="System.DateTimeOffset">DateTimeOffset</see>
    '''                             expression that specifies the value to write. </param>
    ''' <returns> <c>True</c> if success; Otherwise, <c>False</c>. </returns>
    Public Function Write(ByVal settingsName As String, ByVal value As DateTimeOffset) As Boolean
        Return Write(Me.FilePath, Me.SectionName, settingsName, value)
    End Function

    ''' <summary> Writes a "T:Decimal" value to the INI file. </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    ''' <param name="settingsName"> Name of the settings. </param>
    ''' <param name="value">        A <see cref="System.Decimal">Decimal</see> expression that
    '''                             specifies the value to write. </param>
    ''' <param name="format">       Specifies how to format the value. </param>
    ''' <returns> <c>True</c> if success; Otherwise, <c>False</c>. </returns>
    Public Function Write(ByVal settingsName As String, ByVal value As Decimal, ByVal format As String) As Boolean
        Return Write(Me.FilePath, Me.SectionName, settingsName, value, format)
    End Function

    ''' <summary> Writes a "T:Decimal" value to the INI file. </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    ''' <param name="settingsName"> Name of the settings. </param>
    ''' <param name="value">        A <see cref="System.Decimal">Decimal</see> expression that
    '''                             specifies the value to write. </param>
    ''' <returns> <c>True</c> if success; Otherwise, <c>False</c>. </returns>
    Public Function Write(ByVal settingsName As String, ByVal value As Decimal) As Boolean
        Return Write(Me.FilePath, Me.SectionName, settingsName, value)
    End Function

    ''' <summary> Writes a "T:Double" value to the INI file. </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    ''' <param name="settingsName"> Name of the settings. </param>
    ''' <param name="value">        A <see cref="System.Double">Double</see> expression that
    '''                             specifies the value to write. </param>
    ''' <param name="format">       Specifies how to format the value. </param>
    ''' <returns> <c>True</c> if success; Otherwise, <c>False</c>. </returns>
    Public Function Write(ByVal settingsName As String, ByVal value As Double, ByVal format As String) As Boolean
        Return Write(Me.FilePath, Me.SectionName, settingsName, value, format)
    End Function

    ''' <summary> Writes a "T:Double" value to the INI file. </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    ''' <param name="settingsName"> Name of the settings. </param>
    ''' <param name="value">        A <see cref="System.Double">Double</see> expression that
    '''                             specifies the value to write. </param>
    ''' <returns> <c>True</c> if success; Otherwise, <c>False</c>. </returns>
    Public Function Write(ByVal settingsName As String, ByVal value As Double) As Boolean
        Return Write(Me.FilePath, Me.SectionName, settingsName, value)
    End Function

    ''' <summary> Writes a "T:Int64" value to the INI file. </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    ''' <param name="settingsName"> Name of the settings. </param>
    ''' <param name="value">        A <see cref="System.Int64">Int64</see> expression that specifies
    '''                             the value to write. </param>
    ''' <param name="format">       Specifies how to format the value. </param>
    ''' <returns> <c>True</c> if success; Otherwise, <c>False</c>. </returns>
    Public Function Write(ByVal settingsName As String, ByVal value As Int64, ByVal format As String) As Boolean
        Return Write(Me.FilePath, Me.SectionName, settingsName, value, format)
    End Function

    ''' <summary> Writes a "T:Int64" value to the INI file. </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    ''' <param name="settingsName"> Name of the settings. </param>
    ''' <param name="value">        A <see cref="System.Int64">Int64</see> expression that specifies
    '''                             the value to write. </param>
    ''' <returns> <c>True</c> if success; Otherwise, <c>False</c>. </returns>
    Public Function Write(ByVal settingsName As String, ByVal value As Int64) As Boolean
        Return Write(Me.FilePath, Me.SectionName, settingsName, value)
    End Function

    ''' <summary> Writes a "T:String" value to the INI file. </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    ''' <param name="settingsName"> Name of the settings. </param>
    ''' <param name="value">        A <see cref="System.String">String</see> expression that
    '''                             specifies the value to write. </param>
    ''' <returns> <c>True</c> if success; Otherwise, <c>False</c>. </returns>
    Public Function Write(ByVal settingsName As String, ByVal value As String) As Boolean
        Return Write(Me.FilePath, Me.SectionName, settingsName, value)
    End Function

#End Region

#Region " WRITE VALUE "

    ''' <summary> Writes a "T:Boolean" value to the INI file. </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    ''' <param name="value"> A <see cref="System.Boolean">Boolean</see> expression that specifies the
    '''                      value to write. </param>
    ''' <returns> <c>True</c> if success; Otherwise, <c>False</c>. </returns>
    Public Function Write(ByVal value As Boolean) As Boolean
        Return Write(Me.FilePath, Me.SectionName, Me.SettingName, value)
    End Function

    ''' <summary> Writes a "T:Byte" value to the INI file. </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    ''' <param name="value">  A <see cref="System.Byte">Byte</see> expression that specifies the value
    '''                       to write. </param>
    ''' <param name="format"> Specifies how to format the value. </param>
    ''' <returns> <c>True</c> if success; Otherwise, <c>False</c>. </returns>
    Public Function Write(ByVal value As Byte, ByVal format As String) As Boolean
        Return Write(Me.FilePath, Me.SectionName, Me.SettingName, value, format)
    End Function

    ''' <summary> Writes a "T:Byte" value to the INI file. </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    ''' <param name="value"> A <see cref="System.Byte">Byte</see> expression that specifies the value
    '''                      to write. </param>
    ''' <returns> <c>True</c> if success; Otherwise, <c>False</c>. </returns>
    Public Function Write(ByVal value As Byte) As Boolean
        Return Write(Me.FilePath, Me.SectionName, Me.SettingName, value)
    End Function

    ''' <summary> Writes a "T:DateTime" value to the INI file. </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    ''' <param name="value">  A <see cref="System.DateTime">DateTime</see> expression that specifies
    '''                       the value to write. </param>
    ''' <param name="format"> Specifies how to format the value. </param>
    ''' <returns> <c>True</c> if success; Otherwise, <c>False</c>. </returns>
    Public Function Write(ByVal value As DateTime, ByVal format As String) As Boolean
        Return Write(Me.FilePath, Me.SectionName, Me.SettingName, value, format)
    End Function

    ''' <summary> Writes a "T:DateTime" value to the INI file. </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    ''' <param name="value"> A <see cref="System.DateTime">DateTime</see> expression that specifies
    '''                      the value to write. </param>
    ''' <returns> <c>True</c> if success; Otherwise, <c>False</c>. </returns>
    Public Function Write(ByVal value As DateTime) As Boolean
        Return Write(Me.FilePath, Me.SectionName, Me.SettingName, value)
    End Function

    ''' <summary> Writes a "T:DateTimeOffset" value to the INI file. </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    ''' <param name="value">  A <see cref="System.DateTimeOffset">DateTimeOffset</see> expression that
    '''                       specifies the value to write. </param>
    ''' <param name="format"> Specifies how to format the value. </param>
    ''' <returns> <c>True</c> if success; Otherwise, <c>False</c>. </returns>
    Public Function Write(ByVal value As DateTimeOffset, ByVal format As String) As Boolean
        Return Write(Me.FilePath, Me.SectionName, Me.SettingName, value, format)
    End Function

    ''' <summary> Writes a "T:DateTimeOffset" value to the INI file. </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    ''' <param name="value"> A <see cref="System.DateTimeOffset">DateTimeOffset</see> expression that
    '''                      specifies the value to write. </param>
    ''' <returns> <c>True</c> if success; Otherwise, <c>False</c>. </returns>
    Public Function Write(ByVal value As DateTimeOffset) As Boolean
        Return Write(Me.FilePath, Me.SectionName, Me.SettingName, value)
    End Function

    ''' <summary> Writes a "T:Decimal" value to the INI file. </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    ''' <param name="value">  A <see cref="System.Decimal">Decimal</see> expression that specifies the
    '''                       value to write. </param>
    ''' <param name="format"> Specifies how to format the value. </param>
    ''' <returns> <c>True</c> if success; Otherwise, <c>False</c>. </returns>
    Public Function Write(ByVal value As Decimal, ByVal format As String) As Boolean
        Return Write(Me.FilePath, Me.SectionName, Me.SettingName, value, format)
    End Function

    ''' <summary> Writes a "T:Decimal" value to the INI file. </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    ''' <param name="value"> A <see cref="System.Decimal">Decimal</see> expression that specifies the
    '''                      value to write. </param>
    ''' <returns> <c>True</c> if success; Otherwise, <c>False</c>. </returns>
    Public Function Write(ByVal value As Decimal) As Boolean
        Return Write(Me.FilePath, Me.SectionName, Me.SettingName, value)
    End Function

    ''' <summary> Writes a "T:Double" value to the INI file. </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    ''' <param name="value">  A <see cref="System.Double">Double</see> expression that specifies the
    '''                       value to write. </param>
    ''' <param name="format"> Specifies how to format the value. </param>
    ''' <returns> <c>True</c> if success; Otherwise, <c>False</c>. </returns>
    Public Function Write(ByVal value As Double, ByVal format As String) As Boolean
        Return Write(Me.FilePath, Me.SectionName, Me.SettingName, value, format)
    End Function

    ''' <summary> Writes a "T:Double" value to the INI file. </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    ''' <param name="value"> A <see cref="System.Double">Double</see> expression that specifies the
    '''                      value to write. </param>
    ''' <returns> <c>True</c> if success; Otherwise, <c>False</c>. </returns>
    Public Function Write(ByVal value As Double) As Boolean
        Return Write(Me.FilePath, Me.SectionName, Me.SettingName, value)
    End Function

    ''' <summary> Writes a "T:Int64" value to the INI file. </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    ''' <param name="value">  A <see cref="System.Int64">Int64</see> expression that specifies the
    '''                       value to write. </param>
    ''' <param name="format"> Specifies how to format the value. </param>
    ''' <returns> <c>True</c> if success; Otherwise, <c>False</c>. </returns>
    Public Function Write(ByVal value As Int64, ByVal format As String) As Boolean
        Return Write(Me.FilePath, Me.SectionName, Me.SettingName, value, format)
    End Function

    ''' <summary> Writes a "T:Int64" value to the INI file. </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    ''' <param name="value"> A <see cref="System.Int64">Int64</see> expression that specifies the
    '''                      value to write. </param>
    ''' <returns> <c>True</c> if success; Otherwise, <c>False</c>. </returns>
    Public Function Write(ByVal value As Int64) As Boolean
        Return Write(Me.FilePath, Me.SectionName, Me.SettingName, value)
    End Function

    ''' <summary> Writes a "T:String" value to the INI file. </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    ''' <param name="value"> A <see cref="System.String">String</see> expression that specifies the
    '''                      value to write. </param>
    ''' <returns> <c>True</c> if success; Otherwise, <c>False</c>. </returns>
    Public Function Write(ByVal value As String) As Boolean
        Return Write(Me.FilePath, Me.SectionName, Me.SettingName, value)
    End Function

#End Region

End Class
