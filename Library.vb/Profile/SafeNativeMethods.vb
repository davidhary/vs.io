Imports System.Runtime.InteropServices

''' <summary>
''' Gets or sets safe application programming interface calls. This class suppresses stack walks
''' for unmanaged code permission.  This class is for methods that are safe for anyone to call.
''' Callers of these methods are not required to do a full security review to ensure that the
''' usage is secure because the methods are harmless for any caller.
''' </summary>
''' <remarks>
''' David, 01/07/05, 1.0.1832. The A versions of the functions seems to work only when omitting
''' the character set specifications.<para>
''' (c) 2010 Integrated Scientific Resources, Inc. All rights reserved. </para><para>
''' Licensed under The MIT License. </para>
''' </remarks>
<AttributeUsage(AttributeTargets.Class Or AttributeTargets.Method Or AttributeTargets.Interface)>
Public NotInheritable Class SafeNativeMethods

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Prevents construction of this class. </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    Private Sub New()
    End Sub

#End Region

#Region " IMPORTS "

    ''' <summary> Gets profile section. </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    ''' <param name="sectionName">  Name of the section. </param>
    ''' <param name="returnedData"> Information describing the returned. </param>
    ''' <param name="dataLength">   Length of the data. </param>
    ''' <returns> The length of the profile section. </returns>
    <DllImport("KERNEL32.DLL", EntryPoint:="GetProfileSectionW", CharSet:=CharSet.Unicode)>
    Friend Shared Function GetProfileSection(ByVal sectionName As String, ByVal returnedData As Byte(), ByVal dataLength As Int32) As Int32
    End Function

    ''' <summary> Gets private profile section. </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    ''' <param name="sectionName">  Name of the section. </param>
    ''' <param name="returnedData"> Information describing the returned. </param>
    ''' <param name="dataLength">   Length of the data. </param>
    ''' <param name="filePath">     Full pathname of the file. </param>
    ''' <returns> The length of the private profile section. </returns>
    <DllImport("KERNEL32.DLL", EntryPoint:="GetPrivateProfileSectionW", CharSet:=CharSet.Unicode)>
    Friend Shared Function GetPrivateProfileSection(ByVal sectionName As String, ByVal returnedData As Byte(),
                                                    ByVal dataLength As Int32, ByVal filePath As String) As Int32
    End Function

    ''' <summary> Gets private profile section names. </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    ''' <param name="returnedData"> Information describing the returned. </param>
    ''' <param name="dataLength">   Length of the data. </param>
    ''' <param name="filePath">     Full pathname of the file. </param>
    ''' <returns> The length of the private profile section names. </returns>
    <DllImport("KERNEL32.DLL", EntryPoint:="GetPrivateProfileSectionNamesW", CharSet:=CharSet.Unicode)>
    Friend Shared Function GetPrivateProfileSectionNames(ByVal returnedData As Byte(), ByVal dataLength As Int32,
                                                         ByVal filePath As String) As Int32
    End Function

    ''' <summary> Gets profile string. </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    ''' <param name="sectionName">  Name of the section. </param>
    ''' <param name="keyName">      Name of the key. </param>
    ''' <param name="defaultData">  The default data. </param>
    ''' <param name="returnedData"> Information describing the returned. </param>
    ''' <param name="dataLength">   Length of the data. </param>
    ''' <returns> The profile string. </returns>
    <CodeAnalysis.SuppressMessage("Code Quality", "IDE0051:Remove unused private members", Justification:="<Pending>")>
    <DllImport("KERNEL32.DLL", EntryPoint:="GetProfileStringW", CharSet:=CharSet.Unicode)>
    Friend Shared Function GetProfileString(ByVal sectionName As String, ByVal keyName As String,
                                            ByVal defaultData As String, ByVal returnedData As String,
                                            ByVal dataLength As Int32) As Int32
    End Function

    ''' <summary> Gets profile string. </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    ''' <param name="sectionName">  Name of the section. </param>
    ''' <param name="keyName">      Name of the key. </param>
    ''' <param name="defaultData">  The default data. </param>
    ''' <param name="returnedData"> Information describing the returned. </param>
    ''' <param name="dataLength">   Length of the data. </param>
    ''' <returns> The length of the profile string. </returns>
    <DllImport("KERNEL32.DLL", EntryPoint:="GetProfileStringW", CharSet:=CharSet.Unicode)>
    Friend Shared Function GetProfileString(ByVal sectionName As String, ByVal keyName As String,
                                            ByVal defaultData As String, ByVal returnedData As System.Text.StringBuilder,
                                            ByVal dataLength As Int32) As Int32
    End Function

    ''' <summary> Gets private profile string. </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    ''' <param name="sectionName">  Name of the section. </param>
    ''' <param name="keyName">      Name of the key. </param>
    ''' <param name="defaultData">  The default data. </param>
    ''' <param name="returnedData"> Information describing the returned. </param>
    ''' <param name="dataLength">   Length of the data. </param>
    ''' <param name="filePath">     Full pathname of the file. </param>
    ''' <returns> The length of the private profile string. </returns>
    <CodeAnalysis.SuppressMessage("Code Quality", "IDE0051:Remove unused private members", Justification:="<Pending>")>
    <DllImport("kernel32.dll", EntryPoint:="GetPrivateProfileStringW", CharSet:=CharSet.Unicode)>
    Friend Shared Function GetPrivateProfileString(ByVal sectionName As String, ByVal keyName As String,
                                                   ByVal defaultData As String, ByVal returnedData As String,
                                                   ByVal dataLength As Integer, ByVal filePath As String) As Integer
    End Function

    ''' <summary>
    ''' Reads private profile string using 'GetPrivateProfileStringA' without specifying the
    ''' character set.
    ''' </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    ''' <param name="sectionName"> Name of the section. </param>
    ''' <param name="keyName"> Name of the key. </param>
    ''' <param name="defaultData"> The default data. </param>
    ''' <param name="returnedData"> Information describing the returned. </param>
    ''' <param name="dataLength"> Length of the data. </param>
    ''' <param name="filePath"> Full pathname of the file. </param>
    ''' <returns> The length of the profile string. </returns>
    <CodeAnalysis.SuppressMessage("Code Quality", "IDE0051:Remove unused private members", Justification:="<Pending>")>
    <Obsolete("Using GetPrivateProfileStringA fails reading a string such as 7/20/2019'; it reads only 7")>
    <DllImport("kernel32.dll", EntryPoint:="GetPrivateProfileStringA")>
    Friend Shared Function ReadPrivateProfileString(<MarshalAs(UnmanagedType.LPWStr)> ByVal sectionName As String,
                                                    <MarshalAs(UnmanagedType.LPWStr)> ByVal keyName As String,
                                                    <MarshalAs(UnmanagedType.LPWStr)> ByVal defaultData As String,
                                                    <MarshalAs(UnmanagedType.LPWStr)> ByVal returnedData As String,
                                                    ByVal dataLength As Integer,
                                                    <MarshalAs(UnmanagedType.LPWStr)> ByVal filePath As String) As Integer
    End Function

    ''' <summary> Gets private profile string. </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    ''' <param name="sectionName">  Name of the section. </param>
    ''' <param name="keyName">      Name of the key. </param>
    ''' <param name="defaultData">  The default data. </param>
    ''' <param name="returnedData"> Information describing the returned. </param>
    ''' <param name="dataLength">   Length of the data. </param>
    ''' <param name="filePath">     Full pathname of the file. </param>
    ''' <returns> The length of the private profile string. </returns>
    <DllImport("KERNEL32.DLL", EntryPoint:="GetPrivateProfileStringW", CharSet:=CharSet.Unicode)>
    Friend Shared Function GetPrivateProfileString(ByVal sectionName As String, ByVal keyName As String,
                                                   ByVal defaultData As String, ByVal returnedData As System.Text.StringBuilder,
                                                   ByVal dataLength As Int32, ByVal filePath As String) As Int32
    End Function

    ''' <summary> Gets private profile integer. </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    ''' <param name="sectionName"> Name of the section. </param>
    ''' <param name="keyName">     Name of the key. </param>
    ''' <param name="defaultData"> The default data. </param>
    ''' <param name="filePath">    Full pathname of the file. </param>
    ''' <returns> The private profile Integer. </returns>
    <DllImport("KERNEL32.DLL", EntryPoint:="GetPrivateProfileIntW", CharSet:=CharSet.Unicode)>
    Friend Shared Function GetPrivateProfile(ByVal sectionName As String, ByVal keyName As String,
                                             ByVal defaultData As Int32, ByVal filePath As String) As Int32
    End Function

    ''' <summary>
    ''' Writes a private profile section. Writes a list of all names and values for a section of a
    ''' private profile (.ini) file.
    ''' </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    ''' <param name="sectionName"> Name of the section. </param>
    ''' <param name="data">        The data. </param>
    ''' <param name="filePath">    Full pathname of the file. </param>
    ''' <returns> <c>True</c>  if okay; Otherwise, <c>False</c>. </returns>
    <DllImport("KERNEL32.DLL", EntryPoint:="WritePrivateProfileSectionW", CharSet:=CharSet.Unicode)>
    Friend Shared Function WritePrivateProfileSection(ByVal sectionName As String, ByVal data As String,
                                                      ByVal filePath As String) As <MarshalAs(UnmanagedType.Bool)> Boolean
    End Function

    ''' <summary> Writes a private profile section. </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    ''' <param name="sectionName"> Name of the section. </param>
    ''' <param name="data">        The data. </param>
    ''' <param name="filePath">    Full pathname of the file. </param>
    ''' <returns> <c>True</c>  if okay; Otherwise, <c>False</c>. </returns>
    <DllImport("KERNEL32.DLL", EntryPoint:="WritePrivateProfileSectionW", CharSet:=CharSet.Unicode)>
    Friend Shared Function WritePrivateProfileSection(ByVal sectionName As String, ByVal data As Byte(),
                                                      ByVal filePath As String) As <MarshalAs(UnmanagedType.Bool)> Boolean
    End Function

    ''' <summary> Writes a private profile string. </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    ''' <param name="sectionName"> Name of the section. </param>
    ''' <param name="keyName">     Name of the key. </param>
    ''' <param name="data">        The data. </param>
    ''' <param name="filePath">    Full pathname of the file. </param>
    ''' <returns> <c>True</c>  if okay; Otherwise, <c>False</c>. </returns>
    <DllImport("KERNEL32.DLL", EntryPoint:="WritePrivateProfileStringW", CharSet:=CharSet.Unicode)>
    Friend Shared Function WritePrivateProfileString(ByVal sectionName As String, ByVal keyName As String,
                                                     ByVal data As String, ByVal filePath As String) As <MarshalAs(UnmanagedType.Bool)> Boolean
        ' Leave function empty - Import attribute forwards calls to KERNEL32.DLL.
    End Function

    ''' <summary> Writes a private profile string. </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    ''' <param name="sectionName"> Name of the section. </param>
    ''' <param name="data">        The data. </param>
    ''' <param name="filePath">    Full pathname of the file. </param>
    ''' <returns> <c>True</c>  if okay; Otherwise, <c>False</c>. </returns>
    <CodeAnalysis.SuppressMessage("Code Quality", "IDE0051:Remove unused private members", Justification:="<Pending>")>
    <DllImport("KERNEL32.DLL", EntryPoint:="WritePrivateProfileStringW",
       SetLastError:=True, CharSet:=CharSet.Unicode,
       ExactSpelling:=True,
       CallingConvention:=CallingConvention.StdCall)>
    Friend Shared Function WritePrivateProfileString(ByVal sectionName As String, ByVal data As Byte(),
                                                     ByVal filePath As String) As <MarshalAs(UnmanagedType.Bool)> Boolean
    End Function

    ''' <summary>
    ''' Writes a profile section. Writes a list of all names and values for a section of the WIN.INI
    ''' file.
    ''' </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    ''' <param name="sectionName"> Name of the section. </param>
    ''' <param name="data">        The data. </param>
    ''' <returns> <c>True</c>  if okay; Otherwise, <c>False</c>. </returns>
    <DllImport("KERNEL32.DLL", EntryPoint:="WriteProfileSectionW", SetLastError:=True, CharSet:=CharSet.Unicode, ExactSpelling:=True,
        CallingConvention:=CallingConvention.StdCall)>
    Friend Shared Function WriteProfileSection(ByVal sectionName As String, ByVal data As Byte()) As <MarshalAs(UnmanagedType.Bool)> Boolean
    End Function

    ''' <summary> Writes a profile section. </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    ''' <param name="sectionName"> Name of the section. </param>
    ''' <param name="data">        The data. </param>
    ''' <returns> <c>True</c>  if okay; Otherwise, <c>False</c>. </returns>
    <DllImport("KERNEL32.DLL", EntryPoint:="WriteProfileSectionW", CharSet:=CharSet.Unicode)>
    Friend Shared Function WriteProfileSection(ByVal sectionName As String, ByVal data As String) As <MarshalAs(UnmanagedType.Bool)> Boolean
    End Function

    ''' <summary> Writes a profile string. Writes a string to the WIN.INI file. </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    ''' <param name="sectionName"> Name of the section. </param>
    ''' <param name="keyName">     Name of the key. </param>
    ''' <param name="data">        The data. </param>
    ''' <returns> <c>True</c>  if okay; Otherwise, <c>False</c>. </returns>
    <DllImport("KERNEL32.DLL", EntryPoint:="WriteProfileStringW", CharSet:=CharSet.Unicode)>
    Friend Shared Function WriteProfileString(ByVal sectionName As String, ByVal keyName As String,
                                              ByVal data As String) As <MarshalAs(UnmanagedType.Bool)> Boolean
    End Function

#End Region

End Class

