Namespace NetList

    ''' <summary>
    ''' This is the <see cref="isr.IO.NetList.Connector">Connector</see> class of the net list reader
    ''' class library. A connector consists of a set of pins.
    ''' </summary>
    ''' <remarks>
    ''' (c) 2007 Integrated Scientific Resources, Inc. All rights reserved. <para>
    ''' Licensed under The MIT License.</para><para>
    ''' David, 08/07/08, 1.2.3141.x. Add support for military style pins. Allow populating the
    ''' connector before reading the connectors. </para> <para>
    ''' David, 12/18/07, 1.2.2908.x. </para>
    ''' </remarks>
    Public Class Connector

        Implements IDisposable

#Region " CONSTRUCTION and CLEANUP "

        ''' <summary> Constructs this class. </summary>
        ''' <remarks> David, 10/8/2020. </remarks>
        Public Sub New()

            ' instantiate the class
            MyBase.New()
            Me._Pins = New Collections.Generic.Dictionary(Of String, Pin)
            Me._ShellPinNumber = "SH"
            Me.MatingSide = "P1"
        End Sub

        ''' <summary> Calls <see cref="M:Dispose(Boolean Disposing)"/> to cleanup. </summary>
        ''' <remarks>
        ''' Do not make this method Overridable (virtual) because a derived class should not be able to
        ''' override this method.
        ''' </remarks>
        Public Sub Dispose() Implements IDisposable.Dispose

            ' Do not change this code.  Put cleanup code in Dispose(Boolean) below.

            ' this disposes all child classes.
            Me.Dispose(True)

            ' Take this object off the finalization(Queue) and prevent finalization code 
            ' from executing a second time.
            GC.SuppressFinalize(Me)

        End Sub

        ''' <summary> True if is disposed, false if not. </summary>
        Private _IsDisposed As Boolean

        ''' <summary>
        ''' Gets or sets the dispose status sentinel of the base class.  This applies to the derived
        ''' class provided proper implementation.
        ''' </summary>
        ''' <value> The is disposed. </value>
        Protected Property IsDisposed() As Boolean
            Get
                Return Me._IsDisposed
            End Get
            Private Set(ByVal value As Boolean)
                Me._IsDisposed = value
            End Set
        End Property

        ''' <summary>
        ''' Releases the unmanaged resources used by the <see cref="T:System.Windows.Forms.Control" />
        ''' and its child controls and optionally releases the managed resources.
        ''' </summary>
        ''' <remarks> David, 10/8/2020. </remarks>
        ''' <param name="disposing"> <c>True</c> to release both managed and unmanaged resources;
        '''                                                   <c>False</c> to release only unmanaged
        '''                                                   resources when called from the runtime
        '''                                                   finalize. </param>
        <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
        <System.Diagnostics.DebuggerNonUserCode()>
        Protected Overridable Sub Dispose(ByVal disposing As Boolean)
            Try
                If Not Me.IsDisposed AndAlso disposing Then
                    Me._Pins = Nothing
                End If
            Finally
                Me.IsDisposed = True
            End Try
        End Sub

        ''' <summary>
        ''' This destructor will run only if the Dispose method does not get called. It gives the base
        ''' class the opportunity to finalize. Do not provide destructors in types derived from this
        ''' class.
        ''' </summary>
        ''' <remarks> David, 10/8/2020. </remarks>
        Protected Overrides Sub Finalize()
            ' Do not re-create Dispose clean-up code here.
            ' Calling Dispose(false) is optimal for readability and maintainability.
            Me.Dispose(False)
        End Sub

#End Region

#Region " PIN NUMBER FORMAT "

        ''' <summary>
        ''' Specifies the standard pins for alpha numeric pin set.
        ''' </summary>
        Private Shared ReadOnly AlphaPins As String() = {"A", "B", "C", "D", "E", "F", "G", "H", "J", "K", "L", "M", "N", "P", "R", "S", "T", "U", "V", "W", "X", "Y", "Z",
                                                 "a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "m", "n", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z",
                                                 "AA", "BB", "CC", "DD", "EE", "FF", "GG", "HH", "JJ", "KK", "LL", "MM", "NN", "PP"}

        ''' <summary> Builds a pin name from the pin number. </summary>
        ''' <remarks> David, 10/8/2020. </remarks>
        ''' <param name="pinNumber">        . </param>
        ''' <param name="useNumericFormat"> True to use a numeric format for building the pin number. </param>
        ''' <returns> A String. </returns>
        Public Shared Function BuildPinNumber(ByVal pinNumber As Integer, ByVal useNumericFormat As Boolean) As String

            Return If(useNumericFormat, pinNumber.ToString("0", Globalization.CultureInfo.CurrentCulture), AlphaPins(pinNumber - 1))

        End Function

        ''' <summary> Gets the shell pin number. </summary>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <value> The shell pin number. </value>
        Public Property ShellPinNumber() As String

#End Region

#Region " METHODS "

        ''' <summary>
        ''' Adds a new <see cref="Pin">Pin</see> to the <see cref="Connector">Connector</see>
        ''' or returns a reference to an existing pin.
        ''' </summary>
        ''' <remarks> David, 10/8/2020. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="pinNumber"> . </param>
        ''' <returns> A Pin. </returns>
        Public Function AddPin(ByVal pinNumber As String) As Pin

            If String.IsNullOrWhiteSpace(pinNumber) Then Throw New ArgumentNullException(NameOf(pinNumber))

            If Me.Exists(pinNumber) Then
                Return Me.SelectPin(pinNumber)
            Else
                Dim newPin As Pin = Nothing
                Try
                    newPin = New Pin With {
                        .Name = pinNumber,
                        .Number = pinNumber,
                        .ConnectorNumber = Me.Number,
                        .PartValue = Me.Name,
                        .MatingSide = Me.MatingSide,
                        .MatingPinNumber = Me.FirstMatingPinNumber + Me._Pins.Count
                    }
                    Me._Pins.Add(pinNumber, newPin)
                Catch
                    If newPin IsNot Nothing Then newPin.Dispose()
                    Throw
                End Try
                Return newPin
            End If

        End Function

        ''' <summary> Returns true of the pin already exists. </summary>
        ''' <remarks> David, 10/8/2020. </remarks>
        ''' <param name="pin"> . </param>
        ''' <returns> True if it succeeds, false if it fails. </returns>
        Public Function Exists(ByVal pin As Pin) As Boolean

            Return pin IsNot Nothing AndAlso Me.Exists(pin.Number)

        End Function

        ''' <summary> Returns true of the pin already exists. </summary>
        ''' <remarks> David, 10/8/2020. </remarks>
        ''' <param name="pinNumber"> . </param>
        ''' <returns> True if it succeeds, false if it fails. </returns>
        Public Function Exists(ByVal pinNumber As String) As Boolean

            Return Me._Pins IsNot Nothing AndAlso Me._Pins.ContainsKey(pinNumber)

        End Function

        ''' <summary> Gets the condition telling if the connector has pins. </summary>
        ''' <value> The has pins. </value>
        Public ReadOnly Property HasPins() As Boolean
            Get
                Return Me._Pins IsNot Nothing AndAlso Me._Pins.Any
            End Get
        End Property

        ''' <summary> Parses the connector record. </summary>
        ''' <remarks>
        ''' The connector record includes the following information:<para>
        ''' 0         1         2         3         4 01234567890123456789012345678901234567890123456789
        ''' J236                          'J236'    CONNECTOR J236 DB9-1               P1 CONNECTOR DB9-1
        ''' Name (0, 29)</para><para>
        ''' Number (30, 39)</para><para>
        ''' Part Value (40, EOL)</para><para>
        ''' </para>
        ''' </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="record"> A record from the net list file. </param>
        ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c>. </returns>
        Public Function Parse(ByVal record As String) As Boolean

            If String.IsNullOrWhiteSpace(record) Then Throw New ArgumentNullException(NameOf(record))

            Me.Name = record.Substring(0, 30).Trim
            Me.Number = record.Substring(30, 10).Trim

            Return Not (String.IsNullOrWhiteSpace(Me.Number) OrElse String.IsNullOrWhiteSpace(Me.Name))

        End Function

        ''' <summary> Returns a string array of pin keys. </summary>
        ''' <remarks> David, 10/8/2020. </remarks>
        ''' <returns> A String() </returns>
        Public Function PinKeys() As String()
            If Me.HasPins Then
                Dim keys(Me._Pins.Count - 1) As String
                For i As Integer = 0 To Me._Pins.Count - 1
                    keys(i) = Me._Pins.Values(i).Key
                Next
                Return keys
            Else
                Return Array.Empty(Of String)()
            End If
        End Function

        ''' <summary>
        ''' Adds pins to the connector based on the
        ''' <see cref="PinCount">Pin Count</see> and
        ''' <see cref="IsNumericPinNumbering">pin numbering format</see>.
        ''' </summary>
        ''' <remarks> David, 10/8/2020. </remarks>
        ''' <returns> True if it succeeds, false if it fails. </returns>
        Public Function Populate() As Boolean

            For i As Integer = 1 To Me.PinCount
                Me.AddPin(BuildPinNumber(i, Me.IsNumericPinNumbering))
            Next
            If Me.HasShellPin Then
                Me.AddPin(Me.ShellPinNumber)
            End If
            Return True

        End Function

        ''' <summary> Rolls back cable assembly by clearing all the public keys. </summary>
        ''' <remarks> David, 10/8/2020. </remarks>
        Public Sub Rollback()
            Me.PublicKey = 0
            For Each pin As Pin In Me._Pins.Values
                pin.PublicKey = 0
            Next
        End Sub

        ''' <summary> Selects a pin from the pins collection. </summary>
        ''' <remarks> David, 10/8/2020. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="pinNumber"> . </param>
        ''' <returns> A Pin. </returns>
        Public Function SelectPin(ByVal pinNumber As String) As Pin

            If String.IsNullOrWhiteSpace(pinNumber) Then Throw New ArgumentNullException(NameOf(pinNumber))
            Dim newPin As Pin = Me._Pins.Item(pinNumber)
            Return newPin

        End Function

#End Region

#Region " PROPERTIES "

        ''' <summary> Gets the connector name. </summary>
        ''' <value> The name. </value>
        Public Property Name() As String

        ''' <summary> Gets the connector number. </summary>
        ''' <value> The number. </value>
        Public Property Number() As String

        ''' <summary> Gets the public key reference from the database. </summary>
        ''' <value> The public key. </value>
        Public Property PublicKey() As Integer

        ''' <summary> The pins. </summary>
        Private _Pins As Collections.Generic.Dictionary(Of String, Pin)

        ''' <summary> Gets the collection of <see cref="Pin">Pins</see>. </summary>
        ''' <value> The pins. </value>
        Public ReadOnly Property Pins() As System.Collections.ObjectModel.ReadOnlyCollection(Of Pin)
            Get
                Return New System.Collections.ObjectModel.ReadOnlyCollection(Of Pin)(CType(Me._Pins.Values.ToList,
                                                                                     Global.System.Collections.Generic.IList(Of Global.isr.IO.NetList.Pin)))
            End Get
        End Property

        ''' <summary> Gets the number of pins in the connector. </summary>
        ''' <value> The number of pins. </value>
        Public Property PinCount() As Integer

        ''' <summary> Gets the pin numbering format. </summary>
        ''' <value> The is numeric pin numbering. </value>
        Public Property IsNumericPinNumbering() As Boolean = True

#End Region

#Region " CONNECTOR DEFINITIONS "

        ''' <summary> Gets the shall pin attribute. </summary>
        ''' <value> The has shell pin. </value>
        Public Property HasShellPin() As Boolean = True

        ''' <summary> Gets the mating side for this connector. </summary>
        ''' <value> The mating side. </value>
        Public Property MatingSide() As String

        ''' <summary>
        ''' Gets the first pin on the mating side to use for connecting the first pin of this connector.
        ''' One based.
        ''' </summary>
        ''' <value> The first mating pin number. </value>
        Public Property FirstMatingPinNumber() As Integer = 1

        ''' <summary> The non creatable reason. </summary>
        Private _NonCreatableReason As String

        ''' <summary> Gets the reason why this connector is not creatable. </summary>
        ''' <value> The non creatable reason. </value>
        Public ReadOnly Property NonCreatableReason() As String
            Get
                Return Me._NonCreatableReason
            End Get
        End Property

        ''' <summary> Returns <c>True</c> if the connector is ready to populate pins. </summary>
        ''' <value> The creatable. </value>
        Public ReadOnly Property Creatable() As Boolean
            Get
                Me._NonCreatableReason = String.Empty
                If Me.PinCount <= 0 Then
                    Me._NonCreatableReason = String.Format(Globalization.CultureInfo.CurrentCulture,
                                                        "Pin count '={0}' must be greater than 0", Me.PinCount)
                    Return False
                ElseIf Me.FirstMatingPinNumber <= 0 Then
                    Me._NonCreatableReason = String.Format(Globalization.CultureInfo.CurrentCulture,
                                                        "First mating pin number '={0}' must be greater than 0", Me.FirstMatingPinNumber)
                    Return False
                ElseIf Not (Me.MatingSide = "P1" OrElse Me.MatingSide = "P2") Then
                    Me._NonCreatableReason = String.Format(Globalization.CultureInfo.CurrentCulture,
                                                        "Mating side '={0}' must be either P1 or P2", Me.MatingSide)
                End If
                Return True
            End Get
        End Property

#End Region

    End Class

End Namespace

