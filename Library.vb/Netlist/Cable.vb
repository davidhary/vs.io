Namespace NetList

    ''' <summary>
    ''' This is the <see cref="isr.IO.NetList.Cable">Cable</see> class of the net list reader class
    ''' library. A cable consists of a set of connector and nodes defining how the connector pins are
    ''' wired.
    ''' </summary>
    ''' <remarks>
    ''' (c) 2007 Integrated Scientific Resources<para>
    ''' Licensed under The MIT License.</para><para>
    ''' David, 12/18/07, 1.2.2908. </para>
    ''' </remarks>
    Public Class Cable

        Implements IDisposable

#Region " CONSTRUCTION and CLEANUP "

        ''' <summary> Constructs this class. </summary>
        ''' <remarks> David, 10/8/2020. </remarks>
        Public Sub New()

            ' instantiate the class
            MyBase.New()
            Me._Nodes = New Collections.Generic.Dictionary(Of Integer, Node)
            Me._Connectors = New Collections.Generic.Dictionary(Of String, Connector)

        End Sub

        ''' <summary> Calls <see cref="M:Dispose(Boolean Disposing)"/> to cleanup. </summary>
        ''' <remarks>
        ''' Do not make this method Overridable (virtual) because a derived class should not be able to
        ''' override this method.
        ''' </remarks>
        Public Sub Dispose() Implements IDisposable.Dispose

            ' Do not change this code.  Put cleanup code in Dispose(Boolean) below.

            ' this disposes all child classes.
            Me.Dispose(True)

            ' Take this object off the finalization(Queue) and prevent finalization code 
            ' from executing a second time.
            GC.SuppressFinalize(Me)

        End Sub

        ''' <summary> True if is disposed, false if not. </summary>
        Private _IsDisposed As Boolean

        ''' <summary>
        ''' Gets or sets the dispose status sentinel of the base class.  This applies to the derived
        ''' class provided proper implementation.
        ''' </summary>
        ''' <value> The is disposed. </value>
        Protected Property IsDisposed() As Boolean
            Get
                Return Me._IsDisposed
            End Get
            Private Set(ByVal value As Boolean)
                Me._IsDisposed = value
            End Set
        End Property

        ''' <summary>
        ''' Releases the unmanaged resources used by the <see cref="T:System.Windows.Forms.Control" />
        ''' and its child controls and optionally releases the managed resources.
        ''' </summary>
        ''' <remarks> David, 10/8/2020. </remarks>
        ''' <param name="disposing"> <c>True</c> to release both managed and unmanaged resources;
        '''                                                   <c>False</c> to release only unmanaged
        '''                                                   resources when called from the runtime
        '''                                                   finalize. </param>
        <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
        <System.Diagnostics.DebuggerNonUserCode()>
        Protected Overridable Sub Dispose(ByVal disposing As Boolean)
            Try
                If Not Me.IsDisposed AndAlso disposing Then
                    ' Free managed resources when explicitly called
                    Me._Connectors = Nothing
                    Me._Nodes = Nothing
                    Me._Wires = Nothing
                End If
            Finally
                Me.IsDisposed = True
            End Try
        End Sub

        ''' <summary>
        ''' This destructor will run only if the Dispose method does not get called. It gives the base
        ''' class the opportunity to finalize. Do not provide destructors in types derived from this
        ''' class.
        ''' </summary>
        ''' <remarks> David, 10/8/2020. </remarks>
        Protected Overrides Sub Finalize()
            ' Do not re-create Dispose clean-up code here.
            ' Calling Dispose(false) is optimal for readability and maintainability.
            Me.Dispose(False)
        End Sub

#End Region

#Region " METHODS "

        ''' <summary>
        ''' Adds a <see cref="Connector">connector</see> to the <see cref="Cable">cable</see>.
        ''' </summary>
        ''' <remarks> David, 10/8/2020. </remarks>
        ''' <param name="record"> A record from the net list file. </param>
        ''' <returns> A Connector. </returns>
        Public Function AddConnector(ByVal record As String) As Connector

            Dim newConnector As Connector = Nothing
            Try
                newConnector = New Connector
                If newConnector.Parse(record) Then
                    If Not Me._Connectors.ContainsKey(newConnector.Number) Then
                        Me._Connectors.Add(newConnector.Number, newConnector)
                    End If
                End If
            Catch
                If newConnector IsNot Nothing Then newConnector.Dispose()
                Throw
            End Try
            Return newConnector

        End Function

        ''' <summary> Adds a <see cref="Node">Node</see> to the <see cref="Cable">cable</see>. </summary>
        ''' <remarks> David, 10/8/2020. </remarks>
        ''' <param name="record"> A record from the net list file. </param>
        ''' <returns> A Node. </returns>
        Public Function AddNode(ByVal record As String) As Node

            Dim newNode As Node = Nothing
            Try
                newNode = New NetList.Node
                If newNode.Parse(record) Then
                    Me._Nodes.Add(newNode.Number, newNode)
                End If
#Disable Warning CA1031 ' Do not catch general exception types
            Catch
                If newNode IsNot Nothing Then newNode.Dispose()
#Enable Warning CA1031 ' Do not catch general exception types
            End Try
            Return newNode

        End Function

        ''' <summary> Adds <see cref="Wire">Wires</see> to the <see cref="Cable">cable</see>. </summary>
        ''' <remarks> David, 10/8/2020. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="node">                  A record from the net list file. </param>
        ''' <param name="includeRedundantWires"> True to add also redundant wires. </param>
        ''' <returns> True if it succeeds, false if it fails. </returns>
        Public Function AddWires(ByVal node As Node, ByVal includeRedundantWires As Boolean) As Boolean

            If node Is Nothing Then Throw New ArgumentNullException(NameOf(node))

            If node.Pins.Count > 1 Then
                For i As Integer = 1 To node.Pins.Count - 1
                    Dim lastPin As Integer = i
                    If includeRedundantWires Then
                        lastPin = node.Pins.Count - 1
                    End If
                    For j As Integer = i To lastPin
                        Dim newWire As Wire = Nothing
                        Try
                            newWire = New Wire(node.Pins(i - 1), node.Pins(j)) With {
                                .IsPrimary = (i = j)
                            }
                            Debug.Assert(Not Me.Exists(newWire), "Wire exists")
                            If Not Me.Exists(newWire) Then
                                Me._Wires.Add(newWire.Key, newWire)
                            End If
                        Catch
                            If newWire IsNot Nothing Then newWire.Dispose()
                            Throw
                        End Try
                    Next
                Next
            End If
            Return True

        End Function

        ''' <summary>
        ''' Adds a <see cref="Pin">Pin</see> to the <see cref="Connector">Connector</see>.
        ''' </summary>
        ''' <remarks> David, 10/8/2020. </remarks>
        ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
        '''                                              null. </exception>
        ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
        ''' <param name="pinNumber">       The pin number. </param>
        ''' <param name="connectorNumber"> . </param>
        ''' <returns> A new or exiting pin. </returns>
        Public Function AddPin(ByVal pinNumber As String, ByVal connectorNumber As String) As Pin

            If String.IsNullOrWhiteSpace(pinNumber) Then Throw New ArgumentNullException(NameOf(pinNumber))
            If String.IsNullOrWhiteSpace(connectorNumber) Then Throw New ArgumentNullException(NameOf(connectorNumber))

            If Not Me.ConnectorExists(connectorNumber) Then
                Dim message As String = "Failed adding pin '{0}'. Connector number '{1}' not found"
                message = String.Format(Globalization.CultureInfo.CurrentCulture, message, pinNumber, connectorNumber)
                Throw New InvalidOperationException(message)
            End If

            ' add the pin to the connector.
            Return Me.SelectConnector(connectorNumber).AddPin(pinNumber)

        End Function

        ''' <summary> Returns true of the connector already exists. </summary>
        ''' <remarks> David, 10/8/2020. </remarks>
        ''' <param name="connectorNumber"> . </param>
        ''' <returns> True if it succeeds, false if it fails. </returns>
        Public Function ConnectorExists(ByVal connectorNumber As String) As Boolean

            Return Me._Connectors IsNot Nothing AndAlso Me._Connectors.ContainsKey(connectorNumber)

        End Function

        ''' <summary> Returns true of the connector already exists. </summary>
        ''' <remarks> David, 10/8/2020. </remarks>
        ''' <param name="pin"> . </param>
        ''' <returns> True if it succeeds, false if it fails. </returns>
        Public Function ConnectorExists(ByVal pin As Pin) As Boolean

            Return pin IsNot Nothing AndAlso Me._Connectors IsNot Nothing AndAlso Me._Connectors.ContainsKey(pin.ConnectorNumber)

        End Function

        ''' <summary> Returns true of the connector already exists. </summary>
        ''' <remarks> David, 10/8/2020. </remarks>
        ''' <param name="connector"> . </param>
        ''' <returns> True if it succeeds, false if it fails. </returns>
        Public Function Exists(ByVal connector As Connector) As Boolean

            Return connector IsNot Nothing AndAlso Me.ConnectorExists(connector.Number)

        End Function

        ''' <summary> Returns true of the node already exists. </summary>
        ''' <remarks> David, 10/8/2020. </remarks>
        ''' <param name="node"> . </param>
        ''' <returns> True if it succeeds, false if it fails. </returns>
        Public Function Exists(ByVal node As Node) As Boolean

            Return node IsNot Nothing AndAlso Me.NodeExists(node.Number)

        End Function

        ''' <summary> Returns true of the node already exists. </summary>
        ''' <remarks> David, 10/8/2020. </remarks>
        ''' <param name="nodeNumber"> . </param>
        ''' <returns> True if it succeeds, false if it fails. </returns>
        Public Function NodeExists(ByVal nodeNumber As Integer) As Boolean

            Return Me._Nodes IsNot Nothing AndAlso Me._Nodes.ContainsKey(nodeNumber)

        End Function

        ''' <summary> Returns true of the wire already exists. </summary>
        ''' <remarks> David, 10/8/2020. </remarks>
        ''' <param name="wire"> . </param>
        ''' <returns> True if it succeeds, false if it fails. </returns>
        Public Function Exists(ByVal wire As Wire) As Boolean

            Return wire IsNot Nothing AndAlso Me.WireExists(wire.Key)

        End Function

        ''' <summary> Returns the list of pin keys defined for this cable. </summary>
        ''' <remarks> David, 10/8/2020. </remarks>
        ''' <returns> A String() </returns>
        Public Function PinKeys() As String()
            If Me.HasPins Then
                Dim pinArray As String() = Array.Empty(Of String)()
                For Each connector As Connector In Me._Connectors.Values
                    Dim keys As String() = connector.PinKeys
                    Dim currentLength As Integer = pinArray.Length
                    Array.Resize(pinArray, pinArray.Length + keys.Length)
                    keys.CopyTo(pinArray, currentLength)
                Next
                Return pinArray
            Else
                Return Array.Empty(Of String)()
            End If
        End Function

        ''' <summary> The active node. </summary>
        Private _ActiveNode As Node

        ''' <summary> Parses each record based on the reading state. </summary>
        ''' <remarks>
        ''' The sequence of reading the net list proceeds as follows:<para>
        ''' 1.  Look for the connector list.  This is signified by the &lt;&lt;&lt; Component List &gt;
        ''' &gt;&gt;</para><para>
        ''' 2. Read the connectors.</para><para>
        ''' 3. Look for wire list signified by &lt;&lt;&lt; Wire List &gt;&gt;&gt;</para><para>
        ''' 4. Repeat looking for nodes.  Each nodes starts with a [</para><para>
        ''' 5. Fore each node read all the wires adding pins as necessary.</para><para>
        ''' </para>
        ''' </remarks>
        ''' <param name="record">       . </param>
        ''' <param name="readingState"> State of the reading. </param>
        ''' <returns> A ReadingState. </returns>
        Private Function Parse(ByVal record As String, ByVal readingState As ReadingState) As ReadingState

            If readingState = NetList.ReadingState.Done Then

                ' if done do nothing.

            ElseIf readingState = NetList.ReadingState.Failed Then

                ' if failed do nothing.

            ElseIf readingState = NetList.ReadingState.LookingForConnectorList Then

                ' look for the connector header
                If record.IndexOf(_ComponentListHeader, StringComparison.OrdinalIgnoreCase) >= 0 Then
                    readingState = ReadingState.ReadingConnectors
                End If

            ElseIf readingState = NetList.ReadingState.LookingForNode Then

                ' look for the node number prefix
                If record.IndexOf(_NodeNumberPrfix, StringComparison.OrdinalIgnoreCase) >= 0 Then
                    readingState = ReadingState.ReadingNode
                    Me._ActiveNode = Me.AddNode(record)
                End If

            ElseIf readingState = NetList.ReadingState.LookingForWireList Then

                ' look for the wire list header
                If record.IndexOf(_WireListHeader, StringComparison.OrdinalIgnoreCase) >= 0 Then
                    readingState = ReadingState.LookingForNode
                End If

            ElseIf readingState = NetList.ReadingState.None Then

                Return Me.Parse(record, ReadingState.LookingForConnectorList)

            ElseIf readingState = NetList.ReadingState.ReadingConnectors Then

                If record.Length > 2 Then

                    ' add a connector
                    Me.AddConnector(record)

                Else

                    ' if done reading connectors then look for wire list.
                    readingState = ReadingState.LookingForWireList

                End If

            ElseIf readingState = NetList.ReadingState.ReadingNode Then

                If record.Length > 2 Then

                    ' add a pin
                    Me._ActiveNode.AddPin(Me, record)

                Else

                    ' if done reading node look for the next node
                    readingState = ReadingState.LookingForNode

                End If

            Else

                Debug.Assert(Not Debugger.IsAttached, "Unhandled reading state")

            End If

            Return readingState

        End Function

        ''' <summary> Add pins to all connectors based on the number of pins specified. </summary>
        ''' <remarks> David, 10/8/2020. </remarks>
        ''' <returns> True if it succeeds, false if it fails. </returns>
        Public Function PopulateConnectors() As Boolean

            For Each existingConnector As Connector In Me._Connectors.Values
                existingConnector.Populate()
            Next
            Return True

        End Function

        ''' <summary> Creates the collection of wires. </summary>
        ''' <remarks> David, 10/8/2020. </remarks>
        ''' <param name="includeRedundantWires"> True to add also redundant wires. </param>
        ''' <returns> True if it succeeds, false if it fails. </returns>
        Public Function PopulateWires(ByVal includeRedundantWires As Boolean) As Boolean

            Me._Wires = New Collections.Generic.Dictionary(Of String, Wire)
            For Each newNode As Node In Me._Nodes.Values
                Me.AddWires(newNode, includeRedundantWires)
            Next
            Return True

        End Function

        ''' <summary> Reads the net list connectors from the file. </summary>
        ''' <remarks> David, 10/8/2020. </remarks>
        ''' <param name="filePathName"> . </param>
        ''' <returns> True if it succeeds, false if it fails. </returns>
        Public Function ReadNetListConnectors(ByVal filePathName As String) As Boolean

            Dim success As Boolean = True

            If System.IO.File.Exists(filePathName) Then

                Dim fileContents As String
                fileContents = My.Computer.FileSystem.ReadAllText(filePathName)
                If String.IsNullOrWhiteSpace(fileContents) Then
                    success = False
                Else
                    Dim rows As String() = fileContents.Split(CChar(Environment.NewLine))
                    If rows.Length < 3 Then
                        success = False
                    Else
                        Dim readingState As ReadingState = ReadingState.None
                        For Each record As String In rows
                            record = record.Trim(Environment.NewLine.ToCharArray).Trim
                            readingState = Me.Parse(record, readingState)
                            ' stop reading the first time we are looking for a node of wires
                            If readingState = NetList.ReadingState.Failed OrElse readingState = NetList.ReadingState.LookingForNode Then
                                Exit For
                            End If
                        Next
                        If readingState <> ReadingState.Failed Then
                            readingState = ReadingState.Done
                        End If
                        Return readingState = NetList.ReadingState.Done AndAlso Me._Connectors.Any
                    End If
                End If
            Else
                success = False
            End If

            Return success

        End Function

        ''' <summary> Reads a net list from the file. </summary>
        ''' <remarks> David, 10/8/2020. </remarks>
        ''' <param name="filePathName"> . </param>
        ''' <returns> True if it succeeds, false if it fails. </returns>
        Public Function ReadNetList(ByVal filePathName As String) As Boolean

            Dim success As Boolean = True

            If System.IO.File.Exists(filePathName) Then

                Dim fileContents As String
                fileContents = My.Computer.FileSystem.ReadAllText(filePathName)
                If String.IsNullOrWhiteSpace(fileContents) Then
                    success = False
                Else
                    Dim rows As String() = fileContents.Split(CChar(Environment.NewLine))
                    If rows.Length < 3 Then
                        success = False
                    Else
                        Dim readingState As ReadingState = ReadingState.None
                        For Each record As String In rows
                            record = record.Trim(Environment.NewLine.ToCharArray).Trim
                            readingState = Me.Parse(record, readingState)
                            If readingState = NetList.ReadingState.Failed Then
                                Exit For
                            End If
                        Next
                        If readingState <> ReadingState.Failed Then
                            readingState = ReadingState.Done
                        End If
                        Return readingState = NetList.ReadingState.Done AndAlso Me._Connectors.Any AndAlso Me._Nodes.Any
                    End If
                End If
            Else
                success = False
            End If

            Return success

        End Function

        ''' <summary> Rolls back cable assembly by clearing all the public keys. </summary>
        ''' <remarks> David, 10/8/2020. </remarks>
        Public Sub Rollback()
            Me.PublicKey = 0
            For Each connector As Connector In Me._Connectors.Values
                connector.Rollback()
            Next
            For Each node As Node In Me._Nodes.Values
                node.Rollback()
            Next
        End Sub

        ''' <summary> Selects a Connector from the Connectors collection. </summary>
        ''' <remarks> David, 10/8/2020. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="connectorNumber"> . </param>
        ''' <returns> A Connector. </returns>
        Public Function SelectConnector(ByVal connectorNumber As String) As Connector

            If String.IsNullOrWhiteSpace(connectorNumber) Then Throw New ArgumentNullException(NameOf(connectorNumber))
            Dim newConnector As Connector = Me._Connectors.Item(connectorNumber)
            Return newConnector

        End Function

        ''' <summary> Selects a Node from the Nodes collection. </summary>
        ''' <remarks> David, 10/8/2020. </remarks>
        ''' <param name="nodeNumber"> . </param>
        ''' <returns> A Node. </returns>
        Public Function SelectNode(ByVal nodeNumber As Integer) As Node

            Dim newNode As Node = Me._Nodes.Item(nodeNumber)
            Return newNode

        End Function

        ''' <summary> Returns true of the wire already exists. </summary>
        ''' <remarks> David, 10/8/2020. </remarks>
        ''' <param name="wireId"> . </param>
        ''' <returns> True if it succeeds, false if it fails. </returns>
        Public Function WireExists(ByVal wireId As String) As Boolean

            Return Me._Wires IsNot Nothing AndAlso Me._Wires.ContainsKey(wireId)

        End Function

        ''' <summary> Returns the list of wires as keys.  This can be displayed in a list box. </summary>
        ''' <remarks> David, 10/8/2020. </remarks>
        ''' <returns> A String() </returns>
        Public Function WireKeys() As String()
            Return If(Me._Wires Is Nothing, Array.Empty(Of String)(), Me._Wires.Keys.ToArray())
        End Function

#End Region

#Region " PROPERTIES "

        ''' <summary>
        ''' Gets the connector list header string.
        ''' </summary>
        Private Const _ComponentListHeader As String = "<<< Component List >>>"

        ''' <summary>
        ''' Gets the wire list header string.
        ''' </summary>
        Private Const _WireListHeader As String = "<<< Wire List >>>"

        ''' <summary>
        ''' Gets the first character for a new node record.
        ''' </summary>
        Private Const _NodeNumberPrfix As String = "["

        ''' <summary> Gets the cable name. </summary>
        ''' <value> The name. </value>
        Public Property Name() As String

        ''' <summary> Gets the cable number. </summary>
        ''' <value> The number. </value>
        Public Property Number() As String

        ''' <summary> Gets the public key reference from the database. </summary>
        ''' <value> The public key. </value>
        Public Property PublicKey() As Integer

        ''' <summary> The connectors. </summary>
        Private _Connectors As Collections.Generic.Dictionary(Of String, Connector)

        ''' <summary> Gets the collection of <see cref="Connector">Connectors</see>. </summary>
        ''' <value> The connectors. </value>
        Public ReadOnly Property Connectors() As System.Collections.ObjectModel.ReadOnlyCollection(Of Connector)
            Get
                Return New Collections.ObjectModel.ReadOnlyCollection(Of Connector)(CType(Me._Connectors.Values.ToList,
                                                                                    Global.System.Collections.Generic.IList(Of Global.isr.IO.NetList.Connector)))
            End Get
        End Property

        ''' <summary> Gets the condition telling if the cable has connectors. </summary>
        ''' <value> The has connectors. </value>
        Public ReadOnly Property HasConnectors() As Boolean
            Get
                Return Me._Connectors IsNot Nothing AndAlso Me._Connectors.Any
            End Get
        End Property

        ''' <summary> Gets the condition telling if the cable has pins. </summary>
        ''' <value> The has pins. </value>
        Public ReadOnly Property HasPins() As Boolean
            Get
                Return Me.HasConnectors AndAlso Me._Connectors.Values.First.HasPins
            End Get
        End Property

        ''' <summary> Gets the condition telling if the cable has wires. </summary>
        ''' <value> The has wires. </value>
        Public ReadOnly Property HasWires() As Boolean
            Get
                Return Me._Wires IsNot Nothing AndAlso Me._Wires.Any
            End Get
        End Property

        ''' <summary> The nodes. </summary>
        Private _Nodes As Collections.Generic.Dictionary(Of Integer, Node)

        ''' <summary> Gets the collection of <see cref="Node">Nodes</see>. </summary>
        ''' <value> The nodes. </value>
        Public ReadOnly Property Nodes() As System.Collections.ObjectModel.ReadOnlyCollection(Of Node)
            Get
                Return New Collections.ObjectModel.ReadOnlyCollection(Of Node)(CType(Me._Nodes.Values.ToList,
                                                                               Global.System.Collections.Generic.IList(Of Global.isr.IO.NetList.Node)))
            End Get
        End Property

        ''' <summary> Gets the collection of <see cref="Pin">pins</see>. </summary>
        ''' <value> The pins. </value>
        Public ReadOnly Property Pins() As System.Collections.ObjectModel.ReadOnlyCollection(Of Pin)
            Get
                Dim pinsDictionary As New Collections.Generic.Dictionary(Of String, Pin)
                For Each Connector As Connector In Me._Connectors.Values
                    For Each Pin As Pin In Connector.Pins
                        pinsDictionary.Add(Pin.Key, Pin)
                    Next
                Next
                Return New Collections.ObjectModel.ReadOnlyCollection(Of Pin)(CType(pinsDictionary.Values.ToList,
                                                                              Global.System.Collections.Generic.IList(Of Global.isr.IO.NetList.Pin)))
            End Get
        End Property

        ''' <summary> The wires. </summary>
        Private _Wires As Collections.Generic.Dictionary(Of String, Wire)

        ''' <summary> Gets the collection of <see cref="Wire">Wires</see>. </summary>
        ''' <value> The wires. </value>
        Public ReadOnly Property Wires() As System.Collections.ObjectModel.ReadOnlyCollection(Of Wire)
            Get
                Return New Collections.ObjectModel.ReadOnlyCollection(Of Wire)(CType(Me._Wires.Values.ToList,
                                                                               Global.System.Collections.Generic.IList(Of Global.isr.IO.NetList.Wire)))
            End Get
        End Property

#End Region

#Region " CREATABLE "

        ''' <summary> The non creatable reason. </summary>
        Private _NonCreatableReason As String

        ''' <summary> Gets the reason why this connector is not creatable. </summary>
        ''' <value> The non creatable reason. </value>
        Public ReadOnly Property NonCreatableReason() As String
            Get
                Return Me._NonCreatableReason
            End Get
        End Property

        ''' <summary> Returns <c>True</c> if the connector is ready to populate pins. </summary>
        ''' <value> The creatable. </value>
        Public ReadOnly Property Creatable() As Boolean
            Get
                Me._NonCreatableReason = String.Empty
                For Each connector As isr.IO.NetList.Connector In Me.Connectors
                    If Not connector.Creatable Then
                        Me._NonCreatableReason = String.Format(Globalization.CultureInfo.CurrentCulture,
                                                            "Connector number {0} is not creatable because {1}",
                                                            connector.Number, connector.NonCreatableReason)
                        Return False
                    End If
                Next
                Return True
            End Get
        End Property

#End Region

    End Class

End Namespace

