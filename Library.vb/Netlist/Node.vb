Namespace NetList

    ''' <summary>
    ''' This is the <see cref="isr.IO.NetList.Node">Node</see> class of the net list reader class
    ''' library. A node consists of a collection of connected pins.
    ''' </summary>
    ''' <remarks>
    ''' (c) 2007 Integrated Scientific Resources, Inc. All rights reserved. <para>
    ''' Licensed under The MIT License.</para><para>
    ''' David, 12/18/07, 1.2.2908.x. </para>
    ''' </remarks>
    Public Class Node

        Implements IDisposable

#Region " CONSTRUCTION and CLEANUP "

        ''' <summary> Constructs this class. </summary>
        ''' <remarks> David, 10/8/2020. </remarks>
        Public Sub New()

            ' instantiate the class
            MyBase.New()
            Me._Pins = New Collections.Generic.Dictionary(Of String, Pin)

        End Sub

        ''' <summary> Calls <see cref="M:Dispose(Boolean Disposing)"/> to cleanup. </summary>
        ''' <remarks>
        ''' Do not make this method Overridable (virtual) because a derived class should not be able to
        ''' override this method.
        ''' </remarks>
        Public Sub Dispose() Implements IDisposable.Dispose

            ' Do not change this code.  Put cleanup code in Dispose(Boolean) below.

            ' this disposes all child classes.
            Me.Dispose(True)

            ' Take this object off the finalization(Queue) and prevent finalization code 
            ' from executing a second time.
            GC.SuppressFinalize(Me)

        End Sub

        ''' <summary> True if is disposed, false if not. </summary>
        Private _IsDisposed As Boolean

        ''' <summary>
        ''' Gets or sets the dispose status sentinel of the base class.  This applies to the derived
        ''' class provided proper implementation.
        ''' </summary>
        ''' <value> The is disposed. </value>
        Protected Property IsDisposed() As Boolean
            Get
                Return Me._IsDisposed
            End Get
            Private Set(ByVal value As Boolean)
                Me._IsDisposed = value
            End Set
        End Property

        ''' <summary>
        ''' Releases the unmanaged resources used by the <see cref="T:System.Windows.Forms.Control" />
        ''' and its child controls and optionally releases the managed resources.
        ''' </summary>
        ''' <remarks> David, 10/8/2020. </remarks>
        ''' <param name="disposing"> <c>True</c> to release both managed and unmanaged resources;
        '''                                                   <c>False</c> to release only unmanaged
        '''                                                   resources when called from the runtime
        '''                                                   finalize. </param>
        <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
        <System.Diagnostics.DebuggerNonUserCode()>
        Protected Overridable Sub Dispose(ByVal disposing As Boolean)
            Try
                If Not Me.IsDisposed AndAlso disposing Then
                    Me._Pins = Nothing
                End If
            Finally
                Me.IsDisposed = True
            End Try
        End Sub

        ''' <summary>
        ''' This destructor will run only if the Dispose method does not get called. It gives the base
        ''' class the opportunity to finalize. Do not provide destructors in types derived from this
        ''' class.
        ''' </summary>
        ''' <remarks> David, 10/8/2020. </remarks>
        Protected Overrides Sub Finalize()
            ' Do not re-create Dispose clean-up code here.
            ' Calling Dispose(false) is optimal for readability and maintainability.
            Me.Dispose(False)
        End Sub

#End Region

#Region " METHODS "

        ''' <summary>
        ''' Adds a new <see cref="Pin">Pin</see> to the <see cref="Node">Node</see>
        ''' or selects an existing pin.
        ''' </summary>
        ''' <remarks> David, 10/8/2020. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="cable">  . </param>
        ''' <param name="record"> . </param>
        ''' <returns> A reference to the pin. </returns>
        Public Function AddPin(ByVal cable As Cable, ByVal record As String) As Pin

            If cable Is Nothing Then Throw New ArgumentNullException(NameOf(cable))

            If String.IsNullOrWhiteSpace(record) Then Throw New ArgumentNullException(NameOf(record))
            Dim newPin As Pin = Nothing
            Try
                newPin = New Pin
                If newPin.Parse(record) Then

                    ' add the pin to the cable
                    newPin = cable.AddPin(newPin.Number, newPin.ConnectorNumber)

                    ' set reference to the node number.
                    newPin.NodeNumber = Me.Number

                    ' add the pin to the node
                    If Not Me.Exists(newPin) Then
                        Me._Pins.Add(newPin.Key, newPin)
                    End If
                    newPin = Me._Pins.Item(newPin.Key)
                End If
            Catch
                If newPin IsNot Nothing Then newPin.Dispose()
                Throw
            End Try
            Return newPin

        End Function

        ''' <summary> Returns true of the pin already exists. </summary>
        ''' <remarks> David, 10/8/2020. </remarks>
        ''' <param name="pin"> . </param>
        ''' <returns> True if it succeeds, false if it fails. </returns>
        Public Function Exists(ByVal pin As Pin) As Boolean

            Return pin IsNot Nothing AndAlso Me._Pins IsNot Nothing AndAlso Me._Pins.ContainsKey(pin.Number)

        End Function

        ''' <summary> Parses the node record. </summary>
        ''' <remarks>
        ''' The node record includes the following information:<para>
        ''' 0         1
        ''' 01234567890123
        ''' [00001] N00469 Number (1, 5)</para><para>
        ''' Name (8, EOL)</para><para>
        ''' </para>
        ''' </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="record"> A record from the net list file. </param>
        ''' <returns> True if it succeeds, false if it fails. </returns>
        Public Function Parse(ByVal record As String) As Boolean

            If String.IsNullOrWhiteSpace(record) Then Throw New ArgumentNullException(NameOf(record))

            Me.Number = Int32.Parse(record.Substring(1, 5).Trim, Globalization.CultureInfo.InvariantCulture)
            Me.Name = record.Substring(8).Trim
            Return Me.Number > 0 AndAlso Not String.IsNullOrWhiteSpace(Me.Name)

        End Function

        ''' <summary> Rolls back cable assembly by clearing all the public keys. </summary>
        ''' <remarks> David, 10/8/2020. </remarks>
        Public Sub Rollback()
            For Each pin As Pin In Me._Pins.Values
                pin.PublicKey = 0
            Next
        End Sub

        ''' <summary> Selects a pin from the pins collection. </summary>
        ''' <remarks> David, 10/8/2020. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="pinKey"> . </param>
        ''' <returns> A Pin. </returns>
        Public Function SelectPin(ByVal pinKey As String) As Pin

            If String.IsNullOrWhiteSpace(pinKey) Then Throw New ArgumentNullException(NameOf(pinKey))
            Dim newPin As Pin = Me._Pins.Item(pinKey)
            Return newPin

        End Function

#End Region

#Region " PROPERTIES "

        ''' <summary> Gets the node name. </summary>
        ''' <value> The name. </value>
        Public Property Name() As String

        ''' <summary> Gets the node number. </summary>
        ''' <value> The number. </value>
        Public Property Number() As Integer

        ''' <summary> Gets the public key reference from the database. </summary>
        ''' <value> The public key. </value>
        Public Property PublicKey() As Integer

        ''' <summary> The pins. </summary>
        Private _Pins As Collections.Generic.Dictionary(Of String, Pin)

        ''' <summary> Gets the collection of <see cref="Pin">Pins</see>. </summary>
        ''' <value> The pins. </value>
        Public ReadOnly Property Pins() As System.Collections.ObjectModel.ReadOnlyCollection(Of Pin)
            Get
                Return If(Me._Pins Is Nothing,
                    New System.Collections.ObjectModel.ReadOnlyCollection(Of Pin)(New Global.System.Collections.Generic.List(Of Global.isr.IO.NetList.Pin)),
                    New System.Collections.ObjectModel.ReadOnlyCollection(Of Pin)(CType(Me._Pins.Values.ToList,
                                                                                         Global.System.Collections.Generic.IList(Of Global.isr.IO.NetList.Pin))))
            End Get
        End Property

#End Region

    End Class

End Namespace

