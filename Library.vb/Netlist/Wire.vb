Namespace NetList

    ''' <summary>
    ''' This is the <see cref="isr.IO.NetList.Pin">Pin</see> class of the net list reader class
    ''' library.
    ''' </summary>
    ''' <remarks>
    ''' (c) 2007 Integrated Scientific Resources, Inc. All rights reserved. <para>
    ''' Licensed under The MIT License.</para><para>
    ''' David, 12/18/07, 1.2.2908.x. </para>
    ''' </remarks>
    Public Class Wire

        Implements IDisposable

#Region " CONSTRUCTION and CLEANUP "

        ''' <summary> Constructs this class. </summary>
        ''' <remarks> David, 10/8/2020. </remarks>
        ''' <param name="pinA"> The pin a. </param>
        ''' <param name="pinB"> The pin b. </param>
        Public Sub New(ByVal pinA As Pin, ByVal pinB As Pin)

            ' instantiate the class
            MyBase.New()

            Me.PinA = pinA
            Me.PinB = pinB

        End Sub

        ''' <summary> Calls <see cref="M:Dispose(Boolean Disposing)"/> to cleanup. </summary>
        ''' <remarks>
        ''' Do not make this method Overridable (virtual) because a derived class should not be able to
        ''' override this method.
        ''' </remarks>
        Public Sub Dispose() Implements IDisposable.Dispose

            ' Do not change this code.  Put cleanup code in Dispose(Boolean) below.

            ' this disposes all child classes.
            Me.Dispose(True)

            ' Take this object off the finalization(Queue) and prevent finalization code 
            ' from executing a second time.
            GC.SuppressFinalize(Me)

        End Sub

        ''' <summary> True if is disposed, false if not. </summary>
        Private _IsDisposed As Boolean

        ''' <summary>
        ''' Gets or sets the dispose status sentinel of the base class.  This applies to the derived
        ''' class provided proper implementation.
        ''' </summary>
        ''' <value> The is disposed. </value>
        Protected Property IsDisposed() As Boolean
            Get
                Return Me._IsDisposed
            End Get
            Private Set(ByVal value As Boolean)
                Me._IsDisposed = value
            End Set
        End Property

        ''' <summary>
        ''' Releases the unmanaged resources used by the <see cref="T:System.Windows.Forms.Control" />
        ''' and its child controls and optionally releases the managed resources.
        ''' </summary>
        ''' <remarks> David, 10/8/2020. </remarks>
        ''' <param name="disposing"> <c>True</c> to release both managed and unmanaged resources;
        '''                                                   <c>False</c> to release only unmanaged
        '''                                                   resources when called from the runtime
        '''                                                   finalize. </param>
        <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
        <System.Diagnostics.DebuggerNonUserCode()>
        Protected Overridable Sub Dispose(ByVal disposing As Boolean)
            Try
                If Not Me.IsDisposed AndAlso disposing Then
                End If
            Finally
                Me.IsDisposed = True
            End Try
        End Sub

        ''' <summary>
        ''' This destructor will run only if the Dispose method does not get called. It gives the base
        ''' class the opportunity to finalize. Do not provide destructors in types derived from this
        ''' class.
        ''' </summary>
        ''' <remarks> David, 10/8/2020. </remarks>
        Protected Overrides Sub Finalize()
            ' Do not re-create Dispose clean-up code here.
            ' Calling Dispose(false) is optimal for readability and maintainability.
            Me.Dispose(False)
        End Sub

#End Region

#Region " PROPERTIES "

        ''' <summary> Returns a wire ID. </summary>
        ''' <remarks> David, 10/8/2020. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="wire"> The wire. </param>
        ''' <returns> A String. </returns>
        Public Shared Function BuildKey(ByVal wire As Wire) As String
            If wire Is Nothing Then Throw New ArgumentNullException(NameOf(wire))
            Return String.Format(Globalization.CultureInfo.CurrentCulture,
                                 "{0}-{1}", wire.PinA.Key, wire.PinB.Key)
        End Function

        ''' <summary> Returns a unique Wire Key. </summary>
        ''' <value> The key. </value>
        Public ReadOnly Property Key() As String
            Get
                Return BuildKey(Me)
            End Get
        End Property

        ''' <summary> Gets or sets <see cref="Pin">pin A</see> of the wire. </summary>
        ''' <value> The pin a. </value>
        Public ReadOnly Property PinA() As Pin

        ''' <summary> Gets or sets <see cref="Pin">pin B</see> of the wire. </summary>
        ''' <value> The pin b. </value>
        Public ReadOnly Property PinB() As Pin

        ''' <summary>
        ''' Gets or sets the property indicating if the wire is a primary wire derived from connecting
        ''' sequential pins of a node or a redundant wire derived by connecting pins that are already
        ''' connected by primary wires.
        ''' </summary>
        ''' <value> The is primary. </value>
        Public Property IsPrimary() As Boolean

        ''' <summary> Gets or sets the public key reference from the database. </summary>
        ''' <value> The public key. </value>
        Public Property PublicKey() As Integer

#End Region

    End Class

End Namespace

