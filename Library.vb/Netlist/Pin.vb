Namespace NetList

    ''' <summary>
    ''' This is the <see cref="isr.IO.NetList.Pin">Pin</see> class of the net list reader class
    ''' library.
    ''' </summary>
    ''' <remarks>
    ''' David, 08/07/08, 1.2.3141.Add mating connector information for use with the Sandia
    ''' cable tester. <para>
    ''' David, 08/07/08, 1.2.3141. Add node number.  Pins can be listed as belonging to a node
    ''' separately from the connector. The node is a functional description of the pins telling which
    ''' pins are connected together. </para><para>
    ''' David, 12/18/07, 1.2.2908 </para><para>
    ''' (c) 2007 Integrated Scientific Resources, Inc. All rights reserved.</para><para>
    ''' Licensed under The MIT License.</para>
    ''' </remarks>
    Public Class Pin

        Implements IDisposable

#Region " CONSTRUCTION and CLEANUP "

        ''' <summary> Constructs this class. </summary>
        ''' <remarks> David, 10/8/2020. </remarks>
        Public Sub New()

            ' instantiate the class
            MyBase.New()
            Me.MatingSide = "P1"
        End Sub

        ''' <summary> Calls <see cref="M:Dispose(Boolean Disposing)"/> to cleanup. </summary>
        ''' <remarks>
        ''' Do not make this method Overridable (virtual) because a derived class should not be able to
        ''' override this method.
        ''' </remarks>
        Public Sub Dispose() Implements IDisposable.Dispose

            ' Do not change this code.  Put cleanup code in Dispose(Boolean) below.

            ' this disposes all child classes.
            Me.Dispose(True)

            ' Take this object off the finalization(Queue) and prevent finalization code 
            ' from executing a second time.
            GC.SuppressFinalize(Me)

        End Sub

        ''' <summary> True if is disposed, false if not. </summary>
        Private _IsDisposed As Boolean

        ''' <summary>
        ''' Gets or sets the dispose status sentinel of the base class.  This applies to the derived
        ''' class provided proper implementation.
        ''' </summary>
        ''' <value> The is disposed. </value>
        Protected Property IsDisposed() As Boolean
            Get
                Return Me._IsDisposed
            End Get
            Private Set(ByVal value As Boolean)
                Me._IsDisposed = value
            End Set
        End Property

        ''' <summary>
        ''' Releases the unmanaged resources used by the <see cref="T:System.Windows.Forms.Control" />
        ''' and its child controls and optionally releases the managed resources.
        ''' </summary>
        ''' <remarks> David, 10/8/2020. </remarks>
        ''' <param name="disposing"> <c>True</c> to release both managed and unmanaged resources;
        '''                                                   <c>False</c> to release only unmanaged
        '''                                                   resources when called from the runtime
        '''                                                   finalize. </param>
        <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
        <System.Diagnostics.DebuggerNonUserCode()>
        Protected Overridable Sub Dispose(ByVal disposing As Boolean)
            Try
                If Not Me.IsDisposed AndAlso disposing Then
                End If
            Finally
                Me.IsDisposed = True
            End Try
        End Sub

        ''' <summary>
        ''' This destructor will run only if the Dispose method does not get called. It gives the base
        ''' class the opportunity to finalize. Do not provide destructors in types derived from this
        ''' class.
        ''' </summary>
        ''' <remarks> David, 10/8/2020. </remarks>
        Protected Overrides Sub Finalize()
            ' Do not re-create Dispose clean-up code here.
            ' Calling Dispose(false) is optimal for readability and maintainability.
            Me.Dispose(False)
        End Sub

#End Region

#Region " METHODS "

        ''' <summary> Parses the pin record. </summary>
        ''' <remarks>
        ''' The pin record includes the pin information as follows (after Trim):<para>
        ''' 0         1         2         3         4         5         6         7
        ''' 012345678901234567890123456789012345678901234567890123456789012345678901234567
        ''' P2              1       1               Passive        CONNECTOR DB9-2 J237            22
        ''' 22              Passive        J237 Connector Number:  (0,15)</para><para>
        ''' Pin Number (16, 23)</para><para>
        ''' Pin Name (24, 39)</para><para>
        ''' Pin Type (40,54)</para><para>
        ''' Part Value (Connector Name/Description): (55,EOL)</para><para>
        ''' </para>
        ''' </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="record"> A record from the net list file. </param>
        ''' <returns> True if it succeeds, false if it fails. </returns>
        Public Function Parse(ByVal record As String) As Boolean

            If String.IsNullOrWhiteSpace(record) Then Throw New ArgumentNullException(NameOf(record))

            Me.ConnectorNumber = record.Substring(0, 16).Trim
            Me.Number = record.Substring(16, 8).Trim
            Me.Name = record.Substring(24, 16).Trim
            Me.PinType = record.Substring(40, 15).Trim
            Me.PartValue = record.Substring(55).Trim

            Return Not (String.IsNullOrWhiteSpace(Me.ConnectorNumber) OrElse
                        String.IsNullOrWhiteSpace(Me.Number) OrElse
                        String.IsNullOrWhiteSpace(Me.Name) OrElse
                        String.IsNullOrWhiteSpace(Me.PinType) OrElse
                        String.IsNullOrWhiteSpace(Me.PartValue))

        End Function

#End Region

#Region " PROPERTIES "

        ''' <summary> Returns a unique key for a pin. </summary>
        ''' <remarks> David, 10/8/2020. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="pin"> The pin. </param>
        ''' <returns> A String. </returns>
        Public Shared Function BuildKey(ByVal pin As Pin) As String
            If pin Is Nothing Then Throw New ArgumentNullException(NameOf(pin))
            Return String.Format(Globalization.CultureInfo.CurrentCulture,
                                 "{0}.{1}", pin.ConnectorNumber, pin.Number)
        End Function

        ''' <summary> Returns a unique pin Key. </summary>
        ''' <value> The key. </value>
        Public ReadOnly Property Key() As String
            Get
                Return BuildKey(Me)
            End Get
        End Property

        ''' <summary> Gets or sets the pin name. </summary>
        ''' <value> The name. </value>
        Public Property Name() As String

        ''' <summary> Gets or sets the pin number. </summary>
        ''' <value> The number. </value>
        Public Property Number() As String

        ''' <summary> Gets or sets the public key reference from the database. </summary>
        ''' <value> The public key. </value>
        Public Property PublicKey() As Integer

        ''' <summary> Gets or sets the pin type. </summary>
        ''' <value> The type of the pin. </value>
        Public Property PinType() As String

        ''' <summary> Gets or sets the connector number. </summary>
        ''' <value> The connector number. </value>
        Public Property ConnectorNumber() As String

        ''' <summary>
        ''' Gets or sets the node number.  The node number is Nullable as not every pin belongs to a
        ''' node.  Pins not belonging to a node are not wired.
        ''' </summary>
        ''' <value> The node number. </value>
        Public Property NodeNumber() As Integer?

        ''' <summary> Gets or sets the connector value. </summary>
        ''' <value> The part value. </value>
        Public Property PartValue() As String

#End Region

#Region " PIN DEFINITIONS PROPERTIES "

        ''' <summary>
        ''' Gets or sets the mating side for this connector.  For Sandia systems this could be either
        ''' 'P1' or 'P2'.
        ''' </summary>
        ''' <value> The mating side. </value>
        Public Property MatingSide() As String

        ''' <summary>
        ''' Gets or sets the pin number on the mating side to use for connecting this pin.
        ''' </summary>
        ''' <value> The mating pin number. </value>
        Public Property MatingPinNumber() As Integer = 1

#End Region


    End Class

End Namespace

