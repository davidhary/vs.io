﻿Imports System.Reflection
Imports System.Runtime.InteropServices

<Assembly: AssemblyTitle("File Watcher Task Tray")>
<Assembly: AssemblyDescription("File Watcher Task Tray")>
<Assembly: AssemblyProduct("File.Watcher.Task.Tray")>
<Assembly: CLSCompliant(True)>

' Disable accessibility of an individual managed type or member, or of all types within an assembly, to COM.
<Assembly: ComVisible(False)>
