''' <summary> Task tray application context. </summary>
''' <remarks> (c) 2014 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 2014-07-09 </para></remarks>
Public Class TaskTrayApplicationContext
    Inherits ApplicationContext

    ''' <summary> Default constructor. </summary>
    Public Sub New()

        MyBase.New()

        AddHandler Application.ApplicationExit, AddressOf OnApplicationExit

        Me.InitializeComponents()

        Me.InitializeForm()

        _NotifyIcon.Visible = True

    End Sub

    Private _IsDisposed As Boolean

    ''' <summary> Releases the unmanaged resources used by the isr.IO.TaskTrayApplicationContext and
    ''' optionally releases the managed resources. </summary>
    ''' <param name="disposing"> true to release both managed and unmanaged resources; false to
    ''' release only unmanaged resources. </param>
    Protected Overrides Sub Dispose(disposing As Boolean)
        Try
            If Not Me._isDisposed AndAlso disposing Then
                If _ConfigWindow IsNot Nothing Then
                    _ConfigWindow.Dispose()
                    _ConfigWindow = Nothing
                End If
                If _NotifyIcon IsNot Nothing Then
                    _NotifyIcon.Dispose()
                    _NotifyIcon = Nothing
                End If
            End If
        Finally
            Me._isDisposed = True
            MyBase.Dispose(disposing)
        End Try
    End Sub

#Region " COMPONENTS "

    Private _NotifyIcon As NotifyIcon

    ''' <summary> Initializes the components. </summary>
    Private Sub InitializeComponents()

        ' The icon is added to the project resources. 
        ' _NotifyIcon.Icon = My.Resources.AppIcon
        _NotifyIcon = New NotifyIcon() With {
            .Icon = My.Resources.TheArtOfWorkAround24bit
        }

        AddHandler _NotifyIcon.DoubleClick, AddressOf NotifyIcon_DoubleClick

        _NotifyIcon.BalloonTipIcon = ToolTipIcon.Info
        _NotifyIcon.BalloonTipText = My.Settings.BalloonTipText
        _NotifyIcon.BalloonTipTitle = My.Settings.BalloonTipTitle
        _NotifyIcon.Text = My.Settings.IconText

        Dim _NotifyIconContextMenu As New ContextMenuStrip()
        _NotifyIconContextMenu.SuspendLayout()

        ' Add a context menu to the _NotifyIcon:
        _NotifyIconContextMenu = New ContextMenuStrip()
        _NotifyIconContextMenu.SuspendLayout()

        Dim openDialogMenuItem As New ToolStripMenuItem() With {
            .Name = "Open",
            .Size = New Size(153, 22),
            .Text = "Open Dialog"
        }
        AddHandler openDialogMenuItem.Click, AddressOf openDialog

        Dim exitMenuItem As New ToolStripMenuItem() With {
            .Name = "Exit",
            .Size = New Size(153, 22),
            .Text = "Exit the program"
        }
        AddHandler exitMenuItem.Click, AddressOf [Exit]
        ' 
        ' _NotifyIconContextMenu
        ' 
        _NotifyIconContextMenu.Items.AddRange(New ToolStripItem() {openDialogMenuItem, exitMenuItem})
        _NotifyIconContextMenu.Name = "_NotifyIconContextMenu"
        _NotifyIconContextMenu.Size = New Size(153, 70)

        _NotifyIconContextMenu.ResumeLayout(False)

        _NotifyIcon.ContextMenuStrip = _NotifyIconContextMenu

    End Sub

#End Region

#Region " FORM "

    Private _ConfigWindow As ConfigurationForm

    ''' <summary> Initializes the form. </summary>
    Private Sub InitializeForm()

        _ConfigWindow = New ConfigurationForm()

    End Sub

#End Region

#Region " EVENT HANDLERS "

    ''' <summary> Event handler. Called by _NotifyIcon for double click events. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub NotifyIcon_DoubleClick(ByVal sender As Object, ByVal e As EventArgs)
        ' Here you can do stuff if the tray icon is double clicked
        Dim icon As NotifyIcon = TryCast(sender, NotifyIcon)
        If icon IsNot Nothing Then
            icon.ShowBalloonTip(10000)
        End If
    End Sub

    ''' <summary> Shows the underlying dialog form. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub OpenDialog(ByVal sender As Object, ByVal e As EventArgs)
        ' If we are already showing the window merely focus it.
        If _ConfigWindow.Visible Then
            _ConfigWindow.Focus()
        Else
            _ConfigWindow.ShowDialog()
        End If
    End Sub

    ''' <summary> Handles the application exit event. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information to send to registered event handlers. </param>
    Private Sub OnApplicationExit(ByVal sender As Object, ByVal e As EventArgs)
        ' Cleanup so that the icon will be removed when the application is closed
        ' We must manually tidy up and remove the icon before we exit.
        ' Otherwise it will be left behind until the user mouses over.
        If Me._NotifyIcon IsNot Nothing Then
            Me._NotifyIcon.Visible = False
        End If
    End Sub

    ''' <summary> Exits. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub [Exit](ByVal sender As Object, ByVal e As EventArgs)
        If System.Windows.Forms.MessageBox.Show(My.Settings.ExitMessage, "Are you sure?",
                           MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2,
                           MessageBoxOptions.DefaultDesktopOnly) = DialogResult.Yes Then
            Application.Exit()
        End If
    End Sub

#End Region


End Class
