
Friend NotInheritable Class Program

    Private Sub New()
    End Sub

    ''' <summary> The main entry point for the application. </summary>
    <STAThread()> _
    Public Shared Sub Main()
        Application.EnableVisualStyles()
        Application.SetCompatibleTextRenderingDefault(False)
        ' Instead of running a form, we run an ApplicationContext.
        Application.Run(New TaskTrayApplicationContext())
    End Sub

End Class
