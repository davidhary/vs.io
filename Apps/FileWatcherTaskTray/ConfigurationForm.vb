Partial Public Class ConfigurationForm
    Inherits Form

    Public Sub New()
        InitializeComponent()
    End Sub

    Private Sub LoadSettings(ByVal sender As Object, ByVal e As EventArgs) Handles MyBase.Shown
        _ShowMessageCheckBox.Checked = My.Settings.ShowMessage
    End Sub

    Private Sub SaveSettings(ByVal sender As Object, ByVal e As FormClosingEventArgs) Handles MyBase.FormClosing
        ' If the user clicked "Save"
        If Me.DialogResult = System.Windows.Forms.DialogResult.OK Then
            My.Settings.ShowMessage = _ShowMessageCheckBox.Checked
            My.Settings.Save()
        End If
    End Sub

End Class
