Partial Public Class ConfigurationForm
    ''' <summary>
    ''' Required designer variable.
    ''' </summary>
    Private components As System.ComponentModel.IContainer = Nothing

    ''' <summary>
    ''' Clean up any resources being used.
    ''' </summary>
    ''' <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso (components IsNot Nothing) Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

#Region "Windows Form Designer generated code"

    ''' <summary>
    ''' Required method for Designer support - do not modify
    ''' the contents of this method with the code editor.
    ''' </summary>
    Private Sub InitializeComponent()
        Me._ShowMessageCheckBox = New System.Windows.Forms.CheckBox()
        Me._SaveButton = New System.Windows.Forms.Button()
        Me._CancelButton = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'showMessageCheckBox
        '
        Me._ShowMessageCheckBox.AutoSize = True
        Me._ShowMessageCheckBox.Checked = True
        Me._ShowMessageCheckBox.CheckState = System.Windows.Forms.CheckState.Checked
        Me._ShowMessageCheckBox.Location = New System.Drawing.Point(12, 12)
        Me._ShowMessageCheckBox.Name = "showMessageCheckBox"
        Me._ShowMessageCheckBox.Size = New System.Drawing.Size(179, 17)
        Me._ShowMessageCheckBox.TabIndex = 0
        Me._ShowMessageCheckBox.Text = "Show Message On Double-Click"
        Me._ShowMessageCheckBox.UseVisualStyleBackColor = True
        '
        'saveButton
        '
        Me._SaveButton.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me._SaveButton.DialogResult = System.Windows.Forms.DialogResult.OK
        Me._SaveButton.Location = New System.Drawing.Point(104, 36)
        Me._SaveButton.Name = "saveButton"
        Me._SaveButton.Size = New System.Drawing.Size(75, 23)
        Me._SaveButton.TabIndex = 1
        Me._SaveButton.Text = "Save"
        Me._SaveButton.UseVisualStyleBackColor = True
        '
        'cancelButton_Renamed
        '
        Me._CancelButton.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me._CancelButton.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me._CancelButton.Location = New System.Drawing.Point(23, 36)
        Me._CancelButton.Name = "cancelButton_Renamed"
        Me._CancelButton.Size = New System.Drawing.Size(75, 23)
        Me._CancelButton.TabIndex = 2
        Me._CancelButton.Text = "Cancel"
        Me._CancelButton.UseVisualStyleBackColor = True
        '
        'Configuration
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(203, 71)
        Me.Controls.Add(Me._CancelButton)
        Me.Controls.Add(Me._SaveButton)
        Me.Controls.Add(Me._ShowMessageCheckBox)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.MaximizeBox = False
        Me.Name = "Configuration"
        Me.Text = "Configuration"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

#End Region

    Private _ShowMessageCheckBox As System.Windows.Forms.CheckBox
    Private _SaveButton As System.Windows.Forms.Button
    Private _CancelButton As System.Windows.Forms.Button
End Class
