ISR IO File Watcher Demo<sub>&trade;</sub>: File System Watcher Demo. Revision History


*1.2.5302 2014-07-08*  
Created from source project example,

\(c\) 2014 Integrated Scientific Resources, Inc. All rights reserved.

### The MIT License [](#){name=The-MIT-License}
THE SOFTWARE IS PROVIDED \"AS IS\", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

You are hereby granted permission to add your license notice in
accordance with your agreement with ISR.

This software was developed and tested using Microsoft<sup>&reg;</sup> [Visual Studio](https://www.visualstudIO.com/) 2019.  

The source code for this project is hosted on [Bit Bucket](https://bitbucket.org/davidhary).

This software uses open source described and licensed on the following
sites:  
[File System Watcher Pure
Chaos](http://www.codeproject.com/Articles/58740/FileSystemWatcher-Pure-Chaos-Part-of)
