﻿Namespace My

    Partial Friend Class MyApplication

        ''' <summary> Gets the identifier of the trace source. </summary>
        Public Const TraceEventId As Integer = isr.IO.My.ProjectTraceEventId.FileWatcherTester

        Public Const AssemblyTitle As String = "File Watcher"
        Public Const AssemblyDescription As String = "File Watcher"
        Public Const AssemblyProduct As String = "File.Watcher"

    End Class

End Namespace

