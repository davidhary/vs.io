﻿Partial Public Class MainForm
    ''' <summary>
    ''' Required designer variable.
    ''' </summary>
    Private components As System.ComponentModel.IContainer = Nothing

    ''' <summary>
    ''' Required method for Designer support - do not modify
    ''' the contents of this method with the code editor.
    ''' </summary>
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(MainForm))
        Me._IncludeSubFoldersCheckBox = New System.Windows.Forms.CheckBox()
        Me._BrowseForFolderButton = New System.Windows.Forms.Button()
        Me._DetectedEventsListViewLabel = New System.Windows.Forms.Label()
        Me._DoneButton = New System.Windows.Forms.Button()
        Me._StopButton = New System.Windows.Forms.Button()
        Me._StartButton = New System.Windows.Forms.Button()
        Me._FolderToWatchTextBox = New System.Windows.Forms.TextBox()
        Me._FolderToWatchTextBoxLabel = New System.Windows.Forms.Label()
        Me._DetectedEventsListView = New isr.Core.Controls.ListView()
        Me._TimeColumnHeader = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me._EventColumnHeader = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me._FilterColumnHeader = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me._FileNameColumnHeader = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me._ClearListButton = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        '_IncludeSubFoldersCheckBox
        '
        Me._IncludeSubFoldersCheckBox.AutoSize = True
        Me._IncludeSubFoldersCheckBox.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._IncludeSubFoldersCheckBox.Location = New System.Drawing.Point(35, 596)
        Me._IncludeSubFoldersCheckBox.Name = "_IncludeSubFoldersCheckBox"
        Me._IncludeSubFoldersCheckBox.Size = New System.Drawing.Size(146, 21)
        Me._IncludeSubFoldersCheckBox.TabIndex = 17
        Me._IncludeSubFoldersCheckBox.Text = "Include sub-folders"
        Me._IncludeSubFoldersCheckBox.UseVisualStyleBackColor = True
        '
        '_BrowseForFolderButton
        '
        Me._BrowseForFolderButton.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Bold)
        Me._BrowseForFolderButton.Location = New System.Drawing.Point(580, 10)
        Me._BrowseForFolderButton.Name = "_BrowseForFolderButton"
        Me._BrowseForFolderButton.Size = New System.Drawing.Size(33, 23)
        Me._BrowseForFolderButton.TabIndex = 16
        Me._BrowseForFolderButton.Text = "..."
        Me._BrowseForFolderButton.UseVisualStyleBackColor = True
        '
        '_DetectedEventsListViewLabel
        '
        Me._DetectedEventsListViewLabel.AutoSize = True
        Me._DetectedEventsListViewLabel.Location = New System.Drawing.Point(9, 47)
        Me._DetectedEventsListViewLabel.Name = "_DetectedEventsListViewLabel"
        Me._DetectedEventsListViewLabel.Size = New System.Drawing.Size(169, 17)
        Me._DetectedEventsListViewLabel.TabIndex = 15
        Me._DetectedEventsListViewLabel.Text = "Detected File System Events"
        '
        '_DoneButton
        '
        Me._DoneButton.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me._DoneButton.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._DoneButton.Location = New System.Drawing.Point(361, 591)
        Me._DoneButton.Name = "_DoneButton"
        Me._DoneButton.Size = New System.Drawing.Size(75, 30)
        Me._DoneButton.TabIndex = 14
        Me._DoneButton.Text = "Done"
        Me._DoneButton.UseVisualStyleBackColor = True
        '
        '_StopButton
        '
        Me._StopButton.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me._StopButton.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._StopButton.Location = New System.Drawing.Point(276, 591)
        Me._StopButton.Name = "_StopButton"
        Me._StopButton.Size = New System.Drawing.Size(75, 30)
        Me._StopButton.TabIndex = 13
        Me._StopButton.Text = "Stop"
        Me._StopButton.UseVisualStyleBackColor = True
        '
        '_StartButton
        '
        Me._StartButton.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me._StartButton.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._StartButton.Location = New System.Drawing.Point(191, 591)
        Me._StartButton.Name = "_StartButton"
        Me._StartButton.Size = New System.Drawing.Size(75, 30)
        Me._StartButton.TabIndex = 12
        Me._StartButton.Text = "Start"
        Me._StartButton.UseVisualStyleBackColor = True
        '
        '_FolderToWatchTextBox
        '
        Me._FolderToWatchTextBox.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._FolderToWatchTextBox.Location = New System.Drawing.Point(110, 8)
        Me._FolderToWatchTextBox.Name = "_FolderToWatchTextBox"
        Me._FolderToWatchTextBox.Size = New System.Drawing.Size(466, 25)
        Me._FolderToWatchTextBox.TabIndex = 11
        '
        '_FolderToWatchTextBoxLabel
        '
        Me._FolderToWatchTextBoxLabel.AutoSize = True
        Me._FolderToWatchTextBoxLabel.Location = New System.Drawing.Point(7, 12)
        Me._FolderToWatchTextBoxLabel.Name = "_FolderToWatchTextBoxLabel"
        Me._FolderToWatchTextBoxLabel.Size = New System.Drawing.Size(101, 17)
        Me._FolderToWatchTextBoxLabel.TabIndex = 10
        Me._FolderToWatchTextBoxLabel.Text = "Folder to watch:"
        '
        '_DetectedEventsListView
        '
        Me._DetectedEventsListView.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me._DetectedEventsListView.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me._TimeColumnHeader, Me._EventColumnHeader, Me._FilterColumnHeader, Me._FileNameColumnHeader})
        Me._DetectedEventsListView.FullRowSelect = True
        Me._DetectedEventsListView.GridLines = True
        Me._DetectedEventsListView.Location = New System.Drawing.Point(7, 67)
        Me._DetectedEventsListView.Name = "_DetectedEventsListView"
        Me._DetectedEventsListView.Size = New System.Drawing.Size(609, 517)
        Me._DetectedEventsListView.TabIndex = 9
        Me._DetectedEventsListView.UseCompatibleStateImageBehavior = False
        Me._DetectedEventsListView.View = System.Windows.Forms.View.Details
        '
        '_TimeColumnHeader
        '
        Me._TimeColumnHeader.Text = "Time"
        Me._TimeColumnHeader.Width = 50
        '
        '_EventColumnHeader
        '
        Me._EventColumnHeader.Text = "Event"
        Me._EventColumnHeader.Width = 80
        '
        '_FilterColumnHeader
        '
        Me._FilterColumnHeader.Text = "Filter"
        Me._FilterColumnHeader.Width = 80
        '
        '_FileNameColumnHeader
        '
        Me._FileNameColumnHeader.Text = "File Name"
        Me._FileNameColumnHeader.Width = 395
        '
        '_ClearListButton
        '
        Me._ClearListButton.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._ClearListButton.Location = New System.Drawing.Point(541, 39)
        Me._ClearListButton.Name = "_ClearListButton"
        Me._ClearListButton.Size = New System.Drawing.Size(75, 29)
        Me._ClearListButton.TabIndex = 18
        Me._ClearListButton.Text = "Clear List"
        Me._ClearListButton.UseVisualStyleBackColor = True
        '
        'MainForm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 17.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(624, 624)
        Me.Controls.Add(Me._ClearListButton)
        Me.Controls.Add(Me._IncludeSubFoldersCheckBox)
        Me.Controls.Add(Me._BrowseForFolderButton)
        Me.Controls.Add(Me._DetectedEventsListViewLabel)
        Me.Controls.Add(Me._DoneButton)
        Me.Controls.Add(Me._StopButton)
        Me.Controls.Add(Me._StartButton)
        Me.Controls.Add(Me._FolderToWatchTextBox)
        Me.Controls.Add(Me._FolderToWatchTextBoxLabel)
        Me.Controls.Add(Me._DetectedEventsListView)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "MainForm"
        Me.Text = "File Watcher"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Private _IncludeSubFoldersCheckBox As System.Windows.Forms.CheckBox
    Private WithEvents _BrowseForFolderButton As System.Windows.Forms.Button
    Private _DetectedEventsListViewLabel As System.Windows.Forms.Label
    Private WithEvents _DoneButton As System.Windows.Forms.Button
    Private WithEvents _StopButton As System.Windows.Forms.Button
    Private WithEvents _StartButton As System.Windows.Forms.Button
    Private _FolderToWatchTextBox As System.Windows.Forms.TextBox
    Private _FolderToWatchTextBoxLabel As System.Windows.Forms.Label
    Private _DetectedEventsListView As Core.Controls.ListView
    Private _TimeColumnHeader As System.Windows.Forms.ColumnHeader
    Private _EventColumnHeader As System.Windows.Forms.ColumnHeader
    Private _FilterColumnHeader As System.Windows.Forms.ColumnHeader
    Private _FileNameColumnHeader As System.Windows.Forms.ColumnHeader
    Private WithEvents _ClearListButton As System.Windows.Forms.Button
End Class
