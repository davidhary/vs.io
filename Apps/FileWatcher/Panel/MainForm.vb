Imports System.IO
Imports isr.Core.Forma
Imports isr.IO.FileWatcher

Partial Public Class MainForm
    Inherits UserFormBase

#Region " CONSTRUCTION "

    Public Sub New()
        Me.InitializeComponent()

        Me._FolderToWatchTextBox.Text = My.Settings.Folder
        Me._StartButton.Enabled = True
        Me._StopButton.Enabled = False
    End Sub

    ''' <summary>
    ''' Clean up any resources being used.
    ''' </summary>
    ''' <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Me._Watcher IsNot Nothing Then
                Me._Watcher.Dispose()
                Me._Watcher = Nothing
            End If
            If Me.components IsNot Nothing Then
                Me.components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

#End Region

#Region " Helper Methods "

    ''' <summary> The watcher. </summary>
    Private _Watcher As Watcher = Nothing

    ''' <summary> Toggles the Watcher event handlers on/off. </summary>
    ''' <param name="add"> true to add. </param>
    Private Sub ManageEventHandlers(ByVal add As Boolean)

        ' For the purposes of this demo, I funneled all of the change events into 
        ' one handler to keep the code base smaller. You could certainly have an 
        ' individual handler for each type of change event if you desire.
        If Me._Watcher IsNot Nothing Then
            If add Then
                AddHandler Me._Watcher.ChangedAttribute, AddressOf Me.Watcher_EventChanged
                AddHandler Me._Watcher.ChangedCreationTime, AddressOf Me.Watcher_EventChanged
                AddHandler Me._Watcher.ChangedDirectoryName, AddressOf Me.Watcher_EventChanged
                AddHandler Me._Watcher.ChangedFileName, AddressOf Me.Watcher_EventChanged
                AddHandler Me._Watcher.ChangedLastAccess, AddressOf Me.Watcher_EventChanged
                AddHandler Me._Watcher.ChangedLastWrite, AddressOf Me.Watcher_EventChanged
                AddHandler Me._Watcher.ChangedSecurity, AddressOf Me.Watcher_EventChanged
                AddHandler Me._Watcher.ChangedSize, AddressOf Me.Watcher_EventChanged
                AddHandler Me._Watcher.Created, AddressOf Me.Watcher_EventCreated
                AddHandler Me._Watcher.Deleted, AddressOf Me.Watcher_EventDeleted
                AddHandler Me._Watcher.Disposed, AddressOf Me.Watcher_EventDisposed
                AddHandler Me._Watcher.ErrorOccurred, AddressOf Me.Watcher_EventError
                AddHandler Me._Watcher.Renamed, AddressOf Me.Watcher_EventRenamed
                AddHandler Me._Watcher.ConnectionChanged, AddressOf Me.Watcher_ConnectionChanged
            Else
                RemoveHandler Me._Watcher.ChangedAttribute, AddressOf Me.Watcher_EventChanged
                RemoveHandler Me._Watcher.ChangedCreationTime, AddressOf Me.Watcher_EventChanged
                RemoveHandler Me._Watcher.ChangedDirectoryName, AddressOf Me.Watcher_EventChanged
                RemoveHandler Me._Watcher.ChangedFileName, AddressOf Me.Watcher_EventChanged
                RemoveHandler Me._Watcher.ChangedLastAccess, AddressOf Me.Watcher_EventChanged
                RemoveHandler Me._Watcher.ChangedLastWrite, AddressOf Me.Watcher_EventChanged
                RemoveHandler Me._Watcher.ChangedSecurity, AddressOf Me.Watcher_EventChanged
                RemoveHandler Me._Watcher.ChangedSize, AddressOf Me.Watcher_EventChanged
                RemoveHandler Me._Watcher.Created, AddressOf Me.Watcher_EventCreated
                RemoveHandler Me._Watcher.Deleted, AddressOf Me.Watcher_EventDeleted
                RemoveHandler Me._Watcher.Disposed, AddressOf Me.Watcher_EventDisposed
                RemoveHandler Me._Watcher.ErrorOccurred, AddressOf Me.Watcher_EventError
                RemoveHandler Me._Watcher.Renamed, AddressOf Me.Watcher_EventRenamed
                RemoveHandler Me._Watcher.ConnectionChanged, AddressOf Me.Watcher_ConnectionChanged
            End If
        End If
    End Sub

    ''' <summary> Initializes the Watcher object. In the interest of providing a complete demonstration,
    ''' all change events are monitored. This will unlikely be a real-world requirement in most
    ''' cases. </summary>
    ''' <returns> true if it succeeds, false if it fails. </returns>
    Private Function InitWatcher() As Boolean
        Dim result As Boolean = False
        If Directory.Exists(Me._FolderToWatchTextBox.Text) OrElse File.Exists(Me._FolderToWatchTextBox.Text) Then
            Dim info As New WatcherInfo() With {
                .ChangesFilters = NotifyFilters.Attributes Or NotifyFilters.CreationTime Or NotifyFilters.DirectoryName Or
                                  NotifyFilters.FileName Or NotifyFilters.LastAccess Or NotifyFilters.LastWrite Or
                                  NotifyFilters.Security Or NotifyFilters.Size,
                .IncludeSubfolders = Me._IncludeSubFoldersCheckBox.Checked,
                .WatchesFilters = WatcherChangeTypes.All,
                .WatchForDisposed = True,
                .WatchForError = False,
                .WatchPath = Me._FolderToWatchTextBox.Text,
                .MonitorPathInterval = TimeSpan.FromMilliseconds(250)
            }
            Me._Watcher = New Watcher(info)
            Me.ManageEventHandlers(True)
            result = True
        Else
            isr.Core.WindowsForms.ShowDialogOkay($"The folder (or file) {Me._FolderToWatchTextBox.Text} specified does not exist.Please specify a valid folder or filename.",
                                                  "File not found")
        End If
        Return result
    End Function
#End Region ' Helper Methods

#Region " ListView Update Delegate Methods and Definitions "

    Private Delegate Sub DelegateCreateListViewItem(ByVal eventName As String, ByVal filterName As String, ByVal fileName As String)

    ''' <summary> Common ListViewItem creation for all events.  This method checks to see if invoke is
    ''' required, and makes the appropriate call to the InvokedCreateListViewItem() method. </summary>
    ''' <param name="eventName">  Name of the event. </param>
    ''' <param name="filterName"> Name of the filter. </param>
    ''' <param name="fileName">   Filename of the file. </param>
    Private Sub CreateListViewItem(ByVal eventName As String, ByVal filterName As String, ByVal fileName As String)
        If Me._DetectedEventsListView.InvokeRequired Then
            Dim method As New DelegateCreateListViewItem(AddressOf Me.InvokedCreateListViewItem)
            Me.Invoke(method, New Object(2) {eventName, filterName, fileName})
        Else
            Me.InvokedCreateListViewItem(eventName, filterName, fileName)
        End If
    End Sub

    ''' <summary>
    ''' The method actually used to update the list view. 
    ''' </summary>
    ''' <param name="eventName"></param>
    ''' <param name="filterName"></param>
    ''' <param name="fileName"></param>
    Private Sub InvokedCreateListViewItem(ByVal eventName As String, ByVal filterName As String, ByVal fileName As String)
        Dim lvi As New ListViewItem() With {
            .Text = DateTimeOffset.Now.ToString("HH:mm:ss", Globalization.CultureInfo.CurrentCulture)
        }
        lvi.SubItems.Add(New ListViewItem.ListViewSubItem(lvi, eventName))
        lvi.SubItems.Add(New ListViewItem.ListViewSubItem(lvi, filterName))
        lvi.SubItems.Add(New ListViewItem.ListViewSubItem(lvi, fileName))
        'this.listView1.BeginUpdate();

        Me._DetectedEventsListView.Items.Add(lvi)
        Me._DetectedEventsListView.EnsureVisible(Me._DetectedEventsListView.Items.Count - 1)
        'this.listView1.EndUpdate();
    End Sub
#End Region ' ListView Update Delegate Methods and Definitions 

#Region " Watcher Events "

    ''' <summary> Fired when the Watcher object detects a file rename event. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Watcher ex event information. </param>
    Private Sub Watcher_EventRenamed(ByVal sender As Object, ByVal e As WatcherEventArgs)
        Me.CreateListViewItem("Renamed", "N/A", If(e.Arguments Is Nothing, "Null argument object", CType(e.Arguments, RenamedEventArgs).FullPath))
    End Sub

    ''' <summary> Fired when the Watcher object detects an error event. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Watcher ex event information. </param>
    Private Sub Watcher_EventError(ByVal sender As Object, ByVal e As WatcherEventArgs)
        Me.CreateListViewItem("Error", "N/A", If(e.Arguments Is Nothing, "Null argument object", CType(e.Arguments, EventArgs).ToString()))
    End Sub


    ''' <summary> Fired when the Watcher object disposes one of the internal FileSystemWatcher objects. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Watcher ex event information. </param>
    Private Sub Watcher_EventDisposed(ByVal sender As Object, ByVal e As WatcherEventArgs)
        Me.CreateListViewItem("Disposed", "N/A", If(e.Arguments Is Nothing, "Null argument object", CType(e.Arguments, EventArgs).ToString()))
    End Sub


    ''' <summary> Fired when the Watcher object detects a file deleted event. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Watcher ex event information. </param>
    Private Sub Watcher_EventDeleted(ByVal sender As Object, ByVal e As WatcherEventArgs)
        Me.CreateListViewItem("Deleted", "N/A", If(e.Arguments Is Nothing, "Null argument object", CType(e.Arguments, FileSystemEventArgs).FullPath))
    End Sub

    ''' <summary> Fired when the Watcher object detects a file created event. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Watcher ex event information. </param>
    Private Sub Watcher_EventCreated(ByVal sender As Object, ByVal e As WatcherEventArgs)
        Me.CreateListViewItem("Created", "N/A", If(e.Arguments Is Nothing, "Null argument object", CType(e.Arguments, FileSystemEventArgs).FullPath))

    End Sub

    ''' <summary> Fired when the Watcher object detects a folder/file change event. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Watcher ex event information. </param>
    Private Sub Watcher_EventChanged(ByVal sender As Object, ByVal e As WatcherEventArgs)

        Me.CreateListViewItem("Change", e.Filter.ToString(), If(e.Arguments Is Nothing, "Null argument object", CType(e.Arguments, FileSystemEventArgs).FullPath))
    End Sub

    ''' <summary> Fired when the availability of the folder has changed.  If it becomes "disconnected",
    ''' the internal watchers are disabled. When it becomes connected again, the internal watchers
    ''' are restored. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Watcher ex event information. </param>
    Private Sub Watcher_ConnectionChanged(ByVal sender As Object, ByVal e As WatcherEventArgs)
        Dim watcher As Watcher = CType(sender, Watcher)
        If watcher IsNot Nothing AndAlso e IsNot Nothing Then
            Dim eventName As String = "Availability"
            Dim filterName As String = "N/A"
            Dim status = If(e.Arguments Is Nothing, "Null argument object", If(watcher.IsConnected, "Connected", "Disconnected"))
            Me.CreateListViewItem(eventName, filterName, status)
        End If

    End Sub

#End Region

#Region " Form Events "

    ''' <summary> Fired when the form is closing. Disposes the Watcher object. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Form closing event information. </param>
    Private Sub Form_Closing(ByVal sender As Object, ByVal e As FormClosingEventArgs) Handles MyBase.FormClosing
        If Me._Watcher IsNot Nothing Then
            Me.ManageEventHandlers(False)
            Me._Watcher.Dispose()
            Me._Watcher = Nothing
        End If
    End Sub

    ''' <summary> Form load. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub Form_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        Try

            ' Turn on the form hourglass cursor
            Me.Cursor = System.Windows.Forms.Cursors.WaitCursor

            ' set the form caption
            Me.Text = My.Application.Info.BuildDefaultCaption("")

        Catch

            ' Use throw without an argument in order to preserve the stack location 
            ' where the exception was initially raised.
            Throw


        Finally

            Me.Cursor = System.Windows.Forms.Cursors.Default

        End Try

    End Sub

    ''' <summary> Fired when the user clicks the Start button. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub StartButton_Click(ByVal sender As Object, ByVal e As EventArgs) Handles _StartButton.Click
        If Me._Watcher IsNot Nothing Then
            Me._Watcher.Dispose()
            Me._Watcher = Nothing
        End If
        If Me.InitWatcher() Then
            Me._Watcher.Start()
            Me._StartButton.Enabled = False
            Me._StopButton.Enabled = True
        End If
    End Sub

    ''' <summary> Fired when the user clicks the Stop button. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub StopButton_Click(ByVal sender As Object, ByVal e As EventArgs) Handles _StopButton.Click
        If Me._Watcher IsNot Nothing Then
            Me.ManageEventHandlers(False)
            Me._Watcher.Stop()
            Me._StartButton.Enabled = True
            Me._StopButton.Enabled = False
        End If
    End Sub

    ''' <summary> Fired when the user clicks the Done button. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub DoneButton_Click(ByVal sender As Object, ByVal e As EventArgs) Handles _DoneButton.Click
        Me.Close()
    End Sub

    ''' <summary> Fired when the user clicks the Browse button. </summary>
    ''' <param name="sender"> Source of the event. </param>

    ''' <param name="e">      Event information. </param>
    Private Sub BrowseForFolderButton_Click(ByVal sender As Object, ByVal e As EventArgs) Handles _BrowseForFolderButton.Click

        Dim appDataFolder As String = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData)
        appDataFolder = My.Computer.FileSystem.SpecialDirectories.CurrentUserApplicationData
        If String.IsNullOrEmpty(appDataFolder) Then

            isr.Core.WindowsForms.ShowDialogOkay("Could not determine the name of the Application Data folder on this machine. Terminating browse attempt.",
                                                 "Application data folder not determined")
            Return
        End If
        If String.IsNullOrEmpty(Me._FolderToWatchTextBox.Text) Then
            Me._FolderToWatchTextBox.Text = appDataFolder
        End If

        ' determine the folder from which we start browsing
        Dim startingDir As String = Me._FolderToWatchTextBox.Text

        ' if the specified folder doesn't exist
        If Not Directory.Exists(startingDir) Then
            System.Windows.Forms.MessageBox.Show(Me, "Specified folder does not exist. Starting with the Application Data folder instead.",
                            "Starting at Application Folder", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1, MessageBoxOptions.DefaultDesktopOnly)
            ' set our starting folder to the user's application data folder
            startingDir = appDataFolder
        End If

        Using dlg As New FolderBrowserDialog()
            dlg.SelectedPath = startingDir
            Dim result As DialogResult = dlg.ShowDialog()
            If result = System.Windows.Forms.DialogResult.OK Then
                If Not String.IsNullOrEmpty(dlg.SelectedPath) Then
                    Me._FolderToWatchTextBox.Text = dlg.SelectedPath
                    My.Settings.Folder = dlg.SelectedPath
                End If
            End If
        End Using

    End Sub

    ''' <summary> Fired when the user clicks the Clear List button. </summary>
    Private Sub ClearListButton_Click(ByVal sender As Object, ByVal e As EventArgs) Handles _ClearListButton.Click
        Me._DetectedEventsListView.Items.Clear()
    End Sub


#End Region

End Class
