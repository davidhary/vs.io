using System;
using System.Windows.Input;

namespace OdsReadWrite
{
    internal sealed class DelegateCommand : ICommand
    {
        private readonly Action _Execute;
        private readonly Func<bool> _CanExecute;

        public event EventHandler CanExecuteChanged;

        public DelegateCommand(Action execute)
            : this(execute, null)
        {
        }

        public DelegateCommand(Action execute, Func<bool> canExecute)
        {
            this._Execute = execute ?? throw new ArgumentNullException(nameof( execute ) );
            this._CanExecute = canExecute;
        }

        public void InvalidateCanExecuteChanged()
        {
            var handler = this.CanExecuteChanged;
            if (handler != null)
            {
                handler(this, EventArgs.Empty);
            }
        }

        public void Execute(object parameter)
        {
            this._Execute();
        }

        public bool CanExecute(object parameter)
        {
            return this._CanExecute == null || this._CanExecute();
        }
    }
}
