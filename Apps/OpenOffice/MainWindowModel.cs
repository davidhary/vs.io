using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Windows;
using Microsoft.Win32;
using OdsReadWrite.Properties;
using System.Diagnostics;
using isr.IO.OpenOffice;

namespace OdsReadWrite
{
    internal sealed class MainWindowModel : INotifyPropertyChanged, IDataErrorInfo
    {
        private const string
            _DataPropertyName = "Data",
            _SheetsPropertyName = "Sheets",
            _RowsPropertyName = "Rows",
            _ColumnsPropertyName = "Columns",
            _InputPathPropertyName = "InputPath",
            _OutputPathPropertyName = "OutputPath",
            _OpenCreatedFilePropertyName = "OpenCreatedFile",
            _SelectedDataTableIndexPropertyName = "SelectedDataTableIndex";
        private DataSet _Data;

        private string _Sheets = Settings.Default.Sheets.ToString();
        private string _Rows = Settings.Default.Rows.ToString();
        private string _Columns = Settings.Default.Columns.ToString();

        private string _InputPath = Settings.Default.InputPath;
        private string _OutputPath = Settings.Default.OutputPath;

        private bool _OpenCreatedFile = Settings.Default.OpenCreatedFile;

        private int _SelectedDataTableIndex;

        public event PropertyChangedEventHandler PropertyChanged;

        private static int? ToPositiveIntegerOrNull(string value)
        {
            return int.TryParse( value, out int result ) && result > 0 ? result : ( int? ) null;
        }

        private static bool DirectoryExists(string path)
        {
            return string.IsNullOrWhiteSpace(path) ? false : Directory.Exists(Path.GetDirectoryName(Path.GetFullPath(path)));
        }

        public MainWindowModel()
        {
            this.CreateCommand = new DelegateCommand(this.Create, this.CanCreate);
            this.ReadCommand = new DelegateCommand(this.Read, this.CanRead);
            this.WriteCommand = new DelegateCommand(this.Write, this.CanWrite);

            this.ShowOpenFileDialogCommand = new DelegateCommand(this.ShowOpenFileDialog);
            this.ShowSaveFileDialogCommand = new DelegateCommand(this.ShowSaveFileDialog);

            this.Create();

            App.Current.Exit += this.OnExit;
        }

        public DelegateCommand CreateCommand { get; }

        public DelegateCommand ReadCommand { get; }

        public DelegateCommand WriteCommand { get; }

        public DelegateCommand ShowOpenFileDialogCommand { get; }

        public DelegateCommand ShowSaveFileDialogCommand { get; }

        public DataSet Data
        {
            get => this._Data;
            private set => this.SetValue( _DataPropertyName, ref this._Data, value );
        }

        public string Sheets
        {
            get => this._Sheets;
            set => this.SetValue( _SheetsPropertyName, ref this._Sheets, value );
        }

        public string Rows
        {
            get => this._Rows;
            set => this.SetValue( _RowsPropertyName, ref this._Rows, value );
        }

        public string Columns
        {
            get => this._Columns;
            set => this.SetValue( _ColumnsPropertyName, ref this._Columns, value );
        }

        private int? SheetsCount => ToPositiveIntegerOrNull( this.Sheets );

        private int? RowsCount => ToPositiveIntegerOrNull( this.Rows );

        private int? ColumnsCount => ToPositiveIntegerOrNull( this.Columns );

        public string InputPath
        {
            get => this._InputPath;
            set => this.SetValue( _InputPathPropertyName, ref this._InputPath, value );
        }

        public string OutputPath
        {
            get => this._OutputPath;
            set => this.SetValue( _OutputPathPropertyName, ref this._OutputPath, value );
        }

        public bool OpenCreatedFile
        {
            get => this._OpenCreatedFile;
            set => this.SetValue( _OpenCreatedFilePropertyName, ref this._OpenCreatedFile, value );
        }

        public int SelectedDataTableIndex
        {
            get => this._SelectedDataTableIndex;
            set => this.SetValue( _SelectedDataTableIndexPropertyName, ref this._SelectedDataTableIndex, value );
        }

        public string Error => string.Empty;

        public string this[string propertyName]
        {
            get
            {
                switch (propertyName)
                {
                    case _SheetsPropertyName:
                        this.CreateCommand.InvalidateCanExecuteChanged();
                        return this.SheetsCount.HasValue ? string.Empty : "Value must be a positive integer.";
                    case _RowsPropertyName:
                        this.CreateCommand.InvalidateCanExecuteChanged();
                        return this.RowsCount.HasValue ? string.Empty : "Value must be a positive integer.";
                    case _ColumnsPropertyName:
                        this.CreateCommand.InvalidateCanExecuteChanged();
                        return this.ColumnsCount.HasValue ? string.Empty : "Value must be a positive integer.";
                    case _InputPathPropertyName:
                        this.ReadCommand.InvalidateCanExecuteChanged();
                        return File.Exists(this.InputPath) ? string.Empty : "File doesn't exist.";
                    case _OutputPathPropertyName:
                        this.WriteCommand.InvalidateCanExecuteChanged();
                        return DirectoryExists(this.OutputPath) ? string.Empty : "Directory doesn't exist.";
                    default:
                        return string.Empty;
                }
            }
        }

        private void SetValue<T>(string propertyName, ref T field, T value)
        {
            if (!EqualityComparer<T>.Default.Equals(field, value))
            {
                field = value;
                this.OnPropertyChanged(new PropertyChangedEventArgs(propertyName));
            }
        }

        private void OnPropertyChanged(PropertyChangedEventArgs e)
        {
            var handler = this.PropertyChanged;
            if (handler != null)
            {
                handler(this, e);
            }
        }

        private void OnExit(object sender, ExitEventArgs e)
        {
            var count = this.SheetsCount;
            if (count.HasValue)
            {
                Settings.Default.Sheets = count.Value;
            }

            count = this.RowsCount;
            if (count.HasValue)
            {
                Settings.Default.Rows = count.Value;
            }

            count = this.ColumnsCount;
            if (count.HasValue)
            {
                Settings.Default.Columns = count.Value;
            }

            if (File.Exists(this.InputPath))
            {
                Settings.Default.InputPath = this.InputPath;
            }

            if (DirectoryExists(this.OutputPath))
            {
                Settings.Default.OutputPath = this.OutputPath;
            }

            Settings.Default.OpenCreatedFile = this.OpenCreatedFile;

            Settings.Default.Save();
        }

        private bool CanCreate()
        {
            return this.SheetsCount.HasValue && this.RowsCount.HasValue && this.ColumnsCount.HasValue;
        }

        private void Create()
        {
            if (this.CanCreate())
            {
                var data = new DataSet();

                for (int i = 0, sheetsCount = this.SheetsCount.Value; i < sheetsCount; ++i)
                {
                    var table = data.Tables.Add(string.Format(CultureInfo.InvariantCulture, "Sheet {0}", i + 1));
                    int columnsCount = this.ColumnsCount.Value;
                    for (int j = 0; j < columnsCount; ++j)
                    {
                        _ = table.Columns.Add( string.Format( CultureInfo.InvariantCulture, "Column {0}", j + 1 ), typeof( string ) );
                    }

                    for (int j = 0, rowsCount = this.RowsCount.Value; j < rowsCount; ++j)
                    {
                        _ = table.Rows.Add( new string[columnsCount] );
                    }
                }

                this.Data = data;
                this.SelectedDataTableIndex = 0;

                this.WriteCommand.InvalidateCanExecuteChanged();
            }
        }

        private bool CanRead()
        {
            return File.Exists(this.InputPath);
        }

        private void Read()
        {
            if (this.CanCreate())
            {
                this.Data = SpreadsheetScribe.ReadOdsFile(this.InputPath);
                this.SelectedDataTableIndex = 0;
            }
        }

        private bool CanWrite()
        {
            return this.Data != null && DirectoryExists(this.OutputPath);
        }

        private void Write()
        {

            System.Windows.Input.Cursor previousCursor = System.Windows.Input.Mouse.OverrideCursor;
            try
            {
                System.Windows.Input.Mouse.OverrideCursor = System.Windows.Input.Cursors.Wait;
                if (this.CanWrite())
                {
                    SpreadsheetScribe.WriteOdsFile(this.Data, this.OutputPath);
                    if (this.OpenCreatedFile)
                    {
                        _ = Process.Start( this.OutputPath );
                    }
                }
            }
            finally
            {
                System.Windows.Input.Mouse.OverrideCursor = previousCursor;
            }
        }

        private void ShowOpenFileDialog()
        {
            var openFileDialog = new OpenFileDialog()
            {
                FileName = this.InputPath,
                DefaultExt = "*.ods",
                Filter = "Open Document Spreadsheet (.ods)|*.ods"
            };

            if (openFileDialog.ShowDialog() == true)
            {
                this.InputPath = openFileDialog.FileName;
            }
        }

        private void ShowSaveFileDialog()
        {
            var saveFileDialog = new SaveFileDialog()
            {
                FileName = this.OutputPath,
                DefaultExt = "*.ods",
                Filter = "Open Document Spreadsheet (.ods)|*.ods"
            };

            if (saveFileDialog.ShowDialog() == true)
            {
                this.OutputPath = saveFileDialog.FileName;
            }
        }
    }
}
