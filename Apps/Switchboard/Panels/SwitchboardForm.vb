Imports isr.Core.SplitExtensions

Public Class SwitchboardForm

#Region " METHODS  AND  PROPERTIES "

    ''' <summary>Returns True if an instance of the class was created and not disposed.</summary>
    <CodeAnalysis.SuppressMessage("Code Quality", "IDE0051:Remove unused private members", Justification:="<Pending>")>
    Friend Shared ReadOnly Property Instantiated() As Boolean
        ' Returns true if an instance of the class was created and not disposed
        Get
            Return My.Application.OpenForms.Count > 0 AndAlso
                My.Application.OpenForms.Item(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name) IsNot Nothing
        End Get
    End Property
    ''' <summary>Gets or sets the Status message.</summary>
    ''' <value>A <see cref="System.String">String</see>.</value>
    ''' <remarks>Use this property to get the Status message generated by the object.</remarks>
    Public ReadOnly Property StatusMessage() As String = String.Empty

#End Region

#Region " FORM EVENT HANDLERS "

    ''' <summary>Is true after form has completed loading so that we can disable check boxes 
    '''   that should not fire until the form is fully loaded.</summary>
    ''' <remarks>Initialization was removed per performance rule CA1805 as it is done 
    '''   automatically by the runtime.</remarks>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Code Quality", "IDE0052:Remove unread private members", Justification:="<Pending>")>
    Private _Loaded As Boolean

    ''' <summary>Occurs when the form is loaded.</summary>
    ''' <param name="sender"><see cref="System.Object"/> instance of this 
    '''   <see cref="System.Windows.Forms.Form"/></param>
    ''' <param name="e"><see cref="System.EventArgs"/></param>
    ''' <remarks>Use this method for doing any final initialization right before 
    '''   the form is shown.  This is a good place to change the Visible and
    '''   ShowInTaskbar properties to start the form as hidden.  
    '''   Starting a form as hidden is useful for forms that need to be running but that
    '''   should not show themselves right away, such as forms with a notify icon in the
    '''   task bar.</remarks>
    Private Sub Form_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        Try

            ' Turn on the form hourglass cursor
            Me.Cursor = System.Windows.Forms.Cursors.WaitCursor

            ' instantiate form objects
            Me.InstantiateObjects()

            ' set the form caption
            Me.Text = My.Application.Info.ProductName & ": SWITCH BOARD " & My.Application.Info.Version.ToString

            ' center the form
            Me.CenterToScreen()

            ' turn on the loaded flag
            Me._Loaded = True

        Catch

            ' Use throw without an argument in order to preserve the stack location 
            ' where the exception was initially raised.
            Throw

        Finally

            Me.Cursor = System.Windows.Forms.Cursors.Default

        End Try

    End Sub

#End Region

#Region " PRIVATE  and  PROTECTED "

    ''' <summary>Enumerates the action options.</summary>
    Private Enum ActionOption
        DelimitedFileTestPanel
        SignaturesTestPanel
    End Enum

    ''' <summary> Initializes the class objects. </summary>
    ''' <remarks> Called from the form load method to instantiate module-level objects. </remarks>
    Private Sub InstantiateObjects()

        ' populate the action list
        Me.populateActiveList()

        Me._DialogsComboBox.Visible = True

    End Sub

    ''' <summary> Populates the list of options in the action combo box. </summary>
    ''' <remarks> It seems that out enumerated list does not work very well with this list. </remarks>
    Private Sub PopulateActiveList()

        ' set the action list
        Dim itemCaption As String
        Me._DialogsComboBox.Items.Clear()
        itemCaption = ActionOption.DelimitedFileTestPanel.ToString
        Me._DialogsComboBox.Items.Add(itemCaption.SplitWords())
        itemCaption = ActionOption.SignaturesTestPanel.ToString
        Me._DialogsComboBox.Items.Add(itemCaption.SplitWords())
    End Sub

#End Region

#Region " CONTROL EVENT HANDLERS "

    ''' <summary> Closes the form and exits the application. </summary>
    ''' <param name="sender"> <see cref="System.Object"/> instance of this
    ''' <see cref="System.Windows.Forms.Form"/> </param>
    ''' <param name="e">      Event information. </param>
    Private Sub ExitButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _ExitButton.Click
        Me.Close()
    End Sub

    ''' <summary> Open selected items. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub OpenButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _OpenButton.Click
        Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
        Select Case Me._DialogsComboBox.Text
            Case ActionOption.DelimitedFileTestPanel.ToString.SplitWords()
                Using testPanel As New Delimited.TestPanel
                    testPanel.ShowDialog()
                End Using
            Case ActionOption.SignaturesTestPanel.ToString.SplitWords()
                Using testpanel As New Cryptography.TestPanel
                    testpanel.ShowDialog()
                End Using

            Case Else
        End Select
        Me.Cursor = System.Windows.Forms.Cursors.Default
    End Sub

#End Region

End Class
