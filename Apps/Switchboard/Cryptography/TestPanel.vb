Imports isr.Core.HashExtensions

Namespace Cryptography

    ''' <summary> Includes code for form TestPanel. </summary>
    ''' <remarks> Launch this form by calling its Show or ShowDialog method from its default instance. <para>
    ''' (c) 2003 Integrated Scientific Resources, Inc. All rights reserved. </para><para>
    ''' Licensed under The MIT License.</para><para>
    ''' David, 2003-06-17, 1.0.1263.x. </para></remarks>
    Friend Class TestPanel
        Inherits System.Windows.Forms.Form

#Region " CONSTRUCTURES "

        Public Sub New()
            MyBase.New()

            ' This method is required by the Windows Form Designer.
            Me.InitializeComponent()

        End Sub

        'Form overrides dispose to clean up the component list.
        Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
            Try

                If disposing Then
                    If Me._Components IsNot Nothing Then
                        Me._Components.Dispose()
                    End If
                End If
            Finally

                ' Invoke the base class dispose method
                MyBase.Dispose(disposing)

            End Try
        End Sub

#End Region

#Region " Windows Form Designer generated code "

        'Required by the Windows Form Designer
        Private ReadOnly _Components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Windows Form Designer
        'It can be modified using the Windows Form Designer.  
        'Do not modify it using the code editor.
        Private WithEvents XmlFilePathNameTextBoxLabel As System.Windows.Forms.Label
        Private WithEvents SignatureFullFileNameTextBoxLabel As System.Windows.Forms.Label
        Private WithEvents SignAndSaveButton As System.Windows.Forms.Button
        Private WithEvents ReadAndVerifyButton As System.Windows.Forms.Button
        Private WithEvents XmlSignDataTextBox As System.Windows.Forms.TextBox
        Private WithEvents SignatureCaptionLabel As System.Windows.Forms.Label
        Private WithEvents SignatureTextBox As System.Windows.Forms.TextBox
        Private WithEvents XmlFilePathNameTextBox As System.Windows.Forms.TextBox
        Private WithEvents SignatureFullFileNameTextBox As System.Windows.Forms.TextBox
        Private WithEvents SignatureDateCaptionLabel As System.Windows.Forms.Label
        Private WithEvents OutcomeLabel As System.Windows.Forms.Label
        Private WithEvents Tabs As System.Windows.Forms.TabControl
        Private WithEvents XmlSignatureFileTabPage As System.Windows.Forms.TabPage
        Private WithEvents TokenSignatureTabPage As System.Windows.Forms.TabPage
        Private WithEvents VerifySignatureFileButton As System.Windows.Forms.Button
        Private WithEvents CreateSignatureFileButton As System.Windows.Forms.Button
        Private WithEvents ShowMeButton As System.Windows.Forms.Button
        Private WithEvents MakeTokenSignatureTextBox As System.Windows.Forms.TextBox
        Private WithEvents Button1 As Button
        Private WithEvents BrowseXmlDataFileButton As Button
        Private WithEvents BrowseXmlTokenSignatureFileButton As Button
        Private WithEvents XmlTokenSignatureFullFileNameTextBoxLabel As Label
        Private WithEvents XmlTokenSignatureFullFileNameTextBox As TextBox
        Private WithEvents LicenseSignRadioButton As RadioButton
        Private WithEvents SystemSignRadioButton As RadioButton
        Private WithEvents KeyTabPage As TabPage
        Private WithEvents LoadKeyButton As Button
        Private WithEvents KeyFullFileNameTextBox As TextBox
        Private WithEvents KeyManagemenrOutcomeLabel As Label
        Private WithEvents KeyFullFileNameTextBoxLabel As Label
        Private WithEvents SignedXmlFullFileNameTextBoxLabel As Label
        Private WithEvents SignedXmlFullFileNameTextBox As TextBox
        Private WithEvents SaveNewKeyButton As Button
        Private WithEvents ReadKeyInfoButton As Button
        Private WithEvents KeyInfoOuterXmlTextBox As TextBox
        Private WithEvents KeyInfoOuterXmlTextBoxLabel As Label
        Private WithEvents KeyInfoHashTextBoxLabel As Label
        Private WithEvents KeyInfoHashTextBox As TextBox
        Private WithEvents ReadTokenSignatureTextBox As TextBox
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
            Me.XmlFilePathNameTextBox = New System.Windows.Forms.TextBox()
            Me.XmlFilePathNameTextBoxLabel = New System.Windows.Forms.Label()
            Me.SignatureFullFileNameTextBoxLabel = New System.Windows.Forms.Label()
            Me.SignatureFullFileNameTextBox = New System.Windows.Forms.TextBox()
            Me.SignAndSaveButton = New System.Windows.Forms.Button()
            Me.ReadAndVerifyButton = New System.Windows.Forms.Button()
            Me.XmlSignDataTextBox = New System.Windows.Forms.TextBox()
            Me.SignatureDateCaptionLabel = New System.Windows.Forms.Label()
            Me.SignatureCaptionLabel = New System.Windows.Forms.Label()
            Me.SignatureTextBox = New System.Windows.Forms.TextBox()
            Me.OutcomeLabel = New System.Windows.Forms.Label()
            Me.Tabs = New System.Windows.Forms.TabControl()
            Me.KeyTabPage = New System.Windows.Forms.TabPage()
            Me.LoadKeyButton = New System.Windows.Forms.Button()
            Me.KeyFullFileNameTextBox = New System.Windows.Forms.TextBox()
            Me.KeyManagemenrOutcomeLabel = New System.Windows.Forms.Label()
            Me.KeyFullFileNameTextBoxLabel = New System.Windows.Forms.Label()
            Me.SignedXmlFullFileNameTextBoxLabel = New System.Windows.Forms.Label()
            Me.SignedXmlFullFileNameTextBox = New System.Windows.Forms.TextBox()
            Me.SaveNewKeyButton = New System.Windows.Forms.Button()
            Me.ReadKeyInfoButton = New System.Windows.Forms.Button()
            Me.KeyInfoOuterXmlTextBox = New System.Windows.Forms.TextBox()
            Me.KeyInfoOuterXmlTextBoxLabel = New System.Windows.Forms.Label()
            Me.KeyInfoHashTextBoxLabel = New System.Windows.Forms.Label()
            Me.KeyInfoHashTextBox = New System.Windows.Forms.TextBox()
            Me.XmlSignatureFileTabPage = New System.Windows.Forms.TabPage()
            Me.Button1 = New System.Windows.Forms.Button()
            Me.BrowseXmlDataFileButton = New System.Windows.Forms.Button()
            Me.TokenSignatureTabPage = New System.Windows.Forms.TabPage()
            Me.LicenseSignRadioButton = New System.Windows.Forms.RadioButton()
            Me.SystemSignRadioButton = New System.Windows.Forms.RadioButton()
            Me.BrowseXmlTokenSignatureFileButton = New System.Windows.Forms.Button()
            Me.XmlTokenSignatureFullFileNameTextBoxLabel = New System.Windows.Forms.Label()
            Me.XmlTokenSignatureFullFileNameTextBox = New System.Windows.Forms.TextBox()
            Me.VerifySignatureFileButton = New System.Windows.Forms.Button()
            Me.CreateSignatureFileButton = New System.Windows.Forms.Button()
            Me.ShowMeButton = New System.Windows.Forms.Button()
            Me.ReadTokenSignatureTextBox = New System.Windows.Forms.TextBox()
            Me.MakeTokenSignatureTextBox = New System.Windows.Forms.TextBox()
            Me.Tabs.SuspendLayout()
            Me.KeyTabPage.SuspendLayout()
            Me.XmlSignatureFileTabPage.SuspendLayout()
            Me.TokenSignatureTabPage.SuspendLayout()
            Me.SuspendLayout()
            '
            '_XmlFilePathNameTextBox
            '
            Me.XmlFilePathNameTextBox.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.XmlFilePathNameTextBox.Location = New System.Drawing.Point(11, 38)
            Me.XmlFilePathNameTextBox.Name = "_XmlFilePathNameTextBox"
            Me.XmlFilePathNameTextBox.Size = New System.Drawing.Size(456, 20)
            Me.XmlFilePathNameTextBox.TabIndex = 0
            Me.XmlFilePathNameTextBox.Text = "Credit Card Info.xml"
            '
            '_XmlFilePathNameTextBoxLabel
            '
            Me.XmlFilePathNameTextBoxLabel.AutoSize = True
            Me.XmlFilePathNameTextBoxLabel.Location = New System.Drawing.Point(8, 22)
            Me.XmlFilePathNameTextBoxLabel.Name = "_XmlFilePathNameTextBoxLabel"
            Me.XmlFilePathNameTextBoxLabel.Size = New System.Drawing.Size(261, 13)
            Me.XmlFilePathNameTextBoxLabel.TabIndex = 1
            Me.XmlFilePathNameTextBoxLabel.Text = "File including data to sign (also used as resource id):   "
            Me.XmlFilePathNameTextBoxLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
            '
            '_SignatureFullFileNameTextBoxLabel
            '
            Me.SignatureFullFileNameTextBoxLabel.AutoSize = True
            Me.SignatureFullFileNameTextBoxLabel.Location = New System.Drawing.Point(8, 70)
            Me.SignatureFullFileNameTextBoxLabel.Name = "_SignatureFullFileNameTextBoxLabel"
            Me.SignatureFullFileNameTextBoxLabel.Size = New System.Drawing.Size(152, 13)
            Me.SignatureFullFileNameTextBoxLabel.TabIndex = 3
            Me.SignatureFullFileNameTextBoxLabel.Text = "XML Signature Full File Name: "
            Me.SignatureFullFileNameTextBoxLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
            '
            '_SignatureFullFileNameTextBox
            '
            Me.SignatureFullFileNameTextBox.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.SignatureFullFileNameTextBox.Location = New System.Drawing.Point(11, 86)
            Me.SignatureFullFileNameTextBox.Name = "_SignatureFullFileNameTextBox"
            Me.SignatureFullFileNameTextBox.Size = New System.Drawing.Size(455, 20)
            Me.SignatureFullFileNameTextBox.TabIndex = 2
            Me.SignatureFullFileNameTextBox.Text = "signature.xml"
            '
            'signAndSaveButton
            '
            Me.SignAndSaveButton.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.SignAndSaveButton.Location = New System.Drawing.Point(373, 12)
            Me.SignAndSaveButton.Name = "signAndSaveButton"
            Me.SignAndSaveButton.Size = New System.Drawing.Size(94, 26)
            Me.SignAndSaveButton.TabIndex = 4
            Me.SignAndSaveButton.Text = "Sign and Save"
            '
            'readAndVerifyButton
            '
            Me.ReadAndVerifyButton.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.ReadAndVerifyButton.Location = New System.Drawing.Point(373, 60)
            Me.ReadAndVerifyButton.Name = "readAndVerifyButton"
            Me.ReadAndVerifyButton.Size = New System.Drawing.Size(94, 26)
            Me.ReadAndVerifyButton.TabIndex = 5
            Me.ReadAndVerifyButton.Text = "Read and Verify"
            '
            'xmlSignDataTextBox
            '
            Me.XmlSignDataTextBox.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.XmlSignDataTextBox.Location = New System.Drawing.Point(11, 142)
            Me.XmlSignDataTextBox.Multiline = True
            Me.XmlSignDataTextBox.Name = "xmlSignDataTextBox"
            Me.XmlSignDataTextBox.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
            Me.XmlSignDataTextBox.Size = New System.Drawing.Size(456, 100)
            Me.XmlSignDataTextBox.TabIndex = 6
            Me.XmlSignDataTextBox.Text = "This data will be signed"
            '
            'signatureDateCaptionLabel
            '
            Me.SignatureDateCaptionLabel.AutoSize = True
            Me.SignatureDateCaptionLabel.Location = New System.Drawing.Point(8, 126)
            Me.SignatureDateCaptionLabel.Name = "signatureDateCaptionLabel"
            Me.SignatureDateCaptionLabel.Size = New System.Drawing.Size(109, 13)
            Me.SignatureDateCaptionLabel.TabIndex = 7
            Me.SignatureDateCaptionLabel.Text = "XML Signature Data: "
            Me.SignatureDateCaptionLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
            '
            'signatureCaptionLabel
            '
            Me.SignatureCaptionLabel.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.SignatureCaptionLabel.AutoSize = True
            Me.SignatureCaptionLabel.Location = New System.Drawing.Point(11, 246)
            Me.SignatureCaptionLabel.Name = "signatureCaptionLabel"
            Me.SignatureCaptionLabel.Size = New System.Drawing.Size(58, 13)
            Me.SignatureCaptionLabel.TabIndex = 11
            Me.SignatureCaptionLabel.Text = "Signature: "
            Me.SignatureCaptionLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
            '
            'signatureTextBox
            '
            Me.SignatureTextBox.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.SignatureTextBox.Location = New System.Drawing.Point(11, 262)
            Me.SignatureTextBox.Multiline = True
            Me.SignatureTextBox.Name = "signatureTextBox"
            Me.SignatureTextBox.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
            Me.SignatureTextBox.Size = New System.Drawing.Size(456, 136)
            Me.SignatureTextBox.TabIndex = 10
            Me.SignatureTextBox.Text = "where the signature goes"
            '
            'outcomeLabel
            '
            Me.OutcomeLabel.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.OutcomeLabel.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
            Me.OutcomeLabel.Location = New System.Drawing.Point(12, 406)
            Me.OutcomeLabel.Name = "outcomeLabel"
            Me.OutcomeLabel.Size = New System.Drawing.Size(452, 16)
            Me.OutcomeLabel.TabIndex = 13
            Me.OutcomeLabel.Text = "Outcome:  "
            '
            '_Tabs
            '
            Me.Tabs.Controls.Add(Me.KeyTabPage)
            Me.Tabs.Controls.Add(Me.XmlSignatureFileTabPage)
            Me.Tabs.Controls.Add(Me.TokenSignatureTabPage)
            Me.Tabs.Dock = System.Windows.Forms.DockStyle.Fill
            Me.Tabs.Location = New System.Drawing.Point(0, 0)
            Me.Tabs.Name = "_Tabs"
            Me.Tabs.SelectedIndex = 0
            Me.Tabs.Size = New System.Drawing.Size(487, 470)
            Me.Tabs.TabIndex = 14
            '
            '_KeyTabPage
            '
            Me.KeyTabPage.Controls.Add(Me.LoadKeyButton)
            Me.KeyTabPage.Controls.Add(Me.KeyFullFileNameTextBox)
            Me.KeyTabPage.Controls.Add(Me.KeyManagemenrOutcomeLabel)
            Me.KeyTabPage.Controls.Add(Me.KeyFullFileNameTextBoxLabel)
            Me.KeyTabPage.Controls.Add(Me.SignedXmlFullFileNameTextBoxLabel)
            Me.KeyTabPage.Controls.Add(Me.SignedXmlFullFileNameTextBox)
            Me.KeyTabPage.Controls.Add(Me.SaveNewKeyButton)
            Me.KeyTabPage.Controls.Add(Me.ReadKeyInfoButton)
            Me.KeyTabPage.Controls.Add(Me.KeyInfoOuterXmlTextBox)
            Me.KeyTabPage.Controls.Add(Me.KeyInfoOuterXmlTextBoxLabel)
            Me.KeyTabPage.Controls.Add(Me.KeyInfoHashTextBoxLabel)
            Me.KeyTabPage.Controls.Add(Me.KeyInfoHashTextBox)
            Me.KeyTabPage.Location = New System.Drawing.Point(4, 22)
            Me.KeyTabPage.Name = "_KeyTabPage"
            Me.KeyTabPage.Size = New System.Drawing.Size(479, 444)
            Me.KeyTabPage.TabIndex = 2
            Me.KeyTabPage.Text = "KEY"
            Me.KeyTabPage.UseVisualStyleBackColor = True
            '
            '_LoadKeyButton
            '
            Me.LoadKeyButton.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.LoadKeyButton.Location = New System.Drawing.Point(264, 10)
            Me.LoadKeyButton.Name = "_LoadKeyButton"
            Me.LoadKeyButton.Size = New System.Drawing.Size(94, 26)
            Me.LoadKeyButton.TabIndex = 27
            Me.LoadKeyButton.Text = "Load Key"
            Me.LoadKeyButton.UseVisualStyleBackColor = True
            '
            '_KeyFullFileNameTextBox
            '
            Me.KeyFullFileNameTextBox.Location = New System.Drawing.Point(11, 36)
            Me.KeyFullFileNameTextBox.Name = "_KeyFullFileNameTextBox"
            Me.KeyFullFileNameTextBox.Size = New System.Drawing.Size(454, 20)
            Me.KeyFullFileNameTextBox.TabIndex = 15
            Me.KeyFullFileNameTextBox.Text = "C:\My\LIBRARIES\VS\IO\IO\Library\MY\RsaSigningKey.xml"
            '
            '_KeyManagemenrOutcomeLabel
            '
            Me.KeyManagemenrOutcomeLabel.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
            Me.KeyManagemenrOutcomeLabel.Location = New System.Drawing.Point(11, 411)
            Me.KeyManagemenrOutcomeLabel.Name = "_KeyManagemenrOutcomeLabel"
            Me.KeyManagemenrOutcomeLabel.Size = New System.Drawing.Size(454, 16)
            Me.KeyManagemenrOutcomeLabel.TabIndex = 25
            Me.KeyManagemenrOutcomeLabel.Text = "Outcome:  "
            '
            '_KeyFullFileNameTextBoxLabel
            '
            Me.KeyFullFileNameTextBoxLabel.AutoSize = True
            Me.KeyFullFileNameTextBoxLabel.Location = New System.Drawing.Point(11, 20)
            Me.KeyFullFileNameTextBoxLabel.Name = "_KeyFullFileNameTextBoxLabel"
            Me.KeyFullFileNameTextBoxLabel.Size = New System.Drawing.Size(47, 13)
            Me.KeyFullFileNameTextBoxLabel.TabIndex = 16
            Me.KeyFullFileNameTextBoxLabel.Text = "Key File:"
            Me.KeyFullFileNameTextBoxLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
            '
            '_SignedXmlFullFileNameTextBoxLabel
            '
            Me.SignedXmlFullFileNameTextBoxLabel.Location = New System.Drawing.Point(11, 73)
            Me.SignedXmlFullFileNameTextBoxLabel.Name = "_SignedXmlFullFileNameTextBoxLabel"
            Me.SignedXmlFullFileNameTextBoxLabel.Size = New System.Drawing.Size(138, 16)
            Me.SignedXmlFullFileNameTextBoxLabel.TabIndex = 18
            Me.SignedXmlFullFileNameTextBoxLabel.Text = "XML Signature Full File Name: "
            Me.SignedXmlFullFileNameTextBoxLabel.TextAlign = System.Drawing.ContentAlignment.BottomLeft
            '
            '_SignedXmlFullFileNameTextBox
            '
            Me.SignedXmlFullFileNameTextBox.Location = New System.Drawing.Point(11, 91)
            Me.SignedXmlFullFileNameTextBox.Name = "_SignedXmlFullFileNameTextBox"
            Me.SignedXmlFullFileNameTextBox.Size = New System.Drawing.Size(454, 20)
            Me.SignedXmlFullFileNameTextBox.TabIndex = 17
            Me.SignedXmlFullFileNameTextBox.Text = "C:\My\LIBRARIES\VS\IO\IO\Apps\Switchboard\bin\debug\IO.Switchboard.exe.permit"
            '
            '_SaveNewKeyButton
            '
            Me.SaveNewKeyButton.Location = New System.Drawing.Point(371, 10)
            Me.SaveNewKeyButton.Name = "_SaveNewKeyButton"
            Me.SaveNewKeyButton.Size = New System.Drawing.Size(94, 26)
            Me.SaveNewKeyButton.TabIndex = 19
            Me.SaveNewKeyButton.Text = "Save New Key"
            '
            '_ReadKeyInfoButton
            '
            Me.ReadKeyInfoButton.Location = New System.Drawing.Point(371, 65)
            Me.ReadKeyInfoButton.Name = "_ReadKeyInfoButton"
            Me.ReadKeyInfoButton.Size = New System.Drawing.Size(94, 26)
            Me.ReadKeyInfoButton.TabIndex = 20
            Me.ReadKeyInfoButton.Text = "Read Key Info"
            '
            '_KeyInfoOuterXmlTextBox
            '
            Me.KeyInfoOuterXmlTextBox.Location = New System.Drawing.Point(11, 147)
            Me.KeyInfoOuterXmlTextBox.Multiline = True
            Me.KeyInfoOuterXmlTextBox.Name = "_KeyInfoOuterXmlTextBox"
            Me.KeyInfoOuterXmlTextBox.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
            Me.KeyInfoOuterXmlTextBox.Size = New System.Drawing.Size(454, 177)
            Me.KeyInfoOuterXmlTextBox.TabIndex = 21
            Me.KeyInfoOuterXmlTextBox.Text = "where the key info xml goes"
            '
            '_KeyInfoOuterXmlTextBoxLabel
            '
            Me.KeyInfoOuterXmlTextBoxLabel.AutoSize = True
            Me.KeyInfoOuterXmlTextBoxLabel.Location = New System.Drawing.Point(11, 131)
            Me.KeyInfoOuterXmlTextBoxLabel.Name = "_KeyInfoOuterXmlTextBoxLabel"
            Me.KeyInfoOuterXmlTextBoxLabel.Size = New System.Drawing.Size(107, 13)
            Me.KeyInfoOuterXmlTextBoxLabel.TabIndex = 22
            Me.KeyInfoOuterXmlTextBoxLabel.Text = "XML Info Outer XML:"
            Me.KeyInfoOuterXmlTextBoxLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
            '
            '_KeyInfoHashTextBoxLabel
            '
            Me.KeyInfoHashTextBoxLabel.AutoSize = True
            Me.KeyInfoHashTextBoxLabel.Location = New System.Drawing.Point(11, 335)
            Me.KeyInfoHashTextBoxLabel.Name = "_KeyInfoHashTextBoxLabel"
            Me.KeyInfoHashTextBoxLabel.Size = New System.Drawing.Size(80, 13)
            Me.KeyInfoHashTextBoxLabel.TabIndex = 24
            Me.KeyInfoHashTextBoxLabel.Text = "Key Info Hash: "
            Me.KeyInfoHashTextBoxLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
            '
            '_KeyInfoHashTextBox
            '
            Me.KeyInfoHashTextBox.Location = New System.Drawing.Point(11, 351)
            Me.KeyInfoHashTextBox.Multiline = True
            Me.KeyInfoHashTextBox.Name = "_KeyInfoHashTextBox"
            Me.KeyInfoHashTextBox.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
            Me.KeyInfoHashTextBox.Size = New System.Drawing.Size(454, 46)
            Me.KeyInfoHashTextBox.TabIndex = 23
            Me.KeyInfoHashTextBox.Text = "where the key info hash goes"
            '
            '_XmlSignatureFileTabPage
            '
            Me.XmlSignatureFileTabPage.Controls.Add(Me.Button1)
            Me.XmlSignatureFileTabPage.Controls.Add(Me.BrowseXmlDataFileButton)
            Me.XmlSignatureFileTabPage.Controls.Add(Me.XmlFilePathNameTextBox)
            Me.XmlSignatureFileTabPage.Controls.Add(Me.OutcomeLabel)
            Me.XmlSignatureFileTabPage.Controls.Add(Me.XmlFilePathNameTextBoxLabel)
            Me.XmlSignatureFileTabPage.Controls.Add(Me.SignatureFullFileNameTextBoxLabel)
            Me.XmlSignatureFileTabPage.Controls.Add(Me.SignatureFullFileNameTextBox)
            Me.XmlSignatureFileTabPage.Controls.Add(Me.SignAndSaveButton)
            Me.XmlSignatureFileTabPage.Controls.Add(Me.ReadAndVerifyButton)
            Me.XmlSignatureFileTabPage.Controls.Add(Me.XmlSignDataTextBox)
            Me.XmlSignatureFileTabPage.Controls.Add(Me.SignatureDateCaptionLabel)
            Me.XmlSignatureFileTabPage.Controls.Add(Me.SignatureCaptionLabel)
            Me.XmlSignatureFileTabPage.Controls.Add(Me.SignatureTextBox)
            Me.XmlSignatureFileTabPage.Location = New System.Drawing.Point(4, 22)
            Me.XmlSignatureFileTabPage.Name = "_XmlSignatureFileTabPage"
            Me.XmlSignatureFileTabPage.Size = New System.Drawing.Size(479, 444)
            Me.XmlSignatureFileTabPage.TabIndex = 0
            Me.XmlSignatureFileTabPage.Text = "XML Signature"
            '
            'Button1
            '
            Me.Button1.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.Button1.Font = New System.Drawing.Font("Wingdings", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(2, Byte))
            Me.Button1.Location = New System.Drawing.Point(341, 65)
            Me.Button1.Name = "Button1"
            Me.Button1.Size = New System.Drawing.Size(31, 21)
            Me.Button1.TabIndex = 14
            Me.Button1.Text = "sss"
            Me.Button1.TextAlign = System.Drawing.ContentAlignment.BottomCenter
            Me.Button1.UseVisualStyleBackColor = True
            '
            '_BrowseXmlDataFileButton
            '
            Me.BrowseXmlDataFileButton.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.BrowseXmlDataFileButton.Font = New System.Drawing.Font("Wingdings", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(2, Byte))
            Me.BrowseXmlDataFileButton.Location = New System.Drawing.Point(341, 16)
            Me.BrowseXmlDataFileButton.Name = "_BrowseXmlDataFileButton"
            Me.BrowseXmlDataFileButton.Size = New System.Drawing.Size(31, 21)
            Me.BrowseXmlDataFileButton.TabIndex = 14
            Me.BrowseXmlDataFileButton.Text = "sss"
            Me.BrowseXmlDataFileButton.TextAlign = System.Drawing.ContentAlignment.BottomCenter
            Me.BrowseXmlDataFileButton.UseVisualStyleBackColor = True
            '
            '_TokenSignatureTabPage
            '
            Me.TokenSignatureTabPage.Controls.Add(Me.LicenseSignRadioButton)
            Me.TokenSignatureTabPage.Controls.Add(Me.SystemSignRadioButton)
            Me.TokenSignatureTabPage.Controls.Add(Me.BrowseXmlTokenSignatureFileButton)
            Me.TokenSignatureTabPage.Controls.Add(Me.XmlTokenSignatureFullFileNameTextBoxLabel)
            Me.TokenSignatureTabPage.Controls.Add(Me.XmlTokenSignatureFullFileNameTextBox)
            Me.TokenSignatureTabPage.Controls.Add(Me.VerifySignatureFileButton)
            Me.TokenSignatureTabPage.Controls.Add(Me.CreateSignatureFileButton)
            Me.TokenSignatureTabPage.Controls.Add(Me.ShowMeButton)
            Me.TokenSignatureTabPage.Controls.Add(Me.ReadTokenSignatureTextBox)
            Me.TokenSignatureTabPage.Controls.Add(Me.MakeTokenSignatureTextBox)
            Me.TokenSignatureTabPage.Location = New System.Drawing.Point(4, 22)
            Me.TokenSignatureTabPage.Name = "_TokenSignatureTabPage"
            Me.TokenSignatureTabPage.Size = New System.Drawing.Size(479, 444)
            Me.TokenSignatureTabPage.TabIndex = 1
            Me.TokenSignatureTabPage.Text = "Token Signatures"
            '
            '_LicenseSignRadioButton
            '
            Me.LicenseSignRadioButton.AutoSize = True
            Me.LicenseSignRadioButton.Checked = True
            Me.LicenseSignRadioButton.Location = New System.Drawing.Point(379, 388)
            Me.LicenseSignRadioButton.Name = "_LicenseSignRadioButton"
            Me.LicenseSignRadioButton.Size = New System.Drawing.Size(86, 17)
            Me.LicenseSignRadioButton.TabIndex = 18
            Me.LicenseSignRadioButton.TabStop = True
            Me.LicenseSignRadioButton.Text = "License Sign"
            Me.LicenseSignRadioButton.UseVisualStyleBackColor = True
            '
            '_SystemSignRadioButton
            '
            Me.SystemSignRadioButton.AutoSize = True
            Me.SystemSignRadioButton.Location = New System.Drawing.Point(379, 411)
            Me.SystemSignRadioButton.Name = "_SystemSignRadioButton"
            Me.SystemSignRadioButton.Size = New System.Drawing.Size(83, 17)
            Me.SystemSignRadioButton.TabIndex = 18
            Me.SystemSignRadioButton.Text = "System Sign"
            Me.SystemSignRadioButton.UseVisualStyleBackColor = True
            '
            '_BrowseXmlTokenSignatureFileButton
            '
            Me.BrowseXmlTokenSignatureFileButton.Font = New System.Drawing.Font("Wingdings", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(2, Byte))
            Me.BrowseXmlTokenSignatureFileButton.Location = New System.Drawing.Point(498, 13)
            Me.BrowseXmlTokenSignatureFileButton.Name = "_BrowseXmlTokenSignatureFileButton"
            Me.BrowseXmlTokenSignatureFileButton.Size = New System.Drawing.Size(31, 21)
            Me.BrowseXmlTokenSignatureFileButton.TabIndex = 17
            Me.BrowseXmlTokenSignatureFileButton.Text = "sss"
            Me.BrowseXmlTokenSignatureFileButton.TextAlign = System.Drawing.ContentAlignment.BottomCenter
            Me.BrowseXmlTokenSignatureFileButton.UseVisualStyleBackColor = True
            '
            '_XmlTokenSignatureFullFileNameTextBoxLabel
            '
            Me.XmlTokenSignatureFullFileNameTextBoxLabel.Location = New System.Drawing.Point(10, 18)
            Me.XmlTokenSignatureFullFileNameTextBoxLabel.Name = "_XmlTokenSignatureFullFileNameTextBoxLabel"
            Me.XmlTokenSignatureFullFileNameTextBoxLabel.Size = New System.Drawing.Size(138, 16)
            Me.XmlTokenSignatureFullFileNameTextBoxLabel.TabIndex = 16
            Me.XmlTokenSignatureFullFileNameTextBoxLabel.Text = "XML Signature Full File Name: "
            Me.XmlTokenSignatureFullFileNameTextBoxLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
            '
            '_XmlTokenSignatureFullFileNameTextBox
            '
            Me.XmlTokenSignatureFullFileNameTextBox.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.XmlTokenSignatureFullFileNameTextBox.Location = New System.Drawing.Point(8, 34)
            Me.XmlTokenSignatureFullFileNameTextBox.Name = "_XmlTokenSignatureFullFileNameTextBox"
            Me.XmlTokenSignatureFullFileNameTextBox.Size = New System.Drawing.Size(456, 20)
            Me.XmlTokenSignatureFullFileNameTextBox.TabIndex = 15
            Me.XmlTokenSignatureFullFileNameTextBox.Text = "signature.xml"
            '
            '_VerifySignatureFileButton
            '
            Me.VerifySignatureFileButton.Location = New System.Drawing.Point(247, 408)
            Me.VerifySignatureFileButton.Name = "_VerifySignatureFileButton"
            Me.VerifySignatureFileButton.Size = New System.Drawing.Size(117, 23)
            Me.VerifySignatureFileButton.TabIndex = 10
            Me.VerifySignatureFileButton.Text = "Verify Signature File"
            '
            '_CreateSignatureFileButton
            '
            Me.CreateSignatureFileButton.Location = New System.Drawing.Point(94, 408)
            Me.CreateSignatureFileButton.Name = "_CreateSignatureFileButton"
            Me.CreateSignatureFileButton.Size = New System.Drawing.Size(142, 23)
            Me.CreateSignatureFileButton.TabIndex = 9
            Me.CreateSignatureFileButton.Text = "Create Signature File"
            '
            'showMeButton
            '
            Me.ShowMeButton.Location = New System.Drawing.Point(11, 408)
            Me.ShowMeButton.Name = "showMeButton"
            Me.ShowMeButton.Size = New System.Drawing.Size(75, 23)
            Me.ShowMeButton.TabIndex = 8
            Me.ShowMeButton.Text = "Show Me"
            '
            '_ReadTokenSignatureTextBox
            '
            Me.ReadTokenSignatureTextBox.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.ReadTokenSignatureTextBox.Location = New System.Drawing.Point(11, 222)
            Me.ReadTokenSignatureTextBox.Multiline = True
            Me.ReadTokenSignatureTextBox.Name = "_ReadTokenSignatureTextBox"
            Me.ReadTokenSignatureTextBox.ScrollBars = System.Windows.Forms.ScrollBars.Both
            Me.ReadTokenSignatureTextBox.Size = New System.Drawing.Size(457, 154)
            Me.ReadTokenSignatureTextBox.TabIndex = 7
            '
            '_MakeTokenSignatureTextBox
            '
            Me.MakeTokenSignatureTextBox.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.MakeTokenSignatureTextBox.Location = New System.Drawing.Point(10, 62)
            Me.MakeTokenSignatureTextBox.Multiline = True
            Me.MakeTokenSignatureTextBox.Name = "_MakeTokenSignatureTextBox"
            Me.MakeTokenSignatureTextBox.ScrollBars = System.Windows.Forms.ScrollBars.Both
            Me.MakeTokenSignatureTextBox.Size = New System.Drawing.Size(457, 154)
            Me.MakeTokenSignatureTextBox.TabIndex = 7
            '
            'TestPanel
            '
            Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
            Me.ClientSize = New System.Drawing.Size(487, 470)
            Me.Controls.Add(Me.Tabs)
            Me.Name = "TestPanel"
            Me.Text = "Test Panel"
            Me.Tabs.ResumeLayout(False)
            Me.KeyTabPage.ResumeLayout(False)
            Me.KeyTabPage.PerformLayout()
            Me.XmlSignatureFileTabPage.ResumeLayout(False)
            Me.XmlSignatureFileTabPage.PerformLayout()
            Me.TokenSignatureTabPage.ResumeLayout(False)
            Me.TokenSignatureTabPage.PerformLayout()
            Me.ResumeLayout(False)

        End Sub

#End Region

#Region " FORM EVENT HANDLERS "

        ''' <summary>Is true after form has completed loading so that we can disable check boxes 
        '''   that should not fire until the form is fully loaded.</summary>
        ''' <remarks>Initialization was removed per performance rule CA1805 as it is done 
        '''   automatically by the runtime.</remarks>
        <System.Diagnostics.CodeAnalysis.SuppressMessage("Code Quality", "IDE0052:Remove unread private members", Justification:="<Pending>")>
        Private _Loaded As Boolean

        ''' <summary>Occurs when the form is loaded.</summary>
        ''' <param name="sender"><see cref="System.Object"/> instance of this 
        '''   <see cref="System.Windows.Forms.Form"/></param>
        ''' <param name="e"><see cref="System.EventArgs"/></param>
        ''' <remarks>Use this method for doing any final initialization right before 
        '''   the form is shown.  This is a good place to change the Visible and
        '''   ShowInTaskbar properties to start the form as hidden.  
        '''   Starting a form as hidden is useful for forms that need to be running but that
        '''   should not show themselves right away, such as forms with a notify icon in the
        '''   task bar.</remarks>
        Private Sub Form_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

            Try

                ' Turn on the form hourglass cursor
                Me.Cursor = System.Windows.Forms.Cursors.WaitCursor

                ' set the form caption
                Me.Text = My.Application.Info.ProductName & ": CRYPTO TEST PANEL " & My.Application.Info.Version.ToString

                ' center the form
                Me.CenterToScreen()

                Me.XmlTokenSignatureFullFileNameTextBox.Text = System.IO.Path.Combine(My.Application.Info.ApplicationFolder, $"{My.Application.Info.ApplicationFileName}.permit")
                Me.SignedXmlFullFileNameTextBox.Text = System.IO.Path.Combine(My.Application.Info.ApplicationFolder, $"{My.Application.Info.ApplicationFileName}.permit")
                Me.KeyFullFileNameTextBox.Text = isr.IO.My.MySettings.Default.SigningKeyPairFullFileName
                ' turn on the loaded flag
                Me._Loaded = True

            Catch

                ' Use throw without an argument in order to preserve the stack location 
                ' where the exception was initially raised.
                Throw

            Finally

                ' Turn off the form hourglass cursor
                Me.Cursor = System.Windows.Forms.Cursors.Default

            End Try

        End Sub

        Private Sub Form_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Activated
            '   Me.notesMessageList.PushMessage("Activated")
        End Sub

#End Region

#Region " XML FILE SIGNATURE "

        ''' <summary>Occurs when the operator selects the Sign and Save button.</summary>
        ''' <remarks>Use this method to read XML data to sign, sign and save the signature and display 
        '''   the signature and data.</remarks>
        <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
        Private Sub SignAndSaveButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles SignAndSaveButton.Click

            Dim dataId As String

            Try

                ' Clear the outcome label
                Me.OutcomeLabel.Text = String.Empty
                Me.SignatureTextBox.Clear()
                Me.XmlSignDataTextBox.Clear()

                Using xmlSign As New isr.IO.Cryptography.XmlSign

                    ' open the XML signature object
                    xmlSign.Open()

                    ' add a data object
                    dataId = Me.XmlFilePathNameTextBox.Text.ToLower(Globalization.CultureInfo.CurrentCulture)
                    If xmlSign.AddResource(Me.XmlFilePathNameTextBox.Text, dataId) Then

                        ' Generate the Signature
                        xmlSign.GenerateSignature()

                        ' Verify the signature
                        If xmlSign.IsVerifySignature() Then

                            xmlSign.WriteSignature(Me.SignatureFullFileNameTextBox.Text)

                            Me.OutcomeLabel.Text = "Signature created and verified"
                            Me.SignatureTextBox.Text = xmlSign.OuterXml
                            Me.XmlSignDataTextBox.Text = xmlSign.FetchResourceDataOuterXml() '  dataId)

                        Else
                            Me.OutcomeLabel.Text = "Signature failed"
                        End If

                    Else
                        Me.OutcomeLabel.Text = "Failed adding data"
                    End If

                    ' close the XML signature object
                    xmlSign.Close()
                End Using

            Catch ex As Exception
                ex.Data.Add("@isr", "Failed sign and save")
                My.Application.Logger.WriteExceptionDetails(ex, My.MyApplication.TraceEventId)
                isr.Core.WindowsForms.ShowDialog(ex)
            End Try
        End Sub

        ''' <summary>Occurs when the operator selects the Read and Verify button.</summary>
        ''' <remarks>Use this method to read a signature file, verify and display the signature 
        '''   and data.</remarks>
        <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
        Private Sub ReadAndVerifyButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ReadAndVerifyButton.Click

            Dim dataId As String

            Try

                ' Clear the outcome label
                Me.OutcomeLabel.Text = String.Empty
                Me.SignatureTextBox.Clear()
                Me.XmlSignDataTextBox.Clear()

                Using xmlSign As New isr.IO.Cryptography.XmlSign
                    ' open the XML signature object
                    xmlSign.Open()

                    ' use the resource id from the file name
                    dataId = Me.XmlFilePathNameTextBox.Text.ToLower(Globalization.CultureInfo.CurrentCulture)

                    ' read the signature file
                    xmlSign.ReadSignature(Me.SignatureFullFileNameTextBox.Text)
                    ' Verify the signature
                    If xmlSign.IsVerifySignature() Then
                        Me.OutcomeLabel.Text = "Signature read and verified"
                        Me.SignatureTextBox.Text = xmlSign.OuterXml
                        Me.XmlSignDataTextBox.Text = xmlSign.FetchResourceDataOuterXml(dataId)
                    Else
                        Me.OutcomeLabel.Text = "Signature not verified"
                    End If

                    ' close the XML signature object
                    xmlSign.Close()
                End Using

            Catch ex As System.Exception

                ' report failure to print
                Dim userMessage As String = String.Format(Globalization.CultureInfo.CurrentCulture,
                    "Failed reading and verifying {0}{1}",
                    Environment.NewLine, ex.Message)
                System.Windows.Forms.MessageBox.Show(userMessage, Me.Name, MessageBoxButtons.OK, MessageBoxIcon.Error,
                                MessageBoxDefaultButton.Button1, MessageBoxOptions.DefaultDesktopOnly)

            End Try

        End Sub

#End Region

#Region " SYSTEM TOKEN SIGNATURE "

        <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
        Private Sub ShowResults()

            Try

                If False Then
                    Dim itemName As Object
                    Dim itemList As ArrayList
                    itemList = isr.IO.Cryptography.SystemSign.EnumerateSystems("Win32_NetworkAdapter", "deviceid")
                    Me.MakeTokenSignatureTextBox.Clear()
                    For Each itemName In itemList
                        Me.MakeTokenSignatureTextBox.AppendText(itemName.ToString())
                        Me.MakeTokenSignatureTextBox.AppendText(Environment.NewLine)
                    Next
                    Me.MakeTokenSignatureTextBox.AppendText("MAC Address: ")
                    Me.MakeTokenSignatureTextBox.AppendText(isr.IO.Cryptography.SystemSign.GetSystemProperty("Win32_NetworkAdapter.deviceid='3'", "MACAddress"))
                    Exit Sub
                End If

                Dim systemDrive As String = My.Computer.FileSystem.SpecialDirectories.ProgramFiles.Substring(0, 2)

                ' Me.statusLabel.Text = System.IO.Directory.GetCreationTime("c:\").ToString
                Me.MakeTokenSignatureTextBox.Clear()
                Me.MakeTokenSignatureTextBox.AppendText($"{systemDrive} Drive S/N: ")
                Me.MakeTokenSignatureTextBox.AppendText(isr.IO.Cryptography.SystemSign.GetSystemProperty($"Win32_LogicalDisk.deviceid='{systemDrive}'", "VolumeSerialNumber"))
                Me.MakeTokenSignatureTextBox.AppendText(Environment.NewLine)
                Me.MakeTokenSignatureTextBox.AppendText("Processor ID: ")
                Me.MakeTokenSignatureTextBox.AppendText(isr.IO.Cryptography.SystemSign.GetSystemProperty("Win32_Processor.deviceid='CPU0'", "ProcessorId"))
                Me.MakeTokenSignatureTextBox.AppendText(Environment.NewLine)
                Me.MakeTokenSignatureTextBox.AppendText("MAC Address: ")
                Me.MakeTokenSignatureTextBox.AppendText(isr.IO.Cryptography.SystemSign.GetSystemPropertyFirstEnum("Win32_NetworkAdapterConfiguration", "MACAddress"))
                Me.MakeTokenSignatureTextBox.AppendText(Environment.NewLine)
                Me.MakeTokenSignatureTextBox.AppendText("Bios Version: ")
                Me.MakeTokenSignatureTextBox.AppendText(isr.IO.Cryptography.SystemSign.GetSystemPropertyFirstEnum("Win32_BIOS", "Version"))
                Me.MakeTokenSignatureTextBox.AppendText(Environment.NewLine)
                Me.MakeTokenSignatureTextBox.AppendText("C:\ creating time: ")
                Me.MakeTokenSignatureTextBox.AppendText(System.IO.Directory.GetCreationTime("c:\").ToString(Globalization.CultureInfo.CurrentCulture))

                'Me.statusLabel.Text = String.Format(Globalization.CultureInfo.CurrentCulture,           "{0} {1}", Date.Now.Ticks, Gopher.UsingDevices.ToString)
                'Me.Close()

            Catch ex As Exception
                ex.Data.Add("@isr", "show results Error")
                My.Application.Logger.WriteExceptionDetails(ex, My.MyApplication.TraceEventId)
                isr.Core.WindowsForms.ShowDialog(ex)
            Finally
                Me.Cursor = Cursors.Default
            End Try
        End Sub

        <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
        Private Sub ShowMeButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ShowMeButton.Click
            Try
                Me.Cursor = Cursors.WaitCursor
                Me.ShowResults()
            Catch ex As Exception
                ex.Data.Add("@isr", "Failed showing results")
                My.Application.Logger.WriteExceptionDetails(ex, My.MyApplication.TraceEventId)
                isr.Core.WindowsForms.ShowDialog(ex)
            Finally
                Me.Cursor = Cursors.Default
            End Try
        End Sub

        <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
        Private Sub CreateSignatureFileButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CreateSignatureFileButton.Click

            Dim builder As New System.Text.StringBuilder
            Try
                Me.Cursor = Cursors.WaitCursor
                Dim fullFileName As String = Me.XmlTokenSignatureFullFileNameTextBox.Text
                Me.MakeTokenSignatureTextBox.Text = builder.ToString
                Using tokenSign As isr.IO.Cryptography.TokenSignBase = Me.GetSystemSign()
                    tokenSign.Open()
                    tokenSign.ResourceId = "Site1000"

                    Dim keyFilename As String = isr.IO.My.MySettings.Default.SigningKeyPairFullFileName
                    builder.AppendLine("Reading key pair from:")
                    builder.AppendLine(keyFilename)
                    tokenSign.XmlSign.LoadRsaSigningKey(keyFilename)

                    builder.AppendLine("Creating and signing the resource")
                    tokenSign.CreateAndSignResource(fullFileName)

                    builder.AppendLine("Saving")
                    tokenSign.Save()

                    builder.AppendLine("Tokens created and written to disk:")
                    builder.AppendLine(fullFileName)

                    builder.AppendLine("XML Element:")
                    builder.AppendLine(tokenSign.XmlSign.FetchResourceDataOuterXml(tokenSign.ResourceName))
                End Using
                Me.MakeTokenSignatureTextBox.Text = builder.ToString

            Catch ex As Exception
                ex.Data.Add("@isr", "create token Error")
                My.Application.Logger.WriteExceptionDetails(ex, My.MyApplication.TraceEventId)
                isr.Core.WindowsForms.ShowDialog(ex)
                isr.Core.WindowsForms.ShowDialog(builder.ToString, "Additional info")
            Finally
                Me.Cursor = Cursors.Default
            End Try

        End Sub

        Private Function GetSystemSign() As isr.IO.Cryptography.TokenSignBase
            Return If(Me.SystemSignRadioButton.Checked,
                New isr.IO.Cryptography.SystemSign(),
                DirectCast(New isr.IO.Cryptography.LicenseSign("1111", My.Application.Info.Version, 12), IO.Cryptography.TokenSignBase))
        End Function

        <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
        Private Sub VerifySignatureFileButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles VerifySignatureFileButton.Click

            Dim builder As New System.Text.StringBuilder
            Try
                Me.Cursor = Cursors.WaitCursor
                Dim fullFileName As String = Me.XmlTokenSignatureFullFileNameTextBox.Text
                Me.ReadTokenSignatureTextBox.Text = builder.ToString

                builder.AppendLine("Verifying:")
                builder.AppendLine(fullFileName)
                Me.ReadTokenSignatureTextBox.Text = builder.ToString
                builder.AppendLine("Reading public key:")
                builder.AppendLine(isr.IO.My.MySettings.Default.VerifyingKeyFullFileName)
                Dim publicKeyXml As String = My.Computer.FileSystem.ReadAllText(isr.IO.My.MySettings.Default.VerifyingKeyFullFileName)
                Me.ReadTokenSignatureTextBox.Text = builder.ToString
                If isr.IO.Cryptography.XmlSign.IsVerifySignatureFile(fullFileName) Then
                    builder.AppendLine("Signature checked")
                    Using tokenSign As isr.IO.Cryptography.TokenSignBase = Me.GetSystemSign()
                        tokenSign.Open()

                        Dim keyFilename As String = isr.IO.My.MySettings.Default.VerifyingKeyFullFileName
                        builder.AppendLine("Reading public key for verification only from:")
                        builder.AppendLine(keyFilename)
                        tokenSign.XmlSign.LoadRsaSigningKey(keyFilename)

                        If tokenSign.VerifyTrustReadParse(fullFileName, publicKeyXml) Then
                            builder.AppendLine("XML Element:")
                            builder.AppendLine(tokenSign.XmlSign.FetchResourceDataOuterXml(tokenSign.ResourceName))
                            builder.AppendLine("Parsed Tokens:")
                            builder.AppendLine(tokenSign.GetTokens)
                            Dim ActionEventArgs As New isr.Core.ActionEventArgs
                            If tokenSign.VerifyTokens(ActionEventArgs) Then
                                builder.AppendLine("All tokens verified")
                            Else
                                builder.AppendLine(ActionEventArgs.Details)
                            End If
                        Else
                            builder.AppendLine("Signature check FAILED")
                        End If
                        Me.ReadTokenSignatureTextBox.Text = builder.ToString
                    End Using
                Else
                    builder.AppendLine("Signature check FAILED")
                End If
                Me.ReadTokenSignatureTextBox.Text = builder.ToString

            Catch ex As Exception
                ex.Data.Add("@isr", "verify Error")
                My.Application.Logger.WriteExceptionDetails(ex, My.MyApplication.TraceEventId)
                isr.Core.WindowsForms.ShowDialog(ex)
                isr.Core.WindowsForms.ShowDialog(builder.ToString, "Additional info")
            Finally
                Me.Cursor = Cursors.Default
            End Try

        End Sub

#End Region

#Region " KEY MANAGEMENT "

        <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
        Private Sub ReadKeyInfoButton_Click(sender As Object, e As EventArgs) Handles ReadKeyInfoButton.Click
            Dim builder As New System.Text.StringBuilder
            Try
                Me.Cursor = Cursors.WaitCursor
                Me.KeyManagemenrOutcomeLabel.Text = "Reading..."
                Dim fullFileName As String = Me.SignedXmlFullFileNameTextBox.Text
                Me.ReadTokenSignatureTextBox.Text = builder.ToString
                builder.AppendLine("Reading key info from:")
                builder.AppendLine(fullFileName)
                If isr.IO.Cryptography.XmlSign.IsVerifySignatureFile(fullFileName) Then
                    builder.AppendLine("Signature checked")
                Else
                    builder.AppendLine("Signature check FAILED")
                End If
                Using tokenSign As isr.IO.Cryptography.TokenSignBase = Me.GetSystemSign()
                    tokenSign.Open()
                    If tokenSign.IsOpen Then
                        builder.AppendLine("Reading signature")
                        tokenSign.XmlSign.ReadSignature(fullFileName)
                        builder.AppendLine("Fetching key info")
                        Dim keyInfo As String = tokenSign.XmlSign.FetchKeyInfoNode.OuterXml
                        Me.KeyInfoOuterXmlTextBox.Text = keyInfo
                        builder.AppendLine("Getting key info base 64 hash")
                        Me.KeyInfoHashTextBox.Text = keyInfo.ToBase64Hash
                    Else
                        isr.Core.WindowsForms.ShowDialog(builder.ToString, "Open failed")
                    End If
                End Using
                Me.KeyManagemenrOutcomeLabel.Text = "Done reading Key info"
            Catch ex As Exception
                ex.Data.Add("@isr", "Key Info fetch error")
                My.Application.Logger.WriteExceptionDetails(ex, My.MyApplication.TraceEventId)
                isr.Core.WindowsForms.ShowDialog(ex)
                isr.Core.WindowsForms.ShowDialog(builder.ToString, "Additional info")
            Finally
                Me.Cursor = Cursors.Default
            End Try

        End Sub

        <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
        Private Sub SaveNewKeyButton_Click(sender As Object, e As EventArgs) Handles SaveNewKeyButton.Click
            Dim builder As New System.Text.StringBuilder
            Try
                Me.KeyManagemenrOutcomeLabel.Text = "Saving..."
                Me.Cursor = Cursors.WaitCursor
                Dim fullFileName As String = isr.IO.My.MySettings.Default.SigningKeyPairFullFileName
                Using key As System.Security.Cryptography.RSA = isr.IO.Cryptography.XmlSign.CreateRsaSigningKey
                    fullFileName = isr.IO.My.MySettings.Default.SigningKeyPairFullFileName
                    builder.AppendLine("Saving signing key to:")
                    builder.AppendLine(fullFileName)
                    isr.IO.Cryptography.XmlSign.SaveRsaSigningKey(key, fullFileName, True)
                    fullFileName = isr.IO.My.MySettings.Default.VerifyingKeyFullFileName
                    builder.AppendLine("Saving public key to:")
                    builder.AppendLine(fullFileName)
                    isr.IO.Cryptography.XmlSign.SaveRsaSigningKey(key, fullFileName, False)
                End Using
                Me.KeyManagemenrOutcomeLabel.Text = "Key Saved"
            Catch ex As Exception
                ex.Data.Add("@isr", "Key save error")
                My.Application.Logger.WriteExceptionDetails(ex, My.MyApplication.TraceEventId)
                isr.Core.WindowsForms.ShowDialog(ex)
                isr.Core.WindowsForms.ShowDialog(builder.ToString, "Additional info")
            Finally
                Me.Cursor = Cursors.Default
            End Try
        End Sub

        <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
        Private Sub LoadKeyButton_Click(sender As Object, e As EventArgs) Handles LoadKeyButton.Click
            Dim builder As New System.Text.StringBuilder
            Try
                Me.Cursor = Cursors.WaitCursor
                Me.KeyManagemenrOutcomeLabel.Text = "Reading keys..."
                Dim fullFileName As String = isr.IO.My.MySettings.Default.SigningKeyPairFullFileName
                Using tokenSign As isr.IO.Cryptography.TokenSignBase = Me.GetSystemSign()
                    tokenSign.Open()
                    fullFileName = isr.IO.My.MySettings.Default.VerifyingKeyFullFileName
                    builder.AppendLine("Reading public key from:")
                    builder.AppendLine(fullFileName)
                    tokenSign.XmlSign.LoadRsaSigningKey(fullFileName)
                    fullFileName = isr.IO.My.MySettings.Default.SigningKeyPairFullFileName
                    builder.AppendLine("Reading key from:")
                    builder.AppendLine(fullFileName)
                    tokenSign.XmlSign.LoadRsaSigningKey(fullFileName)
                End Using
                Me.KeyManagemenrOutcomeLabel.Text = "Key loaded"
            Catch ex As Exception
                ex.Data.Add("@isr", "Key save error")
                My.Application.Logger.WriteExceptionDetails(ex, My.MyApplication.TraceEventId)
                isr.Core.WindowsForms.ShowDialog(ex)
                isr.Core.WindowsForms.ShowDialog(builder.ToString, "Additional info")
            Finally
                Me.Cursor = Cursors.Default
            End Try
        End Sub

#End Region

    End Class

End Namespace

