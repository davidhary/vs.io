﻿Namespace My

    Partial Friend Class MyApplication

        ''' <summary> Gets the identifier of the trace source. </summary>
        Public Const TraceEventId As Integer = IO.My.ProjectTraceEventId.IOTester

        Public Const AssemblyTitle As String = "IO Library Tester"
        Public Const AssemblyDescription As String = "IO Library Tester"
        Public Const AssemblyProduct As String = "IO.Library.Tester.2019"

    End Class

End Namespace

