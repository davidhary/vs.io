## ISR IO<sub>&trade;</sub>: IO Class Library Tester
### Revision History


*2.1.6174 2016-11-26*  
Cryptography: Supports key management, using a known
key, and trust validation based on the original signing key.

*2.0.6173 2016-11-25*  
Cryptography: Modifies XML Signature. Changes break
backwards compatibility. Supports license signature file.

*1.2.4507 2012-05-04*  
Adds x86 project.

*1.2.4232 2011-08-03*  
Standardize code elements and documentation.

*1.2.4213 2011-07-15*  
Simplifies the assembly information.

*1.2.3412 2009-05-06*  
Uses Foundation, Controls, and Windows Forms
libraries.

*1.2.2961 2008-02-09*  
Update to .NET 3.5.

*1.1.2301 2006-04-20*  
Upgrade to Visual Studio 2005.

*1.0.2219 2006-01-28*  
Remove Visual Basic import.

*1.0.2206 2006-01-15*  
Use path name for the full file and folder names. New
support, core, and exception classes. Use Int32, Int64, and Int16
instead of Integer, Long, and Short.

*1.0.2205 2006-01-14*  
Use path name for folder and file name specifications.

*1.0.1933 2005-04-17 Create from Delimited FIle, XML Sign, and Token Sign,

\(C\) 2005 Integrated Scientific Resources, Inc. All rights reserved.

### The MIT License [](#){name=The-MIT-License}
THE SOFTWARE IS PROVIDED \"AS IS\", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

This software was developed and tested using Microsoft<sup>&reg;</sup> [Visual Studio](https://www.visualstudIO.com/) 2019.  

Source code for this project is hosted on [Bit Bucket](https://bitbucket.org/davidhary).

### Open source  [](#){name=Open-Source}
Open source used by this software is described and licensed at the
following sites:  
[Core Libraries](https://bitbucket.org/davidhary/vs.core)  
[IO Libraries](https://bitbucket.org/davidhary/vs.IO)  
[Excel Data Manipulation Using
VB.NET](http://www.CodeProject.com/KB/vb/ExcelDataManipulation.aspx)  
[File System Watcher Pure
Chaos](http://www.codeproject.com/Articles/58740/FileSystemWatcher-Pure-Chaos-Part-of)
