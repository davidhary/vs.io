Namespace Delimited

    ''' <summary> Includes code for form TestPanel. </summary>
    ''' <remarks> Launch this form by calling its Show or ShowDialog method from its default instance. <para> 
    ''' (c) 2004 Integrated Scientific Resources, Inc. All rights reserved. </para><para>
    ''' Licensed under The MIT License.</para><para>
    ''' David, 2004-03-04, 1.0.1524.x. </para></remarks>
    Friend Class TestPanel
        Inherits System.Windows.Forms.Form

#Region " Windows Form Designer generated code "

        Public Sub New()
            MyBase.New()

            ' This method is required by the Windows Form Designer.
            Me.InitializeComponent()

        End Sub

        'Form overrides dispose to clean up the component list.
        Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
            Try

                If disposing Then
                    If Me._Components IsNot Nothing Then
                        Me._Components.Dispose()
                    End If
                End If
            Finally
                ' Invoke the base class dispose method
                MyBase.Dispose(disposing)
            End Try
        End Sub

        'Required by the Windows Form Designer
        Private ReadOnly _Components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Windows Form Designer
        'It can be modified using the Windows Form Designer.  
        'Do not modify it using the code editor.
        Private WithEvents ExitButton As System.Windows.Forms.Button
        Private WithEvents FilePathNameLabel As System.Windows.Forms.Label
        Private WithEvents FilePathNameTextBox As System.Windows.Forms.TextBox
        Private WithEvents OutputTextBox As System.Windows.Forms.TextBox
        Private WithEvents DataWrittenLabel As System.Windows.Forms.Label
        Private WithEvents StatusLabel As System.Windows.Forms.Label
        Private WithEvents InputTextBox As System.Windows.Forms.TextBox
        Private WithEvents WriteButton As System.Windows.Forms.Button
        Private WithEvents ReadButton As System.Windows.Forms.Button
        Private WithEvents MainTabControl As System.Windows.Forms.TabControl
        Private WithEvents SingleValuesTabPage As System.Windows.Forms.TabPage
        Private WithEvents ArraysTabPage As System.Windows.Forms.TabPage
        Private WithEvents WriteArrayButton As System.Windows.Forms.Button
        Private WithEvents ReadArrayButton As System.Windows.Forms.Button
        Private WithEvents AutoDelimitedCheckBox As System.Windows.Forms.CheckBox
        Private WithEvents ArrayDataGrid As System.Windows.Forms.DataGrid
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
            Me.ExitButton = New System.Windows.Forms.Button()
            Me.FilePathNameLabel = New System.Windows.Forms.Label()
            Me.FilePathNameTextBox = New System.Windows.Forms.TextBox()
            Me.OutputTextBox = New System.Windows.Forms.TextBox()
            Me.DataWrittenLabel = New System.Windows.Forms.Label()
            Me.StatusLabel = New System.Windows.Forms.Label()
            Me.InputTextBox = New System.Windows.Forms.TextBox()
            Me.WriteButton = New System.Windows.Forms.Button()
            Me.ReadButton = New System.Windows.Forms.Button()
            Me.MainTabControl = New System.Windows.Forms.TabControl()
            Me.SingleValuesTabPage = New System.Windows.Forms.TabPage()
            Me.AutoDelimitedCheckBox = New System.Windows.Forms.CheckBox()
            Me.ArraysTabPage = New System.Windows.Forms.TabPage()
            Me.ArrayDataGrid = New System.Windows.Forms.DataGrid()
            Me.ReadArrayButton = New System.Windows.Forms.Button()
            Me.WriteArrayButton = New System.Windows.Forms.Button()
            Me.MainTabControl.SuspendLayout()
            Me.SingleValuesTabPage.SuspendLayout()
            Me.ArraysTabPage.SuspendLayout()
            CType(Me.ArrayDataGrid, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.SuspendLayout()
            '
            'exitButton
            '
            Me.ExitButton.DialogResult = System.Windows.Forms.DialogResult.Cancel
            Me.ExitButton.Location = New System.Drawing.Point(416, 320)
            Me.ExitButton.Name = "exitButton"
            Me.ExitButton.Size = New System.Drawing.Size(75, 23)
            Me.ExitButton.TabIndex = 0
            Me.ExitButton.Text = "E&xit"
            '
            'filePathNameLabel
            '
            Me.FilePathNameLabel.Location = New System.Drawing.Point(8, 6)
            Me.FilePathNameLabel.Name = "filePathNameLabel"
            Me.FilePathNameLabel.Size = New System.Drawing.Size(63, 16)
            Me.FilePathNameLabel.TabIndex = 1
            Me.FilePathNameLabel.Text = "File Name: "
            Me.FilePathNameLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight
            '
            'filePathNameTextBox
            '
            Me.FilePathNameTextBox.Location = New System.Drawing.Point(76, 4)
            Me.FilePathNameTextBox.Name = "filePathNameTextBox"
            Me.FilePathNameTextBox.Size = New System.Drawing.Size(412, 20)
            Me.FilePathNameTextBox.TabIndex = 2
            '
            'outputTextBox
            '
            Me.OutputTextBox.Location = New System.Drawing.Point(13, 40)
            Me.OutputTextBox.Multiline = True
            Me.OutputTextBox.Name = "outputTextBox"
            Me.OutputTextBox.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
            Me.OutputTextBox.Size = New System.Drawing.Size(228, 176)
            Me.OutputTextBox.TabIndex = 3
            '
            'dataWrittenLabel
            '
            Me.DataWrittenLabel.Location = New System.Drawing.Point(13, 16)
            Me.DataWrittenLabel.Name = "dataWrittenLabel"
            Me.DataWrittenLabel.Size = New System.Drawing.Size(120, 16)
            Me.DataWrittenLabel.TabIndex = 4
            Me.DataWrittenLabel.Text = "Data Written to the File"
            '
            'statusLabel
            '
            Me.StatusLabel.Location = New System.Drawing.Point(251, 16)
            Me.StatusLabel.Name = "statusLabel"
            Me.StatusLabel.Size = New System.Drawing.Size(120, 16)
            Me.StatusLabel.TabIndex = 6
            Me.StatusLabel.Text = "Data Read from the File"
            '
            'inputTextBox
            '
            Me.InputTextBox.Location = New System.Drawing.Point(251, 40)
            Me.InputTextBox.Multiline = True
            Me.InputTextBox.Name = "inputTextBox"
            Me.InputTextBox.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
            Me.InputTextBox.Size = New System.Drawing.Size(228, 176)
            Me.InputTextBox.TabIndex = 5
            '
            'writeButton
            '
            Me.WriteButton.Location = New System.Drawing.Point(13, 224)
            Me.WriteButton.Name = "writeButton"
            Me.WriteButton.Size = New System.Drawing.Size(75, 23)
            Me.WriteButton.TabIndex = 7
            Me.WriteButton.Text = "&Write"
            '
            'readButton
            '
            Me.ReadButton.Location = New System.Drawing.Point(251, 224)
            Me.ReadButton.Name = "readButton"
            Me.ReadButton.Size = New System.Drawing.Size(75, 23)
            Me.ReadButton.TabIndex = 8
            Me.ReadButton.Text = "&Read"
            '
            'mainTabControl
            '
            Me.MainTabControl.Controls.Add(Me.SingleValuesTabPage)
            Me.MainTabControl.Controls.Add(Me.ArraysTabPage)
            Me.MainTabControl.Location = New System.Drawing.Point(8, 32)
            Me.MainTabControl.Name = "mainTabControl"
            Me.MainTabControl.SelectedIndex = 0
            Me.MainTabControl.Size = New System.Drawing.Size(501, 280)
            Me.MainTabControl.TabIndex = 9
            '
            'singleValuesTabPage
            '
            Me.SingleValuesTabPage.Controls.Add(Me.AutoDelimitedCheckBox)
            Me.SingleValuesTabPage.Controls.Add(Me.WriteButton)
            Me.SingleValuesTabPage.Controls.Add(Me.InputTextBox)
            Me.SingleValuesTabPage.Controls.Add(Me.ReadButton)
            Me.SingleValuesTabPage.Controls.Add(Me.OutputTextBox)
            Me.SingleValuesTabPage.Controls.Add(Me.StatusLabel)
            Me.SingleValuesTabPage.Controls.Add(Me.DataWrittenLabel)
            Me.SingleValuesTabPage.Location = New System.Drawing.Point(4, 22)
            Me.SingleValuesTabPage.Name = "singleValuesTabPage"
            Me.SingleValuesTabPage.Size = New System.Drawing.Size(493, 254)
            Me.SingleValuesTabPage.TabIndex = 0
            Me.SingleValuesTabPage.Text = "Single Values"
            '
            '_AutoDelimitedCheckBox
            '
            Me.AutoDelimitedCheckBox.AutoSize = True
            Me.AutoDelimitedCheckBox.Location = New System.Drawing.Point(118, 227)
            Me.AutoDelimitedCheckBox.Name = "_AutoDelimitedCheckBox"
            Me.AutoDelimitedCheckBox.Size = New System.Drawing.Size(94, 17)
            Me.AutoDelimitedCheckBox.TabIndex = 9
            Me.AutoDelimitedCheckBox.Text = "Auto Delimited"
            Me.AutoDelimitedCheckBox.UseVisualStyleBackColor = True
            '
            'arraysTabPage
            '
            Me.ArraysTabPage.Controls.Add(Me.ArrayDataGrid)
            Me.ArraysTabPage.Controls.Add(Me.ReadArrayButton)
            Me.ArraysTabPage.Controls.Add(Me.WriteArrayButton)
            Me.ArraysTabPage.Location = New System.Drawing.Point(4, 22)
            Me.ArraysTabPage.Name = "arraysTabPage"
            Me.ArraysTabPage.Size = New System.Drawing.Size(493, 254)
            Me.ArraysTabPage.TabIndex = 1
            Me.ArraysTabPage.Text = "Arrays"
            '
            'arrayDataGrid
            '
            Me.ArrayDataGrid.DataMember = String.Empty
            Me.ArrayDataGrid.HeaderForeColor = System.Drawing.SystemColors.ControlText
            Me.ArrayDataGrid.Location = New System.Drawing.Point(8, 8)
            Me.ArrayDataGrid.Name = "arrayDataGrid"
            Me.ArrayDataGrid.Size = New System.Drawing.Size(477, 208)
            Me.ArrayDataGrid.TabIndex = 12
            '
            'readArrayButton
            '
            Me.ReadArrayButton.Location = New System.Drawing.Point(373, 224)
            Me.ReadArrayButton.Name = "readArrayButton"
            Me.ReadArrayButton.Size = New System.Drawing.Size(112, 23)
            Me.ReadArrayButton.TabIndex = 11
            Me.ReadArrayButton.Text = "&Read Array..."
            '
            'writeArrayButton
            '
            Me.WriteArrayButton.Location = New System.Drawing.Point(232, 224)
            Me.WriteArrayButton.Name = "writeArrayButton"
            Me.WriteArrayButton.Size = New System.Drawing.Size(112, 23)
            Me.WriteArrayButton.TabIndex = 10
            Me.WriteArrayButton.Text = "&Write Array..."
            '
            'TestPanel
            '
            Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
            Me.ClientSize = New System.Drawing.Size(518, 350)
            Me.Controls.Add(Me.MainTabControl)
            Me.Controls.Add(Me.FilePathNameTextBox)
            Me.Controls.Add(Me.FilePathNameLabel)
            Me.Controls.Add(Me.ExitButton)
            Me.Name = "TestPanel"
            Me.Text = "Test Panel"
            Me.MainTabControl.ResumeLayout(False)
            Me.SingleValuesTabPage.ResumeLayout(False)
            Me.SingleValuesTabPage.PerformLayout()
            Me.ArraysTabPage.ResumeLayout(False)
            CType(Me.ArrayDataGrid, System.ComponentModel.ISupportInitialize).EndInit()
            Me.ResumeLayout(False)
            Me.PerformLayout()

        End Sub

#End Region

#Region " FORM EVENT HANDLERS "

        ''' <summary>Is true after form has completed loading so that we can disable check boxes 
        '''   that should not fire until the form is fully loaded.</summary>
        ''' <remarks>Initialization was removed per performance rule CA1805 as it is done 
        '''   automatically by the runtime.</remarks>
        <System.Diagnostics.CodeAnalysis.SuppressMessage("Code Quality", "IDE0052:Remove unread private members", Justification:="<Pending>")>
        Private _Loaded As Boolean

        ''' <summary>Occurs when the form is loaded.</summary>
        ''' <param name="sender"><see cref="System.Object"/> instance of this 
        '''   <see cref="System.Windows.Forms.Form"/></param>
        ''' <param name="e"><see cref="System.EventArgs"/></param>
        ''' <remarks>Use this method for doing any final initialization right before 
        '''   the form is shown.  This is a good place to change the Visible and
        '''   ShowInTaskbar properties to start the form as hidden.  
        '''   Starting a form as hidden is useful for forms that need to be running but that
        '''   should not show themselves right away, such as forms with a notify icon in the
        '''   task bar.</remarks>
        Private Sub Form_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

            Try

                ' Turn on the form hourglass cursor
                Me.Cursor = System.Windows.Forms.Cursors.WaitCursor

                ' set the form caption
                Me.Text = My.Application.Info.ProductName & ": DELIMITED I/O TEST PANEL " & My.Application.Info.Version.ToString

                ' center the form
                Me.CenterToScreen()

                ' update the file name string
                If Me.FilePathNameTextBox.Text.Length = 0 Then
                    Dim delimitedFile As isr.IO.DelimitedReader = New isr.IO.DelimitedReader
                    Me.FilePathNameTextBox.Text = delimitedFile.FilePathName
                End If

                ' turn on the loaded flag
                Me._Loaded = True

            Catch

                ' Use throw without an argument in order to preserve the stack location 
                ' where the exception was initially raised.
                Throw

            Finally

                ' Turn off the form hourglass cursor
                Me.Cursor = System.Windows.Forms.Cursors.Default

            End Try

        End Sub

        Private Sub Form_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Activated
            ' Me.notesMessageList.PushMessage("Activated")
        End Sub

#End Region

#Region " CONTROL EVENT HANDLERS "

        ''' <summary>Closes the form and exits the application.</summary>
        <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
        Private Sub ExitButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ExitButton.Click

            Try

                Me.Close()

            Catch ex As Exception
                ex.Data.Add("@isr", "exit Error")
                My.Application.Logger.WriteExceptionDetails(ex, My.MyApplication.TraceEventId)
                isr.Core.WindowsForms.ShowDialog(ex)
            End Try

        End Sub

        Private _DataValues As Double(,) = {}

        Private _ArrayFilePathName As String

        ''' <summary>reads an array into the data grid.</summary>
        <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
        Private Sub ReadArrayButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ReadArrayButton.Click

            ' get a new file name
            Me._ArrayFilePathName = OpenFileOpenDialog(Me._ArrayFilePathName)

            ' instantiate the delimiter file reader
            ' open the array file 
            Dim reader As isr.IO.DelimitedReader = New isr.IO.DelimitedReader() With {
                .FilePathName = Me._ArrayFilePathName
            }
            reader.Open()

            Try

                ' display the file name
                Me.FilePathNameTextBox.Text = reader.FilePathName()

                ' clear the data array
                Array.Clear(Me._DataValues, 0, Me._DataValues.Length)

                ' read the array from the file
                Me._DataValues = reader.ReadRowsDoubleArray

                ' display the array values
                BindArrayToGrid(Me.ArrayDataGrid, Me._DataValues)

                Me.ArrayDataGrid.AllowNavigation = False
                Me.ArrayDataGrid.ReadOnly = True
                Me.ArrayDataGrid.RowHeaderWidth = 0

            Catch ex As Exception
                ex.Data.Add("@isr", "read array error")
                My.Application.Logger.WriteExceptionDetails(ex, My.MyApplication.TraceEventId)
                isr.Core.WindowsForms.ShowDialog(ex)

            Finally

                ' close the file
                reader.Close()

            End Try

        End Sub

        <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
        Private Sub ReadButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ReadButton.Click

            ' instantiate a new reader class
            Dim reader As isr.IO.DelimitedReader = New isr.IO.DelimitedReader()

            ' open the file name using the default file name
            reader.Open()

            Try

                ' display the file name
                Me.FilePathNameTextBox.Text = reader.FilePathName()

                ' read values from the file
                Dim value As Object = Nothing
                Do While Not reader.IsEndOfFile
                    value = reader.ReadObject
                    If value Is Nothing Then
                        Me.InputTextBox.AppendText(String.Format$("{0}: {1}{2}", "Nothing", "",
                            Environment.NewLine))
                    Else
                        Me.InputTextBox.AppendText(String.Format$("{0}: {1}{2}", value.GetType, value.ToString,
                            Environment.NewLine))
                    End If
                Loop

            Catch ex As Exception
                ex.Data.Add("@isr", "read Error")
                My.Application.Logger.WriteExceptionDetails(ex, My.MyApplication.TraceEventId)
                isr.Core.WindowsForms.ShowDialog(ex)
            Finally

                ' close the file
                reader.Close()

            End Try
        End Sub

        ''' <summary>writes an array to file.</summary>
        <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
        Private Sub WriteArrayButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles WriteArrayButton.Click

            ' get a new file name
            Me._ArrayFilePathName = TestPanel.OpenSaveFileDialog(Me._ArrayFilePathName)

            ' instantiate the delimiter file reader
            ' open the array file 
            Dim writer As isr.IO.DelimitedWriter = New isr.IO.DelimitedWriter() With {
                .FilePathName = Me._ArrayFilePathName
            }
            writer.Open(False)

            Try

                ' display the file name
                Me.FilePathNameTextBox.Text = writer.FilePathName()

                ' read the array from the file
                writer.WriteColumns(Me._DataValues)

                ' display the array values
                BindArrayToGrid(Me.ArrayDataGrid, Me._DataValues)

                Me.ArrayDataGrid.AllowNavigation = False
                Me.ArrayDataGrid.ReadOnly = True
                Me.ArrayDataGrid.RowHeaderWidth = 0

            Catch ex As Exception
                ex.Data.Add("@isr", "write array Error")
                My.Application.Logger.WriteExceptionDetails(ex, My.MyApplication.TraceEventId)
                isr.Core.WindowsForms.ShowDialog(ex)
            Finally

                ' close the file
                writer.Close()

            End Try

        End Sub

        ''' <summary>Occurs when the operator selects the Write button.</summary>
        ''' <remarks>This method writes a preset data to the file.</remarks>
        <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
        Private Sub WriteButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles WriteButton.Click

            ' instantiate a new writer class
            Dim writer As isr.IO.DelimitedWriter = New isr.IO.DelimitedWriter() With {
                .IsAutoDelimited = Me.AutoDelimitedCheckBox.Checked
            }

            ' open the file name using the default file name
            writer.Open(False)

            Try

                ' display the file name
                Me.FilePathNameTextBox.Text = writer.FilePathName()

                ' write the bunch of value to file and display on screen
                Me.OutputTextBox.Clear()

                Dim value As Object = String.Empty
                writer.Write(value)
                Me.OutputTextBox.AppendText(String.Format$("{0}: {1}{2}",
                    writer.LastValue.GetType, writer.LastValue.ToString, Environment.NewLine))

                writer.Write(System.DBNull.Value)
                Me.OutputTextBox.AppendText(String.Format$("{0}: {1}{2}",
                    writer.LastValue.GetType, writer.LastValue.ToString, Environment.NewLine))

                writer.Write(Int16.MaxValue)
                Me.OutputTextBox.AppendText(String.Format$("{0}: {1}{2}",
                    writer.LastValue.GetType, writer.LastValue.ToString, Environment.NewLine))

                writer.Write(Int32.MaxValue)
                Me.OutputTextBox.AppendText(String.Format$("{0}: {1}{2}",
                    writer.LastValue.GetType, writer.LastValue.ToString, Environment.NewLine))

                writer.Write(Int64.MaxValue - 1)
                Me.OutputTextBox.AppendText(String.Format$("{0}: {1}{2}",
                    writer.LastValue.GetType, writer.LastValue.ToString, Environment.NewLine))

                writer.Write(Convert.ToSingle(1000.001))
                Me.OutputTextBox.AppendText(String.Format$("{0}: {1}{2}",
                    writer.LastValue.GetType, writer.LastValue.ToString, Environment.NewLine))

                writer.Write(Convert.ToDouble(1000.00001))
                Me.OutputTextBox.AppendText(String.Format$("{0}: {1}{2}",
                    writer.LastValue.GetType, writer.LastValue.ToString, Environment.NewLine))

                writer.Write(Convert.ToDecimal(1000.01))
                Me.OutputTextBox.AppendText(String.Format$("{0}: {1}{2}",
                    writer.LastValue.GetType, writer.LastValue.ToString, Environment.NewLine))

                writer.Write(DateTimeOffset.Now)
                Me.OutputTextBox.AppendText(String.Format$("{0}: {1}{2}",
                    writer.LastValue.GetType, writer.LastValue.ToString, Environment.NewLine))

                writer.Write("A String")
                Me.OutputTextBox.AppendText(String.Format$("{0}: {1}{2}",
                    writer.LastValue.GetType, writer.LastValue.ToString, Environment.NewLine))

                writer.Write(True)
                Me.OutputTextBox.AppendText(String.Format$("{0}: {1}{2}",
                    writer.LastValue.GetType, writer.LastValue.ToString, Environment.NewLine))

                writer.Write("End Of Record", True)
                Me.OutputTextBox.AppendText(String.Format$("{0}: {1}{2}",
                    writer.LastValue.GetType, writer.LastValue.ToString, Environment.NewLine))

            Catch ex As Exception
                ex.Data.Add("@isr", "write Error")
                My.Application.Logger.WriteExceptionDetails(ex, My.MyApplication.TraceEventId)
                isr.Core.WindowsForms.ShowDialog(ex)
            Finally

                ' close the file
                writer.Close()

            End Try

        End Sub

#End Region

#Region " GRID "

        ''' <summary>Binds a two-dimensional array to a data grid.</summary>
        ''' <param name="grid">the grid</param>
        ''' <param name="dataArray">The data array.</param>
        Private Shared Sub BindArrayToGrid(ByVal grid As DataGrid, ByVal dataArray(,) As Double)

            ' Create a new DataTable.
            Dim myDataTable As DataTable = New DataTable("DataTable") With {
                .Locale = Globalization.CultureInfo.CurrentCulture
            }

            ' Declare variables for DataColumn and DataRow objects.
            Dim myDataColumn As DataColumn
            Dim myDataRow As DataRow

            ' Create ID DataColumn, set DataType, ColumnName and add to DataTable.    
            myDataColumn = New DataColumn With {
                .DataType = System.Type.GetType("System.Int32"),
                .ColumnName = "ID",
                .AutoIncrement = True,
                .Caption = "ID",
                .ReadOnly = True,
                .Unique = True
            }
            ' Add the column to the DataColumnCollection.
            myDataTable.Columns.Add(myDataColumn)

            ' Create new DataColumn, set DataType, ColumnName and add to DataTable.    
            For columnIndex As Int32 = 0 To dataArray.GetUpperBound(1)
                myDataColumn = New DataColumn With {
                    .DataType = System.Type.GetType("System.Double"),
                    .ColumnName = columnIndex.ToString(Globalization.CultureInfo.CurrentCulture),
                    .ReadOnly = True,
                    .Unique = False
                }
                ' Add the Column to the DataColumnCollection.
                myDataTable.Columns.Add(myDataColumn)
            Next

            ' Instantiate the DataSet variable.
            Dim myDataSet As New DataSet() With {
                .Locale = Globalization.CultureInfo.CurrentCulture
            }


            ' Add the new DataTable to the DataSet.
            myDataSet.Tables.Add(myDataTable)

            ' Create three new DataRow objects and add them to the DataTable
            For rowIndex As Int32 = 0 To dataArray.GetUpperBound(0)
                myDataRow = myDataTable.NewRow()
                myDataRow("id") = rowIndex
                For columnIndex As Int32 = 0 To dataArray.GetUpperBound(1)
                    myDataRow.Item(columnIndex + 1) = dataArray(rowIndex, columnIndex).ToString(Globalization.CultureInfo.CurrentCulture)
                Next
                myDataTable.Rows.Add(myDataRow)
            Next rowIndex

            ' Instruct the DataGrid to bind to the DataSet, with the 
            ' DataTable as the topmost DataTable.
            grid.SetDataBinding(myDataSet, "DataTable")

        End Sub

#End Region

#Region " FILE DIALOGS "

        ''' <summary>Opens the File Open dialog box and gets a file name</summary>
        Private Shared Function OpenFileOpenDialog(ByVal fileName As String) As String

            Using fileDialog As New OpenFileDialog
                ' Use the common dialog box
                fileDialog.CheckFileExists = True
                fileDialog.CheckPathExists = True
                fileDialog.Title = "Read from a File"
                fileDialog.FileName = fileName
                fileDialog.Filter = "Text files (*.txt;*.csv;*.log)|*.txt;*.csv;*.log|All files (*.*)|*.*"
                Return If(fileDialog.ShowDialog = Windows.Forms.DialogResult.OK, fileDialog.FileName, fileName)

            End Using

        End Function

        ''' <summary>Opens the File Save dialog box and gets a file name</summary>
        Private Shared Function OpenSaveFileDialog(ByVal fileName As String) As String
            Using fileDialog As New SaveFileDialog
                ' Use the common dialog box
                fileDialog.CheckFileExists = False
                fileDialog.CheckPathExists = True
                fileDialog.Title = "Save Data to a File"
                fileDialog.FileName = fileName
                fileDialog.Filter = "Text files (*.txt;*.csv;*.log)|*.txt;*.csv;*.log|All files (*.*)|*.*"
                Return If(fileDialog.ShowDialog = Windows.Forms.DialogResult.OK, fileDialog.FileName, fileName)
            End Using
        End Function

#End Region

    End Class
End Namespace
