Imports System.Runtime.InteropServices

Friend NotInheritable Class Program

    Private Sub New()
    End Sub

    ''' <summary>
    ''' The main entry point for the application.
    ''' </summary>
    <STAThread>
    Public Shared Sub Main()
        Application.EnableVisualStyles()
        Application.SetCompatibleTextRenderingDefault(False)
        Application.Run(New Form1())
    End Sub

End Class

Friend NotInheritable Class NativeMethods

    Private Sub New()
    End Sub

    ' see https://msdn.microsoft.com/en-us/library/windows/desktop/ms684139%28v=vs.85%29.aspx
    Public Shared Function Is64Bit(ByVal process As Process) As Boolean
        If Not Environment.Is64BitOperatingSystem Then
            Return False
        End If
        ' if this method is not available in your version of .NET, use GetNativeSystemInfo via P/Invoke instead

        Dim isWow64 As Boolean
        If Not IsWow64Process(process.Handle, isWow64) Then
            Throw New InvalidOperationException()
        End If
        Return Not isWow64
    End Function

    Public Shared Function Is64Bit() As Boolean
        Return NativeMethods.Is64Bit(Process.GetCurrentProcess)
    End Function

    <DllImport("kernel32.dll", SetLastError:=True, CallingConvention:=CallingConvention.Winapi)>
    Private Shared Function IsWow64Process(<[In]()> ByVal process As IntPtr, <Out()> ByRef wow64Process As Boolean) As <MarshalAs(UnmanagedType.Bool)> Boolean
    End Function
End Class

