Partial Public Class Form1
    ''' <summary>
    ''' Required designer variable.
    ''' </summary>
    Private components As System.ComponentModel.IContainer = Nothing

    ''' <summary>
    ''' Clean up any resources being used.
    ''' </summary>
    ''' <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso (components IsNot Nothing) Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

#Region "Windows Form Designer generated code"

    ''' <summary>
    ''' Required method for Designer support - do not modify
    ''' the contents of this method with the code editor.
    ''' </summary>
    Private Sub InitializeComponent()
        Me.dataGridView1 = New System.Windows.Forms.DataGridView()
        Me.OpenBuilder = New System.Windows.Forms.Button()
        Me.OpenExpression = New System.Windows.Forms.Button()
        Me.Expression = New System.Windows.Forms.TextBox()
        Me.listBox1 = New System.Windows.Forms.ListBox()
        Me.label1 = New System.Windows.Forms.Label()
        Me.label2 = New System.Windows.Forms.Label()
        CType(Me.dataGridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        ' 
        ' dataGridView1
        ' 
        Me.dataGridView1.Anchor = (CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) Or System.Windows.Forms.AnchorStyles.Left) Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles))
        Me.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dataGridView1.Location = New System.Drawing.Point(16, 87)
        Me.dataGridView1.Name = "dataGridView1"
        Me.dataGridView1.Size = New System.Drawing.Size(538, 174)
        Me.dataGridView1.TabIndex = 0
        ' 
        ' OpenBuilder
        ' 
        Me.OpenBuilder.Location = New System.Drawing.Point(16, 12)
        Me.OpenBuilder.Name = "OpenBuilder"
        Me.OpenBuilder.Size = New System.Drawing.Size(199, 23)
        Me.OpenBuilder.TabIndex = 1
        Me.OpenBuilder.Text = "Open File With Regex Column Builder"
        Me.OpenBuilder.UseVisualStyleBackColor = True
        '			Me.OpenBuilder.Click += New System.EventHandler(Me.OpenBuilder_Click)
        ' 
        ' OpenExpression
        ' 
        Me.OpenExpression.Location = New System.Drawing.Point(16, 41)
        Me.OpenExpression.Name = "OpenExpression"
        Me.OpenExpression.Size = New System.Drawing.Size(199, 23)
        Me.OpenExpression.TabIndex = 2
        Me.OpenExpression.Text = "Open File with Regular Expression"
        Me.OpenExpression.UseVisualStyleBackColor = True
        '			Me.OpenExpression.Click += New System.EventHandler(Me.OpenExpression_Click)
        ' 
        ' Expression
        ' 
        Me.Expression.Location = New System.Drawing.Point(230, 43)
        Me.Expression.Name = "Expression"
        Me.Expression.Size = New System.Drawing.Size(302, 20)
        Me.Expression.TabIndex = 3
        Me.Expression.Text = "^(?<ID>[^,]+),(?<Name>[^,]+),(?<Date>[^,]+)$"
        ' 
        ' listBox1
        ' 
        Me.listBox1.Anchor = (CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles))
        Me.listBox1.FormattingEnabled = True
        Me.listBox1.Location = New System.Drawing.Point(16, 281)
        Me.listBox1.Name = "listBox1"
        Me.listBox1.Size = New System.Drawing.Size(537, 82)
        Me.listBox1.TabIndex = 4
        ' 
        ' label1
        ' 
        Me.label1.AutoSize = True
        Me.label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, (CByte(0)))
        Me.label1.Location = New System.Drawing.Point(15, 265)
        Me.label1.Name = "label1"
        Me.label1.Size = New System.Drawing.Size(57, 13)
        Me.label1.TabIndex = 5
        Me.label1.Text = "Misreads"
        ' 
        ' label2
        ' 
        Me.label2.AutoSize = True
        Me.label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, (CByte(0)))
        Me.label2.Location = New System.Drawing.Point(18, 71)
        Me.label2.Name = "label2"
        Me.label2.Size = New System.Drawing.Size(53, 13)
        Me.label2.TabIndex = 6
        Me.label2.Text = "DataSet"
        ' 
        ' Form1
        ' 
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0F, 13.0F)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(572, 382)
        Me.Controls.Add(Me.label2)
        Me.Controls.Add(Me.label1)
        Me.Controls.Add(Me.listBox1)
        Me.Controls.Add(Me.Expression)
        Me.Controls.Add(Me.OpenExpression)
        Me.Controls.Add(Me.OpenBuilder)
        Me.Controls.Add(Me.dataGridView1)
        Me.Name = "Form1"
        Me.Text = "Demo Text File Data Set"
        CType(Me.dataGridView1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

#End Region

    Private dataGridView1 As System.Windows.Forms.DataGridView
    Private WithEvents OpenBuilder As System.Windows.Forms.Button
    Private WithEvents OpenExpression As System.Windows.Forms.Button
    Private Expression As System.Windows.Forms.TextBox
    Private listBox1 As System.Windows.Forms.ListBox
    Private label1 As System.Windows.Forms.Label
    Private label2 As System.Windows.Forms.Label
End Class

