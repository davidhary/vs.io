Imports System.IO
Imports System.Text.RegularExpressions

Partial Public Class Form1
    Inherits Form

    Public Sub New()
        InitializeComponent()
    End Sub

    Private MyTextFileDataSet As DelimitedFileDataSet

    Private Sub OpenBuilder_Click(ByVal sender As Object, ByVal e As EventArgs) Handles OpenBuilder.Click
        ' create a new instance of MyTextFileDataSet
        MyTextFileDataSet = New DelimitedFileDataSet()
        ' create a new RegexColumnBuilder
        Dim builder As New RegexColumnBuilder()
        builder.AddColumn("ID", ","c, RegexColumnType.Integer)
        builder.AddColumn("Name", ","c, RegexColumnType.String)
        builder.AddColumn("Date", ","c, RegexColumnType.Date)
        ' add the RegexColumnBuilder to the TextFileDataSet
        MyTextFileDataSet.ApplyColumnBuilder(builder)
        ' set the optional table name - default is 'Table1' 
        MyTextFileDataSet.TableName = "DemoText"
        ' open the file 
        Using fileStream As New FileStream("DemoText.txt", FileMode.Open, FileAccess.Read)
            ' fill the dataset
            MyTextFileDataSet.Fill(fileStream)
        End Using
        ' display the misreads
        ShowMisReads()
        ' display the dataset
        ShowDataSet()
    End Sub

    Private Sub ShowDataSet()
        Me.dataGridView1.DataSource = MyTextFileDataSet
        Me.dataGridView1.DataMember = MyTextFileDataSet.TableName
    End Sub

    Private Sub ShowMisReads()
        Me.listBox1.Items.Clear()
        For Each item As String In MyTextFileDataSet.Misreads
            Me.listBox1.Items.Add(item)
        Next item
    End Sub

    Private Sub OpenExpression_Click(ByVal sender As Object, ByVal e As EventArgs) Handles OpenExpression.Click
        ' create a new instance of MyTextFileDataSet
        ' specify the regular expression for validating and recognize columns
        MyTextFileDataSet = New DelimitedFileDataSet() With {
            .ContentExpression = New Regex(Me.Expression.Text)
        }
        ' open the file 
        Using fileStream As New FileStream("DemoText.txt", FileMode.Open, FileAccess.Read)
            ' fill the dataset
            MyTextFileDataSet.Fill(fileStream)
        End Using
        ' display the misreads
        ShowMisReads()
        ' display the dataset
        ShowDataSet()
    End Sub



End Class
