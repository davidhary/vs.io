﻿Imports System.Reflection
Imports System.Runtime.InteropServices

<Assembly: AssemblyTitle("File Watcher Tray Icon")>
<Assembly: AssemblyDescription("File Watcher Tray Icon")>
<Assembly: AssemblyProduct("File.Watcher.Tray.Icon")>
<Assembly: CLSCompliant(True)>

' Disable accessibility of an individual managed type or member, or of all types within an assembly, to COM.
<Assembly: ComVisible(False)>
