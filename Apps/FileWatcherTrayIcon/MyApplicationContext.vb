

Friend Class MyApplicationContext

    Inherits ApplicationContext

    'Component declarations
    Private WithEvents TrayIcon As NotifyIcon
    Private _TrayIconContextMenu As ContextMenuStrip
    Private WithEvents CloseMenuItem As ToolStripMenuItem

    Public Sub New()
        AddHandler Application.ApplicationExit, AddressOf Me.OnApplicationExit

        Me.InitializeComponent()

        Me.TrayIcon.Visible = True
    End Sub

    Protected Overrides Sub Dispose(disposing As Boolean)
        Try
            If disposing Then
                If Me.CloseMenuItem IsNot Nothing Then
                    Me.CloseMenuItem.Dispose()
                    Me.CloseMenuItem = Nothing
                End If
                If Me._TrayIconContextMenu IsNot Nothing Then
                    Me._TrayIconContextMenu.Dispose()
                    Me._TrayIconContextMenu = Nothing
                End If
                If Me.TrayIcon IsNot Nothing Then
                    Me.TrayIcon.Dispose()
                    Me.TrayIcon = Nothing
                End If
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    Private Sub InitializeComponent()

        'The icon is added to the project resources. Here I assume that the name of the file is 'TrayIcon.ico'
        Me.TrayIcon = New NotifyIcon() With {
            .BalloonTipIcon = ToolTipIcon.Info,
            .BalloonTipText = "I noticed that you double-clicked me! What can I do for you?",
            .BalloonTipTitle = "You called Master?",
            .Text = "My fabulous tray icon demo application",
            .Icon = My.Resources.AppIcon
        }

        'Optional - handle double clicks on the icon:
        '			TrayIcon.DoubleClick += TrayIcon_DoubleClick

        'Optional - Add a context menu to the TrayIcon:
        Me._TrayIconContextMenu = New ContextMenuStrip()
        Me.CloseMenuItem = New ToolStripMenuItem()
        Me._TrayIconContextMenu.SuspendLayout()

        ' 
        ' TrayIconContextMenu
        ' 
        Me._TrayIconContextMenu.Items.AddRange(New ToolStripItem() {Me.CloseMenuItem})
        Me._TrayIconContextMenu.Name = "TrayIconContextMenu"
        Me._TrayIconContextMenu.Size = New Size(153, 70)
        ' 
        ' CloseMenuItem
        ' 
        Me.CloseMenuItem.Name = "CloseMenuItem"
        Me.CloseMenuItem.Size = New Size(152, 22)
        Me.CloseMenuItem.Text = "Close the tray icon program"
        '			Me.CloseMenuItem.Click += New EventHandler(Me.CloseMenuItem_Click)

        Me._TrayIconContextMenu.ResumeLayout(False)

        Me.TrayIcon.ContextMenuStrip = Me._TrayIconContextMenu
    End Sub

    Private Sub OnApplicationExit(ByVal sender As Object, ByVal e As EventArgs)
        'Cleanup so that the icon will be removed when the application is closed
        Me.TrayIcon.Visible = False
    End Sub

    Private Sub TrayIcon_DoubleClick(ByVal sender As Object, ByVal e As EventArgs) Handles TrayIcon.DoubleClick
        ' Here you can do stuff if the tray icon is double clicked
        Me.TrayIcon.ShowBalloonTip(10000)
    End Sub

    Private Sub CloseMenuItem_Click(ByVal sender As Object, ByVal e As EventArgs) Handles CloseMenuItem.Click
        If System.Windows.Forms.MessageBox.Show("Do you really want to close me?", "Are you sure?", MessageBoxButtons.YesNo,
                           MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button2, MessageBoxOptions.DefaultDesktopOnly) = DialogResult.Yes Then
            Application.Exit()
        End If
    End Sub
End Class
