using System.Reflection;

//
// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
//
[assembly: AssemblyTitle("GenericParsing.PerformanceTests")]
[assembly: AssemblyDescription("This assembly is used by the GenericParser for performance tests.")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyProduct("GenericParsing.PerformanceTests")]
