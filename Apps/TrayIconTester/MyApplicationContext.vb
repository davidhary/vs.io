﻿Namespace TrayIconTest
	Friend Class MyApplicationContext
		Inherits ApplicationContext

		'Component declarations
		Private TrayIcon As NotifyIcon
		Private TrayIconContextMenu As ContextMenuStrip
		Private WithEvents CloseMenuItem As ToolStripMenuItem

		Public Sub New()
			AddHandler Application.ApplicationExit, AddressOf OnApplicationExit

			InitializeComponent()

			TrayIcon.Visible = True
		End Sub

		Private Sub InitializeComponent()
			TrayIcon = New NotifyIcon()

			TrayIcon.BalloonTipIcon = ToolTipIcon.Info
			TrayIcon.BalloonTipText = "I noticed that you double-clicked me! What can I do for you?"
			TrayIcon.BalloonTipTitle = "You called Master?"
			TrayIcon.Text = "My fabulous tray icon demo application"

			'The icon is added to the project resources. Here I assume that the name of the file is 'TrayIcon.ico'
			TrayIcon.Icon = My.Resources.TrayIcon

			'Optional - handle doubleclicks on the icon:
'			TrayIcon.DoubleClick += TrayIcon_DoubleClick

			'Optional - Add a context menu to the TrayIcon:
			TrayIconContextMenu = New ContextMenuStrip()
			CloseMenuItem = New ToolStripMenuItem()
			TrayIconContextMenu.SuspendLayout()

			' 
			' TrayIconContextMenu
			' 
			Me.TrayIconContextMenu.Items.AddRange(New ToolStripItem() { Me.CloseMenuItem})
			Me.TrayIconContextMenu.Name = "TrayIconContextMenu"
			Me.TrayIconContextMenu.Size = New Size(153, 70)
			' 
			' CloseMenuItem
			' 
			Me.CloseMenuItem.Name = "CloseMenuItem"
			Me.CloseMenuItem.Size = New Size(152, 22)
			Me.CloseMenuItem.Text = "Close the tray icon program"
'			Me.CloseMenuItem.Click += New EventHandler(Me.CloseMenuItem_Click)

			TrayIconContextMenu.ResumeLayout(False)

			TrayIcon.ContextMenuStrip = TrayIconContextMenu
		End Sub

		Private Sub OnApplicationExit(ByVal sender As Object, ByVal e As EventArgs)
			'Cleanup so that the icon will be removed when the application is closed
			TrayIcon.Visible = False
		End Sub

		Private Sub TrayIcon_DoubleClick(ByVal sender As Object, ByVal e As EventArgs)
			'Here you can do stuff if the tray icon is doubleclicked
			TrayIcon.ShowBalloonTip(10000)
		End Sub

		Private Sub CloseMenuItem_Click(ByVal sender As Object, ByVal e As EventArgs) Handles CloseMenuItem.Click
			If System.Windows.Forms.MessageBox.Show("Do you really want to close me?", "Are you sure?", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button2) = DialogResult.Yes Then
				Application.Exit()
			End If
		End Sub
	End Class
End Namespace
