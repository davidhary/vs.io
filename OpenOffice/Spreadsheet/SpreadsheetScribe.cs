using System;
using System.Data;
using System.Globalization;
using System.IO;
using System.Reflection;
using System.Xml;
using Ionic.Zip;

#pragma warning disable IDE1006 // Naming Styles
namespace isr.IO.OpenOffice
#pragma warning restore IDE1006 // Naming Styles
{
    /// <summary>   The Open Office Spreadsheet scribe. </summary>
    ///
    /// <license>
    /// (c) 2012 Josip K. All rights reserved <para>
    /// Licensed under The MIT License.</para><para>
    /// THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
    /// BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
    /// NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
    /// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    /// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.</para>
    /// </license>
    ///
    /// <history date="2019-06-22" by="David" revision="">   Created. </history>

    public static class SpreadsheetScribe
    {
        // Namespaces. We need this to initialize XmlNamespaceManager so that we can search XmlDocument.
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1814:PreferJaggedArraysOverMultidimensional", MessageId = "Member")]
        private static readonly string[,] Namespaces = new string[,] 
        {
            {"table", "urn:oasis:names:tc:opendocument:xmlns:table:1.0"},
            {"office", "urn:oasis:names:tc:opendocument:xmlns:office:1.0"},
            {"style", "urn:oasis:names:tc:opendocument:xmlns:style:1.0"},
            {"text", "urn:oasis:names:tc:opendocument:xmlns:text:1.0"},            
            {"draw", "urn:oasis:names:tc:opendocument:xmlns:drawing:1.0"},
            {"fo", "urn:oasis:names:tc:opendocument:xmlns:xsl-fo-compatible:1.0"},
            {"dc", "http://purl.org/dc/elements/1.1/"},
            {"meta", "urn:oasis:names:tc:opendocument:xmlns:meta:1.0"},
            {"number", "urn:oasis:names:tc:opendocument:xmlns:datastyle:1.0"},
            {"presentation", "urn:oasis:names:tc:opendocument:xmlns:presentation:1.0"},
            {"svg", "urn:oasis:names:tc:opendocument:xmlns:svg-compatible:1.0"},
            {"chart", "urn:oasis:names:tc:opendocument:xmlns:chart:1.0"},
            {"dr3d", "urn:oasis:names:tc:opendocument:xmlns:dr3d:1.0"},
            {"math", "http://www.w3.org/1998/Math/MathML"},
            {"form", "urn:oasis:names:tc:opendocument:xmlns:form:1.0"},
            {"script", "urn:oasis:names:tc:opendocument:xmlns:script:1.0"},
            {"ooo", "http://openoffice.org/2004/office"},
            {"ooow", "http://openoffice.org/2004/writer"},
            {"oooc", "http://openoffice.org/2004/calc"},
            {"dom", "http://www.w3.org/2001/xml-events"},
            {"xforms", "http://www.w3.org/2002/xforms"},
            {"xsd", "http://www.w3.org/2001/XMLSchema"},
            {"xsi", "http://www.w3.org/2001/XMLSchema-instance"},
            {"rpt", "http://openoffice.org/2005/report"},
            {"of", "urn:oasis:names:tc:opendocument:xmlns:of:1.2"},
            {"rdfa", "http://docs.oasis-open.org/opendocument/meta/rdfa#"},
            {"config", "urn:oasis:names:tc:opendocument:xmlns:config:1.0"}
        };

        /// <summary>   Gets zip file. </summary>
        ///
        /// <param name="stream">   The stream. </param>
        ///
        /// <returns>   The zip file. </returns>

        private static ZipFile GetZipFile(Stream stream)
        {
            // Read zip stream (.ods file is zip file).
            return ZipFile.Read(stream);
        }

        /// <summary>   Gets zip file. </summary>
        ///
        /// <param name="inputFilePath">    Path to the .ods file. </param>
        ///
        /// <returns>   The zip file. </returns>

        private static ZipFile GetZipFile(string inputFilePath)
        {
            // Read zip file (.ods file is zip file).
            return ZipFile.Read(inputFilePath);
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Reliability", "CA2000:Dispose objects before losing scope")]
        private static XmlDocument GetContentXmlFile(ZipFile zipFile)
        {
            // Get file(in zip archive) that contains data ("content.xml").
            ZipEntry contentZipEntry = zipFile["content.xml"];

            // Extract that file to MemoryStream.
            Stream contentStream = new MemoryStream();
            contentZipEntry.Extract(contentStream);
            _ = contentStream.Seek( 0, SeekOrigin.Begin );

            // Create XmlDocument from MemoryStream (MemoryStream contains content.xml).
            XmlDocument contentXml = new XmlDocument();
            contentXml.Load(contentStream);

            return contentXml;
        }

        /// <summary>   Initializes the XML namespace manager. </summary>
        ///
        /// <param name="xmlDocument">  The XML document. </param>
        ///
        /// <returns>   An XmlNamespaceManager. </returns>

        private static XmlNamespaceManager InitializeXmlNamespaceManager(XmlDocument xmlDocument)
        {
            XmlNamespaceManager nmsManager = new XmlNamespaceManager(xmlDocument.NameTable);

            for (int i = 0; i < Namespaces.GetLength(0); i++)
            {
                nmsManager.AddNamespace(Namespaces[i, 0], Namespaces[i, 1]);
            }

            return nmsManager;
        }

        /// <summary>
        /// Read .ods file and store it in DataSet.
        /// </summary>
        /// <param name="inputFilePath">Path to the .ods file.</param>
        /// <returns>DataSet that represents .ods file.</returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Reliability", "CA2000:Dispose objects before losing scope")]
        public static DataSet ReadOdsFile(string inputFilePath)
        {
            ZipFile odsZipFile = SpreadsheetScribe.GetZipFile(inputFilePath);

            // Get content.xml file
            XmlDocument contentXml = SpreadsheetScribe.GetContentXmlFile(odsZipFile);

            // Initialize XmlNamespaceManager
            XmlNamespaceManager nmsManager = SpreadsheetScribe.InitializeXmlNamespaceManager(contentXml);

            DataSet odsFile = new DataSet(Path.GetFileName(inputFilePath))
            {
                Locale = CultureInfo.CurrentUICulture
            };

            foreach (XmlNode tableNode in SpreadsheetScribe.GetTableNodes(contentXml, nmsManager))
            {
                odsFile.Tables.Add(SpreadsheetScribe.GetSheet(tableNode, nmsManager));
            }

            return odsFile;
        }

        /// <summary>   Gets table nodes. </summary>
        ///
        /// <param name="contentXmlDocument">   The content XML document. </param>
        /// <param name="nmsManager">           Manager for nms. </param>
        ///
        /// <returns>   The table nodes. </returns>

        private static XmlNodeList GetTableNodes(XmlDocument contentXmlDocument, XmlNamespaceManager nmsManager)
        {
            // In ODF sheet is stored in table:table node
            return contentXmlDocument.SelectNodes("/office:document-content/office:body/office:spreadsheet/table:table", nmsManager);
        }

        /// <summary>   Gets a sheet. </summary>
        ///
        /// <param name="tableNode">    The table node. </param>
        /// <param name="nmsManager">   Manager for nms. </param>
        ///
        /// <returns>   The sheet. </returns>

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Reliability", "CA2000:Dispose objects before losing scope")]
        private static DataTable GetSheet(XmlNode tableNode, XmlNamespaceManager nmsManager)
        {
            DataTable sheet = new DataTable(tableNode.Attributes["table:name"].Value)
            {
                Locale = CultureInfo.CurrentUICulture
            };

            XmlNodeList rowNodes = tableNode.SelectNodes("table:table-row", nmsManager);

            int rowIndex = 0;
            foreach (XmlNode rowNode in rowNodes)
            {
                SpreadsheetScribe.GetRow(rowNode, sheet, nmsManager, ref rowIndex);
            }

            return sheet;
        }

        /// <summary>   Gets a row. </summary>
        ///
        /// <param name="rowNode">      The row node. </param>
        /// <param name="sheet">        The sheet. </param>
        /// <param name="nmsManager">   Manager for nms. </param>
        /// <param name="rowIndex">     [in,out] Zero-based index of the row. </param>

        private static void GetRow(XmlNode rowNode, DataTable sheet, XmlNamespaceManager nmsManager, ref int rowIndex)
        {
            XmlAttribute rowsRepeated = rowNode.Attributes["table:number-rows-repeated"];
            if (rowsRepeated == null || Convert.ToInt32(rowsRepeated.Value, CultureInfo.InvariantCulture) == 1)
            {
                while (sheet.Rows.Count < rowIndex)
                {
                    sheet.Rows.Add(sheet.NewRow());
                }

                DataRow row = sheet.NewRow();

                XmlNodeList cellNodes = rowNode.SelectNodes("table:table-cell", nmsManager);

                int cellIndex = 0;
                foreach (XmlNode cellNode in cellNodes)
                {
                    SpreadsheetScribe.GetCell(cellNode, row, nmsManager, ref cellIndex);
                }

                sheet.Rows.Add(row);

                rowIndex++;
            }
            else
            {
                rowIndex += Convert.ToInt32(rowsRepeated.Value, CultureInfo.InvariantCulture);
            }

            // sheet must have at least one cell
            if (sheet.Rows.Count == 0)
            {
                sheet.Rows.Add(sheet.NewRow());
                _ = sheet.Columns.Add();
            }
        }

        /// <summary>   Gets a cell. </summary>
        ///
        /// <param name="cellNode">     The cell node. </param>
        /// <param name="row">          The row. </param>
        /// <param name="namespaceManager">   Manager for namespace. </param>
        /// <param name="cellIndex">    [in,out] Zero-based index of the cell. </param>

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA1801:ReviewUnusedParameters", MessageId = "nmsManager")]
#pragma warning disable IDE0060 // Remove unused parameter
        private static void GetCell(XmlNode cellNode, DataRow row, XmlNamespaceManager namespaceManager, ref int cellIndex)
#pragma warning restore IDE0060 // Remove unused parameter
        {
            XmlAttribute cellRepeated = cellNode.Attributes["table:number-columns-repeated"];

            if (cellRepeated == null)
            {
                DataTable sheet = row.Table;

                while (sheet.Columns.Count <= cellIndex)
                {
                    _ = sheet.Columns.Add();
                }

                row[cellIndex] = SpreadsheetScribe.ReadCellValue(cellNode);

                cellIndex++;
            }
            else
            {
                cellIndex += Convert.ToInt32(cellRepeated.Value, CultureInfo.InvariantCulture);
            }
        }

        /// <summary>   Reads cell value. </summary>
        ///
        /// <param name="cell"> The cell. </param>
        ///
        /// <returns>   The cell value. </returns>

        private static string ReadCellValue(XmlNode cell)
        {
            XmlAttribute cellVal = cell.Attributes["office:value"];

            return cellVal == null ? String.IsNullOrEmpty(cell.InnerText) ? null : cell.InnerText : cellVal.Value;
        }

        /// <summary>
        /// Writes DataSet as .ods file.
        /// </summary>
        /// <param name="odsFile">DataSet that represent .ods file.</param>
        /// <param name="outputFilePath">The name of the file to save to.</param>
        public static void WriteOdsFile(DataSet odsFile, string outputFilePath)
        {
            if (odsFile == null)
            {
                throw new ArgumentNullException(nameof(odsFile));
            }

            // read the embedded resource
            Stream stream = Assembly.GetExecutingAssembly().GetManifestResourceStream("isr.IO.OpenOffice.Spreadsheet.template.ods");
                
            ZipFile templateFile = SpreadsheetScribe.GetZipFile(stream);

            XmlDocument contentXml = SpreadsheetScribe.GetContentXmlFile(templateFile);

            XmlNamespaceManager nmsManager = SpreadsheetScribe.InitializeXmlNamespaceManager(contentXml);

            XmlNode sheetsRootNode = SpreadsheetScribe.GetSheetsRootNodeAndRemoveChildrens(contentXml, nmsManager);

            foreach (DataTable sheet in odsFile.Tables)
            {
                SpreadsheetScribe.SaveSheet(sheet, sheetsRootNode);
            }

            SpreadsheetScribe.SaveContentXml(templateFile, contentXml);

            templateFile.Save(outputFilePath);
        }

        /// <summary>   Gets sheets root node and remove childrens. </summary>
        ///
        /// <param name="contentXml">   The content XML. </param>
        /// <param name="nmsManager">   Manager for nms. </param>
        ///
        /// <returns>   The sheets root node and remove childrens. </returns>

        private static XmlNode GetSheetsRootNodeAndRemoveChildrens(XmlDocument contentXml, XmlNamespaceManager nmsManager)
        {
            XmlNodeList tableNodes = SpreadsheetScribe.GetTableNodes(contentXml, nmsManager);

            XmlNode sheetsRootNode = tableNodes.Item(0).ParentNode;
            // remove sheets from template file
            foreach (XmlNode tableNode in tableNodes)
            {
                _ = sheetsRootNode.RemoveChild( tableNode );
            }

            return sheetsRootNode;
        }

        /// <summary>   Saves a sheet. </summary>
        ///
        /// <param name="sheet">            The sheet. </param>
        /// <param name="sheetsRootNode">   The sheets root node. </param>

        private static void SaveSheet(DataTable sheet, XmlNode sheetsRootNode)
        {
            XmlDocument ownerDocument = sheetsRootNode.OwnerDocument;

            XmlNode sheetNode = ownerDocument.CreateElement("table:table", SpreadsheetScribe.GetNamespaceUri("table"));

            XmlAttribute sheetName = ownerDocument.CreateAttribute("table:name", SpreadsheetScribe.GetNamespaceUri("table"));
            sheetName.Value = sheet.TableName;
            _ = sheetNode.Attributes.Append( sheetName );

            SpreadsheetScribe.SaveColumnDefinition(sheet, sheetNode, ownerDocument);

            SpreadsheetScribe.SaveRows(sheet, sheetNode, ownerDocument);

            _ = sheetsRootNode.AppendChild( sheetNode );
        }

        /// <summary>   Saves a column definition. </summary>
        ///
        /// <param name="sheet">            The sheet. </param>
        /// <param name="sheetNode">        The sheet node. </param>
        /// <param name="ownerDocument">    The document that owns this item. </param>

        private static void SaveColumnDefinition(DataTable sheet, XmlNode sheetNode, XmlDocument ownerDocument)
        {
            XmlNode columnDefinition = ownerDocument.CreateElement("table:table-column", SpreadsheetScribe.GetNamespaceUri("table"));

            XmlAttribute columnsCount = ownerDocument.CreateAttribute("table:number-columns-repeated", SpreadsheetScribe.GetNamespaceUri("table"));
            columnsCount.Value = sheet.Columns.Count.ToString(CultureInfo.InvariantCulture);
            _ = columnDefinition.Attributes.Append( columnsCount );

            _ = sheetNode.AppendChild( columnDefinition );
        }

        /// <summary>   Saves the rows. </summary>
        ///
        /// <param name="sheet">            The sheet. </param>
        /// <param name="sheetNode">        The sheet node. </param>
        /// <param name="ownerDocument">    The document that owns this item. </param>

        private static void SaveRows(DataTable sheet, XmlNode sheetNode, XmlDocument ownerDocument)
        {
            DataRowCollection rows = sheet.Rows;
            for (int i = 0; i < rows.Count; i++)
            {
                XmlNode rowNode = ownerDocument.CreateElement("table:table-row", SpreadsheetScribe.GetNamespaceUri("table"));

                SpreadsheetScribe.SaveCell(rows[i], rowNode, ownerDocument);

                _ = sheetNode.AppendChild( rowNode );
            }
        }

        /// <summary>   Saves a cell. </summary>
        ///
        /// <param name="row">              The row. </param>
        /// <param name="rowNode">          The row node. </param>
        /// <param name="ownerDocument">    The document that owns this item. </param>

        private static void SaveCell(DataRow row, XmlNode rowNode, XmlDocument ownerDocument)
        {
            object[] cells = row.ItemArray;

            for (int i = 0; i < cells.Length; i++)
            {
                XmlElement cellNode = ownerDocument.CreateElement("table:table-cell", SpreadsheetScribe.GetNamespaceUri("table"));

                if (row[i] != DBNull.Value)
                {
                    // We save values as text (string)
                    XmlAttribute valueType = ownerDocument.CreateAttribute("office:value-type", SpreadsheetScribe.GetNamespaceUri("office"));
                    valueType.Value = "string";
                    _ = cellNode.Attributes.Append( valueType );

                    XmlElement cellValue = ownerDocument.CreateElement("text:p", SpreadsheetScribe.GetNamespaceUri("text"));
                    cellValue.InnerText = row[i].ToString();
                    _ = cellNode.AppendChild( cellValue );
                }

                _ = rowNode.AppendChild( cellNode );
            }
        }

        /// <summary>   Saves a content XML. </summary>
        ///
        /// <param name="templateFile"> The template file. </param>
        /// <param name="contentXml">   The content XML. </param>

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Reliability", "CA2000:Dispose objects before losing scope")]
        private static void SaveContentXml(ZipFile templateFile, XmlDocument contentXml)
        {
            templateFile.RemoveEntry("content.xml");

            MemoryStream memStream = new MemoryStream();
            contentXml.Save(memStream);
            _ = memStream.Seek( 0, SeekOrigin.Begin );

            _ = templateFile.AddEntry( "content.xml", memStream );
        }

        /// <summary>   Gets namespace URI. </summary>
        ///
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        ///
        /// <param name="prefix">   The prefix. </param>
        ///
        /// <returns>   The namespace URI. </returns>

        private static string GetNamespaceUri(string prefix)
        {
            for (int i = 0; i < Namespaces.GetLength(0); i++)
            {
                if (Namespaces[i, 0] == prefix)
                {
                    return Namespaces[i, 1];
                }
            }

            throw new InvalidOperationException("Can't find that namespace URI");
        }
    }
}
