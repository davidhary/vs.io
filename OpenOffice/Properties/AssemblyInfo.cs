using System;
using System.Reflection;
using System.Runtime.InteropServices;

[assembly: AssemblyTitle("OpenOffice")]
[assembly: AssemblyProduct("isr.IO.OpenOffice")]
[assembly: AssemblyDescription("Reads and Writes Open Office Documents")]
[assembly: AssemblyConfiguration("")]
[assembly: CLSCompliant(true)]
[assembly: ComVisible(false)]
