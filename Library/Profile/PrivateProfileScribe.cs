using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace isr.IO
{

    /// <summary> Reads and writes private INI Settings (INI) files. </summary>
    /// <remarks>
    /// Use this class to read and write Settings from a private INI file. <para>
    /// INI Settings (INI) files typically include information about your application in strings that
    /// are called Profile Strings. Such information is used most often to determine how programs are
    /// run by setting specific properties in these programs with information from the INI Settings
    /// (INI) file.  Thus determine the profile of the programs. </para><para>
    /// With the ISR INI settings Scriber Class you can define the INI Settings (INI) file name and
    /// write and read from the INI Settings file information of any variable type.
    /// In addition, you can write or read lists of data.</para>  <para>
    /// (c) 2010 Integrated Scientific Resources, Inc. All rights reserved. </para><para>
    /// Licensed under The MIT License. </para><para>
    /// David, 2096-09-12, 1.0.0001. Add Boolean type.  </para><para>
    /// David, 2097-04-27, 1.0.0002. Add object input and output.  </para><para>
    /// David, 2098-12-31, 1.0.0003. Convert from ISR INI class and remove object and collection methods.  </para><para>
    /// David, 2099-06-06, 1.1.0000. Update error handling. </para><para>
    /// David, 2099-09-27, 1.2.0000. Remove reference to the application object.  </para><para>
    /// David, 2001-08-18, 1.2.0000. New notation.  </para><para>
    /// David, 2003-09-02, 2.0.1340. adapt for .Net.  </para><para>
    /// David, 2003-09-11, 2.0.1349. rename IniSettings.  </para><para>
    /// David, 2004-03-01, 2.0.1521. rename Scriber.  </para><para>
    /// David, 2096-08-23, 1.0.0000. </para>
    /// </remarks>
    public class PrivateProfileScribe : IDisposable
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary>
        /// Initializes a new instance of the <see cref="PrivateProfileScribe" /> class.
        /// </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        public PrivateProfileScribe() : this(BuildDefaultFileName())
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="PrivateProfileScribe" /> class.
        /// </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <param name="filePath"> The file path. This also serves as the instance name. </param>
        public PrivateProfileScribe(string filePath) : base()
        {
            this.FilePath = filePath;
            this.DefaultFileName = BuildDefaultFileName();
            this.SettingType = Type.GetType("System.Int32");
        }

        /// <summary> Calls <see cref="M:Dispose(Boolean Disposing)"/> to cleanup. </summary>
        /// <remarks>
        /// Do not make this method Overridable (virtual) because a derived class should not be able to
        /// override this method.
        /// </remarks>
        public void Dispose()
        {

            // Do not change this code.  Put cleanup code in Dispose(Boolean) below.

            // this disposes all child classes.
            this.Dispose(true);

            // Take this object off the finalization(Queue) and prevent finalization code 
            // from executing a second time.
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// Gets the dispose status sentinel of the base class.  This applies to the derived class
        /// provided proper implementation.
        /// </summary>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <value> The is disposed. </value>
        protected bool IsDisposed { get; set; }

        /// <summary>
        /// Releases the unmanaged resources used by the <see cref="T:PrivateProfileScriber" />
        /// and its child controls and optionally releases the managed resources.
        /// </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <param name="disposing"> <c>True</c> to release both managed and unmanaged resources;
        /// <c>False</c> to release only unmanaged
        /// resources when called from the runtime
        /// finalize. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification = "Exception is published")]
        [DebuggerNonUserCode()]
        protected virtual void Dispose(bool disposing)
        {
            try
            {
                if (!this.IsDisposed && disposing)
                {
                    this.FilePath = string.Empty;
                    this.SectionName = string.Empty;
                    this.SettingName = string.Empty;
                    this._SettingValue = null;
                }
            }
            finally
            {
                this.IsDisposed = true;
            }
        }

        /// <summary>
        /// This destructor will run only if the Dispose method does not get called. It gives the base
        /// class the opportunity to finalize. Do not provide destructors in types derived from this
        /// class.
        /// </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        ~PrivateProfileScribe()
        {
            // Do not re-create Dispose clean-up code here.
            // Calling Dispose(false) is optimal for readability and maintainability.
            this.Dispose(false);
        }

        #endregion

        #region " BASE METHODS AND PROPERTIES "

        /// <summary> Gets the full name of the file. </summary>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <value> The name of the file. </value>
        public string FilePath { get; set; }

        /// <summary> Validates the name of the file. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <param name="filePath"> The file path. </param>
        /// <returns> <c>True</c> if file name is valid, <c>False</c> otherwise. </returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification = "Exception is published")]
        public static bool ValidateFileName(string filePath)
        {
            try
            {
                // Check that the file name is legal.
                _ = System.IO.File.Exists( filePath );
                return true;
            }
            catch
            {
                return false;
            }
        }

        /// <summary> Builds default file name. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <returns> A String. </returns>
        public static string BuildDefaultFileName()
        {
            // System.Windows.Fo rms.Application.ExecutablePath & PrivateProfileScriber.DefaultExtension
            return System.IO.Path.Combine(My.MyProject.Application.Info.DirectoryPath, $"{My.MyProject.Application.Info.AssemblyName}{DefaultExtension}");
        }

        /// <summary> Gets the default name of the file. </summary>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <value> The default name of the file. </value>
        public string DefaultFileName { get; set; }

        #endregion

        #region " SHARED "

        /// <summary> Holds the default extension of an INI file. </summary>
        public const string DefaultExtension = ProfileScribe.DefaultExtension;

        #endregion

        #region " METHODS "

        /// <summary> Reads a list of INI Settings from a INI Settings file. </summary>
        /// <remarks>
        /// The format of the list of INI Settings is SettingName# (e.g., Setting1, Setting2,
        /// ....).<para>
        /// Reads all the INI Settings in the list in sequence until no more INI Settings are
        /// found.</para> <para>
        /// The list is dimensioned starting with First Item, which defaults to 1.  For example, the
        /// first item value must be set to 0 in order to read INI Settings Setting0, Setting1,..'  or to
        /// -1 to read the list ordered as -1,0,1,...</para>
        /// </remarks>
        /// <param name="firstItemIndex"> is an Int32 expression that specifies the index of the first list
        /// item (default is 1) </param>
        /// <returns> The list. </returns>
        /// <example>
        /// This example writes a list of INI Settings to an INI file and then reads the list from the
        /// file.
        /// <code>
        /// Sub Form_Click
        /// ' Declare some data storage
        /// Dim i as Int32
        /// ' Create an instance of the ISR INI Settings Scriber Class
        /// Dim ini as isr.Configuration.IniSettings.Scriber
        /// ini = New isr.Configuration.IniSettings.Scriber
        /// ' Create data to write to the INI Settings file
        /// Dim items As New List(Of String)
        /// For i = 0 to UBound(sngList)
        /// items.Add(Log(convert.ToSingle(i+1))).ToString)
        /// Next i
        /// ' Set the INI Settings file name, section name, and setting name
        /// ini.FilePath = ini.DefaultFilePath()
        /// ini.SectionName = "Section1"
        /// ini.SettingName = "Setting"
        /// ' Write the list to the file.
        /// ini.WriteList(items)
        /// ' Read the list from the file
        /// items = ini.ReadList()
        /// End Sub
        /// </code>
        /// To run This example, paste the code fragment into a Windows Form class. Run the program by
        /// pressing F5, and then click on the form
        /// </example>
        /// <seealso cref="PrivateProfileScribe.FilePath"/>
        public IEnumerable<string> ReadList(int firstItemIndex)
        {
            var items = new List<string>();
            int currentItem = firstItemIndex;

            // Set default to Empty. This ensures
            // that we end reading when the last item is Empty
            this.DefaultValue = null;
            string itemValue;
            // Loop through the list data.
            do
            {

                // Read data from the INI Settings file
                itemValue = Convert.ToString( this.ListSetting(currentItem), System.Globalization.CultureInfo.CurrentCulture);
                if (itemValue.Length > 0)
                {

                    // add item to the list
                    items.Add(itemValue);

                    // Increment item count
                    currentItem += 1;
                }
            }
            while (itemValue.Length != 0);

            // return the list
            return items;
        }

        /// <summary> Reads a list of INI Settings from a INI Settings file. </summary>
        /// <remarks>
        /// The format of the list of INI Settings is SettingName# (e.g., Setting1, Setting2,
        /// ....).<para>
        /// Reads all the INI Settings in the list in sequence until no more INI Settings are
        /// found.</para> <para>
        /// The list is dimensioned starting with First Item Index, which defaults to 1.  For example,
        /// the first item value must be set to 0 in order to read INI Settings Setting0, Setting1,..'
        /// or to
        /// -1 to read the list ordered as -1,0,1,...</para>
        /// </remarks>
        /// <returns> The list. </returns>
        /// <seealso cref="PrivateProfileScribe.ReadList"/>
        public IEnumerable<string> ReadList()
        {
            return this.ReadList(1);
        }

        /// <summary> Writes a list of INI Settings to a INI Settings file. </summary>
        /// <remarks>
        /// The format of the list of INI Settings is SettingName# (e.g., Setting1, Setting2,
        /// ....).<para>
        /// Reads all the INI Settings in the list in sequence until no more INI Settings are
        /// found.</para> <para>
        /// The list is dimensioned starting with First Item Index, which defaults to 1.  For example,
        /// the first item value must be set to 0 in order to read INI Settings Setting0, Setting1,..' or
        /// to
        /// -1 to read the list ordered as -1,0,1,...</para>
        /// </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="values">         The values. </param>
        /// <param name="firstItemIndex"> is an Int32 expression that specifies the index of the first list
        /// item (default is 1) </param>
        /// <example cref="PrivateProfileScribe.ReadList">  </example>
        /// <seealso cref="PrivateProfileScribe.ReadList"/>
        public void WriteList(IEnumerable<string> values, int firstItemIndex)
        {
            if (values is null)
                throw new ArgumentNullException(nameof(values));
            int itemIndex = firstItemIndex;

            // Loop through the list data.
            foreach (var itemValue in values)
            {
                // Write the list setting
                this.ListSettingSetter(itemIndex, itemValue);
                itemIndex += 1;
            }
        }

        /// <summary> Reads a whole section from the INI file. </summary>
        /// <remarks>
        /// Use This method to read a section from an INI file.  The section is returned in an enumerable
        /// list of string values where the section values are delimited by the null character (ASCII 0)
        /// </remarks>
        /// <returns>
        /// An enumerator that allows foreach to be used to process read section in this collection.
        /// </returns>
        public IEnumerable<string> ReadSection()
        {
            return ProfileScribe.ReadSection( this.FilePath, this.SectionName );
        }

        /// <summary> Writes a list of INI Settings to a INI Settings file. </summary>
        /// <remarks>
        /// The format of the list of INI Settings is SettingName# (e.g., Setting1, Setting2,
        /// ....).<para>
        /// Reads all the INI Settings in the list in sequence until no more INI Settings are
        /// found.</para> <para>
        /// The list is dimensioned starting with First Item, which defaults to 1.  For example, the
        /// first item value must be set to 0 in order to read INI Settings Setting0, Setting1,..'  or to
        /// -1S to read the list ordered as -1,0,1,...</para>
        /// </remarks>
        /// <param name="values"> The values. </param>
        /// <example cref="PrivateProfileScribe.ReadList">  </example>
        /// <seealso cref="PrivateProfileScribe.ReadList"/>
        public void WriteList(IEnumerable<string> values)
        {
            this.WriteList(values, 1);
        }

        /// <summary> Writes a section to the INI file. </summary>
        /// <remarks> Use This method to write a section to an INI file. </remarks>
        /// <param name="items"> The items. </param>
        /// <returns> <c>True</c> if success; Otherwise, <c>False</c>. </returns>
        public bool WriteSection(IEnumerable<string> items)
        {
            return ProfileScribe.WriteSection( this.FilePath, this.SectionName, items);
        }

        #endregion

        #region " PROPERTIES "

        /// <summary>
        /// Gets the default value that is set when reading INI Settings from the INI Settings file. The
        /// default value is read if the setting is Empty.
        /// </summary>
        /// <remarks>
        /// Use This property to set or get the default data type and value of the INI settings.
        /// </remarks>
        /// <value>
        /// <c>DefaultValue</c> is an Object property that can be read from or written to (read or write)
        /// that specifies the default value and type of the setting in the INI Settings file.
        /// </value>
        /// <seealso cref="PrivateProfileScribe.FilePath"/>
        public object DefaultValue { get; set; }

        /// <summary> Gets the name of the setting. </summary>
        /// <remarks> Use This property to set or get the name of the INI Settings string. </remarks>
        /// <value>
        /// <c>SettingsName</c> is a String property that can be read from or written to (read or write)
        /// that specifies the name of the INI Settings string in the INI Settings file.
        /// </value>
        /// <seealso cref="PrivateProfileScribe.FilePath"/>
        /// <example>
        /// This example sets a INI Settings file name, writes INI Settings strings to the file, and then
        /// reads the information from back.
        /// <code>
        /// Sub Form_Click
        /// ' Declare some variables.
        /// Dim settingValue As Variant
        /// ' instantiate the ISR INI Settings Scriber Class
        /// Dim ini as isr.Configuration.IniSettings.Scriber
        /// ini = New isr.Configuration.IniSettings.Scriber
        /// ' Set to trap any errors
        /// Try
        /// ' Set the INI Settings file name
        /// ini.FilePath = ini.DefaultFilePath()
        /// ' Set the INI Settings Section
        /// ini.SectionName = "Section1"
        /// ' Write a string to the file String setting
        /// ini.SettingName = "String"
        /// ini.SettingValue = "A String"
        /// ' Write an Int32 value
        /// ini.SettingName = "Int32"
        /// ini.SettingValue = Convert.ToInt32(0)
        /// ' Write an Double value
        /// ini.SettingName = "Double"
        /// ini.settingFormat = "##0.000"
        /// ini.SettingValue = convert.ToDouble(1.11)
        /// ' Write two list items
        /// ini.SettingName = "List"
        /// ini.ListSetting(1) = "One"
        /// ini.ListSetting(2) = "Two"
        /// ' Read the string from the file
        /// ini.SettingName = "String"
        /// Me.Print "String: "; ini.SettingValue
        /// ' Read an Int32 value
        /// ini.SettingName = "Int32"
        /// ini.DefaultValue= Convert.ToInt32(0)
        /// Me.Print "Int32: "; ini.SettingValue
        /// ' Read a double
        /// ini.SettingName = "Double"
        /// Me.Print "Double: "; ini.SettingValue
        /// ' Read two list items
        /// ini.SettingName = "List"
        /// Me.Print ini.ListSetting(1)
        /// Me.Print ini.ListSetting(2)
        /// ' Read the entire section
        /// Me.Print "Section: "; ini.sectionValue
        /// Catch e As Exception
        /// ' respond to any file name errors.
        /// MsgBox e.ToString
        /// End Try
        /// End Sub  </code>
        /// To run This example, paste the code fragment into a Windows Form class.
        /// Run the program by pressing F5, and then click on the form.
        /// </example>
        public string SettingName { get; set; }

        /// <summary> Gets the name of the section. </summary>
        /// <remarks> Use This property to set or get the name of the INI Settings section. </remarks>
        /// <value>
        /// <c>SectionName</c> is a String property that can be read from or written to (read or write)
        /// that specifies the name of the section in the INI Settings file.
        /// </value>
        /// <seealso cref="PrivateProfileScribe.SettingName"/>
        public string SectionName { get; set; }

        /// <summary> Gets the section items. </summary>
        /// <remarks>
        /// Use This property to read or write an entire section to the INI Settings file.  When writing,
        /// This property replaces the entire section erasing any information that was present in the
        /// section and replacing it with new information.  Treat This property tenderly! <para>
        /// When reading a section, returns a string with all the INI Settings in the section.</para>
        /// </remarks>
        /// <value> A list of names of the sections. </value>
        /// <seealso cref="PrivateProfileScribe.SettingName"/>
        public IEnumerable<string> SectionNames =>
                // If we have a file name, get the names
                ProfileScribe.ReadSection( this.FilePath, this.SectionName );

        /// <summary> The setting value. </summary>
        private object _SettingValue;

        /// <summary> Gets or sets the INI Settings value. </summary>
        /// <remarks>
        /// Use This property to read or write a INI Settings value. The setting value is read from the
        /// INI Settings file specified in the <see cref="FilePath">File Path</see> property.  The value
        /// returned depends on the variable type of the <see cref="DefaultValue">Default Value</see>
        /// property.  If the <see cref="DefaultValue">Default Value</see> property is Empty or Null, the
        /// type is set to String. The value that is written to the INI Settings file is determined by
        /// the settingFormat property.
        /// </remarks>
        /// <value>
        /// <c>SettingValue</c> is a String property that can be read from or written to (read or write)
        /// that specifies the INI Settings value.
        /// </value>
        /// <seealso cref="PrivateProfileScribe.SettingName"/>
        public object SettingValue
        {
            get
            {
                // Read the setting.
                this._SettingValue = this.DefaultValue is null ? this.SettingType is null ? ProfileScribe.Read( this.FilePath, this.SectionName, this.SettingName, string.Empty) : ProfileScribe.Read( this.FilePath, this.SectionName, this.SettingName, this.SettingType ) : ProfileScribe.Read( this.FilePath, this.SectionName, this.SettingName, this.DefaultValue );
                // return the value
                return this._SettingValue;
            }

            set
            {
                _ = ProfileScribe.Write( this.FilePath, this.SectionName, this.SettingName, value );
                // Set the value of the private data member
                this._SettingValue = value;
            }
        }

        /// <summary> Gets the INI Settings type. </summary>
        /// <remarks> Use This property to define the INI Settings type. </remarks>
        /// <value>
        /// <c>SettingType</c> is a Type property that can be read from or written to (read or write)
        /// that specifies the INI Settings type.
        /// </value>
        /// <seealso cref="PrivateProfileScribe.SettingName"/>
        public Type SettingType { get; set; }

        /// <summary> This private property returns the name of the ini settings list. </summary>
        /// <remarks>
        /// Use This property to get the name of the list setting for reading or write a list item.
        /// </remarks>
        /// <param name="itemNumber"> Is an Int32 expression that specifies the item number in the
        /// list. </param>
        /// <value> <c>_listSettingName</c> is a read only string property. </value>
        private string ListSettingName(int itemNumber)
        {
            return string.Format(System.Globalization.CultureInfo.CurrentCulture, "{0}{1}", this.SettingName, itemNumber);
        }

        /// <summary> Gets the INI Settings list value. </summary>
        /// <remarks>
        /// Use This property to read or write a settings to a section that consists of a serial list of
        /// INI Settings such as Setting1, Setting2, .... <para>
        /// The value returned depends on the variable type of the <see cref="DefaultValue">Default
        /// Value</see> property.  If the
        /// <see cref="DefaultValue">Default Value</see> property is Empty or Null, the type is set to
        /// String. </para> <para>
        /// If the setting number does not exist, the property value is set to the Default Value.
        /// </para> <para>
        /// The format of the setting value, for data types of single, double, date, or currency setting,
        /// is set by the settingFormat property. </para>
        /// </remarks>
        /// <param name="itemNumber"> is an Int32 expression that specifies the serial number of the
        /// setting in the list. </param>
        /// <value>
        /// <c>ListSetting</c> is a String property that can be read from or written to (read or write)
        /// that specifies the INI Settings value in a setting list.
        /// </value>
        public object ListSetting(int itemNumber)
        {
            this._SettingValue = this.DefaultValue is null ? ProfileScribe.Read( this.FilePath, this.SectionName, this.ListSettingName(itemNumber), this.DefaultValue ) : ProfileScribe.Read( this.FilePath, this.SectionName, this.ListSettingName(itemNumber), this.SettingType );
            return this._SettingValue;
            // use the private writeSetting to write the setting to the INI Settings file.
            // Set the private data member value
        }

        /// <summary> Sets the INI Settings list value. </summary>
        /// <remarks>   David, 2020-10-09. </remarks>
        /// <param name="itemNumber">   Is an Int32 expression that specifies the item number in the
        ///                             list. </param>
        /// <param name="value">        The value. </param>
        public void ListSettingSetter(int itemNumber, object value)
        {
            _ = ProfileScribe.Write( this.FilePath, this.SectionName, this.ListSettingName( itemNumber ), value );
            this._SettingValue = value;
        }

        #endregion

    }
}
