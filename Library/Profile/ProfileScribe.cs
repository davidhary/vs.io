using System;
using System.Collections.Generic;
using System.Diagnostics;
using Microsoft.VisualBasic.CompilerServices;

namespace isr.IO
{

    /// <summary> Reads and writes private and public profile (INI Settings) files. </summary>
    /// <remarks>
    /// Use this class to read and write INI Settings (INI) file.<para>
    /// With the ISR INI settings Scribe Class you can write and read information from INI files.
    /// Both private (yours) and public (WIN.INI) files are accessible.</para><para>
    /// INI Settings (INI) files typically include information about your application in strings that
    /// are called Profile Strings. Such information is used most often to determine how programs are
    /// run by setting specific properties in these programs with information from the INI Settings
    /// (INI) file.  Thus determine the profile of the programs. </para><para>
    /// With the ISR INI settings Scribe Class you can define the INI Settings (INI) file name and
    /// write and read from the INI Settings file information of any variable type.
    /// In addition, you can write or read lists of data.</para>  <para>
    /// (c) 2006 Integrated Scientific Resources, Inc. All rights reserved. </para><para>
    /// Licensed under The MIT License. </para><para>
    /// David, 2006-04-20, 1.0.2301.x. </para>
    /// </remarks>
    public class ProfileScribe : IDisposable
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Initializes a new instance of the <see cref="ProfileScribe" /> class. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        public ProfileScribe() : this(BuildDefaultFileName())
        {
        }

        /// <summary> Initializes a new instance of the <see cref="ProfileScribe" /> class. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <param name="filePath"> The file path. This also serves as the instance name. </param>
        public ProfileScribe(string filePath) : base()
        {
            this.FilePath = filePath;
            this.DefaultFileName = BuildDefaultFileName();
        }

        /// <summary> Calls <see cref="M:Dispose(Boolean Disposing)"/> to cleanup. </summary>
        /// <remarks>
        /// Do not make this method Overridable (virtual) because a derived class should not be able to
        /// override this method.
        /// </remarks>
        public void Dispose()
        {

            // Do not change this code.  Put cleanup code in Dispose(Boolean) below.

            // this disposes all child classes.
            this.Dispose(true);

            // Take this object off the finalization(Queue) and prevent finalization code 
            // from executing a second time.
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// Gets the dispose status sentinel of the base class.  This applies to the derived class
        /// provided proper implementation.
        /// </summary>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <value> The is disposed. </value>
        protected bool IsDisposed { get; set; }

        /// <summary>
        /// Releases the unmanaged resources used by the <see cref="T:ProfileScribe" />
        /// and its child controls and optionally releases the managed resources.
        /// </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <param name="disposing"> <c>True</c> to release both managed and unmanaged resources;
        /// <c>False</c> to release only unmanaged
        /// resources when called from the runtime
        /// finalize. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification = "Exception is published")]
        [DebuggerNonUserCode()]
        protected virtual void Dispose(bool disposing)
        {
            try
            {
                if (!this.IsDisposed && disposing)
                {
                    this.FilePath = string.Empty;
                    this.SectionName = string.Empty;
                    this.SettingName = string.Empty;
                }
            }
            finally
            {
                this.IsDisposed = true;
            }
        }

        /// <summary>
        /// This destructor will run only if the Dispose method does not get called. It gives the base
        /// class the opportunity to finalize. Do not provide destructors in types derived from this
        /// class.
        /// </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        ~ProfileScribe()
        {
            // Do not re-create Dispose clean-up code here.
            // Calling Dispose(false) is optimal for readability and maintainability.
            this.Dispose(false);
        }

        #endregion

        #region " FILE "

        /// <summary> Gets the full name of the file. </summary>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <value> The name of the file. </value>
        public string FilePath { get; set; }

        /// <summary> Queries if a given file exists. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        public bool FileExists()
        {
            return FileExists( this.FilePath );
        }

        /// <summary> Queries if a given filename is valid and the file exists . </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <param name="filePath"> The file path. </param>
        /// <returns> <c>True</c> if file name is valid, <c>False</c> otherwise. </returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification = "Exception is published")]
        public static bool FileExists(string filePath)
        {
            try
            {
                // Check that the file exists
                return System.IO.File.Exists(filePath);
            }
            catch
            {
                return false;
            }
        }

        /// <summary> Validates the given file name. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <param name="filePath"> The file path. </param>
        /// <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification = "Exception is published")]
        public static bool ValidateFileName(string filePath)
        {
            try
            {
                // Check that the file exists
                return System.IO.File.Exists(filePath);
                return true;
            }
            catch
            {
                return false;
            }
        }

        /// <summary> Builds default file name. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <returns> A String. </returns>
        public static string BuildDefaultFileName()
        {
            // System.Windows.Fo rms.Application.ExecutablePath & PrivateProfileScribe.DefaultExtension
            // System.Windows.Fo rms.Application.ExecutablePath & ProfileScribe.DefaultExtension)
            return $@"{My.MyProject.Application.Info.DirectoryPath}\{My.MyProject.Application.Info.AssemblyName}{DefaultExtension}";
        }

        /// <summary> Gets the default name of the file. </summary>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <value> The default name of the file. </value>
        public string DefaultFileName { get; set; }

        /// <summary> Holds the default extension of an INI file. </summary>
        public const string DefaultExtension = ".ini";

        /// <summary> Returns a default INI Settings file name. </summary>
        /// <remarks>
        /// Use this method to get the default name for the executing assembly. The file name defaults
        /// to: 'executable file name'.ini, e.g., ConfigurationTester.exe.ini.
        /// </remarks>
        /// <returns>
        /// A <see cref="System.String">String</see> value consisting of the file name with the extension
        /// .INI.
        /// </returns>
        /// <seealso cref="ProfileScribe.FilePath"/>
        public static string DefaultFilePath()
        {
            // Return the default file name
            // Return System.Windows.Fo rms.Application.ExecutablePath & ProfileScribe.DefaultExtension
            return BuildDefaultFileName();
        }

        #endregion

        #region " READ SECTION "

        /// <summary> Reads a whole section from the INI file. </summary>
        /// <remarks>
        /// Use This method to read a section from an INI file.  The section is returned in an enumerable
        /// of string values where the section values are delimited by the null character (ASCII 0)
        /// </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="filePath">    A <see cref="System.String">String</see> expression that
        /// specifies the INI file name. Specify the empty file name to read
        /// from WIN.INI. </param>
        /// <param name="sectionName"> A <see cref="System.String">String</see> expression that
        /// specifies the section
        /// which to read. </param>
        /// <returns>
        /// An enumerator that allows foreach to be used to process read section in this collection.
        /// </returns>
        public static IEnumerable<string> ReadSection(string filePath, string sectionName)
        {
            if (filePath is null)
                throw new ArgumentNullException(nameof(filePath));
            if (string.IsNullOrWhiteSpace(sectionName))
                throw new ArgumentNullException(nameof(sectionName));
            var items = new List<string>();
            var buffer = new byte[32769];
            System.Text.StringBuilder sb;
            int i;
            int bufferLength = filePath.Length == 0 ? SafeNativeMethods.GetProfileSection(sectionName, buffer, buffer.GetUpperBound(0)) : SafeNativeMethods.GetPrivateProfileSection(sectionName, buffer, buffer.GetUpperBound(0), filePath);
            // read the section
            if (bufferLength > 0)
            {
                sb = new System.Text.StringBuilder();
                var loopTo = bufferLength - 1;
                for (i = 0; i <= loopTo; i++)
                {
                    // separate strings delimited by the null character (ASCII 0)
                    if (buffer[i] != 0)
                    {
                        _ = sb.Append( Convert.ToChar( buffer[i] ) );
                    }
                    else if (sb.Length > 0)
                    {
                        items.Add(sb.ToString());
                        sb = new System.Text.StringBuilder();
                    }
                }
            }

            return items;
        }

        /// <summary> Reads all the section names from the INI file. </summary>
        /// <remarks>
        /// Use This method to read section names from an INI file.  The names are returned in an
        /// enumerable of string values.
        /// </remarks>
        /// <param name="filePath"> A <see cref="System.String">String</see> expression that specifies
        /// the INI file name. </param>
        /// <returns>
        /// An enumerator that allows foreach to be used to process read section names in this .
        /// </returns>
        public static IEnumerable<string> ReadSectionNames(string filePath)
        {
            var sectionNames = new List<string>();
            var buffer = new byte[32769];
            System.Text.StringBuilder sb;
            int i;
            int bufferLength = string.IsNullOrWhiteSpace(filePath) ? SafeNativeMethods.GetPrivateProfileSectionNames(buffer, buffer.GetUpperBound(0), PublicProfileFilePath) : SafeNativeMethods.GetPrivateProfileSectionNames(buffer, buffer.GetUpperBound(0), filePath);
            // read the section names
            if (bufferLength > 0)
            {
                sb = new System.Text.StringBuilder();
                var loopTo = bufferLength - 1;
                for (i = 0; i <= loopTo; i++)
                {
                    // separate sections along the null character.
                    if (buffer[i] != 0)
                    {
                        _ = sb.Append( Convert.ToChar( buffer[i] ) );
                    }
                    else if (sb.Length > 0)
                    {
                        sectionNames.Add(sb.ToString());
                        sb = new System.Text.StringBuilder();
                    }
                }
            }

            return sectionNames;
        }

        #endregion

        #region " READ SECTION SETTING "

        /// <summary> Reads a string value from the INI file. </summary>
        /// <remarks> Use This method to read a String value from an INI file. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="filePath">    A <see cref="System.String">String</see> expression that
        /// specifies the INI file name. Use an empty file name to read from
        /// Win.ini. </param>
        /// <param name="sectionName"> A <see cref="System.String">String</see> expression that
        /// specifies the section in the INI file from which to read the
        /// settings. </param>
        /// <param name="keyName">     A <see cref="System.String">String</see> expression that
        /// specifies a Key in the section from which to read the settings.
        /// </param>
        /// <returns> A <see cref="System.String">String</see> data type. </returns>
        public static string Read(string filePath, string sectionName, string keyName)
        {
            return filePath is null
                ? throw new ArgumentNullException(nameof(filePath))
                : string.IsNullOrWhiteSpace(sectionName)
                ? throw new ArgumentNullException(nameof(sectionName))
                : keyName is null ? throw new ArgumentNullException(nameof(keyName)) : Read(filePath, sectionName, keyName, string.Empty);
        }

        /// <summary> Reads a string value from the INI file. </summary>
        /// <remarks>
        /// Use This method to read a String value from an INI file. <para>
        /// David, 2005-09-20, 2.0.2099. Return string.empty with zero characters. </para>
        /// </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="filePath">     A <see cref="System.String">String</see> expression that
        /// specifies the INI file name. Use an empty file name to read from
        /// Win.ini. </param>
        /// <param name="sectionName">  A <see cref="System.String">String</see> expression that
        /// specifies the section in the INI file from which to read the
        /// settings. </param>
        /// <param name="keyName">      A <see cref="System.String">String</see> expression that
        /// specifies a Key in the section from which to read the settings.
        /// </param>
        /// <param name="defaultValue"> A <see cref="System.String">String</see> expression that
        /// specifies a default value to return if the key is not found.
        /// </param>
        /// <returns> A <see cref="System.String">String</see> data type. </returns>
        public static string Read(string filePath, string sectionName, string keyName, string defaultValue)
        {
            if (filePath is null)
                throw new ArgumentNullException(nameof(filePath));
            if (string.IsNullOrWhiteSpace(sectionName))
                throw new ArgumentNullException(nameof(sectionName));
            if (keyName is null)
                throw new ArgumentNullException(nameof(keyName));
            if (defaultValue is null)
                throw new ArgumentNullException(nameof(defaultValue));
            _ = new string( ' ', SettingsBufferSize );
            string buffer = new string('\0', SettingsBufferSize);
            if (filePath.Length > 0)
            {
                int length = SafeNativeMethods.GetPrivateProfileString(sectionName, keyName, defaultValue, buffer, buffer.Length, filePath);
                return length > 0 ? buffer.Substring(0, length) : defaultValue;
            }
            else
            {
                return SafeNativeMethods.GetProfileString(sectionName, keyName, defaultValue, buffer, SettingsBufferSize) > 0 ? buffer.ToString() : defaultValue;
            }
        }

        /// <summary> Reads using the GetPrivateProfileStringA entry point. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="filePath">     A <see cref="System.String">String</see> expression that
        /// specifies the INI file name. Use an empty file name to read from
        /// Win.ini. </param>
        /// <param name="sectionName">  A <see cref="System.String">String</see> expression that
        /// specifies the section in the INI file from which to read the
        /// settings. </param>
        /// <param name="keyName">      A <see cref="System.String">String</see> expression that
        /// specifies a Key in the section from which to read the settings.
        /// </param>
        /// <param name="defaultValue"> A <see cref="System.String">String</see> expression that
        /// specifies a default value to return if the key is not found.
        /// </param>
        /// <returns> a. </returns>
        [Obsolete("Using GetPrivateProfileStringA fails reading a string such as 2019-07-20'; it reads only 7")]
        public static string ReadA(string filePath, string sectionName, string keyName, string defaultValue)
        {
            if (filePath is null)
                throw new ArgumentNullException(nameof(filePath));
            if (string.IsNullOrWhiteSpace(sectionName))
                throw new ArgumentNullException(nameof(sectionName));
            if (keyName is null)
                throw new ArgumentNullException(nameof(keyName));
            if (defaultValue is null)
                throw new ArgumentNullException(nameof(defaultValue));
            _ = new string( ' ', SettingsBufferSize );
            string buffer = new string('\0', SettingsBufferSize);
            if (filePath.Length > 0)
            {
                int length = SafeNativeMethods.ReadPrivateProfileString(sectionName, keyName, defaultValue, buffer, buffer.Length, filePath);
                return length > 0 ? buffer.Substring(0, length) : defaultValue;
            }
            else
            {
                return SafeNativeMethods.GetProfileString(sectionName, keyName, defaultValue, buffer, SettingsBufferSize) > 0 ? buffer.ToString() : defaultValue;
            }
        }

        /// <summary> Reads a "T:String" value from the INI file. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="filePath">     A <see cref="System.String">String</see> expression that
        /// specifies the INI file name. Use an empty file name to read from
        /// Win.ini. </param>
        /// <param name="sectionName">  A <see cref="System.String">String</see> expression that
        /// specifies the section in the INI file from which to read the
        /// settings. </param>
        /// <param name="keyName">      A <see cref="System.String">String</see> expression that
        /// specifies a Key in the section from which to read the settings.
        /// </param>
        /// <param name="defaultValue"> A <see cref="System.String">String</see> expression that
        /// specifies a default value to return if the key is not found.
        /// </param>
        /// <returns> A <see cref="System.String">String</see> data type. </returns>
        public static string ReadBuffer(string filePath, string sectionName, string keyName, string defaultValue)
        {
            if (filePath is null)
                throw new ArgumentNullException(nameof(filePath));
            if (string.IsNullOrWhiteSpace(sectionName))
                throw new ArgumentNullException(nameof(sectionName));
            if (keyName is null)
                throw new ArgumentNullException(nameof(keyName));
            var buffer = new System.Text.StringBuilder(SettingsBufferSize);
            int length = filePath.Length > 0 ? SafeNativeMethods.GetPrivateProfileString(sectionName, keyName, defaultValue, buffer, buffer.Capacity, filePath) : SafeNativeMethods.GetProfileString(sectionName, keyName, defaultValue, buffer, buffer.Capacity);
            return length > 0 ? buffer.ToString() : defaultValue;
        }

        /// <summary> Reads a value from the INI file. </summary>
        /// <remarks>
        /// The data type of the default value determines the type of the returned object.
        /// </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="filePath">     A <see cref="System.String">String</see> expression that
        /// specifies the INI file name. Use an empty file name to read from
        /// Win.ini. </param>
        /// <param name="sectionName">  A <see cref="System.String">String</see> expression that
        /// specifies the section in the INI file from which to read the
        /// settings. </param>
        /// <param name="keyName">      A <see cref="System.String">String</see> expression that
        /// specifies a Key in the section from which to read the settings.
        /// </param>
        /// <param name="defaultValue"> Is an Object expression that specifies a default value to return
        /// if the key is not found. </param>
        /// <returns> A <see cref="System.String">String</see> data type. </returns>
        public static object Read(string filePath, string sectionName, string keyName, object defaultValue)
        {
            if (filePath is null)
                throw new ArgumentNullException(nameof(filePath));
            if (string.IsNullOrWhiteSpace(sectionName))
                throw new ArgumentNullException(nameof(sectionName));
            if (keyName is null)
                throw new ArgumentNullException(nameof(keyName));
            if (defaultValue is null)
                throw new ArgumentNullException(nameof(defaultValue));
            // read setting from the file.
            string value = Read(filePath, sectionName, keyName, TypeConverter.Serialize(defaultValue));
            // convert string to object
            return TypeConverter.Deserialize(value, defaultValue);
        }

        /// <summary> Reads a value from the INI file. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="filePath">    A <see cref="System.String">String</see> expression that
        /// specifies the INI file name. Use an empty file name to read from
        /// Win.ini. </param>
        /// <param name="sectionName"> A <see cref="System.String">String</see> expression that
        /// specifies the section in the INI file from which to read the
        /// settings. </param>
        /// <param name="keyName">     A <see cref="System.String">String</see> expression that
        /// specifies a Key in the section from which to read the settings.
        /// </param>
        /// <param name="dataType">    Is an System.Type expression that specifies the object type to
        /// return. </param>
        /// <returns> A <see cref="System.String">String</see> data type. </returns>
        public static object Read(string filePath, string sectionName, string keyName, Type dataType)
        {
            if (filePath is null)
                throw new ArgumentNullException(nameof(filePath));
            if (string.IsNullOrWhiteSpace(sectionName))
                throw new ArgumentNullException(nameof(sectionName));
            if (keyName is null)
                throw new ArgumentNullException(nameof(keyName));
            if (dataType is null)
                throw new ArgumentNullException(nameof(dataType));
            // read setting from the file.
            string value = Read(filePath, sectionName, keyName);
            // convert string to object
            return TypeConverter.Deserialize(value, dataType);
        }

        /// <summary> Reads a whole number value from the INI file. </summary>
        /// <remarks> Use This method to read a whole number from an INI file. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="filePath">    A <see cref="System.String">String</see> expression that
        /// specifies the INI file name. </param>
        /// <param name="sectionName"> A <see cref="System.String">String</see> expression that
        /// specifies the section in the INI file from which to read the
        /// settings. </param>
        /// <param name="keyName">     A <see cref="System.String">String</see> expression that
        /// specifies a Key in the section from which to read the sSettings.
        /// </param>
        /// <returns> Returns an Int32 data type. </returns>
        public static int ReadWholeNumber(string filePath, string sectionName, string keyName)
        {
            int defaultValue = -1;
            return filePath is null
                ? throw new ArgumentNullException(nameof(filePath))
                : string.IsNullOrWhiteSpace(sectionName)
                ? throw new ArgumentNullException(nameof(sectionName))
                : keyName is null
                ? throw new ArgumentNullException(nameof(keyName))
                : SafeNativeMethods.GetPrivateProfile(sectionName, keyName, defaultValue, filePath);
        }

        /// <summary> Reads a whole number value from the INI file. </summary>
        /// <remarks> Use This method to read a whole number from an INI file. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="filePath">     A <see cref="System.String">String</see> expression that
        /// specifies the INI file name. </param>
        /// <param name="sectionName">  A <see cref="System.String">String</see> expression that
        /// specifies the section in the INI file from which to read the
        /// settings. </param>
        /// <param name="keyName">      A <see cref="System.String">String</see> expression that
        /// specifies a Key in the section from which to read the settings.
        /// </param>
        /// <param name="defaultValue"> Is an Int32 expression that specifies a default value to return
        /// if the key is not found. </param>
        /// <returns> Returns an Int32 data type. </returns>
        public static int ReadWholeNumber(string filePath, string sectionName, string keyName, int defaultValue)
        {
            return filePath is null
                ? throw new ArgumentNullException(nameof(filePath))
                : string.IsNullOrWhiteSpace(sectionName)
                ? throw new ArgumentNullException(nameof(sectionName))
                : keyName is null
                ? throw new ArgumentNullException(nameof(keyName))
                : SafeNativeMethods.GetPrivateProfile(sectionName, keyName, defaultValue, filePath);
        }

        /// <summary> Writes a string value to the INI file. </summary>
        /// <remarks> Use This method to write a String value to an INI file. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="filePath">    A <see cref="System.String">String</see> expression that
        /// specifies the INI file name. </param>
        /// <param name="sectionName"> A <see cref="System.String">String</see> expression that
        /// specifies the section in the INI file to which to write the
        /// settings. </param>
        /// <param name="keyName">     A <see cref="System.String">String</see> expression that
        /// specifies a Key in the section to which to write the settings.
        /// </param>
        /// <param name="value">       A <see cref="System.String">String</see> expression that
        /// specifies the value to write. </param>
        /// <returns> <c>True</c> if success; Otherwise, <c>False</c>. </returns>
        public static bool Write(string filePath, string sectionName, string keyName, string value)
        {
            return filePath is null
                ? throw new ArgumentNullException(nameof(filePath))
                : string.IsNullOrWhiteSpace(sectionName)
                ? throw new ArgumentNullException(nameof(sectionName))
                : keyName is null
                ? throw new ArgumentNullException(nameof(keyName))
                : filePath.Length > 0 ? SafeNativeMethods.WritePrivateProfileString(sectionName, keyName, value, filePath) : SafeNativeMethods.WriteProfileString(sectionName, keyName, value);
        }

        /// <summary> Writes a value to the INI file. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="filePath">    A <see cref="System.String">String</see> expression that
        /// specifies the INI file name. </param>
        /// <param name="sectionName"> A <see cref="System.String">String</see> expression that
        /// specifies the section in the INI file to which to write the
        /// settings. </param>
        /// <param name="keyName">     A <see cref="System.String">String</see> expression that
        /// specifies a Key in the section to which to write the settings.
        /// </param>
        /// <param name="value">       Is an Object expression that specifies the value to write. </param>
        /// <returns> <c>True</c> if success; Otherwise, <c>False</c>. </returns>
        public static bool Write(string filePath, string sectionName, string keyName, object value)
        {
            return filePath is null
                ? throw new ArgumentNullException(nameof(filePath))
                : string.IsNullOrWhiteSpace(sectionName)
                ? throw new ArgumentNullException(nameof(sectionName))
                : keyName is null
                ? throw new ArgumentNullException(nameof(keyName))
                : Write(filePath, sectionName, keyName, TypeConverter.Serialize(value));
        }

        /// <summary> Writes a section to the INI file. </summary>
        /// <remarks> Use This method to write a section to an INI file. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="filePath">    A <see cref="System.String">String</see> expression that
        /// specifies the INI file name. Specify an empty file name to read
        /// the WIN.INI file. </param>
        /// <param name="sectionName"> A <see cref="System.String">String</see> expression that
        /// specifies the section in the INI file which to write. </param>
        /// <param name="items">       The items. </param>
        /// <returns> <c>True</c> if success; Otherwise, <c>False</c>. </returns>
        public static bool WriteSection(string filePath, string sectionName, IEnumerable<string> items)
        {
            if (filePath is null)
                throw new ArgumentNullException(nameof(filePath));
            if (string.IsNullOrWhiteSpace(sectionName))
                throw new ArgumentNullException(nameof(sectionName));
            if (items is null)
                throw new ArgumentNullException(nameof(items));
            var buffer = new byte[32769];
            int bufferIndex = 0;
            foreach (var sectionItem in items)
            {
                _ = System.Text.Encoding.ASCII.GetBytes( sectionItem, 0, sectionItem.Length, buffer, bufferIndex );
                bufferIndex += sectionItem.Length;
                // restore section item termination
                buffer[bufferIndex] = 0;
                bufferIndex += 1;
            }
            // add final section item termination
            buffer[bufferIndex] = 0;
            // write the section
            return filePath.Length == 0 ? SafeNativeMethods.WriteProfileSection(sectionName, buffer) : SafeNativeMethods.WritePrivateProfileSection(sectionName, buffer, filePath);
        }

        /// <summary> Writes a section to the INI file. </summary>
        /// <remarks> Use This method to write a section to an INI file. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="filePath">     A <see cref="System.String">String</see> expression that
        /// specifies the INI file name. Specify an empty file name to read
        /// the WIN.INI file. </param>
        /// <param name="sectionName">  A <see cref="System.String">String</see> expression that
        /// specifies the section in the INI file which to write. </param>
        /// <param name="sectionValue"> A <see cref="System.String">String</see> expression that
        /// specifies the section data. </param>
        /// <returns> <c>True</c> if success; Otherwise, <c>False</c>. </returns>
        public static bool WriteSection(string filePath, string sectionName, string sectionValue)
        {
            return filePath is null
                ? throw new ArgumentNullException(nameof(filePath))
                : string.IsNullOrWhiteSpace(sectionName)
                ? throw new ArgumentNullException(nameof(sectionName))
                : filePath.Length > 0 ? SafeNativeMethods.WritePrivateProfileSection(sectionName, sectionValue, filePath) : SafeNativeMethods.WriteProfileSection(sectionName, sectionValue);
        }

        /// <summary> Returns the win.ini full file name. </summary>
        /// <value> <c>profileFilePath</c> is a read only string property. </value>
        private static string PublicProfileFilePath => System.IO.Path.Combine( Environment.SystemDirectory, @"\win.ini" );

        /// <summary> Gets the size of the settings buffer. </summary>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <value> The size of the settings buffer. </value>
        public static int SettingsBufferSize { get; set; } = 256;

        /// <summary> Gets the size of the section buffer. </summary>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <value> The size of the section buffer. </value>
        public static int SectionBufferSize { get; set; } = 65535;

        #endregion

        #region " READ FILE SECTION SETTING "

        /// <summary> Reads a "T:Boolean" value from the INI file. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="filePath">     A <see cref="System.String">String</see> expression that
        /// specifies the INI file name. Use an empty file name to read from
        /// Win.ini. </param>
        /// <param name="sectionName">  A <see cref="System.String">String</see> expression that
        /// specifies the section in the INI file from which to read the
        /// settings. </param>
        /// <param name="keyName">      A <see cref="System.String">String</see> expression that
        /// specifies a Key in the section from which to read the settings.
        /// </param>
        /// <param name="defaultValue"> A <see cref="System.Boolean">String</see> expression that
        /// specifies a default value to return if the key is not found.
        /// </param>
        /// <returns> A <see cref="System.Boolean">Boolean</see> data type. </returns>
        public static bool Read(string filePath, string sectionName, string keyName, bool defaultValue)
        {
            if (filePath is null)
                throw new ArgumentNullException(nameof(filePath));
            if (string.IsNullOrWhiteSpace(sectionName))
                throw new ArgumentNullException(nameof(sectionName));
            if (keyName is null)
                throw new ArgumentNullException(nameof(keyName));
            var buffer = new System.Text.StringBuilder(SettingsBufferSize);
            int length = filePath.Length > 0 ? SafeNativeMethods.GetPrivateProfileString(sectionName, keyName, Conversions.ToString(defaultValue), buffer, buffer.Capacity, filePath) : SafeNativeMethods.GetProfileString(sectionName, keyName, Conversions.ToString(defaultValue), buffer, buffer.Capacity);
            return length > 0 ? bool.Parse(buffer.ToString()) : defaultValue;
        }

        /// <summary> Reads a "T:Byte" value from the INI file. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="filePath">     A <see cref="System.String">String</see> expression that
        /// specifies the INI file name. Use an empty file name to read from
        /// Win.ini. </param>
        /// <param name="sectionName">  A <see cref="System.String">String</see> expression that
        /// specifies the section in the INI file from which to read the
        /// settings. </param>
        /// <param name="keyName">      A <see cref="System.String">String</see> expression that
        /// specifies a Key in the section from which to read the settings.
        /// </param>
        /// <param name="defaultValue"> A <see cref="System.Byte">String</see> expression that specifies
        /// a default value to return if the key is not found. </param>
        /// <returns> A <see cref="System.Byte">Byte</see> data type. </returns>
        public static byte Read(string filePath, string sectionName, string keyName, byte defaultValue)
        {
            if (filePath is null)
                throw new ArgumentNullException(nameof(filePath));
            if (string.IsNullOrWhiteSpace(sectionName))
                throw new ArgumentNullException(nameof(sectionName));
            if (keyName is null)
                throw new ArgumentNullException(nameof(keyName));
            var buffer = new System.Text.StringBuilder(SettingsBufferSize);
            int length = filePath.Length > 0 ? SafeNativeMethods.GetPrivateProfileString(sectionName, keyName, defaultValue.ToString(), buffer, buffer.Capacity, filePath) : SafeNativeMethods.GetProfileString(sectionName, keyName, defaultValue.ToString(), buffer, buffer.Capacity);
            return length > 0 ? byte.Parse(buffer.ToString(), System.Globalization.NumberStyles.Number, System.Globalization.CultureInfo.CurrentCulture) : defaultValue;
        }

        /// <summary> Reads a "T:DateTime" value from the INI file. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="filePath">     A <see cref="System.String">String</see> expression that
        /// specifies the INI file name. Use an empty file name to read from
        /// Win.ini. </param>
        /// <param name="sectionName">  A <see cref="System.String">String</see> expression that
        /// specifies the section in the INI file from which to read the
        /// settings. </param>
        /// <param name="keyName">      A <see cref="System.String">String</see> expression that
        /// specifies a Key in the section from which to read the settings.
        /// </param>
        /// <param name="defaultValue"> A <see cref="System.DateTime">String</see> expression that
        /// specifies a default value to return if the key is not found.
        /// </param>
        /// <returns> A <see cref="System.DateTime">Date Time</see> data type. </returns>
        public static DateTime Read(string filePath, string sectionName, string keyName, DateTime defaultValue)
        {
            if (filePath is null)
                throw new ArgumentNullException(nameof(filePath));
            if (string.IsNullOrWhiteSpace(sectionName))
                throw new ArgumentNullException(nameof(sectionName));
            if (keyName is null)
                throw new ArgumentNullException(nameof(keyName));
            var buffer = new System.Text.StringBuilder(SettingsBufferSize);
            int length = filePath.Length > 0 ? SafeNativeMethods.GetPrivateProfileString(sectionName, keyName, Conversions.ToString(defaultValue), buffer, buffer.Capacity, filePath) : SafeNativeMethods.GetProfileString(sectionName, keyName, Conversions.ToString(defaultValue), buffer, buffer.Capacity);
            return length > 0 ? DateTime.Parse(buffer.ToString(), System.Globalization.CultureInfo.CurrentCulture) : defaultValue;
        }

        /// <summary> Reads a string value from the INI file. </summary>
        /// <remarks> Use This method to read a String value from an INI file. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="filePath">     A <see cref="System.String">String</see> expression that
        /// specifies the INI file name. Use an empty file name to read from
        /// Win.ini. </param>
        /// <param name="sectionName">  A <see cref="System.String">String</see> expression that
        /// specifies the section in the INI file from which to read the
        /// settings. </param>
        /// <param name="keyName">      A <see cref="System.String">String</see> expression that
        /// specifies a Key in the section from which to read the settings.
        /// </param>
        /// <param name="defaultValue"> A <see cref="System.String">String</see> expression that
        /// specifies a default value to return if the key is not found.
        /// </param>
        /// <returns> A <see cref="System.String">String</see> data type. </returns>
        public static DateTimeOffset Read(string filePath, string sectionName, string keyName, DateTimeOffset defaultValue)
        {
            if (filePath is null)
                throw new ArgumentNullException(nameof(filePath));
            if (string.IsNullOrWhiteSpace(sectionName))
                throw new ArgumentNullException(nameof(sectionName));
            if (keyName is null)
                throw new ArgumentNullException(nameof(keyName));
            var buffer = new System.Text.StringBuilder(SettingsBufferSize);
            int length = filePath.Length > 0 ? SafeNativeMethods.GetPrivateProfileString(sectionName, keyName, defaultValue.ToString("o"), buffer, buffer.Capacity, filePath) : SafeNativeMethods.GetProfileString(sectionName, keyName, defaultValue.ToString("o"), buffer, buffer.Capacity);
            return length > 0 ? DateTimeOffset.Parse(buffer.ToString(), System.Globalization.CultureInfo.CurrentCulture) : defaultValue;
        }

        /// <summary> Reads a "T:Decimal" value from the INI file. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="filePath">     A <see cref="System.String">String</see> expression that
        /// specifies the INI file name. Use an empty file name to read from
        /// Win.ini. </param>
        /// <param name="sectionName">  A <see cref="System.String">String</see> expression that
        /// specifies the section in the INI file from which to read the
        /// settings. </param>
        /// <param name="keyName">      A <see cref="System.String">String</see> expression that
        /// specifies a Key in the section from which to read the settings.
        /// </param>
        /// <param name="defaultValue"> A <see cref="System.Decimal">String</see> expression that
        /// specifies a default value to return if the key is not found.
        /// </param>
        /// <returns> A <see cref="System.Decimal">double</see> data type. </returns>
        public static decimal Read(string filePath, string sectionName, string keyName, decimal defaultValue)
        {
            if (filePath is null)
                throw new ArgumentNullException(nameof(filePath));
            if (string.IsNullOrWhiteSpace(sectionName))
                throw new ArgumentNullException(nameof(sectionName));
            if (keyName is null)
                throw new ArgumentNullException(nameof(keyName));
            var buffer = new System.Text.StringBuilder(SettingsBufferSize);
            int length = filePath.Length > 0 ? SafeNativeMethods.GetPrivateProfileString(sectionName, keyName, defaultValue.ToString(), buffer, buffer.Capacity, filePath) : SafeNativeMethods.GetProfileString(sectionName, keyName, defaultValue.ToString(), buffer, buffer.Capacity);
            return length > 0 ? decimal.Parse(buffer.ToString(), System.Globalization.NumberStyles.Number, System.Globalization.CultureInfo.CurrentCulture) : defaultValue;
        }

        /// <summary> Reads a "T:Double" value from the INI file. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="filePath">     A <see cref="System.String">String</see> expression that
        /// specifies the INI file name. Use an empty file name to read from
        /// Win.ini. </param>
        /// <param name="sectionName">  A <see cref="System.String">String</see> expression that
        /// specifies the section in the INI file from which to read the
        /// settings. </param>
        /// <param name="keyName">      A <see cref="System.String">String</see> expression that
        /// specifies a Key in the section from which to read the settings.
        /// </param>
        /// <param name="defaultValue"> A <see cref="System.Double">String</see> expression that
        /// specifies a default value to return if the key is not found.
        /// </param>
        /// <returns> A <see cref="System.Double">Double</see> data type. </returns>
        public static double Read(string filePath, string sectionName, string keyName, double defaultValue)
        {
            if (filePath is null)
                throw new ArgumentNullException(nameof(filePath));
            if (string.IsNullOrWhiteSpace(sectionName))
                throw new ArgumentNullException(nameof(sectionName));
            if (keyName is null)
                throw new ArgumentNullException(nameof(keyName));
            var buffer = new System.Text.StringBuilder(SettingsBufferSize);
            int length = filePath.Length > 0 ? SafeNativeMethods.GetPrivateProfileString(sectionName, keyName, defaultValue.ToString(), buffer, buffer.Capacity, filePath) : SafeNativeMethods.GetProfileString(sectionName, keyName, defaultValue.ToString(), buffer, buffer.Capacity);
            return length > 0 ? double.Parse(buffer.ToString(), System.Globalization.NumberStyles.Number, System.Globalization.CultureInfo.CurrentCulture) : defaultValue;
        }

        /// <summary> Reads a "T:Int16" value from the INI file. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="filePath">     A <see cref="System.String">String</see> expression that
        /// specifies the INI file name. Use an empty file name to read from
        /// Win.ini. </param>
        /// <param name="sectionName">  A <see cref="System.String">String</see> expression that
        /// specifies the section in the INI file from which to read the
        /// settings. </param>
        /// <param name="keyName">      A <see cref="System.String">String</see> expression that
        /// specifies a Key in the section from which to read the settings.
        /// </param>
        /// <param name="defaultValue"> A <see cref="System.Int16">String</see> expression that specifies
        /// a default value to return if the key is not found. </param>
        /// <returns> An <see cref="System.Int16">Int16</see> data type. </returns>
        public static short Read(string filePath, string sectionName, string keyName, short defaultValue)
        {
            if (filePath is null)
                throw new ArgumentNullException(nameof(filePath));
            if (string.IsNullOrWhiteSpace(sectionName))
                throw new ArgumentNullException(nameof(sectionName));
            if (keyName is null)
                throw new ArgumentNullException(nameof(keyName));
            var buffer = new System.Text.StringBuilder(SettingsBufferSize);
            int length = filePath.Length > 0 ? SafeNativeMethods.GetPrivateProfileString(sectionName, keyName, defaultValue.ToString(), buffer, buffer.Capacity, filePath) : SafeNativeMethods.GetProfileString(sectionName, keyName, defaultValue.ToString(), buffer, buffer.Capacity);
            return length > 0 ? short.Parse(buffer.ToString(), System.Globalization.NumberStyles.Number, System.Globalization.CultureInfo.CurrentCulture) : defaultValue;
        }

        /// <summary> Reads a "T:Int32" value from the INI file. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="filePath">     A <see cref="System.String">String</see> expression that
        /// specifies the INI file name. Use an empty file name to read from
        /// Win.ini. </param>
        /// <param name="sectionName">  A <see cref="System.String">String</see> expression that
        /// specifies the section in the INI file from which to read the
        /// settings. </param>
        /// <param name="keyName">      A <see cref="System.String">String</see> expression that
        /// specifies a Key in the section from which to read the settings.
        /// </param>
        /// <param name="defaultValue"> A <see cref="System.Int32">String</see> expression that specifies
        /// a default value to return if the key is not found. </param>
        /// <returns> An <see cref="System.Int32">Int32</see> data type. </returns>
        public static int Read(string filePath, string sectionName, string keyName, int defaultValue)
        {
            if (filePath is null)
                throw new ArgumentNullException(nameof(filePath));
            if (string.IsNullOrWhiteSpace(sectionName))
                throw new ArgumentNullException(nameof(sectionName));
            if (keyName is null)
                throw new ArgumentNullException(nameof(keyName));
            var buffer = new System.Text.StringBuilder(SettingsBufferSize);
            int length = filePath.Length > 0 ? SafeNativeMethods.GetPrivateProfileString(sectionName, keyName, defaultValue.ToString(), buffer, buffer.Capacity, filePath) : SafeNativeMethods.GetProfileString(sectionName, keyName, defaultValue.ToString(), buffer, buffer.Capacity);
            return length > 0 ? int.Parse(buffer.ToString(), System.Globalization.NumberStyles.Number, System.Globalization.CultureInfo.CurrentCulture) : defaultValue;
        }

        /// <summary> Reads a "T:Int64" value from the INI file. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="filePath">     A <see cref="System.String">String</see> expression that
        /// specifies the INI file name. Use an empty file name to read from
        /// Win.ini. </param>
        /// <param name="sectionName">  A <see cref="System.String">String</see> expression that
        /// specifies the section in the INI file from which to read the
        /// settings. </param>
        /// <param name="keyName">      A <see cref="System.String">String</see> expression that
        /// specifies a Key in the section from which to read the settings.
        /// </param>
        /// <param name="defaultValue"> A <see cref="System.Int64">String</see> expression that specifies
        /// a default value to return if the key is not found. </param>
        /// <returns> An <see cref="System.Int64">Int64</see> data type. </returns>
        public static long Read(string filePath, string sectionName, string keyName, long defaultValue)
        {
            if (filePath is null)
                throw new ArgumentNullException(nameof(filePath));
            if (string.IsNullOrWhiteSpace(sectionName))
                throw new ArgumentNullException(nameof(sectionName));
            if (keyName is null)
                throw new ArgumentNullException(nameof(keyName));
            var buffer = new System.Text.StringBuilder(SettingsBufferSize);
            long length = filePath.Length > 0 ? SafeNativeMethods.GetPrivateProfileString(sectionName, keyName, defaultValue.ToString(), buffer, buffer.Capacity, filePath) : SafeNativeMethods.GetProfileString(sectionName, keyName, defaultValue.ToString(), buffer, buffer.Capacity);
            return length > 0L ? long.Parse(buffer.ToString(), System.Globalization.NumberStyles.Number, System.Globalization.CultureInfo.CurrentCulture) : defaultValue;
        }

        /// <summary> Reads a "T:Single" value from the INI file. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="filePath">     A <see cref="System.String">String</see> expression that
        /// specifies the INI file name. Use an empty file name to read from
        /// Win.ini. </param>
        /// <param name="sectionName">  A <see cref="System.String">String</see> expression that
        /// specifies the section in the INI file from which to read the
        /// settings. </param>
        /// <param name="keyName">      A <see cref="System.String">String</see> expression that
        /// specifies a Key in the section from which to read the settings.
        /// </param>
        /// <param name="defaultValue"> A <see cref="System.Single">String</see> expression that
        /// specifies a default value to return if the key is not found.
        /// </param>
        /// <returns> A <see cref="System.Single">Single</see> data type. </returns>
        public static float Read(string filePath, string sectionName, string keyName, float defaultValue)
        {
            if (filePath is null)
                throw new ArgumentNullException(nameof(filePath));
            if (string.IsNullOrWhiteSpace(sectionName))
                throw new ArgumentNullException(nameof(sectionName));
            if (keyName is null)
                throw new ArgumentNullException(nameof(keyName));
            var buffer = new System.Text.StringBuilder(SettingsBufferSize);
            float length = filePath.Length > 0 ? SafeNativeMethods.GetPrivateProfileString(sectionName, keyName, defaultValue.ToString(), buffer, buffer.Capacity, filePath) : SafeNativeMethods.GetProfileString(sectionName, keyName, defaultValue.ToString(), buffer, buffer.Capacity);
            return length > 0f ? float.Parse(buffer.ToString(), System.Globalization.NumberStyles.Number, System.Globalization.CultureInfo.CurrentCulture) : defaultValue;
        }

        #endregion

        #region " WRITE FILE SECTION SETTING "

        /// <summary> Writes a "T:Boolean" value to the INI file. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="filePath">    A <see cref="System.String">String</see> expression that
        /// specifies the INI file name. </param>
        /// <param name="sectionName"> A <see cref="System.String">String</see> expression that
        /// specifies the section in the INI file to which to write the
        /// settings. </param>
        /// <param name="keyName">     A <see cref="System.String">String</see> expression that
        /// specifies a Key in the section to which to write the settings.
        /// </param>
        /// <param name="value">       A <see cref="System.Boolean">Boolean</see> expression that
        /// specifies the value to write. </param>
        /// <returns> <c>True</c> if success; Otherwise, <c>False</c>. </returns>
        public static bool Write(string filePath, string sectionName, string keyName, bool value)
        {
            return filePath is null
                ? throw new ArgumentNullException(nameof(filePath))
                : string.IsNullOrWhiteSpace(sectionName)
                ? throw new ArgumentNullException(nameof(sectionName))
                : keyName is null
                ? throw new ArgumentNullException(nameof(keyName))
                : Write(filePath, sectionName, keyName, value.ToString(System.Globalization.CultureInfo.CurrentCulture));
        }

        /// <summary> Writes a "T:DateTime" value to the INI file. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="filePath">    A <see cref="System.String">String</see> expression that
        /// specifies the INI file name. </param>
        /// <param name="sectionName"> A <see cref="System.String">String</see> expression that
        /// specifies the section in the INI file to which to write the
        /// settings. </param>
        /// <param name="keyName">     A <see cref="System.String">String</see> expression that
        /// specifies a Key in the section to which to write the settings.
        /// </param>
        /// <param name="value">       A <see cref="System.DateTime">DateTime</see> expression that
        /// specifies the value to write. </param>
        /// <param name="format">      Specifies how to format the value. </param>
        /// <returns> <c>True</c> if success; Otherwise, <c>False</c>. </returns>
        public static bool Write(string filePath, string sectionName, string keyName, DateTime value, string format)
        {
            return filePath is null
                ? throw new ArgumentNullException(nameof(filePath))
                : string.IsNullOrWhiteSpace(sectionName)
                ? throw new ArgumentNullException(nameof(sectionName))
                : keyName is null
                ? throw new ArgumentNullException(nameof(keyName))
                : Write(filePath, sectionName, keyName, value.ToString(format, System.Globalization.CultureInfo.CurrentCulture));
        }

        /// <summary> Writes a "T:DateTime" value to the INI file. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="filePath">    A <see cref="System.String">String</see> expression that
        /// specifies the INI file name. </param>
        /// <param name="sectionName"> A <see cref="System.String">String</see> expression that
        /// specifies the section in the INI file to which to write the
        /// settings. </param>
        /// <param name="keyName">     A <see cref="System.String">String</see> expression that
        /// specifies a Key in the section to which to write the settings.
        /// </param>
        /// <param name="value">       A <see cref="System.DateTime">DateTime</see> expression that
        /// specifies the value to write. </param>
        /// <returns> <c>True</c> if success; Otherwise, <c>False</c>. </returns>
        public static bool Write(string filePath, string sectionName, string keyName, DateTime value)
        {
            return filePath is null
                ? throw new ArgumentNullException(nameof(filePath))
                : string.IsNullOrWhiteSpace(sectionName)
                ? throw new ArgumentNullException(nameof(sectionName))
                : keyName is null
                ? throw new ArgumentNullException(nameof(keyName))
                : Write(filePath, sectionName, keyName, value.ToString(System.Globalization.CultureInfo.CurrentCulture));
        }

        /// <summary> Writes a "T:DateTimeOffset" value to the INI file. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="filePath">    A <see cref="System.String">String</see> expression that
        /// specifies the INI file name. </param>
        /// <param name="sectionName"> A <see cref="System.String">String</see> expression that
        /// specifies the section in the INI file to which to write the
        /// settings. </param>
        /// <param name="keyName">     A <see cref="System.String">String</see> expression that
        /// specifies a Key in the section to which to write the settings.
        /// </param>
        /// <param name="value">       A <see cref="System.String">String</see> expression that
        /// specifies the value to write. </param>
        /// <param name="format">      Specifies how to format the value. </param>
        /// <returns> <c>True</c> if success; Otherwise, <c>False</c>. </returns>
        public static bool Write(string filePath, string sectionName, string keyName, DateTimeOffset value, string format)
        {
            return filePath is null
                ? throw new ArgumentNullException(nameof(filePath))
                : string.IsNullOrWhiteSpace(sectionName)
                ? throw new ArgumentNullException(nameof(sectionName))
                : keyName is null
                ? throw new ArgumentNullException(nameof(keyName))
                : Write(filePath, sectionName, keyName, value.ToString(format, System.Globalization.CultureInfo.CurrentCulture));
        }

        /// <summary> Writes a string value to the INI file. </summary>
        /// <remarks> Use This method to write a String value to an INI file. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="filePath">    A <see cref="System.String">String</see> expression that
        /// specifies the INI file name. </param>
        /// <param name="sectionName"> A <see cref="System.String">String</see> expression that
        /// specifies the section in the INI file to which to write the
        /// settings. </param>
        /// <param name="keyName">     A <see cref="System.String">String</see> expression that
        /// specifies a Key in the section to which to write the settings.
        /// </param>
        /// <param name="value">       A <see cref="System.String">String</see> expression that
        /// specifies the value to write. </param>
        /// <returns> <c>True</c> if success; Otherwise, <c>False</c>. </returns>
        public static bool Write(string filePath, string sectionName, string keyName, DateTimeOffset value)
        {
            return filePath is null
                ? throw new ArgumentNullException(nameof(filePath))
                : string.IsNullOrWhiteSpace(sectionName)
                ? throw new ArgumentNullException(nameof(sectionName))
                : keyName is null
                ? throw new ArgumentNullException(nameof(keyName))
                : Write(filePath, sectionName, keyName, value.ToString(System.Globalization.CultureInfo.CurrentCulture));
        }

        /// <summary> Writes a "T:Decimal" value to the INI file. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="filePath">    A <see cref="System.String">String</see> expression that
        /// specifies the INI file name. </param>
        /// <param name="sectionName"> A <see cref="System.String">String</see> expression that
        /// specifies the section in the INI file to which to write the
        /// settings. </param>
        /// <param name="keyName">     A <see cref="System.String">String</see> expression that
        /// specifies a Key in the section to which to write the settings.
        /// </param>
        /// <param name="value">       A <see cref="System.Decimal">Decimal</see> expression that
        /// specifies the value to write. </param>
        /// <param name="format">      Specifies how to format the value. </param>
        /// <returns> <c>True</c> if success; Otherwise, <c>False</c>. </returns>
        public static bool Write(string filePath, string sectionName, string keyName, decimal value, string format)
        {
            return filePath is null
                ? throw new ArgumentNullException(nameof(filePath))
                : string.IsNullOrWhiteSpace(sectionName)
                ? throw new ArgumentNullException(nameof(sectionName))
                : keyName is null
                ? throw new ArgumentNullException(nameof(keyName))
                : Write(filePath, sectionName, keyName, value.ToString(format, System.Globalization.CultureInfo.CurrentCulture));
        }

        /// <summary> Writes a "T:Decimal" value to the INI file. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="filePath">    A <see cref="System.String">String</see> expression that
        /// specifies the INI file name. </param>
        /// <param name="sectionName"> A <see cref="System.String">String</see> expression that
        /// specifies the section in the INI file to which to write the
        /// settings. </param>
        /// <param name="keyName">     A <see cref="System.String">String</see> expression that
        /// specifies a Key in the section to which to write the settings.
        /// </param>
        /// <param name="value">       A <see cref="System.Decimal">Decimal</see> expression that
        /// specifies the value to write. </param>
        /// <returns> <c>True</c> if success; Otherwise, <c>False</c>. </returns>
        public static bool Write(string filePath, string sectionName, string keyName, decimal value)
        {
            return filePath is null
                ? throw new ArgumentNullException(nameof(filePath))
                : string.IsNullOrWhiteSpace(sectionName)
                ? throw new ArgumentNullException(nameof(sectionName))
                : keyName is null
                ? throw new ArgumentNullException(nameof(keyName))
                : Write(filePath, sectionName, keyName, value.ToString(System.Globalization.CultureInfo.CurrentCulture));
        }

        /// <summary> Writes a "T:Double" value to the INI file. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="filePath">    A <see cref="System.String">String</see> expression that
        /// specifies the INI file name. </param>
        /// <param name="sectionName"> A <see cref="System.String">String</see> expression that
        /// specifies the section in the INI file to which to write the
        /// settings. </param>
        /// <param name="keyName">     A <see cref="System.String">String</see> expression that
        /// specifies a Key in the section to which to write the settings.
        /// </param>
        /// <param name="value">       A <see cref="System.Double">Double</see> expression that
        /// specifies the value to write. </param>
        /// <param name="format">      Specifies how to format the value. </param>
        /// <returns> <c>True</c> if success; Otherwise, <c>False</c>. </returns>
        public static bool Write(string filePath, string sectionName, string keyName, double value, string format)
        {
            return filePath is null
                ? throw new ArgumentNullException(nameof(filePath))
                : string.IsNullOrWhiteSpace(sectionName)
                ? throw new ArgumentNullException(nameof(sectionName))
                : keyName is null
                ? throw new ArgumentNullException(nameof(keyName))
                : Write(filePath, sectionName, keyName, value.ToString(format, System.Globalization.CultureInfo.CurrentCulture));
        }

        /// <summary> Writes a "T:Double" value to the INI file. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="filePath">    A <see cref="System.String">String</see> expression that
        /// specifies the INI file name. </param>
        /// <param name="sectionName"> A <see cref="System.String">String</see> expression that
        /// specifies the section in the INI file to which to write the
        /// settings. </param>
        /// <param name="keyName">     A <see cref="System.String">String</see> expression that
        /// specifies a Key in the section to which to write the settings.
        /// </param>
        /// <param name="value">       A <see cref="System.Double">Double</see> expression that
        /// specifies the value to write. </param>
        /// <returns> <c>True</c> if success; Otherwise, <c>False</c>. </returns>
        public static bool Write(string filePath, string sectionName, string keyName, double value)
        {
            return filePath is null
                ? throw new ArgumentNullException(nameof(filePath))
                : string.IsNullOrWhiteSpace(sectionName)
                ? throw new ArgumentNullException(nameof(sectionName))
                : keyName is null
                ? throw new ArgumentNullException(nameof(keyName))
                : Write(filePath, sectionName, keyName, value.ToString(System.Globalization.CultureInfo.CurrentCulture));
        }

        /// <summary> Writes a "T:Int64" value to the INI file. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="filePath">    A <see cref="System.String">String</see> expression that
        /// specifies the INI file name. </param>
        /// <param name="sectionName"> A <see cref="System.String">String</see> expression that
        /// specifies the section in the INI file to which to write the
        /// settings. </param>
        /// <param name="keyName">     A <see cref="System.String">String</see> expression that
        /// specifies a Key in the section to which to write the settings.
        /// </param>
        /// <param name="value">       A <see cref="System.Int64">Int64</see> expression that specifies
        /// the value to write. </param>
        /// <param name="format">      Specifies how to format the value. </param>
        /// <returns> <c>True</c> if success; Otherwise, <c>False</c>. </returns>
        public static bool Write(string filePath, string sectionName, string keyName, long value, string format)
        {
            return filePath is null
                ? throw new ArgumentNullException(nameof(filePath))
                : string.IsNullOrWhiteSpace(sectionName)
                ? throw new ArgumentNullException(nameof(sectionName))
                : keyName is null
                ? throw new ArgumentNullException(nameof(keyName))
                : Write(filePath, sectionName, keyName, value.ToString(format, System.Globalization.CultureInfo.CurrentCulture));
        }

        /// <summary> Writes a "T:Int64" value to the INI file. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="filePath">    A <see cref="System.String">String</see> expression that
        /// specifies the INI file name. </param>
        /// <param name="sectionName"> A <see cref="System.String">String</see> expression that
        /// specifies the section in the INI file to which to write the
        /// settings. </param>
        /// <param name="keyName">     A <see cref="System.String">String</see> expression that
        /// specifies a Key in the section to which to write the settings.
        /// </param>
        /// <param name="value">       A <see cref="System.Int64">Int64</see> expression that specifies
        /// the value to write. </param>
        /// <returns> <c>True</c> if success; Otherwise, <c>False</c>. </returns>
        public static bool Write(string filePath, string sectionName, string keyName, long value)
        {
            return filePath is null
                ? throw new ArgumentNullException(nameof(filePath))
                : string.IsNullOrWhiteSpace(sectionName)
                ? throw new ArgumentNullException(nameof(sectionName))
                : keyName is null
                ? throw new ArgumentNullException(nameof(keyName))
                : Write(filePath, sectionName, keyName, value.ToString(System.Globalization.CultureInfo.CurrentCulture));
        }

        #endregion

        #region " NAMES "

        /// <summary> Gets the name of the setting. </summary>
        /// <remarks> Use This property to set or get the name of the INI Settings string. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <value>
        /// <c>SettingsName</c> is a String property that can be read from or written to (read or write)
        /// that specifies the name of the INI Settings string in the INI Settings file.
        /// </value>
        /// <seealso cref="ProfileScribe.FilePath"/>
        /// 
        /// <example>
        /// This example sets a INI Settings file name, writes INI Settings strings to the file, and then
        /// reads the information from back.
        /// <code>
        /// Sub Form_Click
        /// ' Declare some variables.
        /// Dim settingValue As Variant
        /// ' instantiate the ISR INI Settings Scribe Class
        /// Dim ini as isr.Configuration.IniSettings.Scribe
        /// ini = New isr.Configuration.IniSettings.Scribe
        /// ' Set to trap any errors
        /// Try
        /// ' Set the INI Settings file name
        /// ini.FilePath = ini.DefaultFilePath()
        /// ' Set the INI Settings Section
        /// ini.SectionName = "Section1"
        /// ' Write a string to the file String setting
        /// ini.SettingName = "String"
        /// ini.SettingValue = "A String"
        /// ' Write an Int32 value
        /// ini.SettingName = "Int32"
        /// ini.SettingValue = Convert.ToInt32(0)
        /// ' Write an Double value
        /// ini.SettingName = "Double"
        /// ini.settingFormat = "##0.000"
        /// ini.SettingValue = convert.ToDouble(1.11)
        /// ' Write two list items
        /// ini.SettingName = "List"
        /// ini.ListSetting(1) = "One"
        /// ini.ListSetting(2) = "Two"
        /// ' Read the string from the file
        /// ini.SettingName = "String"
        /// Me.Print "String: "; ini.SettingValue
        /// ' Read an Int32 value
        /// ini.SettingName = "Int32"
        /// ini.DefaultValue= Convert.ToInt32(0)
        /// Me.Print "Int32: "; ini.SettingValue
        /// ' Read a double
        /// ini.SettingName = "Double"
        /// Me.Print "Double: "; ini.SettingValue
        /// ' Read two list items
        /// ini.SettingName = "List"
        /// Me.Print ini.ListSetting(1)
        /// Me.Print ini.ListSetting(2)
        /// ' Read the entire section
        /// Me.Print "Section: "; ini.sectionValue
        /// Catch e As Exception
        /// ' respond to any file name errors.
        /// MsgBox e.ToString
        /// End Try
        /// End Sub  </code>
        /// To run This example, paste the code fragment into a Windows Form class.
        /// Run the program by pressing F5, and then click on the form.
        /// </example>
        public string SettingName { get; set; }

        /// <summary> Gets the name of the section. </summary>
        /// <remarks> Use This property to set or get the name of the INI Settings section. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <value>
        /// <c>SectionName</c> is a String property that can be read from or written to (read or write)
        /// that specifies the name of the section in the INI Settings file.
        /// </value>
        /// <seealso cref="ProfileScribe.SettingName"/>
        public string SectionName { get; set; }

        #endregion

        #region " SECTION "

        /// <summary> Returns the section names. </summary>
        /// <remarks>
        /// Use This property to read or write an entire section to the INI Settings file.  When writing,
        /// This property replaces the entire section erasing any information that was present in the
        /// section and replacing it with new information.  Treat This property tenderly! <para>
        /// 
        /// When reading a section, returns a string with all the INI Settings in the section.</para>
        /// <para>
        /// 
        /// If you do not specify a file name, the section is read from or written to the WIN.INI file.
        /// </para>
        /// </remarks>
        /// <returns>
        /// An enumerator that allows foreach to be used to process section names in this collection.
        /// </returns>
        /// <seealso cref="ProfileScribe.FilePath"/>
        public IEnumerable<string> SectionNames()
        {
            // If we have a file name, get the names
            return ReadSection( this.FilePath, this.SectionName );
        }

        /// <summary> Reads a whole section from the INI file. </summary>
        /// <remarks>
        /// Use This method to read a section from an INI file.  The section is returned in an enumerable
        /// of string values where the section values are delimited by the null character (ASCII 0)
        /// </remarks>
        /// <returns>
        /// An enumerator that allows foreach to be used to process read section in this collection.
        /// </returns>
        public IEnumerable<string> ReadSection()
        {
            return ReadSection( this.FilePath, this.SectionName );
        }

        /// <summary> Writes a section to the INI file. </summary>
        /// <remarks> Use This method to write a section to an INI file. </remarks>
        /// <param name="items"> The items. </param>
        /// <returns> <c>True</c> if success; Otherwise, <c>False</c>. </returns>
        public bool WriteSection(IEnumerable<string> items)
        {
            return WriteSection( this.FilePath, this.SectionName, items);
        }

        #endregion

        #region " LIST "

        /// <summary> Reads a list of INI Settings from a INI Settings file. </summary>
        /// <remarks>
        /// The format of the list of INI Settings is SettingName# (e.g., Setting1, Setting2,
        /// ....).<para>
        /// Reads all the INI Settings in the list in sequence until no more INI Settings are
        /// found.</para> <para>
        /// The list is dimensioned starting with First Item, which defaults to 1.  For example, the
        /// first item value must be set to 0 in order to read INI Settings Setting0, Setting1,..'  or to
        /// -1 to read the list ordered as -1,0,1,...</para>
        /// </remarks>
        /// <param name="firstItemIndex"> is an Int32 expression that specifies the index of the first list
        /// item (default is 1) </param>
        /// <returns> The list. </returns>
        /// <example>
        /// This example writes a list of INI Settings to an INI file and then reads the list from the
        /// file. <code>
        /// Sub Form_Click <para>
        /// ' Declare some data storage </para><para>
        /// Dim i as Int32 </para><para>
        /// ' Create an instance of the ISR INI Settings Scribe Class Dim ini as isr.IO.ProfileScribe
        /// </para><para>
        /// ' Create data to write to the INI Settings file Dim items As new List(of
        /// String)</para><para>
        /// For i = 0 to UBound(sngList)</para><para>
        /// items.Add(Log(convert.ToSingle(i+1))).ToString)</para><para>
        /// Next i </para><para>
        /// ' Set the INI Settings file name, section name, and setting name </para><para>
        /// ini.FilePath = ini.DefaultFilePath() </para><para>
        /// ini.SectionName = "Section1" </para><para>
        /// ini.SettingName = "Setting" </para><para>
        /// ' Write the list to the file. </para><para>
        /// ini.WriteList(items) </para><para>
        /// ' Read the list from the file  </para><para>
        /// items = ini.ReadList() </para><para>
        /// End Sub </para></code>
        /// To run This example, paste the code fragment into a Windows Form class. Run the program by
        /// pressing F5, and then click on the form
        /// </example>
        /// <seealso cref="ProfileScribe.SettingName"/>
        public IEnumerable<string> ReadList(int firstItemIndex)
        {
            var items = new List<string>();
            int currentItem = firstItemIndex;
            string itemValue;

            // Loop through the list data.
            do
            {

                // Read data from the INI Settings file
                itemValue = Read( this.FilePath, this.SectionName, this.ListSettingName(currentItem), string.Empty);
                if (itemValue.Length > 0)
                {

                    // add item to the list
                    items.Add(itemValue);

                    // Increment item count
                    currentItem += 1;
                }
            }
            while (itemValue.Length != 0);

            // return the list
            return items;
        }

        /// <summary> Reads a list of INI Settings from a INI Settings file. </summary>
        /// <remarks>
        /// The format of the list of INI Settings is SettingName# (e.g., Setting1, Setting2,
        /// ....).<para>
        /// Reads all the INI Settings in the list in sequence until no more INI Settings are
        /// found.</para> <para>
        /// The list is dimensioned starting with First Item, which defaults to 1.  For example, the
        /// first item value must be set to 0 in order to read INI Settings Setting0, Setting1,..'  or to
        /// -1 to read the list ordered as -1,0,1,...</para>
        /// </remarks>
        /// <returns> The list. </returns>
        /// <seealso cref="ProfileScribe.ReadList"/>
        public IEnumerable<string> ReadList()
        {
            return this.ReadList(1);
        }

        /// <summary> Writes a list of INI Settings to a INI Settings file. </summary>
        /// <remarks>
        /// The format of the list of INI Settings is SettingName# (e.g., Setting1, Setting2,
        /// ....).<para>
        /// Reads all the INI Settings in the list in sequence until no more INI Settings are
        /// found.</para> <para>
        /// The list is dimensioned starting with First Item, which defaults to 1.  For example, the
        /// first item value must be set to 0 in order to read INI Settings Setting0, Setting1,..'  or to
        /// -1 to read the list ordered as -1,0,1,...</para>
        /// </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="values">         The list values. </param>
        /// <param name="firstItemIndex"> is an Int32 expression that specifies the index of the first list
        /// item (default is 1) </param>
        /// <seealso cref="ProfileScribe.SettingName"/>
        public void WriteList(IEnumerable<string> values, int firstItemIndex)
        {
            if (values is null)
                throw new ArgumentNullException(nameof(values));
            int itemIndex = firstItemIndex;
            // Loop through the list data.
            foreach (var itemValue in values)
            {
                // Write the list setting
                _ = Write( this.FilePath, this.SectionName, this.ListSettingName( itemIndex ), itemValue );
                itemIndex += 1;
            }
        }

        /// <summary> Writes a list of INI Settings to a INI Settings file. </summary>
        /// <remarks>
        /// The format of the list of INI Settings is SettingName# (e.g., Setting1, Setting2,
        /// ....).<para>
        /// Reads all the INI Settings in the list in sequence until no more INI Settings are
        /// found.</para> <para>
        /// The list is dimensioned starting with First Item, which defaults to 1.  For example, the
        /// first item value must be set to 0 in order to read INI Settings Setting0, Setting1,..'  or to
        /// -1 to read the list ordered as -1,0,1,...</para>
        /// </remarks>
        /// <param name="values"> The list values. </param>
        /// <seealso cref="ProfileScribe.ReadList"/>
        public void WriteList(IEnumerable<string> values)
        {
            this.WriteList(values, 1);
        }

        /// <summary> This private property returns the name of the ini settings list. </summary>
        /// <remarks>
        /// Use This property to get the name of the list setting for reading or write a list item.
        /// </remarks>
        /// <param name="itemNumber"> Is an Int32 expression that specifies the item number in the list. </param>
        /// <value> <c>_listSettingName</c> is a read only string property. </value>
        private string ListSettingName(int itemNumber)
        {
            return string.Format(System.Globalization.CultureInfo.CurrentCulture, "{0}{1}", this.SettingName, itemNumber);
        }

        #endregion

        #region " READ LIST ITEM "

        /// <summary> Reads a "T:Boolean" list value from the INI file. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <param name="itemNumber">   Specifies the list item number to read. </param>
        /// <param name="defaultValue"> A <see cref="System.Boolean">String</see> expression that
        /// specifies a default value to return if the key is not found.
        /// </param>
        /// <returns> A <see cref="System.Boolean">Boolean</see> data type. </returns>
        public bool ReadListItem(int itemNumber, bool defaultValue)
        {
            return Read( this.FilePath, this.SectionName, this.ListSettingName(itemNumber), defaultValue);
        }

        /// <summary> Reads a "T:Byte" list value from the INI file. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <param name="itemNumber">   Specifies the list item number to read. </param>
        /// <param name="defaultValue"> A <see cref="System.Byte">String</see> expression that specifies
        /// a default value to return if the key is not found. </param>
        /// <returns> A <see cref="System.Byte">Byte</see> data type. </returns>
        public byte ReadListItem(int itemNumber, byte defaultValue)
        {
            return Read( this.FilePath, this.SectionName, this.ListSettingName(itemNumber), defaultValue);
        }

        /// <summary> Reads a "T:DateTime" list value from the INI file. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <param name="itemNumber">   Specifies the list item number to read. </param>
        /// <param name="defaultValue"> A <see cref="System.DateTime">String</see> expression that
        /// specifies a default value to return if the key is not found.
        /// </param>
        /// <returns> A <see cref="System.DateTime">DateTime</see> data type. </returns>
        public DateTime ReadListItem(int itemNumber, DateTime defaultValue)
        {
            return Read( this.FilePath, this.SectionName, this.ListSettingName(itemNumber), defaultValue);
        }

        /// <summary> Reads a "T:DateTimeOffset" list value from the INI file. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <param name="itemNumber">   Specifies the list item number to read. </param>
        /// <param name="defaultValue"> A <see cref="System.DateTime">String</see> expression that
        /// specifies a default value to return if the key is not found.
        /// </param>
        /// <returns> A <see cref="System.DateTimeOffset">DateTimeOffset</see> data type. </returns>
        public DateTimeOffset ReadListItem(int itemNumber, DateTimeOffset defaultValue)
        {
            return Read( this.FilePath, this.SectionName, this.ListSettingName(itemNumber), defaultValue);
        }

        /// <summary> Reads a "T:Decimal" list value from the INI file. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <param name="itemNumber">   Specifies the list item number to read. </param>
        /// <param name="defaultValue"> A <see cref="System.Decimal">String</see> expression that
        /// specifies a default value to return if the key is not found.
        /// </param>
        /// <returns> A <see cref="System.Decimal">Decimal</see> data type. </returns>
        public decimal ReadListItem(int itemNumber, decimal defaultValue)
        {
            return Read( this.FilePath, this.SectionName, this.ListSettingName(itemNumber), defaultValue);
        }

        /// <summary> Reads a "T:Double" list value from the INI file. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <param name="itemNumber">   Specifies the list item number to read. </param>
        /// <param name="defaultValue"> A <see cref="System.Double">String</see> expression that
        /// specifies a default value to return if the key is not found.
        /// </param>
        /// <returns> A <see cref="System.Double">Double</see> data type. </returns>
        public double ReadListItem(int itemNumber, double defaultValue)
        {
            return Read( this.FilePath, this.SectionName, this.ListSettingName(itemNumber), defaultValue);
        }

        /// <summary> Reads a "T:Int16" list value from the INI file. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <param name="itemNumber">   Specifies the list item number to read. </param>
        /// <param name="defaultValue"> A <see cref="System.Int16">String</see> expression that specifies
        /// a default value to return if the key is not found. </param>
        /// <returns> A <see cref="System.Int16">Int16</see> data type. </returns>
        public short ReadListItem(int itemNumber, short defaultValue)
        {
            return Read( this.FilePath, this.SectionName, this.ListSettingName(itemNumber), defaultValue);
        }

        /// <summary> Reads a "T:Int32" list value from the INI file. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <param name="itemNumber">   Specifies the list item number to read. </param>
        /// <param name="defaultValue"> A <see cref="System.Int32">String</see> expression that specifies
        /// a default value to return if the key is not found. </param>
        /// <returns> A <see cref="System.Int32">Int32</see> data type. </returns>
        public int ReadListItem(int itemNumber, int defaultValue)
        {
            return Read( this.FilePath, this.SectionName, this.ListSettingName(itemNumber), defaultValue);
        }

        /// <summary> Reads a "T:Int64" list value from the INI file. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <param name="itemNumber">   Specifies the list item number to read. </param>
        /// <param name="defaultValue"> A <see cref="System.Int64">String</see> expression that specifies
        /// a default value to return if the key is not found. </param>
        /// <returns> A <see cref="System.Int64">Int64</see> data type. </returns>
        public long ReadListItem(int itemNumber, long defaultValue)
        {
            return Read( this.FilePath, this.SectionName, this.ListSettingName(itemNumber), defaultValue);
        }

        /// <summary> Reads a "T:Single" list value from the INI file. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <param name="itemNumber">   Specifies the list item number to read. </param>
        /// <param name="defaultValue"> A <see cref="System.Single">String</see> expression that
        /// specifies a default value to return if the key is not found.
        /// </param>
        /// <returns> A <see cref="System.Single">Single</see> data type. </returns>
        public float ReadListItem(int itemNumber, float defaultValue)
        {
            return Read( this.FilePath, this.SectionName, this.ListSettingName(itemNumber), defaultValue);
        }

        /// <summary> Reads a "T:String" list value from the INI file. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <param name="itemNumber">   Specifies the list item number to read. </param>
        /// <param name="defaultValue"> A <see cref="System.String">String</see> expression that
        /// specifies a default value to return if the key is not found.
        /// </param>
        /// <returns> A <see cref="System.String">String</see> data type. </returns>
        public string ReadListItem(int itemNumber, string defaultValue)
        {
            return Read( this.FilePath, this.SectionName, this.ListSettingName(itemNumber), defaultValue);
        }

        #endregion

        #region " READ SECTION VALUE "

        /// <summary> Reads a "T:Boolean" Section value from the INI file. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <param name="settingsName"> Name of the settings. </param>
        /// <param name="defaultValue"> A <see cref="System.Boolean">String</see> expression that
        /// specifies a default value to return if the key is not found.
        /// </param>
        /// <returns> A <see cref="System.Boolean">Boolean</see> data type. </returns>
        public bool Read(string settingsName, bool defaultValue)
        {
            return Read( this.FilePath, this.SectionName, settingsName, defaultValue);
        }

        /// <summary> Reads a "T:Byte" Section value from the INI file. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <param name="settingsName"> Name of the settings. </param>
        /// <param name="defaultValue"> A <see cref="System.Byte">String</see> expression that specifies
        /// a default value to return if the key is not found. </param>
        /// <returns> A <see cref="System.Byte">Byte</see> data type. </returns>
        public byte Read(string settingsName, byte defaultValue)
        {
            return Read( this.FilePath, this.SectionName, settingsName, defaultValue);
        }

        /// <summary> Reads a "T:DateTime" Section value from the INI file. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <param name="settingsName"> Name of the settings. </param>
        /// <param name="defaultValue"> A <see cref="System.DateTime">String</see> expression that
        /// specifies a default value to return if the key is not found.
        /// </param>
        /// <returns> A <see cref="System.DateTime">DateTime</see> data type. </returns>
        public DateTime Read(string settingsName, DateTime defaultValue)
        {
            return Read( this.FilePath, this.SectionName, settingsName, defaultValue);
        }

        /// <summary> Reads a string value from the INI file. </summary>
        /// <remarks> Use This method to read a String value from an INI file. </remarks>
        /// <param name="settingsName"> Name of the settings. </param>
        /// <param name="defaultValue"> A <see cref="System.DateTimeOffset">String</see> expression that
        /// specifies a default value to return if the key is not found.
        /// </param>
        /// <returns> A <see cref="System.DateTimeOffset">DateTimeOffset</see> data type. </returns>
        public DateTimeOffset Read(string settingsName, DateTimeOffset defaultValue)
        {
            return Read( this.FilePath, this.SectionName, settingsName, defaultValue);
        }

        /// <summary> Reads a "T:Decimal" Section value from the INI file. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <param name="settingsName"> Name of the settings. </param>
        /// <param name="defaultValue"> A <see cref="System.Decimal">String</see> expression that
        /// specifies a default value to return if the key is not found.
        /// </param>
        /// <returns> A <see cref="System.Decimal">Decimal</see> data type. </returns>
        public decimal Read(string settingsName, decimal defaultValue)
        {
            return Read( this.FilePath, this.SectionName, settingsName, defaultValue);
        }

        /// <summary> Reads a "T:Double" Section value from the INI file. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <param name="settingsName"> Name of the settings. </param>
        /// <param name="defaultValue"> A <see cref="System.Double">String</see> expression that
        /// specifies a default value to return if the key is not found.
        /// </param>
        /// <returns> A <see cref="System.Double">Double</see> data type. </returns>
        public double Read(string settingsName, double defaultValue)
        {
            return Read( this.FilePath, this.SectionName, settingsName, defaultValue);
        }

        /// <summary> Reads a "T:Int16" Section value from the INI file. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <param name="settingsName"> Name of the settings. </param>
        /// <param name="defaultValue"> A <see cref="System.Int16">String</see> expression that specifies
        /// a default value to return if the key is not found. </param>
        /// <returns> A <see cref="System.Int16">Int16</see> data type. </returns>
        public short Read(string settingsName, short defaultValue)
        {
            return Read( this.FilePath, this.SectionName, settingsName, defaultValue);
        }

        /// <summary> Reads a "T:Int32" Section value from the INI file. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <param name="settingsName"> Name of the settings. </param>
        /// <param name="defaultValue"> A <see cref="System.Int32">String</see> expression that specifies
        /// a default value to return if the key is not found. </param>
        /// <returns> A <see cref="System.Int32">Int32</see> data type. </returns>
        public int Read(string settingsName, int defaultValue)
        {
            return Read( this.FilePath, this.SectionName, settingsName, defaultValue);
        }

        /// <summary> Reads a "T:Int64" Section value from the INI file. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <param name="settingsName"> Name of the settings. </param>
        /// <param name="defaultValue"> A <see cref="System.Int64">String</see> expression that specifies
        /// a default value to return if the key is not found. </param>
        /// <returns> A <see cref="System.Int64">Int64</see> data type. </returns>
        public long Read(string settingsName, long defaultValue)
        {
            return Read( this.FilePath, this.SectionName, settingsName, defaultValue);
        }

        /// <summary> Reads a "T:Single" Section value from the INI file. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <param name="settingsName"> Name of the settings. </param>
        /// <param name="defaultValue"> A <see cref="System.Single">String</see> expression that
        /// specifies a default value to return if the key is not found.
        /// </param>
        /// <returns> A <see cref="System.Single">Single</see> data type. </returns>
        public float Read(string settingsName, float defaultValue)
        {
            return Read( this.FilePath, this.SectionName, settingsName, defaultValue);
        }

        /// <summary> Reads a "T:String" Section value from the INI file. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <param name="settingsName"> Name of the settings. </param>
        /// <param name="defaultValue"> A <see cref="System.String">String</see> expression that
        /// specifies a default value to return if the key is not found.
        /// </param>
        /// <returns> A <see cref="System.String">String</see> data type. </returns>
        public string Read(string settingsName, string defaultValue)
        {
            return Read( this.FilePath, this.SectionName, settingsName, defaultValue);
        }

        #endregion

        #region " READ VALUE "

        /// <summary> Reads a "T:Boolean" value from the INI file. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <param name="defaultValue"> A <see cref="System.Boolean">String</see> expression that
        /// specifies a default value to return if the key is not found.
        /// </param>
        /// <returns> A <see cref="System.Boolean">Boolean</see> data type. </returns>
        public bool Read(bool defaultValue)
        {
            return Read( this.FilePath, this.SectionName, this.SettingName, defaultValue);
        }

        /// <summary> Reads a "T:Byte" value from the INI file. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <param name="defaultValue"> A <see cref="System.Byte">String</see> expression that specifies
        /// a default value to return if the key is not found. </param>
        /// <returns> A <see cref="System.Byte">Byte</see> data type. </returns>
        public byte Read(byte defaultValue)
        {
            return Read( this.FilePath, this.SectionName, this.SettingName, defaultValue);
        }

        /// <summary> Reads a "T:DateTime" value from the INI file. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <param name="defaultValue"> A <see cref="System.DateTime">String</see> expression that
        /// specifies a default value to return if the key is not found.
        /// </param>
        /// <returns> A <see cref="System.DateTime">DateTime</see> data type. </returns>
        public DateTime Read(DateTime defaultValue)
        {
            return Read( this.FilePath, this.SectionName, this.SettingName, defaultValue);
        }

        /// <summary> Reads a string value from the INI file. </summary>
        /// <remarks> Use This method to read a String value from an INI file. </remarks>
        /// <param name="defaultValue"> A <see cref="System.DateTimeOffset">DateTimeOffset</see>
        /// expression that specifies a default value to return if the key is
        /// not found. </param>
        /// <returns> A <see cref="System.String">String</see> data type. </returns>
        public DateTimeOffset Read(DateTimeOffset defaultValue)
        {
            return Read( this.FilePath, this.SectionName, this.SettingName, defaultValue);
        }

        /// <summary> Reads a "T:Decimal" value from the INI file. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <param name="defaultValue"> A <see cref="System.Decimal">String</see> expression that
        /// specifies a default value to return if the key is not found.
        /// </param>
        /// <returns> A <see cref="System.Decimal">Decimal</see> data type. </returns>
        public decimal Read(decimal defaultValue)
        {
            return Read( this.FilePath, this.SectionName, this.SettingName, defaultValue);
        }

        /// <summary> Reads a "T:Double" value from the INI file. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <param name="defaultValue"> A <see cref="System.Double">String</see> expression that
        /// specifies a default value to return if the key is not found.
        /// </param>
        /// <returns> A <see cref="System.Double">Double</see> data type. </returns>
        public double ReadDouble(double defaultValue)
        {
            return Read( this.FilePath, this.SectionName, this.SettingName, defaultValue);
        }

        /// <summary> Reads a "T:Int16" value from the INI file. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <param name="defaultValue"> A <see cref="System.Int16">String</see> expression that specifies
        /// a default value to return if the key is not found. </param>
        /// <returns> A <see cref="System.Int16">Int16</see> data type. </returns>
        public short Read(short defaultValue)
        {
            return Read( this.FilePath, this.SectionName, this.SettingName, defaultValue);
        }

        /// <summary> Reads a "T:Int32" value from the INI file. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <param name="defaultValue"> A <see cref="System.Int32">String</see> expression that specifies
        /// a default value to return if the key is not found. </param>
        /// <returns> A <see cref="System.Int32">Int32</see> data type. </returns>
        public int Read(int defaultValue)
        {
            return Read( this.FilePath, this.SectionName, this.SettingName, defaultValue);
        }

        /// <summary> Reads a "T:Int64" value from the INI file. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <param name="defaultValue"> A <see cref="System.Int64">String</see> expression that specifies
        /// a default value to return if the key is not found. </param>
        /// <returns> A <see cref="System.Int64">Int64</see> data type. </returns>
        public long Read(long defaultValue)
        {
            return Read( this.FilePath, this.SectionName, this.SettingName, defaultValue);
        }

        /// <summary> Reads a "T:Single" value from the INI file. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <param name="defaultValue"> A <see cref="System.Single">String</see> expression that
        /// specifies a default value to return if the key is not found.
        /// </param>
        /// <returns> A <see cref="System.Single">Single</see> data type. </returns>
        public float Read(float defaultValue)
        {
            return Read( this.FilePath, this.SectionName, this.SettingName, defaultValue);
        }

        /// <summary> Reads a "T:String" value from the INI file. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <param name="defaultValue"> A <see cref="System.String">String</see> expression that
        /// specifies a default value to return if the key is not found.
        /// </param>
        /// <returns> A <see cref="System.String">String</see> data type. </returns>
        public string Read(string defaultValue)
        {
            return Read( this.FilePath, this.SectionName, this.SettingName, defaultValue);
        }

        #endregion

        #region " WRITE LIST ITEEM "

        /// <summary> Writes a "T:Boolean" value to the INI file. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <param name="itemNumber"> Specifies the list item number to read. </param>
        /// <param name="value">      A <see cref="System.Boolean">Boolean</see> expression that specifies
        /// the value to write. </param>
        /// <returns> <c>True</c> if success; Otherwise, <c>False</c>. </returns>
        public bool Write(int itemNumber, bool value)
        {
            return Write( this.FilePath, this.SectionName, this.ListSettingName(itemNumber), value);
        }

        /// <summary> Writes a "T:DateTime" value to the INI file. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <param name="itemNumber"> Specifies the list item number to read. </param>
        /// <param name="value">      A <see cref="System.DateTime">DateTime</see> expression that
        /// specifies the value to write. </param>
        /// <param name="format">     Specifies how to format the value. </param>
        /// <returns> <c>True</c> if success; Otherwise, <c>False</c>. </returns>
        public bool Write(int itemNumber, DateTime value, string format)
        {
            return Write( this.FilePath, this.SectionName, this.ListSettingName(itemNumber), value, format);
        }

        /// <summary> Writes a "T:DateTime" value to the INI file. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <param name="itemNumber"> Specifies the list item number to read. </param>
        /// <param name="value">      A <see cref="System.DateTime">DateTime</see> expression that
        /// specifies the value to write. </param>
        /// <returns> <c>True</c> if success; Otherwise, <c>False</c>. </returns>
        public bool Write(int itemNumber, DateTime value)
        {
            return Write( this.FilePath, this.SectionName, this.ListSettingName(itemNumber), value);
        }

        /// <summary> Writes a "T:DateTime" value to the INI file. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <param name="itemNumber"> Specifies the list item number to read. </param>
        /// <param name="value">      A <see cref="System.DateTimeOffset">DateTimeOffset</see> expression
        /// that specifies the value to write. </param>
        /// <param name="format">     Specifies how to format the value. </param>
        /// <returns> <c>True</c> if success; Otherwise, <c>False</c>. </returns>
        public bool Write(int itemNumber, DateTimeOffset value, string format)
        {
            return Write( this.FilePath, this.SectionName, this.ListSettingName(itemNumber), value, format);
        }

        /// <summary> Writes a "T:Boolean" value to the INI file. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <param name="itemNumber"> Specifies the list item number to read. </param>
        /// <param name="value">      A <see cref="System.DateTimeOffset">DateTimeOffset</see> expression
        /// that specifies the value to write. </param>
        /// <returns> <c>True</c> if success; Otherwise, <c>False</c>. </returns>
        public bool Write(int itemNumber, DateTimeOffset value)
        {
            return Write( this.FilePath, this.SectionName, this.ListSettingName(itemNumber), value);
        }

        /// <summary> Writes a "T:Decimal" value to the INI file. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <param name="itemNumber"> Specifies the list item number to read. </param>
        /// <param name="value">      A <see cref="System.Decimal">Decimal</see> expression that specifies
        /// the value to write. </param>
        /// <param name="format">     Specifies how to format the value. </param>
        /// <returns> <c>True</c> if success; Otherwise, <c>False</c>. </returns>
        public bool Write(int itemNumber, decimal value, string format)
        {
            return Write( this.FilePath, this.SectionName, this.ListSettingName(itemNumber), value, format);
        }

        /// <summary> Writes a "T:Decimal" value to the INI file. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <param name="itemNumber"> Specifies the list item number to read. </param>
        /// <param name="value">      A <see cref="System.Decimal">Decimal</see> expression that specifies
        /// the value to write. </param>
        /// <returns> <c>True</c> if success; Otherwise, <c>False</c>. </returns>
        public bool Write(int itemNumber, decimal value)
        {
            return Write( this.FilePath, this.SectionName, this.ListSettingName(itemNumber), value);
        }

        /// <summary> Writes a "T:Double" value to the INI file. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <param name="itemNumber"> Specifies the list item number to read. </param>
        /// <param name="value">      A <see cref="System.Double">Double</see> expression that specifies
        /// the value to write. </param>
        /// <param name="format">     Specifies how to format the value. </param>
        /// <returns> <c>True</c> if success; Otherwise, <c>False</c>. </returns>
        public bool Write(int itemNumber, double value, string format)
        {
            return Write( this.FilePath, this.SectionName, this.ListSettingName(itemNumber), value, format);
        }

        /// <summary> Writes a "T:Double" value to the INI file. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <param name="itemNumber"> Specifies the list item number to read. </param>
        /// <param name="value">      A <see cref="System.Double">Double</see> expression that specifies
        /// the value to write. </param>
        /// <returns> <c>True</c> if success; Otherwise, <c>False</c>. </returns>
        public bool Write(int itemNumber, double value)
        {
            return Write( this.FilePath, this.SectionName, this.ListSettingName(itemNumber), value);
        }

        /// <summary> Writes a "T:Int64" value to the INI file. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <param name="itemNumber"> Specifies the list item number to read. </param>
        /// <param name="value">      A <see cref="System.Int64">Int64</see> expression that specifies the
        /// value to write. </param>
        /// <param name="format">     Specifies how to format the value. </param>
        /// <returns> <c>True</c> if success; Otherwise, <c>False</c>. </returns>
        public bool Write(int itemNumber, long value, string format)
        {
            return Write( this.FilePath, this.SectionName, this.ListSettingName(itemNumber), value, format);
        }

        /// <summary> Writes a "T:Int64" value to the INI file. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <param name="itemNumber"> Specifies the list item number to read. </param>
        /// <param name="value">      A <see cref="System.Int64">Int64</see> expression that specifies the
        /// value to write. </param>
        /// <returns> <c>True</c> if success; Otherwise, <c>False</c>. </returns>
        public bool Write(int itemNumber, long value)
        {
            return Write( this.FilePath, this.SectionName, this.ListSettingName(itemNumber), value);
        }

        /// <summary> Writes a "T:String" value to the INI file. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <param name="itemNumber"> Specifies the list item number to read. </param>
        /// <param name="value">      A <see cref="System.String">String</see> expression that specifies
        /// the value to write. </param>
        /// <returns> <c>True</c> if success; Otherwise, <c>False</c>. </returns>
        public bool Write(int itemNumber, string value)
        {
            return Write( this.FilePath, this.SectionName, this.ListSettingName(itemNumber), value);
        }

        #endregion

        #region " WRITE SECTION VALUE "

        /// <summary> Writes a "T:Boolean" value to the INI file. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <param name="settingsName"> Name of the settings. </param>
        /// <param name="value">        A <see cref="System.Boolean">Boolean</see> expression that
        /// specifies the value to write. </param>
        /// <returns> <c>True</c> if success; Otherwise, <c>False</c>. </returns>
        public bool Write(string settingsName, bool value)
        {
            return Write( this.FilePath, this.SectionName, settingsName, value);
        }

        /// <summary> Writes a "T:DateTime" value to the INI file. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <param name="settingsName"> Name of the settings. </param>
        /// <param name="value">        A <see cref="System.DateTime">DateTime</see> expression that
        /// specifies the value to write. </param>
        /// <param name="format">       Specifies how to format the value. </param>
        /// <returns> <c>True</c> if success; Otherwise, <c>False</c>. </returns>
        public bool Write(string settingsName, DateTime value, string format)
        {
            return Write( this.FilePath, this.SectionName, settingsName, value, format);
        }

        /// <summary> Writes a "T:DateTime" value to the INI file. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <param name="settingsName"> Name of the settings. </param>
        /// <param name="value">        A <see cref="System.DateTime">DateTime</see> expression that
        /// specifies the value to write. </param>
        /// <returns> <c>True</c> if success; Otherwise, <c>False</c>. </returns>
        public bool Write(string settingsName, DateTime value)
        {
            return Write( this.FilePath, this.SectionName, settingsName, value);
        }

        /// <summary> Writes a "T:DateTimeOffset" value to the INI file. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <param name="settingsName"> Name of the settings. </param>
        /// <param name="value">        A <see cref="System.DateTimeOffset">DateTimeOffset</see>
        /// expression that specifies the value to write. </param>
        /// <param name="format">       Specifies how to format the value. </param>
        /// <returns> <c>True</c> if success; Otherwise, <c>False</c>. </returns>
        public bool Write(string settingsName, DateTimeOffset value, string format)
        {
            return Write( this.FilePath, this.SectionName, settingsName, value, format);
        }

        /// <summary> Writes a "T:DateTimeOffset" value to the INI file. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <param name="settingsName"> Name of the settings. </param>
        /// <param name="value">        A <see cref="System.DateTimeOffset">DateTimeOffset</see>
        /// expression that specifies the value to write. </param>
        /// <returns> <c>True</c> if success; Otherwise, <c>False</c>. </returns>
        public bool Write(string settingsName, DateTimeOffset value)
        {
            return Write( this.FilePath, this.SectionName, settingsName, value);
        }

        /// <summary> Writes a "T:Decimal" value to the INI file. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <param name="settingsName"> Name of the settings. </param>
        /// <param name="value">        A <see cref="System.Decimal">Decimal</see> expression that
        /// specifies the value to write. </param>
        /// <param name="format">       Specifies how to format the value. </param>
        /// <returns> <c>True</c> if success; Otherwise, <c>False</c>. </returns>
        public bool Write(string settingsName, decimal value, string format)
        {
            return Write( this.FilePath, this.SectionName, settingsName, value, format);
        }

        /// <summary> Writes a "T:Decimal" value to the INI file. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <param name="settingsName"> Name of the settings. </param>
        /// <param name="value">        A <see cref="System.Decimal">Decimal</see> expression that
        /// specifies the value to write. </param>
        /// <returns> <c>True</c> if success; Otherwise, <c>False</c>. </returns>
        public bool Write(string settingsName, decimal value)
        {
            return Write( this.FilePath, this.SectionName, settingsName, value);
        }

        /// <summary> Writes a "T:Double" value to the INI file. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <param name="settingsName"> Name of the settings. </param>
        /// <param name="value">        A <see cref="System.Double">Double</see> expression that
        /// specifies the value to write. </param>
        /// <param name="format">       Specifies how to format the value. </param>
        /// <returns> <c>True</c> if success; Otherwise, <c>False</c>. </returns>
        public bool Write(string settingsName, double value, string format)
        {
            return Write( this.FilePath, this.SectionName, settingsName, value, format);
        }

        /// <summary> Writes a "T:Double" value to the INI file. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <param name="settingsName"> Name of the settings. </param>
        /// <param name="value">        A <see cref="System.Double">Double</see> expression that
        /// specifies the value to write. </param>
        /// <returns> <c>True</c> if success; Otherwise, <c>False</c>. </returns>
        public bool Write(string settingsName, double value)
        {
            return Write( this.FilePath, this.SectionName, settingsName, value);
        }

        /// <summary> Writes a "T:Int64" value to the INI file. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <param name="settingsName"> Name of the settings. </param>
        /// <param name="value">        A <see cref="System.Int64">Int64</see> expression that specifies
        /// the value to write. </param>
        /// <param name="format">       Specifies how to format the value. </param>
        /// <returns> <c>True</c> if success; Otherwise, <c>False</c>. </returns>
        public bool Write(string settingsName, long value, string format)
        {
            return Write( this.FilePath, this.SectionName, settingsName, value, format);
        }

        /// <summary> Writes a "T:Int64" value to the INI file. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <param name="settingsName"> Name of the settings. </param>
        /// <param name="value">        A <see cref="System.Int64">Int64</see> expression that specifies
        /// the value to write. </param>
        /// <returns> <c>True</c> if success; Otherwise, <c>False</c>. </returns>
        public bool Write(string settingsName, long value)
        {
            return Write( this.FilePath, this.SectionName, settingsName, value);
        }

        /// <summary> Writes a "T:String" value to the INI file. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <param name="settingsName"> Name of the settings. </param>
        /// <param name="value">        A <see cref="System.String">String</see> expression that
        /// specifies the value to write. </param>
        /// <returns> <c>True</c> if success; Otherwise, <c>False</c>. </returns>
        public bool Write(string settingsName, string value)
        {
            return Write( this.FilePath, this.SectionName, settingsName, value);
        }

        #endregion

        #region " WRITE VALUE "

        /// <summary> Writes a "T:Boolean" value to the INI file. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <param name="value"> A <see cref="System.Boolean">Boolean</see> expression that specifies the
        /// value to write. </param>
        /// <returns> <c>True</c> if success; Otherwise, <c>False</c>. </returns>
        public bool Write(bool value)
        {
            return Write( this.FilePath, this.SectionName, this.SettingName, value);
        }

        /// <summary> Writes a "T:Byte" value to the INI file. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <param name="value">  A <see cref="System.Byte">Byte</see> expression that specifies the value
        /// to write. </param>
        /// <param name="format"> Specifies how to format the value. </param>
        /// <returns> <c>True</c> if success; Otherwise, <c>False</c>. </returns>
        public bool Write(byte value, string format)
        {
            return Write( this.FilePath, this.SectionName, this.SettingName, value, format);
        }

        /// <summary> Writes a "T:Byte" value to the INI file. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <param name="value"> A <see cref="System.Byte">Byte</see> expression that specifies the value
        /// to write. </param>
        /// <returns> <c>True</c> if success; Otherwise, <c>False</c>. </returns>
        public bool Write(byte value)
        {
            return Write( this.FilePath, this.SectionName, this.SettingName, value);
        }

        /// <summary> Writes a "T:DateTime" value to the INI file. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <param name="value">  A <see cref="System.DateTime">DateTime</see> expression that specifies
        /// the value to write. </param>
        /// <param name="format"> Specifies how to format the value. </param>
        /// <returns> <c>True</c> if success; Otherwise, <c>False</c>. </returns>
        public bool Write(DateTime value, string format)
        {
            return Write( this.FilePath, this.SectionName, this.SettingName, value, format);
        }

        /// <summary> Writes a "T:DateTime" value to the INI file. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <param name="value"> A <see cref="System.DateTime">DateTime</see> expression that specifies
        /// the value to write. </param>
        /// <returns> <c>True</c> if success; Otherwise, <c>False</c>. </returns>
        public bool Write(DateTime value)
        {
            return Write( this.FilePath, this.SectionName, this.SettingName, value);
        }

        /// <summary> Writes a "T:DateTimeOffset" value to the INI file. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <param name="value">  A <see cref="System.DateTimeOffset">DateTimeOffset</see> expression that
        /// specifies the value to write. </param>
        /// <param name="format"> Specifies how to format the value. </param>
        /// <returns> <c>True</c> if success; Otherwise, <c>False</c>. </returns>
        public bool Write(DateTimeOffset value, string format)
        {
            return Write( this.FilePath, this.SectionName, this.SettingName, value, format);
        }

        /// <summary> Writes a "T:DateTimeOffset" value to the INI file. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <param name="value"> A <see cref="System.DateTimeOffset">DateTimeOffset</see> expression that
        /// specifies the value to write. </param>
        /// <returns> <c>True</c> if success; Otherwise, <c>False</c>. </returns>
        public bool Write(DateTimeOffset value)
        {
            return Write( this.FilePath, this.SectionName, this.SettingName, value);
        }

        /// <summary> Writes a "T:Decimal" value to the INI file. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <param name="value">  A <see cref="System.Decimal">Decimal</see> expression that specifies the
        /// value to write. </param>
        /// <param name="format"> Specifies how to format the value. </param>
        /// <returns> <c>True</c> if success; Otherwise, <c>False</c>. </returns>
        public bool Write(decimal value, string format)
        {
            return Write( this.FilePath, this.SectionName, this.SettingName, value, format);
        }

        /// <summary> Writes a "T:Decimal" value to the INI file. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <param name="value"> A <see cref="System.Decimal">Decimal</see> expression that specifies the
        /// value to write. </param>
        /// <returns> <c>True</c> if success; Otherwise, <c>False</c>. </returns>
        public bool Write(decimal value)
        {
            return Write( this.FilePath, this.SectionName, this.SettingName, value);
        }

        /// <summary> Writes a "T:Double" value to the INI file. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <param name="value">  A <see cref="System.Double">Double</see> expression that specifies the
        /// value to write. </param>
        /// <param name="format"> Specifies how to format the value. </param>
        /// <returns> <c>True</c> if success; Otherwise, <c>False</c>. </returns>
        public bool Write(double value, string format)
        {
            return Write( this.FilePath, this.SectionName, this.SettingName, value, format);
        }

        /// <summary> Writes a "T:Double" value to the INI file. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <param name="value"> A <see cref="System.Double">Double</see> expression that specifies the
        /// value to write. </param>
        /// <returns> <c>True</c> if success; Otherwise, <c>False</c>. </returns>
        public bool Write(double value)
        {
            return Write( this.FilePath, this.SectionName, this.SettingName, value);
        }

        /// <summary> Writes a "T:Int64" value to the INI file. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <param name="value">  A <see cref="System.Int64">Int64</see> expression that specifies the
        /// value to write. </param>
        /// <param name="format"> Specifies how to format the value. </param>
        /// <returns> <c>True</c> if success; Otherwise, <c>False</c>. </returns>
        public bool Write(long value, string format)
        {
            return Write( this.FilePath, this.SectionName, this.SettingName, value, format);
        }

        /// <summary> Writes a "T:Int64" value to the INI file. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <param name="value"> A <see cref="System.Int64">Int64</see> expression that specifies the
        /// value to write. </param>
        /// <returns> <c>True</c> if success; Otherwise, <c>False</c>. </returns>
        public bool Write(long value)
        {
            return Write( this.FilePath, this.SectionName, this.SettingName, value);
        }

        /// <summary> Writes a "T:String" value to the INI file. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <param name="value"> A <see cref="System.String">String</see> expression that specifies the
        /// value to write. </param>
        /// <returns> <c>True</c> if success; Otherwise, <c>False</c>. </returns>
        public bool Write(string value)
        {
            return Write( this.FilePath, this.SectionName, this.SettingName, value);
        }

        #endregion

    }
}
