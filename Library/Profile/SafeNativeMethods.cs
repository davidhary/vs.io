using System;
using System.Runtime.InteropServices;

namespace isr.IO
{

    /// <summary>
        /// Gets or sets safe application programming interface calls. This class suppresses stack walks
        /// for unmanaged code permission.  This class is for methods that are safe for anyone to call.
        /// Callers of these methods are not required to do a full security review to ensure that the
        /// usage is secure because the methods are harmless for any caller.
        /// </summary>
        /// <remarks>
        /// David, 2005-01-07, 1.0.1832. The A versions of the functions seems to work only when omitting
        /// the character set specifications.<para>
        /// (c) 2010 Integrated Scientific Resources, Inc. All rights reserved. </para><para>
        /// Licensed under The MIT License. </para>
        /// Removed: [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method | AttributeTargets.Interface)]
        ///
        /// </remarks>
    public sealed class SafeNativeMethods
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Prevents construction of this class. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        private SafeNativeMethods()
        {
        }

        #endregion

        #region " IMPORTS "

        /// <summary> Gets profile section. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <param name="sectionName">  Name of the section. </param>
        /// <param name="returnedData"> Information describing the returned. </param>
        /// <param name="dataLength">   Length of the data. </param>
        /// <returns> The length of the profile section. </returns>
        [DllImport("KERNEL32.DLL", EntryPoint = "GetProfileSectionW", CharSet = CharSet.Unicode)]
        internal static extern int GetProfileSection(string sectionName, byte[] returnedData, int dataLength);

        /// <summary> Gets private profile section. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <param name="sectionName">  Name of the section. </param>
        /// <param name="returnedData"> Information describing the returned. </param>
        /// <param name="dataLength">   Length of the data. </param>
        /// <param name="filePath">     Full pathname of the file. </param>
        /// <returns> The length of the private profile section. </returns>
        [DllImport("KERNEL32.DLL", EntryPoint = "GetPrivateProfileSectionW", CharSet = CharSet.Unicode)]
        internal static extern int GetPrivateProfileSection(string sectionName, byte[] returnedData, int dataLength, string filePath);

        /// <summary> Gets private profile section names. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <param name="returnedData"> Information describing the returned. </param>
        /// <param name="dataLength">   Length of the data. </param>
        /// <param name="filePath">     Full pathname of the file. </param>
        /// <returns> The length of the private profile section names. </returns>
        [DllImport("KERNEL32.DLL", EntryPoint = "GetPrivateProfileSectionNamesW", CharSet = CharSet.Unicode)]
        internal static extern int GetPrivateProfileSectionNames(byte[] returnedData, int dataLength, string filePath);

        /// <summary> Gets profile string. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <param name="sectionName">  Name of the section. </param>
        /// <param name="keyName">      Name of the key. </param>
        /// <param name="defaultData">  The default data. </param>
        /// <param name="returnedData"> Information describing the returned. </param>
        /// <param name="dataLength">   Length of the data. </param>
        /// <returns> The profile string. </returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Code Quality", "IDE0051:Remove unused private members", Justification = "<Pending>")]
        [DllImport("KERNEL32.DLL", EntryPoint = "GetProfileStringW", CharSet = CharSet.Unicode)]
        internal static extern int GetProfileString(string sectionName, string keyName, string defaultData, string returnedData, int dataLength);

        /// <summary> Gets profile string. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <param name="sectionName">  Name of the section. </param>
        /// <param name="keyName">      Name of the key. </param>
        /// <param name="defaultData">  The default data. </param>
        /// <param name="returnedData"> Information describing the returned. </param>
        /// <param name="dataLength">   Length of the data. </param>
        /// <returns> The length of the profile string. </returns>
        [DllImport("KERNEL32.DLL", EntryPoint = "GetProfileStringW", CharSet = CharSet.Unicode)]
        internal static extern int GetProfileString(string sectionName, string keyName, string defaultData, System.Text.StringBuilder returnedData, int dataLength);

        /// <summary> Gets private profile string. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <param name="sectionName">  Name of the section. </param>
        /// <param name="keyName">      Name of the key. </param>
        /// <param name="defaultData">  The default data. </param>
        /// <param name="returnedData"> Information describing the returned. </param>
        /// <param name="dataLength">   Length of the data. </param>
        /// <param name="filePath">     Full pathname of the file. </param>
        /// <returns> The length of the private profile string. </returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Code Quality", "IDE0051:Remove unused private members", Justification = "<Pending>")]
        [DllImport("kernel32.dll", EntryPoint = "GetPrivateProfileStringW", CharSet = CharSet.Unicode)]
        internal static extern int GetPrivateProfileString(string sectionName, string keyName, string defaultData, string returnedData, int dataLength, string filePath);

        /// <summary>
        /// Reads private profile string using 'GetPrivateProfileStringA' without specifying the
        /// character set.
        /// </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <param name="sectionName"> Name of the section. </param>
        /// <param name="keyName"> Name of the key. </param>
        /// <param name="defaultData"> The default data. </param>
        /// <param name="returnedData"> Information describing the returned. </param>
        /// <param name="dataLength"> Length of the data. </param>
        /// <param name="filePath"> Full pathname of the file. </param>
        /// <returns> The length of the profile string. </returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Code Quality", "IDE0051:Remove unused private members", Justification = "<Pending>")]
        [Obsolete("Using GetPrivateProfileStringA fails reading a string such as 2019-07-20'; it reads only 7")]
        [DllImport("kernel32.dll", EntryPoint = "GetPrivateProfileStringA")]
        internal static extern int ReadPrivateProfileString([MarshalAs(UnmanagedType.LPWStr)] string sectionName, [MarshalAs(UnmanagedType.LPWStr)] string keyName, [MarshalAs(UnmanagedType.LPWStr)] string defaultData, [MarshalAs(UnmanagedType.LPWStr)] string returnedData, int dataLength, [MarshalAs(UnmanagedType.LPWStr)] string filePath);

        /// <summary> Gets private profile string. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <param name="sectionName">  Name of the section. </param>
        /// <param name="keyName">      Name of the key. </param>
        /// <param name="defaultData">  The default data. </param>
        /// <param name="returnedData"> Information describing the returned. </param>
        /// <param name="dataLength">   Length of the data. </param>
        /// <param name="filePath">     Full pathname of the file. </param>
        /// <returns> The length of the private profile string. </returns>
        [DllImport("KERNEL32.DLL", EntryPoint = "GetPrivateProfileStringW", CharSet = CharSet.Unicode)]
        internal static extern int GetPrivateProfileString(string sectionName, string keyName, string defaultData, System.Text.StringBuilder returnedData, int dataLength, string filePath);

        /// <summary> Gets private profile integer. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <param name="sectionName"> Name of the section. </param>
        /// <param name="keyName">     Name of the key. </param>
        /// <param name="defaultData"> The default data. </param>
        /// <param name="filePath">    Full pathname of the file. </param>
        /// <returns> The private profile Integer. </returns>
        [DllImport("KERNEL32.DLL", EntryPoint = "GetPrivateProfileIntW", CharSet = CharSet.Unicode)]
        internal static extern int GetPrivateProfile(string sectionName, string keyName, int defaultData, string filePath);

        /// <summary>
        /// Writes a private profile section. Writes a list of all names and values for a section of a
        /// private profile (.ini) file.
        /// </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <param name="sectionName"> Name of the section. </param>
        /// <param name="data">        The data. </param>
        /// <param name="filePath">    Full pathname of the file. </param>
        /// <returns> <c>True</c>  if okay; Otherwise, <c>False</c>. </returns>
        [DllImport("KERNEL32.DLL", EntryPoint = "WritePrivateProfileSectionW", CharSet = CharSet.Unicode)]
        internal static extern bool WritePrivateProfileSection(string sectionName, string data, string filePath);

        /// <summary> Writes a private profile section. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <param name="sectionName"> Name of the section. </param>
        /// <param name="data">        The data. </param>
        /// <param name="filePath">    Full pathname of the file. </param>
        /// <returns> <c>True</c>  if okay; Otherwise, <c>False</c>. </returns>
        [DllImport("KERNEL32.DLL", EntryPoint = "WritePrivateProfileSectionW", CharSet = CharSet.Unicode)]
        internal static extern bool WritePrivateProfileSection(string sectionName, byte[] data, string filePath);

        /// <summary> Writes a private profile string. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <param name="sectionName"> Name of the section. </param>
        /// <param name="keyName">     Name of the key. </param>
        /// <param name="data">        The data. </param>
        /// <param name="filePath">    Full pathname of the file. </param>
        /// <returns> <c>True</c>  if okay; Otherwise, <c>False</c>. </returns>
        [DllImport("KERNEL32.DLL", EntryPoint = "WritePrivateProfileStringW", CharSet = CharSet.Unicode)]
        // Leave function empty - Import attribute forwards calls to KERNEL32.DLL.
        internal static extern bool WritePrivateProfileString(string sectionName, string keyName, string data, string filePath);

        /// <summary> Writes a private profile string. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <param name="sectionName"> Name of the section. </param>
        /// <param name="data">        The data. </param>
        /// <param name="filePath">    Full pathname of the file. </param>
        /// <returns> <c>True</c>  if okay; Otherwise, <c>False</c>. </returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Code Quality", "IDE0051:Remove unused private members", Justification = "<Pending>")]
        [DllImport("KERNEL32.DLL", EntryPoint = "WritePrivateProfileStringW", SetLastError = true, CharSet = CharSet.Unicode, ExactSpelling = true, CallingConvention = CallingConvention.StdCall)]
        internal static extern bool WritePrivateProfileString(string sectionName, byte[] data, string filePath);

        /// <summary>
        /// Writes a profile section. Writes a list of all names and values for a section of the WIN.INI
        /// file.
        /// </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <param name="sectionName"> Name of the section. </param>
        /// <param name="data">        The data. </param>
        /// <returns> <c>True</c>  if okay; Otherwise, <c>False</c>. </returns>
        [DllImport("KERNEL32.DLL", EntryPoint = "WriteProfileSectionW", SetLastError = true, CharSet = CharSet.Unicode, ExactSpelling = true, CallingConvention = CallingConvention.StdCall)]
        internal static extern bool WriteProfileSection(string sectionName, byte[] data);

        /// <summary> Writes a profile section. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <param name="sectionName"> Name of the section. </param>
        /// <param name="data">        The data. </param>
        /// <returns> <c>True</c>  if okay; Otherwise, <c>False</c>. </returns>
        [DllImport("KERNEL32.DLL", EntryPoint = "WriteProfileSectionW", CharSet = CharSet.Unicode)]
        internal static extern bool WriteProfileSection(string sectionName, string data);

        /// <summary> Writes a profile string. Writes a string to the WIN.INI file. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <param name="sectionName"> Name of the section. </param>
        /// <param name="keyName">     Name of the key. </param>
        /// <param name="data">        The data. </param>
        /// <returns> <c>True</c>  if okay; Otherwise, <c>False</c>. </returns>
        [DllImport("KERNEL32.DLL", EntryPoint = "WriteProfileStringW", CharSet = CharSet.Unicode)]
        internal static extern bool WriteProfileString(string sectionName, string keyName, string data);

        #endregion

    }
}
