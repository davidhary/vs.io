﻿using System;

namespace isr.IO.Office
{

    /// <summary> Defines a base class for reading spreadsheets. </summary>
        /// <remarks>
        /// (c) 2009 Integrated Scientific Resources, Inc. All rights reserved. <para>
        /// Licensed under The MIT License.</para><para>
        /// David, 2009-01-07, 1.2.3294.x. </para>
        /// </remarks>
    public abstract class SpreadsheetReaderBase : IDisposable
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Specialized default constructor for use only by derived class. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        protected SpreadsheetReaderBase() : base()
        {
        }

        #region " IDISPOSABLE SUPPORT "

        /// <summary> True if is disposed, false if not. </summary>

        /// <summary>
        /// Gets or sets the dispose status sentinel of the base class.  This applies to the derived
        /// class provided proper implementation.
        /// </summary>
        /// <value> The is disposed. </value>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Code Quality", "IDE0051:Remove unused private members", Justification = "<Pending>" )]
        protected bool IsDisposed { get; private set; }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged
        /// resources.
        /// </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <param name="disposing"> True to release both managed and unmanaged resources; false to
        /// release only unmanaged resources. </param>
        protected virtual void Dispose(bool disposing)
        {
            this.IsDisposed = true;
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged
        /// resources.
        /// </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        public void Dispose()
        {
            this.Dispose(true);
            GC.SuppressFinalize(this);
        }

        #endregion

        #endregion

        /// <summary> Gets or sets the name of the last file that was read. </summary>
        /// <value> The full pathname of the file file. </value>
        public string FilePathName { get; set; }

        /// <summary> Reads all spreadsheets from the spreadsheet file. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <param name="filePathName"> . </param>
        /// <returns> True if it succeeds, false if it fails. </returns>
        public abstract bool ReadWorkbook(string filePathName);

        /// <summary> Returns the name of the currently selected worksheet. </summary>
        /// <value> The name of the current worksheet. </value>
        public string CurrentWorksheetName { get; protected set; }

        /// <summary> Selects a worksheet by name. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <param name="name"> The name. </param>
        /// <returns> True if it succeeds, false if it fails. </returns>
        public abstract bool ActivateWorksheet(string name);

        /// <summary> Returns a cell value from the work sheet or DB NUll. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <param name="row"> Specifies a row number. </param>
        /// <param name="col"> Specifies a column number. </param>
        /// <returns> True if it succeeds, false if it fails. </returns>
        public abstract object CellValueGetter(int row, int col);

        /// <summary> Returns the value of the given cell as a Nullable Boolean type. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <param name="row">          Specifies a row number. </param>
        /// <param name="col">          Specifies a column number. </param>
        /// <param name="defaultValue"> Specifies a default value. </param>
        /// <returns> True if it succeeds, false if it fails. </returns>
        public abstract bool? CellValueGetter(int row, int col, bool? defaultValue);

        /// <summary> Returns the value of the specified cell as a Boolean type. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <param name="row">          Specifies a row number. </param>
        /// <param name="col">          Specifies a column number. </param>
        /// <param name="defaultValue"> Specifies a default value. </param>
        /// <returns> True if it succeeds, false if it fails. </returns>
        public abstract bool CellValueGetter(int row, int col, bool defaultValue);

        /// <summary> Returns the value of the specified cell as a Nullable Date type. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <param name="row">          Specifies a row number. </param>
        /// <param name="col">          Specifies a column number. </param>
        /// <param name="defaultValue"> Specifies a default value. </param>
        /// <returns> A DateTime? </returns>
        public abstract DateTime? CellValueGetter(int row, int col, DateTime? defaultValue);

        /// <summary> Returns the value of the specified cell as a Date type. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <param name="row">          Specifies a row number. </param>
        /// <param name="col">          Specifies a column number. </param>
        /// <param name="defaultValue"> Specifies a default value. </param>
        /// <returns> A DateTime. </returns>
        public abstract DateTime CellValueGetter(int row, int col, DateTime defaultValue);

        /// <summary> Returns the value of the specified cell as a Nullable Double type. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <param name="row">          Specifies a row number. </param>
        /// <param name="col">          Specifies a column number. </param>
        /// <param name="defaultValue"> Specifies a default value. </param>
        /// <returns> A Double? </returns>
        public abstract double? CellValueGetter(int row, int col, double? defaultValue);

        /// <summary> Returns the value of the specified cell as a Double type. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <param name="row">          Specifies a row number. </param>
        /// <param name="col">          Specifies a column number. </param>
        /// <param name="defaultValue"> Specifies a default value. </param>
        /// <returns> A Double. </returns>
        public abstract double CellValueGetter(int row, int col, double defaultValue);

        /// <summary> Returns the value of the specified cell as a Nullable Integer type. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <param name="row">          Specifies a row number. </param>
        /// <param name="col">          Specifies a column number. </param>
        /// <param name="defaultValue"> Specifies a default value. </param>
        /// <returns> An Integer? </returns>
        public abstract int? CellValueGetter(int row, int col, int? defaultValue);

        /// <summary> Returns the value of the specified cell as a Integer type. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <param name="row">          Specifies a row number. </param>
        /// <param name="col">          Specifies a column number. </param>
        /// <param name="defaultValue"> Specifies a default value. </param>
        /// <returns> An Integer. </returns>
        public abstract int CellValueGetter(int row, int col, int defaultValue);

        /// <summary> Returns a cell value or default value of String type. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <param name="row">          Specifies a row number. </param>
        /// <param name="col">          Specifies a column number. </param>
        /// <param name="defaultValue"> Specifies a default value. </param>
        /// <returns> A String. </returns>
        public abstract string CellValueGetter(int row, int col, string defaultValue);

        /// <summary> Gets or sets the left Column. </summary>
        /// <value> The current worksheet used range column number. </value>
        public int CurrentWorksheetUsedRangeColumnNumber { get; protected set; }

        /// <summary> Gets or sets the top row. </summary>
        /// <value> The current worksheet used range row number. </value>
        public int CurrentWorksheetUsedRangeRowNumber { get; protected set; }

        /// <summary> Returns <c>True</c> if the specified cell includes a DBNull value. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <param name="row"> Specifies a row number. </param>
        /// <param name="col"> Specifies a column number. </param>
        /// <returns> True if database null, false if not. </returns>
        public abstract bool IsDBNull(int row, int col);

        /// <summary> Returns <c>True</c> if the specified cell is empty. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <param name="row"> Specifies a row number. </param>
        /// <param name="col"> Specifies a column number. </param>
        /// <returns> True if empty, false if not. </returns>
        public abstract bool IsEmpty(int row, int col);

        /// <summary>
        /// Returns <c>True</c> if the specified cell is empty or includes a DBNull value.
        /// </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <param name="row"> Specifies a row number. </param>
        /// <param name="col"> Specifies a column number. </param>
        /// <returns> True if the database null or is empty, false if not. </returns>
        public abstract bool IsDBNullOrEmpty(int row, int col);
    }
}