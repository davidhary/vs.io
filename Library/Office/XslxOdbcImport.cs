﻿using System;
using System.Collections.Generic;
using System.Data;
using Microsoft.VisualBasic.CompilerServices;

namespace isr.IO.Office
{

    /// <summary> Imports XSLX files using OLE DB Provider. </summary>
        /// <remarks>
        /// David, 2009-01-07, 1.2.3294. Excel Data Manipulation Using VB.NET  <para>
        /// http://www.CodeProject.com/KB/vb/ExcelDataManipulation.aspx
        /// https://stackoverflow.com/questions/14261655/best-fastest-way-to-read-an-excel-sheet-into-a-datatable
        /// https://support.microsoft.com/en-us/help/278973/excelado-demonstrates-how-to-use-ado-to-read-and-write-data-in-excel-w
        /// https://blogs.msdn.microsoft.com/dataaccesstechnologies/2017/10/18/unexpected-error-from-external-database-driver-1-microsoft-jet-database-engine-after-applying-october-security-updates/https://social.msdn.microsoft.com/Forums/en-US/2feac7ff-3fbd-4d46-afdc-65341762f753/odbc-excel-driver-stopped-working-with-quotunexpected-error-from-external-database-driver-1?forum=sqldataaccess
        /// (c) 2009 Integrated Scientific Resources, Inc. All rights reserved. </para><para>
        /// Licensed under The MIT License. </para>
        /// </remarks>
    public sealed class XlsxOdbcImport
    {

        /// <summary> Prevent construction. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        private XlsxOdbcImport() : base()
        {
        }

        #region " OLEDB CONNECTION STRING BUILDERS "

        /// <summary> The jet provider version 4.0. </summary>
        public const string JetProvider4 = "Microsoft.Jet.OLEDB.4.0";

        /// <summary> The ace provider version 12.0. </summary>
        public const string AceProviderVersion12 = "Microsoft.Ace.OLEDB.12.0";

        /// <summary> The ace provider version 16. </summary>
        public const string AceProviderVersion16 = "Microsoft.Ace.OLEDB.12.0";

        /// <summary> Gets or sets the preferred provider. </summary>
        /// <value> The preferred provider. </value>
        public static string PreferredProvider { get; set; } = AceProviderVersion16;

        /// <summary>
        /// Builds an OLEDB Connection string to an excel file using the <see cref="PreferredProvider"/>.
        /// </summary>
        /// <remarks> Uses excel version 2000 and above. </remarks>
        /// <param name="filePathName"> Specifies the excel file name. </param>
        /// <returns> A String. </returns>
        public static string BuildConnectionString(string filePathName)
        {
            return BuildConnectionString(PreferredProvider, filePathName, false, false);
            // Dim format As String = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source={0};Extended Properties='Excel 8.0; IMEX=1;';"
            // Return String.Format(Globalization.CultureInfo.CurrentCulture, format, filePathName)
        }

        /// <summary>
        /// Builds an OLEDB Connection string to an excel file using the <see cref="PreferredProvider"/>.
        /// </summary>
        /// <remarks> Uses excel version 2000 and above. </remarks>
        /// <param name="provider">     The provider. </param>
        /// <param name="filePathName"> Specifies the excel file name. </param>
        /// <returns> A String. </returns>
        public static string BuildConnectionString(string provider, string filePathName)
        {
            return BuildConnectionString(provider, filePathName, false, true);
        }

        /// <summary>
        /// Builds an OLEDB Connection string to an excel file using the <see cref="PreferredProvider"/>.
        /// </summary>
        /// <remarks> Uses excel version 2000 and above. </remarks>
        /// <param name="provider">            The provider. </param>
        /// <param name="filePathName">        Specifies the excel file name. </param>
        /// <param name="userFirstRowHeaders"> True to export headers. </param>
        /// <returns> A String. </returns>
        public static string BuildConnectionString(string provider, string filePathName, bool userFirstRowHeaders)
        {
            return BuildConnectionString(provider, filePathName, false, userFirstRowHeaders);
        }

        /// <summary>
        /// Builds an OLEDB Connection string to an excel file using the <see cref="PreferredProvider"/>.
        /// </summary>
        /// <remarks>
        /// A setting of IMEX=1 is used in the connection string’s extended property determines whether
        /// the ImportMixedTypes value is honored. IMEX refers to Import/Export mode. There are three
        /// possible values. IMEX=0 and IMEX=2 result in ImportMixedTypes being ignored and the default
        /// value of 'Majority Types' is used. IMEX=1 is the only way to ensure ImportMixedTypes=Text is
        /// honored.
        /// </remarks>
        /// <param name="provider">              The provider. </param>
        /// <param name="filePathName">          Specifies the excel file name. </param>
        /// <param name="excelVersionBelow2000"> Specifies the excel version.
        /// For Excel 2000 and above, it is set it to Excel 8.0 and
        /// for all others, it is Excel 5.0. </param>
        /// <param name="userFirstRowHeaders">   Specifies the definition of header for each column. If
        /// the value is True, the first row will be treated as
        /// heading. Otherwise, the heading will be generated by the
        /// system like F1, F2 and so on. </param>
        /// <returns> A String. </returns>
        public static string BuildConnectionString(string provider, string filePathName, bool excelVersionBelow2000, bool userFirstRowHeaders)
        {
            string excelVersion = excelVersionBelow2000 ? "5.0" : "8.0";
            return BuildConnectionString(provider, filePathName, excelVersion, userFirstRowHeaders);
            // Dim format As String = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source={0};Extended Properties='Excel {1}; HDR={2}; IMEX=1;';"
            // Return String.Format(Globalization.CultureInfo.CurrentCulture, format, filePathName, If(excelVersionBelow2000, "5.0", "8.0"), If(userFirstRowHeaders, "Yes", "No"))
            // "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=.\excel\myExcelFile.xls;Extended Properties=Excel 8.0;HDR=Yes;"
        }

        /// <summary> Builds an OLEDB Connection string to an excel file. </summary>
        /// <remarks>
        /// A setting of IMEX=1 is used in the connection string’s extended property determines whether
        /// the ImportMixedTypes value is honored. IMEX refers to Import/Export mode. There are three
        /// possible values. IMEX=0 and IMEX=2 result in ImportMixedTypes being ignored and the default
        /// value of 'Majority Types' is used. IMEX=1 is the only way to ensure ImportMixedTypes=Text is
        /// honored.
        /// </remarks>
        /// <param name="provider">            The provider. </param>
        /// <param name="filePathName">        Specifies the excel file name. </param>
        /// <param name="excelVersion">        The excel version. </param>
        /// <param name="userFirstRowHeaders"> True to export headers. </param>
        /// <returns> A String. </returns>
        public static string BuildConnectionString(string provider, string filePathName, string excelVersion, bool userFirstRowHeaders)
        {
            return $"Provider={provider};Data Source={filePathName};Extended Properties='Excel {excelVersion}; HDR={(userFirstRowHeaders ? "Yes" : "No")}; IMEX=1;';";
        }

        #endregion

        #region " OLE DB READER "

        /// <summary> Returns a human readable sheet name from the internal sheet name. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <param name="internalSheetName"> Specifies the internal sheet name. </param>
        /// <returns> A String. </returns>
        private static string ParseSheetName(string internalSheetName)
        {
            return internalSheetName.Trim('$').Replace(Conversions.ToString('#'), ".");
        }

        /// <summary> Reads the work sheets from the workbook into datasets. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <param name="filePathName"> Specifies an Excel file path name. </param>
        /// <returns> The worksheets. </returns>
        public static DataSet ReadWorksheets(string filePathName)
        {
            var ds = new DataSet() { Locale = System.Globalization.CultureInfo.InvariantCulture };
            string connectionString = BuildConnectionString(filePathName);
            using (var connection = new System.Data.OleDb.OleDbConnection(connectionString))
            {
                connection.Open();
                var internalSheetNames = new List<string>();
                // get the list of sheets from the workbook
                using (var schemaTables = connection.GetOleDbSchemaTable(System.Data.OleDb.OleDbSchemaGuid.Tables, null))
                {
                    foreach (DataRow row in schemaTables.Rows)
                        internalSheetNames.Add(Conversions.ToString(row["TABLE_NAME"]));
                }

                // read each sheet into a table and add to the data set
                foreach (string internalSheetName in internalSheetNames)
                {
                    // get an adapter to read all records from the sheet
                    using var adapter = new System.Data.OleDb.OleDbDataAdapter( $"SELECT * FROM [{internalSheetName}]", connection );
                    string tableName = ParseSheetName( internalSheetName );
                    _ = adapter.TableMappings.Add( "Table", tableName );
                    _ = adapter.Fill( ds );
                }

                // close also disposes causing a double dispose; otherwise uncomment: connection.Close()
            }

            return ds;
        }

        #endregion

        #region " Excel File IMPORTER USING OLE DB "

        /// <summary> Imports a work book to a database from an Excel file. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <param name="connectionString"> Specifies an OLEDB Connection string. </param>
        /// <returns> A DataSet. </returns>
        public static DataSet ImportWorkbook(string connectionString)
        {
            var output = new DataSet() { Locale = System.Globalization.CultureInfo.InvariantCulture };
            using (var connection = new System.Data.OleDb.OleDbConnection(connectionString))
            {
                connection.Open();
                var dt = connection.GetOleDbSchemaTable(System.Data.OleDb.OleDbSchemaGuid.Tables, new object[] { null, null, null, "TABLE" });
                foreach (DataRow row in dt.Rows)
                {
                    string internalSheetName = row["TABLE_NAME"].ToString();
                    var cmd = new System.Data.OleDb.OleDbCommand("SELECT * FROM [" + internalSheetName + "]", connection) { CommandType = CommandType.Text };
                    string tableName = ParseSheetName(internalSheetName);
                    var outputTable = new DataTable(tableName) { Locale = System.Globalization.CultureInfo.InvariantCulture };
                    output.Tables.Add(outputTable);
                    _ = new System.Data.OleDb.OleDbDataAdapter( cmd ).Fill( outputTable );
                }

                // close also disposes causing a double dispose; otherwise uncomment: connection.Close()

            }

            return output;
        }

        /// <summary> Imports a work book to a database from an Excel file. </summary>
        /// <remarks> Uses Excel 8.0 file format. </remarks>
        /// <exception cref="System.IO.IOException"> Thrown when an IO failure occurred. </exception>
        /// <param name="provider">            The provider. </param>
        /// <param name="filePathName">        Specifies an Excel file path name. </param>
        /// <param name="userFirstRowHeaders"> Specifies the definition of header for each column. If
        /// the value is True, the first row will be treated as
        /// heading. Otherwise, the heading will be generated by the
        /// system like F1, F2 and so on. </param>
        /// <returns> A DataSet. </returns>
        public static DataSet ImportWorkbook(string provider, string filePathName, bool userFirstRowHeaders)
        {
            try
            {
                string connectionString = BuildConnectionString(provider, filePathName, false, userFirstRowHeaders);
                return ImportWorkbook(connectionString);
            }
            catch (System.Data.OleDb.OleDbException ex)
            {
                string additionalInfo = string.Empty;
                foreach (System.Data.OleDb.OleDbError oledbError in ex.Errors)
                    additionalInfo += string.Format(System.Globalization.CultureInfo.CurrentCulture, "Failed importing workbook. Message: {1}{0}Native Error: {2}{0}SQL State: {3}{0}Source: {4}", Environment.NewLine, oledbError.Message, oledbError.NativeError, oledbError.SQLState, oledbError.Source);
                throw new System.IO.IOException(additionalInfo, ex);
            }
            catch
            {
                throw;
            }
        }

        #endregion

    }
}