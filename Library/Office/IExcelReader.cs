﻿using System;

namespace isr.IO.Office
{

    /// <summary> Defines an interface for reading excel workbooks. </summary>
        /// <remarks>
        /// (c) 2009 Integrated Scientific Resources, Inc. All rights reserved. <para>
        /// Licensed under The MIT License.</para><para>
        /// David, 2009-01-07, 1.2.3294.x. </para>
        /// </remarks>
    public interface IExcelReader : IDisposable
    {

        /// <summary> Gets or sets the name of the last file that was read. </summary>
        /// <value> The full pathname of the file file. </value>
        string FilePathName { get; }

        /// <summary> Reads all worksheets from the Excel file. </summary>
        /// <param name="filePathName"> . </param>
        /// <returns> True if it succeeds, false if it fails. </returns>
        bool ReadWorkbook(string filePathName);

        /// <summary> Returns the name of the currently selected worksheet. </summary>
        /// <value> The name of the current worksheet. </value>
        string CurrentWorksheetName { get; }

        /// <summary> Selects a worksheet by name. </summary>
        /// <param name="name"> The name. </param>
        /// <returns> True if it succeeds, false if it fails. </returns>
        bool ActivateWorksheet(string name);

        /// <summary> Returns a cell value from the work sheet or DB NUll. </summary>
        /// <param name="row"> Specifies a row number. </param>
        /// <param name="col"> Specifies a column number. </param>
        /// <returns> True if it succeeds, false if it fails. </returns>
        object CellValueGetter(int row, int col);

        /// <summary> Returns the value of the given cell as a Nullable Boolean type. </summary>
        /// <param name="row">          Specifies a row number. </param>
        /// <param name="col">          Specifies a column number. </param>
        /// <param name="defaultValue"> Specifies a default value. </param>
        /// <returns> True if it succeeds, false if it fails. </returns>
        bool? CellValueGetter(int row, int col, bool? defaultValue);

        /// <summary> Returns the value of the specified cell as a Boolean type. </summary>
        /// <param name="row">          Specifies a row number. </param>
        /// <param name="col">          Specifies a column number. </param>
        /// <param name="defaultValue"> Specifies a default value. </param>
        /// <returns> True if it succeeds, false if it fails. </returns>
        bool CellValueGetter(int row, int col, bool defaultValue);

        /// <summary> Returns the value of the specified cell as a Nullable Date type. </summary>
        /// <param name="row">          Specifies a row number. </param>
        /// <param name="col">          Specifies a column number. </param>
        /// <param name="defaultValue"> Specifies a default value. </param>
        /// <returns> A DateTime? </returns>
        DateTime? CellValueGetter(int row, int col, DateTime? defaultValue);

        /// <summary> Returns the value of the specified cell as a Date type. </summary>
        /// <param name="row">          Specifies a row number. </param>
        /// <param name="col">          Specifies a column number. </param>
        /// <param name="defaultValue"> Specifies a default value. </param>
        /// <returns> A DateTime. </returns>
        DateTime CellValueGetter(int row, int col, DateTime defaultValue);

        /// <summary> Returns the value of the specified cell as a Nullable Double type. </summary>
        /// <param name="row">          Specifies a row number. </param>
        /// <param name="col">          Specifies a column number. </param>
        /// <param name="defaultValue"> Specifies a default value. </param>
        /// <returns> A Double? </returns>
        double? CellValueGetter(int row, int col, double? defaultValue);

        /// <summary> Returns the value of the specified cell as a Double type. </summary>
        /// <param name="row">          Specifies a row number. </param>
        /// <param name="col">          Specifies a column number. </param>
        /// <param name="defaultValue"> Specifies a default value. </param>
        /// <returns> A Double. </returns>
        double CellValueGetter(int row, int col, double defaultValue);

        /// <summary> Returns the value of the specified cell as a Nullable Integer type. </summary>
        /// <param name="row">          Specifies a row number. </param>
        /// <param name="col">          Specifies a column number. </param>
        /// <param name="defaultValue"> Specifies a default value. </param>
        /// <returns> An Integer? </returns>
        int? CellValueGetter(int row, int col, int? defaultValue);

        /// <summary> Returns the value of the specified cell as a Integer type. </summary>
        /// <param name="row">          Specifies a row number. </param>
        /// <param name="col">          Specifies a column number. </param>
        /// <param name="defaultValue"> Specifies a default value. </param>
        /// <returns> An Integer. </returns>
        int CellValueGetter(int row, int col, int defaultValue);

        /// <summary> Returns a cell value or default value of String type. </summary>
        /// <param name="row">          Specifies a row number. </param>
        /// <param name="col">          Specifies a column number. </param>
        /// <param name="defaultValue"> Specifies a default value. </param>
        /// <returns> A String. </returns>
        string CellValueGetter(int row, int col, string defaultValue);

        /// <summary> Gets or sets the left Column. </summary>
        /// <value> The current worksheet used range column number. </value>
        int CurrentWorksheetUsedRangeColumnNumber { get; }

        /// <summary> Gets or sets the top row. </summary>
        /// <value> The current worksheet used range row number. </value>
        int CurrentWorksheetUsedRangeRowNumber { get; }

        /// <summary> Returns <c>True</c> if the specified cell includes a DBNull value. </summary>
        /// <param name="row"> Specifies a row number. </param>
        /// <param name="col"> Specifies a column number. </param>
        /// <returns> True if database null, false if not. </returns>
        bool IsDBNull(int row, int col);

        /// <summary> Returns <c>True</c> if the specified cell is empty. </summary>
        /// <param name="row"> Specifies a row number. </param>
        /// <param name="col"> Specifies a column number. </param>
        /// <returns> True if empty, false if not. </returns>
        bool IsEmpty(int row, int col);

        /// <summary>
        /// Returns <c>True</c> if the specified cell is empty or includes a DBNull value.
        /// </summary>
        /// <param name="row"> Specifies a row number. </param>
        /// <param name="col"> Specifies a column number. </param>
        /// <returns> True if the database null or is empty, false if not. </returns>
        bool IsDBNullOrEmpty(int row, int col);
    }
}