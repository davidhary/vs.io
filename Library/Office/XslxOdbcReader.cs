using System;
using System.Data;
using System.Diagnostics;

namespace isr.IO.Office
{

    /// <summary>
    /// Handles access to Excel spreadsheets using an <see cref="IExcelReader">Excel Data Reader.
    /// </see>.
    /// </summary>
    /// <remarks>
    /// Requires using the ACE provided, which mean s installing the Access runtime for eaither 32 or
    /// 64 bits.  This likely to break programs using 64 or 32 bit provider, respectively, and is
    /// therefore unsafe. (c) 2009 Integrated Scientific Resources, Inc. All rights reserved. <para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2019-06-22,  </para><para>
    /// David, 2009-01-07, 1.2.3294.x. </para>
    /// </remarks>
    public class ExcelReader : SpreadsheetReaderBase
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Constructs this class. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        public ExcelReader() : base()
        {
        }

        /// <summary>
        /// Releases the unmanaged resources used by the <see cref="T:System.Windows.Forms.Control" />
        /// and its child controls and optionally releases the managed resources.
        /// </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <param name="disposing"> <c>True</c> to release both managed and unmanaged resources;
        /// <c>False</c> to release only unmanaged
        /// resources when called from the runtime
        /// finalize. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification = "Exception is published")]
        [DebuggerNonUserCode()]
        protected override void Dispose(bool disposing)
        {
            try
            {
                if (!this.IsDisposed && disposing)
                {
                }

                if ( this.Workbook is object)
                {
                    this.Workbook.Dispose();
                    this.Workbook = null;
                }

                if ( this._Worksheet is object)
                {
                    this._Worksheet.Dispose();
                    this._Worksheet = null;
                }
            }
            finally
            {
                base.Dispose(disposing);
            }
        }

        /// <summary>
        /// This destructor will run only if the Dispose method does not get called. It gives the base
        /// class the opportunity to finalize. Do not provide destructors in types derived from this
        /// class.
        /// </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        ~ExcelReader()
        {
            // Do not re-create Dispose clean-up code here.
            // Calling Dispose(false) is optimal for readability and maintainability.
            this.Dispose(false);
        }

        #endregion

        #region " EXCEL READER "

        /// <summary> Reads all worksheets from the Excel file. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <exception cref="System.IO.FileNotFoundException"> Thrown when the requested file is not present. </exception>
        /// <param name="filePathName"> . </param>
        /// <returns> True if it succeeds, false if it fails. </returns>
        public override bool ReadWorkbook(string filePathName)
        {

            // dispose of previous data
            if ( this.Workbook is object)
            {
                this.Workbook.Dispose();
                this.Workbook = null;
            }

            // dispose of the worksheet
            if ( this._Worksheet is object)
            {
                this._Worksheet.Dispose();
                this._Worksheet = null;
            }

            // clear the file name
            this.FilePathName = string.Empty;
            if (string.IsNullOrWhiteSpace(filePathName))
                throw new ArgumentNullException(nameof(filePathName));
            if (!System.IO.File.Exists(filePathName))
            {
                throw new System.IO.FileNotFoundException("Failed reading workbook because the workbook file -- file not found.", filePathName);
            }

            this.FilePathName = filePathName;

            // build the connection string using the default provider
            string connectionString = XlsxOdbcImport.BuildConnectionString(filePathName);

            // read the worksheets using OLE DB.
            this.Workbook = XlsxOdbcImport.ImportWorkbook(connectionString);
            return true;
        }

        /// <summary> Gets reference to the workbook dataset. </summary>
        /// <value> The workbook. </value>
        public DataSet Workbook { get; private set; }

        #endregion

        #region " WORKSHEET "

        /// <summary> The worksheet. </summary>
        private DataTable _Worksheet;

        /// <summary>
        /// Gets or sets reference to the worksheet table representing the last selected work sheet.
        /// </summary>
        /// <value> The worksheet. </value>
        public DataTable Worksheet
        {
            get => this._Worksheet;

            set {
                this._Worksheet = value;
                this.CurrentWorksheetName = this._Worksheet?.TableName;
                this.CurrentWorksheetUsedRangeColumnNumber = (this.Worksheet is object && (this._Worksheet?.Columns.Count) > 0) == true ? 1 : 0;
                this.CurrentWorksheetUsedRangeRowNumber = (this.Worksheet is object && (this._Worksheet?.Rows.Count) > 0) == true ? 1 : 0;
            }
        }

        /// <summary> Selects a worksheet by name. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
        /// <exception cref="System.IO.IOException">               Thrown when an IO failure occurred. </exception>
        /// <param name="name"> The name. </param>
        /// <returns> True if it succeeds, false if it fails. </returns>
        public override bool ActivateWorksheet(string name)
        {
            if ( this.Workbook is null)
            {
                throw new InvalidOperationException("failed activating worksheet because workbook was not read from the Excel file.");
            }

            // Select the specified sheet
            if (!string.IsNullOrWhiteSpace( this.CurrentWorksheetName ) && this.CurrentWorksheetName.Equals(name, StringComparison.OrdinalIgnoreCase))
            {
                return true;
            }
            else
            {
                try
                {
                    this._Worksheet = this.Workbook.Tables[name];
                }
                finally
                {
                }

                if (string.IsNullOrWhiteSpace( this.CurrentWorksheetName ))
                {
                    throw new System.IO.IOException($"failed activating worksheet {name} in {this.FilePathName}");
                    return false;
                }
                else
                {
                    return true;
                }
            }
        }

        #endregion

        #region " GETTERS "

        /// <summary> Returns a cell value from the work sheet. </summary>
        /// <remarks> Excel rows and columns are one-based. </remarks>
        /// <exception cref="System.IO.IOException">                 Thrown when an IO failure occurred. </exception>
        /// <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
        /// the required range. </exception>
        /// <param name="row"> Specifies a one-based row number. </param>
        /// <param name="col"> Specifies a one-based column number. </param>
        /// <returns>
        /// Cell value or DB Null if column or row indexes exceed the number of rows or columns in the
        /// worksheet. This allows reading values that are generally thought to be defined in a worksheet
        /// but where not year defined in the particular worksheet and thus might be expected to be given
        /// a default value. The default values are derived in the relevant getters.  Otherwise, a DB
        /// Null is returned.
        /// </returns>
        public override object CellValueGetter(int row, int col)
        {
            return this._Worksheet is null
                ? throw new System.IO.IOException("Worksheet not activated")
                : row <= 0
                ? throw new ArgumentOutOfRangeException(nameof(row), "row number must be a positive")
                : col <= 0
                ? throw new ArgumentOutOfRangeException(nameof(col), "column number must be positive")
                : row <= this._Worksheet.Rows.Count && col <= this._Worksheet.Columns.Count ? Parser.Parse( this._Worksheet.Rows[row - 1][col - 1]) : DBNull.Value;
        }

        /// <summary> Returns the value of the given cell as a Nullable Boolean type. </summary>
        /// <remarks> Excel rows and columns are one-based. </remarks>
        /// <exception cref="System.IO.IOException"> Thrown when an IO failure occurred. </exception>
        /// <param name="row">          Specifies a one-based row number. </param>
        /// <param name="col">          Specifies a one-based column number. </param>
        /// <param name="defaultValue"> Specifies a default value. </param>
        /// <returns> True if it succeeds, false if it fails. </returns>
        public override bool? CellValueGetter(int row, int col, bool? defaultValue)
        {
            return this._Worksheet is null
                ? throw new System.IO.IOException("Worksheet not activated")
                : Parser.Parse( this.CellValueGetter(row, col), defaultValue);
        }

        /// <summary> Returns the value of the specified cell as a Boolean type. </summary>
        /// <remarks> Excel rows and columns are one-based. </remarks>
        /// <param name="row">          Specifies a one-based row number. </param>
        /// <param name="col">          Specifies a one-based column number. </param>
        /// <param name="defaultValue"> Specifies a default value. </param>
        /// <returns> True if it succeeds, false if it fails. </returns>
        public override bool CellValueGetter(int row, int col, bool defaultValue)
        {
            return this.CellValueGetter(row, col, new bool?(defaultValue)).Value;
        }

        /// <summary> Returns the value of the specified cell as a Nullable Date type. </summary>
        /// <remarks> Excel rows and columns are one-based. </remarks>
        /// <param name="row">          Specifies a one-based row number. </param>
        /// <param name="col">          Specifies a one-based column number. </param>
        /// <param name="defaultValue"> Specifies a default value. </param>
        /// <returns> A DateTime? </returns>
        public override DateTime? CellValueGetter(int row, int col, DateTime? defaultValue)
        {
            var value = this.CellValueGetter(row, col, new double?());
            return value.HasValue ? DateTime.FromOADate(value.Value) : defaultValue;
        }

        /// <summary>
        /// Returns the value of the specified cell as a <see cref="T:DateTime">DateTime</see> type.
        /// </summary>
        /// <remarks> Excel rows and columns are one-based. </remarks>
        /// <param name="row">          Specifies a one-based row number. </param>
        /// <param name="col">          Specifies a one-based column number. </param>
        /// <param name="defaultValue"> Specifies a default value. </param>
        /// <returns> A DateTime. </returns>
        public override DateTime CellValueGetter(int row, int col, DateTime defaultValue)
        {
            return this.CellValueGetter(row, col, new DateTime?(defaultValue)).Value;
        }

        /// <summary> Returns the value of the specified cell as a Nullable Double type. </summary>
        /// <remarks> Excel rows and columns are one-based. </remarks>
        /// <exception cref="System.IO.IOException"> Thrown when an IO failure occurred. </exception>
        /// <param name="row">          Specifies a one-based row number. </param>
        /// <param name="col">          Specifies a one-based column number. </param>
        /// <param name="defaultValue"> Specifies a default value. </param>
        /// <returns> A Double? </returns>
        public override double? CellValueGetter(int row, int col, double? defaultValue)
        {
            return this._Worksheet is null
                ? throw new System.IO.IOException("Worksheet not activated")
                : Parser.Parse( this.CellValueGetter(row, col), defaultValue);
        }

        /// <summary> Returns the value of the specified cell as a double type. </summary>
        /// <remarks> Excel rows and columns are one-based. </remarks>
        /// <param name="row">          Specifies a one-based row number. </param>
        /// <param name="col">          Specifies a one-based column number. </param>
        /// <param name="defaultValue"> Specifies a default value. </param>
        /// <returns> A Double. </returns>
        public override double CellValueGetter(int row, int col, double defaultValue)
        {
            return this.CellValueGetter(row, col, new double?(defaultValue)).Value;
        }

        /// <summary> Returns the value of the specified cell as a Nullable Integer type. </summary>
        /// <remarks> Excel rows and columns are one-based. </remarks>
        /// <param name="row">          Specifies a one-based row number. </param>
        /// <param name="col">          Specifies a one-based column number. </param>
        /// <param name="defaultValue"> Specifies a default value. </param>
        /// <returns> An Integer? </returns>
        public override int? CellValueGetter(int row, int col, int? defaultValue)
        {
            var value = this.CellValueGetter(row, col, new double?());
            return value.HasValue ? Convert.ToInt32(value.Value) : defaultValue;
        }

        /// <summary> Returns the value of the specified cell as a Integer type. </summary>
        /// <remarks> Excel rows and columns are one-based. </remarks>
        /// <param name="row">          Specifies a one-based row number. </param>
        /// <param name="col">          Specifies a one-based column number. </param>
        /// <param name="defaultValue"> Specifies a default value. </param>
        /// <returns> An Integer. </returns>
        public override int CellValueGetter(int row, int col, int defaultValue)
        {
            return this.CellValueGetter(row, col, new int?(defaultValue)).Value;
        }

        /// <summary> Returns a cell value or default value of String type. </summary>
        /// <remarks> Excel rows and columns are one-based. </remarks>
        /// <exception cref="System.IO.IOException"> Thrown when an IO failure occurred. </exception>
        /// <param name="row">          Specifies a one-based row number. </param>
        /// <param name="col">          Specifies a one-based column number. </param>
        /// <param name="defaultValue"> Specifies a default value. </param>
        /// <returns> A String. </returns>
        public override string CellValueGetter(int row, int col, string defaultValue)
        {
            return this._Worksheet is null
                ? throw new System.IO.IOException("Worksheet not activated")
                : Parser.Parse( this.CellValueGetter(row, col), defaultValue);
        }

        /// <summary> Returns <c>True</c> if the specified cell includes a DBNull value. </summary>
        /// <remarks> Excel rows and columns are one-based. </remarks>
        /// <exception cref="System.IO.IOException"> Thrown when an IO failure occurred. </exception>
        /// <param name="row"> Specifies a one-based row number. </param>
        /// <param name="col"> Specifies a one-based column number. </param>
        /// <returns> True if database null, false if not. </returns>
        public override bool IsDBNull(int row, int col)
        {
            return this._Worksheet is null
                ? throw new System.IO.IOException("Worksheet not activated")
                : Parser.IsDBNull( this.CellValueGetter(row, col));
        }

        /// <summary> Returns <c>True</c> if the specified cell is empty. </summary>
        /// <remarks> Excel rows and columns are one-based. </remarks>
        /// <exception cref="System.IO.IOException"> Thrown when an IO failure occurred. </exception>
        /// <param name="row"> Specifies a one-based row number. </param>
        /// <param name="col"> Specifies a one-based column number. </param>
        /// <returns> True if empty, false if not. </returns>
        public override bool IsEmpty(int row, int col)
        {
            return this._Worksheet is null
                ? throw new System.IO.IOException("Worksheet not activated")
                : Parser.IsEmpty( this.CellValueGetter(row, col));
        }

        /// <summary>
        /// Returns <c>True</c> if the specified cell is empty or includes a DBNull value.
        /// </summary>
        /// <remarks> Excel rows and columns are one-based. </remarks>
        /// <param name="row"> Specifies a one-based row number. </param>
        /// <param name="col"> Specifies a one-based column number. </param>
        /// <returns> True if the database null or is empty, false if not. </returns>
        public override bool IsDBNullOrEmpty(int row, int col)
        {
            return this.IsDBNull(row, col) || this.IsEmpty(row, col);
        }

        #endregion

    }
}
