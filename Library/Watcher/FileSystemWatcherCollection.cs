using System.Collections.Generic;
using System.Linq;

namespace isr.IO.FileWatcher
{

    /// <summary> The list of <see cref="FileSystemWatcher">file system watchers</see>. </summary>
    /// <remarks>
    /// (c) 2010 John Simmons.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2014-07-08. Created based on
    /// http://www.codeproject.com/Articles/58740/FileSystemWatcher-Pure-Chaos-Part-of. </para>
    /// </remarks>
    public class FileSystemWatcherCollection : List<FileSystemWatcher>
    {

        /// <summary> Enables the raising events. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        public void EnableRaisingEvents()
        {
            foreach (FileSystemWatcher Item in this)
                Item.EnableRaisingEvents = true;
        }

        /// <summary> Disables the raising events. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        public void DisableRaisingEvents()
        {
            foreach (FileSystemWatcher Item in this)
                Item.EnableRaisingEvents = false;
        }

        /// <summary>
        /// Starts all of the internal <see cref="FileSystemWatcher">file system watchers</see>
        /// by setting their EnableRaisingEvents property to true.
        /// </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        public void Start()
        {
            if (this.Any())
            {
                this[0].StartFolderMonitor();
            }

            this.EnableRaisingEvents();
        }

        /// <summary> Stops all of the internal <see cref="FileSystemWatcher">file system watchers</see> by
        /// setting their EnableRaisingEvents property to true. </summary>
        public void Stop()
        {
            if (this.Any())
            {
                this[0].StopFolderMonitor();
            }

            this.DisableRaisingEvents();
        }
    }
}
