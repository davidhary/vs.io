using System;
using System.IO;

namespace isr.IO.FileWatcher
{

    /// <summary>
    /// This class allows us to pass any type of watcher arguments to the calling object's handler
    /// via a single object instead of having to add a lot of event handlers for the various event
    /// arguments types.
    /// </summary>
    /// <remarks>
    /// (c) 2010 John Simmons.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2014-07-08. Created based on
    /// http://www.codeproject.com/Articles/58740/FileSystemWatcher-Pure-Chaos-Part-of. </para>
    /// </remarks>
    public class WatcherEventArgs : EventArgs
    {

        #region " Constructors "

        /// <summary> Constructor. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <param name="watcher">      The watcher. </param>
        /// <param name="arguments">    The arguments. </param>
        /// <param name="argumentType"> Type of the argument. </param>
        /// <param name="filter">       Specifies the filter. </param>
        public WatcherEventArgs(FileSystemWatcher watcher, object arguments, ArgumentType argumentType, NotifyFilters filter)
        {
            this.Watcher = watcher;
            this.Arguments = arguments;
            this.ArgumentType = argumentType;
            this.Filter = filter;
        }

        /// <summary> Constructor. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <param name="watcher">      The watcher. </param>
        /// <param name="arguments">    The arguments. </param>
        /// <param name="argumentType"> Type of the argument. </param>
        public WatcherEventArgs(FileSystemWatcher watcher, object arguments, ArgumentType argumentType)
        {
            this.Watcher = watcher;
            this.Arguments = arguments;
            this.ArgumentType = argumentType;
            this.Filter = NotifyFilters.Attributes;
        }

        #endregion ' Constructors

        /// <summary> Gets or sets the watcher. </summary>
        /// <value> The watcher. </value>
        public FileSystemWatcher Watcher { get; set; }

        /// <summary> Gets or sets the arguments. </summary>
        /// <value> The arguments. </value>
        public object Arguments { get; set; }

        /// <summary> Gets or sets the type of the argument. </summary>
        /// <value> The type of the argument. </value>
        public ArgumentType ArgumentType { get; set; }

        /// <summary> Gets or sets the filter. </summary>
        /// <value> The filter. </value>
        public NotifyFilters Filter { get; set; }
    }

        /// <summary>
        /// Enumerates argument types; used to point which argument is valid in the
        /// <see cref="WatcherEventArgs">watcher event argument object</see>.
        /// </summary>
        /// <remarks> David, 2020-10-08. </remarks>
    public enum ArgumentType
    {

        /// <summary> An Enum constant representing the file system option. </summary>
        FileSystem,

        /// <summary> An Enum constant representing the renamed option. </summary>
        Renamed,

        /// <summary> An Enum constant representing the error option. </summary>
        Error,

        /// <summary> An Enum constant representing the standard event option. </summary>
        StandardEvent,

        /// <summary> An Enum constant representing the folder availability option. </summary>
        FolderAvailability
    }
}
