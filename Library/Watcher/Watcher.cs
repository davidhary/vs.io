using System;
using System.Diagnostics;
using System.IO;
using isr.IO.ExceptionExtensions;

namespace isr.IO.FileWatcher
{

    /// <summary>
    /// This is the main class (and the one you'll use directly). Create an instance of the class
    /// (passing in a WatcherInfo object for initialization), and then attach event handlers to this
    /// object.  One or more watchers will be created to handle the various events and filters, and
    /// will marshal these events into a single set from which you can gather info.
    /// </summary>
    /// <remarks>
    /// (c) 2010 John Simmons.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2014-07-08. Created based on
    /// http://www.codeproject.com/Articles/58740/FileSystemWatcher-Pure-Chaos-Part-of. </para><para>
    /// David, 2015-08-28. Uses null-conditionals. </para>
    /// </remarks>
    public class Watcher : IDisposable
    {

        #region " Constructors "

        /// <summary> Constructor. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="info"> The information. </param>
        public Watcher(WatcherInfo info)
        {
            if (info is null)
                throw new ArgumentNullException(nameof(info));
            this._WatcherInfo = info;
            this.Initialize();
        }

        /// <summary> Calls <see cref="M:Dispose(Boolean Disposing)" /> to cleanup. </summary>
        /// <remarks>
        /// Do not make this method Overridable (virtual) because a derived class should not be able to
        /// override this method.
        /// </remarks>
        public void Dispose()
        {

            // this disposes all child classes.
            this.Dispose(true);

            // Take this object off the finalization(Queue) and prevent finalization code 
            // from executing a second time.
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// Gets or sets the dispose status sentinel of the base class.  This applies to the derived
        /// class provided proper implementation.
        /// </summary>
        /// <value> <c>True</c> if disposed; otherwise, <c>False</c>. </value>
        protected bool IsDisposed { get; set; }

        /// <summary>
        /// Releases the unmanaged resources used by the <see cref="T:System.Windows.Forms.Control" />
        /// and its child controls and optionally releases the managed resources.
        /// </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <param name="disposing"> <c>True</c> to release both managed and unmanaged resources;
        /// <c>False</c> to release only unmanaged
        /// resources when called from the runtime
        /// finalize. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification = "Exception is published")]
        [DebuggerNonUserCode()]
        protected virtual void Dispose(bool disposing)
        {
            try
            {
                if (!this.IsDisposed && disposing)
                {
                    this.DisposeWatchers();
                    Delegate d;
                    Exception ex;
                    foreach (var currentD in ChangedAttribute is null ? Array.Empty<Delegate>() : ChangedAttribute.GetInvocationList())
                    {
                        d = currentD;
                        try
                        {
                            ChangedAttribute -= (EventHandler<WatcherEventArgs>)d;
                        }
                        catch
                        {
                            Debug.Assert(!Debugger.IsAttached, ex.ToFullBlownString());
                        }
                    }

                    foreach (var currentD1 in ChangedCreationTime is null ? Array.Empty<Delegate>() : ChangedCreationTime.GetInvocationList())
                    {
                        d = currentD1;
                        try
                        {
                            ChangedCreationTime -= (EventHandler<WatcherEventArgs>)d;
                        }
                        catch
                        {
                            Debug.Assert(!Debugger.IsAttached, ex.ToFullBlownString());
                        }
                    }

                    foreach (var currentD2 in ChangedDirectoryName is null ? Array.Empty<Delegate>() : ChangedDirectoryName.GetInvocationList())
                    {
                        d = currentD2;
                        try
                        {
                            ChangedDirectoryName -= (EventHandler<WatcherEventArgs>)d;
                        }
                        catch
                        {
                            Debug.Assert(!Debugger.IsAttached, ex.ToFullBlownString());
                        }
                    }

                    foreach (var currentD3 in ChangedFileName is null ? Array.Empty<Delegate>() : ChangedFileName.GetInvocationList())
                    {
                        d = currentD3;
                        try
                        {
                            ChangedFileName -= (EventHandler<WatcherEventArgs>)d;
                        }
                        catch
                        {
                            Debug.Assert(!Debugger.IsAttached, ex.ToFullBlownString());
                        }
                    }

                    foreach (var currentD4 in ChangedLastAccess is null ? Array.Empty<Delegate>() : ChangedLastAccess.GetInvocationList())
                    {
                        d = currentD4;
                        try
                        {
                            ChangedLastAccess -= (EventHandler<WatcherEventArgs>)d;
                        }
                        catch
                        {
                            Debug.Assert(!Debugger.IsAttached, ex.ToFullBlownString());
                        }
                    }

                    foreach (var currentD5 in ChangedLastWrite is null ? Array.Empty<Delegate>() : ChangedLastWrite.GetInvocationList())
                    {
                        d = currentD5;
                        try
                        {
                            ChangedLastWrite -= (EventHandler<WatcherEventArgs>)d;
                        }
                        catch
                        {
                            Debug.Assert(!Debugger.IsAttached, ex.ToFullBlownString());
                        }
                    }

                    foreach (var currentD6 in ChangedSecurity is null ? Array.Empty<Delegate>() : ChangedSecurity.GetInvocationList())
                    {
                        d = currentD6;
                        try
                        {
                            ChangedSecurity -= (EventHandler<WatcherEventArgs>)d;
                        }
                        catch
                        {
                            Debug.Assert(!Debugger.IsAttached, ex.ToFullBlownString());
                        }
                    }

                    foreach (var currentD7 in ChangedSize is null ? Array.Empty<Delegate>() : ChangedSize.GetInvocationList())
                    {
                        d = currentD7;
                        try
                        {
                            ChangedSize -= (EventHandler<WatcherEventArgs>)d;
                        }
                        catch
                        {
                            Debug.Assert(!Debugger.IsAttached, ex.ToFullBlownString());
                        }
                    }

                    foreach (var currentD8 in Created is null ? Array.Empty<Delegate>() : Created.GetInvocationList())
                    {
                        d = currentD8;
                        try
                        {
                            Created -= (EventHandler<WatcherEventArgs>)d;
                        }
                        catch
                        {
                            Debug.Assert(!Debugger.IsAttached, ex.ToFullBlownString());
                        }
                    }

                    foreach (var currentD9 in Deleted is null ? Array.Empty<Delegate>() : Deleted.GetInvocationList())
                    {
                        d = currentD9;
                        try
                        {
                            Deleted -= (EventHandler<WatcherEventArgs>)d;
                        }
                        catch
                        {
                            Debug.Assert(!Debugger.IsAttached, ex.ToFullBlownString());
                        }
                    }

                    foreach (var currentD10 in Renamed is null ? Array.Empty<Delegate>() : Renamed.GetInvocationList())
                    {
                        d = currentD10;
                        try
                        {
                            Renamed -= (EventHandler<WatcherEventArgs>)d;
                        }
                        catch
                        {
                            Debug.Assert(!Debugger.IsAttached, ex.ToFullBlownString());
                        }
                    }

                    foreach (var currentD11 in ChangedAttribute is null ? Array.Empty<Delegate>() : ChangedAttribute.GetInvocationList())
                    {
                        d = currentD11;
                        try
                        {
                            ErrorOccurred -= (EventHandler<WatcherEventArgs>)d;
                        }
                        catch
                        {
                            Debug.Assert(!Debugger.IsAttached, ex.ToFullBlownString());
                        }
                    }

                    foreach (var currentD12 in Disposed is null ? Array.Empty<Delegate>() : Disposed.GetInvocationList())
                    {
                        d = currentD12;
                        try
                        {
                            Disposed -= (EventHandler<WatcherEventArgs>)d;
                        }
                        catch
                        {
                            Debug.Assert(!Debugger.IsAttached, ex.ToFullBlownString());
                        }
                    }

                    foreach (var currentD13 in ConnectionChanged is null ? Array.Empty<Delegate>() : ConnectionChanged.GetInvocationList())
                    {
                        d = currentD13;
                        try
                        {
                            ConnectionChanged -= (EventHandler<WatcherEventArgs>)d;
                        }
                        catch
                        {
                            Debug.Assert(!Debugger.IsAttached, ex.ToFullBlownString());
                        }
                    }
                }
            }
            finally
            {
                this.IsDisposed = true;
            }
        }

        /// <summary>
        /// Disposes of all of our watchers (called from Dispose, or as a result of loosing access to a
        /// folder)
        /// </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        public void DisposeWatchers()
        {
            // turn watches off
            if ( this._FileSystemWatchers is object)
            {
                this._FileSystemWatchers.DisableRaisingEvents();
                Debug.WriteLine("Watcher.DisposeWatchers()");
                for (int i = 0, loopTo = this._FileSystemWatchers.Count - 1; i <= loopTo; i++)
                {
                    if (i == 0)
                    {
                        this.RemoveMainWatcher( this._FileSystemWatchers[i]);
                    }
                    else
                    {
                        this.RemoveWatcher( this._FileSystemWatchers[i]);
                    }

                    this._FileSystemWatchers[i].Dispose();
                }

                this._FileSystemWatchers.Clear();
            }
        }

        #endregion

        #region " Event Definitions "

        /// <summary> Event queue for all listeners interested in EventChangedAttribute events. </summary>
        public event EventHandler<WatcherEventArgs> ChangedAttribute;

        /// <summary> Event queue for all listeners interested in EventChangedCreationTime events. </summary>
        public event EventHandler<WatcherEventArgs> ChangedCreationTime;

        /// <summary> Event queue for all listeners interested in EventChangedDirectoryName events. </summary>
        public event EventHandler<WatcherEventArgs> ChangedDirectoryName;

        /// <summary> Event queue for all listeners interested in EventChangedFileName events. </summary>
        public event EventHandler<WatcherEventArgs> ChangedFileName;

        /// <summary> Event queue for all listeners interested in EventChangedLastAccess events. </summary>
        public event EventHandler<WatcherEventArgs> ChangedLastAccess;

        /// <summary> Event queue for all listeners interested in EventChangedLastWrite events. </summary>
        public event EventHandler<WatcherEventArgs> ChangedLastWrite;

        /// <summary> Event queue for all listeners interested in EventChangedSecurity events. </summary>
        public event EventHandler<WatcherEventArgs> ChangedSecurity;

        /// <summary> Event queue for all listeners interested in EventChangedSize events. </summary>
        public event EventHandler<WatcherEventArgs> ChangedSize;

        /// <summary> Event queue for all listeners interested in EventCreated events. </summary>
        public event EventHandler<WatcherEventArgs> Created;

        /// <summary> Event queue for all listeners interested in EventDeleted events. </summary>
        public event EventHandler<WatcherEventArgs> Deleted;

        /// <summary> Event queue for all listeners interested in EventRenamed events. </summary>
        public event EventHandler<WatcherEventArgs> Renamed;

        /// <summary> Event queue for all listeners interested in EventError events. </summary>
        public event EventHandler<WatcherEventArgs> ErrorOccurred;

        /// <summary> Event queue for all listeners interested in EventDisposed events. </summary>
        public event EventHandler<WatcherEventArgs> Disposed;

        /// <summary> Event queue for all listeners interested in connection changed events. </summary>
        public event EventHandler<WatcherEventArgs> ConnectionChanged;

        #endregion

        #region " Helper Methods "

        /// <summary> Information describing the watcher. </summary>
        private readonly WatcherInfo _WatcherInfo;

        /// <summary> The list of <see cref="FileSystemWatcher">file system watchers</see>. </summary>
        private FileSystemWatcherCollection _FileSystemWatchers;

        /// <summary> <c>true</c> if the <see cref="System.IO.Path">path</see> exists. </summary>
        /// <value> The is connected. </value>
        public bool IsConnected { get; private set; } = true;

        /// <summary>
        /// Determines if the specified NotifyFilter item has been specified to be handled by this object.
        /// </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <param name="filter"> . </param>
        /// <returns> <c>true</c> if it succeeds; otherwise <c>false</c>. </returns>
        public bool HandleNotifyFilter(NotifyFilters filter)
        {
            return (this._WatcherInfo.ChangesFilters & filter) == filter;
        }

        /// <summary>
        /// Determines if the specified WatcherChangeType item has been specified to be handled by this
        /// object.
        /// </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <param name="filter"> . </param>
        /// <returns> <c>true</c> if it succeeds; otherwise <c>false</c>. </returns>
        public bool HandleWatchesFilter(WatcherChangeTypes filter)
        {
            return (this._WatcherInfo.WatchesFilters & filter) == filter;
        }

        /// <summary>
        /// Initializes this object by creating all of the required internal FileSystemWatcher objects
        /// necessary to monitor the folder/file for the desired changes.
        /// </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        private void Initialize()
        {
            Debug.WriteLine("Watcher.Initialize()");

            // the buffer can be from 4 to 64 k bytes.  Default is 8
            this._WatcherInfo.BufferSize = this._WatcherInfo.BufferSize;
            this._FileSystemWatchers = new FileSystemWatcherCollection();

            // create the main watcher (handles create/delete, rename, error, and dispose)
            this.AddWatcher();

            // create a change watcher for each NotifyFilter item
            this.AddWatcher(NotifyFilters.Attributes);
            this.AddWatcher(NotifyFilters.CreationTime);
            this.AddWatcher(NotifyFilters.DirectoryName);
            this.AddWatcher(NotifyFilters.FileName);
            this.AddWatcher(NotifyFilters.LastAccess);
            this.AddWatcher(NotifyFilters.LastWrite);
            this.AddWatcher(NotifyFilters.Security);
            this.AddWatcher(NotifyFilters.Size);
            Debug.WriteLine(string.Format("Watcher.Initialize() - {0} watchers created", this._FileSystemWatchers.Count));
        }

        /// <summary>
        /// Adds the necessary FileSystemWatcher objects, depending on which notify filters the user
        /// specified.
        /// </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <param name="filter"> . </param>
        private void AddWatcher(NotifyFilters filter)
        {
            Debug.WriteLine(string.Format("Watcher.AddWatcher({0})", filter.ToString()));

            // Each "Change" filter gets its own watcher so we can determine *what* 
            // actually changed. This will allow us to react only to the change events 
            // that we actually want.  The reason I do this is because some programs 
            // fire TWO events for  certain changes. For example, Notepad sends two 
            // events when a file is created. One for CreationTime, and one for 
            // Attributes.
            // if we're not handling the currently specified filter, get out
            if ( this.HandleNotifyFilter(filter))
            {
                var fileSystemWatcher = new FileSystemWatcher( this._WatcherInfo.WatchPath)
                {
                    IncludeSubdirectories = this._WatcherInfo.IncludeSubfolders,
                    Filter = this._WatcherInfo.FileFilter,
                    NotifyFilter = filter,
                    InternalBufferSize = this._WatcherInfo.BufferSize
                };
                switch (filter)
                {
                    case NotifyFilters.Attributes:
                        {
                            fileSystemWatcher.Changed += this.FileSystemWatcher_ChangedAttribute;
                            break;
                        }

                    case NotifyFilters.CreationTime:
                        {
                            fileSystemWatcher.Changed += this.FileSystemWatcher_ChangedCreationTime;
                            break;
                        }

                    case NotifyFilters.DirectoryName:
                        {
                            fileSystemWatcher.Changed += this.FileSystemWatcher_ChangedDirectoryName;
                            break;
                        }

                    case NotifyFilters.FileName:
                        {
                            fileSystemWatcher.Changed += this.FileSystemWatcher_ChangedFileName;
                            break;
                        }

                    case NotifyFilters.LastAccess:
                        {
                            fileSystemWatcher.Changed += this.FileSystemWatcher_ChangedLastAccess;
                            break;
                        }

                    case NotifyFilters.LastWrite:
                        {
                            fileSystemWatcher.Changed += this.FileSystemWatcher_ChangedLastWrite;
                            break;
                        }

                    case NotifyFilters.Security:
                        {
                            fileSystemWatcher.Changed += this.FileSystemWatcher_ChangedSecurity;
                            break;
                        }

                    case NotifyFilters.Size:
                        {
                            fileSystemWatcher.Changed += this.FileSystemWatcher_ChangedSize;
                            break;
                        }
                }

                this._FileSystemWatchers.Add(fileSystemWatcher);
            }
        }

        /// <summary>
        /// Removes the FileSystemWatcher event handlers depending on which notify filters the user
        /// specified.
        /// </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <param name="fileSystemWatcher"> The file system watcher. </param>
        private void RemoveWatcher(FileSystemWatcher fileSystemWatcher)
        {
            if (fileSystemWatcher is object && this.HandleNotifyFilter(fileSystemWatcher.NotifyFilter))
            {
                Debug.WriteLine(string.Format("Watcher.RemoveWatcher({0})", fileSystemWatcher.Name));
                switch (fileSystemWatcher.NotifyFilter)
                {
                    case NotifyFilters.Attributes:
                        {
                            fileSystemWatcher.Changed -= this.FileSystemWatcher_ChangedAttribute;
                            break;
                        }

                    case NotifyFilters.CreationTime:
                        {
                            fileSystemWatcher.Changed -= this.FileSystemWatcher_ChangedCreationTime;
                            break;
                        }

                    case NotifyFilters.DirectoryName:
                        {
                            fileSystemWatcher.Changed -= this.FileSystemWatcher_ChangedDirectoryName;
                            break;
                        }

                    case NotifyFilters.FileName:
                        {
                            fileSystemWatcher.Changed -= this.FileSystemWatcher_ChangedFileName;
                            break;
                        }

                    case NotifyFilters.LastAccess:
                        {
                            fileSystemWatcher.Changed -= this.FileSystemWatcher_ChangedLastAccess;
                            break;
                        }

                    case NotifyFilters.LastWrite:
                        {
                            fileSystemWatcher.Changed -= this.FileSystemWatcher_ChangedLastWrite;
                            break;
                        }

                    case NotifyFilters.Security:
                        {
                            fileSystemWatcher.Changed -= this.FileSystemWatcher_ChangedSecurity;
                            break;
                        }

                    case NotifyFilters.Size:
                        {
                            fileSystemWatcher.Changed -= this.FileSystemWatcher_ChangedSize;
                            break;
                        }
                }
            }
        }

        /// <summary> Adds the main watcher for the change types the user specified. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        private void AddWatcher()
        {
            Debug.WriteLine("Watcher.AddWatcher()");

            // All other FileSystemWatcher events are handled through a single "main" watcher.
            if ( this.HandleWatchesFilter(WatcherChangeTypes.Created) || this.HandleWatchesFilter(WatcherChangeTypes.Deleted) || this.HandleWatchesFilter(WatcherChangeTypes.Renamed) || this._WatcherInfo.WatchForError || this._WatcherInfo.WatchForDisposed)
            {
                var fileSystemWatcher = new FileSystemWatcher( this._WatcherInfo.WatchPath, this._WatcherInfo.MonitorPathInterval)
                {
                    IncludeSubdirectories = this._WatcherInfo.IncludeSubfolders,
                    Filter = this._WatcherInfo.FileFilter,
                    InternalBufferSize = this._WatcherInfo.BufferSize
                };
                if ( this.HandleWatchesFilter(WatcherChangeTypes.Created))
                {
                    fileSystemWatcher.Created += this.FileSystemWatcher_CreatedDeleted;
                }

                if ( this.HandleWatchesFilter(WatcherChangeTypes.Deleted))
                {
                    fileSystemWatcher.Deleted += this.FileSystemWatcher_CreatedDeleted;
                }

                if ( this.HandleWatchesFilter(WatcherChangeTypes.Renamed))
                {
                    fileSystemWatcher.Renamed += this.FileSystemWatcher_Renamed;
                }

                if ( this._WatcherInfo.MonitorPathInterval > TimeSpan.Zero)
                {
                    fileSystemWatcher.FolderAvailabilityChanged += this.FileSystemWatcher_FolderAvailabilityChanged;
                }

                if ( this._WatcherInfo.WatchForError)
                {
                    fileSystemWatcher.Error += this.FileSystemWatcher_Error;
                }

                if ( this._WatcherInfo.WatchForDisposed)
                {
                    fileSystemWatcher.Disposed += this.FileSystemWatcher_Disposed;
                }

                this._FileSystemWatchers.Add(fileSystemWatcher);
            }
        }

        /// <summary> Adds the watcher for the change types the user specified. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <param name="fileSystemWatcher"> The file system watcher. </param>
        private void RemoveMainWatcher(FileSystemWatcher fileSystemWatcher)
        {
            if (fileSystemWatcher is object)
            {
                Debug.WriteLine(string.Format("Watcher.RemoveMainWatcher({0})", fileSystemWatcher.Name));
                if ( this.HandleWatchesFilter(WatcherChangeTypes.Created))
                {
                    fileSystemWatcher.Created -= this.FileSystemWatcher_CreatedDeleted;
                }

                if ( this.HandleWatchesFilter(WatcherChangeTypes.Deleted))
                {
                    fileSystemWatcher.Deleted -= this.FileSystemWatcher_CreatedDeleted;
                }

                if ( this.HandleWatchesFilter(WatcherChangeTypes.Renamed))
                {
                    fileSystemWatcher.Renamed -= this.FileSystemWatcher_Renamed;
                }

                if ( this._WatcherInfo.MonitorPathInterval > TimeSpan.Zero)
                {
                    fileSystemWatcher.FolderAvailabilityChanged -= this.FileSystemWatcher_FolderAvailabilityChanged;
                }

                if ( this._WatcherInfo.WatchForError)
                {
                    fileSystemWatcher.Error -= this.FileSystemWatcher_Error;
                }

                if ( this._WatcherInfo.WatchForDisposed)
                {
                    fileSystemWatcher.Disposed -= this.FileSystemWatcher_Disposed;
                }
            }
        }

        /// <summary>
        /// Starts all of the internal FileSystemWatcher objects by setting their EnableRaisingEvents
        /// property to true.
        /// </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        public void Start()
        {
            Debug.WriteLine("Watcher.Start()");
            if ( this._FileSystemWatchers is object)
            {
                this._FileSystemWatchers.Start();
            }
        }

        /// <summary> Stops all of the internal FileSystemWatcher objects by setting their
        /// EnableRaisingEvents property to true. </summary>
        public void Stop()
        {
            Debug.WriteLine("Watcher.Stop()");
            if ( this._FileSystemWatchers is object)
            {
                this._FileSystemWatchers.Stop();
            }
        }

        #endregion ' Helper Methods

        #region " Native Watcher Events "

        /// <summary>
        /// Fired when the watcher responsible for monitoring attribute changes is triggered.
        /// </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      File system event information. </param>
        private void FileSystemWatcher_ChangedAttribute(object sender, FileSystemEventArgs e)
        {
            FileSystemWatcher fileSystemWatcher = (FileSystemWatcher)sender;
            if (fileSystemWatcher is object && e is object)
            {
                Debug.WriteLine("EVENT - Changed Attribute");
                var evt = ChangedAttribute;
                evt?.Invoke(this, new WatcherEventArgs(fileSystemWatcher, e, ArgumentType.FileSystem, NotifyFilters.Attributes));
            }
        }

        /// <summary>
        /// Fired when the watcher responsible for monitoring creation time changes is triggered.
        /// </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      File system event information. </param>
        private void FileSystemWatcher_ChangedCreationTime(object sender, FileSystemEventArgs e)
        {
            FileSystemWatcher fileSystemWatcher = (FileSystemWatcher)sender;
            if (fileSystemWatcher is object && e is object)
            {
                Debug.WriteLine("EVENT - Changed CreationTime");
                var evt = ChangedCreationTime;
                evt?.Invoke(this, new WatcherEventArgs(fileSystemWatcher, e, ArgumentType.FileSystem, NotifyFilters.CreationTime));
            }
        }

        /// <summary>
        /// Fired when the watcher responsible for monitoring directory name changes is triggered.
        /// </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      File system event information. </param>
        private void FileSystemWatcher_ChangedDirectoryName(object sender, FileSystemEventArgs e)
        {
            FileSystemWatcher fileSystemWatcher = (FileSystemWatcher)sender;
            if (fileSystemWatcher is object && e is object)
            {
                Debug.WriteLine("EVENT - Changed DirectoryName");
                var evt = ChangedDirectoryName;
                evt?.Invoke(this, new WatcherEventArgs(fileSystemWatcher, e, ArgumentType.FileSystem, NotifyFilters.DirectoryName));
            }
        }

        /// <summary>
        /// Fired when the watcher responsible for monitoring file name changes is triggered.
        /// </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      File system event information. </param>
        private void FileSystemWatcher_ChangedFileName(object sender, FileSystemEventArgs e)
        {
            FileSystemWatcher fileSystemWatcher = (FileSystemWatcher)sender;
            if (fileSystemWatcher is object && e is object)
            {
                Debug.WriteLine("EVENT - Changed FileName");
                var evt = ChangedFileName;
                evt?.Invoke(this, new WatcherEventArgs(fileSystemWatcher, e, ArgumentType.FileSystem, NotifyFilters.FileName));
            }
        }

        /// <summary>
        /// Fired when the watcher responsible for monitoring last access date/time changes is triggered.
        /// </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      File system event information. </param>
        private void FileSystemWatcher_ChangedLastAccess(object sender, FileSystemEventArgs e)
        {
            FileSystemWatcher fileSystemWatcher = (FileSystemWatcher)sender;
            if (fileSystemWatcher is object && e is object)
            {
                Debug.WriteLine("EVENT - Changed LastAccess");
                var evt = ChangedLastAccess;
                evt?.Invoke(this, new WatcherEventArgs(fileSystemWatcher, e, ArgumentType.FileSystem, NotifyFilters.LastAccess));
            }
        }

        /// <summary>
        /// Fired when the watcher responsible for monitoring last write date/time changes is triggered.
        /// </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      File system event information. </param>
        private void FileSystemWatcher_ChangedLastWrite(object sender, FileSystemEventArgs e)
        {
            FileSystemWatcher fileSystemWatcher = (FileSystemWatcher)sender;
            if (fileSystemWatcher is object && e is object)
            {
                Debug.WriteLine("EVENT - Changed LastWrite");
                var evt = ChangedLastWrite;
                evt?.Invoke(this, new WatcherEventArgs(fileSystemWatcher, e, ArgumentType.FileSystem, NotifyFilters.LastWrite));
            }
        }

        /// <summary>
        /// Fired when the watcher responsible for monitoring security changes is triggered.
        /// </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      File system event information. </param>
        private void FileSystemWatcher_ChangedSecurity(object sender, FileSystemEventArgs e)
        {
            FileSystemWatcher fileSystemWatcher = (FileSystemWatcher)sender;
            if (fileSystemWatcher is object && e is object)
            {
                Debug.WriteLine("EVENT - Changed Security");
                var evt = ChangedSecurity;
                evt?.Invoke(this, new WatcherEventArgs(fileSystemWatcher, e, ArgumentType.FileSystem, NotifyFilters.Security));
            }
        }

        /// <summary>
        /// Fired when the watcher responsible for monitoring size changes is triggered.
        /// </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      File system event information. </param>
        private void FileSystemWatcher_ChangedSize(object sender, FileSystemEventArgs e)
        {
            FileSystemWatcher fileSystemWatcher = (FileSystemWatcher)sender;
            if (fileSystemWatcher is object && e is object)
            {
                Debug.WriteLine("EVENT - Changed Size");
                var evt = ChangedSize;
                evt?.Invoke(this, new WatcherEventArgs(fileSystemWatcher, e, ArgumentType.FileSystem, NotifyFilters.Size));
            }
        }

        /// <summary> Fired when an internal watcher is disposed. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        private void FileSystemWatcher_Disposed(object sender, EventArgs e)
        {
            FileSystemWatcher fileSystemWatcher = (FileSystemWatcher)sender;
            if (fileSystemWatcher is object && e is object)
            {
                Debug.WriteLine("EVENT - Disposed");
                var evt = Disposed;
                evt?.Invoke(this, new WatcherEventArgs(fileSystemWatcher, e, ArgumentType.StandardEvent));
            }
        }

        /// <summary>
        /// Fired when the main watcher detects an error (the watcher that detected the error is part of
        /// the event's arguments object)
        /// </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Error event information. </param>
        private void FileSystemWatcher_Error(object sender, ErrorEventArgs e)
        {
            FileSystemWatcher fileSystemWatcher = (FileSystemWatcher)sender;
            if (fileSystemWatcher is object && e is object)
            {
                Debug.WriteLine("EVENT - Error");
                var evt = ErrorOccurred;
                evt?.Invoke(this, new WatcherEventArgs(fileSystemWatcher, e, ArgumentType.Error));
            }
        }

        /// <summary> Fired when the main watcher detects a file rename. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Renamed event information. </param>
        private void FileSystemWatcher_Renamed(object sender, RenamedEventArgs e)
        {
            FileSystemWatcher fileSystemWatcher = (FileSystemWatcher)sender;
            if (fileSystemWatcher is object && e is object)
            {
                Debug.WriteLine("EVENT - Renamed");
                var evt = Renamed;
                evt?.Invoke(this, new WatcherEventArgs(fileSystemWatcher, e, ArgumentType.Renamed));
            }
        }

        /// <summary> Event handler. Called by watcher for created deleted events. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      File system event information. </param>
        private void FileSystemWatcher_CreatedDeleted(object sender, FileSystemEventArgs e)
        {
            FileSystemWatcher fileSystemWatcher = (FileSystemWatcher)sender;
            if (fileSystemWatcher is object && e is object)
            {
                switch (e.ChangeType)
                {
                    case WatcherChangeTypes.Created:
                        {
                            Debug.WriteLine("EVENT - Created");
                            var evt = Created;
                            evt?.Invoke(this, new WatcherEventArgs(fileSystemWatcher, e, ArgumentType.FileSystem));
                            break;
                        }

                    case WatcherChangeTypes.Deleted:
                        {
                            Debug.WriteLine("EVENT - Changed Deleted");
                            var evt = Deleted;
                            evt?.Invoke(this, new WatcherEventArgs(fileSystemWatcher, e, ArgumentType.FileSystem));
                            break;
                        }
                }
            }
        }

        /// <summary> Event handler. Called by watcher for event folder availability events. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Folder availability event information. </param>
        private void FileSystemWatcher_FolderAvailabilityChanged(object sender, EventArgs e)
        {
            FileSystemWatcher fileSystemWatcher = (FileSystemWatcher)sender;
            if (fileSystemWatcher is object && e is object)
            {
                Debug.WriteLine("EVENT - Folder Availability Changed");
                this.IsConnected = fileSystemWatcher.IsFolderExists;
                var evt = ConnectionChanged;
                evt?.Invoke(this, new WatcherEventArgs(fileSystemWatcher, e, ArgumentType.FolderAvailability));
                this.DisposeWatchers();
                if ( this.IsConnected )
                {
                    this.Initialize();
                }
            }
        }

        #endregion ' Native Watcher Events

    }
}
