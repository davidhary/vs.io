using System;

namespace isr.IO.FileWatcher
{

    /// <summary>
    /// Contains settings that initializes the file system watchers created within the Watcher class.
    /// </summary>
    /// <remarks>
    /// (c) 2010 John Simmons.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2014-07-08, . based on
    /// http://www.codeproject.com/Articles/58740/FileSystemWatcher-Pure-Chaos-Part-of. </para>
    /// </remarks>
    public class WatcherInfo
    {

        /// <summary> Default constructor. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        public WatcherInfo()
        {
            this.WatchPath = string.Empty;
            this.IncludeSubfolders = false;
            this.WatchForError = false;
            this.WatchForDisposed = false;
            this.ChangesFilters = System.IO.NotifyFilters.Attributes;
            this.WatchesFilters = System.IO.WatcherChangeTypes.All;
            this.FileFilter = string.Empty;
            this.BufferSize = 8 * 1024;
            this.MonitorPathInterval = TimeSpan.Zero;
        }

        /// <summary> Gets or sets the full pathname of the watch file. </summary>
        /// <value> The full pathname of the watch file. </value>
        public string WatchPath { get; set; }

        /// <summary> Gets or sets the include sub folders. </summary>
        /// <value> The include sub folders. </value>
        public bool IncludeSubfolders { get; set; }

        /// <summary> Gets or sets the watch for error. </summary>
        /// <value> The watch for error. </value>
        public bool WatchForError { get; set; }

        /// <summary> Gets or sets the watch for disposed. </summary>
        /// <value> The watch for disposed. </value>
        public bool WatchForDisposed { get; set; }

        /// <summary> Gets or sets the changes filters. </summary>
        /// <value> The changes filters. </value>
        public System.IO.NotifyFilters ChangesFilters { get; set; }

        /// <summary> Gets or sets the watches filters. </summary>
        /// <value> The watches filters. </value>
        public System.IO.WatcherChangeTypes WatchesFilters { get; set; }

        /// <summary> Gets or sets the file filter. </summary>
        /// <value> The file filter. </value>
        public string FileFilter { get; set; }

        /// <summary> Maximum Size of the buffer in KBytes. </summary>
        private const int _MaximumBufferSize = 64;

        /// <summary> Minimum Size of the buffer in KBytes. </summary>
        private const int _MinimumBufferSize = 4;

        /// <summary> The buffer k in bytes. </summary>
        private int _BufferSize;

        /// <summary> Gets or sets the buffer size. </summary>
        /// <value> The buffer k bytes; defaults to 8 KBytes. </value>
        public int BufferSize
        {
            get => this._BufferSize;

            set {
                int kBytes = ( int ) Math.Ceiling( value / 1024d );
                this._BufferSize = 1024 * Math.Max( Math.Min( kBytes, _MaximumBufferSize ), _MinimumBufferSize );
            }
        }

        /// <summary> Gets or sets the monitor path interval. </summary>
        /// <value> The monitor path interval. </value>
        public TimeSpan MonitorPathInterval { get; set; }
    }
}
