using System;
using System.Diagnostics;
using System.Threading;
using isr.IO.ExceptionExtensions;

namespace isr.IO.FileWatcher
{

    /// <summary> Extended File system watcher. </summary>
    /// <remarks>
    /// (c) 2010 John Simmons.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2014-07-08, . based on
    /// http://www.codeproject.com/Articles/58740/FileSystemWatcher-Pure-Chaos-Part-of. </para>
    /// </remarks>
    public class FileSystemWatcher : System.IO.FileSystemWatcher
    {

        #region " Constructors "

        /// <summary> Default constructor. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        public FileSystemWatcher() : this(DefaultInterval, DefaultName)
        {
        }

        /// <summary> Constructor. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <param name="path"> Full pathname of the file. </param>
        public FileSystemWatcher(string path) : this(path, DefaultInterval, DefaultName)
        {
        }

        /// <summary> Constructor. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <param name="interval"> The folder monitoring interval. </param>
        public FileSystemWatcher(TimeSpan interval) : this(interval, DefaultName)
        {
        }

        /// <summary> Constructor. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <param name="path">     Full pathname of the file. </param>
        /// <param name="interval"> The folder monitoring interval. </param>
        public FileSystemWatcher(string path, TimeSpan interval) : this(path, interval, DefaultName)
        {
        }

        /// <summary> Constructor. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <param name="interval"> The folder monitoring interval. </param>
        /// <param name="name">     The name. </param>
        public FileSystemWatcher(TimeSpan interval, string name) : base()
        {
            this._Interval = interval;
            this.Name = name;
            this.CreateThread();
        }

        /// <summary> Constructor. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <param name="path">     Full pathname of the file. </param>
        /// <param name="interval"> The folder monitoring interval. </param>
        /// <param name="name">     The name. </param>
        public FileSystemWatcher(string path, TimeSpan interval, string name) : base(path)
        {
            this._Interval = interval;
            this.Name = name;
            this.CreateThread();
        }

        /// <summary>
        /// Gets or sets the dispose status sentinel of the base class.  This applies to the derived
        /// class provided proper implementation.
        /// </summary>
        /// <value> <c>True</c> if disposed; otherwise, <c>False</c>. </value>
        protected bool IsDisposed { get; set; }

        /// <summary>
        /// Releases the unmanaged resources used by the
        /// <see cref="T:System.IO.FileSystemWatcher" /> and optionally releases the managed resources.
        /// </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <param name="disposing"> true to release both managed and unmanaged resources; false to
        /// release only unmanaged resources. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification = "Exception is published")]
        protected override void Dispose(bool disposing)
        {
            try
            {
                if (!this.IsDisposed && disposing)
                {
                    this.RemoveFolderAvailabilityChangedEvent(FolderAvailabilityChanged);
                }
            }
            finally
            {
                // set the sentinel indicating that the class was disposed.
                this.IsDisposed = true;
                base.Dispose(disposing);
            }
        }

        #endregion

        #region " MONITOR FOLDER AVAILABILITY "

        /// <summary> Start the monitoring thread. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        public void StartFolderMonitor()
        {
            if (!this.IsMonitoring )
            {
                _ = Interlocked.Increment( ref this._MonitoringSentinel );
                if ( this._Thread is object)
                {
                    this._Thread.Start();
                }
            }
        }

        /// <summary> Attempts to stop the monitoring thread. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        public void StopFolderMonitor()
        {
            if ( this.IsMonitoring )
            {
                _ = Interlocked.Decrement( ref this._MonitoringSentinel );
                if (!this._Thread.Join(3000))
                {
                    try
                    {
                        this._Thread.Abort();
                    }
                    catch
                    {
                        throw;
                    }
                }

                this._Thread = null;
            }
        }

        /// <summary> The name. </summary>
        /// <value> The name. </value>
        public string Name { get; set; }

        /// <summary> The default name. </summary>
        /// <value> The default name. </value>
        public static string DefaultName { get; private set; } = "FileSystemWatcher";

        /// <summary> The default folder check interval time span. </summary>
        /// <value> The default interval. </value>
        public static TimeSpan DefaultInterval { get; private set; } = TimeSpan.FromMilliseconds(100d);

        /// <summary> The minimum folder check interval time span. </summary>
        /// <value> The minimum interval. </value>
        public static TimeSpan MinInterval { get; private set; } = TimeSpan.FromMilliseconds(10d);

        /// <summary> The maximum folder check interval time span. </summary>
        /// <value> The maximum interval. </value>
        public static TimeSpan MaxInterval { get; private set; } = TimeSpan.FromDays(1d) - TimeSpan.FromMilliseconds(1d);

        /// <summary> True if is folder exists, false if not. </summary>
        private bool _IsFolderExists = true;

        /// <summary> <c>true</c> if the <see cref="System.IO.Path">path</see> exists. </summary>
        /// <value> The is folder exists. </value>
        public bool IsFolderExists
        {
            get => this._IsFolderExists;

            set {
                if ( !value.Equals( this.IsFolderExists ) )
                {
                    this._IsFolderExists = value;
                    var evt = FolderAvailabilityChanged;
                    evt?.Invoke( this, new EventArgs() );
                }
            }
        }

        /// <summary> The interval. </summary>
        private TimeSpan _Interval;

        /// <summary> Gets the monitoring interval. </summary>
        /// <value> The interval. </value>
        public TimeSpan Interval => this._Interval;


        /// <summary> The thread. </summary>
        private Thread _Thread;

        /// <summary> Creates the thread if the interval is greater than 0 milliseconds. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        private void CreateThread()
        {

            // Normalize  the interval
            this._Interval = TimeSpan.FromMilliseconds(Math.Max(MinInterval.TotalMilliseconds, Math.Min( this._Interval.TotalMilliseconds, MaxInterval.TotalMilliseconds)));
            // If the interval is 0, this indicates we don't want to monitor the path 
            // for availability.
            if ( this._Interval > MinInterval)
            {
                this._Thread = new Thread(new ThreadStart( this.MonitorFolderAvailability ))
                {
                    Name = Name,
                    IsBackground = true
                };
            }
        }

        /// <summary> The monitoring sentinel. </summary>
        private long _MonitoringSentinel;

        /// <summary> Reads the monitoring sentinel to determine if monitoring is active. </summary>
        /// <value> The is running. </value>
        public bool IsMonitoring => Interlocked.Read( ref this._MonitoringSentinel ) > 0L;

        /// <summary> The thread method. It sits and spins making sure the folder exists. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        public void MonitorFolderAvailability()
        {
            while ( this.IsMonitoring )
            {
                this.IsFolderExists = System.IO.Directory.Exists( this.Path );
                Thread.Sleep( this.Interval );
            }
        }

        /// <summary> Event queue for all listeners interested in FolderAvailabilityChanged events. </summary>
        public event EventHandler<EventArgs> FolderAvailabilityChanged;

        /// <summary> Removes folder availability changed event. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <param name="value"> The value. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification = "Exception is published")]
        private void RemoveFolderAvailabilityChangedEvent(EventHandler<EventArgs> value)
        {
            foreach (Delegate d in value is null ? Array.Empty<Delegate>() : value.GetInvocationList())
            {
                try
                {
                    FolderAvailabilityChanged -= (EventHandler<EventArgs>)d;
                }
                catch (Exception ex)
                {
                    Debug.Assert(!Debugger.IsAttached, ex.ToFullBlownString());
                }
            }
        }

        #endregion

    }
}
