/// <summary> Parses object values. </summary>
    /// <remarks>
    /// (c) 2010 Integrated Scientific Resources, Inc. All rights reserved. <para>
    /// Licensed under The MIT License. </para><para>
    /// David, 2009-10-01, 3.0.3296.x. </para>
    /// </remarks>
using System;
using Microsoft.VisualBasic.CompilerServices;

namespace isr.IO
{
    public sealed class Parser
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Prevent constructing this class. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        private Parser() : base()
        {
        }

        #endregion

        #region " NULL OR EMPTY "

        /// <summary> Gets or sets the DB Null string as stored in Excel. </summary>
        /// <value> The database null string. </value>
        public static string DBNullString { get; private set; } = "#NULL#";

        /// <summary> Returns <c>True</c> if the value is a DB Null. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <param name="value"> Specifies the parsed value. </param>
        /// <returns> <c>true</c> if database null; otherwise <c>false</c>. </returns>
        public static bool IsDBNull(object value)
        {
            return value is object && value is DBNull;
        }

        /// <summary> Returns <c>True</c> if the value is empty. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <param name="value"> Specifies the parsed value. </param>
        /// <returns> <c>true</c> if empty; otherwise <c>false</c>. </returns>
        public static bool IsEmpty(object value)
        {
            return value is null || value is string && string.IsNullOrWhiteSpace(value.ToString());
        }

        /// <summary>
        /// Returns <c>True</c> if the specified cell is empty or includes a DB Null value.
        /// </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <param name="value"> Specifies the parsed value. </param>
        /// <returns> <c>true</c> if a database null or is empty; otherwise <c>false</c>. </returns>
        public static bool IsDBNullOrEmpty(object value)
        {
            return IsDBNull(value) || IsEmpty(value);
        }

        #endregion

        #region " OBJECT PARSERS "

        /// <summary> Returns a value or DB Null. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <param name="value"> Specifies the parsed value. </param>
        /// <returns> An Object. </returns>
        public static object Parse(object value)
        {
            if (value is object && value is string && Convert.ToString(value, System.Globalization.CultureInfo.CurrentCulture).Equals(DBNullString))
            {
                value = DBNull.Value;
            }

            return value;
        }

        /// <summary> Returns the value of the given cell as a Nullable Boolean type. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <param name="value">        Specifies the parsed value. </param>
        /// <param name="defaultValue"> Specifies a default value. </param>
        /// <returns> A Boolean? </returns>
        public static bool? Parse(object value, bool? defaultValue)
        {
            if (value is bool)
            {
                return Conversions.ToBoolean(value);
            }
            else if (value is string)
            {
                bool output;
                return bool.TryParse(Convert.ToString(value, System.Globalization.CultureInfo.CurrentCulture), out output) ? output : defaultValue;
            }
            else
            {
                return defaultValue;
            }
        }

        /// <summary> Returns the value of the specified cell as a Boolean type. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <param name="value">        Specifies the parsed value. </param>
        /// <param name="defaultValue"> Specifies a default value. </param>
        /// <returns> <c>true</c> if it succeeds; otherwise <c>false</c>. </returns>
        public static bool Parse(object value, bool defaultValue)
        {
            return Parse(value, new bool?(defaultValue)).Value;
        }

        /// <summary> Returns the value of the specified cell as a Nullable Date type. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <param name="value">        Specifies the parsed value. </param>
        /// <param name="defaultValue"> Specifies a default value. </param>
        /// <returns> A DateTime? </returns>
        public static DateTime? Parse(object value, DateTime? defaultValue)
        {
            var output = Parse(value, new double?());
            return output.HasValue ? DateTime.FromOADate(output.Value) : defaultValue;
        }

        /// <summary>
        /// Returns the value of the specified cell as a <see cref="T:DataType">DateTime</see>
        /// type.
        /// </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <param name="value">        Specifies the parsed value. </param>
        /// <param name="defaultValue"> Specifies a default value. </param>
        /// <returns> A DateTime. </returns>
        public static DateTime Parse(object value, DateTime defaultValue)
        {
            return Parse(value, new DateTime?(defaultValue)).Value;
        }

        /// <summary> Returns the value of the specified cell as a Nullable Double type. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <param name="value">        Specifies the parsed value. </param>
        /// <param name="defaultValue"> Specifies a default value. </param>
        /// <returns> A Double? </returns>
        public static double? Parse(object value, double? defaultValue)
        {
            if (value is null)
            {
                return defaultValue;
            }
            else if (value is double)
            {
                double numericValue;
                numericValue = Conversions.ToDouble(value);
                return double.IsNaN(numericValue) ? defaultValue : numericValue;
            }
            else if (value is string)
            {
                string textValue = Convert.ToString(value, System.Globalization.CultureInfo.CurrentCulture).Trim();
                if (textValue.StartsWith("&", StringComparison.OrdinalIgnoreCase))
                {
                    long numericValue;
                    textValue = textValue.TrimStart("&hH".ToCharArray());
                    return long.TryParse(textValue, System.Globalization.NumberStyles.AllowHexSpecifier, System.Globalization.CultureInfo.CurrentCulture, out numericValue) ? numericValue : defaultValue;
                }
                else if (textValue.StartsWith("0x", StringComparison.OrdinalIgnoreCase))
                {
                    long numericValue;
                    textValue = textValue.Substring(2);
                    return long.TryParse(textValue, System.Globalization.NumberStyles.AllowHexSpecifier, System.Globalization.CultureInfo.CurrentCulture, out numericValue) ? numericValue : defaultValue;
                }
                else
                {
                    double numericValue;
                    return double.TryParse(textValue, System.Globalization.NumberStyles.Number | System.Globalization.NumberStyles.AllowExponent, System.Globalization.CultureInfo.CurrentCulture, out numericValue) ? numericValue : defaultValue;
                }
            }
            else
            {
                return defaultValue;
            }
        }

        /// <summary> Returns the value of the specified cell as a double type. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <param name="value">        Specifies the parsed value. </param>
        /// <param name="defaultValue"> Specifies a default value. </param>
        /// <returns> A Double. </returns>
        public static double Parse(object value, double defaultValue)
        {
            return Parse(value, new double?(defaultValue)).Value;
        }

        /// <summary> Returns the value of the specified cell as a Nullable Integer type. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <param name="value">        Specifies the parsed value. </param>
        /// <param name="defaultValue"> Specifies a default value. </param>
        /// <returns> An Integer? </returns>
        public static int? Parse(object value, int? defaultValue)
        {
            var output = Parse(value, new double?());
            return output.HasValue ? Convert.ToInt32(output.Value) : defaultValue;
        }

        /// <summary> Returns the value of the specified cell as a Integer type. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <param name="value">        Specifies the parsed value. </param>
        /// <param name="defaultValue"> Specifies a default value. </param>
        /// <returns> An Integer. </returns>
        public static int Parse(object value, int defaultValue)
        {
            return Parse(value, new int?(defaultValue)).Value;
        }

        /// <summary> Returns a cell value or default value of String type. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <param name="value">        Specifies the parsed value. </param>
        /// <param name="defaultValue"> Specifies a default value. </param>
        /// <returns> A String. </returns>
        public static string Parse(object value, string defaultValue)
        {
            return IsDBNullOrEmpty(value) ? defaultValue : Convert.ToString(value, System.Globalization.CultureInfo.CurrentCulture);
        }

        #endregion

    }
}
