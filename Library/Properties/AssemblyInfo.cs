﻿using System;
using System.Reflection;

[assembly: AssemblyTitle(isr.IO.My.MyLibrary.AssemblyTitle)]
[assembly: AssemblyDescription(isr.IO.My.MyLibrary.AssemblyDescription)]
[assembly: AssemblyProduct(isr.IO.My.MyLibrary.AssemblyProduct)]
[assembly: CLSCompliant(true)]
[assembly: System.Runtime.InteropServices.ComVisible(false)]
