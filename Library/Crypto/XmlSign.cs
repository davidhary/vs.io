using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Xml;
using isr.Core.HashExtensions;
using isr.IO.ExceptionExtensions;

namespace isr.IO.Cryptography
{

    /// <summary> Creates and verifies XML signatures. </summary>
    /// <remarks>
    /// Use this class to sign data using XML signature algorithms. (c) 2003 Integrated Scientific
    /// Resources, Inc. All rights reserved. <para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2003-06-17, 1.0.1263.x. </para>
    /// </remarks>
    public class XmlSign : IDisposable
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Constructs this class. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        public XmlSign() : base()
        {
        }

        /// <summary> Calls <see cref="M:Dispose(Boolean Disposing)"/> to cleanup. </summary>
        /// <remarks>
        /// Do not make this method Overridable (virtual) because a derived class should not be able to
        /// override this method.
        /// </remarks>
        public void Dispose()
        {

            // Do not change this code.  Put cleanup code in Dispose(Boolean) below.

            // this disposes all child classes.
            this.Dispose(true);

            // Take this object off the finalization(Queue) and prevent finalization code 
            // from executing a second time.
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// Gets the dispose status sentinel of the base class.  This applies to the derived class
        /// provided proper implementation.
        /// </summary>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <exception cref="System.IO.FileNotFoundException"> Thrown when the requested file is not present. </exception>
        /// <value> The is disposed. </value>
        protected bool IsDisposed { get; set; }

        /// <summary>
        /// Releases the unmanaged resources used by the <see cref="T:System.Windows.Forms.Control" />
        /// and its child controls and optionally releases the managed resources.
        /// </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <param name="disposing"> <c>True</c> to release both managed and unmanaged resources;
        /// <c>False</c> to release only unmanaged
        /// resources when called from the runtime
        /// finalize. </param>
        [DebuggerNonUserCode()]
        protected virtual void Dispose(bool disposing)
        {
            try
            {
                if (!this.IsDisposed && disposing)
                {
                    // Free managed resources when explicitly called
                    if ( this.RsaSigningKey is object)
                    {
                        this.RsaSigningKey.Dispose();
                        this.RsaSigningKey = null;
                    }

                    this.SignedXml = null;
                }
            }
            finally
            {
                this.IsDisposed = true;
            }
        }

        /// <summary>
        /// This destructor will run only if the Dispose method does not get called. It gives the base
        /// class the opportunity to finalize. Do not provide destructors in types derived from this
        /// class.
        /// </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        ~XmlSign()
        {
            // Do not re-create Dispose clean-up code here.
            // Calling Dispose(false) is optimal for readability and maintainability.
            this.Dispose(false);
        }

        #endregion

        #region " SHARED: BUILD SIGNATURE FILE "

        /// <summary> Adds a resource to the signature file. </summary>
        /// <remarks>
        /// Use this method to add resource specified by its data and id to the signed data set.
        /// </remarks>
        /// <exception cref="ArgumentNullException" guarantee="strong"> . </exception>
        /// <param name="signedXml">    is the <see cref="System.Security.Cryptography.Xml.SignedXml">
        /// signed XML</see> containing the resource and signature. </param>
        /// <param name="resourceData"> is an Object expression that specifies the data to add. </param>
        /// <param name="resourceId">   Specifies the ID of the resource to add. </param>
        public static void AddResource(System.Security.Cryptography.Xml.SignedXml signedXml, XmlNodeList resourceData, string resourceId)
        {
            if (resourceData is null || resourceData.Count == 0)
                throw new ArgumentNullException(nameof(resourceData));
            if (string.IsNullOrWhiteSpace(resourceId))
                throw new ArgumentNullException(nameof(resourceId));
            if (signedXml is null)
                throw new ArgumentNullException(nameof(signedXml));

            // Create a data object to hold the data to sign.
            var xmlDataObject = new System.Security.Cryptography.Xml.DataObject()
            {
                Data = resourceData,
                Id = resourceId
            };

            // Add the data object to the signature.
            signedXml.AddObject(xmlDataObject);

            // Create a reference to be able to package everything into the message.
            var xmlReference = new System.Security.Cryptography.Xml.Reference() { Uri = string.Format(System.Globalization.CultureInfo.CurrentCulture, "#{0}", xmlDataObject.Id) };

            // Add the reference to the message
            signedXml.AddReference(xmlReference);
        }

        /// <summary> Creates an XML resource document from the text data to sign. </summary>
        /// <remarks> Use this method to create an XML document with data to sign. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="prefixName">        Name of the prefix. </param>
        /// <param name="resourceName">      Specifies the name of the resource which to create. </param>
        /// <param name="resourceNamespace"> The resource namespace. </param>
        /// <returns> The XML resource document. </returns>
        public static XmlDocument CreateResource(string prefixName, string resourceName, string resourceNamespace)
        {
            if (prefixName is null)
                throw new ArgumentNullException(nameof(prefixName));
            if (string.IsNullOrWhiteSpace(resourceName))
                throw new ArgumentNullException(nameof(resourceName));
            if (string.IsNullOrWhiteSpace(resourceNamespace))
                throw new ArgumentNullException(nameof(resourceNamespace));

            // Create example data to sign.
            var xmlDoc = new XmlDocument();
            var xmlNode = xmlDoc.CreateNode(XmlNodeType.Element, prefixName, resourceName, resourceNamespace);
            _ = xmlDoc.AppendChild( xmlNode );
            return xmlDoc;
        }

        /// <summary> Creates an XML resource document from the text data to sign. </summary>
        /// <remarks> Use this method to create an XML document with data to sign. </remarks>
        /// <param name="resourceName">      Specifies the name of the resource which to create. </param>
        /// <param name="resourceNamespace"> The element namespace. </param>
        /// <returns> The XML resource document. </returns>
        public static XmlDocument CreateResource(string resourceName, string resourceNamespace)
        {
            return CreateResource("", resourceName, resourceNamespace);
        }

        /// <summary> Adds a resource token (name and value). </summary>
        /// <remarks> Adds a node to the resource XML element. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="xmlDocument"> The document. </param>
        /// <param name="tokenName">   The name. </param>
        /// <param name="tokenValue">  The value. </param>
        public static void AddResourceToken(XmlDocument xmlDocument, string tokenName, string tokenValue)
        {
            if (xmlDocument is null)
                throw new ArgumentNullException(nameof(xmlDocument));
            if (string.IsNullOrWhiteSpace(tokenValue))
                throw new ArgumentNullException(nameof(tokenValue));
            if (string.IsNullOrWhiteSpace(tokenName))
                throw new ArgumentNullException(nameof(tokenName));
            XmlNode root = xmlDocument.DocumentElement;
            var elem = xmlDocument.CreateElement(tokenName);
            elem.InnerText = tokenValue;
            _ = root.AppendChild( elem );
        }

        #endregion

        #region " SHARED: KEY MANAGEMENT "

        /// <summary> Creates an RSA signing key. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <returns> A new <see cref="System.Security.Cryptography.RSA">signing key</see>. </returns>
        public static System.Security.Cryptography.RSA CreateRsaSigningKey()
        {
            return System.Security.Cryptography.RSA.Create();
        }

        /// <summary> Saves a RSA signing key. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="key">                      is an System.Security.Cryptography.KeyedHashAlgorithm
        /// object specifying the key which to use for the
        /// signature. </param>
        /// <param name="fullFileName">             Filename of the full file. </param>
        /// <param name="includePrivateParameters"> True to include, false to exclude the private
        /// parameters. </param>
        public static void SaveRsaSigningKey(System.Security.Cryptography.RSA key, string fullFileName, bool includePrivateParameters)
        {
            if (key is null)
                throw new ArgumentNullException(nameof(key));
            if (string.IsNullOrWhiteSpace(fullFileName))
                throw new ArgumentNullException(nameof(fullFileName));
            string xmlString = key.ToXmlString(includePrivateParameters);
            My.MyProject.Computer.FileSystem.WriteAllText(fullFileName, xmlString, false);
        }

        /// <summary> Creates RSA signing key from XML. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <exception cref="System.IO.FileNotFoundException"> Thrown when the requested file is not present. </exception>
        /// <param name="fullFileName"> Filename of the full file. </param>
        /// <returns> The new RSA signing key from XML. </returns>
        public static System.Security.Cryptography.RSA CreateRsaSigningKeyFromXml(string fullFileName)
        {
            if (string.IsNullOrWhiteSpace(fullFileName))
                throw new ArgumentNullException(nameof(fullFileName));
            var fi = new System.IO.FileInfo(fullFileName);
            if (fi.Exists && fi.Length > 128L)
            {
                string xmlString = My.MyProject.Computer.FileSystem.ReadAllText(fullFileName);
                var key = System.Security.Cryptography.RSA.Create();
                key.FromXmlString(xmlString);
                return key;
            }
            else if (!fi.Exists)
            {
                throw new System.IO.FileNotFoundException("RSA Key file not found", fullFileName);
            }
            else
            {
                throw new System.IO.FileNotFoundException("RSA Key file too small", fullFileName);
            }
        }

        #endregion

        #region " SHARED: SIGN SIGNATURE FILE "

        /// <summary> Signs an XML file using an RSA key. </summary>
        /// <remarks>
        /// Use this method to sign a detached resource data and save the signature to a signature file.
        /// </remarks>
        /// <exception cref="ArgumentNullException" guarantee="strong"> . </exception>
        /// <param name="resourceData"> is an Object expression that specifies the resource data which to
        /// sign. </param>
        /// <param name="signingKey">   The signing key. </param>
        /// <param name="resourceId">   Specifies the ID to associate with the signed resource. </param>
        /// <param name="fullFileName"> Specifies the name of the output signature file. </param>
        /// <returns>
        /// A <see cref="System.Security.Cryptography.Xml.SignedXml">document</see> that includes the
        /// signature.
        /// </returns>
        public static System.Security.Cryptography.Xml.SignedXml SignResource(XmlNodeList resourceData, System.Security.Cryptography.RSA signingKey, string resourceId, string fullFileName)
        {
            if (resourceData is null || resourceData.Count == 0)
                throw new ArgumentNullException(nameof(resourceData));
            if (string.IsNullOrWhiteSpace(resourceId))
                throw new ArgumentNullException(nameof(resourceId));
            if (string.IsNullOrWhiteSpace(fullFileName))
                throw new ArgumentNullException(nameof(fullFileName));

            // Create a SignedXml object.
            var signedXml = new System.Security.Cryptography.Xml.SignedXml();

            // Add the xml data to the signed XML
            AddResource(signedXml, resourceData, resourceId);

            // assign the key as a signing key.
            signedXml.SigningKey = signingKey;

            // Add a KeyInfo to the signature
            var thisKeyInfo = new System.Security.Cryptography.Xml.KeyInfo();
            thisKeyInfo.AddClause(new System.Security.Cryptography.Xml.RSAKeyValue(signingKey));
            signedXml.KeyInfo = thisKeyInfo;

            // Compute the signature.
            signedXml.ComputeSignature();

            // save the signature
            WriteSignatureFile(signedXml, fullFileName);

            // return the signed xml
            return signedXml;
        }

        /// <summary> Signs a <see cref="System.Uri">URI</see> resource. </summary>
        /// <remarks>
        /// Use this method to Sign a <see cref="System.Uri">URI</see> resource (e.g.,
        /// "http://www.microsoft.com")
        /// and save the signature in a new file.  the resource can be an XML file.
        /// </remarks>
        /// <exception cref="ArgumentNullException" guarantee="strong"> . </exception>
        /// <param name="resourceUri">  Specifies the location of the resource which to sign. </param>
        /// <param name="fullFileName"> Specifies the name of the output signature file. </param>
        /// <param name="key">          is an System.Security.Cryptography.KeyedHashAlgorithm object
        /// specifying the key which to use for the signature. </param>
        /// <returns>
        /// A <see cref="System.Security.Cryptography.Xml.SignedXml">document</see> that includes the
        /// signature.
        /// </returns>
        public static System.Security.Cryptography.Xml.SignedXml SignResource(Uri resourceUri, string fullFileName, System.Security.Cryptography.KeyedHashAlgorithm key)
        {
            if (resourceUri is null)
                throw new ArgumentNullException(nameof(resourceUri));
            if (string.IsNullOrWhiteSpace(fullFileName))
                throw new ArgumentNullException(nameof(fullFileName));
            if (key is null)
                throw new ArgumentNullException(nameof(key));

            // Create a SignedXml object.
            var signedXml = new System.Security.Cryptography.Xml.SignedXml();

            // Create a reference to be signed.
            // Add the passed URI to the reference object.
            var reference = new System.Security.Cryptography.Xml.Reference() { Uri = resourceUri.ToString() };

            // Add a transformation if the URI is an XML file.
            if (resourceUri.ToString().EndsWith("xml", StringComparison.OrdinalIgnoreCase))
            {
                reference.AddTransform(new System.Security.Cryptography.Xml.XmlDsigC14NTransform());
            }

            // Add the reference to the SignedXml object.
            signedXml.AddReference(reference);

            // Compute the signature.
            signedXml.ComputeSignature(key);

            // save the signature
            WriteSignatureFile(signedXml, fullFileName);

            // return the signed xml
            return signedXml;
        }

        /// <summary> Signs an XML file using an RSA key. </summary>
        /// <remarks>
        /// Use this method to sign an XML file and save the signature to a signature file.
        /// </remarks>
        /// <exception cref="ArgumentNullException" guarantee="strong"> . </exception>
        /// <param name="xmlFileName">  Specifies the XML file name which to sign. </param>
        /// <param name="resourceId">   Specifies the ID to associate with the signed resource. </param>
        /// <param name="signingKey">   The signing key. </param>
        /// <param name="fullFileName"> Specifies the name of the output signature file. </param>
        /// <returns>
        /// A <see cref="System.Security.Cryptography.Xml.SignedXml">document</see> that includes the
        /// signature.
        /// </returns>
        public static System.Security.Cryptography.Xml.SignedXml SignResource(string xmlFileName, string resourceId, System.Security.Cryptography.RSA signingKey, string fullFileName)
        {
            if (string.IsNullOrWhiteSpace(xmlFileName))
                throw new ArgumentNullException(nameof(xmlFileName));
            if (string.IsNullOrWhiteSpace(resourceId))
                throw new ArgumentNullException(nameof(resourceId));
            if (string.IsNullOrWhiteSpace(fullFileName))
                throw new ArgumentNullException(nameof(fullFileName));

            // instantiate a document to read the XML data
            var xmlDoc = new XmlDocument();

            // read the XML data
            xmlDoc.Load(xmlFileName);

            // sign the XML data
            return SignResource(xmlDoc.ChildNodes, signingKey, resourceId, fullFileName);
        }

        /// <summary> Writes the signature file. </summary>
        /// <remarks> Use this method to save the signature to file. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="signedXml">    is the <see cref="System.Security.Cryptography.Xml.SignedXml">
        /// signed XML</see> containing the resource and signature. </param>
        /// <param name="fullFileName"> Specifies the name of the output signature file. </param>
        public static void WriteSignatureFile(System.Security.Cryptography.Xml.SignedXml signedXml, string fullFileName)
        {
            if (signedXml is null)
                throw new ArgumentNullException(nameof(signedXml));
            if (string.IsNullOrWhiteSpace(fullFileName))
                throw new ArgumentNullException(nameof(fullFileName));

            // Save the signed XML document to a file specified using the passed string.
            using var xmltw = new XmlTextWriter( fullFileName, new System.Text.UTF8Encoding( false ) ) {
                Formatting = Formatting.Indented // always sign indented and ignore white space.
            };
            signedXml.GetXml().WriteTo( xmltw );
            // this also disposes; Otherwise, uncomment: .Close()
        }

        #endregion

        #region " SHARED: READ SIGNATURE FILE RESOURCE ID "

        /// <summary> Returns the data string containing the signed resource. </summary>
        /// <remarks> This method, which uses the signed XML API call, does not seem to work. </remarks>
        /// <exception cref="ArgumentNullException" guarantee="strong"> . </exception>
        /// <param name="signedXml">  is the <see cref="System.Security.Cryptography.Xml.SignedXml">
        /// signed XML</see> containing the resource and signature. </param>
        /// <param name="resourceId"> Specifies the ID of the resource in the signature. </param>
        /// <returns> The data string containing the signed resource. </returns>
        [Obsolete("This method does not seem to work")]
        public static string FetchResourceIdOuterXml(System.Security.Cryptography.Xml.SignedXml signedXml, string resourceId)
        {
            if (string.IsNullOrWhiteSpace(resourceId))
                throw new ArgumentNullException(nameof(resourceId));
            if (signedXml is null)
                throw new ArgumentNullException(nameof(signedXml));

            // get the resource element and return the text
            return FetchResourceIdElement(signedXml, resourceId).OuterXml;
        }

        /// <summary> Returns the signed XML element containing the signed resource. </summary>
        /// <remarks> This method, which uses the signed XML API call, does not seem to work. </remarks>
        /// <exception cref="ArgumentNullException" guarantee="strong"> . </exception>
        /// <param name="signedXml">  is the <see cref="System.Security.Cryptography.Xml.SignedXml">
        /// signed XML</see> containing the resource and signature. </param>
        /// <param name="resourceId"> Specifies the ID of the resource in the signature. </param>
        /// <returns> The signed XML element containing the signed resource. </returns>
        [Obsolete("This method does not seem to work")]
        public static XmlElement FetchResourceIdElement(System.Security.Cryptography.Xml.SignedXml signedXml, string resourceId)
        {
            if (string.IsNullOrWhiteSpace(resourceId))
                throw new ArgumentNullException(nameof(resourceId));
            if (signedXml is null)
                throw new ArgumentNullException(nameof(signedXml));

            // Load the XML.
            var xmlDoc = new XmlDocument() { PreserveWhitespace = false }; // required if formatting with indentations.
            xmlDoc.LoadXml(signedXml.GetXml().OuterXml);
            return signedXml.GetIdElement(xmlDoc, resourceId);
        }

        #endregion

        #region " SHARED: READ SIGNATURE FILE "

        /// <summary> Name of the root node. </summary>
        private const string _RootNodeName = "Signature";

        /// <summary> Name of the key information node. </summary>
        private const string _KeyInfoNodeName = "KeyInfo";

        /// <summary> Name of the object node. </summary>
        private const string _ObjectNodeName = "Object";

        /// <summary> Name of the object node identifier. </summary>
        private const string _ObjectNodeIdName = "Id";

        /// <summary> The path element format. </summary>
        private const string _PathElementFormat = "/*[local-name()='{0}']";

        /// <summary> Builds XML path. </summary>
        /// <remarks> David, 2020-09-07. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="path"> The path string, such as <c>/*[local-name()='Signature']/*[local-
        /// name()='KeyInfo']</c>. </param>
        /// <returns> A String. </returns>
        public static string BuildXmlPath(IList<string> path)
        {
            if (path is null || !path.Any())
                throw new ArgumentNullException(nameof(path));
            var builder = new System.Text.StringBuilder();
            foreach (string pathElement in path)
                _ = builder.AppendFormat( _PathElementFormat, pathElement );
            return builder.ToString();
        }

        /// <summary> Fetches a node. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="signedXml"> is the <see cref="System.Security.Cryptography.Xml.SignedXml">
        /// signed XML</see> containing the resource and signature. </param>
        /// <param name="xmlPath">   The XML document path string, such as
        /// <c>/*[local-name()='Signature']/*[local-name()='KeyInfo']</c>.
        /// </param>
        /// <returns> The node. </returns>
        public static XmlNode FetchNode(System.Security.Cryptography.Xml.SignedXml signedXml, string xmlPath)
        {
            if (signedXml is null)
                throw new ArgumentNullException(nameof(signedXml));
            if (string.IsNullOrWhiteSpace(xmlPath))
                throw new ArgumentNullException(nameof(xmlPath));
            var xml = new XmlDocument();
            xml.LoadXml(signedXml.GetXml().OuterXml);
            var node = xml.SelectSingleNode(xmlPath);
            return node;
        }

        /// <summary> Fetches a node. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="signedXml"> is the <see cref="System.Security.Cryptography.Xml.SignedXml">
        /// signed XML</see> containing the resource and signature. </param>
        /// <param name="xmlPath">   The XML document path. </param>
        /// <returns> The node. </returns>
        public static XmlNode FetchNode(System.Security.Cryptography.Xml.SignedXml signedXml, IList<string> xmlPath)
        {
            return signedXml is null ? throw new ArgumentNullException(nameof(signedXml)) : FetchNode(signedXml, BuildXmlPath(xmlPath));
        }

        /// <summary> Returns the data string containing the signed resource. </summary>
        /// <remarks> Use this method to get the data including the signed resource is stored. </remarks>
        /// <exception cref="ArgumentNullException" guarantee="strong"> . </exception>
        /// <param name="signedXml">    is the <see cref="System.Security.Cryptography.Xml.SignedXml">
        /// signed XML</see> containing the resource and signature. </param>
        /// <param name="resourceName"> Specifies the name of the resource which to find. </param>
        /// <returns> The data string containing the signed resource. </returns>
        public static string FetchResourceDataOuterXml(System.Security.Cryptography.Xml.SignedXml signedXml, string resourceName)
        {
            if (string.IsNullOrWhiteSpace(resourceName))
                throw new ArgumentNullException(nameof(resourceName));
            if (signedXml is null)
                throw new ArgumentNullException(nameof(signedXml));
            // get the resource element and return the text
            return FetchResourceDataNode(signedXml, resourceName).OuterXml;
        }

        /// <summary> Gets resource data node. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="signedXml">    is the <see cref="System.Security.Cryptography.Xml.SignedXml">
        /// signed XML</see> containing the resource and signature. </param>
        /// <param name="resourceName"> Specifies the name of the resource which to find. </param>
        /// <returns> The resource data node. </returns>
        public static XmlNode FetchResourceDataNode(System.Security.Cryptography.Xml.SignedXml signedXml, string resourceName)
        {
            return signedXml is null
                ? throw new ArgumentNullException(nameof(signedXml))
                : string.IsNullOrWhiteSpace(resourceName)
                ? throw new ArgumentNullException(nameof(resourceName))
                : FetchNode(signedXml, new string[] { _RootNodeName, _ObjectNodeName, resourceName });
        }

        /// <summary> Returns the resource XML data element. </summary>
        /// <remarks>
        /// Use this method to get the resource encapsulated in the signature in its XML data element.
        /// </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="signedXml"> is the <see cref="System.Security.Cryptography.Xml.SignedXml">
        /// signed XML</see> containing the resource and signature. </param>
        /// <returns> The outer XML of the resource. </returns>
        public static XmlNode FetchResourceDataNode(System.Security.Cryptography.Xml.SignedXml signedXml)
        {
            return signedXml is null ? throw new ArgumentNullException(nameof(signedXml)) : FetchObjectNode(signedXml).ChildNodes[0];
        }

        /// <summary>
        /// Gets the resource name from the
        /// <see cref="FetchObjectNode(System.Security.Cryptography.Xml.SignedXml)">object node</see>.
        /// </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <param name="signedXml"> is the <see cref="System.Security.Cryptography.Xml.SignedXml">
        /// signed XML</see> containing the resource and signature. </param>
        /// <returns> The resource identifier. </returns>
        public static string FetchResourceName(System.Security.Cryptography.Xml.SignedXml signedXml)
        {
            return FetchResourceDataNode(signedXml).Name;
        }

        /// <summary>
        /// Returns the resource XML object node, which contains the signed resource node.
        /// </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="signedXml"> is the <see cref="System.Security.Cryptography.Xml.SignedXml">
        /// signed XML</see> containing the resource and signature. </param>
        /// <returns> The outer XML of the resource. </returns>
        public static XmlNode FetchObjectNode(System.Security.Cryptography.Xml.SignedXml signedXml)
        {
            return signedXml is null
                ? throw new ArgumentNullException(nameof(signedXml))
                : FetchNode(signedXml, new string[] { _RootNodeName, _ObjectNodeName });
        }

        /// <summary> Gets the resource identifier. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <param name="signedXml"> is the <see cref="System.Security.Cryptography.Xml.SignedXml">
        /// signed XML</see> containing the resource and signature. </param>
        /// <returns> The resource identifier. </returns>
        public static string FetchResourceId(System.Security.Cryptography.Xml.SignedXml signedXml)
        {
            return FetchObjectNode(signedXml).Attributes[_ObjectNodeIdName].Value;
        }

        /// <summary> Fetches key information node. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="signedXml"> is the <see cref="System.Security.Cryptography.Xml.SignedXml">
        /// signed XML</see> containing the resource and signature. </param>
        /// <returns> The key information node. </returns>
        public static XmlNode FetchKeyInfoNode(System.Security.Cryptography.Xml.SignedXml signedXml)
        {
            return signedXml is null
                ? throw new ArgumentNullException(nameof(signedXml))
                : FetchNode(signedXml, new string[] { _RootNodeName, _KeyInfoNodeName });
        }

        #endregion

        #region " SHARED: VERIFY SIGNATURE FILE "

        /// <summary>
        /// Query if 'fullFileName' is trusted by comparing the key info to the key info hash. A better
        /// method would be to check the signature using the public key.
        /// </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <param name="fullFileName"> Specifies the name of the signature file. </param>
        /// <param name="keyInfoHash">  The key information hash. </param>
        /// <returns> <c>true</c> if trusted; otherwise <c>false</c> </returns>
        public static bool IsTrusted(string fullFileName, string keyInfoHash)
        {
            // Create a SignedXml
            var signedXml = ReadSignatureFile(fullFileName);
            bool result = string.Equals(keyInfoHash, FetchKeyInfoNode(signedXml).OuterXml.ToBase64Hash(), StringComparison.Ordinal);
            return result;
        }

        /// <summary> Verifies a signature file. </summary>
        /// <remarks>
        /// Use this method to verify a signature file using a key stored in the signature file.
        /// </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="fullFileName"> Specifies the name of the signature file. </param>
        /// <returns> <c>True</c> if the signature verified. </returns>
        public static bool IsVerifySignatureFile(string fullFileName)
        {
            if (string.IsNullOrWhiteSpace(fullFileName))
                throw new ArgumentNullException(nameof(fullFileName));
            // read the signature and return true if checked.
            return ReadSignatureFile(fullFileName).CheckSignature();
        }

        /// <summary> Verifies the signature XML elements. </summary>
        /// <remarks> Use this method to verify a signature XML element. </remarks>
        /// <exception cref="ArgumentNullException" guarantee="strong"> . </exception>
        /// <param name="xmlElement"> is a System.Xml.XmlElement expression that specifies the XML
        /// signature element. </param>
        /// <returns> <c>True</c> if the signature verified. </returns>
        public static bool IsVerifySignature(XmlElement xmlElement)
        {
            if (xmlElement is null)
                throw new ArgumentNullException(nameof(xmlElement));

            // Load the XML element into an xml document
            var xmlDoc = new XmlDocument() { PreserveWhitespace = false };  // required if formatting indentations.
            xmlDoc.LoadXml(xmlElement.OuterXml);

            // Create a SignedXml
            var signedXml = LoadSignature(xmlDoc);
            // return true if checked
            return signedXml.CheckSignature();
        }

        /// <summary> Verifies a signature file. </summary>
        /// <remarks> Use this method to verify a signature file using a key. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="fullFileName"> Specifies the name of the signature file. </param>
        /// <param name="key">          is an System.Security.Cryptography.KeyedHashAlgorithm object
        /// specifying the key which used to sign the file. </param>
        /// <returns> <c>True</c> if the signature verified. </returns>
        public static bool IsVerifySignatureFile(string fullFileName, System.Security.Cryptography.KeyedHashAlgorithm key)
        {
            if (string.IsNullOrWhiteSpace(fullFileName))
                throw new ArgumentNullException(nameof(fullFileName));
            // read the signature and return true if checked.
            return ReadSignatureFile(fullFileName).CheckSignature(key);
        }

        /// <summary> Verifies a signature file. </summary>
        /// <remarks> Use this method to verify a signature file using a key. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="fullFileName"> Specifies the name of the signature file. </param>
        /// <param name="publicKeyXml"> The public key XML. </param>
        /// <returns> <c>True</c> if the signature verified. </returns>
        public static bool IsVerifySignatureFile(string fullFileName, string publicKeyXml)
        {
            if (string.IsNullOrWhiteSpace(publicKeyXml))
                throw new ArgumentNullException(nameof(publicKeyXml));
            using var key = System.Security.Cryptography.RSA.Create();
            key.FromXmlString( publicKeyXml );
            return ReadSignatureFile( fullFileName ).CheckSignature( key );
        }

        /// <summary> Load a signature from an XML document. </summary>
        /// <remarks> Use this method to read a signature file. </remarks>
        /// <exception cref="ArgumentNullException" guarantee="strong"> . </exception>
        /// <param name="xmlDoc"> Specifies the name of the signature file. </param>
        /// <returns>
        /// A <see cref="System.Security.Cryptography.Xml.SignedXml">document</see> that includes the
        /// signature.
        /// </returns>
        public static System.Security.Cryptography.Xml.SignedXml LoadSignature(XmlDocument xmlDoc)
        {
            if (xmlDoc is null)
                throw new ArgumentNullException(nameof(xmlDoc));

            // Find the "Signature" node and create a new XmlNodeList object.
            var xmlNodeList = xmlDoc.GetElementsByTagName("Signature");

            // Create a new SignedXml object and pass it the XML document class.
            var signedXml = new System.Security.Cryptography.Xml.SignedXml();

            // Load the signature node.
            signedXml.LoadXml((XmlElement)xmlNodeList[0]);

            // return the signature 
            return signedXml;
        }

        /// <summary> Reads a signature file. </summary>
        /// <remarks> Use this method to read a signature file. </remarks>
        /// <exception cref="ArgumentNullException" guarantee="strong"> . </exception>
        /// <param name="fullFileName"> Specifies the name of the signature file. </param>
        /// <returns>
        /// A <see cref="System.Security.Cryptography.Xml.SignedXml">document</see> that includes the
        /// signature.
        /// </returns>
        public static System.Security.Cryptography.Xml.SignedXml ReadSignatureFile(string fullFileName)
        {
            if (string.IsNullOrWhiteSpace(fullFileName))
                throw new ArgumentNullException(nameof(fullFileName));

            // Create a new XML document.
            // Load the passed XML file into the document.
            var xmlDoc = new XmlDocument() { PreserveWhitespace = false };  // required if formatting indentations.
            xmlDoc.Load(fullFileName);

            // load and return the XML signature
            return LoadSignature(xmlDoc);
        }

        #endregion

        #region " OPEN / CLOSE "

        /// <summary> Gets the signed XML. </summary>
        /// <value> The signed XML. </value>
        public System.Security.Cryptography.Xml.SignedXml SignedXml { get; private set; }

        /// <summary> Gets the opened status of the instance. </summary>
        /// <value>
        /// <c>IsOpen</c> is a <see cref="Boolean"/> property that is True if the instance is open.
        /// </value>
        public bool IsOpen => this.SignedXml is object;

        /// <summary> opens this instance. </summary>
        /// <remarks> Use this method to open the instance. </remarks>
        public void Open()
        {
            try
            {
                // instantiate the signed XML message.
                this.SignedXml = new System.Security.Cryptography.Xml.SignedXml();
            }
            catch
            {
                // close to meet strong guarantees
                try
                {
                    this.Close();
                }
                finally
                {
                }

                throw;
            }
        }

        /// <summary> Closes the instance. </summary>
        /// <remarks> Use this method to close the instance. </remarks>
        public void Close()
        {
            try
            {
                // terminate the signing key
                if ( this.RsaSigningKey is object)
                {
                    this.RsaSigningKey.Dispose();
                    this.RsaSigningKey = null;
                }
            }
            catch
            {
                throw;
            }
            finally
            {
                // terminate the signed XML data object
                this.SignedXml = null;
            }
        }

        #endregion

        #region " ADD RESOURCE "

        /// <summary> Adds data to the signature file. </summary>
        /// <remarks> Use this method to add data to the signed data set. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="resourceData"> is an Object expression that specifies the data to add. </param>
        /// <param name="resourceId">   Specifies the ID of the data to add. </param>
        public void AddResource(XmlNodeList resourceData, string resourceId)
        {
            if (resourceData is null || resourceData.Count == 0)
                throw new ArgumentNullException(nameof(resourceData));
            if (string.IsNullOrWhiteSpace(resourceId))
                throw new ArgumentNullException(nameof(resourceId));
            AddResource( this.SignedXml, resourceData, resourceId);
        }

        /// <summary> Adds data to the signature file. </summary>
        /// <remarks> Use this method to add data to the signed data set. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="xmlDoc">     Specifies the name of the signature file. </param>
        /// <param name="resourceId"> Specifies the ID of the resource to add. </param>
        public void AddResource(XmlDocument xmlDoc, string resourceId)
        {
            if (xmlDoc is null)
                throw new ArgumentNullException(nameof(xmlDoc));
            if (string.IsNullOrWhiteSpace(resourceId))
                throw new ArgumentNullException(nameof(resourceId));
            this.AddResource(xmlDoc.ChildNodes, resourceId);
        }

        /// <summary> Adds data from file to the signature file. </summary>
        /// <remarks> Use this method to add XML data to the signed data set. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="xmlFileName"> Specifies the XML file name where the data resides. </param>
        /// <param name="resourceId">  Specifies the ID of the data to add. </param>
        /// <returns>
        /// A Boolean data type that is set true if the data was added and properly referenced.
        /// </returns>
        public bool AddResource(string xmlFileName, string resourceId)
        {
            if (string.IsNullOrWhiteSpace(xmlFileName))
                throw new ArgumentNullException(nameof(xmlFileName));
            if (string.IsNullOrWhiteSpace(resourceId))
                throw new ArgumentNullException(nameof(resourceId));

            // instantiate a document to read the XML data
            var xmlDoc = new XmlDocument();

            // read the XML data
            xmlDoc.Load(xmlFileName);

            // Add the xml data
            this.AddResource(xmlDoc.ChildNodes, resourceId);
            return default;
        }

        #endregion

        #region " SIGN RESOURCE "

        /// <summary> Gets the signing RSA key. </summary>
        /// <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
        /// <value> The RSA signing key. </value>
        public System.Security.Cryptography.RSA RsaSigningKey { get; private set; }

        /// <summary> Generates the signature. </summary>
        /// <remarks> Use this method to generate the signature. </remarks>
        /// <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
        public void GenerateSignature()
        {
            if ( this.RsaSigningKey is null)
                throw new InvalidOperationException("RSA signing key must be imported from XML file");

            // assign the key as a signing key.
            this.SignedXml.SigningKey = this.RsaSigningKey;

            // Add a KeyInfo to the signature
            var thisKeyInfo = new System.Security.Cryptography.Xml.KeyInfo();
            thisKeyInfo.AddClause(new System.Security.Cryptography.Xml.RSAKeyValue( this.RsaSigningKey ));
            this.SignedXml.KeyInfo = thisKeyInfo;

            // Compute the signature.
            this.SignedXml.ComputeSignature();
        }

        /// <summary> Saves an RSA signing key. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        public void SaveRsaSigningKey()
        {
            this.SaveRsaSigningKey(My.Settings.Default.SigningKeyPairFullFileName, true);
            this.SaveRsaSigningKey(My.Settings.Default.VerifyingKeyFullFileName, false);
        }

        /// <summary> Saves a RSA signing key. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <param name="fullFileName">             Filename of the full file. </param>
        /// <param name="includePrivateParameters"> True to include, false to exclude the private
        /// parameters. </param>
        public void SaveRsaSigningKey(string fullFileName, bool includePrivateParameters)
        {
            string xmlString = this.RsaSigningKey.ToXmlString(includePrivateParameters);
            My.MyProject.Computer.FileSystem.WriteAllText(fullFileName, xmlString, false);
        }

        /// <summary> Loads RSA signing key. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        public void LoadRsaSigningKey()
        {
            this.LoadRsaSigningKey(My.Settings.Default.SigningKeyPairFullFileName);
        }

        /// <summary> Loads rsa verifying key. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        public void LoadRsaVerifyingKey()
        {
            this.LoadRsaSigningKey(My.Settings.Default.VerifyingKeyFullFileName);
        }

        /// <summary> Loads RSA signing key. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <param name="fullFileName"> Filename of the full file. </param>
        public void LoadRsaSigningKey(string fullFileName)
        {
            this.RsaSigningKey = CreateRsaSigningKeyFromXml(fullFileName);
        }

        #endregion

        #region " GET RESOURCE INFO "

        /// <summary> Gets the outer XML of the signature file. </summary>
        /// <remarks> Use this property to get the contents of the signature file. </remarks>
        /// <value> <c>Signature</c> is a String property that can be read from (read only). </value>
        public string OuterXml => this.SignedXml.GetXml().OuterXml;

        /// <summary> Fetches a node. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="xmlPath"> The XML document path. </param>
        /// <returns> The node. </returns>
        public XmlNode FetchNode(string xmlPath)
        {
            return string.IsNullOrWhiteSpace(xmlPath) ? throw new ArgumentNullException(nameof(xmlPath)) : FetchNode( this.SignedXml, xmlPath);
        }

        /// <summary> Fetches a node. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <param name="xmlPath"> The XML document path. </param>
        /// <returns> The node. </returns>
        public XmlNode FetchNode(IList<string> xmlPath)
        {
            return FetchNode( this.SignedXml, xmlPath);
        }

        /// <summary> Fetches key information node. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <returns> The key information node. </returns>
        public XmlNode FetchKeyInfoNode()
        {
            return FetchKeyInfoNode( this.SignedXml );
        }

        /// <summary> Returns the data string containing the signed resource. </summary>
        /// <remarks> Use this method to get the data including the signed resource is stored. </remarks>
        /// <returns> The data string containing the signed resource. </returns>
        public string FetchResourceDataOuterXml()
        {
            // return the outer XML of the resource
            return this.FetchResourceDataNode().OuterXml;
        }

        /// <summary> Returns the resource XML data element. </summary>
        /// <remarks>
        /// Use this method to get the resource encapsulated in the signature in its XML data element.
        /// </remarks>
        /// <returns> The outer XML of the resource. </returns>
        public XmlNode FetchResourceDataNode()
        {
            // return the outer XML of the resource
            return FetchResourceDataNode( this.SignedXml );
        }

        /// <summary> Returns the data string containing the signed resource. </summary>
        /// <remarks> Use this method to get the data including the signed resource is stored. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="resourceName"> Specifies the name of the resource which to find. </param>
        /// <returns> The data string containing the signed resource. </returns>
        public string FetchResourceDataOuterXml(string resourceName)
        {
            if (string.IsNullOrWhiteSpace(resourceName))
                throw new ArgumentNullException(nameof(resourceName));
            // return the outer XML of the resource
            return this.FetchResourceDataNode(resourceName).OuterXml;
        }

        /// <summary> Returns the resource XML data element. </summary>
        /// <remarks>
        /// Use this method to get the resource encapsulated in the signature in its XML data element.
        /// </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="resourceName"> Specifies the name of the resource which to find. </param>
        /// <returns> The outer XML of the resource. </returns>
        public XmlNode FetchResourceDataNode(string resourceName)
        {
            if (string.IsNullOrWhiteSpace(resourceName))
                throw new ArgumentNullException(nameof(resourceName));

            // return the outer XML of the resource
            return FetchResourceDataNode( this.SignedXml, resourceName);
        }

        /// <summary> Gets the resource identifier. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <returns> The resource identifier. </returns>
        public string FetchResourceId()
        {
            var node = FetchObjectNode( this.SignedXml );
            return node.Attributes["Id"].Value;
        }

        /// <summary> Gets resource name. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <returns> The resource identifier. </returns>
        public string FetchResourceName()
        {
            return FetchResourceDataNode( this.SignedXml ).Name;
        }

        /// <summary> verifies the signature. </summary>
        /// <remarks> Use this method to verify the signature. </remarks>
        /// <returns> <c>True</c> if the signature verified. </returns>
        public bool IsVerifySignature()
        {
            return IsVerifySignature( this.SignedXml.GetXml());
        }

        /// <summary> verifies the signature. </summary>
        /// <remarks> Use this method to read and verify the signature. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="signatureFileName"> Specifies the XML file name where the data resides. </param>
        /// <returns> <c>True</c> if the signature verified. </returns>
        public bool IsVerifySignature(string signatureFileName)
        {
            if (string.IsNullOrWhiteSpace(signatureFileName))
                throw new ArgumentNullException(nameof(signatureFileName));

            // read the signature
            this.SignedXml = ReadSignatureFile(signatureFileName);

            // return true if verified
            return this.IsVerifySignature();
        }

        /// <summary> Reads the signature file. </summary>
        /// <remarks> Use this method to read the signature Information. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="fullFileName"> Specifies the XML file name where the data resides. </param>
        public void ReadSignature(string fullFileName)
        {
            if (string.IsNullOrWhiteSpace(fullFileName))
                throw new ArgumentNullException(nameof(fullFileName));

            // read and set internal signature object.
            this.SignedXml = ReadSignatureFile(fullFileName);
        }

        /// <summary> Writes the signature to the XML file. </summary>
        /// <remarks> Use this method to write the signature. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="fullFileName"> Specifies the name of the output signature file. </param>
        public void WriteSignature(string fullFileName)
        {
            if (string.IsNullOrWhiteSpace(fullFileName))
                throw new ArgumentNullException(nameof(fullFileName));
            WriteSignatureFile( this.SignedXml, fullFileName);
        }

        #endregion

        #region " EXAMPLES "

        /// <summary> Try sign. </summary>
        /// <remarks>
        /// <code>
        /// Dim details As String = String.Empty
        /// Dim outcome as Boolean = TrySign("xmlsig.xml",details)
        /// </code>
        /// </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="xmlFileName"> Specifies the XML file name where the data resides. </param>
        /// <param name="e">           Cancel details event information. </param>
        /// <returns> <c>True</c> is okay. </returns>
        public static bool TrySign(string xmlFileName, Core.ActionEventArgs e)
        {
            if (e is null)
                throw new ArgumentNullException(nameof(e));
            if (string.IsNullOrWhiteSpace(xmlFileName))
                throw new ArgumentNullException(nameof(xmlFileName));
            bool result = false;
            try
            {

                // The URI to sign.
                var resourceToSign = new Uri("http://www.microsoft.com");

                // Generate a signing key.
                using (var Key = new System.Security.Cryptography.HMACSHA1())
                {
                    Console.WriteLine("Signing: {0}", resourceToSign);

                    // Sign the detached resource and save the signature in an XML file.
                    _ = SignResource( resourceToSign, xmlFileName, Key );
                    Console.WriteLine("XML signature was successfully computed and saved to {0}.", xmlFileName);

                    // Verify the signature of the signed XML.
                    Console.WriteLine("Verifying signature...");

                    // Verify the XML signature in the XML file.
                    result = IsVerifySignatureFile(xmlFileName, Key);
                }

                // Display the results of the signature verification to the console.
                if (result)
                {
                    Console.WriteLine("The XML signature is valid.");
                }
                else
                {
                    Console.WriteLine("The XML signature is not valid.");
                    e.RegisterFailure("The XML signature is not valid.");
                }
            }
            catch (System.Security.Cryptography.CryptographicException ex)
            {
                Console.WriteLine(ex.Message);
                e.RegisterError($"Exception {ex.ToFullBlownString()}");
            }

            return result;
        }

        #endregion

    }
}
