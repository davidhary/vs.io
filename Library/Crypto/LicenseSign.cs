using System;
using System.ComponentModel;
using isr.Core.HashExtensions;

namespace isr.IO.Cryptography
{

    /// <summary> Signs license tokens. </summary>
    /// <remarks>
    /// (c) 2010 Integrated Scientific Resources, Inc. All rights reserved. <para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2003-06-21, 1.0.1267.x. </para>
    /// </remarks>
    public class LicenseSign : TokenSignBase
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Default constructor. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        public LicenseSign() : base()
        {
            this.SupportedTokenNames = LicenseTokenNames.AllOptions;
            this.VerifiableTokenNames = LicenseTokenNames.SerialNumber | LicenseTokenNames.SerialNumberHash;
            this.ResourceName = "LicenseInfo";
            this.ResourceNamespace = "Values";
            this.ExpirationDate = DateTime.MaxValue;
            this.ActivationsCount = 0;
            this.Salt = "0";
        }

        /// <summary> Constructor. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <param name="serialNumber">   The serial number. </param>
        /// <param name="programVersion"> The program version. </param>
        /// <param name="programOptions"> The program version. </param>
        public LicenseSign(string serialNumber, Version programVersion, long programOptions) : this()
        {
            this.SerialNumber = serialNumber;
            this.ProgramVersion = programVersion;
            this.ProgramOptions = programOptions;
        }

        #endregion

        #region " BUILD TOKENS "

        /// <summary> Adds tokens to the token dictionary. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
        public override void AddTokens()
        {
            // add object per the selected system token
            if ((LicenseTokenNames.SerialNumber & this.SupportedTokenNames) != 0)
            {
                this.AddSerialNumberToken();
            }
            else
            {
                throw new InvalidOperationException($"Token of type '{LicenseTokenNames.SerialNumber}' is not supported at this time");
            }

            if ((LicenseTokenNames.SerialNumberHash & this.SupportedTokenNames) != 0)
                this.AddSerialNumberHashToken();
            if ((LicenseTokenNames.ProgramVersion & this.SupportedTokenNames) != 0)
                this.AddProgramVersionToken();
            if ((LicenseTokenNames.TopVersion & this.SupportedTokenNames) != 0)
                this.AddTopVersionToken();
            if ((LicenseTokenNames.ExpirationDate & this.SupportedTokenNames) != 0)
                this.AddExpirationDate();
            if ((LicenseTokenNames.ActivationCount & this.SupportedTokenNames) != 0)
                this.AddActivationsCount();
            if ((LicenseTokenNames.ProgramOptions & this.SupportedTokenNames) != 0)
                this.AddProgramOptionsToken();
        }

        #endregion

        #region " READ AND PARSE "

        /// <summary> Reads and parses the signed resource. </summary>
        /// <remarks> Use this method to read the signed resource. </remarks>
        public override void ReadParse()
        {
            base.ReadParse();
            if ((LicenseTokenNames.ProgramVersion & this.SupportedTokenNames) != 0)
            {
                this.ProgramVersion = new Version( this.TokenDictionary[LicenseTokenName.ProgramVersion.ToString()]);
            }

            if ((LicenseTokenNames.ProgramOptions & this.SupportedTokenNames) != 0)
            {
                this.ProgramOptions = int.Parse( this.TokenDictionary[LicenseTokenName.ProgramOptions.ToString()]);
            }

            if ((LicenseTokenNames.TopVersion & this.SupportedTokenNames) != 0)
            {
                this.TopVersion = new Version( this.TokenDictionary[LicenseTokenName.TopVersion.ToString()]);
            }

            if ((LicenseTokenNames.ExpirationDate & this.SupportedTokenNames) != 0)
            {
                this.ExpirationDate = DateTime.Parse( this.TokenDictionary[LicenseTokenName.ExpirationDate.ToString()]);
            }

            if ((LicenseTokenNames.ActivationCount & this.SupportedTokenNames) != 0)
            {
                this.ActivationsCount = int.Parse( this.TokenDictionary[LicenseTokenName.ActivationCount.ToString()]);
            }
        }


        #endregion

        #region " TOKEN MANAGEMENT "

        /// <summary> Gets or sets the supported system token names. </summary>
        /// <remarks>
        /// Use this property to get or set the type of token(s) the System Token will Create and sign.
        /// </remarks>
        /// <value>
        /// <c>SystemTokenType</c> is a SystemTokenTypes enumerated property that can be read from or
        /// written too (read or write).
        /// </value>
        public LicenseTokenNames SupportedTokenNames { get; set; }

        /// <summary> Gets or sets a list of names of the verifiable tokens. </summary>
        /// <value> A list of names of the verifiable tokens. </value>
        public LicenseTokenNames VerifiableTokenNames { get; set; }

        /// <summary> Verify tokens. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="e"> Cancel details event information. </param>
        /// <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        public override bool VerifyTokens(Core.ActionEventArgs e)
        {
            if (e is null)
                throw new ArgumentNullException(nameof(e));
            if ((LicenseTokenNames.SerialNumber & this.VerifiableTokenNames) != 0)
            {
                _ = this.VerifySerialNumberToken( e );
            }

            if (!e.Failed && (LicenseTokenNames.SerialNumberHash & this.VerifiableTokenNames) != 0)
            {
                _ = this.VerifySerialNumberHashToken( e );
            }

            return !e.Failed;
        }

        #region " SERIAL NUMBER TOKEN "

        /// <summary> Gets or sets the serial number. </summary>
        /// <value> The serial number. </value>
        public string SerialNumber { get; set; }

        /// <summary> Adds a system token data object based on the hard disk serial number. </summary>
        /// <remarks> Use this method to add a hard disk serial number system token. </remarks>
        private void AddSerialNumberToken()
        {
            // get serial number of system drive
            this.AddToken(LicenseTokenName.SerialNumber.ToString(), this.SerialNumber );
        }

        /// <summary> verifies a system token data object based on the hard disk serial number. </summary>
        /// <remarks>
        /// Use this method to verify a token based on the system hard disk serial number.
        /// </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="e"> Action event information. </param>
        /// <returns> <c>True</c> if verified. </returns>
        private bool VerifySerialNumberToken(Core.ActionEventArgs e)
        {
            if (e is null)
                throw new ArgumentNullException(nameof(e));
            string tokenValue = this.SerialNumber;
            string tokenName = LicenseTokenName.SerialNumber.ToString();
            return this.VerifyResourceToken(tokenName, tokenValue, e);
        }

        #endregion

        #region " SERIAL NUMBER HASH TOKEN "

        /// <summary> Gets or sets the salt. </summary>
        /// <value> The salt. </value>
        public string Salt { get; set; }

        /// <summary> Adds a system token data object based on the hard disk serial number. </summary>
        /// <remarks> Use this method to add a hard disk serial number system token. </remarks>
        private void AddSerialNumberHashToken()
        {
            // get serial number of system drive
            string tokenValue = $"{this.SerialNumber}.{this.Salt}".ToBase64Hash();
            this.AddToken(LicenseTokenName.SerialNumberHash.ToString(), tokenValue);
        }

        /// <summary> verifies a system token data object based on the hard disk serial number. </summary>
        /// <remarks>
        /// Use this method to verify a token based on the system hard disk serial number.
        /// </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="e"> Action event information. </param>
        /// <returns> <c>True</c> if verified. </returns>
        private bool VerifySerialNumberHashToken(Core.ActionEventArgs e)
        {
            if (e is null)
                throw new ArgumentNullException(nameof(e));
            string tokenValue = $"{this.SerialNumber}.{this.Salt}".ToBase64Hash();
            string tokenName = LicenseTokenName.SerialNumberHash.ToString();
            return this.VerifyResourceToken(tokenName, tokenValue, e);
        }

        #endregion

        #region " PROGRAM OPTIONS TOKEN "

        /// <summary> Gets or sets the program options. </summary>
        /// <value> The program version. </value>
        public long ProgramOptions { get; set; }

        /// <summary> Adds program options token. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        private void AddProgramOptionsToken()
        {
            this.AddToken(LicenseTokenName.ProgramOptions.ToString(), this.ProgramOptions.ToString());
        }

        #endregion

        #region " PROGRAM VERSION TOKEN "

        /// <summary> Gets or sets the program version. </summary>
        /// <value> The program version. </value>
        public Version ProgramVersion { get; set; }

        /// <summary> Adds program version token. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        private void AddProgramVersionToken()
        {
            this.AddToken(LicenseTokenName.ProgramVersion.ToString(), this.ProgramVersion.ToString());
        }

        #endregion

        #region " TOP VERSION TOKEN "

        /// <summary> Gets or sets the top version. </summary>
        /// <value> The top version. </value>
        public Version TopVersion { get; set; }

        /// <summary> Adds top version token. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        private void AddTopVersionToken()
        {
            this.TopVersion = new Version( this.ProgramVersion.Major, 9);
            this.AddToken(LicenseTokenName.TopVersion.ToString(), this.TopVersion.ToString());
        }

        /// <summary> Query if 'version' greater than top version. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <param name="version"> The version. </param>
        /// <returns>
        /// <c>true</c> if specified version exceeds the top version; otherwise <c>false</c>
        /// </returns>
        public bool IsVersionedOut(Version version)
        {
            return version is null || version.CompareTo( this.TopVersion ) > 0;
        }

        #endregion

        #region " EXPIRATION DATE "

        /// <summary> Gets or sets the expiration date in universal time. </summary>
        /// <value> The expiration date in universal time. </value>
        public DateTimeOffset ExpirationDate { get; set; }

        /// <summary> Adds expiration date. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        private void AddExpirationDate()
        {
            this.AddToken(LicenseTokenName.ExpirationDate.ToString(), this.ExpirationDate.Date.ToShortDateString());
        }

        /// <summary> Query if this object is expired. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <returns> <c>true</c> if expired; otherwise <c>false</c> </returns>
        public bool IsExpired()
        {
            return DateTimeOffset.UtcNow > this.ExpirationDate;
        }

        #endregion

        #region " ACTIVATIONS "

        /// <summary> Gets or sets the number of activations. </summary>
        /// <value> The number of activations. </value>
        public int ActivationsCount { get; set; }

        /// <summary> Adds activations count. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        private void AddActivationsCount()
        {
            this.AddToken(LicenseTokenName.ActivationCount.ToString(), this.ActivationsCount.ToString());
        }

        /// <summary> Query if 'count' is activated out. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <param name="count"> Number of. </param>
        /// <returns> <c>true</c> if activated out; otherwise <c>false</c> </returns>
        public bool IsActivatedOut(int count)
        {
            return this.ActivationsCount > 0 && this.ActivationsCount < count;
        }
        #endregion

        #endregion

    }

        /// <summary> Enumerates which system tokens are supported. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
    [Flags()]
    public enum LicenseTokenNames
    {

        /// <summary> . </summary>
        [Description("None")]
        None,

        /// <summary> Serial number. </summary>
        [Description("Serial Number")]
        SerialNumber = 1,

        /// <summary> Serial number hash. </summary>
        [Description("Serial Number Hash")]
        SerialNumberHash = SerialNumber << 1,

        /// <summary> Expiration date. </summary>
        [Description("Expiration Date")]
        ExpirationDate = SerialNumberHash << 1,

        /// <summary> An enum constant representing the Activation Count option. </summary>
        [Description("Activation Count")]
        ActivationCount = ExpirationDate << 1,

        /// <summary> An enum constant representing the program version option. </summary>
        [Description("Program Version")]
        ProgramVersion = ActivationCount << 1,

        /// <summary> An enum constant representing the top version option. </summary>
        [Description("Top Version")]
        TopVersion = ProgramVersion << 1,

        /// <summary> An enum constant representing the program Options option. </summary>
        [Description("Program Options")]
        ProgramOptions = TopVersion << 1,

        /// <summary> An enum constant representing all options. </summary>
        [Description("All Options")]
        AllOptions = (ProgramOptions << 1) - 1
    }

        /// <summary> Values that represent system token names. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
    public enum LicenseTokenName
    {

        /// <summary> . </summary>
        [Description("None")]
        None,

        /// <summary> An enum constant representing the file name. </summary>
        [Description("File Version")]
        FileName = BaseTokenName.FileName,

        /// <summary> An enum constant representing the file version. </summary>
        [Description("File Version")]
        FileVersion = BaseTokenName.FileVersion,

        /// <summary> An enum constant representing the serial number option. </summary>
        [Description("Serial Number")]
        SerialNumber,

        /// <summary> An enum constant representing the serial number hash option. </summary>
        [Description("Serial Number Hash")]
        SerialNumberHash,

        /// <summary> An enum constant representing the expiration date option. </summary>
        [Description("Expiration Date")]
        ExpirationDate,

        /// <summary> An enum constant representing the Activation Count. </summary>
        [Description("Activation Count")]
        ActivationCount,

        /// <summary> An enum constant representing the program version. </summary>
        [Description("Program Version")]
        ProgramVersion,

        /// <summary> An enum constant representing the top version. </summary>
        [Description("Top Version")]
        TopVersion,

        /// <summary> An enum constant representing the program options. </summary>
        [Description("Program Options")]
        ProgramOptions
    }
}
