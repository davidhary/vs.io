using System;
using System.Collections;
using System.ComponentModel;
using isr.Core.HashExtensions;

namespace isr.IO.Cryptography
{

    /// <summary> Signs system tokens. </summary>
    /// <remarks>
    /// (c) 2010 Integrated Scientific Resources, Inc. All rights reserved. <para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2003-06-21, 1.0.1267.x. </para>
    /// </remarks>
    public class SystemSign : TokenSignBase
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Default constructor. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        public SystemSign() : base()
        {
            this.SupportedTokenNames = SystemTokenNames.SystemDriveSerialNumber | SystemTokenNames.SystemDriveSerialNumberHash;
            this.VerifiableTokenNames = SystemTokenNames.SystemDriveSerialNumber | SystemTokenNames.SystemDriveSerialNumberHash;
            this.ResourceName = "SystemInfo";
            this.ResourceNamespace = "Values";
        }

        #endregion

        #region " SHARED "

        /// <summary> This shared method gets a system property. </summary>
        /// <remarks>
        /// Use Read a system property The following information can be derived (path/property): Path:
        /// "win32_logicalDisk.DeviceId='c:'" Properties: "VolumeSerialNumber", "deviceId", (see
        /// win32_logicalDisk)
        /// Path: "Win32_Processor.DeviceId='CPU0'" Properties: "ProcessorId" Note that some devices
        /// require two keys in the path.  For these it would be possible to get the required information
        /// from the enumerated list.
        /// </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="systemPath">     is a String expression that identifies the system including a
        /// system name and key (see remarks). </param>
        /// <param name="systemProperty"> is a String expression that identifies the system property. </param>
        /// <returns> A system property. </returns>
        public static string GetSystemProperty(string systemPath, string systemProperty)
        {
            if (string.IsNullOrWhiteSpace(systemPath))
                throw new ArgumentNullException(nameof(systemPath));
            if (string.IsNullOrWhiteSpace(systemProperty))
                throw new ArgumentNullException(nameof(systemProperty));
            using var systemEntity = new System.Management.ManagementObject( systemPath );
            systemEntity.Get();
            return systemEntity[systemProperty].ToString();
        }

        /// <summary> This shared method gets a system property. </summary>
        /// <remarks>
        /// Use Read a system property The following information can be derived (name/property): Name:
        /// "Win32_Processor" Properties: "ProcessorId" Name: "Win32_BIOS" Properties: "Version" "BIOS
        /// Version" Note that some devices require two keys in the path.  For theses it would be
        /// possible to get the required information from the enumerated list.
        /// </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="systemName">     is a String expression that identifies the system. </param>
        /// <param name="systemProperty"> is a String expression that identifies the system property. </param>
        /// <returns> A system property. </returns>
        public static string GetSystemPropertyFirstEnum(string systemName, string systemProperty)
        {
            if (string.IsNullOrWhiteSpace(systemName))
                throw new ArgumentNullException(nameof(systemName));
            if (string.IsNullOrWhiteSpace(systemProperty))
                throw new ArgumentNullException(nameof(systemProperty));
            string result = string.Empty;
            using (var systemEntity = new System.Management.ManagementClass(systemName))
            {
                System.Management.ManagementObjectCollection systemsAvailable;
                systemsAvailable = systemEntity.GetInstances();
                foreach (System.Management.ManagementObject oneSystem in systemsAvailable)
                {
                    var mgmObj = oneSystem[systemProperty];
                    if (mgmObj is object)
                    {
                        result = mgmObj.ToString();
                        break;
                    }
                }
            }

            return result;
        }

        /// <summary> This shared method enumerates the systems. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="systemName">     is a String expression that identifies the system. </param>
        /// <param name="systemProperty"> is a String expression that identifies the system property. </param>
        /// <returns> List of systems. </returns>
        /// <example>
        /// <code>
        /// Dim systemName As Object
        /// Dim systemList As ArrayList
        /// systemList = SystemSign.EnumerateSystems("Win32_NetworkAdapter")
        /// me.systemsTextBox.Clear()
        /// For Each systemName In systemList
        /// me.systemsTextBox.AppendText(systemName.ToString)
        /// me.systemsTextBox.AppendText(Environment.NewLine)
        /// Next
        /// </code>
        /// </example>
        public static ArrayList EnumerateSystems(string systemName, string systemProperty)
        {
            if (string.IsNullOrWhiteSpace(systemName))
                throw new ArgumentNullException(nameof(systemName));
            if (string.IsNullOrWhiteSpace(systemProperty))
                throw new ArgumentNullException(nameof(systemProperty));
            using var systemEntity = new System.Management.ManagementClass( systemName );
            System.Management.ManagementObjectCollection systemsAvailable;
            var names = new ArrayList();
            systemsAvailable = systemEntity.GetInstances();
            names.Clear();
            foreach ( System.Management.ManagementObject oneSystem in systemsAvailable )
                _ = names.Add( oneSystem[systemProperty].ToString() );
            return names;
        }

        #endregion

        #region " BUILD "

        /// <summary> Adds tokens to the token dictionary. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
        public override void AddTokens()
        {
            // add object per the selected system token
            if ((SystemTokenNames.SystemDriveSerialNumber & this.SupportedTokenNames) != 0)
            {
                this.AddSystemDriveSerialNumberToken();
            }
            else
            {
                throw new InvalidOperationException($"Token of type '{SystemTokenNames.SystemDriveSerialNumber}' is not supported at this time");
            }

            if ((SystemTokenNames.SystemDriveSerialNumberHash & this.SupportedTokenNames) != 0)
            {
                this.AddSystemDriveSerialNumberHashToken();
            }
        }

        #endregion

        #region " TOKEN MANAGEMENT "

        /// <summary> Gets or sets the supported system token names. </summary>
        /// <remarks>
        /// Use this property to get or set the type of token(s) the System Token will Create and sign.
        /// </remarks>
        /// <value>
        /// <c>SystemTokenType</c> is a SystemTokenTypes enumerated property that can be read from or
        /// written too (read or write).
        /// </value>
        public SystemTokenNames SupportedTokenNames { get; set; }

        /// <summary> Gets or sets a list of names of the verifiable system tokens. </summary>
        /// <value> A list of names of the verifiable system tokens. </value>
        public SystemTokenNames VerifiableTokenNames { get; set; }

        /// <summary> Verify tokens. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="e"> Cancel details event information. </param>
        /// <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        public override bool VerifyTokens(Core.ActionEventArgs e)
        {
            if (e is null)
                throw new ArgumentNullException(nameof(e));
            if ((SystemTokenNames.SystemDriveSerialNumber & this.VerifiableTokenNames) != 0)
            {
                _ = this.VerifySystemDriveSerialNumberToken( e );
            }

            if (!e.Failed && (SystemTokenNames.SystemDriveSerialNumberHash & this.VerifiableTokenNames) != 0)
            {
                _ = this.VerifySystemDriveSerialNumberHashToken( e );
            }

            return !e.Failed;
        }

        #region " SERIAL NUMBER TOKEN "

        /// <summary> Adds a system token data object based on the hard disk serial number. </summary>
        /// <remarks> Use this method to add a hard disk serial number system token. </remarks>
        private void AddSystemDriveSerialNumberToken()
        {
            // get serial number of system drive
            string tokenValue = GetSystemDriveSerialNumberToken();
            this.AddToken(SystemTokenName.SystemDriveSerialNumber.ToString(), tokenValue);
        }

        /// <summary> verifies a system token data object based on the hard disk serial number. </summary>
        /// <remarks>
        /// Use this method to verify a token based on the system hard disk serial number.
        /// </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="e"> Action event information. </param>
        /// <returns> <c>True</c> if verified. </returns>
        private bool VerifySystemDriveSerialNumberToken(Core.ActionEventArgs e)
        {
            if (e is null)
                throw new ArgumentNullException(nameof(e));
            string tokenValue = GetSystemDriveSerialNumberToken();
            string tokenName = SystemTokenName.SystemDriveSerialNumber.ToString();
            return this.VerifyResourceToken(tokenName, tokenValue, e);
        }

        /// <summary> Gets the serial number of the system hard drive. </summary>
        /// <remarks> Use this method to get the serial number of the system drive. </remarks>
        /// <returns> The driver serial number. </returns>
        private static string GetSystemDriveSerialNumberToken()
        {
            string drive = Environment.SystemDirectory.Substring(0, 2);
            using var disk = new System.Management.ManagementObject( string.Format( System.Globalization.CultureInfo.CurrentCulture, "win32_logicalDisk.DeviceId='{0}'", drive ) );
            disk.Get();
            return disk["VolumeSerialNumber"].ToString();
        }

        #endregion

        #region " SERIAL NUMBER HASH TOKEN "

        /// <summary> Adds a system token data object based on the hard disk serial number. </summary>
        /// <remarks> Use this method to add a hard disk serial number system token. </remarks>
        private void AddSystemDriveSerialNumberHashToken()
        {
            // get serial number of system drive
            string tokenValue = GetSystemDriveSerialNumberToken().ToBase64Hash();
            this.AddToken(SystemTokenName.SystemDriveSerialNumberHash.ToString(), tokenValue);
        }

        /// <summary> verifies a system token data object based on the hard disk serial number. </summary>
        /// <remarks>
        /// Use this method to verify a token based on the system hard disk serial number.
        /// </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="e"> Action event information. </param>
        /// <returns> <c>True</c> if verified. </returns>
        private bool VerifySystemDriveSerialNumberHashToken(Core.ActionEventArgs e)
        {
            if (e is null)
                throw new ArgumentNullException(nameof(e));
            string tokenValue = GetSystemDriveSerialNumberToken().ToBase64Hash();
            string tokenName = SystemTokenName.SystemDriveSerialNumberHash.ToString();
            return this.VerifyResourceToken(tokenName, tokenValue, e);
        }

        #endregion


        #endregion

    }

        /// <summary> Enumerates which system tokens are supported. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
    [Flags()]
    public enum SystemTokenNames
    {

        /// <summary> . </summary>
        [Description("None")]
        None,

        /// <summary> System Drive Serial Number. </summary>
        [Description("System Drive Serial Number")]
        SystemDriveSerialNumber = 1,

        /// <summary> System Drive Serial Number Hash. </summary>
        [Description("System Drive Serial Number Hash")]
        SystemDriveSerialNumberHash = SystemDriveSerialNumber << 1,

        /// <summary>  Distance between two hidden system files. </summary>
        [Description("File Distance")]
        FileDistance = SystemDriveSerialNumberHash << 1,

        /// <summary> c:\ creation date and time. </summary>
        [Description("System Drive Creation Date")]
        SystemDriveCreationDate = FileDistance << 1,

        /// <summary> CPU ID. </summary>
        [Description("Processor Id")]
        ProcessorId = SystemDriveCreationDate << 1,

        /// <summary>  Bios Version. </summary>
        [Description("Bios Version")]
        BiosVersion = ProcessorId << 1,
        [Description("All Options")]
        AllOptions = (BiosVersion << 1) - 1
    }

        /// <summary> Values that represent system token names. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
    public enum SystemTokenName
    {

        /// <summary> . </summary>
        [Description("None")]
        None,

        /// <summary> Name of the file name token. </summary>
        [Description("File Version")]
        FileName = BaseTokenName.FileName,

        /// <summary> Name of the file name token. </summary>
        [Description("File Version")]
        FileVersion = BaseTokenName.FileVersion,

        /// <summary> An enum constant representing the system drive serial number option. </summary>
        [Description("System Drive Serial Number")]
        SystemDriveSerialNumber,

        /// <summary> An enum constant representing the system drive serial number hash option. </summary>
        [Description("System Drive Serial Number Hash")]
        SystemDriveSerialNumberHash,

        /// <summary>  Distance between two hidden system files. </summary>
        [Description("File Distance")]
        FileDistance,

        /// <summary> c:\ creation date and time. </summary>
        [Description("System Drive Creation Date")]
        SystemDriveCreationDate,

        /// <summary> CPU ID. </summary>
        [Description("Processor Id")]
        ProcessorId,

        /// <summary>  Bios Version. </summary>
        [Description("Bios Version")]
        BiosVersion
    }
}
