﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;

namespace isr.IO.Cryptography
{

    /// <summary> This is the abstract base class of the library of token signing. </summary>
        /// <remarks>
        /// Inherit this class to implement component token signing. (c) 2003 Integrated Scientific
        /// Resources, Inc. All rights reserved. <para>
        /// Licensed under The MIT License.</para><para>
        /// David, 2003-06-21, 1.0.1267.x. </para>
        /// </remarks>
    public abstract class TokenSignBase : IDisposable
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Constructs this class. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        protected TokenSignBase() : base()
        {
            this.CurrentFileVersion = new Version(1, 0);
            this.ResourceName = "Info";
            this.ResourceName = "Info";
        }

        /// <summary> Calls <see cref="M:Dispose(Boolean Disposing)"/> to cleanup. </summary>
        /// <remarks>
        /// Do not make this method Overridable (virtual) because a derived class should not be able to
        /// override this method.
        /// </remarks>
        public void Dispose()
        {

            // Do not change this code.  Put cleanup code in Dispose(Boolean) below.

            // this disposes all child classes.
            this.Dispose(true);

            // Take this object off the finalization(Queue) and prevent finalization code 
            // from executing a second time.
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// Gets the dispose status sentinel of the base class.  This applies to the derived class
        /// provided proper implementation.
        /// </summary>
        /// <value> The is disposed. </value>
        protected bool IsDisposed { get; set; }

        /// <summary>
        /// Releases the unmanaged resources used by the <see cref="T:System.Windows.Forms.Control" />
        /// and its child controls and optionally releases the managed resources.
        /// </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <param name="disposing"> <c>True</c> to release both managed and unmanaged resources;
        /// <c>False</c> to release only unmanaged
        /// resources when called from the runtime
        /// finalize. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification = "Exception is published")]
        [DebuggerNonUserCode()]
        protected virtual void Dispose(bool disposing)
        {
            try
            {
                if (!this.IsDisposed && disposing)
                {
                    // Free managed resources when explicitly called
                    if ( this.XmlSign is object)
                    {
                        this.XmlSign.Dispose();
                        this.XmlSign = null;
                    }
                }
            }
            finally
            {
                this.IsDisposed = true;
            }
        }

        /// <summary>
        /// This destructor will run only if the Dispose method does not get called. It gives the base
        /// class the opportunity to finalize. Do not provide destructors in types derived from this
        /// class.
        /// </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        ~TokenSignBase()
        {
            // Do not re-create Dispose clean-up code here.
            // Calling Dispose(false) is optimal for readability and maintainability.
            this.Dispose(false);
        }

        #endregion

        #region " OPEN AND CLOSE "

        /// <summary> Gets the opened status of the instance. </summary>
        /// <value>
        /// <c>IsOpen</c> is a <see cref="Boolean"/> property that is True if the instance is open.
        /// </value>
        public bool IsOpen => this.XmlSign is object && this.XmlSign.IsOpen;

        /// <summary> opens this instance. </summary>
        /// <remarks> Use this method to open the instance.  The method Returns <c>True</c> if success or false if
        /// it failed opening the instance. </remarks>
        public virtual void Open()
        {
            try
            {
                // instantiate and open the XML signature object
                this.XmlSign = new XmlSign();
                this.XmlSign.Open();
            }
            catch
            {
                // close to meet strong guarantees
                try
                {
                    this.Close();
                }
                finally
                {
                }

                throw;
            }
        }

        /// <summary> Closes the instance. </summary>
        public virtual void Close()
        {
            // close the signature object
            if ( this.IsOpen )
            {
                this.XmlSign.Close();
                this.XmlSign = null;
            }
        }

        #endregion

        #region " XML SIGNATURE: Does the hard work "

        /// <summary> Gets or sets the XML signing object. </summary>
        /// <remarks> Use this property to get reference to the XML signature for the token. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <value>
        /// an <see cref="isr.IO.Cryptography.XmlSign"/> property that can be read from (read only).
        /// </value>
        public XmlSign XmlSign { get; private set; }

        #endregion

        #region " CREATE AND SAVE "

        /// <summary> Gets or sets the filename of the full file. </summary>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <value> The filename of the full file. </value>
        public string FullFileName { get; private set; }

        /// <summary> Gets or sets the identifier of the resource. </summary>
        /// <remarks> Must be set -- no default provided. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <value> The identifier of the resource. </value>
        public string ResourceId { get; set; }

        /// <summary> Gets or sets the name of the resource. </summary>
        /// <remarks> Defaults to 'Info'. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <value> The name of the resource. </value>
        public string ResourceName { get; set; }

        /// <summary> Gets or sets the resource namespace. </summary>
        /// <remarks> Defaults to 'Value'. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <value> The resource namespace. </value>
        public string ResourceNamespace { get; set; }

        /// <summary> Creates and signs the resource document. </summary>
        /// <remarks> Use this method to create a signed resource document. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="fullFileName">      The file name and path. </param>
        /// <param name="resourceId">        The identifier of the resource. </param>
        /// <param name="resourceName">      Name of the resource. </param>
        /// <param name="resourceNamespace"> The resource namespace. </param>
        public void CreateAndSignResource(string fullFileName, string resourceId, string resourceName, string resourceNamespace)
        {
            if (string.IsNullOrWhiteSpace(fullFileName))
                throw new ArgumentNullException(nameof(fullFileName));
            if (string.IsNullOrWhiteSpace(resourceId))
                throw new ArgumentNullException(nameof(resourceId));
            if (string.IsNullOrWhiteSpace(resourceName))
                throw new ArgumentNullException(nameof(resourceName));
            if (string.IsNullOrWhiteSpace(resourceNamespace))
                throw new ArgumentNullException(nameof(resourceNamespace));
            this.FullFileName = fullFileName;
            this.ResourceId = resourceId;

            // create the resource (XML document) that will contain the signed data nodes.
            var xmlDoc = XmlSign.CreateResource(resourceName, resourceNamespace);

            // clear the token dictionary
            this.ClearKnownState();

            // Add tokens to the token dictionary
            this.AddTokens();

            // adds the tokens to the resource document
            this.PopulateResource(xmlDoc);

            // adds the resource to the signed document
            this.XmlSign.AddResource(xmlDoc, resourceId);

            // generate the signature
            this.XmlSign.GenerateSignature();
        }

        /// <summary> Creates and signs the resource document. </summary>
        /// <remarks> Use this method to create a signed resource document. </remarks>
        /// <param name="fullFileName">      The file name and path. </param>
        /// <param name="resourceName">      Name of the resource. </param>
        /// <param name="resourceNamespace"> The resource namespace. </param>
        public void CreateAndSignResource(string fullFileName, string resourceName, string resourceNamespace)
        {
            this.CreateAndSignResource(fullFileName, this.ResourceId, resourceName, resourceNamespace);
        }

        /// <summary> Creates and sign resource. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <param name="fullFileName"> Filename of the full file. </param>
        public void CreateAndSignResource(string fullFileName)
        {
            this.CreateAndSignResource(fullFileName, this.ResourceId, this.ResourceName, this.ResourceNamespace );
        }

        /// <summary> Adds tokens to the token dictionary. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        public abstract void AddTokens();

        /// <summary> Saves the signed resource to the <see cref="FullFileName"/> . </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        public void Save()
        {
            this.XmlSign.WriteSignature( this.FullFileName );
        }

        #endregion

        #region " READ and PARSE "

        /// <summary> Verifies the trusted signed file. </summary>
        /// <remarks> Use this method to verify the signed resource file. </remarks>
        /// <param name="fullFileName"> The file name and path. </param>
        /// <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        public bool VerifyTrustedFile(string fullFileName)
        {
            this.FullFileName = fullFileName;
            return XmlSign.IsVerifySignatureFile(fullFileName);
        }

        /// <summary> Verify trust file. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <param name="fullFileName"> The file name and path. </param>
        /// <param name="publicKeyXml"> The public key XML. </param>
        /// <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        public bool VerifyTrustFile(string fullFileName, string publicKeyXml)
        {
            this.FullFileName = fullFileName;
            return XmlSign.IsVerifySignatureFile(fullFileName, publicKeyXml);
        }

        /// <summary> Reads and parses the signed resource. </summary>
        /// <remarks> Use this method to read the signed resource. </remarks>
        public virtual void ReadParse()
        {
            this.ClearKnownState();
            this.FullFileName = this.FullFileName;
            this.XmlSign.ReadSignature( this.FullFileName );
            // get the resource ID from the signed file.
            this.ResourceId = this.XmlSign.FetchResourceId();
            this.ResourceName = this.XmlSign.FetchResourceName();
            var resourceNode = this.XmlSign.FetchResourceDataNode( this.ResourceName );
            // add all the tokens to the token dictionary.
            foreach (System.Xml.XmlNode node in resourceNode.ChildNodes)
                this.AddToken(node.Name, node.ChildNodes[0].Value);
        }

        /// <summary> Verify, trust, read and parse. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <param name="fullFileName"> The file name and path. </param>
        /// <param name="publicKeyXml"> The public key XML. </param>
        /// <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        public bool VerifyTrustReadParse(string fullFileName, string publicKeyXml)
        {
            if ( this.VerifyTrustFile(fullFileName, publicKeyXml))
            {
                this.ReadParse();
                return true;
            }
            else
            {
                return false;
            }
        }

        #endregion

        #region " RESOURCE BUILDER "

        /// <summary> The current file version. </summary>

        /// <summary> Gets or sets the current file version. </summary>
        /// <value> The current file version. </value>
        public Version CurrentFileVersion { get; protected set; }

        /// <summary> Clears the known state. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        protected void ClearKnownState()
        {
            this.TokenDictionary = new Dictionary<string, string>();
        }

        /// <summary> Gets or sets a dictionary of tokens. </summary>
        /// <value> A Dictionary of tokens. </value>
        protected IDictionary<string, string> TokenDictionary { get; private set; }

        /// <summary> Gets the token lists in this collection. </summary>
        /// <remarks> David, 2020-09-07. </remarks>
        /// <returns> The token list. </returns>
        public ICollection<KeyValuePair<string, string>> GetTokenList()
        {
            return this.TokenDictionary;
        }

        /// <summary> Gets the tokens. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <returns> The tokens. </returns>
        public string GetTokens()
        {
            var builder = new System.Text.StringBuilder();
            foreach (KeyValuePair<string, string> token in this.TokenDictionary )
                _ = builder.AppendLine( $"{token.Key},{token.Value}" );
            return builder.ToString();
        }

        /// <summary> Adds a token to 'tokenValue'. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <param name="tokenName">  Name of the token. </param>
        /// <param name="tokenValue"> The token value. </param>
        public void AddToken(string tokenName, string tokenValue)
        {
            if ( this.TokenDictionary.ContainsKey(tokenName))
            {
                this.TokenDictionary[tokenName] = tokenValue;
            }
            else
            {
                this.TokenDictionary.Add(tokenName, tokenValue);
            }
        }

        /// <summary> Populate resource. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="xmlDoc"> The XML document. </param>
        public void PopulateResource(System.Xml.XmlDocument xmlDoc)
        {
            if (xmlDoc is null)
                throw new ArgumentNullException(nameof(xmlDoc));
            this.AddToken(BaseTokenName.FileName.ToString(), My.MyProject.Computer.FileSystem.GetFileInfo( this.FullFileName ).Name);
            this.AddToken(BaseTokenName.FileVersion.ToString(), this.CurrentFileVersion.ToString());
            foreach (KeyValuePair<string, string> token in this.TokenDictionary )
                XmlSign.AddResourceToken(xmlDoc, token.Key, token.Value);
        }

        #endregion

        #region " RESOURCE VALUE VERIFICATIONS "

        /// <summary> Verify resource token. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="fullFileName"> The file name and path. </param>
        /// <param name="publicKeyXml"> The public key XML. </param>
        /// <param name="tokenName">    Name of the token. </param>
        /// <param name="tokenValue">   The token value. </param>
        /// <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        public bool VerifyResourceToken(string fullFileName, string publicKeyXml, string tokenName, string tokenValue)
        {
            return string.IsNullOrWhiteSpace(fullFileName)
                ? throw new ArgumentNullException(nameof(fullFileName))
                : string.IsNullOrWhiteSpace(tokenName)
                ? throw new ArgumentNullException(nameof(tokenName))
                : string.IsNullOrWhiteSpace(tokenValue)
                ? throw new ArgumentNullException(nameof(tokenValue))
                : this.VerifyTrustReadParse(fullFileName, publicKeyXml) && this.TokenDictionary.ContainsKey(tokenName) && string.Equals(tokenValue, this.TokenDictionary[tokenName], StringComparison.Ordinal);
        }

        /// <summary> Verify resource token. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="tokenName">  Name of the token. </param>
        /// <param name="tokenValue"> The token value. </param>
        /// <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        protected bool VerifyResourceToken(string tokenName, string tokenValue)
        {
            return string.IsNullOrWhiteSpace(tokenName)
                ? throw new ArgumentNullException(nameof(tokenName))
                : string.IsNullOrWhiteSpace(tokenValue)
                ? throw new ArgumentNullException(nameof(tokenValue))
                : this.TokenDictionary.ContainsKey(tokenName) && string.Equals(tokenValue, this.TokenDictionary[tokenName], StringComparison.Ordinal);
        }

        /// <summary> Verify resource token. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="tokenName">  Name of the token. </param>
        /// <param name="tokenValue"> The token value. </param>
        /// <param name="e">          Cancel details event information. </param>
        /// <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        protected bool VerifyResourceToken(string tokenName, string tokenValue, Core.ActionEventArgs e)
        {
            if (e is null)
                throw new ArgumentNullException(nameof(e));
            if (!this.VerifyResourceToken(tokenName, tokenValue))
            {
                if ( this.TokenDictionary.ContainsKey(tokenName))
                {
                    e.RegisterFailure($"{tokenName} {this.TokenDictionary[tokenName]} different from {tokenValue}");
                }
                else
                {
                    e.RegisterFailure($"{this.FullFileName} does not contain the {tokenName} value");
                }
            }

            return !e.Failed;
        }

        /// <summary> Verifies all tokens. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <param name="e"> Cancel details event information. </param>
        /// <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        public abstract bool VerifyTokens(Core.ActionEventArgs e);

        #endregion

    }

        /// <summary> Values that represent base token names. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
    public enum BaseTokenName
    {

        /// <summary> Not specified. </summary>
        [Description("Not specified")]
        None,

        /// <summary> Name of the file name token. </summary>
        [Description("File Version")]
        FileName,

        /// <summary> Name of the file name token. </summary>
        [Description("File Version")]
        FileVersion
    }
}