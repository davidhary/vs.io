using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using Microsoft.VisualBasic.CompilerServices;

namespace isr.IO.NetList
{

    /// <summary>
    /// This is the <see cref="isr.IO.NetList.Cable">Cable</see> class of the net list reader class
    /// library. A cable consists of a set of connector and nodes defining how the connector pins are
    /// wired.
    /// </summary>
    /// <remarks>
    /// (c) 2007 Integrated Scientific Resources<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2007-12-18, 1.2.2908. </para>
    /// </remarks>
    public class Cable : IDisposable
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Constructs this class. </summary>
        /// <remarks> David, 2020-10-08. </remarks>

        // instantiate the class
        public Cable() : base()
        {
            this._Nodes = new Dictionary<int, Node>();
            this._Connectors = new Dictionary<string, Connector>();
        }

        /// <summary> Calls <see cref="M:Dispose(Boolean Disposing)"/> to cleanup. </summary>
        /// <remarks>
        /// Do not make this method Overridable (virtual) because a derived class should not be able to
        /// override this method.
        /// </remarks>
        public void Dispose()
        {

            // Do not change this code.  Put cleanup code in Dispose(Boolean) below.

            // this disposes all child classes.
            this.Dispose(true);

            // Take this object off the finalization(Queue) and prevent finalization code 
            // from executing a second time.
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// Gets or sets the dispose status sentinel of the base class.  This applies to the derived
        /// class provided proper implementation.
        /// </summary>
        /// <value> The is disposed. </value>
        protected bool IsDisposed { get; private set; }

        /// <summary>
        /// Releases the unmanaged resources used by the <see cref="T:System.Windows.Forms.Control" />
        /// and its child controls and optionally releases the managed resources.
        /// </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <param name="disposing"> <c>True</c> to release both managed and unmanaged resources;
        /// <c>False</c> to release only unmanaged
        /// resources when called from the runtime
        /// finalize. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification = "Exception is published")]
        [DebuggerNonUserCode()]
        protected virtual void Dispose(bool disposing)
        {
            try
            {
                if (!this.IsDisposed && disposing)
                {
                    // Free managed resources when explicitly called
                    this._Connectors = null;
                    this._Nodes = null;
                    this._Wires = null;
                }
            }
            finally
            {
                this.IsDisposed = true;
            }
        }

        /// <summary>
        /// This destructor will run only if the Dispose method does not get called. It gives the base
        /// class the opportunity to finalize. Do not provide destructors in types derived from this
        /// class.
        /// </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        ~Cable()
        {
            // Do not re-create Dispose clean-up code here.
            // Calling Dispose(false) is optimal for readability and maintainability.
            this.Dispose(false);
        }

        #endregion

        #region " METHODS "

        /// <summary>
        /// Adds a <see cref="Connector">connector</see> to the <see cref="Cable">cable</see>.
        /// </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <param name="record"> A record from the net list file. </param>
        /// <returns> A Connector. </returns>
        public Connector AddConnector(string record)
        {
            Connector newConnector = null;
            try
            {
                newConnector = new Connector();
                if (newConnector.Parse(record))
                {
                    if (!this._Connectors.ContainsKey(newConnector.Number))
                    {
                        this._Connectors.Add(newConnector.Number, newConnector);
                    }
                }
            }
            catch
            {
                if (newConnector is object)
                    newConnector.Dispose();
                throw;
            }

            return newConnector;
        }


        /// <summary> Adds a <see cref="Node">Node</see> to the <see cref="Cable">cable</see>. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <param name="record"> A record from the net list file. </param>
        /// <returns> A Node. </returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "<Pending>" )]
        public Node AddNode(string record)
        {
            Node newNode = null;
            try
            {
                newNode = new Node();
                if (newNode.Parse(record))
                {
                    this._Nodes.Add(newNode.Number, newNode);
                }
            }
            catch
            {
                if (newNode is object)
                    newNode.Dispose();
            }

            return newNode;
        }

        /// <summary> Adds <see cref="Wire">Wires</see> to the <see cref="Cable">cable</see>. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="node">                  A record from the net list file. </param>
        /// <param name="includeRedundantWires"> True to add also redundant wires. </param>
        /// <returns> True if it succeeds, false if it fails. </returns>
        public bool AddWires(Node node, bool includeRedundantWires)
        {
            if (node is null)
                throw new ArgumentNullException(nameof(node));
            if (node.Pins.Count > 1)
            {
                for (int i = 1, loopTo = node.Pins.Count - 1; i <= loopTo; i++)
                {
                    int lastPin = i;
                    if (includeRedundantWires)
                    {
                        lastPin = node.Pins.Count - 1;
                    }

                    for (int j = i, loopTo1 = lastPin; j <= loopTo1; j++)
                    {
                        Wire newWire = null;
                        try
                        {
                            newWire = new Wire(node.Pins[i - 1], node.Pins[j]) { IsPrimary = i == j };
                            Debug.Assert(!this.Exists(newWire), "Wire exists");
                            if (!this.Exists(newWire))
                            {
                                this._Wires.Add(newWire.Key, newWire);
                            }
                        }
                        catch
                        {
                            if (newWire is object)
                                newWire.Dispose();
                            throw;
                        }
                    }
                }
            }

            return true;
        }

        /// <summary>
        /// Adds a <see cref="Pin">Pin</see> to the <see cref="Connector">Connector</see>.
        /// </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
        /// null. </exception>
        /// <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
        /// <param name="pinNumber">       The pin number. </param>
        /// <param name="connectorNumber"> . </param>
        /// <returns> A new or exiting pin. </returns>
        public Pin AddPin(string pinNumber, string connectorNumber)
        {
            if (string.IsNullOrWhiteSpace(pinNumber))
                throw new ArgumentNullException(nameof(pinNumber));
            if (string.IsNullOrWhiteSpace(connectorNumber))
                throw new ArgumentNullException(nameof(connectorNumber));
            if (!this.ConnectorExists(connectorNumber))
            {
                string message = "Failed adding pin '{0}'. Connector number '{1}' not found";
                message = string.Format(System.Globalization.CultureInfo.CurrentCulture, message, pinNumber, connectorNumber);
                throw new InvalidOperationException(message);
            }

            // add the pin to the connector.
            return this.SelectConnector(connectorNumber).AddPin(pinNumber);
        }

        /// <summary> Returns true of the connector already exists. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <param name="connectorNumber"> . </param>
        /// <returns> True if it succeeds, false if it fails. </returns>
        public bool ConnectorExists(string connectorNumber)
        {
            return this._Connectors is object && this._Connectors.ContainsKey(connectorNumber);
        }

        /// <summary> Returns true of the connector already exists. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <param name="pin"> . </param>
        /// <returns> True if it succeeds, false if it fails. </returns>
        public bool ConnectorExists(Pin pin)
        {
            return pin is object && this._Connectors is object && this._Connectors.ContainsKey(pin.ConnectorNumber);
        }

        /// <summary> Returns true of the connector already exists. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <param name="connector"> . </param>
        /// <returns> True if it succeeds, false if it fails. </returns>
        public bool Exists(Connector connector)
        {
            return connector is object && this.ConnectorExists(connector.Number);
        }

        /// <summary> Returns true of the node already exists. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <param name="node"> . </param>
        /// <returns> True if it succeeds, false if it fails. </returns>
        public bool Exists(Node node)
        {
            return node is object && this.NodeExists(node.Number);
        }

        /// <summary> Returns true of the node already exists. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <param name="nodeNumber"> . </param>
        /// <returns> True if it succeeds, false if it fails. </returns>
        public bool NodeExists(int nodeNumber)
        {
            return this._Nodes is object && this._Nodes.ContainsKey(nodeNumber);
        }

        /// <summary> Returns true of the wire already exists. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <param name="wire"> . </param>
        /// <returns> True if it succeeds, false if it fails. </returns>
        public bool Exists(Wire wire)
        {
            return wire is object && this.WireExists(wire.Key);
        }

        /// <summary> Returns the list of pin keys defined for this cable. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <returns> A String() </returns>
        public string[] PinKeys()
        {
            if ( this.HasPins )
            {
                var pinArray = Array.Empty<string>();
                foreach (Connector connector in this._Connectors.Values)
                {
                    var keys = connector.PinKeys();
                    int currentLength = pinArray.Length;
                    Array.Resize(ref pinArray, pinArray.Length + keys.Length);
                    keys.CopyTo(pinArray, currentLength);
                }

                return pinArray;
            }
            else
            {
                return Array.Empty<string>();
            }
        }

        /// <summary> The active node. </summary>
        private Node _ActiveNode;

        /// <summary> Parses each record based on the reading state. </summary>
        /// <remarks>
        /// The sequence of reading the net list proceeds as follows:<para>
        /// 1.  Look for the connector list.  This is signified by the &lt;&lt;&lt; Component List &gt;
        /// &gt;&gt;</para><para>
        /// 2. Read the connectors.</para><para>
        /// 3. Look for wire list signified by &lt;&lt;&lt; Wire List &gt;&gt;&gt;</para><para>
        /// 4. Repeat looking for nodes.  Each nodes starts with a [</para><para>
        /// 5. Fore each node read all the wires adding pins as necessary.</para><para>
        /// </para>
        /// </remarks>
        /// <param name="record">       . </param>
        /// <param name="readingState"> State of the reading. </param>
        /// <returns> A ReadingState. </returns>
        private ReadingState Parse(string record, ReadingState readingState)
        {
            if (readingState == ReadingState.Done)
            {
            }

            // if done do nothing.

            else if (readingState == ReadingState.Failed)
            {
            }

            // if failed do nothing.

            else if (readingState == ReadingState.LookingForConnectorList)
            {

                // look for the connector header
                if (record.IndexOf(_ComponentListHeader, StringComparison.OrdinalIgnoreCase) >= 0)
                {
                    readingState = ReadingState.ReadingConnectors;
                }
            }
            else if (readingState == ReadingState.LookingForNode)
            {

                // look for the node number prefix
                if (record.IndexOf(_NodeNumberPrfix, StringComparison.OrdinalIgnoreCase) >= 0)
                {
                    readingState = ReadingState.ReadingNode;
                    this._ActiveNode = this.AddNode(record);
                }
            }
            else if (readingState == ReadingState.LookingForWireList)
            {

                // look for the wire list header
                if (record.IndexOf(_WireListHeader, StringComparison.OrdinalIgnoreCase) >= 0)
                {
                    readingState = ReadingState.LookingForNode;
                }
            }
            else if (readingState == ReadingState.None)
            {
                return this.Parse(record, ReadingState.LookingForConnectorList);
            }
            else if (readingState == ReadingState.ReadingConnectors)
            {
                if (record.Length > 2)
                {

                    // add a connector
                    _ = this.AddConnector( record );
                }
                else
                {

                    // if done reading connectors then look for wire list.
                    readingState = ReadingState.LookingForWireList;
                }
            }
            else if (readingState == ReadingState.ReadingNode)
            {
                if (record.Length > 2)
                {

                    // add a pin
                    _ = this._ActiveNode.AddPin( this, record );
                }
                else
                {

                    // if done reading node look for the next node
                    readingState = ReadingState.LookingForNode;
                }
            }
            else
            {
                Debug.Assert(!Debugger.IsAttached, "Unhandled reading state");
            }

            return readingState;
        }

        /// <summary> Add pins to all connectors based on the number of pins specified. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <returns> True if it succeeds, false if it fails. </returns>
        public bool PopulateConnectors()
        {
            foreach (Connector existingConnector in this._Connectors.Values)
                _ = existingConnector.Populate();
            return true;
        }

        /// <summary> Creates the collection of wires. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <param name="includeRedundantWires"> True to add also redundant wires. </param>
        /// <returns> True if it succeeds, false if it fails. </returns>
        public bool PopulateWires(bool includeRedundantWires)
        {
            this._Wires = new Dictionary<string, Wire>();
            foreach (Node newNode in this._Nodes.Values)
                _ = this.AddWires( newNode, includeRedundantWires );
            return true;
        }

        /// <summary> Reads the net list connectors from the file. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <param name="filePathName"> . </param>
        /// <returns> True if it succeeds, false if it fails. </returns>
        public bool ReadNetListConnectors(string filePathName)
        {
            bool success;
            if (System.IO.File.Exists(filePathName))
            {
                string fileContents;
                fileContents = My.MyProject.Computer.FileSystem.ReadAllText(filePathName);
                if (string.IsNullOrWhiteSpace(fileContents))
                {
                    success = false;
                }
                else
                {
                    var rows = fileContents.Split(Conversions.ToChar(Environment.NewLine));
                    if (rows.Length < 3)
                    {
                        success = false;
                    }
                    else
                    {
                        var readingState = ReadingState.None;
                        foreach (string row in rows)
                        {
                            string record = row.Trim(Environment.NewLine.ToCharArray()).Trim();
                            readingState = this.Parse(record, readingState);
                            // stop reading the first time we are looking for a node of wires
                            if (readingState == ReadingState.Failed || readingState == ReadingState.LookingForNode)
                            {
                                break;
                            }
                        }

                        if (readingState != ReadingState.Failed)
                        {
                            readingState = ReadingState.Done;
                        }

                        return readingState == ReadingState.Done && this._Connectors.Any();
                    }
                }
            }
            else
            {
                success = false;
            }

            return success;
        }

        /// <summary> Reads a net list from the file. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <param name="filePathName"> . </param>
        /// <returns> True if it succeeds, false if it fails. </returns>
        public bool ReadNetList(string filePathName)
        {
            bool success;
            if (System.IO.File.Exists(filePathName))
            {
                string fileContents;
                fileContents = My.MyProject.Computer.FileSystem.ReadAllText(filePathName);
                if (string.IsNullOrWhiteSpace(fileContents))
                {
                    success = false;
                }
                else
                {
                    var rows = fileContents.Split(Conversions.ToChar(Environment.NewLine));
                    if (rows.Length < 3)
                    {
                        success = false;
                    }
                    else
                    {
                        var readingState = ReadingState.None;
                        foreach (string row in rows)
                        {
                            string record = row.Trim(Environment.NewLine.ToCharArray()).Trim();
                            readingState = this.Parse(record, readingState);
                            if (readingState == ReadingState.Failed)
                            {
                                break;
                            }
                        }

                        if (readingState != ReadingState.Failed)
                        {
                            readingState = ReadingState.Done;
                        }

                        return readingState == ReadingState.Done && this._Connectors.Any() && this._Nodes.Any();
                    }
                }
            }
            else
            {
                success = false;
            }

            return success;
        }

        /// <summary> Rolls back cable assembly by clearing all the public keys. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        public void Rollback()
        {
            this.PublicKey = 0;
            foreach (Connector connector in this._Connectors.Values)
                connector.Rollback();
            foreach (Node node in this._Nodes.Values)
                node.Rollback();
        }

        /// <summary> Selects a Connector from the Connectors collection. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="connectorNumber"> . </param>
        /// <returns> A Connector. </returns>
        public Connector SelectConnector(string connectorNumber)
        {
            if (string.IsNullOrWhiteSpace(connectorNumber))
                throw new ArgumentNullException(nameof(connectorNumber));
            var newConnector = this._Connectors[connectorNumber];
            return newConnector;
        }

        /// <summary> Selects a Node from the Nodes collection. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <param name="nodeNumber"> . </param>
        /// <returns> A Node. </returns>
        public Node SelectNode(int nodeNumber)
        {
            var newNode = this._Nodes[nodeNumber];
            return newNode;
        }

        /// <summary> Returns true of the wire already exists. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <param name="wireId"> . </param>
        /// <returns> True if it succeeds, false if it fails. </returns>
        public bool WireExists(string wireId)
        {
            return this._Wires is object && this._Wires.ContainsKey(wireId);
        }

        /// <summary> Returns the list of wires as keys.  This can be displayed in a list box. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <returns> A String() </returns>
        public string[] WireKeys()
        {
            return this._Wires is null ? Array.Empty<string>() : this._Wires.Keys.ToArray();
        }

        #endregion

        #region " PROPERTIES "

        /// <summary>
        /// Gets the connector list header string.
        /// </summary>
        private const string _ComponentListHeader = "<<< Component List >>>";

        /// <summary>
        /// Gets the wire list header string.
        /// </summary>
        private const string _WireListHeader = "<<< Wire List >>>";

        /// <summary>
        /// Gets the first character for a new node record.
        /// </summary>
        private const string _NodeNumberPrfix = "[";

        /// <summary> Gets the cable name. </summary>
        /// <value> The name. </value>
        public string Name { get; set; }

        /// <summary> Gets the cable number. </summary>
        /// <value> The number. </value>
        public string Number { get; set; }

        /// <summary> Gets the public key reference from the database. </summary>
        /// <value> The public key. </value>
        public int PublicKey { get; set; }

        /// <summary> The connectors. </summary>
        private Dictionary<string, Connector> _Connectors;

        /// <summary> Gets the collection of <see cref="Connector">Connectors</see>. </summary>
        /// <value> The connectors. </value>
        public System.Collections.ObjectModel.ReadOnlyCollection<Connector> Connectors => new System.Collections.ObjectModel.ReadOnlyCollection<Connector>( this._Connectors.Values.ToList() );

        /// <summary> Gets the condition telling if the cable has connectors. </summary>
        /// <value> The has connectors. </value>
        public bool HasConnectors => this._Connectors is object && this._Connectors.Any();

        /// <summary> Gets the condition telling if the cable has pins. </summary>
        /// <value> The has pins. </value>
        public bool HasPins => this.HasConnectors && this._Connectors.Values.First().HasPins;

        /// <summary> Gets the condition telling if the cable has wires. </summary>
        /// <value> The has wires. </value>
        public bool HasWires => this._Wires is object && this._Wires.Any();

        /// <summary> The nodes. </summary>
        private Dictionary<int, Node> _Nodes;

        /// <summary> Gets the collection of <see cref="Node">Nodes</see>. </summary>
        /// <value> The nodes. </value>
        public System.Collections.ObjectModel.ReadOnlyCollection<Node> Nodes => new System.Collections.ObjectModel.ReadOnlyCollection<Node>( this._Nodes.Values.ToList() );

        /// <summary> Gets the collection of <see cref="Pin">pins</see>. </summary>
        /// <value> The pins. </value>
        public System.Collections.ObjectModel.ReadOnlyCollection<Pin> Pins
        {
            get
            {
                var pinsDictionary = new Dictionary<string, Pin>();
                foreach (Connector Connector in this._Connectors.Values)
                {
                    foreach (Pin Pin in Connector.Pins)
                        pinsDictionary.Add(Pin.Key, Pin);
                }

                return new System.Collections.ObjectModel.ReadOnlyCollection<Pin>(pinsDictionary.Values.ToList());
            }
        }

        /// <summary> The wires. </summary>
        private Dictionary<string, Wire> _Wires;

        /// <summary> Gets the collection of <see cref="Wire">Wires</see>. </summary>
        /// <value> The wires. </value>
        public System.Collections.ObjectModel.ReadOnlyCollection<Wire> Wires => new System.Collections.ObjectModel.ReadOnlyCollection<Wire>( this._Wires.Values.ToList() );

        #endregion

        #region " CREATABLE "

        /// <summary> The non creatable reason. </summary>

        /// <summary> Gets the reason why this connector is not creatable. </summary>
        /// <value> The non creatable reason. </value>
        public string NonCreatableReason { get; private set; }

        /// <summary> Returns <c>True</c> if the connector is ready to populate pins. </summary>
        /// <value> The creatable. </value>
        public bool Creatable
        {
            get
            {
                this.NonCreatableReason = string.Empty;
                foreach (Connector connector in this.Connectors )
                {
                    if (!connector.Creatable)
                    {
                        this.NonCreatableReason = string.Format(System.Globalization.CultureInfo.CurrentCulture, "Connector number {0} is not creatable because {1}", connector.Number, connector.NonCreatableReason);
                        return false;
                    }
                }

                return true;
            }
        }

        #endregion

    }
}
