using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

namespace isr.IO.NetList
{

    /// <summary>
    /// This is the <see cref="isr.IO.NetList.Node">Node</see> class of the net list reader class
    /// library. A node consists of a collection of connected pins.
    /// </summary>
    /// <remarks>
    /// (c) 2007 Integrated Scientific Resources, Inc. All rights reserved. <para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2007-12-18, 1.2.2908.x. </para>
    /// </remarks>
    public class Node : IDisposable
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Constructs this class. </summary>
        /// <remarks> David, 2020-10-08. </remarks>

        // instantiate the class
        public Node() : base()
        {
            this._Pins = new Dictionary<string, Pin>();
        }

        /// <summary> Calls <see cref="M:Dispose(Boolean Disposing)"/> to cleanup. </summary>
        /// <remarks>
        /// Do not make this method Overridable (virtual) because a derived class should not be able to
        /// override this method.
        /// </remarks>
        public void Dispose()
        {

            // Do not change this code.  Put cleanup code in Dispose(Boolean) below.

            // this disposes all child classes.
            this.Dispose(true);

            // Take this object off the finalization(Queue) and prevent finalization code 
            // from executing a second time.
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// Gets or sets the dispose status sentinel of the base class.  This applies to the derived
        /// class provided proper implementation.
        /// </summary>
        /// <value> The is disposed. </value>
        protected bool IsDisposed { get; private set; }

        /// <summary>
        /// Releases the unmanaged resources used by the <see cref="T:System.Windows.Forms.Control" />
        /// and its child controls and optionally releases the managed resources.
        /// </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <param name="disposing"> <c>True</c> to release both managed and unmanaged resources;
        /// <c>False</c> to release only unmanaged
        /// resources when called from the runtime
        /// finalize. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification = "Exception is published")]
        [DebuggerNonUserCode()]
        protected virtual void Dispose(bool disposing)
        {
            try
            {
                if (!this.IsDisposed && disposing)
                {
                    this._Pins = null;
                }
            }
            finally
            {
                this.IsDisposed = true;
            }
        }

        /// <summary>
        /// This destructor will run only if the Dispose method does not get called. It gives the base
        /// class the opportunity to finalize. Do not provide destructors in types derived from this
        /// class.
        /// </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        ~Node()
        {
            // Do not re-create Dispose clean-up code here.
            // Calling Dispose(false) is optimal for readability and maintainability.
            this.Dispose(false);
        }

        #endregion

        #region " METHODS "

        /// <summary>
        /// Adds a new <see cref="Pin">Pin</see> to the <see cref="Node">Node</see>
        /// or selects an existing pin.
        /// </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="cable">  . </param>
        /// <param name="record"> . </param>
        /// <returns> A reference to the pin. </returns>
        public Pin AddPin(Cable cable, string record)
        {
            if (cable is null)
                throw new ArgumentNullException(nameof(cable));
            if (string.IsNullOrWhiteSpace(record))
                throw new ArgumentNullException(nameof(record));
            Pin newPin = null;
            try
            {
                newPin = new Pin();
                if (newPin.Parse(record))
                {

                    // add the pin to the cable
                    newPin = cable.AddPin(newPin.Number, newPin.ConnectorNumber);

                    // set reference to the node number.
                    newPin.NodeNumber = this.Number;

                    // add the pin to the node
                    if (!this.Exists(newPin))
                    {
                        this._Pins.Add(newPin.Key, newPin);
                    }

                    newPin = this._Pins[newPin.Key];
                }
            }
            catch
            {
                if (newPin is object)
                    newPin.Dispose();
                throw;
            }

            return newPin;
        }

        /// <summary> Returns true of the pin already exists. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <param name="pin"> . </param>
        /// <returns> True if it succeeds, false if it fails. </returns>
        public bool Exists(Pin pin)
        {
            return pin is object && this._Pins is object && this._Pins.ContainsKey(pin.Number);
        }

        /// <summary> Parses the node record. </summary>
        /// <remarks>
        /// The node record includes the following information:<para>
        /// 0         1
        /// 01234567890123
        /// [00001] N00469 Number (1, 5)</para><para>
        /// Name (8, EOL)</para><para>
        /// </para>
        /// </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="record"> A record from the net list file. </param>
        /// <returns> True if it succeeds, false if it fails. </returns>
        public bool Parse(string record)
        {
            if (string.IsNullOrWhiteSpace(record))
                throw new ArgumentNullException(nameof(record));
            this.Number = int.Parse(record.Substring(1, 5).Trim(), System.Globalization.CultureInfo.InvariantCulture);
            this.Name = record.Substring(8).Trim();
            return this.Number > 0 && !string.IsNullOrWhiteSpace( this.Name );
        }

        /// <summary> Rolls back cable assembly by clearing all the public keys. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        public void Rollback()
        {
            foreach (Pin pin in this._Pins.Values)
                pin.PublicKey = 0;
        }

        /// <summary> Selects a pin from the pins collection. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="pinKey"> . </param>
        /// <returns> A Pin. </returns>
        public Pin SelectPin(string pinKey)
        {
            if (string.IsNullOrWhiteSpace(pinKey))
                throw new ArgumentNullException(nameof(pinKey));
            var newPin = this._Pins[pinKey];
            return newPin;
        }

        #endregion

        #region " PROPERTIES "

        /// <summary> Gets the node name. </summary>
        /// <value> The name. </value>
        public string Name { get; set; }

        /// <summary> Gets the node number. </summary>
        /// <value> The number. </value>
        public int Number { get; set; }

        /// <summary> Gets the public key reference from the database. </summary>
        /// <value> The public key. </value>
        public int PublicKey { get; set; }

        /// <summary> The pins. </summary>
        private Dictionary<string, Pin> _Pins;

        /// <summary> Gets the collection of <see cref="Pin">Pins</see>. </summary>
        /// <value> The pins. </value>
        public System.Collections.ObjectModel.ReadOnlyCollection<Pin> Pins => this._Pins is null ? new System.Collections.ObjectModel.ReadOnlyCollection<Pin>( new List<Pin>() ) : new System.Collections.ObjectModel.ReadOnlyCollection<Pin>( this._Pins.Values.ToList() );

        #endregion

    }
}
