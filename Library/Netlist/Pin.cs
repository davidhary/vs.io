using System;
using System.Diagnostics;

namespace isr.IO.NetList
{

    /// <summary>
    /// This is the <see cref="isr.IO.NetList.Pin">Pin</see> class of the net list reader class
    /// library.
    /// </summary>
    /// <remarks>
    /// David, 2008-08-07, 1.2.3141. Add mating connector information for use with the Sandia
    /// cable tester. <para>
    /// David, 2008-08-07, 1.2.3141. Add node number.  Pins can be listed as belonging to a node
    /// separately from the connector. The node is a functional description of the pins telling which
    /// pins are connected together. </para><para>
    /// David, 2007-12-18, 1.2.2908 </para><para>
    /// (c) 2007 Integrated Scientific Resources, Inc. All rights reserved.</para><para>
    /// Licensed under The MIT License.</para>
    /// </remarks>
    public class Pin : IDisposable
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Constructs this class. </summary>
        /// <remarks> David, 2020-10-08. </remarks>

        // instantiate the class
        public Pin() : base()
        {
            this.MatingSide = "P1";
        }

        /// <summary> Calls <see cref="M:Dispose(Boolean Disposing)"/> to cleanup. </summary>
        /// <remarks>
        /// Do not make this method Overridable (virtual) because a derived class should not be able to
        /// override this method.
        /// </remarks>
        public void Dispose()
        {

            // Do not change this code.  Put cleanup code in Dispose(Boolean) below.

            // this disposes all child classes.
            this.Dispose(true);

            // Take this object off the finalization(Queue) and prevent finalization code 
            // from executing a second time.
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// Gets or sets the dispose status sentinel of the base class.  This applies to the derived
        /// class provided proper implementation.
        /// </summary>
        /// <value> The is disposed. </value>
        protected bool IsDisposed { get; private set; }

        /// <summary>
        /// Releases the unmanaged resources used by the <see cref="T:System.Windows.Forms.Control" />
        /// and its child controls and optionally releases the managed resources.
        /// </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <param name="disposing"> <c>True</c> to release both managed and unmanaged resources;
        /// <c>False</c> to release only unmanaged
        /// resources when called from the runtime
        /// finalize. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification = "Exception is published")]
        [DebuggerNonUserCode()]
        protected virtual void Dispose(bool disposing)
        {
            try
            {
                if (!this.IsDisposed && disposing)
                {
                }
            }
            finally
            {
                this.IsDisposed = true;
            }
        }

        /// <summary>
        /// This destructor will run only if the Dispose method does not get called. It gives the base
        /// class the opportunity to finalize. Do not provide destructors in types derived from this
        /// class.
        /// </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        ~Pin()
        {
            // Do not re-create Dispose clean-up code here.
            // Calling Dispose(false) is optimal for readability and maintainability.
            this.Dispose(false);
        }

        #endregion

        #region " METHODS "

        /// <summary> Parses the pin record. </summary>
        /// <remarks>
        /// The pin record includes the pin information as follows (after Trim):<para>
        /// 0         1         2         3         4         5         6         7
        /// 012345678901234567890123456789012345678901234567890123456789012345678901234567
        /// P2              1       1               Passive        CONNECTOR DB9-2 J237            22
        /// 22              Passive        J237 Connector Number:  (0,15)</para><para>
        /// Pin Number (16, 23)</para><para>
        /// Pin Name (24, 39)</para><para>
        /// Pin Type (40,54)</para><para>
        /// Part Value (Connector Name/Description): (55,EOL)</para><para>
        /// </para>
        /// </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="record"> A record from the net list file. </param>
        /// <returns> True if it succeeds, false if it fails. </returns>
        public bool Parse(string record)
        {
            if (string.IsNullOrWhiteSpace(record))
                throw new ArgumentNullException(nameof(record));
            this.ConnectorNumber = record.Substring(0, 16).Trim();
            this.Number = record.Substring(16, 8).Trim();
            this.Name = record.Substring(24, 16).Trim();
            this.PinType = record.Substring(40, 15).Trim();
            this.PartValue = record.Substring(55).Trim();
            return !(string.IsNullOrWhiteSpace( this.ConnectorNumber ) || string.IsNullOrWhiteSpace( this.Number ) || string.IsNullOrWhiteSpace( this.Name ) || string.IsNullOrWhiteSpace( this.PinType ) || string.IsNullOrWhiteSpace( this.PartValue ));
        }

        #endregion

        #region " PROPERTIES "

        /// <summary> Returns a unique key for a pin. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="pin"> The pin. </param>
        /// <returns> A String. </returns>
        public static string BuildKey(Pin pin)
        {
            return pin is null
                ? throw new ArgumentNullException(nameof(pin))
                : string.Format(System.Globalization.CultureInfo.CurrentCulture, "{0}.{1}", pin.ConnectorNumber, pin.Number);
        }

        /// <summary> Returns a unique pin Key. </summary>
        /// <value> The key. </value>
        public string Key => BuildKey( this );

        /// <summary> Gets or sets the pin name. </summary>
        /// <value> The name. </value>
        public string Name { get; set; }

        /// <summary> Gets or sets the pin number. </summary>
        /// <value> The number. </value>
        public string Number { get; set; }

        /// <summary> Gets or sets the public key reference from the database. </summary>
        /// <value> The public key. </value>
        public int PublicKey { get; set; }

        /// <summary> Gets or sets the pin type. </summary>
        /// <value> The type of the pin. </value>
        public string PinType { get; set; }

        /// <summary> Gets or sets the connector number. </summary>
        /// <value> The connector number. </value>
        public string ConnectorNumber { get; set; }

        /// <summary>
        /// Gets or sets the node number.  The node number is Nullable as not every pin belongs to a
        /// node.  Pins not belonging to a node are not wired.
        /// </summary>
        /// <value> The node number. </value>
        public int? NodeNumber { get; set; }

        /// <summary> Gets or sets the connector value. </summary>
        /// <value> The part value. </value>
        public string PartValue { get; set; }

        #endregion

        #region " PIN DEFINITIONS PROPERTIES "

        /// <summary>
        /// Gets or sets the mating side for this connector.  For Sandia systems this could be either
        /// 'P1' or 'P2'.
        /// </summary>
        /// <value> The mating side. </value>
        public string MatingSide { get; set; }

        /// <summary>
        /// Gets or sets the pin number on the mating side to use for connecting this pin.
        /// </summary>
        /// <value> The mating pin number. </value>
        public int MatingPinNumber { get; set; } = 1;

        #endregion


    }
}
