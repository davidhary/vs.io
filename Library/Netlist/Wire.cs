using System;
using System.Diagnostics;

namespace isr.IO.NetList
{

    /// <summary>
    /// This is the <see cref="isr.IO.NetList.Pin">Pin</see> class of the net list reader class
    /// library.
    /// </summary>
    /// <remarks>
    /// (c) 2007 Integrated Scientific Resources, Inc. All rights reserved. <para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2007-12-18, 1.2.2908.x. </para>
    /// </remarks>
    public class Wire : IDisposable
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Constructs this class. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <param name="pinA"> The pin a. </param>
        /// <param name="pinB"> The pin b. </param>

        // instantiate the class
        public Wire(Pin pinA, Pin pinB) : base()
        {
            this.PinA = pinA;
            this.PinB = pinB;
        }

        /// <summary> Calls <see cref="M:Dispose(Boolean Disposing)"/> to cleanup. </summary>
        /// <remarks>
        /// Do not make this method Overridable (virtual) because a derived class should not be able to
        /// override this method.
        /// </remarks>
        public void Dispose()
        {

            // Do not change this code.  Put cleanup code in Dispose(Boolean) below.

            // this disposes all child classes.
            this.Dispose(true);

            // Take this object off the finalization(Queue) and prevent finalization code 
            // from executing a second time.
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// Gets or sets the dispose status sentinel of the base class.  This applies to the derived
        /// class provided proper implementation.
        /// </summary>
        /// <value> The is disposed. </value>
        protected bool IsDisposed { get; private set; }

        /// <summary>
        /// Releases the unmanaged resources used by the <see cref="T:System.Windows.Forms.Control" />
        /// and its child controls and optionally releases the managed resources.
        /// </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <param name="disposing"> <c>True</c> to release both managed and unmanaged resources;
        /// <c>False</c> to release only unmanaged
        /// resources when called from the runtime
        /// finalize. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification = "Exception is published")]
        [DebuggerNonUserCode()]
        protected virtual void Dispose(bool disposing)
        {
            try
            {
                if (!this.IsDisposed && disposing)
                {
                }
            }
            finally
            {
                this.IsDisposed = true;
            }
        }

        /// <summary>
        /// This destructor will run only if the Dispose method does not get called. It gives the base
        /// class the opportunity to finalize. Do not provide destructors in types derived from this
        /// class.
        /// </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        ~Wire()
        {
            // Do not re-create Dispose clean-up code here.
            // Calling Dispose(false) is optimal for readability and maintainability.
            this.Dispose(false);
        }

        #endregion

        #region " PROPERTIES "

        /// <summary> Returns a wire ID. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="wire"> The wire. </param>
        /// <returns> A String. </returns>
        public static string BuildKey(Wire wire)
        {
            return wire is null
                ? throw new ArgumentNullException(nameof(wire))
                : string.Format(System.Globalization.CultureInfo.CurrentCulture, "{0}-{1}", wire.PinA.Key, wire.PinB.Key);
        }

        /// <summary> Returns a unique Wire Key. </summary>
        /// <value> The key. </value>
        public string Key => BuildKey( this );

        /// <summary> Gets or sets <see cref="Pin">pin A</see> of the wire. </summary>
        /// <value> The pin a. </value>
        public Pin PinA { get; private set; }

        /// <summary> Gets or sets <see cref="Pin">pin B</see> of the wire. </summary>
        /// <value> The pin b. </value>
        public Pin PinB { get; private set; }

        /// <summary>
        /// Gets or sets the property indicating if the wire is a primary wire derived from connecting
        /// sequential pins of a node or a redundant wire derived by connecting pins that are already
        /// connected by primary wires.
        /// </summary>
        /// <value> The is primary. </value>
        public bool IsPrimary { get; set; }

        /// <summary> Gets or sets the public key reference from the database. </summary>
        /// <value> The public key. </value>
        public int PublicKey { get; set; }

        #endregion

    }
}
