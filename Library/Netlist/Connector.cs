using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

namespace isr.IO.NetList
{

    /// <summary>
    /// This is the <see cref="isr.IO.NetList.Connector">Connector</see> class of the net list reader
    /// class library. A connector consists of a set of pins.
    /// </summary>
    /// <remarks>
    /// (c) 2007 Integrated Scientific Resources, Inc. All rights reserved. <para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2008-08-07, 1.2.3141.x. Add support for military style pins. Allow populating the
    /// connector before reading the connectors. </para> <para>
    /// David, 2007-12-18, 1.2.2908.x. </para>
    /// </remarks>
    public class Connector : IDisposable
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Constructs this class. </summary>
        /// <remarks> David, 2020-10-08. </remarks>

        // instantiate the class
        public Connector() : base()
        {
            this._Pins = new Dictionary<string, Pin>();
            this.ShellPinNumber = "SH";
            this.MatingSide = "P1";
        }

        /// <summary> Calls <see cref="M:Dispose(Boolean Disposing)"/> to cleanup. </summary>
        /// <remarks>
        /// Do not make this method Overridable (virtual) because a derived class should not be able to
        /// override this method.
        /// </remarks>
        public void Dispose()
        {

            // Do not change this code.  Put cleanup code in Dispose(Boolean) below.

            // this disposes all child classes.
            this.Dispose(true);

            // Take this object off the finalization(Queue) and prevent finalization code 
            // from executing a second time.
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// Gets or sets the dispose status sentinel of the base class.  This applies to the derived
        /// class provided proper implementation.
        /// </summary>
        /// <value> The is disposed. </value>
        protected bool IsDisposed { get; private set; }

        /// <summary>
        /// Releases the unmanaged resources used by the <see cref="T:System.Windows.Forms.Control" />
        /// and its child controls and optionally releases the managed resources.
        /// </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <param name="disposing"> <c>True</c> to release both managed and unmanaged resources;
        /// <c>False</c> to release only unmanaged
        /// resources when called from the runtime
        /// finalize. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification = "Exception is published")]
        [DebuggerNonUserCode()]
        protected virtual void Dispose(bool disposing)
        {
            try
            {
                if (!this.IsDisposed && disposing)
                {
                    this._Pins = null;
                }
            }
            finally
            {
                this.IsDisposed = true;
            }
        }

        /// <summary>
        /// This destructor will run only if the Dispose method does not get called. It gives the base
        /// class the opportunity to finalize. Do not provide destructors in types derived from this
        /// class.
        /// </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        ~Connector()
        {
            // Do not re-create Dispose clean-up code here.
            // Calling Dispose(false) is optimal for readability and maintainability.
            this.Dispose(false);
        }

        #endregion

        #region " PIN NUMBER FORMAT "

        /// <summary>
        /// Specifies the standard pins for alpha numeric pin set.
        /// </summary>
        private static readonly string[] AlphaPins = new[] { "A", "B", "C", "D", "E", "F", "G", "H", "J", "K", "L", "M", "N", "P", "R", "S", "T", "U", "V", "W", "X", "Y", "Z", "a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "m", "n", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z", "AA", "BB", "CC", "DD", "EE", "FF", "GG", "HH", "JJ", "KK", "LL", "MM", "NN", "PP" };

        /// <summary> Builds a pin name from the pin number. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <param name="pinNumber">        . </param>
        /// <param name="useNumericFormat"> True to use a numeric format for building the pin number. </param>
        /// <returns> A String. </returns>
        public static string BuildPinNumber(int pinNumber, bool useNumericFormat)
        {
            return useNumericFormat ? pinNumber.ToString("0", System.Globalization.CultureInfo.CurrentCulture) : AlphaPins[pinNumber - 1];
        }

        /// <summary> Gets the shell pin number. </summary>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <value> The shell pin number. </value>
        public string ShellPinNumber { get; set; }

        #endregion

        #region " METHODS "

        /// <summary>
        /// Adds a new <see cref="Pin">Pin</see> to the <see cref="Connector">Connector</see>
        /// or returns a reference to an existing pin.
        /// </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="pinNumber"> . </param>
        /// <returns> A Pin. </returns>
        public Pin AddPin(string pinNumber)
        {
            if (string.IsNullOrWhiteSpace(pinNumber))
                throw new ArgumentNullException(nameof(pinNumber));
            if ( this.Exists(pinNumber))
            {
                return this.SelectPin(pinNumber);
            }
            else
            {
                Pin newPin = null;
                try
                {
                    newPin = new Pin()
                    {
                        Name = pinNumber,
                        Number = pinNumber,
                        ConnectorNumber = Number,
                        PartValue = Name,
                        MatingSide = MatingSide,
                        MatingPinNumber = this.FirstMatingPinNumber + this._Pins.Count
                    };
                    this._Pins.Add(pinNumber, newPin);
                }
                catch
                {
                    if (newPin is object)
                        newPin.Dispose();
                    throw;
                }

                return newPin;
            }
        }

        /// <summary> Returns true of the pin already exists. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <param name="pin"> . </param>
        /// <returns> True if it succeeds, false if it fails. </returns>
        public bool Exists(Pin pin)
        {
            return pin is object && this.Exists(pin.Number);
        }

        /// <summary> Returns true of the pin already exists. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <param name="pinNumber"> . </param>
        /// <returns> True if it succeeds, false if it fails. </returns>
        public bool Exists(string pinNumber)
        {
            return this._Pins is object && this._Pins.ContainsKey(pinNumber);
        }

        /// <summary> Gets the condition telling if the connector has pins. </summary>
        /// <value> The has pins. </value>
        public bool HasPins => this._Pins is object && this._Pins.Any();

        /// <summary> Parses the connector record. </summary>
        /// <remarks>
        /// The connector record includes the following information:<para>
        /// 0         1         2         3         4 01234567890123456789012345678901234567890123456789
        /// J236                          'J236'    CONNECTOR J236 DB9-1               P1 CONNECTOR DB9-1
        /// Name (0, 29)</para><para>
        /// Number (30, 39)</para><para>
        /// Part Value (40, EOL)</para><para>
        /// </para>
        /// </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="record"> A record from the net list file. </param>
        /// <returns> <c>true</c> if it succeeds; otherwise <c>false</c>. </returns>
        public bool Parse(string record)
        {
            if (string.IsNullOrWhiteSpace(record))
                throw new ArgumentNullException(nameof(record));
            this.Name = record.Substring(0, 30).Trim();
            this.Number = record.Substring(30, 10).Trim();
            return !(string.IsNullOrWhiteSpace( this.Number ) || string.IsNullOrWhiteSpace( this.Name ));
        }

        /// <summary> Returns a string array of pin keys. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <returns> A String() </returns>
        public string[] PinKeys()
        {
            if ( this.HasPins )
            {
                var keys = new string[this._Pins.Count];
                for (int i = 0, loopTo = this._Pins.Count - 1; i <= loopTo; i++)
                    keys[i] = this._Pins.Values.ElementAtOrDefault(i).Key;
                return keys;
            }
            else
            {
                return Array.Empty<string>();
            }
        }

        /// <summary>
        /// Adds pins to the connector based on the
        /// <see cref="PinCount">Pin Count</see> and
        /// <see cref="IsNumericPinNumbering">pin numbering format</see>.
        /// </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <returns> True if it succeeds, false if it fails. </returns>
        public bool Populate()
        {
            for (int i = 1, loopTo = this.PinCount; i <= loopTo; i++)
                _ = this.AddPin( BuildPinNumber( i, this.IsNumericPinNumbering ) );
            if ( this.HasShellPin )
            {
                _ = this.AddPin( this.ShellPinNumber );
            }

            return true;
        }

        /// <summary> Rolls back cable assembly by clearing all the public keys. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        public void Rollback()
        {
            this.PublicKey = 0;
            foreach (Pin pin in this._Pins.Values)
                pin.PublicKey = 0;
        }

        /// <summary> Selects a pin from the pins collection. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="pinNumber"> . </param>
        /// <returns> A Pin. </returns>
        public Pin SelectPin(string pinNumber)
        {
            if (string.IsNullOrWhiteSpace(pinNumber))
                throw new ArgumentNullException(nameof(pinNumber));
            var newPin = this._Pins[pinNumber];
            return newPin;
        }

        #endregion

        #region " PROPERTIES "

        /// <summary> Gets the connector name. </summary>
        /// <value> The name. </value>
        public string Name { get; set; }

        /// <summary> Gets the connector number. </summary>
        /// <value> The number. </value>
        public string Number { get; set; }

        /// <summary> Gets the public key reference from the database. </summary>
        /// <value> The public key. </value>
        public int PublicKey { get; set; }

        /// <summary> The pins. </summary>
        private Dictionary<string, Pin> _Pins;

        /// <summary> Gets the collection of <see cref="Pin">Pins</see>. </summary>
        /// <value> The pins. </value>
        public System.Collections.ObjectModel.ReadOnlyCollection<Pin> Pins => new System.Collections.ObjectModel.ReadOnlyCollection<Pin>( this._Pins.Values.ToList() );

        /// <summary> Gets the number of pins in the connector. </summary>
        /// <value> The number of pins. </value>
        public int PinCount { get; set; }

        /// <summary> Gets the pin numbering format. </summary>
        /// <value> The is numeric pin numbering. </value>
        public bool IsNumericPinNumbering { get; set; } = true;

        #endregion

        #region " CONNECTOR DEFINITIONS "

        /// <summary> Gets the shall pin attribute. </summary>
        /// <value> The has shell pin. </value>
        public bool HasShellPin { get; set; } = true;

        /// <summary> Gets the mating side for this connector. </summary>
        /// <value> The mating side. </value>
        public string MatingSide { get; set; }

        /// <summary>
        /// Gets the first pin on the mating side to use for connecting the first pin of this connector.
        /// One based.
        /// </summary>
        /// <value> The first mating pin number. </value>
        public int FirstMatingPinNumber { get; set; } = 1;

        /// <summary> Gets the reason why this connector is not creatable. </summary>
        /// <value> The non creatable reason. </value>
        public string NonCreatableReason { get; private set; }

        /// <summary> Returns <c>True</c> if the connector is ready to populate pins. </summary>
        /// <value> The creatable. </value>
        public bool Creatable
        {
            get
            {
                this.NonCreatableReason = string.Empty;
                if ( this.PinCount <= 0)
                {
                    this.NonCreatableReason = string.Format(System.Globalization.CultureInfo.CurrentCulture, "Pin count '={0}' must be greater than 0", this.PinCount );
                    return false;
                }
                else if ( this.FirstMatingPinNumber <= 0)
                {
                    this.NonCreatableReason = string.Format(System.Globalization.CultureInfo.CurrentCulture, "First mating pin number '={0}' must be greater than 0", this.FirstMatingPinNumber );
                    return false;
                }
                else if (!(this.MatingSide == "P1" || this.MatingSide == "P2"))
                {
                    this.NonCreatableReason = string.Format(System.Globalization.CultureInfo.CurrentCulture, "Mating side '={0}' must be either P1 or P2", this.MatingSide );
                }

                return true;
            }
        }

        #endregion

    }
}
