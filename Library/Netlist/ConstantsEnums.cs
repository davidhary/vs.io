﻿
namespace isr.IO.NetList
{

    /// <summary> Enumerates the states reading the net list. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
    internal enum ReadingState
    {

        /// <summary> An enum constant representing the none option. </summary>
        None,

        /// <summary> An enum constant representing the looking for connector list option. </summary>
        LookingForConnectorList,

        /// <summary> An enum constant representing the reading connectors option. </summary>
        ReadingConnectors,

        /// <summary> An enum constant representing the looking for wire list option. </summary>
        LookingForWireList,

        /// <summary> An enum constant representing the looking for node option. </summary>
        LookingForNode,

        /// <summary> An enum constant representing the reading node option. </summary>
        ReadingNode,

        /// <summary> An enum constant representing the done option. </summary>
        Done,

        /// <summary> An enum constant representing the failed option. </summary>
        Failed
    }
}