using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace isr.IO
{

    /// <summary> Reads delimited files. </summary>
    /// <remarks>
    /// (c) 2006 Integrated Scientific Resources, Inc. All rights reserved. <para>
    /// Licensed under The MIT License. </para><para>
    /// David, 2006-04-20, 1.1.2301.x. </para>
    /// </remarks>
    public class StructuredReader : IDisposable
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Constructs this class. </summary>
        /// <remarks> David, 2020-10-08. </remarks>

        // instantiate the base class
        public StructuredReader() : this(DefaultFilePathName)
        {
        }

        /// <summary> Constructs this class. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <param name="filePathName"> Specifies the file path name. </param>

        // instantiate the base class
        public StructuredReader(string filePathName) : base()
        {
            this._FilePathName = filePathName;

            // open the parser
            this.Parser = new Microsoft.VisualBasic.FileIO.TextFieldParser( this._FilePathName )
            {
                TextFieldType = Microsoft.VisualBasic.FileIO.FieldType.Delimited,
                Delimiters = new string[] { ",", Microsoft.VisualBasic.Constants.vbTab }
            };
        }

        /// <summary> Calls <see cref="M:Dispose(Boolean Disposing)"/> to cleanup. </summary>
        /// <remarks>
        /// Do not make this method Overridable (virtual) because a derived class should not be able to
        /// override this method.
        /// </remarks>
        public void Dispose()
        {

            // Do not change this code.  Put cleanup code in Dispose(Boolean) below.

            // this disposes all child classes.
            this.Dispose(true);

            // Take this object off the finalization(Queue) and prevent finalization code 
            // from executing a second time.
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// Gets the dispose status sentinel of the base class.  This applies to the derived class
        /// provided proper implementation.
        /// </summary>
        /// <value> The is disposed. </value>
        protected bool IsDisposed { get; set; }

        /// <summary>
        /// Releases the unmanaged resources used by the <see cref="T:System.Windows.Forms.Control" />
        /// and its child controls and optionally releases the managed resources.
        /// </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <param name="disposing"> <c>True</c> to release both managed and unmanaged resources;
        /// <c>False</c> to release only unmanaged
        /// resources when called from the runtime
        /// finalize. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification = "Exception is published")]
        [DebuggerNonUserCode()]
        protected virtual void Dispose(bool disposing)
        {
            try
            {
                if (!this.IsDisposed && disposing)
                {
                    if ( this.Parser is object)
                    {
                        this.Parser.Close();
                        this.Parser.Dispose();
                        this.Parser = null;
                    }
                }
            }
            finally
            {
                this.IsDisposed = true;
            }
        }

        #endregion

        #region " SHARED "

        /// <summary> Gets the default extension. </summary>
        public const string DefaultExtension = ".csv";

        /// <summary>
        /// Returns the default file name for the structured I/O.  This would be the executable file name
        /// appended with the
        /// <see cref="DefaultExtension">default extension</see>.
        /// </summary>
        /// <value> The default file path name. </value>
        public static string DefaultFilePathName => Core.ApplicationInfo.BuildApplicationConfigFileName( DefaultExtension );

        /// <summary>
        /// Return file rows as a jagged array allowing each row to be of different length.
        /// </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <param name="filePathName"> Specifies the file path name. </param>
        /// <returns> The array of arrays of rows. </returns>
        public static System.Collections.ObjectModel.ReadOnlyCollection<string[]> ReadRows(string filePathName)
        {
            var list = new List<string[]>();
            using (var MyReader = new Microsoft.VisualBasic.FileIO.TextFieldParser(filePathName))
            {
                MyReader.TextFieldType = Microsoft.VisualBasic.FileIO.FieldType.Delimited;
                MyReader.Delimiters = new string[] { "," };
                // MyReader.HasFieldsEnclosedInQuotes'
                string[] currentRow;
                // Loop through all of the fields in the file. 
                // If any lines are corrupt, report an error and continue parsing. 
                while (!MyReader.EndOfData)
                {
                    currentRow = MyReader.ReadFields();
                    if (currentRow is object && currentRow.Length > 0)
                    {
                        list.Add(currentRow);
                    }
                }
            }

            return new System.Collections.ObjectModel.ReadOnlyCollection<string[]>(list);
        }

        /// <summary>
        /// Return file rows as a jagged array allowing each row to be of different length.
        /// </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <param name="filePathName"> Specifies the file path name. </param>
        /// <returns> The array of arrays of rows. </returns>
        public static string[][] ReadAllRows(string filePathName)
        {
            var data = Array.Empty<string[]>();
            using (var MyReader = new Microsoft.VisualBasic.FileIO.TextFieldParser(filePathName))
            {
                MyReader.TextFieldType = Microsoft.VisualBasic.FileIO.FieldType.Delimited;
                MyReader.Delimiters = new string[] { "," };
                // MyReader.HasFieldsEnclosedInQuotes'
                string[] currentRow;
                // Loop through all of the fields in the file. 
                // If any lines are corrupt, report an error and continue parsing. 
                int n = 0;
                while (!MyReader.EndOfData)
                {
                    currentRow = MyReader.ReadFields();
                    if (currentRow is object && currentRow.Length > 0)
                    {
                        Array.Resize(ref data, data.GetLength(0) + 1);
                        data[n] = currentRow;
                    }

                    n += 1;
                }
            }

            return data;
        }

        #endregion

        #region " FILE NAME "

        /// <summary> Full pathname of the file. </summary>
        private string _FilePathName = string.Empty;

        /// <summary> Gets or sets the file name. </summary>
        /// <remarks> Use this property to get or set the file name. </remarks>
        /// <value> <c>FilePathName</c> is a String property. </value>
        public string FilePathName
        {
            get {
                if ( this._FilePathName.Length == 0 )
                {
                    // set default file name if empty.
                    this._FilePathName = DefaultFilePathName;
                }

                return this._FilePathName;
            }

            set => this._FilePathName = value;
        }

        #endregion

        #region " FIELDS: PARSE "

        /// <summary>
        /// Specifies the field index from which to read a field value. Resets to zero upon reading a new
        /// record.
        /// </summary>
        /// <exception cref="InvalidOperationException">   Thrown when the requested operation is
        /// invalid. </exception>
        /// <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
        /// the required range. </exception>
        /// <value> The field index. </value>
        public int FieldIndex { get; set; }

        /// <summary>
        /// Returns the field for the specified <paramref name="index">index</paramref>.
        /// </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <exception cref="InvalidOperationException">   Thrown when the requested operation is
        /// invalid. </exception>
        /// <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
        /// the required range. </exception>
        /// <param name="index"> Specifies the index in the fields record from which to get the value. </param>
        /// <returns> The field value. </returns>
        public string SelectField(int index)
        {
            if ( this.Fields() is null)
            {
                throw new InvalidOperationException("Record not available");
            }
            else if (index < 0 || index >= this._Fields.Length)
            {
                throw new ArgumentOutOfRangeException(nameof(index), index, string.Format(System.Globalization.CultureInfo.CurrentCulture, "Must be positive value less than {0}", this._Fields.Length));
            }

            return this._Fields[index];
        }

        /// <summary> Tries to select field, returning empty if nothing. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <param name="index"> Specifies the index in the fields record from which to get the value. </param>
        /// <returns> A String. </returns>
        public string TrySelectField(int index)
        {
            if ( this.Fields() is null)
            {
                return string.Empty;
            }
            else if (index < 0 || index >= this._Fields.Length)
            {
                return string.Empty;
            }

            return this._Fields[index];
        }

        /// <summary>
        /// Returns the field for the current <see cref="FieldIndex">index</see>. Increments the current
        /// field index.
        /// </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <returns> The field value. </returns>
        public string SelectField()
        {
            this.FieldIndex += 1;
            return this.SelectField( this.FieldIndex - 1);
        }

        /// <summary>
        /// Returns a <see cref="T:Boolean"/> field value for the specified field
        /// <paramref name="index">index</paramref>.
        /// </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <param name="index"> Specifies the index in the fields record from which to get the value. </param>
        /// <param name="value"> A dummy value. </param>
        /// <returns> The <see cref="T:Boolean"/> field value. </returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Style", "IDE0060:Remove unused parameter", Justification = "<Pending>")]
        public bool ParseField(int index, bool value)
        {
            return bool.Parse( this.SelectField(index));
        }

        /// <summary>
        /// Returns a <see cref="T:Boolean"/> field value for the current
        /// <see cref="FieldIndex">index</see>. Increments the current field index.
        /// </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <param name="value"> A dummy value. </param>
        /// <returns> The <see cref="T:Boolean"/> field value. </returns>
        public bool ParseField(bool value)
        {
            this.FieldIndex += 1;
            return this.ParseField( this.FieldIndex - 1, value);
        }

        /// <summary>
        /// Returns a <see cref="T:Byte"/> field value for the specified field
        /// <paramref name="index">index</paramref>.
        /// </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <param name="index"> Specifies the index in the fields record from which to get the value. </param>
        /// <param name="value"> A dummy value. </param>
        /// <returns> The <see cref="T:Byte"/> field value. </returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Style", "IDE0060:Remove unused parameter", Justification = "<Pending>")]
        public byte ParseField(int index, byte value)
        {
            return byte.Parse( this.SelectField(index), System.Globalization.NumberStyles.Integer, System.Globalization.CultureInfo.CurrentCulture);
        }

        /// <summary>
        /// Returns a <see cref="T:Byte"/> field value for the current
        /// <see cref="FieldIndex">index</see>. Increments the current field index.
        /// </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <param name="value"> A dummy value. </param>
        /// <returns> The  <see cref="T:Byte"/> field value. </returns>
        public byte ParseField(byte value)
        {
            this.FieldIndex += 1;
            return this.ParseField( this.FieldIndex - 1, value);
        }

        /// <summary>
        /// Returns a <see cref="T:DateTime"/> field value for the specified field
        /// <paramref name="index">index</paramref>.
        /// </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <param name="index"> Specifies the index in the fields record from which to get the value. </param>
        /// <param name="value"> A dummy value. </param>
        /// <returns> The field value. </returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Style", "IDE0060:Remove unused parameter", Justification = "<Pending>")]
        public DateTime ParseField(int index, DateTime value)
        {
            return DateTime.Parse( this.SelectField(index), System.Globalization.CultureInfo.CurrentCulture);
        }

        /// <summary>
        /// Returns a <see cref="T:DateTime"/> field value for the current
        /// <see cref="FieldIndex">index</see>. Increments the current field index.
        /// </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <param name="value"> A dummy value. </param>
        /// <returns> The field value. </returns>
        public DateTime ParseField(DateTime value)
        {
            this.FieldIndex += 1;
            return this.ParseField( this.FieldIndex - 1, value);
        }

        /// <summary>
        /// Returns a <see cref="T:DateTimeOffset"/> field value for the specified field.
        /// </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <param name="index"> The field index. </param>
        /// <param name="value"> A dummy value. </param>
        /// <returns> The <see cref="T:Boolean"/> field value. </returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Style", "IDE0060:Remove unused parameter", Justification = "<Pending>")]
        public DateTimeOffset ParseField(int index, DateTimeOffset value)
        {
            return DateTime.Parse( this.SelectField(index), System.Globalization.CultureInfo.CurrentCulture);
        }

        /// <summary>
        /// Returns a <see cref="T:DateTimeOffset"/> field value for the specified field.
        /// </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <param name="value"> A dummy value. </param>
        /// <returns> The <see cref="T:Boolean"/> field value. </returns>
        public DateTimeOffset ParseField(DateTimeOffset value)
        {
            this.FieldIndex += 1;
            return this.ParseField( this.FieldIndex - 1, value);
        }

        /// <summary>
        /// Returns a <see cref="T:Decimal"/> field value for the specified field
        /// <paramref name="index">index</paramref>.
        /// </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <param name="index"> Specifies the index in the fields record from which to get the value. </param>
        /// <param name="value"> A dummy value. </param>
        /// <returns> The field value. </returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Style", "IDE0060:Remove unused parameter", Justification = "<Pending>")]
        public decimal ParseField(int index, decimal value)
        {
            return decimal.Parse( this.SelectField(index), System.Globalization.NumberStyles.Float, System.Globalization.CultureInfo.CurrentCulture);
        }

        /// <summary>
        /// Returns a <see cref="T:Decimal"/> field value for the current
        /// <see cref="FieldIndex">index</see>. Increments the current field index.
        /// </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <param name="value"> A dummy value. </param>
        /// <returns> The field value. </returns>
        public decimal ParseField(decimal value)
        {
            this.FieldIndex += 1;
            return this.ParseField( this.FieldIndex - 1, value);
        }

        /// <summary>
        /// Returns a <see cref="T:Double"/> field value for the specified field
        /// <paramref name="index">index</paramref>.
        /// </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <param name="index"> Specifies the index in the fields record from which to get the value. </param>
        /// <param name="value"> A dummy value. </param>
        /// <returns> The field value. </returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Style", "IDE0060:Remove unused parameter", Justification = "<Pending>")]
        public double ParseField(int index, double value)
        {
            return double.Parse( this.SelectField(index), System.Globalization.NumberStyles.Float, System.Globalization.CultureInfo.CurrentCulture);
        }

        /// <summary>
        /// Returns a <see cref="T:Double"/> field value for the current
        /// <see cref="FieldIndex">index</see>. Increments the current field index.
        /// </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <param name="value"> A dummy value. </param>
        /// <returns> The field value. </returns>
        public double ParseField(double value)
        {
            this.FieldIndex += 1;
            return this.ParseField( this.FieldIndex - 1, value);
        }

        /// <summary>
        /// Returns a <see cref="T:Int16"/> field value for the specified field
        /// <paramref name="index">index</paramref>.
        /// </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <param name="index"> Specifies the index in the fields record from which to get the value. </param>
        /// <param name="value"> A dummy value. </param>
        /// <returns> The field value. </returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Style", "IDE0060:Remove unused parameter", Justification = "<Pending>")]
        public short ParseField(int index, short value)
        {
            return short.Parse( this.SelectField(index), System.Globalization.NumberStyles.Number, System.Globalization.CultureInfo.CurrentCulture);
        }

        /// <summary>
        /// Returns a <see cref="T:Int16"/> field value for the current
        /// <see cref="FieldIndex">index</see>. Increments the current field index.
        /// </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <param name="value"> A dummy value. </param>
        /// <returns> The field value. </returns>
        public short ParseField(short value)
        {
            this.FieldIndex += 1;
            return this.ParseField( this.FieldIndex - 1, value);
        }

        /// <summary>
        /// Returns a <see cref="T:Int32"/> field value for the specified field
        /// <paramref name="index">index</paramref>.
        /// </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <param name="index"> Specifies the index in the fields record from which to get the value. </param>
        /// <param name="value"> A dummy value. </param>
        /// <returns> The field value. </returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Style", "IDE0060:Remove unused parameter", Justification = "<Pending>")]
        public int ParseField(int index, int value)
        {
            return int.Parse( this.SelectField(index), System.Globalization.NumberStyles.Number, System.Globalization.CultureInfo.CurrentCulture);
        }

        /// <summary>
        /// Returns a <see cref="T:Int32"/> field value for the current
        /// <see cref="FieldIndex">index</see>. Increments the current field index.
        /// </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <param name="value"> A dummy value. </param>
        /// <returns> The field value. </returns>
        public int ParseField(int value)
        {
            this.FieldIndex += 1;
            return this.ParseField( this.FieldIndex - 1, value);
        }

        /// <summary>
        /// Returns a <see cref="T:Int64"/> field value for the specified field
        /// <paramref name="index">index</paramref>.
        /// </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <param name="index"> Specifies the index in the fields record from which to get the value. </param>
        /// <param name="value"> A dummy value. </param>
        /// <returns> The field value. </returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Style", "IDE0060:Remove unused parameter", Justification = "<Pending>")]
        public long ParseField(int index, long value)
        {
            return long.Parse( this.SelectField(index), System.Globalization.NumberStyles.Number, System.Globalization.CultureInfo.CurrentCulture);
        }

        /// <summary>
        /// Returns a <see cref="T:Int64"/> field value for the current
        /// <see cref="FieldIndex">index</see>. Increments the current field index.
        /// </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <param name="value"> A dummy value. </param>
        /// <returns> The field value. </returns>
        public long ParseField(long value)
        {
            this.FieldIndex += 1;
            return this.ParseField( this.FieldIndex - 1, value);
        }

        /// <summary>
        /// Returns a <see cref="T:Single"/> field value for the specified field
        /// <paramref name="index">index</paramref>.
        /// </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <param name="index"> Specifies the index in the fields record from which to get the value. </param>
        /// <param name="value"> A dummy value. </param>
        /// <returns> The field value. </returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Style", "IDE0060:Remove unused parameter", Justification = "<Pending>")]
        public float ParseField(int index, float value)
        {
            return float.Parse( this.SelectField(index), System.Globalization.NumberStyles.Float, System.Globalization.CultureInfo.CurrentCulture);
        }

        /// <summary>
        /// Returns a <see cref="T:Single"/> field value for the current
        /// <see cref="FieldIndex">index</see>. Increments the current field index.
        /// </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <param name="value"> A dummy value. </param>
        /// <returns> The field value. </returns>
        public float ParseField(float value)
        {
            this.FieldIndex += 1;
            return this.ParseField( this.FieldIndex - 1, value);
        }

        /// <summary>
        /// Returns a <see cref="T:TimeSpan"/> field value for the specified field
        /// <paramref name="index">index</paramref>.
        /// </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <param name="index"> Specifies the index in the fields record from which to get the value. </param>
        /// <param name="value"> A dummy value. </param>
        /// <returns> The field value. </returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Style", "IDE0060:Remove unused parameter", Justification = "<Pending>")]
        public TimeSpan ParseField(int index, TimeSpan value)
        {
            return TimeSpan.Parse( this.SelectField(index));
        }

        /// <summary>
        /// Returns a <see cref="T:TimeSpan"/> field value for the current
        /// <see cref="FieldIndex">index</see>. Increments the current field index.
        /// </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <param name="value"> A dummy value. </param>
        /// <returns> The field value. </returns>
        public TimeSpan ParseField(TimeSpan value)
        {
            this.FieldIndex += 1;
            return this.ParseField( this.FieldIndex - 1, value);
        }

        #endregion

        #region " FIELDS: TRY PARSE "

        /// <summary>
        /// Returns a <see cref="T:Boolean"/> field value for the specified field
        /// <paramref name="index">index</paramref>.
        /// </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <param name="index"> Specifies the index in the fields record from which to get the value. </param>
        /// <param name="value"> [in,out] parsed value. </param>
        /// <returns> The <see cref="T:Boolean"/> field value. </returns>
        public bool TryParseField(int index, ref bool value)
        {
            return bool.TryParse( this.SelectField(index), out value);
        }

        /// <summary>
        /// Returns a <see cref="T:Boolean"/> field value for the current
        /// <see cref="FieldIndex">index</see>. Increments the current field index.
        /// </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <param name="value"> [in,out] parsed value. </param>
        /// <returns> The <see cref="T:Boolean"/> field value. </returns>
        public bool TryParseField(ref bool value)
        {
            this.FieldIndex += 1;
            return this.TryParseField( this.FieldIndex - 1, ref value);
        }

        /// <summary>
        /// Returns a <see cref="T:Byte"/> field value for the specified field
        /// <paramref name="index">index</paramref>.
        /// </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <param name="index"> Specifies the index in the fields record from which to get the value. </param>
        /// <param name="value"> [in,out] parsed value. </param>
        /// <returns> The <see cref="T:Byte"/> field value. </returns>
        public bool TryParseField(int index, ref byte value)
        {
            return byte.TryParse( this.SelectField(index), System.Globalization.NumberStyles.Number, System.Globalization.CultureInfo.CurrentCulture, out value);
        }

        /// <summary>
        /// Returns a <see cref="T:Byte"/> field value for the current
        /// <see cref="FieldIndex">index</see>. Increments the current field index.
        /// </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <param name="value"> [in,out] parsed value. </param>
        /// <returns> The  <see cref="T:Byte"/> field value. </returns>
        public bool TryParseField(ref byte value)
        {
            this.FieldIndex += 1;
            return this.TryParseField( this.FieldIndex - 1, ref value);
        }

        /// <summary>
        /// Returns a <see cref="T:DateTime"/> field value for the specified field
        /// <paramref name="index">index</paramref>.
        /// </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <param name="index"> Specifies the index in the fields record from which to get the value. </param>
        /// <param name="value"> [in,out] parsed value. </param>
        /// <returns> The field value. </returns>
        public bool TryParseField(int index, ref DateTime value)
        {
            return DateTime.TryParse( this.SelectField(index), System.Globalization.CultureInfo.CurrentCulture, System.Globalization.DateTimeStyles.None, out value);
        }

        /// <summary>
        /// Returns a <see cref="T:DateTime"/> field value for the current
        /// <see cref="FieldIndex">index</see>. Increments the current field index.
        /// </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <param name="value"> [in,out] parsed value. </param>
        /// <returns> The field value. </returns>
        public bool TryParseField(ref DateTime value)
        {
            this.FieldIndex += 1;
            return this.TryParseField( this.FieldIndex - 1, ref value);
        }

        /// <summary>
        /// Returns a <see cref="T:DateTimeOffset"/> field value for the specified field.
        /// </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <param name="index"> index. </param>
        /// <param name="value"> [in,out] parsed value. </param>
        /// <returns> The <see cref="T:Boolean"/> field value. </returns>
        public bool TryParseField(int index, ref DateTimeOffset value)
        {
            return DateTimeOffset.TryParse( this.SelectField(index), System.Globalization.CultureInfo.CurrentCulture, System.Globalization.DateTimeStyles.None, out value);
        }

        /// <summary>
        /// Returns a <see cref="T:DateTimeOffset"/> field value for the current
        /// <see cref="FieldIndex">index</see>. Increments the current field index.
        /// </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <param name="value"> [in,out] parsed value. </param>
        /// <returns> The <see cref="T:Boolean"/> field value. </returns>
        public bool TryParseField(ref DateTimeOffset value)
        {
            this.FieldIndex += 1;
            return this.TryParseField( this.FieldIndex - 1, ref value);
        }

        /// <summary>
        /// Returns a <see cref="T:Decimal"/> field value for the specified field
        /// <paramref name="index">index</paramref>.
        /// </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <param name="index"> Specifies the index in the fields record from which to get the value. </param>
        /// <param name="value"> [in,out] parsed value. </param>
        /// <returns> The field value. </returns>
        public bool TryParseField(int index, ref decimal value)
        {
            return decimal.TryParse( this.SelectField(index), System.Globalization.NumberStyles.Float, System.Globalization.CultureInfo.CurrentCulture, out value);
        }

        /// <summary>
        /// Returns a <see cref="T:Decimal"/> field value for the current
        /// <see cref="FieldIndex">index</see>. Increments the current field index.
        /// </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <param name="value"> [in,out] A dummy value. </param>
        /// <returns> The field value. </returns>
        public bool TryParseField(ref decimal value)
        {
            this.FieldIndex += 1;
            return this.TryParseField( this.FieldIndex - 1, ref value);
        }

        /// <summary>
        /// Returns a <see cref="T:Double"/> field value for the specified field
        /// <paramref name="index">index</paramref>.
        /// </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <param name="index"> Specifies the index in the fields record from which to get the value. </param>
        /// <param name="value"> [in,out] parsed value. </param>
        /// <returns> The field value. </returns>
        public bool TryParseField(int index, ref double value)
        {
            return double.TryParse( this.SelectField(index), System.Globalization.NumberStyles.Float, System.Globalization.CultureInfo.CurrentCulture, out value);
        }

        /// <summary>
        /// Returns a <see cref="T:Double"/> field value for the current
        /// <see cref="FieldIndex">index</see>. Increments the current field index.
        /// </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <param name="value"> [in,out] parsed value. </param>
        /// <returns> The field value. </returns>
        public bool TryParseField(ref double value)
        {
            this.FieldIndex += 1;
            return this.TryParseField( this.FieldIndex - 1, ref value);
        }

        /// <summary>
        /// Returns a <see cref="T:Int16"/> field value for the specified field
        /// <paramref name="index">index</paramref>.
        /// </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <param name="index"> Specifies the index in the fields record from which to get the value. </param>
        /// <param name="value"> [in,out] parsed value. </param>
        /// <returns> The field value. </returns>
        public bool TryParseField(int index, ref short value)
        {
            return short.TryParse( this.SelectField(index), System.Globalization.NumberStyles.Number, System.Globalization.CultureInfo.CurrentCulture, out value);
        }

        /// <summary>
        /// Returns a <see cref="T:Int16"/> field value for the current
        /// <see cref="FieldIndex">index</see>. Increments the current field index.
        /// </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <param name="value"> [in,out] parsed value. </param>
        /// <returns> The field value. </returns>
        public bool TryParseField(ref short value)
        {
            this.FieldIndex += 1;
            return this.TryParseField( this.FieldIndex - 1, ref value);
        }

        /// <summary>
        /// Returns a <see cref="T:Int32"/> field value for the specified field
        /// <paramref name="index">index</paramref>.
        /// </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <param name="index"> Specifies the index in the fields record from which to get the value. </param>
        /// <param name="value"> [in,out] parsed value. </param>
        /// <returns> The field value. </returns>
        public bool TryParseField(int index, ref int value)
        {
            return int.TryParse( this.SelectField(index), System.Globalization.NumberStyles.Number, System.Globalization.CultureInfo.CurrentCulture, out value);
        }

        /// <summary>
        /// Returns a <see cref="T:Int32"/> field value for the current
        /// <see cref="FieldIndex">index</see>. Increments the current field index.
        /// </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <param name="value"> [in,out] parsed value. </param>
        /// <returns> The field value. </returns>
        public bool TryParseField(ref int value)
        {
            this.FieldIndex += 1;
            return this.TryParseField( this.FieldIndex - 1, ref value);
        }

        /// <summary>
        /// Returns a <see cref="T:Int64"/> field value for the specified field
        /// <paramref name="index">index</paramref>.
        /// </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <param name="index"> Specifies the index in the fields record from which to get the value. </param>
        /// <param name="value"> [in,out] A dummy value. </param>
        /// <returns> The field value. </returns>
        public bool TryParseField(int index, ref long value)
        {
            return long.TryParse( this.SelectField(index), System.Globalization.NumberStyles.Number, System.Globalization.CultureInfo.CurrentCulture, out value);
        }

        /// <summary>
        /// Returns a <see cref="T:Int64"/> field value for the current
        /// <see cref="FieldIndex">index</see>. Increments the current field index.
        /// </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <param name="value"> [in,out] parsed value. </param>
        /// <returns> The field value. </returns>
        public bool TryParseField(ref long value)
        {
            this.FieldIndex += 1;
            return this.TryParseField( this.FieldIndex - 1, ref value);
        }

        /// <summary>
        /// Returns a <see cref="T:Single"/> field value for the specified field
        /// <paramref name="index">index</paramref>.
        /// </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <param name="index"> Specifies the index in the fields record from which to get the value. </param>
        /// <param name="value"> [in,out] parsed value. </param>
        /// <returns> The field value. </returns>
        public bool TryParseField(int index, ref float value)
        {
            return float.TryParse( this.SelectField(index), System.Globalization.NumberStyles.Float, System.Globalization.CultureInfo.CurrentCulture, out value);
        }

        /// <summary>
        /// Returns a <see cref="T:Single"/> field value for the current
        /// <see cref="FieldIndex">index</see>. Increments the current field index.
        /// </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <param name="value"> [in,out] parsed value. </param>
        /// <returns> The field value. </returns>
        public bool TryParseField(ref float value)
        {
            this.FieldIndex += 1;
            return this.TryParseField( this.FieldIndex - 1, ref value);
        }

        /// <summary>
        /// Returns a <see cref="T:TimeSpan"/> field value for the specified field
        /// <paramref name="index">index</paramref>.
        /// </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <param name="index"> Specifies the index in the fields record from which to get the value. </param>
        /// <param name="value"> [in,out] parsed value. </param>
        /// <returns> The field value. </returns>
        public bool TryParseField(int index, ref TimeSpan value)
        {
            return TimeSpan.TryParse( this.SelectField(index), out value);
        }

        /// <summary>
        /// Returns a <see cref="T:TimeSpan"/> field value for the current
        /// <see cref="FieldIndex">index</see>. Increments the current field index.
        /// </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <param name="value"> [in,out] parsed value. </param>
        /// <returns> The field value. </returns>
        public bool TryParseField(ref TimeSpan value)
        {
            this.FieldIndex += 1;
            return this.TryParseField( this.FieldIndex - 1, ref value);
        }

        #endregion

        #region " FIELDS READER "

        /// <summary> The fields. </summary>
        private string[] _Fields = Array.Empty<string>();

        /// <summary> Returns the last record that was read from the file. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <returns> The last record that was read from the file. </returns>
        public string[] Fields()
        {
            return this._Fields;
        }

        /// <summary> Returns <c>True</c> if the parser read fields from the file. </summary>
        /// <value> The has fields. </value>
        public bool HasFields => this._Fields is object && this._Fields.Length > 0;

        /// <summary>
        /// Reads a record of fields from the file. Skips to cursor to the next line containing data.
        /// </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <returns> <c>true</c> if has new data or false it end of file or no data. </returns>
        public bool ReadFields()
        {
            this.FieldIndex = 0;
            this._Fields = Array.Empty<string>();
            if (!this.Parser.EndOfData)
            {
                this._Fields = this.Parser.ReadFields();
            }

            return this.HasFields;
        }

        /// <summary> Reads the next record and returns an array of doubles. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <param name="value"> The value. </param>
        /// <returns> The fields converted to <see cref="T:Double"/>. </returns>
        public double[] ReadFields(double value)
        {
            if ( this.ReadFields())
            {

                // allocate data array
                var data = new double[this._Fields.Length];
                for (int i = 0, loopTo = this._Fields.Length - 1; i <= loopTo; i++)
                    data[i] = this.ParseField(i, value);
                return data;
            }
            else
            {

                // return the empty array 
                var data = Array.Empty<double>();
                return data;
            }
        }

        /// <summary>
        /// Reads the next record and returns an array of doubles skipping an field that failed to parse.
        /// </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <param name="value"> The value. </param>
        /// <returns> The fields converted to <see cref="T:Double"/>. </returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Style", "IDE0060:Remove unused parameter", Justification = "<Pending>")]
        public double[] TryReadFields(double value)
        {
            if ( this.ReadFields())
            {

                // allocate data array
                var data = new double[this._Fields.Length];
                for (int i = 0, loopTo = this._Fields.Length - 1; i <= loopTo; i++)
                    _ = this.TryParseField( i, ref data[i] );
                return data;
            }
            else
            {

                // return the empty array 
                var data = Array.Empty<double>();
                return data;
            }
        }

        /// <summary>
        /// Reads a record of fields from the file. This read is contiguous--it requires that the line
        /// number of the record is contiguous with the previous line. Normally, the
        /// <see cref="Parser">parser</see> skips empty line reading from the next line with data. Skips
        /// to cursor to the next line containing data.
        /// </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <returns> <c>true</c> if has new data or false it end of file or no data. </returns>
        public bool ReadFieldsContiguous()
        {
            this.FieldIndex = 0;
            this._Fields = Array.Empty<string>();
            long currentLine = this.Parser.LineNumber;
            if (!this.Parser.EndOfData)
            {
                this._Fields = this.Parser.ReadFields();
                if ( this.Parser.LineNumber > currentLine + 1L)
                {
                    this._Fields = Array.Empty<string>();
                }
            }

            return this.HasFields;
        }

        /// <summary>
        /// Reads the next record and returns an array of doubles. This read is contiguous--it requires
        /// that the line number of the record is contiguous with the previous line. Normally, the
        /// <see cref="Parser">parser</see> skips empty line reading from the next line with data. Skips
        /// to cursor to the next line containing data.
        /// </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <param name="value"> The value. </param>
        /// <returns> The fields converted to <see cref="T:Double"/>. </returns>
        public double[] ReadFieldsContiguous(double value)
        {
            if ( this.ReadFieldsContiguous())
            {

                // allocate data array
                var data = new double[this._Fields.Length];
                for (int i = 0, loopTo = this._Fields.Length - 1; i <= loopTo; i++)
                    data[i] = this.ParseField(i, value);
                return data;
            }
            else
            {

                // return the empty array 
                var data = Array.Empty<double>();
                return data;
            }
        }

        /// <summary>
        /// Reads the next record and returns an array of doubles skipping an field that failed to parse.
        /// </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <param name="value"> The value. </param>
        /// <returns> The fields converted to <see cref="T:Double"/>. </returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Style", "IDE0060:Remove unused parameter", Justification = "<Pending>")]
        public double[] TryReadFieldsContiguous(double value)
        {
            if ( this.ReadFieldsContiguous())
            {

                // allocate data array
                var data = new double[this._Fields.Length];
                for (int i = 0, loopTo = this._Fields.Length - 1; i <= loopTo; i++)
                    _ = this.TryParseField( i, ref data[i] );
                return data;
            }
            else
            {

                // return the empty array 
                var data = Array.Empty<double>();
                return data;
            }
        }

        /// <summary> Skips the given line count. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <exception cref="System.IO.EndOfStreamException"> Thrown when the end of the stream was
        /// unexpectedly reached. </exception>
        /// <param name="lineCount"> Number of lines. </param>
        public void Skip(int lineCount)
        {
            while (lineCount > 0 && !this.Parser.EndOfData)
            {
                lineCount -= 1;
                _ = this.Parser.ReadLine();
            }

            if ( this.Parser.EndOfData && lineCount > 0)
            {
                throw new System.IO.EndOfStreamException("Tried to read past end of file when skipping lines");
            }
        }

        /// <summary> Skip until fields start with. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <exception cref="System.IO.EndOfStreamException"> Thrown when the end of the stream was
        /// unexpectedly reached. </exception>
        /// <param name="value"> A dummy value. </param>
        public void SkipUntilFieldsStartWith(string value)
        {
            bool found;
            do
            {
                _ = this.ReadFields();
                found = this.SelectField(0).StartsWith(value, StringComparison.OrdinalIgnoreCase);
            }
            while (!found && !this.Parser.EndOfData);
            if (!found)
            {
                throw new System.IO.EndOfStreamException(string.Format("Reached end of file before finding '{0}'", value));
            }
        }

        /// <summary> Skip until start with. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <exception cref="System.IO.EndOfStreamException"> Thrown when the end of the stream was
        /// unexpectedly reached. </exception>
        /// <param name="value"> A dummy value. </param>
        public void SkipUntilStartWith(string value)
        {
            bool found;
            do
                found = this.Parser.ReadLine().StartsWith(value, StringComparison.OrdinalIgnoreCase);
            while (!found && !this.Parser.EndOfData);
            if (!found)
            {
                throw new System.IO.EndOfStreamException(string.Format("Reached end of file before finding '{0}'", value));
            }
        }

        #endregion

        #region " PARSER: READ ROWS "

        /// <summary>
        /// Return file contiguous rows as a jagged two-dimensional array allowing each row to be of
        /// different length.
        /// </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <param name="value"> A dummy value. </param>
        /// <returns> Array of arrays. </returns>
        public double[][] ReadContiguousRows(double value)
        {
            var data = new double[1][];
            Array.Resize(ref data, 1000);
            var rowIndex = default(int);
            bool isRowEmpty = false;
            while (!(isRowEmpty || this.Parser.EndOfData))
            {
                var oneDimensionValues = this.ReadFieldsContiguous(value);
                isRowEmpty = oneDimensionValues is null || oneDimensionValues.Length == 0;
                if (!isRowEmpty)
                {
                    if (rowIndex >= data.GetLength(0))
                    {
                        Array.Resize(ref data, rowIndex + 1000 + 1);
                    }

                    data[rowIndex] = oneDimensionValues;
                    rowIndex += 1;
                }
            }

            Array.Resize(ref data, rowIndex);
            return data;
        }

        /// <summary>
        /// Returns file contiguous rows as a jagged two-dimensional array allowing each row to be of
        /// different length. Any field that failed to parse is skipped.
        /// </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <param name="value"> A dummy value. </param>
        /// <returns> Array of arrays. </returns>
        public double[][] TryReadContiguousRows(double value)
        {
            var data = new double[1][];
            Array.Resize(ref data, 1000);
            var rowIndex = default(int);
            bool isRowEmpty = false;
            while (!(isRowEmpty || this.Parser.EndOfData))
            {
                var oneDimensionValues = this.TryReadFieldsContiguous(value);
                isRowEmpty = oneDimensionValues is null || oneDimensionValues.Length == 0;
                if (!isRowEmpty)
                {
                    if (rowIndex >= data.GetLength(0))
                    {
                        Array.Resize(ref data, rowIndex + 1000 + 1);
                    }

                    data[rowIndex] = oneDimensionValues;
                    rowIndex += 1;
                }
            }

            Array.Resize(ref data, rowIndex);
            return data;
        }

        #endregion

        #region " PARSER "

        /// <summary> Returns <c>True</c> if the parser is open. </summary>
        /// <value> <c>IsOpen</c>Is a Boolean property. </value>
        public bool IsOpen => this.Parser is object;

        /// <summary>
        /// Returns reference to the
        /// <see cref="Microsoft.VisualBasic.FileIO.TextFieldParser">text field parser</see>
        /// for reading the file.
        /// </summary>
        /// <value> The parser. </value>
        public Microsoft.VisualBasic.FileIO.TextFieldParser Parser { get; private set; }

        #endregion

    }
}
