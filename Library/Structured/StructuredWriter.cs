using System;
using System.Collections;

namespace isr.IO
{

    /// <summary> Writes delimited files. </summary>
    /// <remarks>
    /// (c) 2006 Integrated Scientific Resources, Inc. All rights reserved. <para>
    /// Licensed under The MIT License. </para><para>
    /// David, 2006-04-20, 1.1.2301.x. </para>
    /// </remarks>
    public class StructuredWriter
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Constructs this class. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        public StructuredWriter() : this(StructuredReader.DefaultFilePathName)
        {
        }

        /// <summary> Constructs this class. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <param name="filePathName"> Specifies the file path name. </param>
        public StructuredWriter(string filePathName) : base()
        {
            this._FilePathName = filePathName;
            this.Delimiter = ",";
        }

        #endregion

        #region " FILE "

        /// <summary> Holds true if fields are to be enclosed in quotes. </summary>
        /// <value> The has fields enclosed in quotes. </value>
        public bool HasFieldsEnclosedInQuotes { get; set; }

        /// <summary> Full pathname of the file. </summary>
        private string _FilePathName = string.Empty;

        /// <summary> Gets or sets the file name. </summary>
        /// <remarks> Use this property to get or set the file name. </remarks>
        /// <value> <c>FilePathName</c> is a String property. </value>
        public string FilePathName
        {
            get {
                if ( this._FilePathName.Length == 0 )
                {
                    // set default file name if empty.
                    this._FilePathName = StructuredReader.DefaultFilePathName;
                }

                return this._FilePathName;
            }

            set => this._FilePathName = value;
        }

        #endregion

        #region " FIELDS "

        /// <summary> Adds a "T:String" value to the current <see cref="Fields">record</see>. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <param name="value"> Specifies the value to add to the record. </param>
        /// <returns> The added value. </returns>
        public string AddField(string value)
        {
            _ = this.Fields().Add( value );
            return value;
        }

        /// <summary>
        /// Adds a <see cref="T:Boolean"/> value to the current <see cref="Fields">record</see>.
        /// </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <param name="value"> Specifies the value to add to the record. </param>
        /// <returns> The added value converted to string. </returns>
        public string AddField(bool value)
        {
            return this.AddField(value.ToString(System.Globalization.CultureInfo.CurrentCulture));
        }

        /// <summary>
        /// Adds a <see cref="T:Byte"/> value to the current <see cref="Fields">record</see>.
        /// </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <param name="value"> Specifies the value to add to the record. </param>
        /// <returns> The added value converted to string. </returns>
        public string AddField(byte value)
        {
            return this.AddField(value.ToString(System.Globalization.CultureInfo.CurrentCulture));
        }

        /// <summary>
        /// Adds a <see cref="T:Byte"/> value to the current <see cref="Fields">record</see>.
        /// </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <param name="value">  Specifies the value to add to the record. </param>
        /// <param name="format"> Specifies the format to use when expressing the value. </param>
        /// <returns> The added value converted to string. </returns>
        public string AddField(byte value, string format)
        {
            return this.AddField(value.ToString(format, System.Globalization.CultureInfo.CurrentCulture));
        }

        /// <summary>
        /// Adds a <see cref="T:DateTime"/> value to the current <see cref="Fields">record</see>.
        /// </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <param name="value"> Specifies the value to add to the record. </param>
        /// <returns> The added value converted to string. </returns>
        public string AddField(DateTime value)
        {
            return this.AddField(value.ToString(System.Globalization.CultureInfo.CurrentCulture));
        }

        /// <summary>
        /// Adds a <see cref="T:DateTime"/> value to the current <see cref="Fields">record</see>.
        /// </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <param name="value">  Specifies the value to add to the record. </param>
        /// <param name="format"> Specifies the format to use when expressing the value. </param>
        /// <returns> The added value converted to string. </returns>
        public string AddField(DateTime value, string format)
        {
            return this.AddField(value.ToString(format, System.Globalization.CultureInfo.CurrentCulture));
        }

        /// <summary>
        /// Adds a "T:DateTimeOffset" value to the current <see cref="Fields">record</see>.
        /// </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <param name="value"> Specifies the value to add to the record. </param>
        /// <returns> The added value. </returns>
        public string AddField(DateTimeOffset value)
        {
            return this.AddField(value.ToString(System.Globalization.CultureInfo.CurrentCulture));
        }

        /// <summary>
        /// Adds a <see cref="T:DateTimeOffset"/> value to the current <see cref="Fields">record</see>.
        /// </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <param name="value">  Specifies the value to add to the record. </param>
        /// <param name="format"> Specifies the format to use when expressing the value. </param>
        /// <returns> The added value converted to string. </returns>
        public string AddField(DateTimeOffset value, string format)
        {
            return this.AddField(value.ToString(format, System.Globalization.CultureInfo.CurrentCulture));
        }

        /// <summary>
        /// Adds a <see cref="T:Decimal"/> value to the current <see cref="Fields">record</see>.
        /// </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <param name="value"> Specifies the value to add to the record. </param>
        /// <returns> The added value converted to string. </returns>
        public string AddField(decimal value)
        {
            return this.AddField(value.ToString(System.Globalization.CultureInfo.CurrentCulture));
        }

        /// <summary>
        /// Adds a <see cref="T:Decimal"/> value to the current <see cref="Fields">record</see>.
        /// </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <param name="value">  Specifies the value to add to the record. </param>
        /// <param name="format"> Specifies the format to use when expressing the value. </param>
        /// <returns> The added value converted to string. </returns>
        public string AddField(decimal value, string format)
        {
            return this.AddField(value.ToString(format, System.Globalization.CultureInfo.CurrentCulture));
        }

        /// <summary> Adds a "T:Double" value to the current <see cref="Fields">record</see>. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <param name="value"> Specifies the value to add to the record. </param>
        /// <returns> The added value converted to string. </returns>
        public string AddField(double value)
        {
            return this.AddField(value.ToString(System.Globalization.CultureInfo.CurrentCulture));
        }

        /// <summary> Adds a "T:Double" value to the current <see cref="Fields">record</see>. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <param name="value">  Specifies the value to add to the record. </param>
        /// <param name="format"> Specifies the format to use when expressing the value. </param>
        /// <returns> The added value converted to string. </returns>
        public string AddField(double value, string format)
        {
            return this.AddField(value.ToString(format, System.Globalization.CultureInfo.CurrentCulture));
        }

        /// <summary>
        /// Adds a <see cref="T:Int64"/> value to the current <see cref="Fields">record</see>.
        /// </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <param name="value"> Specifies the value to add to the record. </param>
        /// <returns> The added value converted to string. </returns>
        public string AddField(long value)
        {
            return this.AddField(value.ToString(System.Globalization.CultureInfo.CurrentCulture));
        }

        /// <summary>
        /// Adds a <see cref="T:Int64"/> value to the current <see cref="Fields">record</see>.
        /// </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <param name="value">  Specifies the value to add to the record. </param>
        /// <param name="format"> Specifies the format to use when expressing the value. </param>
        /// <returns> The added value converted to string. </returns>
        public string AddField(long value, string format)
        {
            return this.AddField(value.ToString(format, System.Globalization.CultureInfo.CurrentCulture));
        }

        /// <summary>
        /// Adds a <see cref="T:Single"/> value to the current <see cref="Fields">record</see>.
        /// </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <param name="value"> Specifies the value to add to the record. </param>
        /// <returns> The added value converted to string. </returns>
        public string AddField(float value)
        {
            return this.AddField(value.ToString(System.Globalization.CultureInfo.CurrentCulture));
        }

        /// <summary>
        /// Adds a <see cref="T:Single"/> value to the current <see cref="Fields">record</see>.
        /// </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <param name="value">  Specifies the value to add to the record. </param>
        /// <param name="format"> Specifies the format to use when expressing the value. </param>
        /// <returns> The added value converted to string. </returns>
        public string AddField(float value, string format)
        {
            return this.AddField(value.ToString(format, System.Globalization.CultureInfo.CurrentCulture));
        }

        #endregion

        #region " FIELDS METHODS AND PROPERTIES "

        /// <summary> Holds the file delimiter. </summary>
        /// <value> The delimiter. </value>
        public string Delimiter { get; set; }

        /// <summary> The fields. </summary>
        private ArrayList _Fields;

        /// <summary> Holds "T:ArrayList" of field values. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <returns> The list of field values. </returns>
        public ArrayList Fields()
        {
            if ( this._Fields is null)
            {
                this._Fields = new ArrayList();
            }

            return this._Fields;
        }

        /// <summary> Builds the current record for saving to the file. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <param name="newLine"> Specifies if the record is being appended as a new line. </param>
        /// <returns> The delimited record. </returns>
        public string BuildRecord(bool newLine)
        {
            var record = new System.Text.StringBuilder();
            foreach (string field in this.Fields())
            {
                if (record.Length > 0)
                {
                    _ = record.Append( this.Delimiter );
                }

                _ = this.HasFieldsEnclosedInQuotes
                    ? record.AppendFormat( System.Globalization.CultureInfo.CurrentCulture, "\"{0}\"", field )
                    : record.Append( field );
            }

            if (newLine)
            {
                _ = record.Insert( 0, Environment.NewLine );
            }

            return record.ToString();
        }

        /// <summary> <c>true</c> if this object is new. </summary>
        private bool _IsNew;

        /// <summary> Tag the next write as creating a new file. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        public void NewFile()
        {
            this._IsNew = true;
        }

        /// <summary> Creates a new record. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        public void NewRecord()
        {
            this._Fields = new ArrayList();
        }

        /// <summary> Writes the current fields record to the file. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        public void WriteFields()
        {
            // append to he file if it exists and not requesting a new file.
            bool append = !this._IsNew && My.MyProject.Computer.FileSystem.FileExists( this.FilePathName );
            My.MyProject.Computer.FileSystem.WriteAllText( this.FilePathName, this.BuildRecord(append), append);
            // toggle the new flag so as the append from now on.
            this._IsNew = false;
        }

        #endregion

    }
}
