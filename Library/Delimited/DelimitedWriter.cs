using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Microsoft.VisualBasic.CompilerServices;

namespace isr.IO
{

    /// <summary> This is the writer class of the delimited file class library. </summary>
    /// <remarks>
    /// (c) 2004 Integrated Scientific Resources, Inc. All rights reserved. <para>
    /// Licensed under The MIT License. </para><para>
    /// David, 2004-03-04, 1.0.1524.x. </para>
    /// </remarks>
    public class DelimitedWriter : DelimitedFileBase
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Constructs this class. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        public DelimitedWriter() : base()
        {
            this.Delimiter = DefaultDelimiter;
        }

        /// <summary>   Constructs this class. </summary>
        /// <remarks>   David, 2021-04-22. </remarks>
        /// <param name="fileName"> Filename of the file. </param>
        public DelimitedWriter( string fileName ) : base( fileName )
        {
            this.Delimiter = DefaultDelimiter;
        }

        #endregion

        #region " FILE DIALOG "

        /// <summary> Opens the File Save dialog box and gets a file name. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <returns> A Windows.Forms.DialogResult. </returns>
        public override bool OpenFileDialog()
        {
            bool result;
            var Ofd = new Core.SaveFileDialog()
            {
                FileDialogTitle = FileDialogTitle,
                FilePathName = FilePathName,
                FileDialogFilter = FileDialogFilter,
                FileExtension = FileExtension
            };
            result = Ofd.TrySelectFile();
            if (result)
                this.FilePathName = Ofd.FilePathName;
            return result;
        }

        #endregion

        #region " WRITE VALUE "

        /// <summary> Gets or sets the tab delimited. </summary>
        /// <value> The tab delimited. </value>
        public bool TabDelimited
        {
            get => string.Equals( Conversions.ToString( this.Delimiter ), Microsoft.VisualBasic.Constants.vbTab );

            set => this.Delimiter = value ? Convert.ToChar( Microsoft.VisualBasic.Constants.vbTab ) : DefaultDelimiter;
        }

        /// <summary> The default delimiter. </summary>
        public const char DefaultDelimiter = ',';

        /// <summary> Gets the delimiter. </summary>
        /// <value> The delimiter. </value>
        public char Delimiter { get; set; }

        /// <summary> Writes an end of record (CR LF) to the file. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        [System.Security.Permissions.FileIOPermission(System.Security.Permissions.SecurityAction.Demand, Unrestricted = true)]
        public void EndRecord()
        {
            Microsoft.VisualBasic.FileSystem.WriteLine( this.FileNumber );
        }

        /// <summary>
        /// Gets or set limiting output data with commas and notation marks such as quotes for text. With
        /// header sections, such notation needs to be turned off.
        /// </summary>
        /// <value> The is automatic delimited. </value>
        public bool IsAutoDelimited { get; set; }

        /// <summary> Returns the last value written. </summary>
        /// <value> The last value. </value>
        public object LastValue { get; private set; } = string.Empty;

        /// <summary>
        /// Writes a value to the delimited file,  limiting output data with commas and notation marks
        /// such as quotes for text.
        /// </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <param name="value"> The value to write. </param>
        [System.Security.Permissions.FileIOPermission(System.Security.Permissions.SecurityAction.Demand, Unrestricted = true)]
        public void Write(object value)
        {
            this.LastValue = value;
            if ( this.IsAutoDelimited )
            {
                if ( this.TabDelimited )
                {
                    Microsoft.VisualBasic.FileSystem.Print( this.FileNumber, value);
                    Microsoft.VisualBasic.FileSystem.Print( this.FileNumber, (object) this.Delimiter );
                }
                else
                {
                    Microsoft.VisualBasic.FileSystem.Write( this.FileNumber, value);
                }
            }
            else
            {
                Microsoft.VisualBasic.FileSystem.Print( this.FileNumber, value);
                Microsoft.VisualBasic.FileSystem.Print( this.FileNumber, (object) this.Delimiter );
            }
        }

        /// <summary> Writes a value to the delimited file and holds this value. </summary>
        /// <remarks>
        /// The end-of-record indicator controls how the object handles writing.  When on, the writer
        /// will insert end of record character (CR LF) instead of a delimiter.
        /// </remarks>
        /// <param name="value">       The value to write. </param>
        /// <param name="endOfRecord"> true to end this record after writing the value. </param>
        [System.Security.Permissions.FileIOPermission(System.Security.Permissions.SecurityAction.Demand, Unrestricted = true)]
        public void Write(object value, bool endOfRecord)
        {
            this.LastValue = value;
            if (endOfRecord)
            {
                if ( this.IsAutoDelimited )
                {
                    Microsoft.VisualBasic.FileSystem.WriteLine( this.FileNumber, value);
                }
                else
                {
                    Microsoft.VisualBasic.FileSystem.PrintLine( this.FileNumber, value);
                }
            }
            else
            {
                this.Write(value);
            }
        }

        #endregion

        #region " WRITE ROWS "

        /// <summary> Writes a single dimension array to the file. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <exception cref="ArgumentNullException" guarantee="strong"> Values is Nothing. </exception>
        /// <param name="values"> Contains the array values. </param>
        [System.Security.Permissions.FileIOPermission(System.Security.Permissions.SecurityAction.Demand, Unrestricted = true)]
        public void WriteRow(double[] values)
        {
            if (values is null)
                throw new ArgumentNullException(nameof(values));
            if (values.Any())
            {
                var stringRecord = new System.Text.StringBuilder();
                for (int i = values.GetLowerBound(0), loopTo = values.GetUpperBound(0); i <= loopTo; i++)
                {
                    if (i > 0)
                    {
                        _ = stringRecord.Append( this.Delimiter );
                    }

                    _ = stringRecord.Append( values[i].ToString( System.Globalization.CultureInfo.CurrentCulture ) );
                }

                Microsoft.VisualBasic.FileSystem.PrintLine( this.FileNumber, stringRecord.ToString());
            }
        }

        /// <summary> Writes a two-dimensional array to the file. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <exception cref="ArgumentNullException" guarantee="strong"> Values is Nothing. </exception>
        /// <param name="values"> Contains the array values to write.  Array rows are written to the file
        /// as comma-separated successive lines. </param>
        [System.Security.Permissions.FileIOPermission(System.Security.Permissions.SecurityAction.Demand, Unrestricted = true)]
        public void WriteColumns(double[,] values)
        {
            if (values is null)
                throw new ArgumentNullException(nameof(values));
            if (values.Length > 0)
            {
                for (int i = values.GetLowerBound(0), loopTo = values.GetUpperBound(0); i <= loopTo; i++)
                {
                    var rowRecord = new double[values.GetUpperBound(1) - values.GetLowerBound(1) + 1 + 1];
                    for (int j = values.GetLowerBound(1), loopTo1 = values.GetUpperBound(1); j <= loopTo1; j++)
                        rowRecord[j] = values[i, j];
                    this.WriteRow(rowRecord);
                }
            }
        }

        /// <summary> Writes a two-dimensional array to the file. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <exception cref="ArgumentNullException" guarantee="strong"> Values is Nothing. </exception>
        /// <param name="values"> Contains the array values to write.  Array rows are written to the file
        /// as comma-separated successive lines. </param>
        [System.Security.Permissions.FileIOPermission(System.Security.Permissions.SecurityAction.Demand, Unrestricted = true)]
        public void WriteRows(double[,] values)
        {
            if (values is null)
                throw new ArgumentNullException(nameof(values));
            if (values.Length > 0)
            {
                for (int i = values.GetLowerBound(1), loopTo = values.GetUpperBound(1); i <= loopTo; i++)
                {
                    var rowRecord = new double[values.GetUpperBound(0) - values.GetLowerBound(0) + 1 + 1];
                    for (int j = values.GetLowerBound(0), loopTo1 = values.GetUpperBound(0); j <= loopTo1; j++)
                        rowRecord[j] = values[j, i];
                    this.WriteRow(rowRecord);
                }
            }
        }

        /// <summary> Writes a jagged two-dimensional array to the file. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <exception cref="ArgumentNullException" guarantee="strong"> Values is Nothing. </exception>
        /// <param name="values"> Contains the array values to write.  Array rows are written to the file
        /// as comma-separated successive lines. </param>
        [CLSCompliant(false)]
        [System.Security.Permissions.FileIOPermission(System.Security.Permissions.SecurityAction.Demand, Unrestricted = true)]
        public void WriteColumns(double[][] values)
        {
            if (values is null)
                throw new ArgumentNullException(nameof(values));
            if (values.Length > 0)
            {
                for (int j = values.GetLowerBound(1), loopTo = values.GetUpperBound(1); j <= loopTo; j++)
                {
                    var rowRecord = new double[values.GetUpperBound(0) - values.GetLowerBound(0) + 1 + 1];
                    for (int i = values.GetLowerBound(0), loopTo1 = values.GetUpperBound(0); i <= loopTo1; i++)
                        rowRecord[i] = values[i][j];
                    this.WriteRow(rowRecord);
                }
            }
        }

        /// <summary> Writes a jagged two-dimensional array to the file in columns. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <exception cref="ArgumentNullException" guarantee="strong"> Values is Nothing. </exception>
        /// <param name="values"> Contains the array values to write.  Array rows are written to the file
        /// as comma-separated successive lines. </param>
        [CLSCompliant( false )]
        [System.Security.Permissions.FileIOPermission(System.Security.Permissions.SecurityAction.Demand, Unrestricted = true)]
        public void WriteRows(double[][] values)
        {
            if (values is null)
                throw new ArgumentNullException(nameof(values));
            if (values.Length > 0)
            {
                for (int i = values.GetLowerBound(0), loopTo = values.GetUpperBound(0); i <= loopTo; i++)
                    this.WriteRow(values[i]);
            }
        }

        #endregion

        #region " WRITE DATA TABLE "

        /// <summary> Writes a data table. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="sourceTable">    Source table. </param>
        /// <param name="writer">         The writer. </param>
        /// <param name="includeHeaders"> True to include, false to exclude the headers. </param>
        public static void WriteDataTable(DataTable sourceTable, System.IO.TextWriter writer, bool includeHeaders)
        {
            if (writer is null)
                throw new ArgumentNullException(nameof(writer));
            if (sourceTable is null)
                throw new ArgumentNullException(nameof(sourceTable));
            if (includeHeaders)
            {
                var headerValues = sourceTable.Columns.OfType<DataColumn>().Select(column => QuoteValue(column.ColumnName));
                writer.WriteLine(string.Join(",", headerValues));
            }

            IEnumerable<string> items = null;
            foreach (DataRow row in sourceTable.Rows)
            {
                items = row.ItemArray.Select(o => QuoteValue(o));
                // items = row.ItemArray.Select(Function(o) QuoteValue(If(o?.ToString(), String.Empty)))
                writer.WriteLine(string.Join(",", items));
            }

            writer.Flush();
        }

        /// <summary> Quote value. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <param name="value"> The value to write. </param>
        /// <returns> A String. </returns>
        private static string QuoteValue(object value)
        {
            return value is null ? string.Empty : value is string ? QuoteValue(value.ToString()) : value.ToString();
        }

        /// <summary> Quote value. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="value"> The value to write. </param>
        /// <returns> A String. </returns>
        private static string QuoteValue(string value)
        {
            return value is null ? throw new ArgumentNullException(nameof(value)) : string.Concat("\"", value.Replace("\"", "\"\""), "\"");
        }

        /// <summary> Writes a data table. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="sourceTable">    Source table. </param>
        /// <param name="includeHeaders"> True to include, false to exclude the headers. </param>
        public void WriteDataTable(DataTable sourceTable, bool includeHeaders)
        {
            if (sourceTable is null)
                throw new ArgumentNullException(nameof(sourceTable));
            using var writer = My.MyProject.Computer.FileSystem.OpenTextFileWriter( this.FilePathName, false );
            WriteDataTable( sourceTable, writer, includeHeaders );
        }

        #endregion

    }
}
