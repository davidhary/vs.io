using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Text.RegularExpressions;

namespace isr.IO
{

    /// <summary>
    /// Class for transforming a text-file into a dataset based on one regular expression.
    /// </summary>
    /// <remarks>
    /// (c) 2017 Christiaan van Bergen. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2017-03-27. </para><para>
    /// https://www.codeproject.com/Articles/22400/Converting-text-files-CSV-to-datasets.
    /// https://www.codeproject.com/script/Membership/View.aspx?mid=325286 </para>
    /// </remarks>
    [System.Diagnostics.CodeAnalysis.SuppressMessage("Usage", "CA2237:Mark ISerializable types with serializable",
        Justification = "[Serializable] relates essentially just to BinaryFormatter, which usually isn't a good choice." )]
    public partial class DelimitedFileDataSet : DataSet
    {

        /// <summary> Name of the new. </summary>
        private const string _NewName = "NewName";

        /// <summary> The default group. </summary>
        private const string _DefaultGroup = "0";

        /// <summary> The default table name. </summary>
        private const string _DefaultTableName = "Table1";

        /// <summary> The RegEx columns. </summary>
        private List<RegexColumn> _RegexColumns;

        /// <summary>
        /// Set the RegexColumnBuilder on setting this the columns and their RegexColumnTypes are set as
        /// is the complete ContentExpression.
        /// </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="value"> The value. </param>
        public void ApplyColumnBuilder(RegexColumnBuilder value)
        {
            if (value is null)
                throw new ArgumentNullException(nameof(value));
            this._RegexColumns = new List<RegexColumn>(value.Columns);
            this.ContentExpression = value.CreateRegularExpression();
        }

        /// <summary> The content RegEx. </summary>
        private Regex _ContentRegex;

        /// <summary>
        /// Regular Expression that is used for validating text lines and defining the column names of
        /// the dataset.
        /// </summary>
        /// <value> The content expression. </value>
        public Regex ContentExpression
        {
            get => this._ContentRegex;

            set {
                if ( value is object && (this.ContentExpression is null || !Equals( value, this.ContentExpression )) )
                {
                    this._ContentRegex = value;
                    this.ContentExpressionHasChanged = true;
                }
            }
        }

        /// <summary> Gets the content expression has changed. </summary>
        /// <value> The content expression has changed. </value>
        private bool ContentExpressionHasChanged { get; set; }

        /// <summary>
        /// The regular expression used for handling a first row that could contain column headers. If
        /// you do not have a first row with headers use UseFirstRowNamesAsColumnNames=false, or if you
        /// do have a row with headers but don't want to use them use: SkipFirstRow=true.
        /// </summary>
        /// <value> The first row expression. </value>
        public Regex FirstRowExpression { get; set; }

        /// <summary>
        /// When set to true the values in the first match made are used as column names instead of the
        /// ones supplied in te regular expression.
        /// </summary>
        /// <value> A list of names of the use first row names as columns. </value>
        public bool UseFirstRowNamesAsColumnNames { get; set; } = false;

        /// <summary> Gets the numbers of first rows to skip. </summary>
        /// <value> The number of skip first rows. </value>
        public int SkipFirstRowCount { get; set; }

        /// <summary>
        /// The name the data table in the dataset should get or the name of the data table to use when a
        /// dataset is provided.
        /// </summary>
        /// <value> The name of the table. </value>
        public string TableName { get; set; }

        /// <summary> The text file to be read. </summary>
        /// <value> The text file. </value>
        public Stream TextFile { get; set; }

        /// <summary> Gets the data table. </summary>
        /// <value> The data table. </value>
        private DataTable DataTable
        {
            get
            {
                if (string.IsNullOrWhiteSpace( this.TableName ) )
                    this.TableName = _DefaultTableName;
                if (!this.Tables.Contains( this.TableName ))
                {
                    var newTable = new DataTable( this.TableName ) { Locale = System.Globalization.CultureInfo.CurrentCulture };
                    this.Tables.Add(newTable);
                }

                return this.Tables[this.TableName];
            }
        }

        /// <summary> Lines in the text file that did not match the regular expression. </summary>
        /// <value> The misreads. </value>
        public IList<string> Misreads { get; private set; }

        /// <summary> Adds the mis read. </summary>
        /// <remarks> David, 2020-09-07. </remarks>
        /// <param name="missRead"> The miss read. </param>
        private void AddMisRead(string missRead)
        {
            if ( this.Misreads is null)
            {
                this.Misreads = new List<string>();
            }

            this.Misreads.Add(missRead);
        }

        /// <summary> Removes the data table. </summary>
        /// <remarks> David, 2020-09-07. </remarks>
        private void RemoveDataTable()
        {
            if ( this.Tables.Contains( this.TableName ))
            {
                this.Tables.Remove( this.TableName );
            }
        }

        /// <summary> Builds RegEx schema into data set. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        private void BuildRegexSchemaIntoDataSet()
        {
            if ( this.ContentExpression is object || this.ContentExpressionHasChanged )
            {
                this.RemoveDataTable();
                foreach (string sGroup in this.ContentExpression.GetGroupNames())
                {
                    if ((sGroup ?? "") != _DefaultGroup)
                    {
                        var newDc = new DataColumn() { DataType = typeof(string) };
                        if ( this._RegexColumns is object)
                        {
                            foreach (RegexColumn r in this._RegexColumns )
                            {
                                if ((r.ColumnName ?? "") == (sGroup ?? ""))
                                {
                                    newDc.DataType = r.ColumnTypeAsType;
                                    break;
                                }
                            }
                        }

                        newDc.ColumnName = sGroup;
                        this.DataTable.Columns.Add(newDc);
                    }
                }
            }
        }

        /// <summary>
        /// Reads every line in the text file and tries to match it with the given regular expression.
        /// Every match will be placed as a new row in the data table.
        /// </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <param name="textFile">          . </param>
        /// <param name="regularExpression"> . </param>
        /// <param name="tableName">         . </param>
        public void Fill(Stream textFile, Regex regularExpression, string tableName)
        {
            this.TableName = tableName;
            this.Fill(textFile, regularExpression);
        }

        /// <summary>
        /// Reads every line in the text file and tries to match it with the given regular expression.
        /// Every match will be placed as a new row in the data table.
        /// </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <param name="textFile">          . </param>
        /// <param name="regularExpression"> . </param>
        public void Fill(Stream textFile, Regex regularExpression)
        {
            this.ContentExpression = regularExpression;
            this.Fill(textFile);
        }

        /// <summary>
        /// Reads every line in the text file and tries to match it with the given regular expression.
        /// Every match will be placed as a new row in the data table.
        /// </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <param name="textFile"> . </param>
        public void Fill(Stream textFile)
        {
            this.TextFile = textFile;
            this.Fill();
        }

        /// <summary>
        /// Reads every line in the text file and tries to match it with the given regular expression.
        /// Every match will be placed as a new row in the Data Table.
        /// </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
        public void Fill()
        {
            this.BuildRegexSchemaIntoDataSet();
            if ( this.TextFile is null)
                throw new InvalidOperationException("No stream available to convert to a DataSet");
            _ = this.TextFile.Seek( 0L, SeekOrigin.Begin );
            var sr = new StreamReader( this.TextFile );
            bool isFirstLine = true;
            int rowNumber = 0;
            while (!sr.EndOfStream)
            {
                string line = sr.ReadLine();
                rowNumber += 1;

                // allow skipping empty lines.
                if (string.IsNullOrWhiteSpace(line))
                    continue;
                if (rowNumber <= this.SkipFirstRowCount )
                    continue;
                if (isFirstLine && this.UseFirstRowNamesAsColumnNames )
                {
                    if ( this.FirstRowExpression is null)
                    {
                        throw new InvalidOperationException("'First Row Expression' is not set, but 'Use First Row Names As Column Names' is set to true");
                    }

                    if (!this.FirstRowExpression.IsMatch(line))
                    {
                        throw new InvalidOperationException("The first row in the file does not match the 'First Row Expression'");
                    }

                    var m = this.FirstRowExpression.Match(line);
                    foreach (string sGroup in this.FirstRowExpression.GetGroupNames())
                    {
                        if ((sGroup ?? "") != _DefaultGroup)
                        {
                            this.DataTable.Columns[sGroup].ExtendedProperties.Add(_NewName, m.Groups[sGroup].Value);
                        }
                    }
                }
                else if ( this.ContentExpression.IsMatch(line))
                {
                    var m = this.ContentExpression.Match(line);
                    var newRow = this.DataTable.NewRow();
                    foreach (string sGroup in this.ContentExpression.GetGroupNames())
                    {
                        if ((sGroup ?? "") != _DefaultGroup)
                        {
                            newRow[sGroup] = ReferenceEquals(newRow.Table.Columns[sGroup].DataType, typeof(int))
                                ? Convert.ToInt32(m.Groups[sGroup].Value)
                                : ReferenceEquals( newRow.Table.Columns[sGroup].DataType, typeof( double ) )
                                    ? Convert.ToDouble( m.Groups[sGroup].Value )
                                    : ReferenceEquals( newRow.Table.Columns[sGroup].DataType, typeof( DateTime ) )
                                                                    ? Convert.ToDateTime( m.Groups[sGroup].Value )
                                                                    : ( object ) m.Groups[sGroup].Value;
                        }
                    }

                    this.DataTable.Rows.Add(newRow);
                }
                else if (!isFirstLine)
                {
                    this.AddMisRead(line);
                }

                isFirstLine = false;
                isr.Core.ApplianceBase.DoEvents();
            }

            if ( this.UseFirstRowNamesAsColumnNames )
            {
                foreach (DataColumn column in this.DataTable.Columns)
                {
                    if (column.ExtendedProperties.ContainsKey(_NewName))
                    {
                        column.ColumnName = column.ExtendedProperties[_NewName].ToString();
                    }
                }
            }
        }
    }
}
