
namespace isr.IO
{
    /// <summary> This is the base class for the Delimited file class library. </summary>
    /// <remarks>
    /// (c) 2004 Integrated Scientific Resources, Inc. All rights reserved. <para>
    /// Licensed under The MIT License. </para><para>
    /// David, 2004-03-04, 1.0.1524.x. </para>
    /// </remarks>
    public abstract class DelimitedFileBase
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Constructs this class. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        protected DelimitedFileBase() : this(string.Empty)
        {
        }

        /// <summary>   Constructs this class. </summary>
        /// <remarks>   David, 2021-04-20. </remarks>
        /// <param name="fileName"> Filename of the file. </param>
        protected DelimitedFileBase(string fileName) : base()
        {
            this._FilePathName = fileName;
            this.FileExtension = ".CSV";
            this.FileDialogFilter = "Text files (*.txt;*.csv;*.log)|*.txt;*.csv;*.log|All files (*.*)|*.*";
            this.FileDialogTitle = "Select File";
        }

        #endregion

        #region " FILE DIALOG "

        /// <summary> Gets or sets the file dialog filter. </summary>
        /// <value> The file dialog filter. </value>
        public string FileDialogFilter { get; set; }

        /// <summary> Gets or sets the file dialog title. </summary>
        /// <value> The file dialog title. </value>
        public string FileDialogTitle { get; set; }

        /// <summary> Opens the File dialog box and gets a file name. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <returns> <c>True</c> of selected a file; <c>False</c> if user canceled. </returns>
        public abstract bool OpenFileDialog();

        /// <summary> Try find file. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <param name="fileName"> Filename of the file. </param>
        /// <returns> A String if file found or empty if not. </returns>
        public static string TryFindFile(string fileName)
        {
            return Core.OpenFileDialog.TryFindFile(fileName);
        }

#endregion

        #region " OPEN AND CLOSE "

        /// <summary> opens the file for input. </summary>
        /// <remarks> Use this method to open the instance. </remarks>
        [System.Security.Permissions.FileIOPermission(System.Security.Permissions.SecurityAction.Demand, Unrestricted = true)]
        public void Open()
        {
            try
            {
                // open the file for reading
                this.FileNumber = NewFileNumber();
                // TO_DO: Use task; See VI Session Base Await Service request
                int trialCount = 10;
                while (trialCount > 0)
                {
                    trialCount -= 1;
                    try
                    {
                        Microsoft.VisualBasic.FileSystem.FileOpen( this.FileNumber, this.FilePathName, Microsoft.VisualBasic.OpenMode.Input);
                        trialCount = 0;
                        break;
                    }
                    catch
                    {
                        if (trialCount == 1)
                            throw;
                    }

                    if (trialCount > 0)
                    {
                        isr.Core.ApplianceBase.DoEvents();
                        isr.Core.ApplianceBase.Delay(100d);
                    }
                }
            }
            catch
            {
                // close to meet strong guarantees
                try
                {
                    this.Close();
                }
                finally
                {
                }

                throw;
            }
            finally
            {
                isr.Core.ApplianceBase.DoEvents();
            }
        }

        /// <summary> Open for output. </summary>
        /// <remarks> Use this method to open the instance.  The method Returns <c>True</c> if success or
        /// false if it failed opening the file. </remarks>
        /// <param name="append"> A Boolean expression that specifies if the new data will append to an
        /// existing file or if a new file will be Created possibly over-writing an existing file. </param>
        [System.Security.Permissions.FileIOPermission(System.Security.Permissions.SecurityAction.Demand, Unrestricted = true)]
        public void Open(bool append)
        {
            try
            {
                this.FileNumber = NewFileNumber();
                // TO_DO: Use task; See VI Session Base Await Service request
                int trialCount = 10;
                while (trialCount > 0)
                {
                    trialCount -= 1;
                    try
                    {
                        if (append)
                        {
                            Microsoft.VisualBasic.FileSystem.FileOpen( this.FileNumber, this.FilePathName, Microsoft.VisualBasic.OpenMode.Append);
                        }
                        else
                        {
                            Microsoft.VisualBasic.FileSystem.FileOpen( this.FileNumber, this.FilePathName, Microsoft.VisualBasic.OpenMode.Output);
                        }

                        trialCount = 0;
                        break;
                    }
                    catch
                    {
                        if (trialCount == 1)
                            throw;
                    }

                    if (trialCount > 0)
                    {
                        isr.Core.ApplianceBase.DoEvents();
                        isr.Core.ApplianceBase.Delay(100d);
                    }
                }
            }
            catch
            {
                // close to meet strong guarantees
                try
                {
                    this.Close();
                }
                finally
                {
                }

                throw;
            }
            finally
            {
                isr.Core.ApplianceBase.DoEvents();
            }
        }

        /// <summary> Open for output. </summary>
        /// <param name="openMode"> Specifies how the file is to be opened. </param>
        /// <remarks> Use this method to open the instance.  The method Returns <c>True</c> if success or
        /// false if it failed opening the file. </remarks>
        [System.Security.Permissions.FileIOPermission(System.Security.Permissions.SecurityAction.Demand, Unrestricted = true)]
        public void Open(Microsoft.VisualBasic.OpenMode openMode)
        {
            try
            {
                this.FileNumber = NewFileNumber();
                // TO_DO: Use task; See VI Session Base Await Service request
                int trialCount = 10;
                while (trialCount > 0)
                {
                    trialCount -= 1;
                    try
                    {
                        Microsoft.VisualBasic.FileSystem.FileOpen( this.FileNumber, this.FilePathName, openMode);
                        trialCount = 0;
                        break;
                    }
                    catch
                    {
                        if (trialCount == 1)
                            throw;
                    }

                    if (trialCount > 0)
                    {
                        isr.Core.ApplianceBase.DoEvents();
                        isr.Core.ApplianceBase.Delay(100d);
                    }
                }
            }
            catch
            {
                // close to meet strong guarantees
                try
                {
                    this.Close();
                }
                finally
                {
                }

                throw;
            }
            finally
            {
                isr.Core.ApplianceBase.DoEvents();
            }
        }

        /// <summary> Closes the instance. </summary>
        /// <remarks> Use this method to close the instance.  The method Returns <c>True</c> if success or
        /// false if it failed closing the instance. </remarks>
        [System.Security.Permissions.FileIOPermission(System.Security.Permissions.SecurityAction.Demand, Unrestricted = true)]
        public virtual void Close()
        {
            // close the file if not closed
            if ( this.IsOpen )
            {
                Microsoft.VisualBasic.FileSystem.FileClose( this.FileNumber );
                // zero the file number to indicate that the file is closed.
                this.FileNumber = 0;
            }
        }

        #endregion

        #region " IO "

        /// <summary> Determines whether the specified folder path is writable. </summary>
        /// <remarks>
        /// Uses a temporary random file name to test if the file can be created. The file is deleted
        /// thereafter.
        /// </remarks>
        /// <param name="path"> The path. </param>
        /// <returns> <c>True</c> if the specified path is writable; otherwise, <c>False</c>. </returns>
        public static bool IsFolderWritable(string path)
        {
            string filePath = string.Empty;
            bool affirmative = false;
            try
            {
                filePath = System.IO.Path.Combine(path, System.IO.Path.GetRandomFileName());
                using (var s = System.IO.File.Open(filePath, System.IO.FileMode.OpenOrCreate))
                {
                }

                affirmative = true;
            }
            catch
            {
            }
            finally
            {
                // SS reported an exception from this test possibly indicating that Windows allowed writing the file 
                // by failed report deletion. Or else, Windows raised another exception type.
                try
                {
                    if (System.IO.File.Exists(filePath))
                    {
                        System.IO.File.Delete(filePath);
                    }
                }
                catch
                {
                }
            }

            return affirmative;
        }

        /// <summary> Selects the default file path. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <returns>
        /// The default file path: either the application folder or the user documents folder.
        /// </returns>
        public static string DefaultFolderPath()
        {
            string candidatePath = My.MyProject.Application.Info.DirectoryPath;
            if (!IsFolderWritable(candidatePath))
            {
                candidatePath = My.MyProject.Computer.FileSystem.SpecialDirectories.MyDocuments;
            }

            return candidatePath;
        }

        #endregion

        #region " SAFE DELETE "

        /// <summary> Deletes the file described by fileName. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <param name="fileName"> Filename of the file. </param>
        public static void DeleteFile(string fileName)
        {
            if (My.MyProject.Computer.FileSystem.FileExists(fileName))
            {
                My.MyProject.Computer.FileSystem.DeleteFile(fileName);
                isr.Core.ApplianceBase.DoEvents();
                isr.Core.ApplianceBase.Delay(100d);
            }
        }

        #endregion

        #region " PROPERTIES "

        /// <summary> Gets or sets the file extension. </summary>
        /// <value> The file extension. </value>
        public string FileExtension { get; set; }

        /// <summary> Full pathname of the file. </summary>
        private string _FilePathName;

        /// <summary> Gets or sets the file name. </summary>
        /// <remarks> Use this property to get or set the file name. </remarks>
        /// <value> <c>FilePathName</c> is a String property. </value>
        public string FilePathName
        {
            get {
                if ( this._FilePathName.Length == 0 )
                {
                    // set default file name if empty.
                    this._FilePathName = System.IO.Path.Combine( DefaultFolderPath(), My.MyProject.Application.Info.AssemblyName + this.FileExtension );
                }

                return this._FilePathName;
            }

            set => this._FilePathName = value;
        }

        /// <summary> Gets the file number. </summary>
        /// <value> <c>FileNumber</c>is an Int32 property. </value>
        public int FileNumber { get; set; }

        /// <summary> Returns <c>True</c> if the file is at end of file. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <returns> <c>True</c> if end of file. </returns>
        [System.Security.Permissions.FileIOPermission(System.Security.Permissions.SecurityAction.Demand, Unrestricted = true)]
        public bool IsEndOfFile()
        {
            return Microsoft.VisualBasic.FileSystem.EOF( this.FileNumber );
        }

        /// <summary> Returns <c>True</c> if the file is open. </summary>
        /// <remarks> The file is open if the file number is not zero. </remarks>
        /// <value> <c>IsOpen</c>Is a Boolean property. </value>
        public bool IsOpen => this.FileNumber != 0;

        /// <summary>
        /// The property returns a new free file number that can be used to open a file.
        /// </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <returns> A new free file number that can be used to open a file. </returns>
        [System.Security.Permissions.FileIOPermission(System.Security.Permissions.SecurityAction.Demand, Unrestricted = true)]
        public static int NewFileNumber()
        {
            return Microsoft.VisualBasic.FileSystem.FreeFile();
        }

        #endregion

    }
}
