using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace isr.IO
{

    /// <summary>
    /// Class for creating regular expressions for use in <see cref="DelimitedFileDataSet"/>
    /// </summary>
    /// <remarks>
    /// (c) 2017 Christiaan van Bergen. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2017-03-27.  </para><para>
    /// https://www.codeproject.com/Articles/22400/Converting-text-files-CSV-to-datasets.  </para><para>
    /// https://www.codeproject.com/script/Membership/View.aspx?mid=325286 </para>
    /// </remarks>
    public class RegexColumnBuilder
    {

        /// <summary>
        /// Adds a column to the collection of columns from which the regular expression will be created.
        /// </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <param name="columnName"> name of the column. </param>
        /// <param name="delimiter">  column delimiter. </param>
        public void AddColumn(string columnName, char delimiter)
        {
            this.AddColumn(columnName, string.Format("[^{0}]+", delimiter), string.Empty);
            this.Delimiter = delimiter.ToString();
        }

        /// <summary>
        /// Adds a column to the collection of columns from which the regular expression will be created.
        /// </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <param name="columnName"> name of the column. </param>
        /// <param name="delimiter">  column delimiter. </param>
        /// <param name="columnType"> RegexColumnType of this column. </param>
        public void AddColumn(string columnName, char delimiter, RegexColumnType columnType)
        {
            this.AddColumn(columnName, string.Format("[^{0}]+", delimiter), string.Empty, columnType);
            this.Delimiter = delimiter.ToString();
        }

        /// <summary>
        /// Adds a column to the collection of columns from which the regular expression will be created.
        /// </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <param name="columnName"> name of the column. </param>
        /// <param name="length">     amount of characters this column has. </param>
        public void AddColumn(string columnName, int length)
        {
            this.AddColumn(columnName, ".{" + length + "}", string.Empty);
        }

        /// <summary>
        /// Adds a column to the collection of columns from which the regular expression will be created.
        /// </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <param name="columnName"> name of the column. </param>
        /// <param name="length">     amount of characters this column has. </param>
        /// <param name="columnType"> RegexColumnType of this column. </param>
        public void AddColumn(string columnName, int length, RegexColumnType columnType)
        {
            this.AddColumn(columnName, ".{" + length + "}", string.Empty, columnType);
        }

        /// <summary>
        /// Adds a column to the collection of columns from which the regular expression will be created.
        /// </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <param name="columnName">  name of the column. </param>
        /// <param name="columnRegex"> regular expression for capturing the value of this column. </param>
        public void AddColumn(string columnName, string columnRegex)
        {
            this.AddColumn(columnName, columnRegex, string.Empty);
        }

        /// <summary>
        /// Adds a column to the collection of columns from which the regular expression will be created.
        /// </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <param name="columnName">  name of the column. </param>
        /// <param name="columnRegex"> regular expression for capturing the value of this column. </param>
        /// <param name="columnType">  RegexColumnType of this column. </param>
        public void AddColumn(string columnName, string columnRegex, RegexColumnType columnType)
        {
            this.AddColumn(columnName, columnRegex, string.Empty, columnType);
        }

        /// <summary>
        /// Adds a column to the collection of columns from which the regular expression will be created.
        /// </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <param name="columnName">    name of the column. </param>
        /// <param name="columnRegex">   regular expression for capturing the value of this column. </param>
        /// <param name="trailingRegex"> regular expression for any data not to be captured for this
        /// column. </param>
        public void AddColumn(string columnName, string columnRegex, string trailingRegex)
        {
            this._Columns.Add(new RegexColumn(columnName, columnRegex, trailingRegex));
        }

        /// <summary>
        /// Adds a column to the collection of columns from which the regular expression will be created.
        /// </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <param name="columnName">    name of the column. </param>
        /// <param name="columnRegex">   regular expression for capturing the value of this column. </param>
        /// <param name="trailingRegex"> regular expression for any data not to be captured for this
        /// column. </param>
        /// <param name="columnType">    RegexColumnType of this column. </param>
        public void AddColumn(string columnName, string columnRegex, string trailingRegex, RegexColumnType columnType)
        {
            this._Columns.Add(new RegexColumn(columnName, columnRegex, trailingRegex, columnType));
        }

        /// <summary> The columns. </summary>
        private readonly List<RegexColumn> _Columns = new List<RegexColumn>();

        /// <summary> Get the collection of created RegexColumns. </summary>
        /// <value> The columns. </value>
        public IEnumerable<RegexColumn> Columns => this._Columns;

        /// <summary>
        /// A delimiter string that will be appended after each column expression (including any trailing
        /// regular expression)
        /// </summary>
        /// <value> The delimiter. </value>
        public string Delimiter { get; set; } = string.Empty;

        /// <summary>
        /// Indicates whether the regular expression will start searching at each beginning of a
        /// line/string default is set to true.
        /// </summary>
        /// <value> The start at begin of string. </value>
        public bool StartAtBeginOfString { get; set; } = true;

        /// <summary>
        /// Indicates whether the regular expression will end searching at each end of a line/string
        /// default is set to true.
        /// </summary>
        /// <value> The end at end of string. </value>
        public bool EndAtEndOfString { get; set; } = true;

        /// <summary>
        /// creates a regular expression string based on the added columns and optional delimiter.
        /// </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <returns> regular expression for use in TextFileDataSet. </returns>
        public string CreateRegularExpressionString()
        {
            var builder = new StringBuilder();
            if ( this.StartAtBeginOfString )
            {
                _ = builder.Append( "^" );
            }

            for (int i = 0, loopTo = this.Columns.Count() - 1; i <= loopTo; i++)
            {
                _ = builder.Append( "(?<" );
                _ = builder.Append( this.Columns.ElementAtOrDefault( i ).ColumnName );
                _ = builder.Append( ">" );
                _ = builder.Append( this.Columns.ElementAtOrDefault( i ).Regex );
                _ = builder.Append( ")" );
                _ = builder.Append( this.Columns.ElementAtOrDefault( i ).TrailingRegex );
                if (i < this.Columns.Count() - 1)
                {
                    _ = builder.Append( this.Delimiter );
                }
            }

            if ( this.EndAtEndOfString )
            {
                _ = builder.Append( "$" );
            }

            return builder.ToString();
        }

        /// <summary>
        /// creates a regular expression based on the added columns and optional delimiter.
        /// </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <returns> The new regular expression. </returns>
        public Regex CreateRegularExpression()
        {
            return new Regex( this.CreateRegularExpressionString());
        }

        /// <summary>
        /// creates a regular expression based on the added columns and optional delimiter.
        /// </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <param name="options"> . </param>
        /// <returns> The new regular expression. </returns>
        public Regex CreateRegularExpression(RegexOptions options)
        {
            return new Regex( this.CreateRegularExpressionString(), options);
        }
    }

    /// <summary> Class for defining a regular expression column. </summary>
    /// <remarks>
    /// (c) 2017 Christiaan van Bergen. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2017-03-27. </para><para>
    /// https://www.codeproject.com/Articles/22400/Converting-text-files-CSV-to-datasets. </para><para>
    /// https://www.codeproject.com/script/Membership/View.aspx?mid=325286 </para>
    /// </remarks>
    public class RegexColumn
    {

        /// <summary> Constructor. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <param name="columnName">    name of the column. </param>
        /// <param name="regex">         regular expression for capturing the value of this column. </param>
        /// <param name="trailingRegex"> regular expression for any data not to be captured for this
        /// column. </param>
        public RegexColumn(string columnName, string regex, string trailingRegex) : base()
        {
            this.Init(columnName, regex, trailingRegex, RegexColumnType.String);
        }

        /// <summary> Constructor. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <param name="columnName">    name of the column. </param>
        /// <param name="regex">         regular expression for capturing the value of this column. </param>
        /// <param name="trailingRegex"> regular expression for any data not to be captured for this
        /// column. </param>
        /// <param name="columnType">    Type of this column. </param>
        public RegexColumn(string columnName, string regex, string trailingRegex, RegexColumnType columnType) : base()
        {
            this.Init(columnName, regex, trailingRegex, columnType);
        }

        /// <summary> Initializes this object. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <param name="columnName">    name of the column. </param>
        /// <param name="regEx">         The RegEx. </param>
        /// <param name="trailingRegex"> regular expression for any data not to be captured for this
        /// column. </param>
        /// <param name="columnType">    Type of this column. </param>
        private void Init(string columnName, string regEx, string trailingRegex, RegexColumnType columnType)
        {
            this.ColumnName = columnName;
            this.Regex = regEx;
            this.TrailingRegex = trailingRegex;
            this.ColumnType = columnType;
        }

        /// <summary> Get or set the name of the column. </summary>
        /// <value> The name of the column. </value>
        public string ColumnName { get; set; }

        /// <summary> Get or set the regular expression for capturing the value of this column. </summary>
        /// <value> The RegEx. </value>
        public string Regex { get; set; }

        /// <summary>
        /// Get or set the regular expression for any data not to be captured for this column.
        /// </summary>
        /// <value> The trailing RegEx. </value>
        public string TrailingRegex { get; set; }

        /// <summary> Get or set the Type of this column. </summary>
        /// <value> The type of the column. </value>
        public RegexColumnType ColumnType { get; set; }

        /// <summary> Get the System.Type of this RegexColumn. </summary>
        /// <value> The type of the column type as. </value>
        public Type ColumnTypeAsType => this.ColumnType == RegexColumnType.Integer ? typeof( int ) : this.ColumnType == RegexColumnType.Double ? typeof( double ) : this.ColumnType == RegexColumnType.Date ? typeof( DateTime ) : typeof( string );
    }

        /// <summary> Enumeration for certain types used in TextFileDataSet. </summary>
    /// <remarks> David, 2020-10-08. </remarks>
    public enum RegexColumnType
    {
        /// <summary>
        /// Integer
        /// </summary>
        Integer,
        /// <summary>
        /// Double
        /// </summary>
        Double,
        /// <summary>
        /// String
        /// </summary>
        String,
        /// <summary>
        /// DateTime
        /// </summary>
        Date
    }
}
