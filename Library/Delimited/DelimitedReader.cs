using System;
using System.Collections.Generic;
using Microsoft.VisualBasic.CompilerServices;

namespace isr.IO
{

    /// <summary> This is the reader class of the delimited file class library. </summary>
    /// <remarks>
    /// (c) 2004 Integrated Scientific Resources, Inc. All rights reserved. <para>
    /// Licensed under The MIT License. </para><para>
    /// David, 2004-03-04, 1.0.1524.x. </para>
    /// </remarks>
    public class DelimitedReader : DelimitedFileBase
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Constructs this class. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        public DelimitedReader() : base()
        {
            this.Delimiter = DefaultDelimiter;
        }

        /// <summary>   Constructs this class. </summary>
        /// <remarks>   David, 2021-04-22. </remarks>
        /// <param name="fileName"> Filename of the file. </param>
        public DelimitedReader( string fileName ) : base( fileName )
        {
            this.Delimiter = DefaultDelimiter;
        }

        #endregion

        #region " FILE DIALOG "

        /// <summary> Opens the File Open dialog box and gets a file name. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <returns> A Windows.Forms.DialogResult. </returns>
        public override bool OpenFileDialog()
        {
            bool result;
            var Ofd = new Core.OpenFileDialog()
            {
                FileDialogTitle = FileDialogTitle,
                FilePathName = FilePathName,
                FileDialogFilter = FileDialogFilter,
                FileExtension = FileExtension
            };
            result = Ofd.TrySelectFile();
            if (result)
                this.FilePathName = Ofd.FilePathName;
            return result;
        }

        /// <summary> Queries if a given file exists. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        public bool FileExists()
        {
            var fi = new System.IO.FileInfo( this.FilePathName );
            return fi.Exists;
        }

#endregion

#region " READ VALUE "

        /// <summary> The last value. </summary>
        private object _LastValue = string.Empty;

        /// <summary> Returns the last value read. </summary>
        /// <value> The last value. </value>
        public object LastValue => this._LastValue;

        /// <summary> Reads a value from the delimited file. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <exception cref="System.IO.EndOfStreamException"> Thrown when the end of the stream was
        /// unexpectedly reached. </exception>
        /// <returns> An Object. </returns>
        [System.Security.Permissions.FileIOPermission(System.Security.Permissions.SecurityAction.Demand, Unrestricted = true)]
        public object ReadObject()
        {
            if ( this.IsEndOfFile())
            {
                // if end of file, raise an exception
                throw new System.IO.EndOfStreamException();
            }
            else
            {
                Microsoft.VisualBasic.FileSystem.Input( this.FileNumber, ref this._LastValue );
                return this._LastValue;
            }
        }

#endregion

#region " DELIMITED "

        /// <summary> Gets or sets the tab delimited. </summary>
        /// <value> The tab delimited. </value>
        public bool TabDelimited
        {
            get => string.Equals( Conversions.ToString( this.Delimiter ), Microsoft.VisualBasic.Constants.vbTab );

            set => this.Delimiter = value ? Convert.ToChar( Microsoft.VisualBasic.Constants.vbTab ) : DefaultDelimiter;
        }

        /// <summary> The default delimiter. </summary>
        public const char DefaultDelimiter = ',';

        /// <summary> Gets or sets the delimiter. </summary>
        /// <value> The delimiter. </value>
        public char Delimiter { get; set; }

#endregion

#region " READ ROWS "

        /// <summary> Skips the given line count. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <param name="lineCount"> Number of lines. </param>
        public void Skip(int lineCount)
        {
            for (int i = 1, loopTo = lineCount; i <= loopTo; i++)
                _ = Microsoft.VisualBasic.FileSystem.LineInput( this.FileNumber );
        }

        /// <summary> Reads the line. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <returns> The line. </returns>
        public string ReadLine()
        {
            return Microsoft.VisualBasic.FileSystem.LineInput( this.FileNumber );
        }

        /// <summary> Returns an array of row data. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <exception cref="ArgumentOutOfRangeException" guarantee="strong"> element count is negative. </exception>
        /// <param name="elementCount"> Specifies the number of elements to tread. </param>
        /// <returns> An array of <see cref="System.Double">Double values</see>. </returns>
        [System.Security.Permissions.FileIOPermission(System.Security.Permissions.SecurityAction.Demand, Unrestricted = true)]
        public double[] ReadRowDouble(int elementCount)
        {
            if (elementCount < 0)
            {
                string message = "Array size specified as {0} must be non-negative.";
                message = string.Format(System.Globalization.CultureInfo.CurrentCulture, message, elementCount);
                throw new ArgumentOutOfRangeException(nameof(elementCount), elementCount, message);
            }

            if (elementCount == 0)
            {

                // return the empty array 
                var data = Array.Empty<double>();
                return data;
            }
            else
            {
                string record = Microsoft.VisualBasic.FileSystem.LineInput( this.FileNumber );
                if (record.Length > 0)
                {

                    // if we have data, get an array
                    var stringValues = record.Split( this.Delimiter );

                    // allow as many elements as requested or present.
                    elementCount = Math.Min(elementCount, stringValues.Length);

                    // allocate data array
                    var data = new double[elementCount];
                    for (int i = 0, loopTo = elementCount - 1; i <= loopTo; i++)
                        data[i] = Convert.ToDouble(stringValues[i], System.Globalization.CultureInfo.CurrentCulture);
                    return data;
                }
                else
                {

                    // return the empty array 
                    var data = Array.Empty<double>();
                    return data;
                }
            }
        }

        /// <summary> Returns an array of row data. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <returns> The row double. </returns>
        [System.Security.Permissions.FileIOPermission(System.Security.Permissions.SecurityAction.Demand, Unrestricted = true)]
        public double[] ReadRowDouble()
        {
            string record = Microsoft.VisualBasic.FileSystem.LineInput( this.FileNumber );
            if (record.Length > 0)
            {

                // if we have data, get an array
                var stringValues = record.Split( this.Delimiter );

                // allocate data array
                var data = new double[stringValues.Length];
                for (int i = 0, loopTo = stringValues.Length - 1; i <= loopTo; i++)
                    data[i] = Convert.ToDouble(stringValues[i], System.Globalization.CultureInfo.CurrentCulture);
                return data;
            }
            else
            {

                // return the empty array 
                var data = Array.Empty<double>();
                return data;
            }
        }

        /// <summary> Returns file rows as a two-dimensional array. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <returns> The rows double array. </returns>
        [System.Security.Permissions.FileIOPermission(System.Security.Permissions.SecurityAction.Demand, Unrestricted = true)]
        public double[,] ReadRowsDoubleArray()
        {
            double[,] data = new double[,] { };
            var columnIndex = default(int);
            while (!this.IsEndOfFile())
            {
                var oneDimensionValues = this.ReadRowDouble();
                if (oneDimensionValues is object && oneDimensionValues.Length > 0)
                {
                    if (data.Length == 0)
                    {
                        data = new double[oneDimensionValues.Length, columnIndex + 1];
                    }
                    else if (data.GetUpperBound(1) < columnIndex)
                    {
                        var oldData = data;
                        data = new double[data.GetUpperBound(0) + 1, columnIndex + 1];
                        if (oldData is object)
                            for (var i = 0; i <= oldData.Length / oldData.GetLength(1) - 1; ++i)
                                Array.Copy(oldData, i * oldData.GetLength(1), data, i * data.GetLength(1), Math.Min(oldData.GetLength(1), data.GetLength(1)));
                    }

                    if (data is object)
                    {
                        for (int i = 0, loopTo = Math.Min(data.GetUpperBound(0), oneDimensionValues.Length - 1); i <= loopTo; i++)
                            data[i, columnIndex] = Convert.ToDouble(oneDimensionValues[i]);
                    }
                }

                columnIndex += 1;
            }

            return data;
        }

        /// <summary>
        /// Return file rows as a jagged two-dimensional array allowing each row to be of different
        /// length.
        /// </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <returns> The rows double. </returns>
        [System.Security.Permissions.FileIOPermission(System.Security.Permissions.SecurityAction.Demand, Unrestricted = true)]
        public double[][] ReadRowsDouble()
        {
            var data = Array.Empty<double[]>();
            var rowIndex = default(int);
            while (!this.IsEndOfFile())
            {
                var oneDimensionValues = this.ReadRowDouble();
                if (oneDimensionValues is object && oneDimensionValues.Length > 0)
                {
                    data[rowIndex] = new double[oneDimensionValues.Length];
                    for (int i = 0, loopTo = Math.Min(data.GetUpperBound(0), oneDimensionValues.Length - 1); i <= loopTo; i++)
                        data[rowIndex][i] = oneDimensionValues[i];
                }

                rowIndex += 1;
            }

            return data;
        }

        /// <summary>
        /// Return file rows as a jagged array allowing each row to be of different length.
        /// </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <returns> all rows. </returns>
        public IList<string[]> ReadAllRows()
        {
            return this.ReadAllRows(new string[] { this.Delimiter.ToString() });
        }

        /// <summary>
        /// Return file rows as a jagged array allowing each row to be of different length.
        /// </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <param name="skipCount"> Number of skips. </param>
        /// <returns> all rows. </returns>
        public IList<string[]> ReadAllRows(int skipCount)
        {
            return this.ReadAllRows(skipCount, new string[] { this.Delimiter.ToString() } );
        }

        /// <summary>
        /// Return file rows as a jagged array allowing each row to be of different length.
        /// </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <param name="delimiters"> The delimiters. </param>
        /// <returns> all rows. </returns>
        public IList<string[]> ReadAllRows(string[] delimiters)
        {
            if (delimiters is null)
                delimiters = new string[] { this.Delimiter.ToString() };
            using var MyReader = new Microsoft.VisualBasic.FileIO.TextFieldParser( this.FilePathName ) {
                TextFieldType = Microsoft.VisualBasic.FileIO.FieldType.Delimited,
                Delimiters = delimiters
            };
            return ReadAllRows( MyReader );
        }

        /// <summary>
        /// Return file rows as a jagged array allowing each row to be of different length.
        /// </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <param name="skipCount">  Number of skips. </param>
        /// <param name="delimiters"> The delimiters. </param>
        /// <returns> all rows. </returns>
        public IList<string[]> ReadAllRows(int skipCount, string[] delimiters)
        {
            if (delimiters is null)
                delimiters = new string[] { this.Delimiter.ToString() };
            using var MyReader = new Microsoft.VisualBasic.FileIO.TextFieldParser( this.FilePathName ) {
                TextFieldType = Microsoft.VisualBasic.FileIO.FieldType.Delimited,
                Delimiters = delimiters
            };
            while ( skipCount > 0 )
            {
                skipCount -= 1;
                _ = MyReader.ReadLine();
            }

            return ReadAllRows( MyReader );
        }

        /// <summary>
        /// Return file rows as a jagged array allowing each row to be of different length.
        /// </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <param name="skipCount"> Number of skips. </param>
        /// <param name="delimiter"> The delimiter. </param>
        /// <returns> all rows. </returns>
        public IList<string[]> ReadAllRows(int skipCount, string delimiter)
        {
            if (string.IsNullOrEmpty(delimiter))
                delimiter = Conversions.ToString(this.Delimiter);
            var delimiters = new string[] { delimiter };
            using var MyReader = new Microsoft.VisualBasic.FileIO.TextFieldParser( this.FilePathName ) {
                TextFieldType = Microsoft.VisualBasic.FileIO.FieldType.Delimited,
                Delimiters = delimiters
            };
            while ( skipCount > 0 )
            {
                skipCount -= 1;
                _ = MyReader.ReadLine();
            }

            return ReadAllRows( MyReader );
        }

        /// <summary>
        /// Return file rows as a jagged array allowing each row to be of different length.
        /// </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="reader"> The reader. </param>
        /// <returns> all rows. </returns>
        public static IList<string[]> ReadAllRows(Microsoft.VisualBasic.FileIO.TextFieldParser reader)
        {
            if (reader is null)
                throw new ArgumentNullException(nameof(reader));
            var l = new List<string[]>();
            string[] currentRow;
            // Loop through all of the fields in the file. 
            // If any lines are corrupt, report an error and continue parsing. 
            while (!reader.EndOfData)
            {
                currentRow = reader.ReadFields();
                if (currentRow is object && currentRow.Length > 0)
                {
                    l.Add(currentRow);
                }
            }

            return l.ToArray();
        }

#endregion

    }
}
