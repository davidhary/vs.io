﻿using System;

namespace isr.IO.My
{

    /// <summary> Provides assembly information for the class library. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
    public sealed partial class MyLibrary
    {

        /// <summary>
        /// Constructor that prevents a default instance of this class from being created.
        /// </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        private MyLibrary() : base()
        {
        }

        /// <summary> Returns True if this is 64 bit process. </summary>
        /// <value> The 64 bit process sentinel. </value>
        public static bool Is64BitProcess => IntPtr.Size == 8;

        /// <summary> Gets the identifier of the trace source. </summary>
        public const int TraceEventId = (int)ProjectTraceEventId.CoreLibrary;

        /// <summary> The assembly title. </summary>
        public const string AssemblyTitle = "IO Core Library";

        /// <summary> Information describing the assembly. </summary>
        public const string AssemblyDescription = "IO Core Library";

        /// <summary> The assembly product. </summary>
        public const string AssemblyProduct = "IO.Core";
    }

        /// <summary> Values that represent project trace event identifiers. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
    public enum ProjectTraceEventId
    {

        /// <summary> An enum constant representing the none option. </summary>
        [System.ComponentModel.Description("Not specified")]
        None,

        /// <summary> . </summary>
        [System.ComponentModel.Description("IO Library")]
        CoreLibrary = Core.ProjectTraceEventId.IO,

        /// <summary> . </summary>
        [System.ComponentModel.Description("IO Tester")]
        IOTester = CoreLibrary + 0xA,

        /// <summary> . </summary>
        [System.ComponentModel.Description("File Watcher Tester")]
        FileWatcherTester = CoreLibrary + 0xB,

        /// <summary> . </summary>
        [System.ComponentModel.Description("IO Units")]
        CoreUnitTest = CoreLibrary + 0xC
    }
}