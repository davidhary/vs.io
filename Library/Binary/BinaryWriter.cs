using System;

namespace isr.IO
{

    /// <summary>
    /// Extends the <see cref="System.IO.BinaryWriter">system binary reader</see> class.
    /// </summary>
    /// <remarks>
    /// (c) 2010 Integrated Scientific Resources, Inc. All rights reserved. <para>
    /// Licensed under The MIT License. </para><para>
    /// David, 2006-03-07, 1.0.2257.x. </para>
    /// </remarks>
    public class BinaryWriter : System.IO.BinaryWriter
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Constructs this class. </summary>
        /// <remarks> Use this constructor to open a binary reader for the specified file. </remarks>
        /// <param name="filePathName"> Specifies the file name. </param>
        /// <param name="fileMode">     Specifies the <see cref="System.IO.FileMode">file mode</see>.
        /// This would <see cref="System.IO.FileMode.CreateNew">Create
        /// New</see> to create a new file,
        /// <see cref="System.IO.FileMode.OpenOrCreate">create or open an
        /// existing</see>
        /// file or <see cref="System.IO.FileMode.Append">append</see>. to
        /// open and position for appending to the file. </param>

        // instantiate the base class
        public BinaryWriter(string filePathName, System.IO.FileMode fileMode) : base(OpenStream(filePathName, fileMode))
        {
            this.FilePathName = filePathName;
        }

        /// <summary> Opens a stream. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <param name="filePathName"> Specifies the name of the binary file which to read. </param>
        /// <param name="fileMode">     Specifies the <see cref="System.IO.FileMode">file mode</see>.
        /// This would <see cref="System.IO.FileMode.CreateNew">Create
        /// New</see> to create a new file,
        /// <see cref="System.IO.FileMode.OpenOrCreate">create or open an
        /// existing</see>
        /// file or <see cref="System.IO.FileMode.Append">append</see>. to
        /// open and position for appending to the file. </param>
        /// <returns> A System.IO.FileStream. </returns>
        private static System.IO.FileStream OpenStream(string filePathName, System.IO.FileMode fileMode)
        {
            System.IO.FileStream fileStream = null;
            System.IO.FileStream tempFileStream = null;
            if (!string.IsNullOrWhiteSpace(filePathName))
            {
                try
                {
                    tempFileStream = new System.IO.FileStream(filePathName, fileMode, System.IO.FileAccess.Write);
                    fileStream = tempFileStream;
                }
                catch
                {
                    if (tempFileStream is object)
                        tempFileStream.Dispose();
                    throw;
                }
            }

            return fileStream;
        }

        #endregion

        #region " METHODS  AND  PROPERTIES "

        /// <summary> Closes the binary writer. </summary>
        /// <remarks>
        /// Use this method to close the instance.  The method Returns <c>True</c> if success or false if
        /// it failed closing the instance.
        /// </remarks>
        public override void Close()
        {

            // Close the file.
            System.IO.FileStream fs = (System.IO.FileStream)base.BaseStream;
            base.Close();
            if (fs is object)
                fs.Close();
        }

        /// <summary> Gets or sets the file name. </summary>
        /// <remarks> Use this property to get or set the file name. </remarks>
        /// <value> <c>FilePathName</c> is a String property. </value>
        public string FilePathName { get; private set; } = string.Empty;

        /// <summary>
        /// Writes a single-dimension <see cref="Double">double-precision</see>
        /// array to the data file.
        /// </summary>
        /// <remarks> Returns values as an array. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="values"> Holds the values to write. </param>
        public void Write(double[] values)
        {
            if (values is null)
                throw new ArgumentNullException(nameof(values));

            // write the data size for verifying size and, indirectly also location, upon read
            base.Write(Convert.ToInt32(values.Length));

            // write values to the file
            for (int i = values.GetLowerBound(0), loopTo = values.GetUpperBound(0); i <= loopTo; i++)
                base.Write(values[i]);
        }

        /// <summary>
        /// Writes a single-dimension <see cref="Int32">Integer</see>
        /// array to the data file.
        /// </summary>
        /// <remarks> Returns values as an array. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="values"> Holds the values to write. </param>
        public void Write(int[] values)
        {
            if (values is null)
                throw new ArgumentNullException(nameof(values));

            // write the data size for verifying size and, indirectly also location, upon read
            base.Write(Convert.ToInt32(values.Length));

            // write values to the file
            for (int i = values.GetLowerBound(0), loopTo = values.GetUpperBound(0); i <= loopTo; i++)
                base.Write(values[i]);
        }

        /// <summary>
        /// Writes a single-dimension <see cref="Int64">Long Integer</see>
        /// array to the data file.
        /// </summary>
        /// <remarks> Returns values as an array. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="values"> Holds the values to write. </param>
        public void Write(long[] values)
        {
            if (values is null)
                throw new ArgumentNullException(nameof(values));

            // write the data size for verifying size and, indirectly also location, upon read
            base.Write(Convert.ToInt32(values.Length));

            // write values to the file
            for (int i = 0, loopTo = values.GetUpperBound(0); i <= loopTo; i++)
                base.Write(values[i]);
        }

        /// <summary>
        /// Writes a single-dimension <see cref="Single">single-precision</see>
        /// array to the data file.
        /// </summary>
        /// <remarks> Returns values as an array. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="values"> Holds the values to write. </param>
        public void Write(float[] values)
        {
            if (values is null)
                throw new ArgumentNullException(nameof(values));

            // write the data size for verifying size and, indirectly also location, upon read
            base.Write(Convert.ToInt32(values.Length));

            // write values to the file
            for (int i = 0, loopTo = values.GetUpperBound(0); i <= loopTo; i++)
                base.Write(values[i]);
        }

        /// <summary>
        /// Writes a string to the binary file padding it with spaces as necessary to fill the length.
        /// </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="value">  Holds the value to write. </param>
        /// <param name="length"> . </param>
        public void Write(string value, int length)
        {
            if (value is null)
                throw new ArgumentNullException(nameof(value));

            // pad but make sure not to exceed length.
            base.Write(value.PadRight(length).Substring(0, length));
        }

        /// <summary> Writes a double-precision value to the data file. </summary>
        /// <remarks> Returns values as an array. </remarks>
        /// <param name="value">    Specifies the value to write. </param>
        /// <param name="location"> Specifies the file location. </param>
        public void Write(double value, long location)
        {
            _ = base.BaseStream.Seek( location, System.IO.SeekOrigin.Begin );
            base.Write(value);
        }

        /// <summary> Writes an Int32 value to the data file. </summary>
        /// <remarks> Returns values as an array. </remarks>
        /// <param name="value">    Specifies the value to write. </param>
        /// <param name="location"> Specifies the file location. </param>
        public void Write(int value, long location)
        {
            _ = base.BaseStream.Seek( location, System.IO.SeekOrigin.Begin );
            base.Write(value);
        }

        /// <summary> Writes a Int64 value to the data file. </summary>
        /// <remarks> Returns values as an array. </remarks>
        /// <param name="value">    Specifies the value to write. </param>
        /// <param name="location"> Specifies the file location. </param>
        public void Write(long value, long location)
        {
            _ = base.BaseStream.Seek( location, System.IO.SeekOrigin.Begin );
            base.Write(value);
        }

        /// <summary> Writes a single-precision value to the data file. </summary>
        /// <remarks> Returns values as an array. </remarks>
        /// <param name="value">    Specifies the value to write. </param>
        /// <param name="location"> Specifies the file location. </param>
        public void Write(float value, long location)
        {
            _ = base.BaseStream.Seek( location, System.IO.SeekOrigin.Begin );
            base.Write(value);
        }

        /// <summary> Writes a string value to the data file. </summary>
        /// <remarks> Returns values as an array. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="value">    Specifies the value to write. </param>
        /// <param name="location"> Specifies the file location. </param>
        public void Write(string value, long location)
        {
            if (value is null)
                throw new ArgumentNullException(nameof(value));
            _ = base.BaseStream.Seek( location, System.IO.SeekOrigin.Begin );
            base.Write(value);
        }

        #endregion

    }
}
