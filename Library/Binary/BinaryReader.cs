using System;

namespace isr.IO
{

    /// <summary>
    /// Extends the <see cref="System.IO.BinaryReader">system binary reader</see> class.
    /// </summary>
    /// <remarks>
    /// (c) 2006 Integrated Scientific Resources, Inc. All rights reserved. <para>
    /// Licensed under The MIT License. </para><para>
    /// David, 2006-03-07, 1.0.2257.x. </para>
    /// </remarks>
    public class BinaryReader : System.IO.BinaryReader
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Constructs this class. </summary>
        /// <remarks> Use this constructor to open a binary reader for the specified file. </remarks>
        /// <param name="filePathName"> Specifies the name of the binary file which to read. </param>

        // instantiate the base class
        public BinaryReader(string filePathName) : base(OpenStream(filePathName))
        {
            this.FilePathName = filePathName;
        }

        /// <summary> Opens a stream. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <param name="filePathName"> Specifies the name of the binary file which to read. </param>
        /// <returns> A System.IO.FileStream. </returns>
        private static System.IO.FileStream OpenStream(string filePathName)
        {
            System.IO.FileStream fileStream = null;
            System.IO.FileStream tempFileStream = null;
            if (!string.IsNullOrWhiteSpace(filePathName))
            {
                try
                {
                    tempFileStream = new System.IO.FileStream(filePathName, System.IO.FileMode.Open, System.IO.FileAccess.Read);
                    fileStream = tempFileStream;
                }
                catch
                {
                    if (tempFileStream is object)
                        tempFileStream.Dispose();
                    throw;
                }
            }

            return fileStream;
        }

        #endregion

        #region " METHODS  AND  PROPERTIES "

        /// <summary> Closes the binary reader and base stream. </summary>
        /// <remarks> Use this method to close the instance. </remarks>
        public override void Close()
        {
            // Close the file.
            System.IO.FileStream fs = (System.IO.FileStream)base.BaseStream;
            base.Close();
            fs?.Close();
        }

        /// <summary> Gets or sets the file name. </summary>
        /// <remarks> Use this property to get or set the file name. </remarks>
        /// <value> <c>FilePathName</c> is a String property. </value>
        public string FilePathName { get; private set; } = string.Empty;

        /// <summary>
        /// Opens a binary file for reading and returns a reference to the reader. The file is
        /// <see cref="System.IO.FileMode.Open">opened</see> in
        /// <see cref="System.IO.FileAccess.Read">read access</see>.
        /// </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <exception cref="System.IO.FileNotFoundException"> Thrown when the requested file is not present. </exception>
        /// <param name="filePathName"> Specifies the file name. </param>
        /// <returns> A reference to an open <see cref="IO.BinaryReader">binary reader</see>. </returns>
        public static BinaryReader OpenBinaryReader(string filePathName)
        {
            if (string.IsNullOrWhiteSpace(filePathName))
                throw new ArgumentNullException(nameof(filePathName));
            if (!System.IO.File.Exists(filePathName))
            {
                string message = "Failed opening a binary reader -- file not found.";
                throw new System.IO.FileNotFoundException(message, filePathName);
            }

            return new BinaryReader(filePathName);
        }

        /// <summary>
        /// Reads a single-dimension <see cref="Double">double-precision</see>
        /// array from the data file.
        /// </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <exception cref="System.IO.IOException"> Thrown when an IO failure occurred. </exception>
        /// <returns> A <see cref="System.Double">double-precision</see> array. </returns>
        public double[] ReadDoubleArray()
        {
            long startingPosition = base.BaseStream.Position;

            // read the stored length
            int storedLength = base.ReadInt32();
            return storedLength < 0
                ? throw new System.IO.IOException(string.Format(System.Globalization.CultureInfo.CurrentCulture, "Program encountered a negative array length of {0}. Possibly the file is corrupt the reader position at {1} is incorrect.", storedLength, startingPosition))
                : this.ReadDoubleArray(storedLength);
        }

        /// <summary>
        /// Reads a single-dimension <see cref="Double">double-precision</see>
        /// array from the data file.
        /// </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
        /// the required range. </exception>
        /// <param name="elementCount"> Specifies the number of data points. </param>
        /// <returns> A <see cref="Double">double-precision</see> array. </returns>
        public double[] ReadDoubleArray(int elementCount)
        {
            if (elementCount < 0)
            {
                string message = "Array size specified as {0} must be non-negative.";
                message = string.Format(System.Globalization.CultureInfo.CurrentCulture, message, elementCount);
                throw new ArgumentOutOfRangeException(nameof(elementCount), elementCount, message);
            }

            if (elementCount == 0)
            {

                // return the empty array 
                var data = Array.Empty<double>();
                return data;
            }
            else
            {
                // allocate data array
                var data = new double[elementCount];

                // Read the voltage from the file
                for (int sampleNumber = 0, loopTo = elementCount - 1; sampleNumber <= loopTo; sampleNumber++)
                    data[sampleNumber] = base.ReadDouble();
                return data;
            }
        }

        /// <summary>
        /// Reads a single-dimension <see cref="Double">double-precision</see>
        /// array from the data file.
        /// </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
        /// the required range. </exception>
        /// <param name="elementCount"> Specifies the number of data points. </param>
        /// <param name="verifyLength"> If true, verifies the length against the given length. </param>
        /// <returns> A single-dimension <see cref="Double">double-precision</see> array. </returns>
        public double[] ReadDoubleArray(int elementCount, bool verifyLength)
        {
            if (verifyLength)
            {
                long startingPosition = base.BaseStream.Position;
                var data = this.ReadDoubleArray();
                if (data.Length != elementCount)
                {
                    string message = "Data length stored in file of {0} elements does not match the expected data length of {1} elements at {2}.";
                    message = string.Format(System.Globalization.CultureInfo.CurrentCulture, message, data.Length, elementCount, startingPosition);
                    throw new ArgumentOutOfRangeException(nameof(elementCount), elementCount, message);
                }
                else
                {
                    return data;
                }
            }
            else
            {
                return this.ReadDoubleArray();
            }
        }

        /// <summary>
        /// Reads a single-dimension <see cref="Double">double-precision</see>
        /// array from the data file.
        /// </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
        /// the required range. </exception>
        /// <param name="count">      Specifies the number of data points. </param>
        /// <param name="startIndex"> Specifies the index of the first data point. </param>
        /// <param name="stepSize">   Specifies the step size between adjacent data points. </param>
        /// <returns> A single-dimension <see cref="Double">double-precision</see> array. </returns>
        public double[] ReadDoubleArray(int count, int startIndex, int stepSize)
        {
            long startingPosition = base.BaseStream.Position;

            // read the stored length
            int storedLength = base.ReadInt32();
            if (storedLength != count)
            {
                string message = "Data length stored in file of {0} elements does not match the expected data length of {1} elements at {2}.";
                message = string.Format(System.Globalization.CultureInfo.CurrentCulture, message, storedLength, count, startingPosition);
                throw new ArgumentOutOfRangeException(nameof(count), count, message);
            }

            // allocate data array
            var data = new double[count];

            // skip samples to get to the first channel of this sample set.
            if (startIndex > 0)
            {
                for (int i = 1, loopTo = startIndex; i <= loopTo; i++)
                    _ = base.ReadDouble();
            }
            // Read the voltage from the file
            for (int sampleNumber = 0, loopTo1 = count - 1; sampleNumber <= loopTo1; sampleNumber++)
            {
                data[sampleNumber] = base.ReadDouble();
                if (stepSize > 1)
                {
                    for (int i = 2, loopTo2 = stepSize; i <= loopTo2; i++)
                        _ = base.ReadDouble();
                }
            }

            return data;
        }

        /// <summary> Reads a double precision value from the data file. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <param name="location"> Specifies the file location. </param>
        /// <returns> A <see cref="System.Double">value</see>. </returns>
        public double ReadDoubleValue(long location)
        {
            _ = base.BaseStream.Seek( location, System.IO.SeekOrigin.Begin );
            return base.ReadDouble();
        }

        /// <summary>
        /// Reads a single-dimension <see cref="Int32">Integer</see>
        /// array from the data file.
        /// </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <exception cref="System.IO.IOException"> Thrown when an IO failure occurred. </exception>
        /// <returns> An <see cref="System.Int32">integer</see> array. </returns>
        public int[] ReadInt32Array()
        {
            long startingPosition = base.BaseStream.Position;

            // read the stored length
            int storedLength = base.ReadInt32();
            return storedLength < 0
                ? throw new System.IO.IOException(string.Format(System.Globalization.CultureInfo.CurrentCulture, "Program encountered a negative array length of {0}. Possibly the file is corrupt the reader position at {1} is incorrect.", storedLength, startingPosition))
                : this.ReadInt32Array(storedLength);
        }

        /// <summary>
        /// Reads a single-dimension <see cref="Int32">Integer</see>
        /// array from the data file.
        /// </summary>
        /// <remarks> Returns values as an array. </remarks>
        /// <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
        /// the required range. </exception>
        /// <param name="elementCount"> Specifies the number of data points. </param>
        /// <returns> An <see cref="System.Int32">integer</see> array. </returns>
        public int[] ReadInt32Array(int elementCount)
        {
            if (elementCount < 0)
            {
                string message = "Array size must be non-negative {0}.";
                message = string.Format(System.Globalization.CultureInfo.CurrentCulture, message, elementCount);
                throw new ArgumentOutOfRangeException(nameof(elementCount), elementCount, message);
            }

            if (elementCount == 0)
            {

                // return the empty array 
                var data = Array.Empty<int>();
                return data;
            }
            else
            {
                // allocate data array
                var data = new int[elementCount];

                // Read the voltage from the file
                for (int sampleNumber = 0, loopTo = elementCount - 1; sampleNumber <= loopTo; sampleNumber++)
                    data[sampleNumber] = base.ReadInt32();
                return data;
            }
        }

        /// <summary>
        /// Reads a single-dimension <see cref="Int32">Integer</see>
        /// array from the data file.
        /// </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
        /// the required range. </exception>
        /// <param name="elementCount"> Specifies the number of data points. </param>
        /// <param name="verifyLength"> If true, verifies the length against the given length. </param>
        /// <returns> An <see cref="System.Int32">integer</see> array. </returns>
        public int[] ReadInt32Array(int elementCount, bool verifyLength)
        {
            if (verifyLength)
            {
                long startingPosition = base.BaseStream.Position;
                var data = this.ReadInt32Array();
                if (data.Length != elementCount)
                {
                    string message = "Data length stored in file of {0} elements does not match the expected data length of {1} elements at {2}.";
                    message = string.Format(System.Globalization.CultureInfo.CurrentCulture, message, data.Length, elementCount, startingPosition);
                    throw new ArgumentOutOfRangeException(nameof(elementCount), elementCount, message);
                }
                else
                {
                    return data;
                }
            }
            else
            {
                return this.ReadInt32Array();
            }
        }

        /// <summary>
        /// Reads a single-dimension <see cref="Int32">Integer</see>
        /// array from the data file.
        /// </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
        /// the required range. </exception>
        /// <param name="count">      Specifies the number of data points. </param>
        /// <param name="startIndex"> Specifies the index of the first data point. </param>
        /// <param name="stepSize">   Specifies the step size between adjacent data points. </param>
        /// <returns> An <see cref="System.Int32">integer</see> array. </returns>
        public int[] ReadInt32(int count, int startIndex, int stepSize)
        {
            long startingPosition = base.BaseStream.Position;

            // read the stored length
            int storedLength = base.ReadInt32();
            if (storedLength != count)
            {
                string message = "Data length stored in file of {0} elements does not match the expected data length of {1} elements at {2}.";
                message = string.Format(System.Globalization.CultureInfo.CurrentCulture, message, storedLength, count, startingPosition);
                throw new ArgumentOutOfRangeException(nameof(count), count, message);
            }

            // allocate data array
            var data = new int[count];

            // skip samples to get to the first channel of this sample set.
            if (startIndex > 0)
            {
                for (int i = 1, loopTo = startIndex; i <= loopTo; i++)
                    _ = base.ReadInt32();
            }
            // Read the voltage from the file
            for (int sampleNumber = 0, loopTo1 = count - 1; sampleNumber <= loopTo1; sampleNumber++)
            {
                data[sampleNumber] = base.ReadInt32();
                if (stepSize > 1)
                {
                    for (int i = 2, loopTo2 = stepSize; i <= loopTo2; i++)
                        _ = base.ReadInt32();
                }
            }

            return data;
        }

        /// <summary> Reads an Int32 value from the data file. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <param name="location"> Specifies the file location. </param>
        /// <returns> An <see cref="System.Int32">integer</see> value. </returns>
        public int ReadInt32(long location)
        {
            _ = base.BaseStream.Seek( location, System.IO.SeekOrigin.Begin );
            return base.ReadInt32();
        }

        /// <summary>
        /// Reads a <see cref="Int64">long integer</see>
        /// long-integer array from the data file.
        /// </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <exception cref="System.IO.IOException"> Thrown when an IO failure occurred. </exception>
        /// <returns> An <see cref="System.Int64">long</see> array. </returns>
        public long[] ReadInt64Array()
        {
            long startingPosition = base.BaseStream.Position;

            // read the stored length
            int storedLength = base.ReadInt32();
            return storedLength < 0
                ? throw new System.IO.IOException(string.Format(System.Globalization.CultureInfo.CurrentCulture, "Program encountered a negative array length of {0}. Possibly the file is corrupt the reader position at {1} is incorrect.", storedLength, startingPosition))
                : this.ReadInt64Array(storedLength);
        }

        /// <summary>
        /// Reads a <see cref="Int64">long integer</see>
        /// long integer array from the data file.
        /// </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
        /// the required range. </exception>
        /// <param name="elementCount"> Specifies the number of data points. </param>
        /// <returns> A <see cref="System.Int64">long</see> array. </returns>
        public long[] ReadInt64Array(int elementCount)
        {
            if (elementCount < 0)
            {
                string message = "Array size must be non-negative {0}.";
                message = string.Format(System.Globalization.CultureInfo.CurrentCulture, message, elementCount);
                throw new ArgumentOutOfRangeException(nameof(elementCount), elementCount, message);
            }

            if (elementCount == 0)
            {

                // return the empty array 
                var data = Array.Empty<long>();
                return data;
            }
            else
            {
                // allocate data array
                var data = new long[elementCount];

                // Read the voltage from the file
                for (int sampleNumber = 0, loopTo = elementCount - 1; sampleNumber <= loopTo; sampleNumber++)
                    data[sampleNumber] = base.ReadInt64();
                return data;
            }
        }

        /// <summary>
        /// Reads a <see cref="Int64">long integer</see>
        /// long integer array from the data file.
        /// </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
        /// the required range. </exception>
        /// <param name="elementCount"> Specifies the number of data points. </param>
        /// <param name="verifyLength"> If true, verifies the length against the given length. </param>
        /// <returns> A <see cref="System.Int64">Long</see> array. </returns>
        public long[] ReadInt64Array(int elementCount, bool verifyLength)
        {
            if (verifyLength)
            {
                long startingPosition = base.BaseStream.Position;
                var data = this.ReadInt64Array();
                if (data.Length != elementCount)
                {
                    string message = "Data length stored in file of {0} elements does not match the expected data length of {1} elements at {2}.";
                    message = string.Format(System.Globalization.CultureInfo.CurrentCulture, message, data.Length, elementCount, startingPosition);
                    throw new ArgumentOutOfRangeException(nameof(elementCount), elementCount, message);
                }
                else
                {
                    return data;
                }
            }
            else
            {
                return this.ReadInt64Array();
            }
        }

        /// <summary>
        /// Reads a single-dimension <see cref="Int64">Long Integer</see>
        /// array from the data file.
        /// </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
        /// the required range. </exception>
        /// <param name="count">      Specifies the number of data points. </param>
        /// <param name="startIndex"> Specifies the index of the first data point. </param>
        /// <param name="stepSize">   Specifies the step size between adjacent data points. </param>
        /// <returns> A <see cref="System.Int64">Long</see> array. </returns>
        public long[] ReadInt64(int count, int startIndex, int stepSize)
        {

            // read the stored length
            int storedLength = base.ReadInt32();
            if (storedLength != count)
            {
                long startingPosition = base.BaseStream.Position;
                string message = "Data length stored in file of {0} elements does not match the expected data length of {1} elements at {2}.";
                message = string.Format(System.Globalization.CultureInfo.CurrentCulture, message, storedLength, count, startingPosition);
                throw new ArgumentOutOfRangeException(nameof(count), count, message);
            }

            // allocate data array
            var data = new long[count];

            // skip samples to get to the first channel of this sample set.
            if (startIndex > 0)
            {
                for (int i = 1, loopTo = startIndex; i <= loopTo; i++)
                    _ = base.ReadInt64();
            }
            // Read the voltage from the file
            for (int sampleNumber = 0, loopTo1 = count - 1; sampleNumber <= loopTo1; sampleNumber++)
            {
                data[sampleNumber] = base.ReadInt64();
                if (stepSize > 1)
                {
                    for (int i = 2, loopTo2 = stepSize; i <= loopTo2; i++)
                        _ = base.ReadInt64();
                }
            }

            return data;
        }

        /// <summary> Reads a Int64 value from the data file. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <param name="location"> Specifies the file location. </param>
        /// <returns> A <see cref="System.Int64">Long</see> value. </returns>
        public long ReadInt64(long location)
        {
            _ = base.BaseStream.Seek( location, System.IO.SeekOrigin.Begin );
            return base.ReadInt64();
        }

        /// <summary>
        /// Reads a single-dimension <see cref="Single">single-precision</see>
        /// array from the data file.
        /// </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <exception cref="System.IO.IOException"> Thrown when an IO failure occurred. </exception>
        /// <returns> A <see cref="System.Single">single-dimension</see> array. </returns>
        public float[] ReadSingleArray()
        {
            long startingPosition = base.BaseStream.Position;

            // read the stored length
            int storedLength = base.ReadInt32();
            return storedLength < 0
                ? throw new System.IO.IOException(string.Format(System.Globalization.CultureInfo.CurrentCulture, "Program encountered a negative array length of {0}. Possibly the file is corrupt the reader position at {1} is incorrect.", storedLength, startingPosition))
                : this.ReadSingleArray(storedLength);
        }

        /// <summary>
        /// Reads a single-dimension <see cref="Single">single-precision</see>
        /// array from the data file.
        /// </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
        /// the required range. </exception>
        /// <param name="elementCount"> Specifies the number of data points. </param>
        /// <returns> A <see cref="System.Single">Single-Dimension</see> array. </returns>
        public float[] ReadSingleArray(int elementCount)
        {
            if (elementCount < 0)
            {
                string message = "Array size must be non-negative {0}.";
                message = string.Format(System.Globalization.CultureInfo.CurrentCulture, message, elementCount);
                throw new ArgumentOutOfRangeException(nameof(elementCount), elementCount, message);
            }

            if (elementCount == 0)
            {

                // return the empty array 
                var data = Array.Empty<float>();
                return data;
            }
            else
            {
                // allocate data array
                var data = new float[elementCount];

                // Read the voltage from the file
                for (int sampleNumber = 0, loopTo = elementCount - 1; sampleNumber <= loopTo; sampleNumber++)
                    data[sampleNumber] = base.ReadSingle();
                return data;
            }
        }

        /// <summary>
        /// Reads a single-dimension <see cref="Single">single-precision</see>
        /// array from the data file.
        /// </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
        /// the required range. </exception>
        /// <param name="elementCount"> Specifies the number of data points. </param>
        /// <param name="verifyLength"> If true, verifies the length against the given length. </param>
        /// <returns> A <see cref="System.Single">Single-Dimension</see> array. </returns>
        public float[] ReadSingleArray(int elementCount, bool verifyLength)
        {
            if (verifyLength)
            {
                var data = this.ReadSingleArray();
                if (data.Length != elementCount)
                {
                    string message = "Data length stored in file of {0} elements does not match the expected data length of {1} elements.";
                    message = string.Format(System.Globalization.CultureInfo.CurrentCulture, message, data.Length, elementCount);
                    throw new ArgumentOutOfRangeException(nameof(elementCount), elementCount, message);
                }
                else
                {
                    return data;
                }
            }
            else
            {
                return this.ReadSingleArray();
            }
        }

        /// <summary>
        /// Reads a single-dimension <see cref="Single">single-precision</see>
        /// array from the data file.
        /// </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
        /// the required range. </exception>
        /// <param name="count">      Specifies the number of data points. </param>
        /// <param name="startIndex"> Specifies the index of the first data point. </param>
        /// <param name="stepSize">   Specifies the step size between adjacent data points. </param>
        /// <returns> A <see cref="System.Single">Single-Dimension</see> array. </returns>
        public float[] ReadSingle(int count, int startIndex, int stepSize)
        {
            long startingPosition = base.BaseStream.Position;

            // read the stored length
            int storedLength = base.ReadInt32();
            if (storedLength != count)
            {
                string message = "Data length stored in file of {0} elements does not match the expected data length of {1} elements at {2}.";
                message = string.Format(System.Globalization.CultureInfo.CurrentCulture, message, storedLength, count, startingPosition);
                throw new ArgumentOutOfRangeException(nameof(count), count, message);
            }

            // allocate data array
            var data = new float[count];

            // skip samples to get to the first channel of this sample set.
            if (startIndex > 0)
            {
                for (int i = 1, loopTo = startIndex; i <= loopTo; i++)
                    _ = base.ReadSingle();
            }
            // Read the voltage from the file
            for (int sampleNumber = 0, loopTo1 = count - 1; sampleNumber <= loopTo1; sampleNumber++)
            {
                data[sampleNumber] = base.ReadSingle();
                if (stepSize > 1)
                {
                    for (int i = 2, loopTo2 = stepSize; i <= loopTo2; i++)
                        _ = base.ReadSingle();
                }
            }

            return data;
        }

        /// <summary> Reads a single precision value from the data file. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <param name="location"> Specifies the file location. </param>
        /// <returns> A <see cref="System.Single">Single-Dimension</see> value. </returns>
        public float ReadSingle(long location)
        {
            _ = base.BaseStream.Seek( location, System.IO.SeekOrigin.Begin );
            return base.ReadSingle();
        }

        /// <summary> Reads a string value from the data file. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <param name="location"> Specifies the file location. </param>
        /// <returns> A <see cref="System.String">String</see> value. </returns>
        public string ReadString(long location)
        {
            _ = base.BaseStream.Seek( location, System.IO.SeekOrigin.Begin );
            return base.ReadString();
        }

        #endregion

    }
}
