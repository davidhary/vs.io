Using isr.IO.Zip
Namespace ZipSnippets

Public Module Methods

Public Sub ZipSnippet(byval zipFile as string, ByVal Params zipFilesFolders as String())

	Using zip As New ZipFile()
	dim qu as new Queue(of String)(zipFilesFolders)
	dim filename as string = string.empty
	dim zipFolder as string = string.empty
	Do while qu.Any
		zipFolder = string.empty
		fileName as string = qu.Enqueue
		if qu.Any then zipFolder = qu.Enqueue 
		if string.isnullorwhitespace(zipFolder) then
			 ' add file into the zip directory in the zip archive
			 zip.AddFile(fileName, zipFolder)
		else
			 ' add file into the root directory in the zip archive
			 zip.AddFile(fileName)
		endif
	End Do
	zip.Save(zipFile)
	End Using

End Sub

Private Sub ExtractProgress(byval sender as Object, byval e as ExtractProgressEventArgs) 
End Sub

Public Sub UnzipSnippet(byval zipFile as string, ByVal targetFolder as String)

	' Here is some VB code that unpacks a zip file (extracts all the entries):
	Using zip As ZipFile = ZipFile.Read(zipFile)   
       AddHandler zip.ExtractProgress, AddressOf ExtractProgress
       Dim e As ZipEntry   
       ' here, we extract every entry, but we could extract    
       ' based on entry name, size, date, etc.   
       For Each e As ZipEntry  In zip
           e.Extract(targetDir, ExtractExistingFileAction.OverwriteSilently)   
       Next  
   End Using  

End Sub

End Module

End Namespace