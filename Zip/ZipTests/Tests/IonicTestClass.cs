// IonicTestClass.cs
// ------------------------------------------------------------------
//
// Copyright (c) 2009 Dino Chiesa.
// All rights reserved.
//
// This code module is part of DotNetZip, a zipfile class library.
//
// ------------------------------------------------------------------
//
// This code is licensed under the Microsoft Public License.
// See the file License.txt for the license details.
// More info on: http://dotnetzip.codeplex.com
//
// ------------------------------------------------------------------
//
// last saved (in emacs):
// Time-stamp: <2011-July-26 10:04:54>
//
// ------------------------------------------------------------------
//
// This module defines the base class for DotNetZip test classes.
//
// ------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using System.Net;
using System.Linq;
using System.IO;
using Ionic.Zip;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Ionic.Zip.Tests.Utilities
{
    [TestClass]
    public class IonicTestClass
    {
        protected System.Random Rnd { get; set; }
        protected System.Collections.Generic.List<string> FilesToRemove { get; set; }
        protected static string CurrentDir = null;
        private string _TopLevelDir;
        protected string TopLevelDir
        {
            get => this._TopLevelDir;
            set => this._TopLevelDir = value;
        }

        private string _Wzunzip = null;
        private string _Wzzip = null;
        private string _Sevenzip = null;
        private string _Zipit = null;
        private string _Infozipzip = null;
        private string _Infozipunzip = null;
        private bool? _ZipitIsPresent;
        private bool? _WinZipIsPresent;
        private bool? _SevenZipIsPresent;
        private bool? _InfoZipIsPresent;

        protected Ionic.CopyData.Transceiver TransmitReceive { get; set; }


        public IonicTestClass()
        {
            this.Rnd = new System.Random();
            this.FilesToRemove = new System.Collections.Generic.List<string>();
        }

        #region Context

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext { get; set; }

        #endregion



        #region Test Init and Cleanup
        //
        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        [ClassInitialize]
#pragma warning disable IDE0060 // Remove unused parameter
        public static void BaseClassInitialize( TestContext testContext )
#pragma warning restore IDE0060 // Remove unused parameter
        {
            CurrentDir = Directory.GetCurrentDirectory();
            Assert.AreNotEqual<string>( Path.GetFileName( CurrentDir ), "Temp", "at startup" );
        }

        //
        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //


        // Use TestInitialize to run code before running each test
        [TestInitialize()]
        public void MyTestInitialize()
        {
            if ( CurrentDir == null )
            {
                CurrentDir = Directory.GetCurrentDirectory();
            }

            TestUtilities.Initialize( out this._TopLevelDir );
            this.FilesToRemove.Add( this.TopLevelDir );
            Directory.SetCurrentDirectory( this.TopLevelDir );
        }

        // Use TestCleanup to run code after each test has run
        [TestCleanup()]
        public void MyTestCleanup()
        {
            // The CWD of the monitoring process is the CurrentDir,
            // therefore this test must shut down the monitoring process
            // FIRST, to allow the deletion of the directory.
            if ( this.TransmitReceive != null )
            {
                try
                {
                    this.TransmitReceive.Send( "stop" );
                    this.TransmitReceive = null;
                }
#pragma warning disable CA1031 // Do not catch general exception types
                catch { }
#pragma warning restore CA1031 // Do not catch general exception types
            }

            TestUtilities.Cleanup( CurrentDir, this.FilesToRemove );
        }

        #endregion



        internal string Exec( string program, string args )
        {
            return this.Exec( program, args, true );
        }

        internal string Exec( string program, string args, bool waitForExit )
        {
            return this.Exec( program, args, waitForExit, true );
        }

        internal string Exec( string program, string args, bool waitForExit, bool emitOutput )
        {
            if ( program == null )
            {
                throw new ArgumentException( "program" );
            }

            if ( args == null )
            {
                throw new ArgumentException( "args" );
            }

            // Microsoft.VisualStudio.TestTools.UnitTesting
            this.TestContext.WriteLine( "running command: {0} {1}", program, args );

            int rc = TestUtilities.Exec_NoContext( program, args, waitForExit, out string output );

            if ( rc != 0 )
            {
                throw new Exception( String.Format( "Non-zero RC {0}: {1}", program, output ) );
            }

            if ( emitOutput )
            {
                this.TestContext.WriteLine( "output: {0}", output );
            }
            else
            {
                this.TestContext.WriteLine( "A-OK. (output suppressed)" );
            }

            return output;
        }


#pragma warning disable CA1034 // Nested types should not be visible
        public class AsyncReadState
#pragma warning restore CA1034 // Nested types should not be visible
        {
            public System.IO.Stream S { get; set; }
#pragma warning disable CA1819 // Properties should not return arrays
            public static byte[] Buf => new byte[1024];
#pragma warning restore CA1819 // Properties should not return arrays
        }


        internal int ExecRedirectStdOut( string program, string args, string outFile )
        {
            if ( program == null )
            {
                throw new ArgumentException( "program" );
            }

            if ( args == null )
            {
                throw new ArgumentException( "args" );
            }

            this.TestContext.WriteLine( "running command: {0} {1}", program, args );

            Stream fs = File.Create( outFile );
            try
            {
                System.Diagnostics.Process p = new System.Diagnostics.Process {
                    StartInfo =
                    {
                        FileName = program,
                        CreateNoWindow = true,
                        Arguments = args,
                        WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden,
                        UseShellExecute = false,
                        RedirectStandardOutput = true,
                        RedirectStandardError = true,
                    }
                };

                _ = p.Start();

                var stdout = p.StandardOutput.BaseStream;
                var rs = new AsyncReadState { S = stdout };
                Action<System.IAsyncResult> readAsync1 = null;
                var readAsync = new Action<System.IAsyncResult>( ( ar ) => {
                    AsyncReadState state = ( AsyncReadState ) ar.AsyncState;
                    int n = state.S.EndRead( ar );
                    if ( n > 0 )
                    {
                        fs.Write( AsyncReadState.Buf, 0, n );
                        _ = state.S.BeginRead( AsyncReadState.Buf,
                                          0,
                                          AsyncReadState.Buf.Length,
                                          new System.AsyncCallback( readAsync1 ),
                                          state );
                    }
                } );
                readAsync1 = readAsync; // ??

                // kickoff
                _ = stdout.BeginRead( AsyncReadState.Buf,
                                 0,
                                 AsyncReadState.Buf.Length,
                                 new System.AsyncCallback( readAsync ),
                                 rs );

                p.WaitForExit();

                this.TestContext.WriteLine( "Process exited, rc={0}", p.ExitCode );

                return p.ExitCode;
            }
            finally
            {
                if ( fs != null )
                {
                    fs.Dispose();
                }
            }
        }


        protected string SevenZip => this.SevenZipIsPresent ? this._Sevenzip : null;

        protected string Zipit => this.ZipitIsPresent ? this._Zipit : null;

        protected string InfoZip => this.InfoZipIsPresent ? this._Infozipzip : null;

        protected string InfoZipUnzip => this.InfoZipIsPresent ? this._Infozipunzip : null;

        protected string Wzzip => this.WinZipIsPresent ? this._Wzzip : null;

        protected string Wzunzip => this.WinZipIsPresent ? this._Wzunzip : null;

        protected bool ZipitIsPresent
        {
            get {
                if ( this._ZipitIsPresent == null )
                {
                    string sourceDir = CurrentDir;
                    for ( int i = 0; i < 3; i++ )
                    {
                        sourceDir = Path.GetDirectoryName( sourceDir );
                    }

                    this._Zipit =
                        Path.Combine( sourceDir, "Tools\\Zipit\\bin\\Debug\\Zipit.exe" );

                    this._ZipitIsPresent = new Nullable<bool>( File.Exists( this._Zipit ) );
                }
                return this._ZipitIsPresent.Value;
            }
        }

        protected bool WinZipIsPresent
        {
            get {
                if ( this._WinZipIsPresent == null )
                {
                    if ( this._Wzunzip == null || this._Wzzip == null )
                    {
                        string progfiles = System.Environment.GetEnvironmentVariable( "ProgramFiles(x86)" );
                        this._Wzunzip = Path.Combine( progfiles, "winzip\\wzunzip.exe" );
                        this._Wzzip = Path.Combine( progfiles, "winzip\\wzzip.exe" );
                    }
                    this._WinZipIsPresent = new Nullable<bool>( File.Exists( this._Wzunzip ) && File.Exists( this._Wzzip ) );
                }
                return this._WinZipIsPresent.Value;
            }
        }

        protected bool SevenZipIsPresent
        {
            get {
                if ( this._SevenZipIsPresent == null )
                {
                    if ( this._Sevenzip == null )
                    {
                        string progfiles = System.Environment.GetEnvironmentVariable( "ProgramFiles" );
                        this._Sevenzip = Path.Combine( progfiles, "7-zip\\7z.exe" );
                    }
                    this._SevenZipIsPresent = new Nullable<bool>( File.Exists( this._Sevenzip ) );
                }
                return this._SevenZipIsPresent.Value;
            }
        }


        protected bool InfoZipIsPresent
        {
            get {
                if ( this._InfoZipIsPresent == null )
                {
                    if ( this._Infozipzip == null )
                    {
                        string progfiles = System.Environment.GetEnvironmentVariable( "ProgramFiles(x86)" );
                        this._Infozipzip = Path.Combine( progfiles, "infozip.org\\zip.exe" );
                        this._Infozipunzip = Path.Combine( progfiles, "infozip.org\\unzip.exe" );
                    }
                    this._InfoZipIsPresent = new Nullable<bool>( File.Exists( this._Infozipzip ) &&
                                                           File.Exists( this._Infozipunzip ) );
                }
                return this._InfoZipIsPresent.Value;
            }
        }

        internal string BasicVerifyZip( string zipfile )
        {
            return this.BasicVerifyZip( zipfile, null );
        }


        internal string BasicVerifyZip( string zipfile, string password )
        {
            return this.BasicVerifyZip( zipfile, password, true );
        }

        internal string BasicVerifyZip( string zipfile, string password, bool emitOutput )
        {
            return this.BasicVerifyZip( zipfile, password, emitOutput, null );
        }


        internal string BasicVerifyZip( string zipfile, string password, bool emitOutput,
                                       EventHandler<ExtractProgressEventArgs> extractProgress )
        {
            // basic verification of the zip file - can it be extracted?
            // The extraction tool will verify checksums and passwords, as appropriate
#if NOT
            if (WinZipIsPresent)
            {
                TestContext.WriteLine("Verifying zip file {0} with WinZip", zipfile);
                string args = (password == null)
                    ? String.Format("-t {0}", zipfile)
                    : String.Format("-s{0} -t {1}", password, zipfile);

                string wzunzipOut = this.Exec(wzunzip, args, true, emitOutput);
            }
            else
#endif
            {
                this.TestContext.WriteLine( "Verifying zip file {0} with DotNetZip", zipfile );
                ReadOptions options = new ReadOptions();
                if ( emitOutput )
                {
                    options.StatusMessageWriter = new StringWriter();
                }

                string extractDir = "verify";
                int c = 0;
                while ( Directory.Exists( extractDir + c ) )
                {
                    c++;
                }

                extractDir += c;

                using ( ZipFile zip2 = ZipFile.Read( zipfile, options ) )
                {
                    zip2.Password = password;
                    if ( extractProgress != null )
                    {
                        zip2.ExtractProgress += extractProgress;
                    }

                    zip2.ExtractAll( extractDir );
                }
                // emit output, as desired
                if ( emitOutput )
                {
                    this.TestContext.WriteLine( "{0}", options.StatusMessageWriter.ToString() );
                }

                return extractDir;
            }
        }



        internal static void CreateFilesAndChecksums( string subdir,
                                                     out string[] filesToZip,
                                                     out Dictionary<string, byte[]> checksums )
        {
            CreateFilesAndChecksums( subdir, 0, 0, out filesToZip, out checksums );
        }


        internal static void CreateFilesAndChecksums( string subdir,
                                                     int numFiles,
                                                     int baseSize,
                                                     out string[] filesToZip,
                                                     out Dictionary<string, byte[]> checksums )
        {
            // create a bunch of files
            filesToZip = TestUtilities.GenerateFilesFlat( subdir, numFiles, baseSize );
            DateTime atMidnight = new DateTime( DateTime.Now.Year,
                                               DateTime.Now.Month,
                                               DateTime.Now.Day );
            DateTime fortyFiveDaysAgo = atMidnight - new TimeSpan( 45, 0, 0, 0 );

            // get checksums for each one
            checksums = new Dictionary<string, byte[]>();

            var rnd = new System.Random();
            foreach ( var f in filesToZip )
            {
                if ( rnd.Next( 3 ) == 0 )
                {
                    File.SetLastWriteTime( f, fortyFiveDaysAgo );
                }
                else
                {
                    File.SetLastWriteTime( f, atMidnight );
                }

                var key = Path.GetFileName( f );
                var chk = TestUtilities.ComputeChecksum( f );
                checksums.Add( key, chk );
            }
        }

        protected static void CreateLargeFilesWithChecksums
            ( string subdir,
             int numFiles,
             Action<int, int, Int64> update,
             out string[] filesToZip,
             out Dictionary<string, byte[]> checksums )
        {
            var rnd = new System.Random();
            // create a bunch of files
            filesToZip = TestUtilities.GenerateFilesFlat( subdir,
                                                         numFiles,
                                                         256 * 1024,
                                                         3 * 1024 * 1024,
                                                         update );

            var dates = new DateTime[rnd.Next( 6 ) + 7];
            // midnight
            dates[0] = new DateTime( DateTime.Now.Year,
                                    DateTime.Now.Month,
                                    DateTime.Now.Day );

            for ( int i = 1; i < dates.Length; i++ )
            {
                dates[i] = DateTime.Now -
                    new TimeSpan( rnd.Next( 300 ),
                                 rnd.Next( 23 ),
                                 rnd.Next( 60 ),
                                 rnd.Next( 60 ) );
            }

            // get checksums for each one
            checksums = new Dictionary<string, byte[]>();

            foreach ( var f in filesToZip )
            {
                File.SetLastWriteTime( f, dates[rnd.Next( dates.Length )] );
                var key = Path.GetFileName( f );
                var chk = TestUtilities.ComputeChecksum( f );
                checksums.Add( key, chk );
            }
        }



        protected void VerifyChecksums( string extractDir,
            System.Collections.Generic.IEnumerable<String> filesToCheck,
            Dictionary<string, byte[]> checksums )
        {
            this.TestContext.WriteLine( "" );
            this.TestContext.WriteLine( "Verify checksums..." );
            int count = 0;
            foreach ( var fqPath in filesToCheck )
            {
                var f = Path.GetFileName( fqPath );
                var extractedFile = Path.Combine( extractDir, f );
                Assert.IsTrue( File.Exists( extractedFile ), "File does not exist ({0})", extractedFile );
                var chk = TestUtilities.ComputeChecksum( extractedFile );
                Assert.AreEqual<String>( TestUtilities.CheckSumToString( checksums[f] ),
                                        TestUtilities.CheckSumToString( chk ),
                                        String.Format( "Checksums for file {0} do not match.", f ) );
                count++;
            }

            if ( checksums.Count < count )
            {
                this.TestContext.WriteLine( "There are {0} more extracted files than checksums", count - checksums.Count );
                foreach ( var file in filesToCheck )
                {
                    if ( !checksums.ContainsKey( file ) )
                    {
                        this.TestContext.WriteLine( "Missing: {0}", Path.GetFileName( file ) );
                    }
                }
            }

            if ( checksums.Count > count )
            {
                this.TestContext.WriteLine( "There are {0} more checksums than extracted files", checksums.Count - count );
                foreach ( var file in checksums.Keys )
                {
                    var selection = from f in filesToCheck where Path.GetFileName( f ).Equals( file ) select f;

                    if ( selection.Count() == 0 )
                    {
                        this.TestContext.WriteLine( "Missing: {0}", Path.GetFileName( file ) );
                    }
                }
            }


            Assert.AreEqual<Int32>( checksums.Count, count, "There's a mismatch between the checksums and the filesToCheck." );
        }
    }


}
