// RandomTextGenerator.cs
// ------------------------------------------------------------------
//
// Copyright (c) 2009 Dino Chiesa
// All rights reserved.
//
// This code module is part of DotNetZip, a zipfile class library.
//
// ------------------------------------------------------------------
//
// This code is licensed under the Microsoft Public License.
// See the file License.txt for the license details.
// More info on: http://dotnetzip.codeplex.com
//
// ------------------------------------------------------------------
//
// last saved (in emacs):
// Time-stamp: <2011-July-13 16:37:19>
//
// ------------------------------------------------------------------
//
// This module defines a class that generates random text sequences
// using a Markov chain.
//
// ------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using System.Net;
using System.IO;
using Ionic.Zip;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Threading;

namespace Ionic.Zip.Tests.Utilities
{

    public class RandomTextGenerator
    {
        private static readonly string[] Uris = new string[]
            {
                // "Through the Looking Glass", by Lewis Carroll (~181k)
                "http://www.gutenberg.org/files/12/12.txt",

                // Decl of Independence (~16k)
                "http://www.gutenberg.org/files/16780/16780.txt",

                // Decl of Independence, alternative source
                "http://www.constitution.org/usdeclar.txt",

                // Section 552a of the US code - on privacy for individuals
                "http://www.opm.gov/feddata/usc552a.txt",

                // The Naval War of 1812, by Theodore Roosevelt (968k)
                "http://www.gutenberg.org/dirs/etext05/7trnv10.txt",

                // On Prayer and the Contemplative Life, by Thomas Aquinas (440k)
                "http://www.gutenberg.org/files/22295/22295.txt",

                // IETF RFC 1951 - the DEFLATE format
                "http://www.ietf.org/rfc/rfc1951.txt",

                // pkware's appnote
                "http://www.pkware.com/documents/casestudies/APPNOTE.TXT",
            };
        private readonly SimpleMarkovChain _Markov;

        public RandomTextGenerator()
        {
            System.Random rnd = new System.Random();
            int cycles = 0;
            string seedText;
            do
            {
                try
                {
                    string uri = Uris[rnd.Next( Uris.Length )];
                    seedText = GetPageMarkup( uri );
                }
                catch ( System.Net.WebException )
                {
                    cycles++;
                    if ( cycles > 8 )
                    {
                        throw;
                    }

                    seedText = null;
                }
            } while ( seedText == null );

            this._Markov = new SimpleMarkovChain( seedText );
        }


        public string Generate( int length )
        {
            return this._Markov.GenerateText( length );
        }


        private static string GetPageMarkup( string uri )
        {
            string pageData = null;
            using ( WebClient client = new WebClient() )
            {
                pageData = client.DownloadString( uri );
            }
            return pageData;
        }
    }


    /// <summary>
    /// Implements a simple Markov chain for text.
    /// </summary>
    ///
    /// <remarks>
    /// Uses a Markov chain starting with some base texts to produce
    /// random natural-ish text. This implementation is based on Pike's
    /// perl implementation, see
    /// http://cm.bell-labs.com/cm/cs/tpop/markov.pl
    /// </remarks>
    public class SimpleMarkovChain
    {
        private readonly Dictionary<String, List<String>> _Table = new Dictionary<String, List<String>>();
        private readonly System.Random _Rnd = new System.Random();

        public SimpleMarkovChain( string seed )
        {
            string NEWLINE = "\n";
            string key = NEWLINE;
            var sr = new StringReader( seed );
            string line;
            while ( (line = sr.ReadLine()) != null )
            {
                foreach ( var word in line.SplitByWords() )
                {
                    var w = (string.IsNullOrEmpty(word)) ? NEWLINE : word; // newline
                    if ( word == "\r" )
                    {
                        w = NEWLINE;
                    }

                    if ( !this._Table.ContainsKey( key ) )
                    {
                        this._Table.Add( key, new List<string>() );
                    }

                    this._Table[key].Add( w );
                    key = w.ToLower().TrimPunctuation();
                }
            }
            if ( !this._Table.ContainsKey( key ) )
            {
                this._Table.Add( key, new List<string>() );
            }

            this._Table[key].Add( NEWLINE );
        }


        internal void Diag()
        {
            Console.WriteLine( "There are {0} keys in the table", this._Table.Keys.Count );
            foreach ( string s in this._Table.Keys )
            {
                string x = s.Replace( "\n", "�" );
                var y = this._Table[s].ToArray();
                Console.WriteLine( "  {0}: {1}", x, String.Join( ", ", y ) );
            }
        }

        internal void ShowList( string word )
        {
            string x = word.Replace( "\n", "�" );
            if ( this._Table.ContainsKey( word ) )
            {
                var y = this._Table[word].ToArray();
                var z = Array.ConvertAll( y, x1 => x1.Replace( "\n", "�" ) );
                Console.WriteLine( "  {0}: {1}", x, String.Join( ", ", z ) );
            }
            else
            {
                Console.WriteLine( "  {0}: -key not found-", x );
            }
        }

        private List<string> _Keywords;
        private List<string> Keywords
        {
            get {
                if ( this._Keywords == null )
                {
                    this._Keywords = new List<String>( this._Table.Keys );
                }

                return this._Keywords;
            }
        }

        /// <summary>
        /// Generates random text with a minimum character length.
        /// </summary>
        ///
        /// <param name="minimumLength">
        /// The minimum length of text, in characters, to produce.
        /// </param>
        public string GenerateText( int minimumLength )
        {
            var chosenStartWord = this.Keywords[this._Rnd.Next( this.Keywords.Count )];
            return this.InternalGenerate( chosenStartWord, StopCriterion.NumberOfChars, minimumLength );
        }

        /// <summary>
        /// Generates random text with a minimum character length.
        /// </summary>
        ///
        /// <remarks>
        /// The first sentence will start with the given start word.
        /// </remarks>
        ///
        /// <param name="minimumLength">
        /// The minimum length of text, in characters, to produce.
        /// </param>
        /// <param name="start">
        /// The word to start with. If this word does not exist in the
        /// seed text, the generation will fail.
        /// </param>
        /// <seealso cref="GenerateText(int)"/>
        /// <seealso cref="GenerateWords(int)"/>
        /// <seealso cref="GenerateWords(string, int)"/>
        public string GenerateText( string start, int minimumLength )
        {
            return this.InternalGenerate( start, StopCriterion.NumberOfChars, minimumLength );
        }

        /// <summary>
        /// Generate random text with a minimum number of words.
        /// </summary>
        ///
        /// <remarks>
        /// The first sentence will start with the given start word.
        /// </remarks>
        ///
        /// <param name="minimumWords">
        /// The minimum number of words of text to produce.
        /// </param>
        /// <param name="start">
        /// The word to start with. If this word does not exist in the
        /// seed text, the generation will fail.
        /// </param>
        /// <seealso cref="GenerateText(int)"/>
        /// <seealso cref="GenerateText(string, int)"/>
        /// <seealso cref="GenerateWords(int)"/>
        public string GenerateWords( string start, int minimumWords )
        {
            return this.InternalGenerate( start, StopCriterion.NumberOfWords, minimumWords );
        }


        /// <summary>
        /// Generate random text with a minimum number of words.
        /// </summary>
        ///
        /// <param name="minimumWords">
        /// The minimum number of words of text to produce.
        /// </param>
        /// <seealso cref="GenerateText(int)"/>
        /// <seealso cref="GenerateWords(string, int)"/>
        public string GenerateWords( int minimumWords )
        {
            var chosenStartWord = this.Keywords[this._Rnd.Next( this.Keywords.Count )];
            return this.InternalGenerate( chosenStartWord, StopCriterion.NumberOfWords, minimumWords );
        }


        private string InternalGenerate( string start, StopCriterion crit, int limit )
        {
            string w1 = start.ToLower();
            StringBuilder sb = new StringBuilder();
            _ = sb.Append( start.Capitalize() );

            int consecutiveNewLines = 0;

            // About the stop criteria:
            // we keep going till we reach the specified number of words or chars, with the added
            // proviso that we have to complete the in-flight sentence when the limit is reached.

            for ( int i = 0;
                 (crit == StopCriterion.NumberOfWords && i < limit) ||
                     (crit == StopCriterion.NumberOfChars && sb.Length < limit) ||
                     consecutiveNewLines == 0;
                 i++ )
            {
                if ( this._Table.ContainsKey( w1 ) )
                {
                    var list = this._Table[w1];
                    int ix = this._Rnd.Next( list.Count );
                    string word = list[ix];
                    if ( word != "\n" )
                    {
                        // capitalize
                        _ = consecutiveNewLines > 0 ? sb.Append( word.Capitalize() ) : sb.Append( " " ).Append( word );

                        // words that end sentences get a newline
                        if ( word.EndsWith( "." ) )
                        {
                            if ( consecutiveNewLines == 0 || consecutiveNewLines == 1 )
                            {
                                _ = sb.Append( "\n" );
                            }

                            consecutiveNewLines++;
                        }
                        else
                        {
                            consecutiveNewLines = 0;
                        }
                    }
                    w1 = word.ToLower().TrimPunctuation();
                }
            }
            return sb.ToString();
        }



        private enum StopCriterion
        {
            NumberOfWords,
            NumberOfChars
        }

    }



    public class RandomTextInputStream : Stream
    {
        private readonly RandomTextGenerator _Rtg;
        private long _DesiredLength;
        private readonly System.Text.Encoding _Encoding;
        private readonly byte[][] _RandomText;
        private readonly System.Random _Rnd;
        private byte[] _Src = null;
        private const int _ChunkSize = 1024 * 128;
        private const int _Chunks = 48;

        public RandomTextInputStream( Int64 length )
            : this( length, System.Text.Encoding.GetEncoding( "ascii" ) )
        {
        }

        public RandomTextInputStream( Int64 length, System.Text.Encoding encoding )
            : base()
        {
            this._DesiredLength = length;
            this._Rtg = new RandomTextGenerator();
            this._Encoding = encoding;
            this._RandomText = new byte[_Chunks][];
            this._Rnd = new System.Random();
        }

        /// <summary>
        ///   for diagnostic purposes only
        /// </summary>
        public int GetNewTextCount { get; private set; }

        public new void Dispose()
        {
            this.Dispose( true );
        }

        /// <summary>The Dispose method</summary>
        protected override void Dispose( bool disposeManagedResources )
        {
        }

        private byte[] GetNewText()
        {
            this.GetNewTextCount++;
            int nowServing = this._Rnd.Next( _Chunks );
            if ( this._RandomText[nowServing] == null )
            {
                this._RandomText[nowServing] = this._Encoding.GetBytes( this._Rtg.Generate( _ChunkSize ) );
            }

            return this._RandomText[nowServing];
        }

        public Int64 BytesRead { get; private set; }

        public override int Read( byte[] buffer, int offset, int count )
        {
            int bytesToReadThisTime = count;
            if ( this._DesiredLength - this.BytesRead < bytesToReadThisTime )
            {
                bytesToReadThisTime = unchecked(( int ) (this._DesiredLength - this.BytesRead));
            }

            int bytesToRead = bytesToReadThisTime;
            while ( bytesToRead > 0 )
            {
                this._Src = this.GetNewText();
                int bytesAvailable = this._Src.Length;
                int chunksize = (bytesToRead > bytesAvailable)
                    ? bytesAvailable
                    : bytesToRead;

                Buffer.BlockCopy( this._Src, 0, buffer, offset, chunksize );
                bytesToRead -= chunksize;
                offset += chunksize;
            }
            this.BytesRead += bytesToReadThisTime;
            return bytesToReadThisTime;
        }

        public override void Write( byte[] buffer, int offset, int count )
        {
            throw new NotSupportedException();
        }

        public override bool CanRead => true;
        public override bool CanSeek => false;

        public override bool CanWrite => false;

        public override long Length => this._DesiredLength;

        public override long Position
        {
            get => this._DesiredLength - this.BytesRead;
            set => throw new NotSupportedException();
        }

        public override long Seek( long offset, System.IO.SeekOrigin origin )
        {
            throw new NotSupportedException();
        }

        public override void SetLength( long value )
        {
            if ( value < this.BytesRead )
            {
                throw new NotSupportedException();
            }

            this._DesiredLength = value;
        }

        public override void Flush()
        {
        }
    }



}
