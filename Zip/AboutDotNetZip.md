About DotNetZip 

This is DotNetZip, a free zip tool. It reads or creates zip files. The
tool is built on the DotNetZip library.

This zip tool and the library that it uses are part of the DotNetZip
project, an open-source effort to build a zip library and tools for
.NET. In addition to the library and the tool, the DotNetZip project
delivers various command-line tools for creating and unpacking zip
files.

The DotNetZip library enables any .NET application to read and write ZIP
files, simply and easily. This graphical tool is just one example of the
things you can build with the library.

The DotNetZip library supports many advanced ZIP features like ZIP64,
WinZip-compatible AES encryption, Unicode or arbitrary code pages,
self-extracting archives, archive and entry comments, split archives,
and more. DotNetZip has a simple and clean programming interface, which
takes advantage of nice .NET features like eventing and generic
collections. There is a full helpfile with plenty of code examples to
get you started.

DotNetZip is really nice to use with .NET apps, but it can also be used
from Powershell scripts, as well as any COM-compliant language or
environment. For example, you can write VBScript code that uses
DotNetZip, to script the creation of AES-encrypted ZIP files.

DotNetZip is CLS-compliant, FxCop-approved, fair-trade, color-blind,
family-friendly, low-fat, and totally free of cost. The library can be
used from any .NET programming language. There are versions of the
library for the Compact Framework, Silverlight, and the regular .NET
Framework, though this tool runs only on the desktop Framework.
DotNetZip requires .NET 2.0 at a minimum.

The library and this tool are produced in an open source project hosted
at CodePlex: http:// DotNetZip.codeplex.com/.

DotNetZip is donationware. This tool and the library that supports it
are free to use, but if you like the tool or library and find it them
useful, you are encouraged to donate. Proceeds benefit a charity. (which
may be me, if I am unemployed). To learn more:
http://cheeso.members.winisp.net/DotNetZipDonate.aspx

Licensing.
----------

DotNetZip is licensed under the MS-PL.

> Microsoft Public License (Ms-PL)
>
> This license governs use of the accompanying software, DotNetZip
> (\"the software\"). If you use the software, you accept this license.
> If you do not accept the license, do not use the software.
>
> 1\. Definitions
>
> The terms \"reproduce,\" \"reproduction,\" \"derivative works,\" and
> \"distribution\" have the same meaning here as under U.S. copyright
> law.
>
> A \"contribution\" is the original software, or any additions or
> changes to the software.
>
> A \"contributor\" is any person that distributes its contribution
> under this license.
>
> \"Licensed patents\" are a contributor\'s patent claims that read
> directly on its contribution.
>
> 2\. Grant of Rights
>
> \(A\) Copyright Grant- Subject to the terms of this license, including
> the license conditions and limitations in section 3, each contributor
> grants you a non-exclusive, worldwide, royalty-free copyright license
> to reproduce its contribution, prepare derivative works of its
> contribution, and distribute its contribution or any derivative works
> that you create.
>
> \(B\) Patent Grant- Subject to the terms of this license, including
> the license conditions and limitations in section 3, each contributor
> grants you a non-exclusive, worldwide, royalty-free license under its
> licensed patents to make, have made, use, sell, offer for sale,
> import, and/or otherwise dispose of its contribution in the software
> or derivative works of the contribution in the software.
>
> 3\. Conditions and Limitations
>
> \(A\) No Trademark License- This license does not grant you rights to
> use any contributors\' name, logo, or trademarks.
>
> \(B\) If you bring a patent claim against any contributor over patents
> that you claim are infringed by the software, your patent license from
> such contributor to the software ends automatically.
>
> \(C\) If you distribute any portion of the software, you must retain
> all copyright, patent, trademark, and attribution notices that are
> present in the software.
>
> \(D\) If you distribute any portion of the software in source code
> form, you may do so only under this license by including a complete
> copy of this license with your distribution. If you distribute any
> portion of the software in compiled or object code form, you may only
> do so under a license that complies with this license.
>
> \(E\) The software is licensed \"as-is.\" You bear the risk of using
> it. The contributors give no express warranties, guarantees or
> conditions. You may have additional consumer rights under your local
> laws which this license cannot change. To the extent permitted under
> your local laws, the contributors exclude the implied warranties of
> merchantability, fitness for a particular purpose and
> non-infringement.

DotNetZip is based in part on code derived from jzlib, which itself is
derived from ZLIB, which are both licensed separately.

The following applies to ZLIB:

> Copyright (C) 1995-2004 Jean-loup Gailly and Mark Adler
>
> The ZLIB software is provided \'as-is\', without any express or
> implied warranty. In no event will the authors be held liable for any
> damages arising from the use of this software.
>
> Permission is granted to anyone to use this software for any purpose,
> including commercial applications, and to alter it and redistribute it
> freely, subject to the following restrictions:
>
> 1\. The origin of this software must not be misrepresented; you must
> not claim that you wrote the original software. If you use this
> software in a product, an acknowledgment in the product documentation
> would be appreciated but is not required.
>
> 2\. Altered source versions must be plainly marked as such, and must
> not be misrepresented as being the original software.
>
> 3\. This notice may not be removed or altered from any source
> distribution.
>
> Jean-loup Gailly <jloup@gzip.org>\
> Mark Adler madler\@alumni.caltech.edu

The following applies to jzlib:

> JZlib 0.0.\* were released under the GNU LGPL license. Later, we have
> switched over to a BSD-style license.
>
> \-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\--
>
> Copyright (c) 2000,2001,2002,2003 ymnk, JCraft,Inc. All rights
> reserved.
>
> Redistribution and use in source and binary forms, with or without
> modification, are permitted provided that the following conditions are
> met:
>
> 1\. Redistributions of source code must retain the above copyright
> notice, this list of conditions and the following disclaimer.
>
> 2\. Redistributions in binary form must reproduce the above copyright
> notice, this list of conditions and the following disclaimer in the
> documentation and/or other materials provided with the distribution.
>
> 3\. The names of the authors may not be used to endorse or promote
> products derived from this software without specific prior written
> permission.
>
> THIS SOFTWARE IS PROVIDED \`\`AS IS\'\' AND ANY EXPRESSED OR IMPLIED
> WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
> MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
> IN NO EVENT SHALL JCRAFT, INC. OR ANY CONTRIBUTORS TO THIS SOFTWARE BE
> LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
> CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
> SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
> BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
> WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
> OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
> IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

Tool Usage
----------

The tool is pretty self-explanatory. You should be able to figure it
out.

There are 3 tabs -- one is used for creating zip files; one is used for
reading zip files, and this tab provides this text.

In the create tab, the top groupbox lets you specify which files to add
into a zip.

![](media/img1.png){width="4.46875in" height="0.6666666666666666in"}

Specify directories and file name patterns, then click the + button to
add those files. You can also tell the tool whether to recurse a
directory (select files from subdirectories) and whether to traverse
junctions in the filesystem. Also specify the "directory in zip" for
those files.

The middle group box of settings lets you specify how to add those files
into the zip file you are creating. Here you specify encryption,
encoding, compression level, whether to create a self-extracting
archive, split zips, and so on. You can also specify zipfile comments
and other zip settings.

![](media/img2.png){width="6.0625in" height="2.1770833333333335in"}

The files you've provisionally added into the zip appear in a listbox
below. You can edit this list, and change the path or filename used in
the zipfile for any entry.

Click "Zip it!" to create the zip with the given specifications.

### Drag and Drop

You can also drag-and-drop into the zip tool. If you drag a .zip file
onto the listview that appears at the bottom of the Read tab, that will
open the given zip file and read it.

Drag a set of fen to the listview that appears at the bottom of the
Create tab to add that set of files to the zip file you are creating.
