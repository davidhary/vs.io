using System.Reflection;
using System.Resources;
[assembly: AssemblyCompany("Integrated Scientific Resources")]
[assembly: AssemblyCopyright("(c) 2012 Andrew Rissing. All rights reserved")]
[assembly: AssemblyTrademark("Licensed under The MIT License")]
[assembly: NeutralResourcesLanguage("en-US", UltimateResourceFallbackLocation.MainAssembly)]
[assembly: AssemblyCulture("")]

