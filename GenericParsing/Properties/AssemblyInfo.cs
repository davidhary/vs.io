using System;
using System.Reflection;
[assembly: AssemblyTitle("GenericParsing")]
[assembly: AssemblyProduct("isr.IO.GenericParsing")]
[assembly: AssemblyDescription("This assembly contains tools to aid in parsing flat files.")]
[assembly: AssemblyConfiguration("")]
[assembly: CLSCompliant(true)]
