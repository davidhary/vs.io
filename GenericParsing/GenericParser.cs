//  GenericParsing
//  Copyright © 2010 Andrew Rissing
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights 
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
//  of the Software, and to permit persons to whom the Software is furnished to do so, 
//  subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in all 
//  copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
//  INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//  PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE 
//  FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
//  ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#region Using Directives

using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Xml;

#endregion Using Directives

namespace GenericParsing
{
    /// <summary>
    /// The <see cref="GenericParser"/> class is designed to be a flexible and efficient manner
    /// of parsing various flat files formats.
    /// </summary>
    /// <threadsafety static="false" instance="false"/>
    public class GenericParser : IDisposable
    {
        #region Constants

        #region Default Values

        /// <summary>
        ///   Defines the default max buffer size (4096).
        /// </summary>
        public const int DefaultMaxBufferSize = 4096;
        /// <summary>
        ///   Defines the max rows value (0 = no limit).
        /// </summary>
        public const int DefaultMaxRows = 0;
        /// <summary>
        ///   Defines the number of skip starting data rows (0).
        /// </summary>
        public const int DefaultSkipStartingDataRows = 0;
        /// <summary>
        ///   Defines the number of expected columns (0 = no limit).
        /// </summary>
        public const int DefaultExpectedColumnCount = 0;
        /// <summary>
        ///   Defines the default first row has a header (false).
        /// </summary>
        public const bool DefaultFirstRowHasHeader = false;
        /// <summary>
        ///   Defines the default value for trim results (false).
        /// </summary>
        public const bool DefaultTrimResults = false;
        /// <summary>
        ///   Defines the default value for stripping control characters (false).
        /// </summary>
        public const bool DefaulStripControlCharacters = false;
        /// <summary>
        ///   Defines the default value for skipping empty rows (true).
        /// </summary>
        public const bool DefaulSkipEmptyRows = true;
        /// <summary>
        ///   Defines the default value for text field type (Delimited).
        /// </summary>
        public const FieldType DefaultTextFieldType = FieldType.Delimited;
        /// <summary>
        ///   Defines the default for first row sets the expected column count (false).
        /// </summary>
        public const bool DefaultFirstRowSetsExpectedColumnCount = false;
        /// <summary>
        ///   Defines the default column delimiter (',').
        /// </summary>
        public const char DefaultColumnDelimiter = ',';
        /// <summary>
        ///   Defines the default text qualifier ('\"').
        /// </summary>
        public const char DefaultTextQualifier = '\"';
        /// <summary>
        ///   Defines the default comment row character ('#').
        /// </summary>
        public const char DefaultCommentCharacter = '#';

        #endregion Default Values

        /// <summary>
        ///   Indicates the current type of row being processed.
        /// </summary>
        private enum RowType
        {
            /// <summary>
            ///   The row type is unknown and needs to be determined.
            /// </summary>
            Unknown = 0,
            /// <summary>
            ///   The row type is a comment row and can be ignored.
            /// </summary>
            CommentRow = 1,
            /// <summary>
            ///   The row type is a header row to name the columns.
            /// </summary>
            HeaderRow = 2,
            /// <summary>
            ///   The row type is a skipped row that is not intended to be extracted.
            /// </summary>
            SkippedRow = 3,
            /// <summary>
            ///   The row type is data row that is intended to be extracted.
            /// </summary>
            DataRow = 4
        }

        #region XmlConfig Constants

#pragma warning disable IDE1006 // Naming Styles
        private const string XML_ROOT_NODE = "GenericParser";
        private const string XML_COLUMN_WIDTH = "ColumnWidth";
        private const string XML_COLUMN_WIDTHS = "ColumnWidths";
        private const string XML_MAX_BUFFER_SIZE = "MaxBufferSize";
        private const string XML_MAX_ROWS = "MaxRows";
        private const string XML_SKIP_STARTING_DATA_ROWS = "SkipStartingDataRows";
        private const string XML_EXPECTED_COLUMN_COUNT = "ExpectedColumnCount";
        private const string XML_FIRST_ROW_HAS_HEADER = "FirstRowHasHeader";
        private const string XML_TRIM_RESULTS = "TrimResults";
        private const string XML_STRIP_CONTROL_CHARS = "StripControlChars";
        private const string XML_SKIP_EMPTY_ROWS = "SkipEmptyRows";
        private const string XML_TEXT_FIELD_TYPE = "TextFieldType";
        private const string XML_FIRST_ROW_SETS_EXPECTED_COLUMN_COUNT = "FirstRowSetsExpectedColumnCount";
        private const string XML_COLUMN_DELIMITER = "ColumnDelimiter";
        private const string XML_TEXT_QUALIFIER = "TextQualifier";
        private const string XML_ESCAPE_CHARACTER = "EscapeCharacter";
        private const string XML_COMMENT_CHARACTER = "CommentCharacter";
#pragma warning disable IDE0051 // Remove unused private members
        private const string XML_SAFE_STRING_DELIMITER = ",";
#pragma warning restore IDE0051 // Remove unused private members
#pragma warning restore IDE1006 // Naming Styles

        #endregion XmlConfig Constants

        #endregion Constants

        #region Static Code

        /// <summary>
        ///   Clones the provided array in a type-friendly way.
        /// </summary>
        /// <typeparam name="T">The type of the array to clone.</typeparam>
        /// <param name="array">The array to clone.</param>
        /// <returns>The cloned version of the array.</returns>
        private static T[] CloneArray<T>( T[] array )
        {
            T[] clone;

            if ( array != null )
            {
                clone = new T[array.Length];

                for ( int i = 0; i < array.Length; ++i )
                {
                    clone[i] = array[i];
                }
            }
            else
            {
                clone = null;
            }

            return clone;
        }

        #endregion Static Code

        #region Constructors

        /// <summary>
        ///   Constructs an instance of a <see cref="GenericParser"/> with the default settings.
        /// </summary>
        /// <remarks>
        ///   When using this constructor, the datasource must be set prior to using the parser
        ///   (using <see cref="GenericParser.SetDataSource(string)"/>), otherwise an exception will be thrown.
        /// </remarks>
        public GenericParser()
        {
            this.ParserState = ParserState.NoDataSource;
            this._TextReader = null;
            this.IsDisposed = false;
            this._ObjLock = new object();

            this.InitializeConfigurationVariablesThis();
        }
        /// <summary>
        ///   Constructs an instance of a <see cref="GenericParser"/> and sets the initial datasource
        ///   as the file referenced by the string passed in.
        /// </summary>
        /// <param name="strFileName">The file name to set as the initial datasource.</param>
        /// <exception cref="ArgumentNullException">Supplying <see langword="null"/>.</exception>
        /// <exception cref="ArgumentException">Supplying a filename to a file that does not exist.</exception>
        /// <exception cref="InvalidOperationException">Attempting to modify the configuration, while parsing.</exception>
        public GenericParser( string strFileName )
            : this()
        {
            this.SetDataSource( strFileName );
        }
        /// <summary>
        ///   Constructs an instance of a <see cref="GenericParser"/> and sets the initial datasource
        ///   as the file referenced by the string passed in with the provided encoding.
        /// </summary>
        /// <param name="strFileName">The file name to set as the initial datasource.</param>
        /// <param name="encoding">The <see cref="Encoding"/> of the file being referenced.</param>
        /// <exception cref="ArgumentNullException">Supplying <see langword="null"/>.</exception>
        /// <exception cref="ArgumentException">Supplying a filename to a file that does not exist.</exception>
        /// <exception cref="InvalidOperationException">Attempting to modify the configuration, while parsing.</exception>
        public GenericParser( string strFileName, Encoding encoding )
            : this()
        {
            this.SetDataSource( strFileName, encoding );
        }
        /// <summary>
        ///   Constructs an instance of a <see cref="GenericParser"/> and sets the initial datasource
        ///   as the <see cref="TextReader"/> passed in.
        /// </summary>
        /// <param name="txtReader">The <see cref="TextReader"/> containing the data to be parsed.</param>
        /// <exception cref="ArgumentNullException">Supplying <see langword="null"/>.</exception>
        /// <exception cref="InvalidOperationException">Attempting to modify the configuration, while parsing.</exception>
        public GenericParser( TextReader txtReader )
            : this()
        {
            this.SetDataSource( txtReader );
        }

        #endregion Constructors

        #region Public Code

        /// <summary>
        ///    Gets whether or not the instance has been disposed of.
        /// </summary>
        /// <value>
        ///   <para>
        ///     <see langword="true"/> - Indicates the instance has be disposed of.
        ///   </para>
        ///   <para>
        ///     <see langword="false"/> - Indicates the instance has not be disposed of.
        ///   </para>
        /// </value>
        public bool IsDisposed { get; private set; }
        /// <summary>
        ///   Gets or sets an integer array indicating the number of characters needed for each column.
        /// </summary>
        /// <value>An int[] containing the number of spaces for each column.</value>
        /// <remarks>
        ///   <para>
        ///     If parsing has started, this value cannot be updated.
        ///   </para>
        ///   <para>
        ///     By setting this property, the <see cref="TextFieldType"/> and <see cref="ExpectedColumnCount"/> are automatically updated.
        ///   </para>
        /// </remarks>
        /// <exception cref="ArgumentOutOfRangeException">Passing in an empty array or an
        /// array of values that have a number less than one.</exception>
        /// <exception cref="InvalidOperationException">Attempting to modify the configuration, while parsing.</exception>
#pragma warning disable CA1819 // Properties should not return arrays
        public int[] ColumnWidths
#pragma warning restore CA1819 // Properties should not return arrays
        {
            get => GenericParser.CloneArray( this._IaColumnWidths );
            set {
                if ( this.ParserState == ParserState.Parsing )
                {
                    throw new InvalidOperationException( "Parsing has already begun, close the existing parse first." );
                }

                this._IaColumnWidths = GenericParser.CloneArray( value );

                if ( value == null )
                {
                    this._TextFieldType = FieldType.Delimited;
                    this._ExpectedColumnCount = 0;
                }
                else
                {
                    if ( this._IaColumnWidths.Length < 1 )
                    {
                        throw new ArgumentOutOfRangeException( nameof( value ), "ColumnWidths cannot be an empty array." );
                    }

                    // Make sure all of the ColumnWidths are valid.
                    for ( int intColumnIndex = 0; intColumnIndex < this._IaColumnWidths.Length; ++intColumnIndex )
                    {
                        if ( this._IaColumnWidths[intColumnIndex] < 1 )
                        {
                            throw new ArgumentOutOfRangeException( nameof( value ), "ColumnWidths cannot contain a number less than one." );
                        }
                    }

                    this._TextFieldType = FieldType.FixedWidth;
                    this._ExpectedColumnCount = this._IaColumnWidths.Length;
                }
            }
        }
        /// <summary>
        ///   Gets or sets the maximum size of the internal buffer used to cache the data.
        /// </summary>
        /// <value>The maximum size of the internal buffer to cache data from the datasource.</value>
        /// <remarks>
        ///   <para>
        ///     Maintaining the smallest number possible here improves memory usage, but
        ///     trades it off for higher CPU usage. The <see cref="MaxBufferSize"/> must
        ///     be at least the size of one column of data, plus the Max(column delimiter
        ///     width, row delimiter width).
        ///   </para>
        ///   <para>
        ///     Default: 4096
        ///   </para>
        ///   <para>
        ///     If parsing has started, this value cannot be updated.
        ///   </para>
        /// </remarks>
        /// <exception cref="ArgumentOutOfRangeException">Setting the value to something less than one.</exception>
        /// <exception cref="InvalidOperationException">Attempting to modify the configuration, while parsing.</exception>
        public int MaxBufferSize
        {
            get => this._MaxBufferSize;
            set {
                if ( this.ParserState == ParserState.Parsing )
                {
                    throw new InvalidOperationException( "Parsing has already begun, close the existing parse first." );
                }

                if ( value > 0 )
                {
                    this._MaxBufferSize = value;
                }
                else
                {
                    throw new ArgumentOutOfRangeException( nameof( value ), value, "The MaxBufferSize must be greater than 0." );
                }
            }
        }
        /// <summary>
        ///   Gets or sets the maximum number of rows to parse.
        /// </summary>
        /// <value>The maximum number of rows to parse.</value>
        /// <remarks>
        ///   <para>
        ///     Setting the value to zero will cause all of the rows to be returned.
        ///   </para>
        ///  <para>
        ///    Default: 0
        ///  </para>
        ///   <para>
        ///     If parsing has started, this value cannot be updated.
        ///   </para>
        /// </remarks>
        /// <exception cref="InvalidOperationException">Attempting to modify the configuration, while parsing.</exception>
        public int MaxRows
        {
            get => this._MaxRows;
            set {
                if ( this.ParserState == ParserState.Parsing )
                {
                    throw new InvalidOperationException( "Parsing has already begun, close the existing parse first." );
                }

                this._MaxRows = value;

                if ( this._MaxRows < 0 )
                {
                    this._MaxRows = 0;
                }
            }
        }
        /// <summary>
        ///   Gets or sets the number of rows of data to ignore at the start of the file.
        /// </summary>
        /// <value>The number of data rows to initially skip in the datasource.</value>
        /// <remarks>
        ///   <para>
        ///     The header row (if present) and comment rows will not be taken into account
        ///     when determining the number of rows to skip. Setting the value to zero will
        ///     cause no rows to be ignored.
        ///   </para>
        ///   <para>
        ///     Default: 0
        ///   </para>
        ///   <para>
        ///     If parsing has started, this value cannot be updated.
        ///   </para>
        /// </remarks>
        /// <exception cref="InvalidOperationException">Attempting to modify the configuration, while parsing.</exception>
        public int SkipStartingDataRows
        {
            get => this._SkipStartingDataRows;
            set {
                if ( this.ParserState == ParserState.Parsing )
                {
                    throw new InvalidOperationException( "Parsing has already begun, close the existing parse first." );
                }

                this._SkipStartingDataRows = value;

                if ( this._SkipStartingDataRows < 0 )
                {
                    this._SkipStartingDataRows = 0;
                }
            }
        }
        /// <summary>
        ///   Gets or sets the number of rows of data that have currently been parsed.
        /// </summary>
        /// <value>The number of rows of data that have been parsed.</value>
        /// <remarks>The DataRowNumber property is read-only.</remarks>
        public int DataRowNumber { get; private set; }
        /// <summary>
        ///   Gets or sets how many rows in the file have been parsed.
        /// </summary>
        /// <value>The number of rows in the file that have been parsed.</value>
        /// <remarks>The <see cref="FileRowNumber"/> property is read-only and includes all
        /// rows possible (header, comment, and data).</remarks>
        public int FileRowNumber { get; private set; }
        /// <summary>
        ///   Gets or sets the expected number of columns to find in the data.  If
        ///   the number of columns differs, an exception will be thrown.
        /// </summary>
        /// <value>The number of columns expected per row of data.</value>
        /// <remarks>
        ///   <para>
        ///     Setting the value to zero will cause the <see cref="GenericParser"/> to ignore
        ///     the column count in case the number changes per row.
        ///   </para>
        ///   <para>
        ///     Default: 0
        ///   </para>
        ///   <para>
        ///     By setting this property, the <see cref="TextFieldType"/> and <see cref="ColumnWidths"/>
        ///     are automatically updated.
        ///   </para>
        ///   <para>
        ///     If parsing has started, this value cannot be updated.
        ///   </para>
        /// </remarks>
        /// <exception cref="InvalidOperationException">Attempting to modify the configuration, while parsing.</exception>
        public int ExpectedColumnCount
        {
            get => this._ExpectedColumnCount;
            set {
                if ( this.ParserState == ParserState.Parsing )
                {
                    throw new InvalidOperationException( "Parsing has already begun, close the existing parse first." );
                }

                this._ExpectedColumnCount = value;

                if ( this._ExpectedColumnCount < 0 )
                {
                    this._ExpectedColumnCount = 0;
                }

                // Make sure the ExpectedColumnCount matches the column width's
                // supplied.
                if ( (this._TextFieldType == FieldType.FixedWidth)
                 && (this._IaColumnWidths != null)
                 && (this._IaColumnWidths.Length != this._ExpectedColumnCount) )
                {
                    // Null it out to force the proper column width's to be supplied.
                    this._IaColumnWidths = null;
                    this._TextFieldType = FieldType.Delimited;
                }
            }
        }
        /// <summary>
        ///   Gets or sets whether or not the first row of data in the file contains
        ///   the header information.
        /// </summary>
        /// <value>
        ///   <para>
        ///     <see langword="true"/> - Header found on first 'data row'.
        ///   </para>
        ///   <para>
        ///     <see langword="false"/> - Header row does not exist.
        ///   </para>
        /// </value>
        /// <remarks>
        ///   <para>
        ///     Default: <see langword="false"/>
        ///   </para>
        ///   <para>
        ///     If parsing has started, this value cannot be updated.
        ///   </para>
        /// </remarks>
        /// <exception cref="InvalidOperationException">Attempting to modify the configuration, while parsing.</exception>
        public bool FirstRowHasHeader
        {
            get => this._FirstRowHasHeader;
            set {
                if ( this.ParserState == ParserState.Parsing )
                {
                    throw new InvalidOperationException( "Parsing has already begun, close the existing parse first." );
                }

                this._FirstRowHasHeader = value;
            }
        }
        /// <summary>
        ///   Gets or sets whether or not to trim the values for each column.
        /// </summary>
        /// <value>
        ///   <para>
        ///     <see langword="true"/> - Indicates to trim the resulting strings.
        ///   </para>
        ///   <para>
        ///     <see langword="false"/> - Indicates to not trim the resulting strings.
        ///   </para>
        /// </value>
        /// <remarks>
        ///   <para>
        ///     Trimming only occurs on the strings if they are not text qualified.
        ///     So by placing values in quotes, it preserves all whitespace within
        ///     quotes.
        ///   </para>
        ///   <para>
        ///     Default: <see langword="false"/>
        ///   </para>
        ///   <para>
        ///     If parsing has started, this value cannot be updated.
        ///   </para>
        /// </remarks>
        /// <exception cref="InvalidOperationException">Attempting to modify the configuration, while parsing.</exception>
        public bool TrimResults
        {
            get => this._TrimResults;
            set {
                if ( this.ParserState == ParserState.Parsing )
                {
                    throw new InvalidOperationException( "Parsing has already begun, close the existing parse first." );
                }

                this._TrimResults = value;
            }
        }
        /// <summary>
        ///   Gets or sets whether or not to strip control characters out of the input.
        /// </summary>
        /// <value>
        ///   <para>
        ///     <see langword="true"/> - Indicates to remove control characters from the input.
        ///   </para>
        ///   <para>
        ///     <see langword="false"/> - Indicates to leave control characters in the input.
        ///   </para>
        /// </value>
        /// <remarks>
        ///   <para>
        ///     Setting this to <see langword="true"/> can cause a performance boost.
        ///   </para>
        ///   <para>
        ///     Default: <see langword="false"/>
        ///   </para>
        ///   <para>
        ///     If parsing has started, this value cannot be updated.
        ///   </para>
        /// </remarks>
        /// <exception cref="InvalidOperationException">Attempting to modify the configuration, while parsing.</exception>
        public bool StripControlChars
        {
            get => this._StripControlChars;
            set {
                if ( this.ParserState == ParserState.Parsing )
                {
                    throw new InvalidOperationException( "Parsing has already begun, close the existing parse first." );
                }

                this._StripControlChars = value;
            }
        }
        /// <summary>
        ///   Gets or sets whether or not to skip empty rows in the input.
        /// </summary>
        /// <value>
        ///   <para>
        ///     <see langword="true"/> - Indicates to skip empty rows in the input.
        ///   </para>
        ///   <para>
        ///     <see langword="false"/> - Indicates to include empty rows in the input.
        ///   </para>
        /// </value>
        /// <remarks>
        ///   <para>
        ///     Default: <see langword="true"/>
        ///   </para>
        ///   <para>
        ///     If parsing has started, this value cannot be updated.
        ///   </para>
        /// </remarks>
        /// <exception cref="InvalidOperationException">Attempting to modify the configuration, while parsing.</exception>
        public bool SkipEmptyRows
        {
            get => this._SkipEmptyRows;
            set {
                if ( this.ParserState == ParserState.Parsing )
                {
                    throw new InvalidOperationException( "Parsing has already begun, close the existing parse first." );
                }

                this._SkipEmptyRows = value;
            }
        }
        /// <summary>
        ///   Gets whether or not the current row is an empty row.
        /// </summary>
        public bool IsCurrentRowEmpty { get; private set; }
        /// <summary>
        ///   Gets or sets the <see cref="FieldType"/> of the data encoded in the rows.
        /// </summary>
        /// <remarks>
        ///   <para>
        ///     By setting <see cref="ColumnWidths"/>, this property is automatically set.
        ///   </para>
        ///   <para>
        ///     Default: <see cref="FieldType.Delimited"/>
        ///   </para>
        ///   <para>
        ///     If parsing has started, this value cannot be updated.
        ///   </para>
        /// </remarks>
        /// <exception cref="InvalidOperationException">Attempting to modify the configuration, while parsing.</exception>
        public FieldType TextFieldType
        {
            get => this._TextFieldType;
            set {
                if ( this.ParserState == ParserState.Parsing )
                {
                    throw new InvalidOperationException( "Parsing has already begun, close the existing parse first." );
                }

                this._TextFieldType = value;

                if ( this._TextFieldType == FieldType.FixedWidth )
                {
                    this._ColumnDelimiter = null;
                    this._FirstRowSetsExpectedColumnCount = false;
                }
                else
                {
                    this._IaColumnWidths = null;
                }
            }
        }
        /// <summary>
        ///   Gets or sets the number of columns in the header/first data row determines
        ///   the expected number of columns in the data.
        /// </summary>
        /// <value>
        ///   <para>
        ///     <see langword="true"/> - Indicates the data's column count should match the header/first data row's column count.
        ///   </para>
        ///   <para>
        ///     <see langword="false"/> - Indicates the data's column count does not necessarily match the header/first data row's column count.
        ///   </para>
        /// </value>
        /// <remarks>
        ///   <para>
        ///     If set to <see langword="true"/>, <see cref="FieldType"/> will automatically be set to <see langword="false"/>.
        ///   </para>
        ///   <para>
        ///     Default: <see langword="false"/>
        ///   </para>
        ///   <para>
        ///     If parsing has started, this value cannot be updated.
        ///   </para>
        /// </remarks>
        /// <exception cref="InvalidOperationException">Attempting to modify the configuration, while parsing.</exception>
        public bool FirstRowSetsExpectedColumnCount
        {
            get => this._FirstRowSetsExpectedColumnCount;
            set {
                if ( this.ParserState == ParserState.Parsing )
                {
                    throw new InvalidOperationException( "Parsing has already begun, close the existing parse first." );
                }

                this._FirstRowSetsExpectedColumnCount = value;

                // If set to true, unset fixed width as it makes no sense.
                if ( value )
                {
                    this.TextFieldType = FieldType.Delimited;
                }
            }
        }
        /// <summary>
        ///   Gets the <see cref="GenericParsing.ParserState"/> value indicating the current
        ///   internal state of the parser.
        /// </summary>
        /// <value>The <see cref="State"/> property is read-only and is used to return
        /// information about the internal state of the parser.</value>
        public ParserState State => this.ParserState;
        /// <summary>
        ///   Gets or sets the character used to match the end of a column of data.
        /// </summary>
        /// <value>Contains the character used to delimit a column.</value>
        /// <remarks>
        ///   <para>
        ///     By setting this property, the <see cref="TextFieldType"/> is automatically
        ///     updated. This is only meaningful when performing delimited parsing.
        ///   </para>
        ///   <para>
        ///     Default: ','
        ///   </para>
        ///   <para>
        ///     If parsing has started, this value cannot be updated.
        ///   </para>
        /// </remarks>
        /// <exception cref="InvalidOperationException">Attempting to modify the configuration, while parsing.</exception>
        public char? ColumnDelimiter
        {
            get => this._ColumnDelimiter;
            set {
                if ( this.ParserState == ParserState.Parsing )
                {
                    throw new InvalidOperationException( "Parsing has already begun, close the existing parse first." );
                }
                else
                {
                    this._ColumnDelimiter = value;
                    this._TextFieldType = (value == null) ? FieldType.FixedWidth : FieldType.Delimited;
                }
            }
        }
        /// <summary>
        ///   Gets or sets the character that is used to enclose a string that would otherwise
        ///   be potentially trimmed (Ex. "  this  ").
        /// </summary>
        /// <value>
        ///   The character used to enclose a string, so that row/column delimiters are ignored
        ///   and whitespace is preserved.
        /// </value>
        /// <remarks>
        ///   <para>
        ///     The Text Qualifiers must be present at the beginning and end of the column to
        ///     have them properly removed from the ends of the string.  Furthermore, for a
        ///     string that has been enclosed with the text qualifier, if the text qualifier is
        ///     doubled up inside the string, the characters will be treated as an escape for
        ///     the literal character of the text qualifier (ie. "This""Test" will translate
        ///     with only one double quote inside the string).
        ///   </para>
        ///   <para>
        ///     Setting this to <see langword="null"/> can cause a performance boost, if none of the values are
        ///     expected to require escaping.
        ///   </para>
        ///   <para>
        ///     Default: '\"'
        ///   </para>
        ///   <para>
        ///     If parsing has started, this value cannot be updated.
        ///   </para>
        /// </remarks>
        /// <exception cref="InvalidOperationException">Attempting to modify the configuration, while parsing.</exception>
        public char? TextQualifier
        {
            get => this._TextQualifier;
            set {
                if ( this.ParserState == ParserState.Parsing )
                {
                    throw new InvalidOperationException( "Parsing has already begun, close the existing parse first." );
                }

                this._TextQualifier = value;
            }
        }
        /// <summary>
        ///   Gets or sets the character that is used to escape a character (Ex. "\"This\"").
        /// </summary>
        /// <value>The character used to escape row/column delimiters and the text qualifier.</value>
        /// <remarks>
        ///   <para>
        ///     Upon parsing the file, the escaped characters will be stripped out, leaving
        ///     the desired character in place.  To produce the escaped character, use the
        ///     escaped character twice (Ex. \\).  Text qualifiers are already assumed to be
        ///     escaped if used twice.
        ///   </para>
        ///   <para>
        ///     Setting this to <see langword="null"/> can cause a performance boost, if none of the values are
        ///     expected to require escaping.
        ///   </para>
        ///   <para>
        ///     Default: null
        ///   </para>
        ///   <para>
        ///     If parsing has started, this value cannot be updated.
        ///   </para>
        /// </remarks>
        /// <exception cref="InvalidOperationException">Attempting to modify the configuration, while parsing.</exception>
        public char? EscapeCharacter
        {
            get => this._EscapeCharacter;
            set {
                if ( this.ParserState == ParserState.Parsing )
                {
                    throw new InvalidOperationException( "Parsing has already begun, close the existing parse first." );
                }

                this._EscapeCharacter = value;
            }
        }
        /// <summary>
        ///   Gets or sets the character that is used to mark the beginning of a row that contains
        ///   purely comments and that should not be parsed.
        /// </summary>
        /// <value>
        ///   The character used to indicate the current row is to be ignored as a comment.
        /// </value>
        /// <remarks>
        ///   <para>
        ///     Default: '#'
        ///   </para>
        ///   <para>
        ///     If parsing has started, this value cannot be updated.
        ///   </para>
        /// </remarks>
        /// <exception cref="InvalidOperationException">Attempting to modify the configuration, while parsing.</exception>
        public char? CommentCharacter
        {
            get => this._CommentCharacter;
            set {
                if ( this.ParserState == ParserState.Parsing )
                {
                    throw new InvalidOperationException( "Parsing has already begun, close the existing parse first." );
                }

                this._CommentCharacter = value;
            }
        }
        /// <summary>
        ///   Gets the data found in the current row of data by the column index.
        /// </summary>
        /// <value>The value of the column at the given index.</value>
        /// <param name="intColumnIndex">The index of the column to retreive.</param>
        /// <remarks>
        ///   If the column is outside the bounds of the columns found or the column
        ///   does not possess a name, it will return <see langword="null"/>.
        /// </remarks>
        public string this[int intColumnIndex] => (intColumnIndex > -1) && (intColumnIndex < this.DataList.Count) ? this.DataList[intColumnIndex] : null;

        /// <summary>
        ///   Gets the data found in the current row of data by the column name.
        /// </summary>
        /// <value>The value of the column with the given column name.</value>
        /// <param name="strColumnName">The name of the column to retreive.</param>
        /// <remarks>
        ///   If the header has yet to be parsed (or no header exists), the property will
        ///   return <see langword="null"/>.
        /// </remarks>
        public string this[string strColumnName] => this[this.GetColumnIndexThis( strColumnName )];
        /// <summary>
        ///   Gets the number of columns found in the current row.
        /// </summary>
        /// <value>The number of data columns found in the current row.</value>
        /// <remarks>The <see cref="ColumnCount"/> property is read-only.  The number of columns per row can differ, if allowed.</remarks>
        public int ColumnCount => this.DataList.Count;
        /// <summary>
        ///   Gets the largest column count found thusfar from parsing.
        /// </summary>
        /// <value>The largest column count found thusfar from parsing.</value>
        /// <remarks>The <see cref="LargestColumnCount"/> property is read-only. The LargestColumnCount can increase due to rows with additional data.</remarks>
        public int LargestColumnCount => this.ColumnNames.Count;

        /// <summary>
        ///   Sets the file as the datasource.
        /// </summary>
        /// <remarks>
        ///   If the parser is currently parsing a file, all data associated
        ///   with the previous file is lost and the parser is reset back to
        ///   its initial values.
        /// </remarks>
        /// <param name="strFileName">The <see cref="string"/> containing the name of the file
        /// to set as the data source.</param>
        /// <example>
        ///   <code lang="C#" escaped="true">
        ///     using (GenericParser p = new GenericParser())
        ///       p.SetDataSource(@"C:\MyData.txt");
        ///   </code>
        /// </example>
        /// <exception cref="ArgumentNullException">Supplying <see langword="null"/>.</exception>
        /// <exception cref="ArgumentException">Supplying a filename to a file that does not exist.</exception>
        /// <exception cref="InvalidOperationException">Attempting to modify the configuration, while parsing.</exception>
        public void SetDataSource( string strFileName )
        {
            this.SetDataSource( strFileName, Encoding.UTF8 );
        }
        /// <summary>
        ///   Sets the file as the datasource using the provided encoding.
        /// </summary>
        /// <remarks>
        ///   If the parser is currently parsing a file, all data associated
        ///   with the previous file is lost and the parser is reset back to
        ///   its initial values.
        /// </remarks>
        /// <param name="strFileName">The <see cref="string"/> containing the name of the file
        /// to set as the data source.</param>
        /// <param name="encoding">The <see cref="Encoding"/> of the file being referenced.</param>
        /// <example>
        ///   <code lang="C#" escaped="true">
        ///     using (GenericParser p = new GenericParser())
        ///       p.SetDataSource(@"C:\MyData.txt", Encoding.ASCII);
        ///   </code>
        /// </example>
        /// <exception cref="ArgumentNullException">Supplying <see langword="null"/>.</exception>
        /// <exception cref="ArgumentException">Supplying a filename to a file that does not exist.</exception>
        /// <exception cref="InvalidOperationException">Attempting to modify the configuration, while parsing.</exception>
        public void SetDataSource( string strFileName, Encoding encoding )
        {
            if ( this.ParserState == ParserState.Parsing )
            {
                throw new InvalidOperationException( "Parsing has already begun, close the existing parse first." );
            }

            if ( strFileName == null )
            {
                throw new ArgumentNullException( nameof( strFileName ), "The filename cannot be a null value." );
            }

            if ( !File.Exists( strFileName ) )
            {
                throw new ArgumentException( string.Format( "File, {0}, does not exist.", strFileName ), nameof( strFileName ) );
            }

            if ( encoding == null )
            {
                throw new ArgumentNullException( nameof( encoding ), "The encoding cannot be a null value." );
            }

            // Clean up the existing text reader if it exists.
            if ( this._TextReader != null )
            {
                this._TextReader.Dispose();
            }

            this.ParserState = ParserState.Ready;
            this._TextReader = new StreamReader( strFileName, encoding, true );
        }
        /// <summary>
        ///   Sets the <see cref="TextReader"/> as the datasource.
        /// </summary>
        /// <param name="txtReader">The <see cref="TextReader"/> that contains the data to be parsed.</param>
        /// <remarks>
        ///   If the parser is currently parsing a file, all data associated with the
        ///   previous file is lost and the parser is reset back to its initial values.
        /// </remarks>
        /// <example>
        ///   <code lang="C#" escaped="true">
        ///     using (GenericParser p = new GenericParser())
        ///       using (StreamReader srReader = new StreamReader(@"C:\MyData.txt"))
        ///         p.SetDataSource(srReader);
        ///   </code>
        /// </example>
        /// <exception cref="ArgumentNullException">Supplying <see langword="null"/>.</exception>
        /// <exception cref="InvalidOperationException">Attempting to modify the configuration, while parsing.</exception>
        public void SetDataSource( TextReader txtReader )
        {
            if ( this.ParserState == ParserState.Parsing )
            {
                throw new InvalidOperationException( "Parsing has already begun, close the existing parse first." );
            }

            // Clean up the existing text reader if it exists.
            if ( this._TextReader != null )
            {
                this._TextReader.Dispose();
            }

            this.ParserState = ParserState.Ready;
            this._TextReader = txtReader ?? throw new ArgumentNullException( nameof( txtReader ), "The text reader cannot be a null value." );
        }

        /// <summary>
        ///   <para>
        ///     Parses the data-source till it arrives at one row of data.
        ///   </para>
        /// </summary>
        /// <returns>
        ///   <para>
        ///     <see langword="true"/> - Successfully parsed a new data row.
        ///   </para>
        ///   <para>
        ///     <see langword="false"/> - No new data rows were found.
        ///   </para>
        /// </returns>
        /// <remarks>
        ///   <para>
        ///     If it finds a header, and its expecting a header row, it will not stop
        ///     at the row and continue on till it has found a row of data.
        ///   </para>
        ///   <para>
        ///     Internally, the header row is treated as a data row, but will not cause
        ///     the parser to stop after finding it.
        ///   </para>
        /// </remarks>
        /// <exception cref="InvalidOperationException">
        ///   Attempting to read without properly setting up the <see cref="GenericParser"/>.
        /// </exception>
        /// <exception cref="ParsingException">
        ///   Thrown in the situations where the <see cref="GenericParser"/> cannot continue
        ///   due to a conflict between the setup and the data being parsed.
        /// </exception>
        /// <example>
        ///   <code lang="C#" escaped="true">
        ///     using (GenericParser p = new GenericParser(@"C:\MyData.txt"))
        ///     {
        ///       while(p.Read())
        ///       {
        ///         // Put code here to retrieve results of the read.
        ///       }
        ///     }
        ///   </code>
        /// </example>
        public bool Read()
        {
            // Setup some internal variables for the parsing.
            this.InitializeParseThis();

            // Do we need to stop parsing rows.
            if ( this.ParserState == ParserState.Finished )
            {
                return false;
            }

            // Read character by character into the buffer, until we reach the end of the data source.
            while ( this.GetNextCharacterThis() )
            {
                // If the row type is unknown, we're at the beginning of the row and need to determine its type.
                if ( this._RowType == RowType.Unknown )
                {
                    this.ParseRowTypeThis();

                    // If we finished due to reading comments, break out.
                    if ( this.ParserState == ParserState.Finished )
                    {
                        return false;
                    }
                }

                if ( this._TextFieldType == FieldType.Delimited )
                {
                    if ( this._CurrentChar == this._EscapeCharacter )
                    {
                        this._ContainsEscapedCharacters = true;

                        if ( this.GetNextCharacterThis() )
                        {
                            continue;
                        }
                        else
                        {
                            // We ran out of data, so break out.
                            break;
                        }
                    }
                    else if ( ((this._ReadIndex - 1) == this._StartOfCurrentColumnIndex) && (this._CurrentChar == this._TextQualifier) )
                    {
                        this.SkipToEndOfTextThis();
                        continue;
                    }
                }

                // See if we have reached the end of a line.
                if ( ((this._CurrentChar == '\r') && (this._ColumnDelimiter != '\r')) || (this._CurrentChar == '\n') )
                {
                    // Make sure we update the state and extract columns as necessary.
                    this.HandleEndOfRowThis( this._ReadIndex - 2 );

                    // Read the next character, if it is a newline, keep the state as is. Otherwise, roll back the index.
                    if ( this.GetNextCharacterThis() && (((this._CurrentChar != '\r') || (this._ColumnDelimiter == '\r')) && (this._CurrentChar != '\n')) )
                    {
                        --this._ReadIndex;
                    }

                    // If we were in a data row, we need to stop.
                    if ( (this._RowType == RowType.DataRow) && ((this.DataList.Count > 0) || !this._SkipEmptyRows) )
                    {
                        return true;
                    }
                    else
                    {
                        this._RowType = RowType.Unknown;
                        continue;
                    }
                }

                if ( ((this._TextFieldType == FieldType.Delimited)
                  && (this._CurrentChar == this._ColumnDelimiter))
                 || ((this._TextFieldType == FieldType.FixedWidth)
                  && (this.DataList.Count < this._IaColumnWidths.Length)
                  && ((this._ReadIndex - this._StartOfCurrentColumnIndex) >= this._IaColumnWidths[this.DataList.Count])) )
                {
                    // Move back one character to get the last character in the column
                    // (ended with column delimiter).
                    if ( (this._RowType == RowType.DataRow) || (this._RowType == RowType.HeaderRow) )
                    {
                        if ( this._TextFieldType == FieldType.Delimited )
                        {
                            this.ExtractColumnThis( this._ReadIndex - 2 );
                        }
                        else
                        {
                            this.ExtractColumnThis( this._ReadIndex - 1 );
                        }
                    }

                    // Update the column specific flags.
                    this.IsCurrentRowEmpty = false;
                    this._FoundTextQualifierAtStart = false;
                    this._ContainsEscapedCharacters = false;
                    this._StartOfCurrentColumnIndex = this._ReadIndex;
                    continue;
                }
            }

            // We ran out of data, flush out the last row and return.
            this.HandleEndOfRowThis( this._ReadIndex - 1 );

            return ((this.DataList.Count > 0) || !this._SkipEmptyRows || (this._HeaderRowFound && (this._RowType == RowType.HeaderRow)));
        }

        /// <summary>
        /// Loads the configuration of the <see cref="GenericParser"/> object from an <see cref="XmlReader"/>.
        /// </summary>
        /// <param name="xrConfigXmlFile">The <see cref="XmlReader"/> containing the XmlConfig file to load configuration from.</param>
        /// <exception cref="ArgumentException">In the event that the XmlConfig file contains a value that is invalid,
        /// an <see cref="ArgumentException"/> could be thrown.</exception>
        /// <exception cref="ArgumentNullException">In the event that the XmlConfig file contains a value that is invalid,
        /// an <see cref="ArgumentNullException"/> could be thrown.</exception>
        /// <exception cref="ArgumentOutOfRangeException">In the event that the XmlConfig file contains a value that is invalid,
        /// an <see cref="ArgumentOutOfRangeException"/> could be thrown.</exception>
        /// <exception cref="InvalidOperationException">Attempting to modify the configuration, while parsing.</exception>
        /// <example>
        ///   <code lang="C#" escaped="true">
        ///     using (FileStream fs = new FileStream(@"C:\MyData.txt", FileMode.Open))
        ///       using (XmlTextReader xmlTextReader = new XmlTextReader(fs))
        ///         using (GenericParser p = new GenericParser())
        ///           p.Load(xmlTextReader);
        ///   </code>
        /// </example>
        public void Load( XmlReader xrConfigXmlFile )
        {
            if ( this.ParserState == ParserState.Parsing )
            {
                throw new InvalidOperationException( "Parsing has already begun, close the existing parse first." );
            }

            XmlDocument xmlConfig = new XmlDocument();

            xmlConfig.Load( xrConfigXmlFile );

            this.Load( xmlConfig );
        }
        /// <summary>
        /// Loads the configuration of the <see cref="GenericParser"/> object from an <see cref="TextReader"/>.
        /// </summary>
        /// <param name="trConfigXmlFile">The <see cref="TextReader"/> containing the XmlConfig file to load configuration from.</param>
        /// <exception cref="ArgumentException">In the event that the XmlConfig file contains a value that is invalid,
        /// an <see cref="ArgumentException"/> could be thrown.</exception>
        /// <exception cref="ArgumentNullException">In the event that the XmlConfig file contains a value that is invalid,
        /// an <see cref="ArgumentNullException"/> could be thrown.</exception>
        /// <exception cref="ArgumentOutOfRangeException">In the event that the XmlConfig file contains a value that is invalid,
        /// an <see cref="ArgumentOutOfRangeException"/> could be thrown.</exception>
        /// <exception cref="InvalidOperationException">Attempting to modify the configuration, while parsing.</exception>
        /// <example>
        ///   <code lang="C#" escaped="true">
        ///     using (StreamReader sr = new StreamReader(@"C:\MyData.txt"))
        ///       using (GenericParser p = new GenericParser())
        ///         p.Load(sr);
        ///   </code>
        /// </example>
        public void Load( TextReader trConfigXmlFile )
        {
            if ( this.ParserState == ParserState.Parsing )
            {
                throw new InvalidOperationException( "Parsing has already begun, close the existing parse first." );
            }

            XmlDocument xmlConfig = new XmlDocument();

            xmlConfig.Load( trConfigXmlFile );

            this.Load( xmlConfig );
        }
        /// <summary>
        /// Loads the configuration of the <see cref="GenericParser"/> object from an <see cref="Stream"/>.
        /// </summary>
        /// <param name="sConfigXmlFile">The <see cref="Stream"/> containing the XmlConfig file to load configuration from.</param>
        /// <exception cref="ArgumentException">In the event that the XmlConfig file contains a value that is invalid,
        /// an <see cref="ArgumentException"/> could be thrown.</exception>
        /// <exception cref="ArgumentNullException">In the event that the XmlConfig file contains a value that is invalid,
        /// an <see cref="ArgumentNullException"/> could be thrown.</exception>
        /// <exception cref="ArgumentOutOfRangeException">In the event that the XmlConfig file contains a value that is invalid,
        /// an <see cref="ArgumentOutOfRangeException"/> could be thrown.</exception>
        /// <exception cref="InvalidOperationException">Attempting to modify the configuration, while parsing.</exception>
        /// <example>
        ///   <code lang="C#" escaped="true">
        ///     using (FileStream fs = new FileStream(@"C:\MyData.txt", FileMode.Open))
        ///       using (GenericParser p = new GenericParser())
        ///         p.Load(fs);
        ///   </code>
        /// </example>
        public void Load( Stream sConfigXmlFile )
        {
            if ( this.ParserState == ParserState.Parsing )
            {
                throw new InvalidOperationException( "Parsing has already begun, close the existing parse first." );
            }

            XmlDocument xmlConfig = new XmlDocument();

            xmlConfig.Load( sConfigXmlFile );

            this.Load( xmlConfig );
        }
        /// <summary>
        /// Loads the configuration of the <see cref="GenericParser"/> object from a file on the file system.
        /// </summary>
        /// <param name="strConfigXmlFile">The full path to the XmlConfig file on the file system.</param>
        /// <exception cref="ArgumentException">In the event that the XmlConfig file contains a value that is invalid,
        /// an <see cref="ArgumentException"/> could be thrown.</exception>
        /// <exception cref="ArgumentNullException">In the event that the XmlConfig file contains a value that is invalid,
        /// an <see cref="ArgumentNullException"/> could be thrown.</exception>
        /// <exception cref="ArgumentOutOfRangeException">In the event that the XmlConfig file contains a value that is invalid,
        /// an <see cref="ArgumentOutOfRangeException"/> could be thrown.</exception>
        /// <exception cref="InvalidOperationException">Attempting to modify the configuration, while parsing.</exception>
        /// <example>
        ///   <code lang="C#" escaped="true">
        ///     using (GenericParser p = new GenericParser())
        ///       p.Load(@"C:\MyData.txt");
        ///   </code>
        /// </example>
        public void Load( string strConfigXmlFile )
        {
            if ( this.ParserState == ParserState.Parsing )
            {
                throw new InvalidOperationException( "Parsing has already begun, close the existing parse first." );
            }

            XmlDocument xmlConfig = new XmlDocument();

            xmlConfig.Load( strConfigXmlFile );

            this.Load( xmlConfig );
        }
        /// <summary>
        /// Loads the configuration of the <see cref="GenericParser"/> object from an <see cref="XmlDocument"/>.
        /// </summary>
        /// <param name="xmlConfig">The <see cref="XmlDocument"/> object containing the configuration information.</param>
        /// <exception cref="ArgumentException">In the event that the XmlConfig file contains a value that is invalid,
        /// an <see cref="ArgumentException"/> could be thrown.</exception>
        /// <exception cref="ArgumentNullException">In the event that the XmlConfig file contains a value that is invalid,
        /// an <see cref="ArgumentNullException"/> could be thrown.</exception>
        /// <exception cref="ArgumentOutOfRangeException">In the event that the XmlConfig file contains a value that is invalid,
        /// an <see cref="ArgumentOutOfRangeException"/> could be thrown.</exception>
        /// <exception cref="InvalidOperationException">Attempting to modify the configuration, while parsing.</exception>
        /// <example>
        ///   <code lang="C#" escaped="true">
        ///     XmlDocument xmlConfig = new XmlDocument();
        ///     xmlConfig.Load(strConfigXmlFile);
        ///
        ///     using (GenericParser p = new GenericParser())
        ///       p.Load(xmlConfig);
        ///   </code>
        /// </example>
        public virtual void Load( XmlDocument xmlConfig )
        {
            XmlElement xmlElement;

            if ( this.ParserState == ParserState.Parsing )
            {
                throw new InvalidOperationException( "Parsing has already begun, close the existing parse first." );
            }

            // Reset all of the configuration variables.
            this.InitializeConfigurationVariablesThis();

            ////////////////////////////////////////////////////////////////////
            // Access each element and load the contents of the configuration //
            // into the current GenericParser object.                         //
            ////////////////////////////////////////////////////////////////////

            xmlElement = xmlConfig.DocumentElement[XML_COLUMN_WIDTHS];

            if ( (xmlElement != null) && (xmlElement.ChildNodes.Count > 0) )
            {
                List<int> lstColumnWidths = new List<int>( xmlElement.ChildNodes.Count );

                foreach ( XmlElement xmlColumnWidth in xmlElement.ChildNodes )
                {
                    if ( xmlColumnWidth.Name == XML_COLUMN_WIDTH )
                    {
                        lstColumnWidths.Add( Convert.ToInt32( xmlColumnWidth.InnerText ) );
                    }
                }

                if ( lstColumnWidths.Count > 0 )
                {
                    this.ColumnWidths = lstColumnWidths.ToArray();
                }
            }

            /////////////////////////////////////////////////////////////

            xmlElement = xmlConfig.DocumentElement[XML_MAX_BUFFER_SIZE];

            if ( (xmlElement != null) && (xmlElement.InnerText != null) )
            {
                this.MaxBufferSize = Convert.ToInt32( xmlElement.InnerText );
            }

            /////////////////////////////////////////////////////////////

            xmlElement = xmlConfig.DocumentElement[XML_MAX_ROWS];

            if ( (xmlElement != null) && (xmlElement.InnerText != null) )
            {
                this.MaxRows = Convert.ToInt32( xmlElement.InnerText );
            }

            /////////////////////////////////////////////////////////////

            xmlElement = xmlConfig.DocumentElement[XML_SKIP_STARTING_DATA_ROWS];

            if ( (xmlElement != null) && (xmlElement.InnerText != null) )
            {
                this.SkipStartingDataRows = Convert.ToInt32( xmlElement.InnerText );
            }

            /////////////////////////////////////////////////////////////

            xmlElement = xmlConfig.DocumentElement[XML_EXPECTED_COLUMN_COUNT];

            if ( (xmlElement != null) && (xmlElement.InnerText != null) )
            {
                this.ExpectedColumnCount = Convert.ToInt32( xmlElement.InnerText );
            }

            /////////////////////////////////////////////////////////////

            xmlElement = xmlConfig.DocumentElement[XML_FIRST_ROW_HAS_HEADER];

            if ( (xmlElement != null) && (xmlElement.InnerText != null) )
            {
                this.FirstRowHasHeader = Convert.ToBoolean( xmlElement.InnerText );
            }

            /////////////////////////////////////////////////////////////

            xmlElement = xmlConfig.DocumentElement[XML_TRIM_RESULTS];

            if ( (xmlElement != null) && (xmlElement.InnerText != null) )
            {
                this.TrimResults = Convert.ToBoolean( xmlElement.InnerText );
            }

            /////////////////////////////////////////////////////////////

            xmlElement = xmlConfig.DocumentElement[XML_STRIP_CONTROL_CHARS];

            if ( (xmlElement != null) && (xmlElement.InnerText != null) )
            {
                this.StripControlChars = Convert.ToBoolean( xmlElement.InnerText );
            }

            /////////////////////////////////////////////////////////////

            xmlElement = xmlConfig.DocumentElement[XML_SKIP_EMPTY_ROWS];

            if ( (xmlElement != null) && (xmlElement.InnerText != null) )
            {
                this.SkipEmptyRows = Convert.ToBoolean( xmlElement.InnerText );
            }

            /////////////////////////////////////////////////////////////

            xmlElement = xmlConfig.DocumentElement[XML_TEXT_FIELD_TYPE];

            if ( (xmlElement != null) && (xmlElement.InnerText != null) && Enum.IsDefined( typeof( FieldType ), xmlElement.InnerText ) )
            {
                this.TextFieldType = ( FieldType ) Enum.Parse( typeof( FieldType ), xmlElement.InnerText );
            }

            /////////////////////////////////////////////////////////////

            xmlElement = xmlConfig.DocumentElement[XML_FIRST_ROW_SETS_EXPECTED_COLUMN_COUNT];

            if ( (xmlElement != null) && (xmlElement.InnerText != null) )
            {
                this.FirstRowSetsExpectedColumnCount = Convert.ToBoolean( xmlElement.InnerText );
            }

            /////////////////////////////////////////////////////////////

            xmlElement = xmlConfig.DocumentElement[XML_COLUMN_DELIMITER];

            this.ColumnDelimiter = (xmlElement != null) && (xmlElement.InnerText != null) && (xmlElement.InnerText.Length > 0)
                ? Convert.ToChar( Convert.ToInt32( xmlElement.InnerText ) )
                : ( char? ) null;

            /////////////////////////////////////////////////////////////

            xmlElement = xmlConfig.DocumentElement[XML_TEXT_QUALIFIER];

            this.TextQualifier = (xmlElement != null) && (xmlElement.InnerText != null) && (xmlElement.InnerText.Length > 0)
                ? Convert.ToChar( Convert.ToInt32( xmlElement.InnerText ) )
                : ( char? ) null;

            /////////////////////////////////////////////////////////////

            xmlElement = xmlConfig.DocumentElement[XML_ESCAPE_CHARACTER];

            this.EscapeCharacter = (xmlElement != null) && (xmlElement.InnerText != null) && (xmlElement.InnerText.Length > 0)
                ? Convert.ToChar( Convert.ToInt32( xmlElement.InnerText ) )
                : ( char? ) null;

            /////////////////////////////////////////////////////////////

            xmlElement = xmlConfig.DocumentElement[XML_COMMENT_CHARACTER];

            this.CommentCharacter = (xmlElement != null) && (xmlElement.InnerText != null) && (xmlElement.InnerText.Length > 0)
                ? Convert.ToChar( Convert.ToInt32( xmlElement.InnerText ) )
                : ( char? ) null;
        }

        /// <summary>
        ///   Saves the configuration to a <see cref="XmlWriter"/>.
        /// </summary>
        /// <param name="xwXmlConfig">The XmlWriter to save the the <see cref="XmlDocument"/> to.</param>
        /// <example>
        ///   <code lang="C#" escaped="true">
        ///     using (XmlTextWriter xwXmlConfig = new XmlTextWriter(@"C:\MyData.txt", Encoding.Default))
        ///       using (GenericParser p = new GenericParser())
        ///         p.Save(xwXmlConfig);
        ///   </code>
        /// </example>
        public void Save( XmlWriter xwXmlConfig )
        {
            XmlDocument xmlConfig = this.Save();

            xmlConfig.Save( xwXmlConfig );
        }
        /// <summary>
        ///   Saves the configuration to a <see cref="TextWriter"/>.
        /// </summary>
        /// <param name="twXmlConfig">The TextWriter to save the <see cref="XmlDocument"/> to.</param>
        /// <example>
        ///   <code lang="C#" escaped="true">
        ///     using (StringWriter sw = new StringWriter())
        ///       using (GenericParser p = new GenericParser())
        ///         p.Save(sw);
        ///   </code>
        /// </example>
        public void Save( TextWriter twXmlConfig )
        {
            XmlDocument xmlConfig = this.Save();

            xmlConfig.Save( twXmlConfig );
        }
        /// <summary>
        ///   Saves the configuration to a <see cref="Stream"/>.
        /// </summary>
        /// <param name="sXmlConfig">The stream to save the <see cref="XmlDocument"/> to.</param>
        /// <example>
        ///   <code lang="C#" escaped="true">
        ///     using (FileStream fs = new FileStream(@"C:\MyData.txt", FileMode.Create))
        ///       using (GenericParser p = new GenericParser())
        ///         p.Save(fs);
        ///   </code>
        /// </example>
        public void Save( Stream sXmlConfig )
        {
            XmlDocument xmlConfig = this.Save();

            xmlConfig.Save( sXmlConfig );
        }
        /// <summary>
        ///   Saves the configuration to the file system.
        /// </summary>
        /// <param name="strConfigXmlFile">The file name to save the <see cref="XmlDocument"/> to.</param>
        /// <example>
        ///   <code lang="C#" escaped="true">
        ///     using (GenericParser p = new GenericParser())
        ///       p.Load(@"C:\MyData.txt");
        ///   </code>
        /// </example>
        public void Save( string strConfigXmlFile )
        {
            XmlDocument xmlConfig = this.Save();

            xmlConfig.Save( strConfigXmlFile );
        }
        /// <summary>
        ///   Saves the configuration to an <see cref="XmlDocument"/>.
        /// </summary>
        /// <returns>The <see cref="XmlDocument"/> containing the configuration information.</returns>
        /// <example>
        ///   <code lang="C#" escaped="true">
        ///     using (GenericParser p = new GenericParser())
        ///       XmlDocument xmlConfig = p.Save();
        ///   </code>
        /// </example>
        public virtual XmlDocument Save()
        {
            XmlDocument xmlConfig = new XmlDocument();
            XmlDeclaration xmlDeclaration;
            XmlElement xmlRoot, xmlElement, xmlSubElement;

            // Create the XML declaration
            xmlDeclaration = xmlConfig.CreateXmlDeclaration( "1.0", "utf-8", null );

            // Create the root element
            xmlRoot = xmlConfig.CreateElement( XML_ROOT_NODE );
            _ = xmlConfig.InsertBefore( xmlDeclaration, xmlConfig.DocumentElement );
            _ = xmlConfig.AppendChild( xmlRoot );

            ////////////////////////////////////////////////////////////////////
            // Save each of the pertinent configurable settings of the        //
            // GenericParser object into the XmlDocument.                     //
            ////////////////////////////////////////////////////////////////////

            if ( (this._TextFieldType == FieldType.FixedWidth) && (this._IaColumnWidths != null) )
            {
                xmlElement = xmlConfig.CreateElement( XML_COLUMN_WIDTHS );
                _ = xmlRoot.AppendChild( xmlElement );

                // Create the column width elements underneath the column widths node.
                foreach ( int intColumnWidth in this._IaColumnWidths )
                {
                    xmlSubElement = xmlConfig.CreateElement( XML_COLUMN_WIDTH );
                    xmlSubElement.InnerText = intColumnWidth.ToString();
                    _ = xmlElement.AppendChild( xmlSubElement );
                }
            }

            /////////////////////////////////////////////////////////////

            xmlElement = xmlConfig.CreateElement( XML_MAX_BUFFER_SIZE );
            xmlElement.InnerText = this._MaxBufferSize.ToString();
            _ = xmlRoot.AppendChild( xmlElement );

            /////////////////////////////////////////////////////////////

            xmlElement = xmlConfig.CreateElement( XML_MAX_ROWS );
            xmlElement.InnerText = this._MaxRows.ToString();
            _ = xmlRoot.AppendChild( xmlElement );

            /////////////////////////////////////////////////////////////

            xmlElement = xmlConfig.CreateElement( XML_SKIP_STARTING_DATA_ROWS );
            xmlElement.InnerText = this._SkipStartingDataRows.ToString();
            _ = xmlRoot.AppendChild( xmlElement );

            /////////////////////////////////////////////////////////////

            xmlElement = xmlConfig.CreateElement( XML_EXPECTED_COLUMN_COUNT );
            xmlElement.InnerText = this._ExpectedColumnCount.ToString();
            _ = xmlRoot.AppendChild( xmlElement );

            /////////////////////////////////////////////////////////////

            xmlElement = xmlConfig.CreateElement( XML_FIRST_ROW_HAS_HEADER );
            xmlElement.InnerText = this._FirstRowHasHeader.ToString();
            _ = xmlRoot.AppendChild( xmlElement );

            /////////////////////////////////////////////////////////////

            xmlElement = xmlConfig.CreateElement( XML_TRIM_RESULTS );
            xmlElement.InnerText = this._TrimResults.ToString();
            _ = xmlRoot.AppendChild( xmlElement );

            /////////////////////////////////////////////////////////////

            xmlElement = xmlConfig.CreateElement( XML_STRIP_CONTROL_CHARS );
            xmlElement.InnerText = this._StripControlChars.ToString();
            _ = xmlRoot.AppendChild( xmlElement );

            /////////////////////////////////////////////////////////////

            xmlElement = xmlConfig.CreateElement( XML_SKIP_EMPTY_ROWS );
            xmlElement.InnerText = this._SkipEmptyRows.ToString();
            _ = xmlRoot.AppendChild( xmlElement );

            /////////////////////////////////////////////////////////////

            xmlElement = xmlConfig.CreateElement( XML_TEXT_FIELD_TYPE );
            xmlElement.InnerText = this._TextFieldType.ToString();
            _ = xmlRoot.AppendChild( xmlElement );

            /////////////////////////////////////////////////////////////

            xmlElement = xmlConfig.CreateElement( XML_FIRST_ROW_SETS_EXPECTED_COLUMN_COUNT );
            xmlElement.InnerText = this._FirstRowSetsExpectedColumnCount.ToString();
            _ = xmlRoot.AppendChild( xmlElement );

            /////////////////////////////////////////////////////////////

            if ( this._TextFieldType == FieldType.Delimited )
            {
                xmlElement = xmlConfig.CreateElement( XML_COLUMN_DELIMITER );
                xmlElement.InnerText = Convert.ToInt32( this._ColumnDelimiter ).ToString();
                _ = xmlRoot.AppendChild( xmlElement );
            }

            /////////////////////////////////////////////////////////////

            if ( this._TextQualifier.HasValue )
            {
                xmlElement = xmlConfig.CreateElement( XML_TEXT_QUALIFIER );
                xmlElement.InnerText = Convert.ToInt32( this._TextQualifier ).ToString();
                _ = xmlRoot.AppendChild( xmlElement );
            }

            /////////////////////////////////////////////////////////////

            if ( this._EscapeCharacter.HasValue )
            {
                xmlElement = xmlConfig.CreateElement( XML_ESCAPE_CHARACTER );
                xmlElement.InnerText = Convert.ToInt32( this._EscapeCharacter ).ToString();
                _ = xmlRoot.AppendChild( xmlElement );
            }

            /////////////////////////////////////////////////////////////

            if ( this._CommentCharacter.HasValue )
            {
                xmlElement = xmlConfig.CreateElement( XML_COMMENT_CHARACTER );
                xmlElement.InnerText = Convert.ToInt32( this._CommentCharacter ).ToString();
                _ = xmlRoot.AppendChild( xmlElement );
            }

            return xmlConfig;
        }

        /// <summary>
        ///   Releases the underlying resources of the <see cref="GenericParser"/>.
        /// </summary>
        /// <example>
        ///   <code lang="C#" escaped="true">
        ///     using (GenericParser p = new GenericParser())
        ///     {
        ///       p.SetDataSource(@"C:\MyData.txt");
        ///     
        ///       while(p.Read())
        ///       {
        ///         // Put code here to retrieve results of the read.
        ///       }
        ///     }
        ///   </code>
        /// </example>
        public void Close()
        {
            this.CleanUpParserThis( false );
        }
        /// <summary>
        ///   Returns the index of the column based on its name.
        /// </summary>
        /// <remarks>
        ///   <para>
        ///     <see langword="null"/> column name is not a valid name for a column.
        ///   </para>
        ///   <para>
        ///     If the column is not found, the column index will be -1.
        ///   </para>
        /// </remarks>
        /// <param name="strColumnName">The name of the column to get the index for.</param>
        /// <returns>The index of the column with the name strColumnName. If none exists, -1 will be returned.</returns>
        /// <example>
        ///   <code lang="C#" escaped="true">
        ///     int intID, intPrice;
        ///     bool blnGotIndices = false;
        ///     
        ///     using (GenericParser p = new GenericParser())
        ///     {
        ///       p.SetDataSource(@"C:\MyData.txt");
        ///       p.FirstRowHasHeader = true;
        ///     
        ///       while(p.Read())
        ///       {
        ///         if (!blnGotIndices)
        ///         {
        ///           blnGotIndices = true;
        ///           intID = p.GetColumnIndex("ID");
        ///           intPrice = p.GetColumnIndex("Price");
        ///         }
        ///       
        ///         // Put code here to retrieve results of the read.
        ///       }
        ///     }
        ///   </code>
        /// </example>
        public int GetColumnIndex( string strColumnName )
        {
            return this.GetColumnIndexThis( strColumnName );
        }
        /// <summary>
        ///   Returns the name of the column based on its index.
        /// </summary>
        /// <param name="intColumnIndex">The column index to return the name for.</param>
        /// <remarks>
        ///   If the column is not found or the index is outside the range
        ///   of possible columns, <see langword="null"/> will be returned.
        /// </remarks>
        /// <returns>The name of the column at the given index, if none exists <see langword="null"/> is returned.</returns>
        /// <example>
        ///   <code lang="C#" escaped="true">
        ///     string strColumn1, strColumn2;
        ///     bool blnGotColumnNames = false;
        ///     
        ///     using (GenericParser p = new GenericParser())
        ///     {
        ///       p.SetDataSource(@"C:\MyData.txt");
        ///       p.FirstRowHasHeader = true;
        ///     
        ///       while(p.Read())
        ///       {
        ///         if (!blnGotColumnNames)
        ///         {
        ///           blnGotColumnNames = true;
        ///           strColumn1 = p.GetColumnIndex(0);
        ///           strColumn2 = p.GetColumnIndex(1);
        ///         }
        ///       
        ///         // Put code here to retrieve results of the read.
        ///       }
        ///     }
        ///   </code>
        /// </example>
        public string GetColumnName( int intColumnIndex )
        {
            return this.GetColumnNameThis( intColumnIndex );
        }

        /// <summary>
        ///   Releases all of the underlying resources used by this instance.
        /// </summary>
        /// <remarks>
        ///   Calls <see cref="Dispose(bool)"/> with blnDisposing set to <see langword="true"/>
        ///   to free unmanaged and managed resources.
        /// </remarks>
        public void Dispose()
        {
            this.Dispose( true );
            GC.SuppressFinalize( this );
        }

        /// <summary>
        /// Occurs when this instance is disposed of.
        /// </summary>
        public event EventHandler Disposed;

        #endregion Public Code

        #region Protected Code


        /// <summary>
        ///   The current <see cref="GenericParsing.ParserState"/> of the parser.
        /// </summary>
        protected ParserState ParserState { get; set; }

        /// <summary>
        ///   The current values of all the parsed columns within the row.
        /// </summary>
        protected List<string> DataList { get; set; }

        /// <summary>
        ///   The current values of all the parsed column headers within the row.
        /// </summary>
        protected List<string> ColumnNames { get; set; }

        /// <summary>
        /// Raises the <see cref="Disposed"/> Event.
        /// </summary>
        protected virtual void OnDisposed()
        {
            if (this.Disposed != null)
            {
                this.Disposed(this, EventArgs.Empty);
            }
        }
        /// <summary>
        ///   Releases the all unmanaged resources used by this instance and optionally releases the managed resources.
        /// </summary>
        /// <param name="blnDisposing">
        ///   <see langword="true"/> to release both managed and unmanaged resources; <see langword="false"/> to release only unmanaged resources.
        /// </param>
        protected virtual void Dispose(bool blnDisposing)
        {
            lock (this._ObjLock)
            {
                if (!this.IsDisposed)
                {
                    this.CleanUpParserThis(true);
                    this.IsDisposed = true;
                }
            }

            try
            {
                this.OnDisposed();
            }
#pragma warning disable CA1031 // Do not catch general exception types
            catch
            {
                /* Do nothing */
            }
#pragma warning restore CA1031 // Do not catch general exception types
        }

        #endregion Protected Code

        #region Private Code

        #region Configuration Data

        private FieldType _TextFieldType;
        private int[] _IaColumnWidths;
        private int _MaxBufferSize;
        private int _MaxRows;
        private int _SkipStartingDataRows;
        private int _ExpectedColumnCount;
        private bool _FirstRowHasHeader;
        private bool _TrimResults;
        private bool _StripControlChars;
        private bool _SkipEmptyRows;
        private bool _FirstRowSetsExpectedColumnCount;
        private char? _ColumnDelimiter;
        private char? _TextQualifier;
        private char? _EscapeCharacter;
        private char? _CommentCharacter;

        #endregion Configuration Data

        #region Parsing Variables

        private TextReader _TextReader;
        private bool _HeaderRowFound;
        private bool _FoundTextQualifierAtStart;
        private bool _ContainsEscapedCharacters;

        private int _StartIndexOfNewData;
        private int _NumberOfCharactersInBuffer;
        private int _ReadIndex;
        private int _StartOfCurrentColumnIndex;

        private char _CurrentChar;
        private char[] _Buffer;

        private RowType _RowType;

        #endregion Parsing Variables

        private readonly object _ObjLock;

        /// <summary>
        ///   Initializes internal variables that are maintained for internal tracking
        ///   of state during parsing.
        /// </summary>
        /// <exception cref="InvalidOperationException">
        ///   In the event that the <see cref="GenericParser"/> wasn't setup properly, this exception will be thrown.
        /// </exception>
        private void InitializeParseThis()
        {
            switch (this.ParserState)
            {
                /////////////////////////////////////////////////////////////////////////////////////////////////////

                case ParserState.NoDataSource:
                    throw new InvalidOperationException("No data source was supplied to parse.");

                /////////////////////////////////////////////////////////////////////////////////////////////////////

                case ParserState.Ready:

                    // Peform a quick sanity check to make sure we're setup properly.
                    if ((this._TextFieldType == FieldType.FixedWidth) && (this._IaColumnWidths == null))
                    {
                        throw new InvalidOperationException("Column widths were not set in order to parse fixed width data.");
                    }

                    if ((this._TextFieldType == FieldType.Delimited) && !this._ColumnDelimiter.HasValue)
                    {
                        throw new InvalidOperationException("Column delimiter was not set in order to parse delimited data.");
                    }

                    this.ParserState = ParserState.Parsing;
                    this._RowType = RowType.Unknown;

                    this._HeaderRowFound = false;
                    this._StartIndexOfNewData = 0;
                    this.DataRowNumber = 0;
                    this.FileRowNumber = 0;
                    this._ReadIndex = 0;
                    this._NumberOfCharactersInBuffer = 0;
                    this._StartOfCurrentColumnIndex = -1;

                    if (this.DataList == null)
                    {
                        this.DataList = new List<string>();
                    }
                    else
                    {
                        this.DataList.Clear();
                    }

                    if (this.ColumnNames == null)
                    {
                        this.ColumnNames = new List<string>();
                    }
                    else
                    {
                        this.ColumnNames.Clear();
                    }

                    // Only allocate the buffers if they are null or improperly sized.
                    if ((this._Buffer == null) || (this._Buffer.Length != this._MaxBufferSize))
                    {
                        this._Buffer = new char[this._MaxBufferSize];
                    }

                    break;

                /////////////////////////////////////////////////////////////////////////////////////////////////////

                case ParserState.Parsing:

                    this.DataList.Clear();

                    // Have we hit the max row count?
                    if ((this._MaxRows > 0) && ((this.DataRowNumber - this._SkipStartingDataRows) >= this._MaxRows))
                    {
                        // We're done, so clean up the text reader.
                        this._TextReader.Dispose();
                        this._TextReader = null;
                        this.ParserState = ParserState.Finished;
                    }
                    else
                    {
                        this._RowType = RowType.Unknown;
                    }

                    break;

                /////////////////////////////////////////////////////////////////////////////////////////////////////

                case ParserState.Finished:
                default:

                    // Nothing.
                    break;
            }
        }
        /// <summary>
        ///   Gets the next character from the input buffer (and refills it if necessary and possible).
        /// </summary>
        /// <returns>
        ///   <para>
        ///     <see langword="true"/> - A new character was read from the data source.
        ///   </para>
        ///   <para>
        ///     <see langword="false"/> - No more characters are available in the data source.
        ///   </para>
        /// </returns>
        private bool GetNextCharacterThis()
        {
            int intCharactersRead;

            // See if we have any more characters left in the input buffer.
            if (this._ReadIndex >= this._NumberOfCharactersInBuffer)
            {
                // Make sure we haven't finished.
                if (this.ParserState == ParserState.Finished)
                {
                    return false;
                }

                // Move the leftover data in the buffer to the front and start over (only if this isn't the initial load).
                if (this._StartOfCurrentColumnIndex > -1)
                {
                    this.CopyRemainingDataToFrontThis(this._StartOfCurrentColumnIndex);
                }

                // Read the next block of characters into the input buffer.
                intCharactersRead = this._TextReader.ReadBlock(this._Buffer, this._StartIndexOfNewData, (this._MaxBufferSize - this._StartIndexOfNewData));
                this._NumberOfCharactersInBuffer = intCharactersRead + this._StartIndexOfNewData;
                this._ReadIndex = this._StartIndexOfNewData;

                if (intCharactersRead < 1)
                {
                    // We're done, so clean up the text reader.
                    this._TextReader.Dispose();
                    this._TextReader = null;
                    this.ParserState = ParserState.Finished;

                    return false;
                }
            }

            this._CurrentChar = this._Buffer[this._ReadIndex++];
            return true;
        }
        /// <summary>
        ///   Reads till a non-comment row is found.
        /// </summary>
        private void SkipCommentRowsThis()
        {
            // We start at the comment character, so get the next and keep reading till we find a new line.
            while (this.GetNextCharacterThis())
            {
                // Check for the end of a row.
                if (((this._CurrentChar == '\r') && (this._ColumnDelimiter != '\r')) || (this._CurrentChar == '\n'))
                {
                    ++this.FileRowNumber;

                    // Read the next character and read another if its a row delimiter.
                    if (!this.GetNextCharacterThis()
                     || ((((this._CurrentChar == '\r') && (this._ColumnDelimiter != '\r')) || (this._CurrentChar == '\n')) && !this.GetNextCharacterThis())
                     || (this._CurrentChar != this._CommentCharacter))
                    {
                        // Ran out of data or the next character is not a comment row.
                        break;
                    }
                }
            }
        }
        /// <summary>
        ///   Reads till the end of the text is found.
        /// </summary>
        private void SkipToEndOfTextThis()
        {
            this._FoundTextQualifierAtStart = true;

            while (this.GetNextCharacterThis())
            {
                if (this._CurrentChar == this._EscapeCharacter)
                {
                    this._ContainsEscapedCharacters = true;

                    if (this.GetNextCharacterThis())
                    {
                        continue;
                    }
                    else
                    {
                        // We ran out of data, so break out.
                        break;
                    }
                }

                // If the next character is a text qualifier, make sure it isn't the case of "a""c".
                if (this._CurrentChar == this._TextQualifier)
                {
                    if (!this.GetNextCharacterThis())
                    {
                        // We ran out of data, so break out.
                        break;
                    }
                    else if (this._CurrentChar == this._TextQualifier)
                    {
                        // Skip the escaped text qualifier and continue looking for the end.
                        this._ContainsEscapedCharacters = true;
                        continue;
                    }
                    else
                    {
                        // Backup the index if its greater than zero and break out.
                        if (this._ReadIndex > 0)
                        {
                            --this._ReadIndex;
                        }

                        break;
                    }
                }
            }
        }
        /// <summary>
        ///   Removes all references to internally allocated resources.  Depending on
        ///   <paramref name="blnCompletely"/>, it will free up all of the internal resources
        ///   to prepare the instance for disposing.
        /// </summary>
        /// <param name="blnCompletely">
        ///   <para>
        ///     <see langword="true"/> - Clean-up the entire parser (used for disposing the instance).
        ///   </para>
        ///   <para>
        ///     <see langword="false"/> - Clean-up the parser to all it to be reused later.
        ///   </para>
        /// </param>
        private void CleanUpParserThis(bool blnCompletely)
        {
            this.ParserState = ParserState.Finished;

            if (this._TextReader != null)
            {
                this._TextReader.Dispose();
            }

            this._TextReader = null;
            this._Buffer = null;
            this.DataList = null;
            this.ColumnNames = null;

            if (blnCompletely)
            {
                this._IaColumnWidths = null;
                this._ColumnDelimiter = null;
            }
        }
        /// <summary>
        ///   Examines the beginning of the row and the current state information
        ///   to determine how the parser will interpret the next line and updates
        ///   the internal RowType accordingly.
        /// </summary>
        private void ParseRowTypeThis()
        {
            // Skip past any comment rows we find.
            if (this._CurrentChar == this._CommentCharacter)
            {
                this._RowType = RowType.CommentRow;
                this.SkipCommentRowsThis();

                // If we finished, we need to break out.
                if (this.ParserState == ParserState.Finished)
                {
                    return;
                }
            }

            this._StartOfCurrentColumnIndex = this._ReadIndex - 1;
            this._ContainsEscapedCharacters = false;
            this.IsCurrentRowEmpty = true;

            this._RowType = this._FirstRowHasHeader && !this._HeaderRowFound
                ? RowType.HeaderRow
                : this.DataRowNumber < this._SkipStartingDataRows ? RowType.SkippedRow : RowType.DataRow;
        }
        /// <summary>
        ///   Takes the data parsed from the row and places it into the ColumnNames collection.
        /// </summary>
        private void SetColumnNamesThis()
        {
            // Since the current data row was a header row, reset the flag to an empty row.
            this.IsCurrentRowEmpty = true;

            this._HeaderRowFound = true;
            this.ColumnNames.AddRange(this.DataList);
            this.DataList.Clear();
        }
        /// <summary>
        ///   Handles the logic necessary for updating state due to a row ending.
        /// </summary>
        /// <param name="intEndOfDataIndex">The index of the last character in the column.</param>
        /// <exception cref="ParsingException">
        ///   If parsing a fixed width format and the number of columns found differs
        ///   what was expected, this exception will be thrown.
        /// </exception>
        private void HandleEndOfRowThis(int intEndOfDataIndex)
        {
            bool blnIsColumnEmpty;

            blnIsColumnEmpty = (intEndOfDataIndex < this._StartOfCurrentColumnIndex);

            // Determine if we have an empty row or not.
            this.IsCurrentRowEmpty &= blnIsColumnEmpty;

            // Make sure we don't have an empty row by seeing if we have some data somewhere.
            if (!this.IsCurrentRowEmpty || !this._SkipEmptyRows)
            {
                if ((!blnIsColumnEmpty || (!this.IsCurrentRowEmpty && (this._TextFieldType == FieldType.Delimited)))
                 && ((this._RowType == RowType.DataRow) || (this._RowType == RowType.HeaderRow)))
                {
                    this.ExtractColumnThis(intEndOfDataIndex);
                }

                if ((this._RowType == RowType.DataRow) || (this._RowType == RowType.SkippedRow))
                {
                    ++this.DataRowNumber;
                }

                // Update the column specific flags.
                this._FoundTextQualifierAtStart = false;
                this._ContainsEscapedCharacters = false;
                this._StartOfCurrentColumnIndex = this._ReadIndex;
            }

            // Ensure that we have some data, before trying to do something with it.
            // This prevents problems with empty rows.
            if (this.DataList.Count > 0)
            {
                // Have we got a row that meets our expected number of columns.
                if ((this._ExpectedColumnCount > 0) && (this.DataList.Count != this._ExpectedColumnCount))
                {
                    throw this.CreateParsingExceptionThis(string.Format("Expected column count of {0} not found.", this._ExpectedColumnCount));
                }

                // If we have a valid row, update the expected column count if we have the flag set.
                // This only makes sense when using delimiters, as fixed width would have already set this value.
                if ((this._TextFieldType == FieldType.Delimited) && (this.DataList.Count > 0) && this._FirstRowSetsExpectedColumnCount)
                {
                    this._ExpectedColumnCount = this.DataList.Count;
                }

                if (this._RowType == RowType.HeaderRow)
                {
                    this.SetColumnNamesThis();
                }
            }

            // Increment our file row counter to help with debugging in case of an error in syntax.
            ++this.FileRowNumber;
        }
        /// <summary>
        ///   Takes a range within the character buffer and extracts the desired
        ///   string from within it and places it into the DataArray.  If an escape
        ///   character has been set, the escape characters are stripped out and the
        ///   unescaped string is returned.
        /// </summary>
        /// <param name="intEndOfDataIndex">The index of the last character in the column.</param>
        /// <exception cref="ParsingException">
        ///   In the event that the <see cref="ExpectedColumnCount"/> is set to a value of greater
        ///   than zero (which is by default for a fixed width format) and the number of columns
        ///   found differs from what's expected, this exception will be thrown.
        /// </exception>
        private void ExtractColumnThis(int intEndOfDataIndex)
        {
            int intStartOfDataIndex, intRemovedCharacters;
            bool blnTrimResults, blnInText;

            // Make sure we haven't exceeded our expected column count.
            if ((this._ExpectedColumnCount > 0) && (this.DataList.Count >= this._ExpectedColumnCount))
            {
                throw this.CreateParsingExceptionThis(string.Format("Current column {0} exceeds ExpectedColumnCount of {1}.",
                  this.DataList.Count + 1,
                  this._ExpectedColumnCount));
            }

            // If we have a length less than 1 character, it means we have an empty string, so bypass this logic.
            if (intEndOfDataIndex >= this._StartOfCurrentColumnIndex)
            {
                // Handle quoted text by stripping off any text qualifiers, if they are present.
                if (this._FoundTextQualifierAtStart && (this._Buffer[intEndOfDataIndex] == this._TextQualifier))
                {
                    // Only trim on non-textqualified strings.
                    blnTrimResults = false;
                    blnInText = true;

                    intStartOfDataIndex = this._StartOfCurrentColumnIndex + 1;
                    --intEndOfDataIndex;
                }
                else
                {
                    blnTrimResults = this._TrimResults;
                    blnInText = false;

                    intStartOfDataIndex = this._StartOfCurrentColumnIndex;
                }

                // Before trimming the results, we need to check to see if we need to strip control characters.
                if (this._StripControlChars || this._ContainsEscapedCharacters)
                {
                    intRemovedCharacters = 0;

                    // Escape out all of the control characters by sliding down the subsequent characters over them.
                    for (int intSource = intStartOfDataIndex, intDestination = intStartOfDataIndex; intSource <= intEndOfDataIndex; ++intSource)
                    {
                        // For every control character found, we must move up the source indice and increment the stripped counter.
                        if (this._StripControlChars && char.IsControl(this._Buffer[intSource]))
                        {
                            ++intRemovedCharacters;
                            continue;
                        }
                        else if ((this._Buffer[intSource] == this._EscapeCharacter) || (blnInText && (this._Buffer[intSource] == this._TextQualifier)))
                        {
                            ++intRemovedCharacters;

                            // If we hit an escape character or a text qualifier, it must be an escaped character.
                            if (++intSource > intEndOfDataIndex)
                            {
                                break;
                            }
                        }
                        else if (intRemovedCharacters == 0)
                        {
                            // If we haven't found any characters to remove, just continue onto the next character.
                            ++intDestination;
                            continue;
                        }

                        this._Buffer[intDestination++] = this._Buffer[intSource];
                    }

                    // For every stripped character, we must decrement the ending indice.
                    intEndOfDataIndex -= intRemovedCharacters;
                }

                if (blnTrimResults)
                {
                    // Move up the beginning indice if we have white-space.
                    while ((intStartOfDataIndex <= intEndOfDataIndex) && char.IsWhiteSpace(this._Buffer[intStartOfDataIndex]))
                    {
                        ++intStartOfDataIndex;
                    }

                    // Move up the ending indice if we have white-space.
                    while ((intStartOfDataIndex <= intEndOfDataIndex) && char.IsWhiteSpace(this._Buffer[intEndOfDataIndex]))
                    {
                        --intEndOfDataIndex;
                    }
                }

                // Add the results to the string collection of data.
                this.DataList.Add(new string(this._Buffer, intStartOfDataIndex, intEndOfDataIndex - intStartOfDataIndex + 1));
            }
            else
            {
                this.DataList.Add(string.Empty);
            }

            // If we're extending beyond the supplied column headings, add a new column.
            if ((!this._FirstRowHasHeader || this._HeaderRowFound) && (this.DataList.Count > this.ColumnNames.Count))
            {
                this.ColumnNames.Add(null);
            }
        }
        /// <summary>
        ///   When the buffer has reached the end of its parsing and there are no more
        ///   complete columns to be parsed, the remaining data must be moved up to the
        ///   front of the buffer so that the next batch of data can be appended to
        ///   the end.
        /// </summary>
        /// <param name="intStartIndex">The index that starts the beginning of the data to be moved.</param>
        /// <exception cref="ParsingException">In the event that the entire buffer is full and a single
        /// column cannot be parsed from it, parsing can no longer continue.</exception>
        private void CopyRemainingDataToFrontThis(int intStartIndex)
        {
            // Make sure we haven't exceeded our buffer size.
            if ((intStartIndex == 0) && (this._NumberOfCharactersInBuffer == this._MaxBufferSize))
            {
                throw this.CreateParsingExceptionThis("MaxBufferSize exceeded. Try increasing the buffer size.");
            }
            else if (this._RowType != RowType.CommentRow)
            {
                int intLength = (this._NumberOfCharactersInBuffer - intStartIndex);

                // Shift the value from the end of the buffer to the beginning.
                if (intStartIndex > 0)
                {
                    Array.Copy(this._Buffer, intStartIndex, this._Buffer, 0, intLength);
                }

                // Set the next position to begin placing data.
                this._StartIndexOfNewData = intLength;
                this._ReadIndex = intLength;
                this._StartOfCurrentColumnIndex = 0;
            }
            else
            {
                // Throw away the data in the buffer if we're in a comment row.
                this._StartIndexOfNewData = 0;
                this._ReadIndex = 0;
                this._StartOfCurrentColumnIndex = 0;
            }
        }
        /// <summary>
        ///   Returns the name of the Column based on its ColumnIndex.
        /// </summary>
        /// <param name="intColumnIndex">The column index to return the name for.</param>
        /// <remarks>
        ///   If the column is not found or the index is outside the range
        ///   of possible columns, <see langword="null"/> will be returned.
        /// </remarks>
        /// <returns>The name of the column at the given ColumnIndex, if
        /// none exists <see langword="null"/> is returned.</returns>
        private string GetColumnNameThis(int intColumnIndex)
        {
            return this._HeaderRowFound && ((intColumnIndex > -1) && (intColumnIndex < this.ColumnNames.Count))
                ? this.ColumnNames[intColumnIndex]
                : null;
        }
        /// <summary>
        ///   Returns the index of the Column based on its Name.
        /// </summary>
        /// <remarks>
        ///   <para>
        ///     <see langword="null"/> column name is not a valid name for a column.
        ///   </para>
        ///   <para>
        ///     If the column is not found, the column index will be -1.
        ///   </para>  
        /// </remarks>
        /// <param name="strColumnName">The name of the column to find the index for.</param>
        /// <returns>The index of the column with the name strColumnName.
        /// If none exists, -1 will be returned.</returns>
        private int GetColumnIndexThis(string strColumnName)
        {
            return this._HeaderRowFound && (strColumnName != null) ? this.ColumnNames.IndexOf(strColumnName) : -1;
        }
        /// <summary>
        ///   Creates a detailed message for a parsing exception and then throws it.
        /// </summary>
        /// <param name="strMessage">The exception specific information to go into the <see cref="ParsingException"/>.</param>
        /// <returns>The <see cref="ParsingException"/> with the provided message.</returns>
        private ParsingException CreateParsingExceptionThis(string strMessage)
        {
            int intColumnNumber;

            intColumnNumber = (this.DataList != null) ? this.DataList.Count : -1;

            return new ParsingException(
                string.Format("{0} [Row: {1}, Column: {2}]",
                    strMessage,
                    this.FileRowNumber,
                    intColumnNumber),
                this.FileRowNumber,
                intColumnNumber);
        }
        /// <summary>
        ///   Initializes the parsing variables for the GenericParser.
        /// </summary>
        private void InitializeConfigurationVariablesThis()
        {
            this._IaColumnWidths = null;
            this._MaxBufferSize = GenericParser.DefaultMaxBufferSize;
            this._MaxRows = GenericParser.DefaultMaxRows;
            this._SkipStartingDataRows = GenericParser.DefaultSkipStartingDataRows;
            this._ExpectedColumnCount = GenericParser.DefaultExpectedColumnCount;
            this._FirstRowHasHeader = GenericParser.DefaultFirstRowHasHeader;
            this._TrimResults = GenericParser.DefaultTrimResults;
            this._StripControlChars = GenericParser.DefaulStripControlCharacters;
            this._SkipEmptyRows = GenericParser.DefaulSkipEmptyRows;
            this._TextFieldType = GenericParser.DefaultTextFieldType;
            this._FirstRowSetsExpectedColumnCount = GenericParser.DefaultFirstRowSetsExpectedColumnCount;
            this._ColumnDelimiter = GenericParser.DefaultColumnDelimiter;
            this._TextQualifier = GenericParser.DefaultTextQualifier;
            this._EscapeCharacter = null;
            this._CommentCharacter = GenericParser.DefaultCommentCharacter;
        }

        #endregion Private Code
    }
}
