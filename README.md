# IO Libraries

Supporting structured I/O such as delimited files, XML signing or netlists.

* [Source Code](#Source-Code)
* [MIT License](LICENSE.md)
* [Change Log](CHANGELOG.md)
* [Facilitated By](#FacilitatedBy)
* [Authors](#Authors)
* [Acknowledgments](#Acknowledgments)
* [Open Source](#Open-Source)
* [Closed Software](#Closed-software)

<a name="Source-Code"></a>
## Source Code
Clone the repository along with its requisite repositories to their respective relative path.

### Repositories
The repositories listed in [external repositories](ExternalReposCommits.csv) are required:
* [Core](https://www.bitbucket.org/davidhary/vs.core) - Core Libraries
* [IO](https://www.bitbucket.org/davidhary/vs.io) - IO Libraries

```
git clone git@bitbucket.org:davidhary/vs.core.git
git clone git@bitbucket.org:davidhary/vs.io.git
```

Clone the repositories into the following relative path(s) (parents of the .git folder):
```
.\Libraries\VS\IO\IO
.\Libraries\VS\Core\Core
```

#### Global Configuration Files
ISR libraries use a global editor configuration file and a global test run settings file. 
These files can be found in the [IDE Repository]((https://www.bitbucket.org/davidhary/vs.ide)).

Restoring Editor Configuration assuming c:\My is the root folder of the .NET solutions):
```
xcopy /Y c:\My\.editorconfig c:\My\.editorconfig.bak
xcopy /Y c:\My\Libraries\VS\Core\IDE\code\.editorconfig c:\My\.editorconfig
```

Restoring Run Settings assuming c:\user\<me> is the root user folder:
```
xcopy /Y c:\user\<me>\.runsettings c:\user\<me>\.runsettings.bak
xcopy /Y c:\My\Libraries\VS\Core\IDE\code\.runsettings c:\user\<me>\.runsettings
```

<a name="FacilitatedBy"></a>
## Facilitated By
* [Visual Studio](https://www.visualstudio.com/) - Visual Studio
* [Jarte](https://www.jarte.com/) - RTF Editor
* [Wix Installer](https://www.wixtoolset.org/) - WiX Toolset
* [Atomineer Code Documentation](https://www.atomineerutils.com/) - Code Documentation
* [EW Software](https://github.com/EWSoftware/VSSpellChecker/wiki/) - Spell Checker
* [Code Converter](https://github.com/icsharpcode/CodeConverter) - Code Converter
* [Search and Replace](http://www.funduc.com/search_replace.htm) - Funduc Search and Replace for Windows
* [Generic Parser](https://www.codeproject.com/Articles/11698/A-Portable-and-Efficient-Generic-Parser-for-Flat-F)
* [ODS Read Write](https://www.codeproject.com/Articles/38425/How-to-Read-and-Write-ODF-ODS-Files-OpenDocument-2)
* [dot net zip](https://archive.codeplex.com/?p=DotNetZip)

<a name="Authors"></a>
## Authors
* [ATE Coder](https://www.IntegratedScientificResources.com)

<a name="Acknowledgments"></a>
## Acknowledgments
* [Its all a remix](https://www.everythingisaremix.info) -- we are but a spec on the shoulders of giants
* [John Simmons](https://www.codeproject.com/script/Membership/View.aspx?mid=7741) - outlaw programmer
* [Stack overflow](https://www.stackoveflow.com) - Joel Spolsky

<a name="Open-Source"></a>
### Open source
Open source used by this software is described and licensed at the
following sites:  
[Core Libraries](https://bitbucket.org/davidhary/vs.core)  
[Excel Data Manipulation Using VB.NET](http://www.CodeProject.com/KB/vb/ExcelDataManipulation.aspx)  
[File System Watcher Pure Chaos](http://www.codeproject.com/Articles/58740/FileSystemWatcher-Pure-Chaos-Part-of)  
[Text file data set](https://www.codeproject.com/Articles/22400/Converting-text-files-CSV-to-datasets)  
[Generic Parser](https://www.codeproject.com/Articles/11698/A-Portable-and-Efficient-Generic-Parser-for-Flat-F)  
[ODS Read Write](https://www.codeproject.com/Articles/38425/How-to-Read-and-Write-ODF-ODS-Files-OpenDocument-2)  
[dot net zip](https://archive.codeplex.com/?p=DotNetZip)

<a name="Closed-software"></a>
### Closed software
Closed software used by this software are described and licensed on
the following sites:  
[Core Libraries](https://bitbucket.org/davidhary/vs.core)

