Imports System.Reflection

' End of C:\My\Libraries\VS\IO\IO\Items\SolutionInfo.vb
<Assembly: AssemblyCompany("Integrated Scientific Resources")>
<Assembly: AssemblyCopyright("(c) 2005 Integrated Scientific Resources, Inc. All rights reserved.")>
<Assembly: AssemblyTrademark("Licensed under The MIT License.")>
<Assembly: Resources.NeutralResourcesLanguage("en-US", Resources.UltimateResourceFallbackLocation.MainAssembly)>
