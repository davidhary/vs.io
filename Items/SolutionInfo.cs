using System.Reflection;

[assembly: AssemblyCompany( "Integrated Scientific Resources" )]
[assembly: AssemblyCopyright( "(c) 2005 Integrated Scientific Resources, Inc. All rights reserved." )]
[assembly: AssemblyTrademark( "Licensed under The MIT License." )]
[assembly: System.Resources.NeutralResourcesLanguage( "en-US", System.Resources.UltimateResourceFallbackLocation.MainAssembly )]
